#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("spiderWalkMTOPT")
int main(int argc,char** argv)
{
  OmpSettings::getOmpSettingsNonConst().setNrThreads((int)omp_get_num_procs());
  FEMEnvironmentMT env;
  ASSERT(exists(NAME+"Learned.dat"))
  env.Environment::read(NAME+"Learned.dat");
  //env.debugDMP(NAME,200,0.05f);
  //exit(-1);
  string addParam=parseProps(argc,argv,env.sol()._tree);

  //configure QLearning
  boost::shared_ptr<FEMEnvironmentDQN> envDQL;
  if(!exists(NAME+addParam+"DQN.dat")) {
    env.sol()._tree.put<bool>("useNeuralQ",true);
    env.sol()._tree.put<scalar>("ignoreDirY",1);
    boost::property_tree::ptree pt;
    readPtreeAscii(pt,"optTrajs.xml");
    envDQL=env.upgradeDQN(&pt);
    envDQL->Environment::write(NAME+addParam+"DQN.dat");
  } else {
    envDQL.reset(new FEMEnvironmentDQN);
    envDQL->Environment::read(NAME+addParam+"DQN.dat");
  }
  envDQL->setUseFit(envDQL->sol()._tree.get<bool>("useFitTrajectory",true));
  envDQL->setUseFixTarget(envDQL->sol()._tree.get<bool>("useFixTarget",false));
  envDQL->setUsePeriodicTarget(6,1);

  //learn
  if(!exists(NAME+addParam+"LearnedDQN.dat")) {
    QLearning learner(*envDQL);
    envDQL->getPolicy()._net->_tree.put<sizeType>("maxIterations",1E2);
    envDQL->getPolicy()._net->_tree.put<bool>("callbackLBFGS",false);
    learner._tree.put<scalar>("learningRate",0.1f);
    learner._tree.put<sizeType>("nrTraj",envDQL->nrTraj());
    learner._tree.put<sizeType>("trajLen",100);
    learner._tree.put<sizeType>("maxIter",3000);
    learner._tree.put<sizeType>("maxIterOpt",1);
    learner._tree.put<bool>("useWholeMemoryNewton",false);
    learner._tree.put<bool>("useWholeMemoryRMSProp",false);
    learner._tree.put<bool>("useWholeMemoryLBFGS",true);
    parseProps(argc,argv,learner._tree);
    learner.learn();
    envDQL->Environment::write(NAME+addParam+"LearnedDQN.dat");
    envDQL->debugTrajectory("./trajs",
                            learner._tree.get<sizeType>("nrTraj"),
                            learner._tree.get<sizeType>("trajLen"));
  } else {
    envDQL->Environment::read(NAME+addParam+"LearnedDQN.dat");
    parseProps(argc,argv,envDQL->sol()._tree);
    envDQL->setUseFit(false);
    envDQL->setUseFixTarget(false);
    envDQL->addEmbeddedMesh("./Models/4LegWalkerOpenS1.obj");
    envDQL->getPolicy().setEps(0);
    envDQL->debugTrajectory("./trajs",3,200);
  }
  return 0;
}
