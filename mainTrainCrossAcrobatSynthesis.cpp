#include "mainUtils.h"
#include <SoftBodyController/FEMDDPSynthesis.h>

USE_PRJ_NAMESPACE

#define PATH string("CrossAcrobatNew3/")
#define NAME string("crossRollZTwiceOPTLearned")
int main(int argc,char** argv)
{
  FEMEnvironment env;
  ASSERT(exists(PATH+NAME+".dat"))
  env.Environment::read(PATH+NAME+".dat");

  FEMDDPSynthesis syn(env);
  readPtreeAscii(syn._tree,PATH+"optTrajs.xml");
  syn.writeStatesVTK(PATH+"crossAcrobatSynthesis",NULL,false,true);
  return 0;
}
