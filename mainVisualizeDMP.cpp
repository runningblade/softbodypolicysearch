#include "mainUtils.h"
#include <CommonFile/geom/StaticGeom.h>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  ASSERT(argc == 2)
  FEMEnvironment env;
  FEMDDPSolver sol(env);
  boost::filesystem::path path(argv[1]);
  boost::filesystem::ifstream is(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  sol.read(is,dat.get());
  FEMDDPPlotter plotter(sol);
  plotter.writeDMPPolicyMaxima("norm",path.replace_extension("mac").string(),true,5);
  plotter.writeDMPPolicyMaxima("norm",path.replace_extension("macAll").string(),false,1);
  plotter.writeDMPPolicyMaximaAnalytic("norm",path.replace_extension("macAnalytic").string());
  return 0;
}
