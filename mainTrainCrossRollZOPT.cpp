#include "mainUtils.h"
#include <CommonFile/RotationUtil.h>

USE_PRJ_NAMESPACE

#define NAME string("crossRollZOPT")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  createCrossRollZ(tree,10);

  parseProps(argc,argv,tree);

  FEMEnvironment env;
  if(!exists(NAME+".dat")) {
    env.reset(tree);
    env.Environment::write(NAME+".dat");
  } else {
    env.Environment::read(NAME+".dat");
  }
  env.sol().getBody()._tree.put<bool>("MTCubature",false);
  env.sol()._tree.put<bool>("useCallback",false);

  FEMDDPSolver learner(env);
  learner._tree.put<sizeType>("nrFrm",2E2);
  learner._tree.put<sizeType>("nrIterInner",1E3);
  learner._tree.put<scalar>("regPhysicsCoef",2E5);
  //learner._tree.put<scalar>("minPhysViolation",0.01f);
  //learner._tree.put<scalar>("maxPhysViolation",0.01f);
  learner._tree.put<scalar>("muConicShuffleAvoidance",1);
  learner._tree.put<scalar>("regConicShuffleAvoidance",0);
  learner._tree.put<scalar>("regContactActivationCoef",4E4);
  learner._tree.put<scalar>("regCoef",1E-2f);
  learner._tree.put<scalar>("k1",1);
  learner._tree.put<bool>("frequentContactUpdate",true);
  learner._tree.put<bool>("handleSelfCollision",true);
  learner._tree.put<bool>("periodicPosition",false);
  learner._tree.put<bool>("periodicRotation",false);
  {
    boost::property_tree::ptree init;
    init.put<sizeType>("key0",0);
    init.put<scalar>("rotX0",0);
    init.put<scalar>("rotY0",0);
    init.put<scalar>("rotZ0",0);

    init.put<sizeType>("key1",60);
    init.put<scalar>("rotX1",0);
    init.put<scalar>("rotY1",0);
    init.put<scalar>("rotZ1",0);

    init.put<sizeType>("key2",140);
    init.put<scalar>("rotX2",0);
    init.put<scalar>("rotY2",0);
    init.put<scalar>("rotZ2",M_PI);
    learner._tree.add_child("smartInitialization",init);
  }
  parseProps(argc,argv,learner._tree);
  learner.resetParam(learner._tree.get<sizeType>("nrIterInner"),1,learner._tree.get<sizeType>("nrFrm"));
  learner.resetEps(1E-5f,1E-5f,0);

  //learner._tree.put<bool>("fixRotation",true);
  //learner.debugCIOSQPEnergy(true,true,true);
  //learner.debugCIOGradient(true,true);
  //learner.debugCIOLMEnergy(true,true);
  //learner.debugCIOForce(true,true);

  learner.optimizeCIOPolicy(true,true);
  learner.writeStatesVTK(NAME+"Learned");
  env.Environment::write(NAME+"Learned.dat");
  writePtreeAscii(learner._tree,"optTrajs.xml");
  return 0;
}
