#ifndef MAIN_UTILS_H
#define MAIN_UTILS_H

#include <SoftBodyController/Environment.h>
#include <SoftBodyController/DeepQLearning.h>
#include <SoftBodyController/FEMEnvironmentDQNObstacle.h>
#include <SoftBodyController/FEMDDPSolverMT.h>
#include <SoftBodyController/FEMDDPSolver.h>
#include <SoftBodyController/FEMDDPPlotter.h>
#include <SoftBodyController/TRPO.h>
#include <Dynamics/FEMMeshFormat.h>
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/FEMUtils.h>
#include <CMAES/MPIInterface.h>
#include <CMAES/EvolutionInterface.h>

USE_PRJ_NAMESPACE

void createDolphinSwim(boost::property_tree::ptree& tree,sizeType nrDOF,bool useDMP,bool isMT)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //fluid
  tree.put<bool>("useCubAD",true);
  tree.put<scalar>("coefFluidDrag",1000.0f);
  //gravity
  tree.put<scalar>("gravityCoef",0);
  //geometry
  tree.put<scalar>("floorSize",-1);
  //policy
  if(useDMP)
    tree.put<sizeType>("DMPBasis",5);
  else {
    tree.put<sizeType>("nrH1",40);
    tree.put<sizeType>("nrH2",40);
  }
  //model
  tree.put<string>("model","./meshes/dolphin/dolphin.abq");
  tree.put<bool>("recenter",true);
  //energy
  if(isMT) {
    //task0
    boost::property_tree::ptree& pt0=
      tree.add_child("taskEnergy0",boost::property_tree::ptree());
    pt0.put<scalar>("coefBalance",1000);
    pt0.put<scalar>("coefCurvedWalk",1000);
    pt0.put<scalar>("curveWalkVelZ",1);
    pt0.put<scalar>("curveWalkRotY",0);
    //task1
    boost::property_tree::ptree& pt1=
      tree.add_child("taskEnergy1",boost::property_tree::ptree());
    pt1.put<scalar>("coefBalance",1000);
    pt1.put<scalar>("coefCurvedWalk",1000);
    pt1.put<scalar>("curveWalkVelZ",1);
    pt1.put<scalar>("curveWalkRotY",4);
    //task2
    boost::property_tree::ptree& pt2=
      tree.add_child("taskEnergy2",boost::property_tree::ptree());
    pt2.put<scalar>("coefBalance",1000);
    pt2.put<scalar>("coefCurvedWalk",1000);
    pt2.put<scalar>("curveWalkVelZ",1);
    pt2.put<scalar>("curveWalkRotY",-4);
  } else if(useDMP) {
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("coefWalkTo",1000);
    tree.put<scalar>("peakWalkingTime0",0.01);
    tree.put<scalar>("peakWalkingTime1",9.99);
    tree.put<scalar>("targetWalkingPositionZ0",0);
    tree.put<scalar>("targetWalkingPositionZ1",10);
  } else {
    tree.put<scalar>("coefCurvedWalk",1000);
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("curveWalkVelZ",1);
  }
}
void createFishSwim(boost::property_tree::ptree& tree,sizeType nrDOF,bool useDMP,bool isMT,bool isCMT=false)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //fluid
  tree.put<bool>("useCubAD",true);
  tree.put<scalar>("coefFluidDrag",1000.0f);
  //gravity
  tree.put<scalar>("gravityCoef",0);
  //geometry
  tree.put<scalar>("floorSize",-1);
  //policy
  if(useDMP)
    tree.put<sizeType>("DMPBasis",5);
  else {
    tree.put<sizeType>("nrH1",40);
    tree.put<sizeType>("nrH2",40);
  }
  //model
  tree.put<string>("model","./Models/GoldenFishSmall.ABQ");
  tree.put<bool>("recenter",true);
  //energy
  if(isMT) {
    //task0
    boost::property_tree::ptree& pt0=
      tree.add_child("taskEnergy0",boost::property_tree::ptree());
    pt0.put<scalar>("coefBalance",1000);
    pt0.put<scalar>("coefCurvedWalk",1000);
    pt0.put<scalar>("curveWalkVelX",1);
    pt0.put<scalar>("curveWalkRotY",4);
    //task1
    boost::property_tree::ptree& pt1=
      tree.add_child("taskEnergy1",boost::property_tree::ptree());
    pt1.put<scalar>("coefBalance",1000);
    pt1.put<scalar>("coefCurvedWalk",1000);
    pt1.put<scalar>("curveWalkVelX",1);
    pt1.put<scalar>("curveWalkRotY",-4);
    //task2
    boost::property_tree::ptree& pt2=
      tree.add_child("taskEnergy2",boost::property_tree::ptree());
    pt2.put<scalar>("coefBalance",1000);
    pt2.put<scalar>("coefWalkTo",1000);
    pt2.put<scalar>("peakWalkingTime0",0.01);
    pt2.put<scalar>("peakWalkingTime1",9.99);
    pt2.put<scalar>("targetWalkingPositionX0",0);
    pt2.put<scalar>("targetWalkingPositionX1",10);
  } else if(useDMP) {
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("coefWalkTo",1000);
    tree.put<scalar>("peakWalkingTime0",0.01);
    tree.put<scalar>("peakWalkingTime1",9.99);
    tree.put<scalar>("targetWalkingPositionX0",0);
    tree.put<scalar>("targetWalkingPositionX1",10);
  } else {
    tree.put<scalar>("coefCurvedWalk",1000);
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("curveWalkVelX",1);
    tree.put<scalar>("curveWalkRotY",1);
    if(isCMT)
      tree.put<sizeType>("curveWalkMTId",2);
  }
}
void createArticulatedSwim(boost::property_tree::ptree& tree,sizeType nrSeg,bool useDMP,bool isMT,bool isCMT=false)
{
  tree.put<sizeType>("dim",3);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //fluid
  tree.put<bool>("useCubAD",false);
  tree.put<scalar>("coefFluidDrag",1000.0f);
  //gravity
  tree.put<scalar>("gravityCoef",0);
  //geometry
  tree.put<scalar>("floorSize",-1);
  //policy
  if(useDMP)
    tree.put<sizeType>("DMPBasis",5);
  else {
    tree.put<sizeType>("nrH1",40);
    tree.put<sizeType>("nrH2",40);
  }
  //model
  boost::property_tree::ptree pt;
  FEMMeshFormat::genSwimmer3D(pt,nrSeg,1,0.5f,0.1f,0.1f,0,true);
  pt.put<scalar>("limitCoef",1E3f);
  tree.add_child("articulatedModel",pt);
  tree.put<bool>("recenter",true);
  //energy
  if(isMT) {
    //task0
    boost::property_tree::ptree& pt0=
      tree.add_child("taskEnergy0",boost::property_tree::ptree());
    //pt0.put<scalar>("coefBalance",1000);
    pt0.put<scalar>("coefCurvedWalk",1000);
    pt0.put<scalar>("curveWalkVelX",1);
    pt0.put<scalar>("curveWalkRotY",4);
    //task1
    boost::property_tree::ptree& pt1=
      tree.add_child("taskEnergy1",boost::property_tree::ptree());
    //pt1.put<scalar>("coefBalance",1000);
    pt1.put<scalar>("coefCurvedWalk",1000);
    pt1.put<scalar>("curveWalkVelX",1);
    pt1.put<scalar>("curveWalkRotY",-4);
    //task2
    boost::property_tree::ptree& pt2=
      tree.add_child("taskEnergy2",boost::property_tree::ptree());
    //pt2.put<scalar>("coefBalance",1000);
    pt2.put<scalar>("coefWalkTo",1000);
    pt2.put<scalar>("peakWalkingTime0",0.01);
    pt2.put<scalar>("peakWalkingTime1",9.99);
    pt2.put<scalar>("targetWalkingPositionX0",0);
    pt2.put<scalar>("targetWalkingPositionX1",10);
  } else if(useDMP) {
    //tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("coefWalkTo",1000);
    tree.put<scalar>("peakWalkingTime0",0.01);
    tree.put<scalar>("peakWalkingTime1",9.99);
    tree.put<scalar>("targetWalkingPositionX0",0);
    tree.put<scalar>("targetWalkingPositionX1",10);
  } else {
    tree.put<scalar>("coefCurvedWalk",1000);
    //tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("curveWalkVelX",1);
    //tree.put<scalar>("curveWalkRotY",1);
    if(isCMT)
      tree.put<sizeType>("curveWalkMTId",2);
  }
}
void createSpiderSwim(boost::property_tree::ptree& tree,sizeType nrDOF,bool useDMP,bool isMT)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //fluid
  tree.put<bool>("useCubAD",true);
  tree.put<scalar>("coefFluidDrag",1000.0f);
  //gravity
  tree.put<scalar>("gravityCoef",0);
  //geometry
  tree.put<scalar>("floorSize",-1);
  //policy
  if(useDMP)
    tree.put<sizeType>("DMPBasis",5);
  else {
    tree.put<sizeType>("nrH1",40);
    tree.put<sizeType>("nrH2",40);
  }
  //model
  tree.put<string>("model","./Models/4LegWalker.ABQ");
  tree.put<bool>("recenter",true);
  //energy
  if(isMT) {
    //task0
    boost::property_tree::ptree& pt0=
      tree.add_child("taskEnergy0",boost::property_tree::ptree());
    pt0.put<scalar>("coefOrientationTo",1000);
    pt0.put<scalar>("peakOrientationTime0",0.01);
    pt0.put<scalar>("peakOrientationTime1",9.99);
    pt0.put<scalar>("3DOrientationToX0",0);
    pt0.put<scalar>("3DOrientationToX1",-5);
    //task1
    boost::property_tree::ptree& pt1=
      tree.add_child("taskEnergy1",boost::property_tree::ptree());
    pt1.put<scalar>("coefOrientationTo",1000);
    pt1.put<scalar>("peakOrientationTime0",0.01);
    pt1.put<scalar>("peakOrientationTime1",9.99);
    pt1.put<scalar>("3DOrientationToX0",0);
    pt1.put<scalar>("3DOrientationToX1",5);
    //task2
    boost::property_tree::ptree& pt2=
      tree.add_child("taskEnergy2",boost::property_tree::ptree());
    pt2.put<scalar>("coefOrientationTo",1000);
    pt2.put<scalar>("peakOrientationTime0",0.01);
    pt2.put<scalar>("peakOrientationTime1",9.99);
    pt2.put<scalar>("3DOrientationToZ0",0);
    pt2.put<scalar>("3DOrientationToZ1",-5);
    //task3
    boost::property_tree::ptree& pt3=
      tree.add_child("taskEnergy3",boost::property_tree::ptree());
    pt3.put<scalar>("coefOrientationTo",1000);
    pt3.put<scalar>("peakOrientationTime0",0.01);
    pt3.put<scalar>("peakOrientationTime1",9.99);
    pt3.put<scalar>("3DOrientationToZ0",0);
    pt3.put<scalar>("3DOrientationToZ1",5);
    //task4
    boost::property_tree::ptree& pt4=
      tree.add_child("taskEnergy4",boost::property_tree::ptree());
    pt4.put<scalar>("coefBalance",1000);
    pt4.put<scalar>("coefWalkTo",4000);
    pt4.put<scalar>("peakWalkingTime0",0.01);
    pt4.put<scalar>("peakWalkingTime1",9.99);
    pt4.put<scalar>("targetWalkingPositionY0",0);
    pt4.put<scalar>("targetWalkingPositionY1",5);
  } else {
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("coefWalkTo",4000);
    tree.put<scalar>("peakWalkingTime0",0.01);
    tree.put<scalar>("peakWalkingTime1",9.99);
    tree.put<scalar>("targetWalkingPositionY0",0);
    tree.put<scalar>("targetWalkingPositionY1",5);
    //tree.put<scalar>("coefCurvedWalk",1000);
    //tree.put<scalar>("curveWalkVelY",0.5);
  }
}
void createSpiderWalk(boost::property_tree::ptree& tree,sizeType nrDOF,bool useDMP,bool isMT)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("thresCollisionDelete",-1);
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("eps",1E-6);
  tree.put<scalar>("collK",1E2);
  tree.put<scalar>("collKCIO",1E6);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBLThres",0.1);
  tree.put<sizeType>("maxIter",1000);
  //policy
  if(useDMP)
    tree.put<sizeType>("DMPBasis",5);
  else {
    tree.put<sizeType>("nrH1",40);
    tree.put<sizeType>("nrH2",40);
  }
  //model
  tree.put<string>("model","./Models/4LegWalker.ABQ");
  //energy
  if(isMT) {
    //task0
    boost::property_tree::ptree& pt0=
      tree.add_child("taskEnergy0",boost::property_tree::ptree());
    pt0.put<scalar>("coefCurvedWalk",1000);
    pt0.put<scalar>("coefBalance",1000);
    pt0.put<scalar>("curveWalkVelZ",0.5);
    //task1
    boost::property_tree::ptree& pt1=
      tree.add_child("taskEnergy1",boost::property_tree::ptree());
    pt1.put<scalar>("coefCurvedWalk",1000);
    pt1.put<scalar>("coefBalance",1000);
    pt1.put<scalar>("curveWalkVelZ",-0.5);
    //task2
    boost::property_tree::ptree& pt2=
      tree.add_child("taskEnergy2",boost::property_tree::ptree());
    pt2.put<scalar>("coefCurvedWalk",1000);
    pt2.put<scalar>("coefBalance",1000);
    pt2.put<scalar>("curveWalkVelX",0.5);
    //task3
    boost::property_tree::ptree& pt3=
      tree.add_child("taskEnergy3",boost::property_tree::ptree());
    pt3.put<scalar>("coefCurvedWalk",1000);
    pt3.put<scalar>("coefBalance",1000);
    pt3.put<scalar>("curveWalkVelX",-0.5);
  } else {
    tree.put<scalar>("coefCurvedWalk",1000);
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("curveWalkVelX",0.5);
  }
}
void createLetterTWalk(boost::property_tree::ptree& tree,sizeType nrDOF,bool isMT)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBLThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  //model
  tree.put<string>("model","./Models/LetterT.ABQ");
  //energy
  if(isMT) {
    //task0
    boost::property_tree::ptree& pt0=
      tree.add_child("taskEnergy0",boost::property_tree::ptree());
    pt0.put<scalar>("coefBalance",1000);
    pt0.put<scalar>("targetBalanceDirX0",1);
    pt0.put<scalar>("coefCurvedWalk",1000);
    pt0.put<scalar>("curveWalkVelX",1);
    //task1
    boost::property_tree::ptree& pt1=
      tree.add_child("taskEnergy1",boost::property_tree::ptree());
    pt1.put<scalar>("coefBalance",1000);
    pt1.put<scalar>("targetBalanceDirX0",1);
    pt1.put<scalar>("coefCurvedWalk",1000);
    pt1.put<scalar>("curveWalkVelX",-1);
    //task2
    boost::property_tree::ptree& pt2=
      tree.add_child("taskEnergy2",boost::property_tree::ptree());
    pt2.put<scalar>("coefBalance",1000);
    pt2.put<scalar>("targetBalanceDirX0",1);
    pt2.put<scalar>("coefCurvedWalk",1000);
    pt2.put<scalar>("curveWalkVelZ",1);
    //task3
    boost::property_tree::ptree& pt3=
      tree.add_child("taskEnergy3",boost::property_tree::ptree());
    pt3.put<scalar>("coefBalance",1000);
    pt3.put<scalar>("targetBalanceDirX0",1);
    pt3.put<scalar>("coefCurvedWalk",1000);
    pt3.put<scalar>("curveWalkVelZ",-1);
  } else {
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("coefCurvedWalk",1000);
    tree.put<scalar>("curveWalkVelX",1);
  }
}
void createLetterTWalk2D(boost::property_tree::ptree& tree,sizeType nrDOF,bool isMT)
{
  tree.put<sizeType>("dim",2);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBLThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  //model
  tree.put<string>("model","./Models/LetterT.svg");
  //energy
  if(isMT) {
    //task0
    boost::property_tree::ptree& pt0=
      tree.add_child("taskEnergy0",boost::property_tree::ptree());
    pt0.put<scalar>("coefBalance",1000);
    pt0.put<scalar>("targetBalanceDirX0",1);
    pt0.put<scalar>("coefCurvedWalk",1000);
    pt0.put<scalar>("curveWalkVelX",1);
    //task1
    boost::property_tree::ptree& pt1=
      tree.add_child("taskEnergy1",boost::property_tree::ptree());
    pt1.put<scalar>("coefBalance",1000);
    pt1.put<scalar>("targetBalanceDirX0",1);
    pt1.put<scalar>("coefCurvedWalk",1000);
    pt1.put<scalar>("curveWalkVelX",-1);
    //task2
    boost::property_tree::ptree& pt2=
      tree.add_child("taskEnergy2",boost::property_tree::ptree());
    pt2.put<scalar>("coefBalance",1000);
    pt2.put<scalar>("targetBalanceDirX0",1);
    pt2.put<scalar>("coefCurvedWalk",1000);
    pt2.put<scalar>("curveWalkVelZ",1);
    //task3
    boost::property_tree::ptree& pt3=
      tree.add_child("taskEnergy3",boost::property_tree::ptree());
    pt3.put<scalar>("coefBalance",1000);
    pt3.put<scalar>("targetBalanceDirX0",1);
    pt3.put<scalar>("coefCurvedWalk",1000);
    pt3.put<scalar>("curveWalkVelZ",-1);
  } else {
    tree.put<scalar>("coefBalance",1000);
    tree.put<scalar>("coefCurvedWalk",1000);
    tree.put<scalar>("curveWalkVelX",1);
  }
}
void createArticulatedJumper2D(boost::property_tree::ptree& tree)
{
  tree.put<sizeType>("dim",2);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotZ",-M_PI/2);
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBLThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  //model
  boost::property_tree::ptree pt;
  FEMMeshFormat::genJumper2D(pt,0.05f,0.05f,0.5f,0.5f);
  pt.put<scalar>("limitCoef",1E3f);
  tree.add_child("articulatedModel",pt);
  tree.put<bool>("recenter",true);
  //energy
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("targetBalanceDirY",1);
  tree.put<scalar>("targetBalanceDirLX",-1);
  //tree.put<scalar>("coefJump",1000);
  //tree.put<scalar>("peakJumpingTime0",-1);
  //tree.put<scalar>("targetJumpingHeight0",1);
}
void createArticulatedWalker2D(boost::property_tree::ptree& tree)
{
  tree.put<sizeType>("dim",2);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBLThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  //model
  boost::property_tree::ptree pt;
  FEMMeshFormat::genWalker2D(pt,0.05f,0.05f,0.2f,1.0f,-15.0f);
  pt.put<scalar>("limitCoef",1E4f);
  tree.add_child("articulatedModel",pt);
  tree.put<bool>("recenter",true);
  //energy
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("coefCurvedWalk",1000);
  tree.put<scalar>("curveWalkVelX",1);
}
void createDinoWalk(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",0.5f);
  tree.put<scalar>("contactBBLThres",0.05f);
  //policy
  tree.put<sizeType>("DMPBasis",2);//5);
  //model
  tree.put<string>("model","./Models/deformedDinoCut.abq");
  //energy
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("coefWalkTo",1000);
  tree.put<scalar>("peakWalkingTime0",0.0f);
  tree.put<scalar>("peakWalkingTime1",9.99f);
  tree.put<scalar>("targetWalkingPositionX0",0);
  tree.put<scalar>("targetWalkingPositionX1",10);
}
void createDinoWalkSpd(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",0.5f);
  tree.put<scalar>("contactBBLThres",0.05f);
  //policy
  tree.put<sizeType>("DMPBasis",2);//5);
  //model
  tree.put<string>("model","./Models/deformedDinoCut.abq");
  //energy
  tree.put<scalar>("coefCurvedWalk",1000);
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("curveWalkVelX",1);
}
void createTwoLegWalk(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",2);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("thresCollisionDelete",-1);
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("eps",1E-4f);
  tree.put<scalar>("collK",1E2);
  tree.put<scalar>("collKCIO",1E6);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBLThres",0.1);
  tree.put<sizeType>("maxIter",1E4);
  //policy
  tree.put<sizeType>("nrH1",40);
  tree.put<sizeType>("nrH2",40);
  //model
  tree.put<string>("model","./Models/TwoLeg.svg");
  //energy
  tree.put<scalar>("coefCurvedWalk",1000);
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("curveWalkVelX",0.5);
}
void createCrossRoll(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.05f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",0);  //check this
  tree.put<scalar>("contactBBThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  //model
  tree.put<string>("model","./Models/CrossLong.ABQ");
  //energy
  tree.put<scalar>("coefRoll3D",1000);
  tree.put<scalar>("targetRollingSpeedZ",2);
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("targetBalanceDirX",0);
  tree.put<scalar>("targetBalanceDirY",0);
  tree.put<scalar>("targetBalanceDirZ",1);
}
void createCrossRollZ(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.025f);
  tree.put<bool>("useGPU",false);
  tree.put<bool>("fixZ",true);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  tree.put<bool>("rhythmic",false);
  //model
  tree.put<string>("model","./Models/CrossLongRollZ.ABQ");
  tree.put<scalar>("gravityCoef",2.0f);
  //energy
  //height
  //tree.put<scalar>("coefJump",10000);
  //tree.put<scalar>("peakJumpingTime0",2.49f);
  //tree.put<scalar>("targetJumpingHeight0",1.0f);
  //orientation
  tree.put<scalar>("coefOrientationTo",10000);
  tree.put<scalar>("peakOrientationTime0",0.01f);
  tree.put<scalar>("3DOrientationToX0",0);
  tree.put<scalar>("3DOrientationToY0",0);
  tree.put<scalar>("3DOrientationToZ0",0);
  tree.put<scalar>("peakOrientationTime1",2.49f);
  tree.put<scalar>("3DOrientationToX1",0);
  tree.put<scalar>("3DOrientationToY1",0);
  tree.put<scalar>("3DOrientationToZ1",M_PI/2);
  tree.put<scalar>("peakOrientationTime2",4.99f);
  tree.put<scalar>("3DOrientationToX2",0);
  tree.put<scalar>("3DOrientationToY2",0);
  tree.put<scalar>("3DOrientationToZ2",M_PI);
  //balance
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("targetBalanceDirX",0);
  tree.put<scalar>("targetBalanceDirY",0);
  tree.put<scalar>("targetBalanceDirZ",1);
}

void createCrossRollZTwice(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.025f);
  tree.put<bool>("useGPU",false);
  tree.put<bool>("fixZ",true);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  tree.put<bool>("rhythmic",false);
  //model
  tree.put<string>("model","./Models/CrossLongRollZ.ABQ");
  tree.put<scalar>("gravityCoef",2.0f);
  //energy
  //height
  //tree.put<scalar>("coefJump",10000);
  //tree.put<scalar>("peakJumpingTime0",1.24f);
  //tree.put<scalar>("targetJumpingHeight0",1.0f);
  //tree.put<scalar>("peakJumpingTime1",3.74f);
  //tree.put<scalar>("targetJumpingHeight1",1.0f);
  //orientation
  tree.put<scalar>("coefOrientationTo",10000);
  tree.put<scalar>("peakOrientationTime0",0.01f);
  tree.put<scalar>("3DOrientationToX0",0);
  tree.put<scalar>("3DOrientationToY0",0);
  tree.put<scalar>("3DOrientationToZ0",0);
  tree.put<scalar>("peakOrientationTime1",1.24f);
  tree.put<scalar>("3DOrientationToX1",0);
  tree.put<scalar>("3DOrientationToY1",0);
  tree.put<scalar>("3DOrientationToZ1",M_PI/2);
  tree.put<scalar>("peakOrientationTime2",2.49f);
  tree.put<scalar>("3DOrientationToX2",0);
  tree.put<scalar>("3DOrientationToY2",0);
  tree.put<scalar>("3DOrientationToZ2",M_PI);
  tree.put<scalar>("peakOrientationTime3",3.74f);
  tree.put<scalar>("3DOrientationToX3",0);
  tree.put<scalar>("3DOrientationToY3",0);
  tree.put<scalar>("3DOrientationToZ3",M_PI/2);
  tree.put<scalar>("peakOrientationTime4",4.99f);
  tree.put<scalar>("3DOrientationToX4",0);
  tree.put<scalar>("3DOrientationToY4",0);
  tree.put<scalar>("3DOrientationToZ4",0);
  //balance
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("targetBalanceDirX",0);
  tree.put<scalar>("targetBalanceDirY",0);
  tree.put<scalar>("targetBalanceDirZ",1);
}
void createCrossWalk(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.01f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotZ",M_PI/4);
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  tree.put<scalar>("contactBBThres",0.1);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  //model
  tree.put<string>("model","./Models/CrossLong.ABQ");
  //energy
  tree.put<scalar>("coefWalk",1000);
  tree.put<scalar>("targetWalkingSpeedZ",2);
  //tree.put<scalar>("coefWalkTo",1000);
  //tree.put<scalar>("peakWalkingTime0",0.0f);
  //tree.put<scalar>("peakWalkingTime1",9.99f);
  //tree.put<scalar>("targetWalkingPositionZ0",0);
  //tree.put<scalar>("targetWalkingPositionZ1",10);
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("targetBalanceDirX0",0);
  tree.put<scalar>("targetBalanceDirY0",1);
  tree.put<scalar>("targetBalanceDirZ0",0);
  tree.put<scalar>("targetBalanceDirX1",1);
  tree.put<scalar>("targetBalanceDirY1",0);
  tree.put<scalar>("targetBalanceDirZ1",0);
}
void testNetReadWrite(NeuralNet& net)
{
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("net.dat",ios::binary);
    net.write(os,dat.get());
  }
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is("net.dat",ios::binary);
    net.read(is,dat.get());
  }
}

void createCactusJump(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.005f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  //tree.put<scalar>("contactBBThres",0.1);
  tree.put<scalar>("contactBBLThres",0.1);
  //gravity
  tree.put<scalar>("gravityCoef",9.81f);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  tree.put<bool>("rhythmic",true);
  //model
  tree.put<string>("model","./Models/cactus.ABQ");
  //energy
  //height
  tree.put<scalar>("coefJump",10000);
  tree.put<scalar>("peakJumpingTime0",0.49f);
  tree.put<scalar>("targetJumpingHeight0",1.0f);
  tree.put<scalar>("peakJumpingTime1",1.49f);
  tree.put<scalar>("targetJumpingHeight1",1.0f);
  //fix normal velocity
  tree.put<scalar>("coefWalkTo",10000);
  tree.put<scalar>("peakWalkingTime0",0.01f);
  tree.put<scalar>("targetWalkingPositionZ0",0);
  tree.put<bool>("targetWalkingPositionGT0",false);
  tree.put<bool>("targetWalkingPositionGN0",false);
  tree.put<scalar>("peakWalkingTime1",0.99f);
  tree.put<scalar>("targetWalkingPositionZ1",1);
  tree.put<bool>("targetWalkingPositionGT1",false);
  tree.put<bool>("targetWalkingPositionGN1",false);
  tree.put<scalar>("peakWalkingTime2",1.99f);
  tree.put<scalar>("targetWalkingPositionZ2",2);
  tree.put<bool>("targetWalkingPositionGT2",false);
  tree.put<bool>("targetWalkingPositionGN2",false);
  //balance
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("targetBalanceDirX0",1);
  tree.put<scalar>("targetBalanceDirY0",0);
  tree.put<scalar>("targetBalanceDirZ0",0);
  //tree.put<scalar>("targetBalanceDirX1",0);
  //tree.put<scalar>("targetBalanceDirY1",1);
  //tree.put<scalar>("targetBalanceDirZ1",0);
  //orientation to
  tree.put<scalar>("coefOrientationTo",10000);
  tree.put<scalar>("peakOrientationTime0",0.01f);
  tree.put<scalar>("peakOrientationTime1",0.99f);
  tree.put<scalar>("peakOrientationTime2",1.99f);
}
void createCactusJumpSingle(boost::property_tree::ptree& tree,sizeType nrDOF)
{
  tree.put<sizeType>("dim",3);
  tree.put<sizeType>("DOF",nrDOF);
  tree.put<scalar>("dt",0.02f);
  tree.put<bool>("useGPU",false);
  //initial condition
  tree.put<scalar>("initRotScale",0);
  tree.put<scalar>("initVelScale",0);
  tree.put<bool>("shapeBasedControl",false);
  //contact
  tree.put<scalar>("collMu",0.75f);
  tree.put<scalar>("collK",1E3);
  tree.put<scalar>("collKSelf",1E6);
  tree.put<scalar>("shuffleDistCoef",2);
  //tree.put<scalar>("contactBBThres",0.1);
  tree.put<scalar>("contactBBLThres",0.1);
  //gravity
  tree.put<scalar>("gravityCoef",2.0f);
  //policy
  tree.put<sizeType>("DMPBasis",5);
  tree.put<bool>("rhythmic",true);
  //model
  tree.put<string>("model","./Models/cactus.ABQ");
  //energy
  //height
  tree.put<scalar>("coefJump",10000);
  tree.put<scalar>("peakJumpingTime0",0.49f);
  tree.put<scalar>("targetJumpingHeight0",0.5f);
  tree.put<scalar>("peakJumpingTime1",1.49f);
  tree.put<scalar>("targetJumpingHeight1",0.5f);
  tree.put<scalar>("peakJumpingTime2",2.49f);
  tree.put<scalar>("targetJumpingHeight2",0.5f);
  tree.put<scalar>("peakJumpingTime3",3.49f);
  tree.put<scalar>("targetJumpingHeight3",0.5f);
  //fix normal velocity
  tree.put<scalar>("coefWalkTo",10000);
  tree.put<scalar>("peakWalkingTime0",0.01f);
  tree.put<scalar>("targetWalkingPositionZ0",0);
  tree.put<bool>("targetWalkingPositionGT0",false);
  tree.put<bool>("targetWalkingPositionGN0",false);
  tree.put<scalar>("peakWalkingTime1",0.99f);
  tree.put<scalar>("targetWalkingPositionZ1",1.5f);
  tree.put<bool>("targetWalkingPositionGT1",false);
  tree.put<bool>("targetWalkingPositionGN1",false);
  tree.put<scalar>("peakWalkingTime2",1.99f);
  tree.put<scalar>("targetWalkingPositionZ2",3.0f);
  tree.put<bool>("targetWalkingPositionGT2",false);
  tree.put<bool>("targetWalkingPositionGN2",false);
  tree.put<scalar>("peakWalkingTime3",2.99f);
  tree.put<scalar>("targetWalkingPositionZ3",4.5f);
  tree.put<bool>("targetWalkingPositionGT3",false);
  tree.put<bool>("targetWalkingPositionGN3",false);
  tree.put<scalar>("peakWalkingTime4",3.99f);
  tree.put<scalar>("targetWalkingPositionZ4",6.0f);
  tree.put<bool>("targetWalkingPositionGT4",false);
  tree.put<bool>("targetWalkingPositionGN4",false);
  //balance
  tree.put<scalar>("coefBalance",1000);
  tree.put<scalar>("targetBalanceDirX0",1);
  tree.put<scalar>("targetBalanceDirY0",0);
  tree.put<scalar>("targetBalanceDirZ0",0);
  //tree.put<scalar>("targetBalanceDirX1",0);
  //tree.put<scalar>("targetBalanceDirY1",1);
  //tree.put<scalar>("targetBalanceDirZ1",0);
  //orientation to
  tree.put<scalar>("coefOrientationTo",10000);
  tree.put<scalar>("peakOrientationTime0",0.01f);
  tree.put<scalar>("peakOrientationTime1",0.99f);
  tree.put<scalar>("peakOrientationTime2",1.99f);
  tree.put<scalar>("peakOrientationTime3",2.99f);
  tree.put<scalar>("peakOrientationTime4",3.99f);
}

#endif
