#include <SoftBodyController/NeuralNet.h>
#include <SoftBodyController/FEMFeature.h>
#include <SoftBodyController/TrainingData.h>
#include <SoftBodyController/CNNLayer.h>
#include <boost/lexical_cast.hpp>
#include "mainUtils.h"

USE_PRJ_NAMESPACE

void testCNN()
{
#define NR_ACTION 6
  vector<Vec4i,Eigen::aligned_allocator<Vec4i> > imgs;
  imgs.push_back(Vec4i(16,16,2,-1));
  imgs.push_back(Vec4i(3,2,16,1));
  imgs.push_back(Vec4i(2,3,6,2));
  imgs.push_back(Vec4i(2,2,12,2));
  NeuralNet net(imgs,NR_ACTION);

  TrainingData datum;
  RandomTrajectorySampler sampler(imgs[0].segment<3>(0).prod(),net.nrOutput());
  datum.randomSample(5,5,10,10,sampler);
  net.debugForeBackprop(datum);
  net.debugWeightprop(datum,true,true);
#undef NR_ACTION
}
void testCNNComposite()
{
#define NR_STATE 22
#define NR_ACTION 6
  boost::shared_ptr<CompositeLayerInPlace> feat0;
  {
    vector<Vec4i,Eigen::aligned_allocator<Vec4i> > imgs;
    imgs.push_back(Vec4i(16,16,2,-1));
    imgs.push_back(Vec4i(3,2,16,1));
    imgs.push_back(Vec4i(2,3,32,2));
    imgs.push_back(Vec4i(2,2,32,2));
    NeuralNet net(imgs,NR_ACTION);
    feat0=net.toCompositeLayer();
  }
  boost::shared_ptr<FeatureTransformLayer> feat1(new FEMRSFeatureTransformLayer(NR_STATE,0.05f,3,true));
  boost::shared_ptr<FeatureTransformLayer> feat2(new FEMRotRSFeatureTransformLayer(NR_STATE,0.05f,false,false,false));
  boost::shared_ptr<FeatureTransformLayer> feat3(new ConstFeatureTransformLayer(3));
  boost::shared_ptr<FeatureTransformLayer> feat4(new FEMRotRSFeatureTransformLayer(NR_STATE,0.05f,false,true,false));
  boost::shared_ptr<FeatureTransformLayer> feat5(new NoFeatureTransformLayer(5));
  boost::shared_ptr<FeatureTransformLayer> featAugIP(new FeatureAugmentedInPlace(feat0,feat1,feat2,feat3,feat4,feat5));
  NeuralNet net(2,3,4,featAugIP->nrInput(),NR_ACTION,NULL,NULL,featAugIP);
  net._tree.put<scalar>("augment0",RandEngine::randR01());
  net._tree.put<scalar>("augment1",RandEngine::randR01());
  net._tree.put<scalar>("augment2",RandEngine::randR01());
  net.resetOnline();
  testNetReadWrite(net);

  TrainingData datum;
  RandomTrajectorySampler sampler(featAugIP->nrInput(),NR_ACTION);
  datum.randomSample(5,5,10,10,sampler);
  net.debugForeBackprop(datum);
#undef NR_ACTION
#undef NR_STATE
}
int main(int argc,char** argv)
{
  //testCNN();
  testCNNComposite();
  return 0;
}
