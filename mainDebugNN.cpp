#include "mainUtils.h"
#include <SoftBodyController/DMPLayer.h>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  /*TrainingData dat;
  RandomTrajectorySampler rsampler(3,5);
  dat.randomSample(10,10,20,20,rsampler);
  NeuralNet nn(10,10,10,3,5);
  nn.debugForeBackprop(dat);*/

  TrainingData dat;
  RandomTrajectorySampler rsampler(1,5);
  dat.randomSample(10,10,20,20,rsampler);

  boost::property_tree::ptree pt;
  pt.put<bool>("cosOnly",true);
  DMPNeuralNet nn(5,5,&pt);
  nn.debugForeBackprop(dat);
  return 0;
}
