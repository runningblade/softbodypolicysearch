find . -name CIOResult -exec rm -rf {} \;
find . -name "*Synthesis" -exec rm -rf {} \;
find . -name "*Tracking" -exec rm -rf {} \;
find . -name "*Render" -exec rm -rf {} \;
find . -name "CIOResult.dat" -type f -delete
find . -name "offlineOut.dat" -type f -delete
find . -name "*~" -type f -delete
