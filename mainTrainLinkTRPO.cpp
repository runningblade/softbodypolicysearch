#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("linkTRPO")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  parseProps(argc,argv,tree);

  LinkEnvironment env;
  MPIInterface::createMPI(argc,argv);
  TrainingData::mpiSlaveRun(env);
  env.reset(tree);
  env.Environment::write(NAME+".dat");

  TRPO learner(env);
  parseProps(argc,argv,learner._tree);
  //learner.debugSurrogateGradient();
  //learner.debugKLDistance();
  learner.learn();

  TrainingData dat;
  dat.randomSample(5,5,1000,1000,env,NULL,NAME+"Learned");
  TrainingData::mpiExit();
  return 0;
}
