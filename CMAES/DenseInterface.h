#include <Dynamics/LMInterface.h>

#ifdef SUITESPARSE_SUPPORT
#include "cholmod.h"
PRJ_BEGIN
class CholmodWrapper : public LMInterface::LMDenseInterface
{
public:
  typedef LMInterface::SMat SMat;
  CholmodWrapper();
  virtual ~CholmodWrapper();
  bool recompute(SMat& smat,scalarD shift,bool sameA) override;
  Matd solve(const Matd& b) override;
  void setSupernodal(bool super);
  void setSameStruct(bool same);
  void setUseEigen(bool useEigen);
private:
  void copyData(const SMat& smat);
  bool tryFactorize(SMat& smat,scalarD shift);
  void clear();
  Eigen::SimplicialCholesky<SMat> _eigenSol;
  //cholmod data
  cholmod_factor* _L;
  cholmod_sparse* _A;
  cholmod_common _c;
  //Our data
  Matd _ret;
  bool _built;
  bool _same;
  bool _useEigen;
  bool _supernodal;
};
PRJ_END
#endif
