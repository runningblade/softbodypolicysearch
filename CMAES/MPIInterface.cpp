#include "MPIInterface.h"
#include <boost/lexical_cast.hpp>
#include <CommonFile/GridBasic.h>
#include <Eigen/Sparse>

USE_PRJ_NAMESPACE

#ifdef MPI_SUPPORT
#include "mpi.h"
boost::shared_ptr<MPIInterface> _mpi;
//MPIInterface
#define MPI_SAFE_CALL(CALL) if((CALL) != MPI_SUCCESS){ \
INFOV("Error Calling " STRINGIFY_OMP(CALL) " at line: %d, file: %s",__LINE__,__FILE__) \
if(_initCalled)MPI_Abort(MPI_COMM_WORLD,0); \
exit(EXIT_FAILURE);}
void MPIInterface::createMPI(int argc,char** argv)
{
  _mpi.reset(new MPIInterface(argc,argv));
}
MPIInterface::~MPIInterface()
{
  //finalize
  MPI_SAFE_CALL(MPI_Finalize())
}
MPIInterface* MPIInterface::getInterface()
{
  return _mpi.get();
}
//get information
int MPIInterface::nrProc() const
{
  return _nrRank;
}
int MPIInterface::currProc() const
{
  return _currRank;
}
void MPIInterface::redirectIO(const string& filename,int pid)
{
  MPIInterface* mpi=getInterface();
  if(!mpi || (pid >= 0 && mpi->currProc() != pid))
    return;
  string currId=boost::lexical_cast<string>(mpi->currProc());
  mpi->_out.reset(new ofstream(filename+"_proc"+currId+".txt"));
  std::cout.rdbuf(mpi->_out->rdbuf());
}
bool MPIInterface::isIORedirected()
{
  MPIInterface* mpi=getInterface();
  return mpi->_out != NULL;
}
//do static schedule
Vec2i MPIInterface::fromTo(sizeType nrJob) const
{
  sizeType nrJobPerProc=(nrJob+nrProc()-1)/nrProc();
  sizeType from=currProc()*nrJobPerProc;
  sizeType to=from+nrJobPerProc;
  return Vec2i(from,min(to,nrJob));
}
//do work: broadcast
void MPIInterface::broadcastObject(Serializable& obj,int sender)
{
  int nr=-1;
  vector<char> buf;
  if(currProc() == sender) {
    boost::interprocess::basic_ovectorstream<vector<char> > os(ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    obj.write(os,dat.get());
    os.flush();
    buf=os.vector();
    nr=(int)buf.size();
  }
  //read data size
  MPI_SAFE_CALL(MPI_Bcast(&nr,1,MPI_INT,sender,MPI_COMM_WORLD))
  ASSERT_MSGV(nr > 0,"Received non positive nr=%d",nr)
  //read data
  if(currProc() != sender)
    buf.resize(nr);
  MPI_SAFE_CALL(MPI_Bcast(&buf[0],buf.size(),MPI_CHAR,sender,MPI_COMM_WORLD))
  if(currProc() != sender) {
    boost::interprocess::basic_ivectorstream<vector<char> > is(buf,ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    obj.read(is,dat.get());
  }
}
template <typename T>
void MPIInterface::broadcastTpl(T& data,int sender)
{
  int nr=-1;
  vector<char> buf;
  if(currProc() == sender) {
    boost::interprocess::basic_ovectorstream<vector<char> > os(ios::binary);
    writeBinaryData(data,os);
    os.flush();
    buf=os.vector();
    nr=(int)buf.size();
  }
  //read data size
  MPI_SAFE_CALL(MPI_Bcast(&nr,1,MPI_INT,sender,MPI_COMM_WORLD))
  ASSERT_MSGV(nr > 0,"Received non positive nr=%d",nr)
  //read data
  if(currProc() != sender)
    buf.resize(nr);
  MPI_SAFE_CALL(MPI_Bcast(&buf[0],buf.size(),MPI_CHAR,sender,MPI_COMM_WORLD))
  if(currProc() != sender) {
    boost::interprocess::basic_ivectorstream<vector<char> > is(buf,ios::binary);
    readBinaryData(data,is);
  }
}
void MPIInterface::broadcast(sizeType& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(scalarF& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(scalarD& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(string& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Mati& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Matf& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Matd& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Matc& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Coli& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Colf& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Cold& data,int sender)
{
  broadcastTpl(data,sender);
}
void MPIInterface::broadcast(Colc& data,int sender)
{
  broadcastTpl(data,sender);
}
//do work: scatter
template <typename MAT,typename VEC>
void MPIInterface::scatterTpl(const MAT& send,VEC& receive,int sender)
{
  ASSERT_MSG(send.rows() == receive.size(),"You must have: send.rows() == receive.size() for scatter")
  MPI_SAFE_CALL(MPI_Scatter((void*)send.data(),send.rows()*sizeof(typename MAT::Scalar),MPI_CHAR,
                            receive.data(),send.rows()*sizeof(typename MAT::Scalar),MPI_CHAR,sender,MPI_COMM_WORLD))
}
void MPIInterface::scatter(const Mati& send,Coli& receive,int sender)
{
  scatterTpl(send,receive,sender);
}
void MPIInterface::scatter(const Matf& send,Colf& receive,int sender)
{
  scatterTpl(send,receive,sender);
}
void MPIInterface::scatter(const Matd& send,Cold& receive,int sender)
{
  scatterTpl(send,receive,sender);
}
void MPIInterface::scatter(const Matc& send,Colc& receive,int sender)
{
  scatterTpl(send,receive,sender);
}
template <typename VEC>
void MPIInterface::scatterSTpl(const VEC& send,typename VEC::Scalar& receive,int sender)
{
  MPI_SAFE_CALL(MPI_Scatter((void*)send.data(),sizeof(typename VEC::Scalar),MPI_CHAR,
                            &receive,sizeof(typename VEC::Scalar),MPI_CHAR,sender,MPI_COMM_WORLD))
}
void MPIInterface::scatter(const Coli& send,sizeType& receive,int sender)
{
  scatterSTpl(send,receive,sender);
}
void MPIInterface::scatter(const Colf& send,scalarF& receive,int sender)
{
  scatterSTpl(send,receive,sender);
}
void MPIInterface::scatter(const Cold& send,scalarD& receive,int sender)
{
  scatterSTpl(send,receive,sender);
}
void MPIInterface::scatter(const Colc& send,char& receive,int sender)
{
  scatterSTpl(send,receive,sender);
}
//do work: gather
template <typename MAT,typename VEC>
void MPIInterface::gatherTpl(MAT& receive,const VEC& send,int receiver)
{
  ASSERT_MSG(send.size() == receive.rows(),"You must have: receive.rows() == send.size() for gatter")
  MPI_SAFE_CALL(MPI_Gather((void*)send.data(),send.size()*sizeof(typename MAT::Scalar),MPI_CHAR,
                           receive.data(),send.size()*sizeof(typename MAT::Scalar),MPI_CHAR,receiver,MPI_COMM_WORLD))
}
void MPIInterface::gather(Mati& receive,const Coli& send,int receiver)
{
  gatherTpl(receive,send,receiver);
}
void MPIInterface::gather(Matf& receive,const Colf& send,int receiver)
{
  gatherTpl(receive,send,receiver);
}
void MPIInterface::gather(Matd& receive,const Cold& send,int receiver)
{
  gatherTpl(receive,send,receiver);
}
void MPIInterface::gather(Matc& receive,const Colc& send,int receiver)
{
  gatherTpl(receive,send,receiver);
}
template <typename VEC>
void MPIInterface::gatherSTpl(VEC& receive,const typename VEC::Scalar& send,int receiver)
{
  MPI_SAFE_CALL(MPI_Gather((void*)&send,sizeof(typename VEC::Scalar),MPI_CHAR,
                           receive.data(),sizeof(typename VEC::Scalar),MPI_CHAR,receiver,MPI_COMM_WORLD))
}
void MPIInterface::gather(Coli& receive,const sizeType& send,int receiver)
{
  gatherSTpl(receive,send,receiver);
}
void MPIInterface::gather(Colf& receive,const scalarF& send,int receiver)
{
  gatherSTpl(receive,send,receiver);
}
void MPIInterface::gather(Cold& receive,const scalarD& send,int receiver)
{
  gatherSTpl(receive,send,receiver);
}
void MPIInterface::gather(Colc& receive,const char& send,int receiver)
{
  gatherSTpl(receive,send,receiver);
}
//do work: scatterDynamic
template <typename MAT,typename ALLOC>
void MPIInterface::scatterDynamicTpl(const vector<MAT,ALLOC>& send,MAT& receive,int sender)
{
  typedef typename Eigen::Matrix<typename MAT::Scalar,-1,1> VEC;
  Vec2i maxSize(0,0),sizeI;
  Mati sizes(2,nrProc());
  for(sizeType i=0; i<nrProc(); i++) {
    sizes.col(i)=Vec2i(send[i].rows(),send[i].cols());
    maxSize=maxSize.cwiseMax(sizes.col(i));
  }
  broadcastTpl(maxSize,sender);
  scatterTpl(sizes,sizeI,sender);

  MAT assembledSend(maxSize(0,0)*maxSize(1,0),nrProc());
  VEC assembledReceive(maxSize(0,0)*maxSize(1,0));
  if(currProc() == sender)
    for(sizeType i=0; i<nrProc(); i++)
      assembledSend.block(0,i,send[i].size(),1)=
        Eigen::Map<const VEC>(send[i].data(),send[i].size());
  scatter(assembledSend,assembledReceive,sender);

  receive.resize(sizeI[0],sizeI[1]);
  Eigen::Map<VEC>(receive.data(),receive.size())=
    assembledReceive.block(0,0,sizeI[0]*sizeI[1],1);
}
void MPIInterface::scatterDynamic(const vector<Mati,Eigen::aligned_allocator<Mati> >& send,Mati& receive,int sender)
{
  MPIInterface::scatterDynamicTpl(send,receive,sender);
}
void MPIInterface::scatterDynamic(const vector<Matf,Eigen::aligned_allocator<Matf> >& send,Matf& receive,int sender)
{
  MPIInterface::scatterDynamicTpl(send,receive,sender);
}
void MPIInterface::scatterDynamic(const vector<Matd,Eigen::aligned_allocator<Matd> >& send,Matd& receive,int sender)
{
  MPIInterface::scatterDynamicTpl(send,receive,sender);
}
void MPIInterface::scatterDynamic(const vector<Matc,Eigen::aligned_allocator<Matc> >& send,Matc& receive,int sender)
{
  MPIInterface::scatterDynamicTpl(send,receive,sender);
}
//do work: gatherDynamic
template <typename MAT,typename ALLOC>
void MPIInterface::gatherDynamicTpl(vector<MAT,ALLOC>& receive,const MAT& send,int receiver)
{
  typedef typename Eigen::Matrix<typename MAT::Scalar,-1,-1> MATF;
  typedef typename Eigen::Matrix<typename MAT::Scalar,-1,1> VEC;
  Mati sizes(2,nrProc());
  gather(sizes,Vec2i(send.rows(),send.cols()),receiver);

  sizeType maxSize=0;
  if(currProc() == receiver)
    for(sizeType i=0; i<nrProc(); i++)
      maxSize=max<sizeType>(sizes(0,i)*sizes(1,i),maxSize);
  broadcast(maxSize,receiver);

  MATF assembledReceive(maxSize,nrProc());
  VEC assembledSend=VEC::Zero(maxSize);
  assembledSend.block(0,0,send.size(),1)=
    Eigen::Map<const VEC>(send.data(),send.size());
  gather(assembledReceive,assembledSend,receiver);

  if(currProc() == receiver) {
    receive.resize(nrProc());
    for(sizeType i=0; i<nrProc(); i++) {
      receive[i].setZero(sizes(0,i),sizes(1,i));
      Eigen::Map<VEC>(receive[i].data(),receive[i].size())=
        assembledReceive.block(0,i,sizes(0,i)*sizes(1,i),1);
    }
  }
}
void MPIInterface::gatherDynamic(vector<Mati,Eigen::aligned_allocator<Mati> >& receive,const Mati& send,int receiver)
{
  gatherDynamicTpl(receive,send,receiver);
}
void MPIInterface::gatherDynamic(vector<Matf,Eigen::aligned_allocator<Matf> >& receive,const Matf& send,int receiver)
{
  gatherDynamicTpl(receive,send,receiver);
}
void MPIInterface::gatherDynamic(vector<Matd,Eigen::aligned_allocator<Matd> >& receive,const Matd& send,int receiver)
{
  gatherDynamicTpl(receive,send,receiver);
}
void MPIInterface::gatherDynamic(vector<Matc,Eigen::aligned_allocator<Matc> >& receive,const Matc& send,int receiver)
{
  gatherDynamicTpl(receive,send,receiver);
}
//shutdown control
void MPIInterface::abort()
{
  MPI_SAFE_CALL(MPI_Abort(MPI_COMM_WORLD,0))
}
bool MPIInterface::signalShutdown(int sender,bool shutdown)
{
  char shutdownC=shutdown ? 1 : 0;
  MPI_SAFE_CALL(MPI_Bcast(&shutdownC,1,MPI_CHAR,sender,MPI_COMM_WORLD))
  return shutdownC == 1;
}
void MPIInterface::setSingleHost()
{
  int nrT=max<int>(1,omp_get_num_procs()/nrProc());
  INFOV("On single host, each of %d procs can use %d threads",nrProc(),nrT)
  omp_set_num_threads(nrT);
}
void MPIInterface::printInfo() const
{
  INFOV("Proc %d of %d, running on host: %s, with %d available threads",
        _currRank,_nrRank,_name.c_str(),omp_get_num_procs())
}
//helper
MPIInterface::MPIInterface(int argc,char** argv)
{
  //initialize
  _initCalled=false;
  MPI_SAFE_CALL(MPI_Init(&argc,&argv));
  _initCalled=true;
  MPI_SAFE_CALL(MPI_Comm_size(MPI_COMM_WORLD,&_nrRank))
  MPI_SAFE_CALL(MPI_Comm_rank(MPI_COMM_WORLD,&_currRank))
  //get name
  int len;
  char hostname[MPI_MAX_PROCESSOR_NAME];
  MPI_SAFE_CALL(MPI_Get_processor_name(hostname,&len))
  _name.resize(len);
  copy(hostname,hostname+len,_name.begin());
  //print
  printInfo();
}
#else
void MPIInterface::createMPI(int argc,char** argv) {}
MPIInterface::~MPIInterface() {}
MPIInterface* MPIInterface::getInterface()
{
  return NULL;
}
//get information
int MPIInterface::nrProc() const
{
  return -1;
}
int MPIInterface::currProc() const
{
  return -1;
}
void MPIInterface::redirectIO(const string& filename,int pid) {}
bool MPIInterface::isIORedirected()
{
  return false;
}
//do static schedule
Vec2i MPIInterface::fromTo(sizeType nrJob) const
{
  return Vec2i::Constant(-1);
}
//do work: broadcast
void MPIInterface::broadcastObject(Serializable& obj,int sender) {}
template <typename T>
void MPIInterface::broadcastTpl(T& data,int sender) {}
void MPIInterface::broadcast(sizeType& data,int sender) {}
void MPIInterface::broadcast(scalarF& data,int sender) {}
void MPIInterface::broadcast(scalarD& data,int sender) {}
void MPIInterface::broadcast(string& data,int sender) {}
void MPIInterface::broadcast(Mati& data,int sender) {}
void MPIInterface::broadcast(Matf& data,int sender) {}
void MPIInterface::broadcast(Matd& data,int sender) {}
void MPIInterface::broadcast(Matc& data,int sender) {}
void MPIInterface::broadcast(Coli& data,int sender) {}
void MPIInterface::broadcast(Colf& data,int sender) {}
void MPIInterface::broadcast(Cold& data,int sender) {}
void MPIInterface::broadcast(Colc& data,int sender) {}
//do work: scatter
template <typename MAT,typename VEC>
void MPIInterface::scatterTpl(const MAT& send,VEC& receive,int sender) {}
void MPIInterface::scatter(const Mati& send,Coli& receive,int sender) {}
void MPIInterface::scatter(const Matf& send,Colf& receive,int sender) {}
void MPIInterface::scatter(const Matd& send,Cold& receive,int sender) {}
void MPIInterface::scatter(const Matc& send,Colc& receive,int sender) {}
template <typename VEC>
void MPIInterface::scatterSTpl(const VEC& send,typename VEC::Scalar& receive,int sender) {}
void MPIInterface::scatter(const Coli& send,sizeType& receive,int sender) {}
void MPIInterface::scatter(const Colf& send,scalarF& receive,int sender) {}
void MPIInterface::scatter(const Cold& send,scalarD& receive,int sender) {}
void MPIInterface::scatter(const Colc& send,char& receive,int sender) {}
//do work: gather
template <typename MAT,typename VEC>
void MPIInterface::gatherTpl(MAT& receive,const VEC& send,int receiver) {}
void MPIInterface::gather(Mati& receive,const Coli& send,int receiver) {}
void MPIInterface::gather(Matf& receive,const Colf& send,int receiver) {}
void MPIInterface::gather(Matd& receive,const Cold& send,int receiver) {}
void MPIInterface::gather(Matc& receive,const Colc& send,int receiver) {}
template <typename VEC>
void MPIInterface::gatherSTpl(VEC& receive,const typename VEC::Scalar& send,int receiver) {}
void MPIInterface::gather(Coli& receive,const sizeType& send,int receiver) {}
void MPIInterface::gather(Colf& receive,const scalarF& send,int receiver) {}
void MPIInterface::gather(Cold& receive,const scalarD& send,int receiver) {}
void MPIInterface::gather(Colc& receive,const char& send,int receiver) {}
//do work: scatterDynamic
template <typename MAT,typename ALLOC>
void MPIInterface::scatterDynamicTpl(const vector<MAT,ALLOC>& send,MAT& receive,int sender) {}
void MPIInterface::scatterDynamic(const vector<Mati,Eigen::aligned_allocator<Mati> >& send,Mati& receive,int sender) {}
void MPIInterface::scatterDynamic(const vector<Matf,Eigen::aligned_allocator<Matf> >& send,Matf& receive,int sender) {}
void MPIInterface::scatterDynamic(const vector<Matd,Eigen::aligned_allocator<Matd> >& send,Matd& receive,int sender) {}
void MPIInterface::scatterDynamic(const vector<Matc,Eigen::aligned_allocator<Matc> >& send,Matc& receive,int sender) {}
//do work: gatherDynamic
template <typename MAT,typename ALLOC>
void MPIInterface::gatherDynamicTpl(vector<MAT,ALLOC>& receive,const MAT& send,int receiver) {}
void MPIInterface::gatherDynamic(vector<Mati,Eigen::aligned_allocator<Mati> >& receive,const Mati& send,int receiver) {}
void MPIInterface::gatherDynamic(vector<Matf,Eigen::aligned_allocator<Matf> >& receive,const Matf& send,int receiver) {}
void MPIInterface::gatherDynamic(vector<Matd,Eigen::aligned_allocator<Matd> >& receive,const Matd& send,int receiver) {}
void MPIInterface::gatherDynamic(vector<Matc,Eigen::aligned_allocator<Matc> >& receive,const Matc& send,int receiver) {}
//shutdown control
void MPIInterface::abort() {}
bool MPIInterface::signalShutdown(int sender,bool shutdown)
{
  return true;
}
void MPIInterface::setSingleHost() {}
void MPIInterface::printInfo() const {}
//helper
MPIInterface::MPIInterface(int argc,char** argv) {}
#endif

//test
template <typename MAT,typename VEC>
void MPIInterface::testMPIScatterGatherTpl()
{
  //send
  MAT send1=MAT::Zero(4,nrProc());
  if(currProc() == 0) {
    for(sizeType r=0; r<send1.rows(); r++)
      for(sizeType c=0; c<send1.cols(); c++)
        send1(r,c)=(r+1)*(c+1);
  }
  VEC receive1=VEC::Zero(4);
  scatter(send1,receive1,0);

  //modify and receive
  VEC send2=(currProc() == 0 ? receive1 : VEC(receive1*2))+VEC::Ones(4);
  MAT receive2=MAT::Zero(4,nrProc());
  gather(receive2,send2,0);

  //print
  if(currProc() == 0) {
    cout << "----------------------SEND Static Size" << endl;
    cout << send1 << endl;
    cout << "----------------------RECEIVE Static Size" << endl;
    cout << receive2 << endl;
  }
}
template <typename VEC>
void MPIInterface::testMPIScatterGatherSTpl()
{
  //send
  VEC send1=VEC::Zero(nrProc());
  if(currProc() == 0) {
    for(sizeType r=0; r<send1.rows(); r++)
      for(sizeType c=0; c<send1.cols(); c++)
        send1(r,c)=(r+1)*(c+1);
  }
  typename VEC::Scalar receive1=0;
  scatter(send1,receive1,0);

  //modify and receive
  typename VEC::Scalar send2=(currProc() == 0 ? receive1 : receive1*2)+1;
  VEC receive2=VEC::Zero(nrProc());
  gather(receive2,send2,0);

  //print
  if(currProc() == 0) {
    cout << "----------------------SEND Static Size" << endl;
    cout << send1 << endl;
    cout << "----------------------RECEIVE Static Size" << endl;
    cout << receive2 << endl;
  }
}
template <typename MAT>
void MPIInterface::testMPIScatterGatherDynamicTpl()
{
  //send
  vector<MAT,Eigen::aligned_allocator<MAT> > send1(nrProc());
  if(currProc() == 0)
    for(sizeType i=0; i<nrProc(); i++)
      send1[i].setRandom(RandEngine::randI(1,5),RandEngine::randI(1,5));
  MAT receive1;
  scatterDynamic(send1,receive1,0);

  //modify and receive
  MAT send2=(currProc() == 0 ? receive1 : MAT(receive1*2))+MAT::Ones(receive1.rows(),receive1.cols());
  vector<MAT,Eigen::aligned_allocator<MAT> > receive2;
  gatherDynamic(receive2,send2,0);

  //print
  if(currProc() == 0)
    for(sizeType i=0; i<nrProc(); i++) {
      cout << "----------------------SEND Dynamic Size Proc=" << i << endl;
      cout << send1[i] << endl;
      cout << "----------------------RECEIVE Dynamic Size Proc=" << i << endl;
      cout << receive2[i] << endl;
    }
}
void MPIInterface::testMPIGatherDynamicObject()
{
  vector<boost::shared_ptr<ScalarField> > send,receive;
  if(currProc() != 0) {
    send.resize(RandEngine::randI(1,5));
    cout << "----------------------" << endl;
    for(sizeType i=0; i<(sizeType)send.size(); i++) {
      send[i].reset(new ScalarField);
      sizeType nrG=RandEngine::randI(5,10);
      scalarD initVal=RandEngine::randR01();
      send[i]->reset(Vec3i::Constant(nrG),BBox<scalar>(Vec3::Zero(),Vec3::Ones()),initVal);
      cout << "Proc" << currProc() << " Sent: ";
      cout << send[i]->getNrPoint().transpose() << " " << send[i]->sum() << endl;
    }
  }
  gatherDynamicVectorTpl(receive,send,0);
  if(currProc() == 0) {
    cout << "----------------------RECEIVE Object Info" << endl;
    for(sizeType i=0; i<(sizeType)receive.size(); i++)
      cout << receive[i]->getNrPoint().transpose() << " " << receive[i]->sum() << endl;
  }
}
void MPIInterface::testMPIGatherDynamicVector()
{
  vector<Eigen::Triplet<scalarD,sizeType> > trips,tripsGather;
  trips.resize(currProc()+1);
  cout << "----------------------SEND Vector Proc=" << currProc() << endl;
  for(sizeType i=0; i<(sizeType)trips.size(); i++) {
    trips[i]=Eigen::Triplet<scalarD,sizeType>(currProc(),i,i*i);
    cout << "Triplet " << i << ": (" << trips[i].row() << "," << trips[i].col() << "," << trips[i].value() << ")" << endl;
  }
  gatherDynamicVectorTpl(tripsGather,trips,0);
  if(currProc() == 0) {
    cout << "----------------------RECEIVE Vector" << endl;
    for(sizeType i=0; i<(sizeType)tripsGather.size(); i++)
      cout << "Triplet " << i << ": (" << tripsGather[i].row() << "," << tripsGather[i].col() << "," << tripsGather[i].value() << ")" << endl;
  }
}
void MPIInterface::testMPIScatterGather()
{
  testMPIScatterGatherSTpl<Colc>();
  testMPIScatterGatherSTpl<Cold>();
  testMPIScatterGatherSTpl<Colf>();
  testMPIScatterGatherSTpl<Coli>();

  testMPIScatterGatherTpl<Matc,Colc>();
  testMPIScatterGatherTpl<Matd,Cold>();
  testMPIScatterGatherTpl<Matf,Colf>();
  testMPIScatterGatherTpl<Mati,Coli>();
}
void MPIInterface::testMPIScatterGatherDynamic()
{
  testMPIScatterGatherDynamicTpl<Matd>();
  testMPIScatterGatherDynamicTpl<Matf>();
  testMPIScatterGatherDynamicTpl<Mati>();
  testMPIScatterGatherDynamicTpl<Matc>();
}
void MPIInterface::testMPIBroadcast()
{
  ScalarField obj;
  if(currProc() == 0)
    obj.reset(Vec3i(10,10,0),BBox<scalar>(Vec3::Zero(),Vec3(1,1,0)),10);
  broadcastObject(obj,0);
  if(currProc() != 0) {
    cout << "----------------------RECEIVED FIELD AT PROC: " << currProc() << endl;
    for(sizeType x=0; x<obj.getNrPoint()[0]; x++) {
      for(sizeType y=0; y<obj.getNrPoint()[1]; y++)
        cout << obj.get(Vec3i(x,y,0)) << " ";
      cout << endl;
    }
  }
}
