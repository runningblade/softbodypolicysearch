#include "MLUtil.h"
#include <CommonFile/IO.h>
#include <Dynamics/OPTInterface.h>
#include <boost/math/distributions/normal.hpp>

PRJ_BEGIN

void DEBUG_COMPARE(const string& name,scalarD VAL,scalarD ERR,scalarD THRES)
{
  if(ERR/max(abs(VAL),THRES) > THRES) {
    WARNINGV("%s Val: %f Err: %f",name.c_str(),VAL,ERR)
  } else {
    INFOV("%s Val: %f Err: %f",name.c_str(),VAL,ERR)
  }
}
scalarD logDet(const Eigen::LLT<Matd>& chol)
{
  scalarD ld=0;
  auto& U=chol.matrixL();
  for(unsigned i=0; i<U.rows(); ++i)
    ld+=log(U(i,i));
  return ld*2;
}
scalarD dotMat(const Matd& A,const Matd& B)
{
  return (A.transpose()*B).trace();
}
Cold oneVec(scalarD val)
{
  return Cold::Ones(1)*val;
}
Matd oneMat(scalarD val)
{
  return Matd::Ones(1,1)*val;
}

//multivariate normal distribution
Cold proposeNormal(const Cold& mean,const Matd& cov)
{
  Eigen::LLT<Matd> invCov=cov.llt();
  Cold x=Cold::Zero(mean.size());
  for(unsigned i=0; i<mean.size(); i++)
    x[i]=RandEngine::normal();
  return mean+invCov.matrixL()*x;
}
scalarD logPossNormal(const Cold& mean,const Eigen::LLT<Matd>& invCov,const Cold& val)
{
  static scalarD LOGTWOPI=log(2*M_PI);
  return -0.5f*(logDet(invCov)+(val-mean).dot(invCov.solve(val-mean))+(scalarD)mean.size()*LOGTWOPI);
}
scalarD logPossNormal(const Cold& mean,const Matd& cov,const Cold& val)
{
  Eigen::LLT<Matd> invCov=cov.llt();
  return logPossNormal(mean,invCov,val);
}
Cold DLogPossDMeanNormal(const Cold& mean,const Matd& cov,const Cold& val)
{
  Matd invCov=cov.inverse();
  return invCov*(val-mean);
}
Matd DLogPossDCovNormal(const Cold& mean,const Matd& cov,const Cold& val)
{
  Matd invCov=cov.inverse();
  Cold covDiff=invCov*(val-mean);
  return 0.5f*(covDiff*covDiff.transpose()-invCov);
}
//singlevariate normal distribution
scalarD proposeNormal(scalarD mean,scalarD cov)
{
  return proposeNormal(oneVec(mean),oneMat(cov))[0];
}
scalarD logPossNormal(scalarD mean,scalarD cov,scalarD val)
{
  return logPossNormal(oneVec(mean),oneMat(cov),oneVec(val));
}
scalarD DLogPossDMeanNormal(scalarD mean,scalarD cov,scalarD val)
{
  return DLogPossDMeanNormal(oneVec(mean),oneMat(cov),oneVec(val))[0];
}
scalarD DLogPossDCovNormal(scalarD mean,scalarD cov,scalarD val)
{
  return DLogPossDCovNormal(oneVec(mean),oneMat(cov),oneVec(val))(0,0);
}
scalarD confidenceCovNormal(scalarD range,scalarD confidence)
{
  range=-abs(range);
  confidence=(1-confidence)/2;
  scalarD covA=1,covB=1;
  while(boost::math::cdf(boost::math::normal_distribution<>(0,covA),range) > confidence)
    covA*=0.5f;
  while(boost::math::cdf(boost::math::normal_distribution<>(0,covB),range) < confidence)
    covB*=2.0f;
  scalarD mid=(covA+covB)/2;
  while(covB-covA > 1E-4f) {
    mid=(covA+covB)/2;
    if(boost::math::cdf(boost::math::normal_distribution<>(0,mid),range) > confidence)
      covB=mid;
    else covA=mid;
  }
  return mid*mid;
}
scalarD integralNormal(const Cold& mean,const Eigen::LLT<Matd>& cov,const Cold* mean2,const Eigen::LLT<Matd>* cov2,
                       sizeType coord,const Cold& minV,const Cold& maxV,const scalarD& delta)
{
  if((maxV-minV).maxCoeff() > delta) {
    scalarD ret=0;
    Cold minVSmaller=minV;
    Cold maxVSmaller=maxV;
    maxVSmaller[coord]=(minV[coord]+maxV[coord])/2;
    ret+=integralNormal(mean,cov,mean2,cov2,(coord+1)%mean.size(),minVSmaller,maxVSmaller,delta);
    minVSmaller[coord]=(minV[coord]+maxV[coord])/2;
    maxVSmaller[coord]=maxV[coord];
    ret+=integralNormal(mean,cov,mean2,cov2,(coord+1)%mean.size(),minVSmaller,maxVSmaller,delta);
    return ret;
  } else if(!mean2) {
    return (maxV-minV).prod()*exp(logPossNormal(mean,cov,(maxV+minV)/2));
  } else {
    scalarD logP=logPossNormal(mean,cov,(maxV+minV)/2);
    scalarD logQ=logPossNormal(*mean2,*cov2,(maxV+minV)/2);
    return (maxV-minV).prod()*exp(logP)*(logP-logQ);
  }
}
scalarD integralNormal(const Cold& mean,const Matd& cov,const Cold* mean2,const Matd* cov2,
                       sizeType coord,const Cold& minV,const Cold& maxV,scalarD delta)
{
  Eigen::LLT<Matd> invCov1=cov.llt(),invCov2;
  if(cov2)
    invCov2=cov2->llt();
  return integralNormal(mean,invCov1,mean2,&invCov2,coord,minV,maxV,delta);
}
//debug
void debugDLogPossNormal(sizeType nr)
{
  Cold mean=Cold::Random(nr);
  Matd cov=Matd::Random(nr,nr);
  cov=cov.transpose()*cov;
  debugDLogPossNormal(mean,cov);
}
void debugIntegralNormal(sizeType nr)
{
  Cold mean=Cold::Random(nr);
  Matd cov=Matd::Random(nr,nr);
  cov=cov.transpose()*cov;
  debugIntegralNormal(mean,cov);
}
void debugDLogPossNormal(const Cold& mean,const Matd& cov)
{
#define NR_TEST 10
#define DELTA 1E-7f
  for(sizeType i=0; i<NR_TEST; i++) {
    Cold deltaMean=Cold::Random(mean.size());
    Matd deltaCov=Matd::Random(cov.rows(),cov.cols());
    deltaCov=(deltaCov+deltaCov.transpose()).eval()/2;

    Cold val=proposeNormal(mean,cov);
    scalarD logPoss=logPossNormal(mean,cov,val);
    scalarD DMeanN=(logPossNormal(mean+deltaMean*DELTA,cov,val)-logPoss)/DELTA;
    scalarD DCovN=(logPossNormal(mean,cov+deltaCov*DELTA,val)-logPoss)/DELTA;
    DEBUG_COMPARE("DMean",DLogPossDMeanNormal(mean,cov,val).dot(deltaMean),DLogPossDMeanNormal(mean,cov,val).dot(deltaMean)-DMeanN,1E-5f);
    DEBUG_COMPARE("DCov",dotMat(DLogPossDCovNormal(mean,cov,val),deltaCov),dotMat(DLogPossDCovNormal(mean,cov,val),deltaCov)-DCovN,1E-5f);
  }
#undef DELTA
#undef NR_TEST
}
void debugIntegralNormal(const Cold& mean,const Matd& cov)
{
  boost::math::normal_distribution<double> dist(0,cov.diagonal().maxCoeff());
  Cold range=Cold::Constant(mean.size(),-boost::math::quantile(dist,1E-5f));
  scalarD ret=integralNormal(mean,cov,NULL,NULL,0,mean-range,mean+range,5E-3f);
  INFOV("Integral of Normal PDF: %f",ret)
}
void debugConfidenceCovNormal()
{
  scalarD val=1.5f,poss=0.95f;
  scalarD cov=confidenceCovNormal(val,poss);
  scalarD possTest=integralNormal(oneVec(0),oneMat(cov),NULL,NULL,0,oneVec(-val),oneVec(val),1E-5f);
  DEBUG_COMPARE("ConfidenceCov Target Possibility",poss,poss-possTest,1E-5f);
}
//multivariate Kullback-Leibler distance: KL(N(mean1,cov1),N(mean2,cov2))
scalarD KLDistanceNormal(const Cold& mean1,const Matd& cov1,const Cold& mean2,const Matd& cov2)
{
  Eigen::LLT<Matd> invCov1=cov1.llt(),invCov2=cov2.llt();
  return 0.5f*(invCov2.solve(cov1).trace()+
               (mean2-mean1).dot(invCov2.solve(mean2-mean1))-(scalarD)mean1.size()+
               logDet(invCov2)-logDet(invCov1));
}
scalarD KLDistanceBFNormal(const Cold& mean1,const Matd& cov1,const Cold& mean2,const Matd& cov2)
{
  boost::math::normal_distribution<double> dist(0,cov1.diagonal().maxCoeff()+cov2.diagonal().maxCoeff());
  Cold range=Cold::Constant(mean1.size(),-boost::math::quantile(dist,1E-5f));
  return integralNormal(mean1,cov1,&mean2,&cov2,0,mean1.cwiseMin(mean2)-range,mean1.cwiseMax(mean2)+range,1E-3f);
}
void diffKLDistanceNormal(Cold& diffMean1,Matd& diffCov1,const Cold& mean1,const Matd& cov1,
                          Cold& diffMean2,Matd& diffCov2,const Cold& mean2,const Matd& cov2)
{
  Matd invCov1=cov1.inverse(),invCov2=cov2.inverse();
  diffMean1= invCov2*(mean1-mean2);
  diffMean2=-diffMean1;
  diffCov1=0.5f*(invCov2-invCov1);
  diffCov2=0.5f*(invCov2-invCov2*((cov1+(mean1-mean2)*(mean1-mean2).transpose())*invCov2));
}
//singlevariate Kullback-Leibler distance: KL(N(mean1,cov1),N(mean2,cov2))
scalarD KLDistanceNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2)
{
  return KLDistanceNormal(oneVec(mean1),oneMat(cov1),oneVec(mean2),oneMat(cov2));
}
scalarD KLDistanceBFNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2)
{
  return KLDistanceBFNormal(oneVec(mean1),oneMat(cov1),oneVec(mean2),oneMat(cov2));
}
Vec4d diffKLDistanceNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2)
{
  Cold diffMean1,diffMean2;
  Matd diffCov1,diffCov2;
  diffKLDistanceNormal(diffMean1,diffCov1,oneVec(mean1),oneMat(cov1),
                       diffMean2,diffCov2,oneVec(mean2),oneMat(cov2));
  return Vec4d(diffMean1[0],diffCov1(0,0),diffMean2[0],diffCov2(0,0));
}
Mat4d hessKLDistanceNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2)
{
  Mat4d ret=Mat4d::Zero();
  ret(0,0)=ret(2,2)= 1/cov2;
  ret(0,2)=ret(2,0)=-1/cov2;
  ret(1,1)=1/cov1/cov1/2;

  ret(0,3)=ret(3,0)=-(mean1-mean2)/pow(cov2,2);
  ret(2,3)=ret(3,2)= (mean1-mean2)/pow(cov2,2);
  ret(1,3)=ret(3,1)=-1/pow(cov2,2)/2;

  ret(3,3)=(cov1+pow(mean1-mean2,2))/pow(cov2,3)-1/cov2/cov2/2;
  return ret;
}
//debug
void debugKLDistanceNormal(sizeType nr)
{
#define NR_TEST 10
#define DELTA 1E-7f
  scalarD KL1,KL2,KL3,KLBF;
  for(sizeType i=0; i<NR_TEST; i++) {
    Cold mean1=Cold::Random(nr);
    Matd cov1=Matd::Random(nr,nr);
    cov1=cov1.transpose()*cov1;

    Cold mean2=Cold::Random(nr);
    Matd cov2=Matd::Random(nr,nr);
    cov2=cov2.transpose()*cov2;

    Cold diffMean1,diffMean2;
    Matd diffCov1,diffCov2;
    diffKLDistanceNormal(diffMean1,diffCov1,mean1,cov1,
                         diffMean2,diffCov2,mean2,cov2);
    KL1=KLDistanceNormal(mean1,cov1,mean2,cov2);

    Cold deltaMean1=Cold::Random(nr);
    Cold deltaMean2=Cold::Random(nr);
    KL2=KLDistanceNormal(mean1+deltaMean1*DELTA,cov1,mean2,cov2);
    KL3=KLDistanceNormal(mean1,cov1,mean2+deltaMean2*DELTA,cov2);
    DEBUG_COMPARE("DiffMean1",diffMean1.dot(deltaMean1),(diffMean1.dot(deltaMean1)-(KL2-KL1)/DELTA),1E-5f);
    DEBUG_COMPARE("DiffMean2",diffMean2.dot(deltaMean2),(diffMean2.dot(deltaMean2)-(KL3-KL1)/DELTA),1E-5f);

    Matd deltaCov1=Matd::Random(nr,nr);
    Matd deltaCov2=Matd::Random(nr,nr);
    deltaCov1=(deltaCov1+deltaCov1.transpose()).eval()/2;
    deltaCov2=(deltaCov2+deltaCov2.transpose()).eval()/2;
    KL2=KLDistanceNormal(mean1,cov1+deltaCov1*DELTA,mean2,cov2);
    KL3=KLDistanceNormal(mean1,cov1,mean2,cov2+deltaCov2*DELTA);
    DEBUG_COMPARE("DiffCov1",dotMat(diffCov1,deltaCov1),(dotMat(diffCov1,deltaCov1)-(KL2-KL1)/DELTA),1E-5f);
    DEBUG_COMPARE("DiffCov2",dotMat(diffCov2,deltaCov2),(dotMat(diffCov2,deltaCov2)-(KL3-KL1)/DELTA),1E-5f);
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    scalarD mean1=RandEngine::randR(-1,1);
    scalarD cov1=RandEngine::randR(0.1,0.9f);
    scalarD mean2=RandEngine::randR(-1,1);
    scalarD cov2=RandEngine::randR(0.1,0.9f);
    KL1=KLDistanceNormal(mean1,cov1,mean2,cov2);
    KLBF=KLDistanceBFNormal(mean1,cov1,mean2,cov2);
    DEBUG_COMPARE("KLDistance",KL1,KL1-KLBF,1E-5f);

    Vec4d delta=Vec4d::Random();
    scalarD mean1T=mean1+delta[0]*DELTA;
    scalarD cov1T=cov1+delta[1]*DELTA;
    scalarD mean2T=mean2+delta[2]*DELTA;
    scalarD cov2T=cov2+delta[3]*DELTA;
    KL2=KLDistanceNormal(mean1T,cov1T,mean2T,cov2T);
    Vec4d diffKL1=diffKLDistanceNormal(mean1,cov1,mean2,cov2);
    Vec4d diffKL2=diffKLDistanceNormal(mean1T,cov1T,mean2T,cov2T);
    Mat4d hessKL=hessKLDistanceNormal(mean1,cov1,mean2,cov2);
    DEBUG_COMPARE("DiffKLDistance",diffKL1.dot(delta),(diffKL1.dot(delta)-(KL2-KL1)/DELTA),1E-5f);
    DEBUG_COMPARE("HessKLDistance",(hessKL*delta).norm(),(hessKL*delta-(diffKL2-diffKL1)/DELTA).norm(),1E-5f);
  }
#undef DELTA
#undef NR_TEST
}

//softmax
sizeType proposeSoftMax(const Cold& x)
{
  Cold expX=x.array().exp().matrix();
  expX/=expX.sum();

  scalarD test=0,val=RandEngine::randR01();
  for(sizeType i=0;i<expX.size();i++) {
    test+=expX[i];
    if(val < test)
      return i;
  }
  return expX.size()-1;
}
scalarD logPossSoftMax(const Cold& x,sizeType id)
{
  return x[id]-log(x.array().exp().sum());
}
Cold DLogPossDXSoftMax(const Cold& x,sizeType id)
{
  Cold expX=x.array().exp().matrix();
  Cold ret=-expX/expX.sum();
  ret[id]+=1;
  return ret;
}
//Kullback-Leibler distance
scalarD KLDistanceSoftMax(const Cold& x1,const Cold& x2)
{
  Cold expX1=x1.array().exp().matrix();
  Cold expX2=x2.array().exp().matrix();
  expX1/=expX1.sum();
  expX2/=expX2.sum();
  return (expX1.array()*(expX1.array()/expX2.array()).log()).sum();
}
Cold diffKLDistanceSoftMax(const Cold& x1,const Cold& x2)
{
  Cold expX1=x1.array().exp().matrix();
  Cold expX2=x2.array().exp().matrix();
  expX1/=expX1.sum();
  expX2/=expX2.sum();
  return -expX1+expX2;
}
Matd hessKLDistanceSoftMax(const Cold& x2)
{
  Cold expX2=x2.array().exp().matrix();
  expX2/=expX2.sum();

  Matd ret=Matd::Zero(x2.size(),x2.size());
  ret.diagonal()=expX2;
  ret-=expX2*expX2.transpose();
  return ret;
}

//random subset
Mat4d AToB(Cold a,Cold b)
{
  Mat4d ret=Mat4d::Identity();
  sizeType nrP=a.size()/3;
  //translation
  Eigen::Map<Mat3Xd> aP(a.data(),3,nrP);
  Eigen::Map<Mat3Xd> bP(b.data(),3,nrP);
  Vec3d aCtr=aP*Cold::Constant(nrP,1/(scalarD)nrP);
  aP-=aCtr*Cold::Ones(nrP).transpose();
  Vec3d bCtr=bP*Cold::Constant(nrP,1/(scalarD)nrP);
  bP-=bCtr*Cold::Ones(nrP).transpose();
  //rotation
  Eigen::JacobiSVD<Mat3d> svd(aP*bP.transpose(),Eigen::ComputeFullU|Eigen::ComputeFullV);
  ret.block<3,3>(0,0)=svd.matrixV()*svd.matrixU().transpose();
  ret.block<3,1>(0,3)=bCtr-ret.block<3,3>(0,0)*aCtr;
  return ret;
}
vector<sizeType> randomSubset(sizeType NT,sizeType N)
{
  if(NT < N)
    return vector<sizeType>();
  vector<sizeType> ret(N);
  boost::unordered_map<sizeType,sizeType> perm;
  for(sizeType i=0; i<N; i++) {
    sizeType other=RandEngine::randI(i,NT-1);
    if(perm.find(i) == perm.end())
      perm[i]=i;
    if(perm.find(other) == perm.end())
      perm[other]=other;
    swap(perm[i],perm[other]);
    ret[i]=perm[i];
  }
  return ret;
}
//miscallenous
scalarD meanDist(const Cold& vals)
{
  return vals.sum()/(scalarD)vals.size();
}
scalarD SDDist(const Cold& vals)
{
  return sqrt(meanDist((vals.array()-meanDist(vals)).square().matrix()));
}
Vec3 randVec(sizeType dim)
{
  Vec3 ret=Vec3::Zero();
  ret.segment(0,dim).setRandom();
  return ret;
}
Vec3 randVecR(sizeType dim)
{
  Vec3 ret=Vec3::Zero();
  if(dim == 2)
    ret.segment<1>(2).setRandom();
  else ret.setRandom();
  return ret*M_PI*2;
}

PRJ_END
