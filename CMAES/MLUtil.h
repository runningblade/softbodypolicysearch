#ifndef ML_UTIL_H
#define ML_UTIL_H

#include <CommonFile/MathBasic.h>
#include <CommonFile/Zero.h>
#include <boost/unordered_map.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

void DEBUG_COMPARE(const string& name,scalarD VAL,scalarD ERR,scalarD THRES);

//multivariate normal distribution
Cold proposeNormal(const Cold& mean,const Matd& cov);
scalarD logPossNormal(const Cold& mean,const Matd& cov,const Cold& val);
Cold DLogPossDMeanNormal(const Cold& mean,const Matd& cov,const Cold& val);
Matd DLogPossDCovNormal(const Cold& mean,const Matd& cov,const Cold& val);
//singlevariate normal distribution
scalarD proposeNormal(scalarD mean,scalarD cov);
scalarD logPossNormal(scalarD mean,scalarD cov,scalarD val);
scalarD DLogPossDMeanNormal(scalarD mean,scalarD cov,scalarD val);
scalarD DLogPossDCovNormal(scalarD mean,scalarD cov,scalarD val);
scalarD confidenceCovNormal(scalarD range,scalarD confidence);
//debug
void debugDLogPossNormal(sizeType nr);
void debugIntegralNormal(sizeType nr);
void debugDLogPossNormal(const Cold& mean,const Matd& cov);
void debugIntegralNormal(const Cold& mean,const Matd& cov);
void debugConfidenceCovNormal();
//multivariate Kullback-Leibler distance: KL(N(mean1,cov1),N(mean2,cov2))
scalarD KLDistanceNormal(const Cold& mean1,const Matd& cov1,const Cold& mean2,const Matd& cov2);
scalarD KLDistanceBFNormal(const Cold& mean1,const Matd& cov1,const Cold& mean2,const Matd& cov2);
void diffKLDistanceNormal(Cold& diffMean1,Matd& diffCov1,const Cold& mean1,const Matd& cov1,
                          Cold& diffMean2,Matd& diffCov2,const Cold& mean2,const Matd& cov2);
//singlevariate Kullback-Leibler distance: KL(N(mean1,cov1),N(mean2,cov2))
scalarD KLDistanceNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2);
scalarD KLDistanceBFNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2);
Vec4d diffKLDistanceNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2);
Mat4d hessKLDistanceNormal(scalarD mean1,scalarD cov1,scalarD mean2,scalarD cov2);
//debug
void debugKLDistanceNormal(sizeType nr);

//softmax
sizeType proposeSoftMax(const Cold& x);
scalarD logPossSoftMax(const Cold& x,sizeType id);
Cold DLogPossDXSoftMax(const Cold& x,sizeType id);
//Kullback-Leibler distance
scalarD KLDistanceSoftMax(const Cold& x1,const Cold& x2);
Cold diffKLDistanceSoftMax(const Cold& x1,const Cold& x2);
Matd hessKLDistanceSoftMax(const Cold& x2);

//random subset
Mat4d AToB(Cold a,Cold b);
vector<sizeType> randomSubset(sizeType NT,sizeType N);
template <typename T>
vector<T> randomSubset(const vector<T>& vss,sizeType N)
{
  vector<sizeType> ids=randomSubset((sizeType)vss.size(),N);
  vector<T> ret(ids.size());
  for(sizeType i=0; i<(sizeType)ids.size(); i++)
    ret[i]=vss[ids[i]];
  return ret;
}
template <typename T>
T matrixSqrt(const T& metric,T* metricInv=NULL)
{
  Eigen::SelfAdjointEigenSolver<T> eig(metric,Eigen::ComputeEigenvectors);
  Eigen::DiagonalMatrix<typename T::Scalar,-1,-1> diag(metric.rows()),diagInv(metric.rows());
  diag.diagonal().array()=eig.eigenvalues().cwiseAbs().array().sqrt();
  if(metricInv) {
    diagInv.diagonal().array()=1/eig.eigenvalues().array().max(1E-10f);
    *metricInv=eig.eigenvectors()*(diagInv*eig.eigenvectors().transpose());
  }
  return eig.eigenvectors()*(diag*eig.eigenvectors().transpose());
}
//miscallenous
scalarD meanDist(const Cold& vals);
scalarD SDDist(const Cold& vals);
Vec3 randVec(sizeType dim);
Vec3 randVecR(sizeType dim);

//vector read/write for property_tree
template <typename VEC>
VEC parseVec(const string& name,const boost::property_tree::ptree& tree)
{
  vector<typename VEC::Scalar> x;
  for(sizeType i=0;; i++) {
    boost::optional<typename VEC::Scalar> prop=
      tree.get_optional<typename VEC::Scalar>(name+".x"+boost::lexical_cast<string>(i));
    if(!prop)
      break;
    x.push_back(*prop);
  }
  return Eigen::Map<const VEC>(&x[0],x.size());
}
template <typename VEC>
VEC parseVec(const string& name,sizeType frm,const boost::property_tree::ptree& tree)
{
  return parseVec<VEC>(name+boost::lexical_cast<string>(frm),tree);
}
template <typename VEC>
void assignVec(const VEC& x,const string& name,boost::property_tree::ptree& tree)
{
  for(sizeType i=0; i<x.size(); i++)
    tree.put<typename VEC::Scalar>(name+".x"+boost::lexical_cast<string>(i),x[i]);
}
template <typename VEC>
void assignVec(const VEC& x,const string& name,sizeType frm,boost::property_tree::ptree& tree)
{
  assignVec<VEC>(x,name+boost::lexical_cast<string>(frm),tree);
}
template <typename VEC>
VEC copyConcat(const VEC& x,sizeType nr)
{
  VEC ret;
  ret.setZero(x.size()*nr);
  for(sizeType i=0; i<nr; i++)
    ret.segment(i*x.size(),x.size())=x;
  return ret;
}

PRJ_END

#endif
