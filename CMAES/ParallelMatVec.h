#ifndef PARALLEL_MAT_VEC_H
#define PARALLEL_MAT_VEC_H

#include <CommonFile/MathBasic.h>

PRJ_BEGIN

template <typename T>
class ParallelMatVec
{
public:
  typedef vector<T,Eigen::aligned_allocator<T> > vector_type;
  ParallelMatVec() {
    clear(0,1);
  }
  ParallelMatVec(sizeType r,sizeType c=1) {
    clear(r,c);
  }
  void clear(sizeType r,sizeType c=1) {
    _r=r;
    _c=c;
    _blocks.assign(omp_get_num_procs(),T::Zero(_r,_c));
  }
  void clear() {
    clear(_r,_c);
  }
  void operator+=(const T& newVal) {
    ASSERT((sizeType)_blocks.size() > 1)
    _blocks[omp_get_thread_num()]+=newVal;
  }
  T& getCurr() {
    ASSERT((sizeType)_blocks.size() > 1)
    return _blocks[omp_get_thread_num()];
  }
  const T& get() const {
    const_cast<ParallelMatVec<T>*>(this)->join();
    return _blocks[0];
  }
  T& get() {
    join();
    return _blocks[0];
  }
protected:
  void join() {
    for(sizeType i=1; i<(sizeType)_blocks.size(); i++) {
      _blocks[0]+=_blocks[i];
      _blocks[i].setZero(_r,_c);
    }
    _blocks.resize(1);
  }
  vector_type _blocks;
  sizeType _r,_c;
};

PRJ_END

#endif
