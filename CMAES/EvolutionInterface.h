#ifndef CMAES_INTERFACE_H
#define CMAES_INTERFACE_H

#include "MPIInterface.h"
#include <MultiTraj/MultiTrajCallback.h>
#include <SoftBodyController/FEMEnvironment.h>
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/solvers/Callback.h>
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

//minimizer
class MPIInterface;
class Environment;
class EnvObjective : public Objective<scalarD>
{
public:
  EnvObjective(Environment& env);
  bool feasible(const Vec& x);
  const Environment& getEnv() const;
  Environment& getEnv();
  const EnvironmentMT* getEnvMT() const;
  EnvironmentMT* getEnvMT();
  const Vec& getS0() const;
  Matd getBounds() const;
  int inputs() const;
protected:
  Environment& _env;
  Vec _s0;
};
struct MultiTrajCMAESWrapper : public MultiTrajCallback {
  MultiTrajCMAESWrapper(FEMEnvironment& env,sizeType trajLen,sizeType nrTraj,scalar coefPerturb,sizeType k,const string& path);
  MultiTrajCMAESWrapper(Environment& env,const Matd& points,const Cold& s0,sizeType trajLen,sizeType k,const string& path);
  Cold onNewState(const Cold& state,sizeType tid,sizeType fid) override;
  Cold getInitState(sizeType tid) override;
  sizeType initialize() override;
  sizeType trajLen() override;
  const Cold& rewards() const;
protected:
  Environment& _env;
  Matd _points;
  Cold _s0,_rewards;
  sizeType _trajLen,_k;
  const string _path;
};
class Evolution
{
  typedef Objective<scalarD> ObjFunc;
  typedef ObjFunc::Vec Vec;
  typedef Matd Mat;
public:
  Evolution();
  Evolution::Vec minimizeCMAES(Objective<scalarD>& obj,const string& resume="");
  Evolution::Vec fetchBestX(Objective<scalarD>& obj,const string& input);
  void learn(Environment& env,const string& resume="");
  //to configure the algorithm
  void setBound(sizeType id,scalarD low,scalarD high);
  void writePar(const string& path,const Objective<scalarD>& obj) const;
  void setVector(const string& name,const Vec& pt);
  MPIInterface* getMPI() const;
  //to run and visual maze solving example
  static void runExampleMaze(Evolution& evo,Vec3i res,sizeType maxIterWalk=100);
  boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > > _cb;
  boost::property_tree::ptree _pt;
private:
  template <typename T>
  void write(ostream& os,const string& name,T def,bool optional=false) const;
  void writeVector(ostream& os,const string& name,sizeType N,scalarD def,bool optional=false) const;
  Vec transformBoundary(const Objective<scalarD>& obj,const Vec& vec) const;
  Evolution::Vec evalPopulation(Objective<scalarD>& obj,const Mat& points);
  Evolution::Vec evalPopulationEnv(EnvObjective& obj,const Mat& points,sizeType k,const string& path);
  Evolution::Vec evalPopulationEnvMT(EnvObjective& obj,const Mat& points,const string& path);
  void mpiSlaveRun(Objective<scalarD>& obj);
  map<sizeType,pair<scalarD,scalarD> > _bounds;
  boost::shared_ptr<MPIInterface> _mpi;
};

PRJ_END

#endif
