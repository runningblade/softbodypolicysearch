#include "EvolutionInterface.h"
#include "MPIInterface.h"
#include <Dynamics/FEMUtils.h>
#include <Dynamics/FEMCIOEngine.h>
#include <CommonFile/GridOp.h>
#include <CommonFile/Timing.h>
#include <CommonFile/GridBasic.h>
#include <boost/lexical_cast.hpp>
#include <boost/interprocess/streams/vectorstream.hpp>
#include <deque>

extern "C" {
#include "boundary_transformation.h"
#include "cmaes_interface.h"
}

PRJ_BEGIN

//EnvObjective
EnvObjective::EnvObjective(Environment& env):_env(env)
{
  _s0=_env.sampleS0();
}
bool EnvObjective::feasible(const Vec& x)
{
  return true;
}
const Environment& EnvObjective::getEnv() const
{
  return _env;
}
Environment& EnvObjective::getEnv()
{
  return _env;
}
const EnvironmentMT* EnvObjective::getEnvMT() const
{
  return dynamic_cast<const EnvironmentMT*>(&_env);
}
EnvironmentMT* EnvObjective::getEnvMT()
{
  return dynamic_cast<EnvironmentMT*>(&_env);
}
const EnvObjective::Vec& EnvObjective::getS0() const
{
  return _s0;
}
Matd EnvObjective::getBounds() const
{
  boost::shared_ptr<NeuralNet> net=static_cast<NeuralNetPolicy&>(_env.getPolicy())._net;
  return net->getBounds();
}
int EnvObjective::inputs() const
{
  boost::shared_ptr<NeuralNet> net=static_cast<NeuralNetPolicy&>(_env.getPolicy())._net;
  return net->nrParam();
}
//MultiTrajCMAESWrapper
MultiTrajCMAESWrapper::MultiTrajCMAESWrapper(FEMEnvironment& env,sizeType trajLen,sizeType nrTraj,scalar coefPerturb,sizeType k,const string& path)
  :_env(env),_s0(env.sampleS0()),_trajLen(trajLen),_k(k),_path(path)
{
  boost::shared_ptr<NeuralNet> net=static_cast<NeuralNetPolicy&>(_env.getPolicy())._net;
  _points.resize(net->nrParam(),nrTraj);
  Matd bd=net->getBounds();
  Cold param=net->toVec(false);
  for(sizeType i=0; i<nrTraj; i++) {
    _points.col(i)=param;
    _points.col(i).array()+=Cold::Random(bd.rows()).array()*(bd.col(1)-bd.col(0)).array()*coefPerturb;
  }
}
MultiTrajCMAESWrapper::MultiTrajCMAESWrapper(Environment& env,const Matd& points,const Cold& s0,sizeType trajLen,sizeType k,const string& path)
  :_env(env),_points(points),_s0(s0),_trajLen(trajLen),_k(k),_path(path) {}
Cold MultiTrajCMAESWrapper::onNewState(const Cold& state,sizeType tid,sizeType fid)
{
  ASSERT_MSG(fid <= trajLen(),"MultiTrajError, out-of-bound call to onNewState!")
  boost::shared_ptr<NeuralNet> net=static_cast<NeuralNetPolicy&>(_env.getPolicy())._net;
  _env.setTrajId(tid);
  net->fromVec(_points.col(tid));
  EnvironmentMT* mt=dynamic_cast<EnvironmentMT*>(&_env);
  if(_k >= 0 && mt)
    mt->setK(_k);
  Cold a=_env.sampleA(fid,state);
  _rewards[tid]+=_env.getCost(fid,state,a);
  if(!_path.empty()) {
    _env.setState(state);
    _env.writeStateVTK(_path+"/traj"+boost::lexical_cast<string>(tid)+"/frm"+boost::lexical_cast<string>(fid)+".vtk");
  }
  return a;
}
Cold MultiTrajCMAESWrapper::getInitState(sizeType tid)
{
  if(!_path.empty())
    recreate(_path+"/traj"+boost::lexical_cast<string>(tid));
  return _s0;
}
sizeType MultiTrajCMAESWrapper::initialize()
{
  if(!_path.empty())
    recreate(_path);
  _rewards.setZero(_points.cols());
  return _points.cols();
}
sizeType MultiTrajCMAESWrapper::trajLen()
{
  return _trajLen;
}
const Cold& MultiTrajCMAESWrapper::rewards() const
{
  return _rewards;
}
//Evolution
void readSignalsSafe(cmaes_t& evo,const string& str)
{
  boost::filesystem::path p(str);
  const string name="cmaes_signals.par";
  while(!p.empty() && !exists(p/name))
    p=p.parent_path();
  ASSERT_MSGV(exists(p/name),"We cannot find file %s",(p/name).string().c_str())
  cmaes_ReadSignals(&evo,(p/name).string().c_str());
}
void writeCMAData(cmaes_t& evo,boost::property_tree::ptree& pt,sizeType iter,bool last)
{
  const string workspace=pt.get<string>("workspace",".");
  const string iterStr="_iter"+boost::lexical_cast<string>(iter);
  //write all CMA results
  const string outputPath=pt.get<string>("outputFilePath","/cmaes_all");
  if(!outputPath.empty())
    cmaes_WriteToFile(&evo,"all",(workspace+outputPath+iterStr+".dat").c_str());
  //write CMA resume point
  const string resumePath=pt.get<string>("resumeFilePath","/cmaes_resume");
  if(!resumePath.empty())
    cmaes_WriteToFile(&evo,"resume",(workspace+resumePath+iterStr+".dat").c_str());
  //write property tree
  const string ptPath=pt.get<string>("ptPath","/cmaes_profile");
  if(!ptPath.empty())
    writePtreeAscii(pt,workspace+ptPath+iterStr+".xml");
  //write exit information
  if(last) {
    string stopInfo=cmaes_TestForTermination(&evo);
    INFOV("CMAES stopping information: %s",stopInfo.c_str());
    boost::filesystem::ofstream(workspace+"/stop_info.dat") << stopInfo << endl;
  }
}
Evolution::Evolution() {}
Evolution::Vec Evolution::minimizeCMAES(Objective<scalarD>& obj,const string& resume)
{
  if(_mpi) {
    if(_mpi->currProc() != 0) {
      mpiSlaveRun(obj);
      return Vec();
    } else {
      try {
        _mpi->broadcastObject(dynamic_cast<Serializable&>(obj),0);
      } catch(...) {
        WARNING("You need a serializable objective!")
        exit(EXIT_FAILURE);
      }
    }
  }

  //declaration
  cmaes_t evo;
  cmaes_boundary_transformation_t boundaries;
  double *arFunvals,*xInBounds,*const* pop;

  //set boundary
  vector<double> lowerBounds(obj.inputs(),-ScalarUtil<scalar>::scalar_max);
  vector<double> upperBounds(obj.inputs(),ScalarUtil<scalar>::scalar_max);
  for(map<sizeType,pair<scalarD,scalarD> >::const_iterator beg=_bounds.begin(),end=_bounds.end(); beg!=end; beg++) {
    lowerBounds[beg->first]=(double)(beg->second.first);
    upperBounds[beg->first]=(double)(beg->second.second);
  }
  cmaes_boundary_transformation_init(&boundaries,&lowerBounds[0],&upperBounds[0],obj.inputs());

  //initialize
  const string workspace=_pt.get<string>("workspace",".");
  create(workspace);
  writePar(workspace+"/init.par",obj);
  arFunvals=cmaes_init(&evo,(int)obj.inputs(),NULL,NULL,0,0,(workspace+"/init.par").c_str());
  xInBounds=cmaes_NewDouble(obj.inputs());
  readSignalsSafe(evo,workspace);
  if(_cb)
    (*_cb).reset();

  //the main loop with boundary transformation
  sizeType iter=0,outputInterval=_pt.get<sizeType>("outputInterval",1);
  if(!resume.empty()) {
    INFOV("Resuming CMA-ES from: %s!",resume.c_str())
    cmaes_resume_distribution(&evo,(char*)resume.c_str());
  }
  for(; !cmaes_TestForTermination(&evo); iter++) {
    //sample population
    sizeType nrPop=cmaes_Get(&evo,"lambda");
    Mat points=Mat::Zero(obj.inputs(),nrPop);
    pop=cmaes_SamplePopulation(&evo);
    obj.onNewPop();
    for(sizeType i=0; i<nrPop; i++) {
      //generate sample
      cmaes_boundary_transformation(&boundaries,pop[i],xInBounds,obj.inputs());
      while(!obj.feasible(Eigen::Map<Eigen::Matrix<double,-1,1> >(xInBounds,obj.inputs()).cast<scalarD>())) {
        cmaes_ReSampleSingle(&evo,i);
        cmaes_boundary_transformation(&boundaries,pop[i],xInBounds,obj.inputs());
      }
      points.col(i)=Eigen::Map<const Eigen::Matrix<double,-1,1> >(xInBounds,obj.inputs()).cast<scalarD>();
    }
    //evaluate population
    string path=_pt.get<string>("debugOutputPath","");
    if(!path.empty()) {
      srand(0);
      points.setRandom();
      points.array()=points.array()*0.5f+0.5f;
    }
    if(dynamic_cast<EnvObjective*>(&obj) && dynamic_cast<EnvObjective*>(&obj)->getEnvMT())
      Eigen::Map<Eigen::Matrix<double,-1,1> >(arFunvals,nrPop)=evalPopulationEnvMT((EnvObjective&)obj,points,path).cast<double>();
    else if(dynamic_cast<EnvObjective*>(&obj))
      Eigen::Map<Eigen::Matrix<double,-1,1> >(arFunvals,nrPop)=evalPopulationEnv((EnvObjective&)obj,points,-1,path).cast<double>();
    else Eigen::Map<Eigen::Matrix<double,-1,1> >(arFunvals,nrPop)=evalPopulation(obj,points).cast<double>();
    if(!path.empty())
      exit(EXIT_SUCCESS);
    for(sizeType i=0; i<nrPop; i++)
      _pt.put<scalar>("funval"+boost::lexical_cast<string>(i),arFunvals[i]);
    //CMAES update
    cmaes_UpdateDistribution(&evo,arFunvals);
    readSignalsSafe(evo,workspace);
    fflush(stdout);
    //output
    if((iter%outputInterval) == 0)
      writeCMAData(evo,_pt,iter,false);
    //callback
    if(_cb && (*_cb)(iter,Eigen::Map<Eigen::Matrix<double,-1,1> >(arFunvals,nrPop).cast<scalarD>(),points) != 0)
      break;
  }

  //output
  writeCMAData(evo,_pt,iter,true);
  cmaes_boundary_transformation(&boundaries,(double const*)cmaes_GetPtr(&evo,"xbest"),xInBounds,obj.inputs());
  Vec ret=Eigen::Map<Eigen::Matrix<double,-1,1> >(xInBounds,obj.inputs()).cast<scalarD>();

  //release resources
  cmaes_exit(&evo);
  cmaes_boundary_transformation_exit(&boundaries);
  free(xInBounds);
  if(_mpi)
    _mpi->signalShutdown(0,true);
  return ret;
}
Evolution::Vec Evolution::fetchBestX(Objective<scalarD>& obj,const string& input)
{
  Evolution::Vec ret=Evolution::Vec::Zero(0);
  sizeType nrInputs=-1;
  string line,keyword;
  boost::filesystem::ifstream is(input);
  while(!is.eof() && std::getline(is,line).good()) {
    istringstream iss(line);
    iss >> keyword;
    if(keyword == "N")
      iss >> nrInputs;
    else if(keyword == "xbestever" && nrInputs == obj.inputs()) {
      ret=Evolution::Vec::Zero(obj.inputs());
      for(sizeType curr=0; curr<ret.size();) {
        std::getline(is,line);
        istringstream issVec(line);
        while(curr<ret.size() && !issVec.eof())
          issVec >> ret[curr++];
      }
      ret=transformBoundary(obj,ret);
      break;
    }
  }
  return ret;
}
void Evolution::learn(Environment& env,const string& resume)
{
  EnvObjective obj(env);
  Matd bounds=obj.getBounds();
  for(sizeType i=0; i<bounds.rows(); i++)
    setBound(i,0,1);
  setVector("initialStandardDeviations",Cold::Constant(bounds.rows(),0.1f));
  //solve
  boost::shared_ptr<NeuralNet> net=static_cast<NeuralNetPolicy&>(env.getPolicy())._net;
  INFOV("CMAES searching in %ld-dimensional space!",net->nrParam())
  Vec ret=minimizeCMAES(obj,resume);
  //assign
  for(sizeType i=0; i<ret.size(); i++)
    ret[i]=interp1D(bounds(i,0),bounds(i,1),ret[i]);
  net->fromVec(ret);
}
void Evolution::setBound(sizeType id,scalarD low,scalarD high)
{
  _bounds[id]=make_pair(low,high);
}
void Evolution::writePar(const string& path,const Objective<scalarD>& obj) const
{
  boost::filesystem::ofstream os(path);
  write<sizeType>(os,"N",obj.inputs());
  writeVector(os,"initialX",obj.inputs(),0.5);
  writeVector(os,"typicalX",obj.inputs(),0.5);
  writeVector(os,"initialStandardDeviations",obj.inputs(),0.3);
  write<sizeType>(os,"stopMaxFunEvals",numeric_limits<int>::max());//900*(N+3)*(N+3));
  write<scalarD>(os,"fac*maxFunEvals",1.0f,true);
  //-------------------------------------------------------------------
  if(_pt.get_optional<sizeType>("stopMaxIter"))
    write<sizeType>(os,"stopMaxIter",_pt.get<sizeType>("stopMaxIter"));
  else if(_pt.get_optional<sizeType>("maxIter"))
    write<sizeType>(os,"stopMaxIter",_pt.get<sizeType>("maxIter"));
  else write<sizeType>(os,"stopMaxIter",numeric_limits<int>::max());//900*(N+3)*(N+3));
  //-------------------------------------------------------------------
  write<scalarD>(os,"stopFitness",1E-9,true);
  write<scalarD>(os,"stopTolFun",1E-12);
  write<scalarD>(os,"stopTolFunHist",1E-13);
  write<scalarD>(os,"stopTolX",1E-11);
  write<scalarD>(os,"stopTolUpXFactor",1E3);
  //-------------------------------------------------------------------
  if(_pt.get_optional<sizeType>("seed"))
    write<sizeType>(os,"seed",_pt.get<sizeType>("seed"));
  else write<scalarD>(os,"seed",1000);
  //-------------------------------------------------------------------
  writeVector(os,"diffMinChange",obj.inputs(),1E-299,true);
  write<scalarD>(os,"maxTimeFractionForEigendecompostion",1);
  write<scalarD>(os,"updatecov",1,true);
  write<scalarD>(os,"ccov",1,true);
  write<scalarD>(os,"fac*updatecov",3,true);
  write<string>(os,"resume","none",true);
  //-------------------------------------------------------------------
  if(_pt.get_optional<sizeType>("lambda"))
    write<sizeType>(os,"lambda",_pt.get<sizeType>("lambda"));
  else if(_pt.get_optional<sizeType>("maxTraj"))
    write<sizeType>(os,"lambda",_pt.get<sizeType>("maxTraj"));
  else write<sizeType>(os,"lambda",9);
  //-------------------------------------------------------------------
  write<scalarD>(os,"mu",0,true);
  write<string>(os,"weights","log",true);
  write<sizeType>(os,"diagonalCovarianceMatrix",-1,true);
  write<scalarD>(os,"fac*damp",1,true);
  write<scalarD>(os,"ccumcov",1,true);
  write<scalarD>(os,"mucov",1,true);
  write<scalarD>(os,"fac*ccov",1,true);
}
void Evolution::setVector(const string& name,const Vec& pt)
{
  for(sizeType i=0; i<pt.size(); i++)
    _pt.put<scalarD>(name+boost::lexical_cast<string>(i),pt[i]);
}
MPIInterface* Evolution::getMPI() const
{
  return _mpi.get();
}
template <typename T>
void Evolution::write(ostream& os,const string& name,T def,bool optional) const
{
  optional=optional && _pt.find(name) == _pt.not_found();
  def=_pt.get<T>(name,def);
  os << (optional ? "#" : "") << name << " " << def << endl;
}
void Evolution::writeVector(ostream& os,const string& name,sizeType N,scalarD def,bool optional) const
{
  optional=optional && _pt.find(name+"0") == _pt.not_found();
  os << (optional ? "#" : "") << name << " " << N << ":" << endl;
  os << (optional ? "#" : "");
  for(sizeType i=0; i<N; i++)
    os << _pt.get<scalar>(name+boost::lexical_cast<string>(i),def) << " ";
  os << endl;
}
Evolution::Vec Evolution::transformBoundary(const Objective<scalarD>& obj,const Vec& vec) const
{
  cmaes_boundary_transformation_t boundaries;
  Eigen::Matrix<double,-1,1> vecTmp=vec.cast<double>();
  Eigen::Matrix<double,-1,1> vecOut=vec.cast<double>();
  vector<double> lowerBounds(obj.inputs(),-ScalarUtil<double>::scalar_max);
  vector<double> upperBounds(obj.inputs(),ScalarUtil<double>::scalar_max);
  for(map<sizeType,pair<scalarD,scalarD> >::const_iterator beg=_bounds.begin(),end=_bounds.end(); beg!=end; beg++) {
    lowerBounds[beg->first]=(double)(beg->second.first);
    upperBounds[beg->first]=(double)(beg->second.second);
  }
  cmaes_boundary_transformation_init(&boundaries,&lowerBounds[0],&upperBounds[0],obj.inputs());
  cmaes_boundary_transformation(&boundaries,(const double*)vecTmp.data(),(double*)vecOut.data(),obj.inputs());
  cmaes_boundary_transformation_exit(&boundaries);
  return vecOut.cast<scalarD>();
}
Evolution::Vec Evolution::evalPopulation(Objective<scalarD>& obj,const Mat& points)
{
  if(_mpi) {
    if(_mpi->nrProc() != points.cols()) {
      WARNINGV("Incorrect number of processes, you need %ld processes",points.cols())
      _mpi->abort();
      exit(EXIT_FAILURE);
    }
    //receive
    Vec receive=Vec::Zero(obj.inputs());
    //return
    Mat receive2=Mat::Zero(1,_mpi->nrProc());
    Vec send2=Vec::Zero(1);
    //evaluation loop
    _mpi->signalShutdown(0,false);
    TBEG();
    _mpi->scatter(points,receive,0);
    send2[0]=obj(receive);
    _mpi->gather(receive2,send2,0);
    TENDT("timeCost",_pt);
    return receive2.transpose();
  } else {
    Vec arFunvals(points.cols());
    for(sizeType i=0; i<points.cols(); i++) {
      TBEG();
      ostringstream oss;
      arFunvals[i]=obj(points.col(i));
      oss << "timeCost_" << i << "_" << points.cols();
      TENDT(oss.str(),_pt);
    }
    return arFunvals;
  }
}
Evolution::Vec Evolution::evalPopulationEnv(EnvObjective& obj,const Mat& points,sizeType k,const string& path)
{
  //interpolate
  Matd bounds=obj.getBounds();
  Matd pointsInterp=points;
  for(sizeType r=0; r<points.rows(); r++)
    for(sizeType c=0; c<points.cols(); c++)
      pointsInterp(r,c)=interp1D(bounds(r,0),bounds(r,1),points(r,c));
  //run
  scalarD r;
  Vec s0=obj.getS0(),s;
  //cout << s0.transpose() << endl;
  Vec ret=Vec::Zero(pointsInterp.cols());
  sizeType trajLen=_pt.get<sizeType>("trajLen");
  Environment& env=obj.getEnv();
  if(env.supportMultiTraj()) {
    MultiTrajCMAESWrapper cb(env,pointsInterp,s0,trajLen,k,path);
    env.startMultiTraj(cb);
    ret=cb.rewards();
  } else {
    if(!path.empty())
      recreate(path);
    Environment& env=obj.getEnv();
    boost::shared_ptr<NeuralNet> net=static_cast<NeuralNetPolicy&>(env.getPolicy())._net;
    for(sizeType i=0; i<pointsInterp.cols(); i++) {
      net->fromVec(pointsInterp.col(i));
      if(k >= 0)
        obj.getEnvMT()->setK(k);
      env.setTrajId(i);
      env.setState(s=s0);
      if(!path.empty())
        recreate(path+"/traj"+boost::lexical_cast<string>(i));
      for(sizeType t=0; t<trajLen; t++) {
        Vec a=env.sampleA(t,s);
        s=env.transfer(t,a,r);
        if(!path.empty())
          env.writeStateVTK(path+"/traj"+boost::lexical_cast<string>(i)+"/frm"+boost::lexical_cast<string>(t)+".vtk");
        ret[i]-=r;
      }
    }
  }
  return ret;
}
Evolution::Vec Evolution::evalPopulationEnvMT(EnvObjective& obj,const Mat& points,const string& path)
{
  Vec ret=Vec::Zero(points.cols());
  EnvironmentMT* mt=obj.getEnvMT();
  for(sizeType k=0; k<mt->K(); k++) {
    const string pathTask=path+"Task"+boost::lexical_cast<string>(k);
    ret+=evalPopulationEnv(obj,points,k,path == "" ? "" : pathTask);
  }
  return ret;
}
void Evolution::mpiSlaveRun(Objective<scalarD>& obj)
{
  try {
    _mpi->broadcastObject(dynamic_cast<Serializable&>(obj),0);
  } catch(...) {
    WARNING("You need a serializable objective!")
    exit(EXIT_FAILURE);
  }
  //receive
  Mat send=Mat::Zero(obj.inputs(),_mpi->nrProc());
  Vec receive=Vec::Zero(obj.inputs());
  //return
  Mat receive2=Mat::Zero(1,_mpi->nrProc());
  Vec send2=Vec::Zero(1);
  //evaluation loop
  while(!_mpi->signalShutdown(0,false)) {
    _mpi->scatter(send,receive,0);
    send2[0]=obj(receive);
    _mpi->gather(receive2,send2,0);
  }
}
//example
#define FACTOR 4
#define NEIGH(GRD) Vec3i::Unit((GRD)/2)*(((GRD)%2)*2-1)
class MazeObjective : public Objective<scalarD>, public Serializable
{
public:
  MazeObjective():Serializable(typeid(MazeObjective).name()) {}
  MazeObjective(const ScalarField& grid,const sizeType& VAL):Serializable(typeid(MazeObjective).name()),_grid(grid),_VAL(VAL) {}
  virtual bool read(istream& is,IOData* dat) {
    readBinaryData(_grid,is);
    readBinaryData(_VAL,is);
    return is.good();
  }
  virtual bool write(ostream& os,IOData* dat) const {
    writeBinaryData(_grid,os);
    writeBinaryData(_VAL,os);
    return os.good();
  }
  virtual boost::shared_ptr<Serializable> copy() const {
    return boost::shared_ptr<Serializable>(new MazeObjective());
  }
  virtual scalarD operator()(const Vec& x) {
    return _grid.sampleSafe(Vec3((scalar)x[0],(scalar)x[1],0));
  }
  virtual bool feasible(const Vec& x) {
    sizeType offS[4];
    scalar coefs[4];

    sizeType NR=_grid.getSampleStencilSafe(Vec3(x[0],x[1],0),coefs,NULL,offS,NULL);
    for(sizeType i=0; i<NR; i++)
      if(_grid.get(offS[i]) == _VAL)
        return false;
    return true;
  }
  virtual int inputs() const {
    return 2;
  }
  ScalarField _grid;
  sizeType _VAL;
};
class EvolutionVisualizer : public Callback<scalarD,Kernel<scalarD> >
{
public:
  EvolutionVisualizer(const string& path):_path(path) {}
  void reset() {
    recreate(_path);
    _iter=0;
  }
  sizeType operator()(sizeType iter,const Vec& vals,const Mat& points) {
    VTKWriter<scalarD> os("CMAPopulation",_path+"/iter"+boost::lexical_cast<string>(iter)+".vtk",true);
    vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
    for(sizeType i=0; i<(sizeType)points.cols(); i++) {
      Vec3d pt=Vec3d::Zero();
      sizeType nr=min(pt.size(),points.rows());
      pt.block(0,0,nr,1)=points.col(i).block(0,0,nr,1).cast<scalarD>();
      vss.push_back(pt);
    }
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,0),
                   VTKWriter<scalarD>::IteratorIndex<Vec3i>(vss.size(),0,0),
                   VTKWriter<scalarD>::POINT);
    return 0;
  }
  const string _path;
  sizeType _iter;
};
void Evolution::runExampleMaze(Evolution& evo,Vec3i res,sizeType maxIterWalk)
{
  ASSERT_MSG(res[0] >= 3 && res[1] >= 3 && res[2] == 0,
             "Incorrect size, you must have: res=(i>=3,j>=3,0)")

  //declaration
  ScalarField grid;
  grid.reset(res,BBox<scalar>(Vec3::Zero(),Vec3(1,1,0)),0);
  scalar VAL=grid.getNrPoint().prod();
  maxIterWalk*=VAL;
  grid.init(VAL);

  //generate a maze by random walk
  Vec3i init=Vec3i(1,1,0),target=init,curr=init;
  for(sizeType iter=0; iter<maxIterWalk; iter++) {
    grid[curr]=0;
    if((target-init).norm() < (curr-init).norm())
      target=curr;
    const sizeType next=RandEngine::randI(0,3);
    const Vec3i nextGrd=curr+NEIGH(next);
    //you are just moving in the maze
    if(grid[nextGrd] == 0) {
      curr=nextGrd;
      continue;
    } else {
      //check valid
      bool valid=true;
      for(sizeType i=0; i<2; i++) {
        if(nextGrd[i] == 0 || nextGrd[i] == res[i]-1)
          valid=false;
      }
      for(sizeType i=0; i<4; i++) {
        Vec3i other=nextGrd+NEIGH(i);
        if(other != curr && grid[other] == 0)
          valid=false;
      }
      for(sizeType i=0; i<4; i++) {
        Vec3i other=nextGrd+Vec3i((i&1)*2-1,(i&2)-1,0);
        if(grid[other] == 0 &&
           grid[Vec3i(nextGrd[0],other[1],0)] != 0 &&
           grid[Vec3i(other[0],nextGrd[1],0)] != 0)
          valid=false;
      }
      //move
      if(valid)
        curr=nextGrd;
    }
  }

  //upsample
  {
    res*=FACTOR;
    ScalarField tmp=grid;
    grid.reset(res,tmp.getBB(),VAL);
    for(sizeType x=0; x<res[0]; x++)
      for(sizeType y=0; y<res[1]; y++)
        grid.get(Vec3i(x,y,0))=tmp.get(Vec3i(x/FACTOR,y/FACTOR,0));

    init=init*FACTOR;
    init.block<2,1>(0,0).array()+=FACTOR/2;

    target=target*FACTOR;
    target.block<2,1>(0,0).array()+=FACTOR/2;
  }

  //create a gradient
  std::deque<Vec3i> ss;
  ss.push_back(target);
  grid[target]=1;
  while(!ss.empty()) {
    Vec3i curr=ss.front();
    ss.pop_front();
    for(sizeType i=0; i<4; i++) {
      Vec3i other=curr+NEIGH(i);
      if(grid[other] == 0) {
        grid[other]=grid[curr]+1;
        ss.push_back(other);
      }
    }
  }

  //write VTK
  {
    vector<scalar> val;
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;

    Vec3i id=Vec3i::Zero();
    Vec3 cellSz=grid.getCellSize();
    VTKWriter<scalar> os("maze","./exampleMaze/maze.vtk",true);
    for(id[0]=0; id[0]<grid.getNrPoint()[0]; id[0]++)
      for(id[1]=0; id[1]<grid.getNrPoint()[1]; id[1]++) {
        vss.push_back(grid.getPt(id)-cellSz/2);
        vss.push_back(grid.getPt(id)+cellSz/2);
        val.push_back(grid[id]);
        //if(grid[id] < 1000.0f)
        //  vss.back()+=Vec3::Unit(2)*grid[id]*0.001f;
      }
    os.appendVoxels(vss.begin(),vss.end(),true);
    os.appendCustomData("color",val.begin(),val.end());
    GridOp<scalar,scalar>::write2DScalarGridVTK("./exampleMaze/mazeFunc.vtk",grid,false);
  }

  //solve using CMA
  MazeObjective obj(grid,VAL);
  evo._cb.reset(new EvolutionVisualizer("./exampleMaze/CMAESIters"));
  evo.setBound(0,0,1);
  evo.setBound(1,0,1);
  evo.setVector("initialX",grid.getPt(init).block<2,1>(0,0).cast<scalarD>());
  evo.setVector("initialStandardDeviations",Vec2d::Constant(0.1f));
  evo._pt.put<scalar>("lambda",10);
  //evo._pt.put<sizeType>("stopMaxIter",10);
  evo._pt.put<string>("workspace","./exampleMaze");
  evo.minimizeCMAES(obj);
}
#undef NEIGH
#undef FACTOR

PRJ_END
