#ifndef MPI_INTERFACE_H
#define MPI_INTERFACE_H

#include <CommonFile/IO.h>
#include <boost/interprocess/streams/vectorstream.hpp>

PRJ_BEGIN

//MPIInterface
class MPIInterface
{
public:
  template <typename T>
  struct registerTypeFlexible {
    static void registerType(IOData* dat) {}
  };
  template <typename T>
  struct registerTypeFlexible<boost::shared_ptr<T> > {
    static void registerType(IOData* dat) {
      COMMON::registerType(dat,boost::shared_ptr<Serializable>(new T));
    }
  };
  static void createMPI(int argc,char** argv);
  static MPIInterface* getInterface();
  ~MPIInterface();
  //get information
  int nrProc() const;
  int currProc() const;
  static void redirectIO(const string& filename,int pid=0);
  static bool isIORedirected();
  //do static schedule
  Vec2i fromTo(sizeType nrJob) const;
  //do work: broadcast
  void broadcastObject(Serializable& obj,int sender);
  template <typename T>
  void broadcastTpl(T& data,int sender);
  void broadcast(sizeType& data,int sender);
  void broadcast(scalarF& data,int sender);
  void broadcast(scalarD& data,int sender);
  void broadcast(string& data,int sender);
  void broadcast(Mati& data,int sender);
  void broadcast(Matf& data,int sender);
  void broadcast(Matd& data,int sender);
  void broadcast(Matc& data,int sender);
  void broadcast(Coli& data,int sender);
  void broadcast(Colf& data,int sender);
  void broadcast(Cold& data,int sender);
  void broadcast(Colc& data,int sender);
  //do work: scatter
  template <typename MAT,typename VEC>
  void scatterTpl(const MAT& send,VEC& receive,int sender);
  void scatter(const Mati& send,Coli& receive,int sender);
  void scatter(const Matf& send,Colf& receive,int sender);
  void scatter(const Matd& send,Cold& receive,int sender);
  void scatter(const Matc& send,Colc& receive,int sender);
  template <typename VEC>
  void scatterSTpl(const VEC& send,typename VEC::Scalar& receive,int sender);
  void scatter(const Coli& send,sizeType& receive,int sender);
  void scatter(const Colf& send,scalarF& receive,int sender);
  void scatter(const Cold& send,scalarD& receive,int sender);
  void scatter(const Colc& send,char& receive,int sender);
  //do work: gather
  template <typename MAT,typename VEC>
  void gatherTpl(MAT& receive,const VEC& send,int receiver);
  void gather(Mati& receive,const Coli& send,int receiver);
  void gather(Matf& receive,const Colf& send,int receiver);
  void gather(Matd& receive,const Cold& send,int receiver);
  void gather(Matc& receive,const Colc& send,int receiver);
  template <typename VEC>
  void gatherSTpl(VEC& receive,const typename VEC::Scalar& send,int receiver);
  void gather(Coli& receive,const sizeType& send,int receiver);
  void gather(Colf& receive,const scalarF& send,int receiver);
  void gather(Cold& receive,const scalarD& send,int receiver);
  void gather(Colc& receive,const char& send,int receiver);
  //do work: scatterDynamic
  template <typename MAT,typename ALLOC>
  void scatterDynamicTpl(const vector<MAT,ALLOC>& send,MAT& receive,int sender);
  void scatterDynamic(const vector<Mati,Eigen::aligned_allocator<Mati> >& send,Mati& receive,int sender);
  void scatterDynamic(const vector<Matf,Eigen::aligned_allocator<Matf> >& send,Matf& receive,int sender);
  void scatterDynamic(const vector<Matd,Eigen::aligned_allocator<Matd> >& send,Matd& receive,int sender);
  void scatterDynamic(const vector<Matc,Eigen::aligned_allocator<Matc> >& send,Matc& receive,int sender);
  //do work: gatherDynamic
  template <typename MAT,typename ALLOC>
  void gatherDynamicTpl(vector<MAT,ALLOC>& receive,const MAT& send,int receiver);
  void gatherDynamic(vector<Mati,Eigen::aligned_allocator<Mati> >& receive,const Mati& send,int receiver);
  void gatherDynamic(vector<Matf,Eigen::aligned_allocator<Matf> >& receive,const Matf& send,int receiver);
  void gatherDynamic(vector<Matd,Eigen::aligned_allocator<Matd> >& receive,const Matd& send,int receiver);
  void gatherDynamic(vector<Matc,Eigen::aligned_allocator<Matc> >& receive,const Matc& send,int receiver);
  //do work: gatherDynamicVector
  template <typename OBJ>
  void gatherDynamicVectorTpl(vector<OBJ>& receive,const vector<OBJ>& send,int receiver) {
    boost::interprocess::basic_ovectorstream<vector<char> > os(ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    registerTypeFlexible<OBJ>::registerType(dat.get());
    writeVector(send,os,dat.get());
    os.flush();
    Mati sizes(1,nrProc());
    gather(sizes,Coli::Constant(1,os.vector().size()),receiver);

    sizeType maxSize=(currProc() == receiver)?sizes.maxCoeff():0;
    broadcast(maxSize,receiver);

    Matc receiveC=Matc::Zero(maxSize,nrProc());
    Colc sendC=Colc::Zero(maxSize);
    sendC.segment(0,os.vector().size())=Eigen::Map<const Colc>(os.vector().data(),os.vector().size());
    gather(receiveC,sendC,receiver);

    if(currProc() == receiver) {
      receive.clear();
      receive.reserve(receive.size()+sizes.sum());
      vector<char> buf(maxSize);
      for(sizeType i=0; i<receiveC.cols(); i++) {
        Eigen::Map<Colc>(&(buf[0]),buf.size())=receiveC.col(i);
        boost::interprocess::basic_ivectorstream<vector<char> > is(buf,ios::binary);
        boost::shared_ptr<IOData> dat=getIOData();
        registerTypeFlexible<OBJ>::registerType(dat.get());
        readVector(receive,is,dat.get(),true);
      }
    }
  }
  //showdown control
  void abort();
  bool signalShutdown(int sender,bool shutdown);
  void setSingleHost();
  void printInfo() const;
  //test
  template <typename MAT,typename VEC>
  void testMPIScatterGatherTpl();
  template <typename VEC>
  void testMPIScatterGatherSTpl();
  template <typename MAT>
  void testMPIScatterGatherDynamicTpl();
  void testMPIGatherDynamicObject();
  void testMPIGatherDynamicVector();
  void testMPIScatterGather();
  void testMPIScatterGatherDynamic();
  void testMPIBroadcast();
private:
  MPIInterface(int argc,char** argv);
  boost::shared_ptr<ofstream> _out;
  int _nrRank,_currRank;
  bool _initCalled;
  string _name;
};

PRJ_END

#endif
