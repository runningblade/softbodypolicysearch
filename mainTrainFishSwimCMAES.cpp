#include "mainUtils.h"
#include <SoftBodyController/DMPLayer.h>

USE_PRJ_NAMESPACE

#define NAME string("fishSwimCMAES")
int main(int argc,char** argv)
{
  FEMEnvironment env;
  if(exists(NAME+"Learned.dat")) {
    env.Environment::read(NAME+"Learned.dat");
    env.sol()._tree.put<bool>("useCallback",true);
    env.fixRotXZ();
  } else {
    boost::property_tree::ptree tree;
    createFishSwim(tree,5,true,false);
    tree.put<bool>("GPUEval",true);
    tree.put<bool>("cosOnly",true);
    tree.put<sizeType>("DMPBasis",1);
    parseProps(argc,argv,tree);

    if(!exists(NAME+".dat")) {
      env.reset(tree);
      env.Environment::write(NAME+".dat");
    } else {
      env.Environment::read(NAME+".dat");
    }
    env.getPolicy()._net->getLayer<DMPLayer>(0)->wBiasOptimizable()=false;
    env.sol().getBody()._tree.put<bool>("MTCubature",true);
    env.sol()._tree.put<bool>("useCallback",false);
    env.fixRotXZ();

    Evolution learner;
    parseProps(argc,argv,learner._pt);
    learner._pt.put<sizeType>("maxIter",2E2);
    learner._pt.put<sizeType>("trajLen",2E2);
    learner._pt.put<sizeType>("maxTraj",1E2);
    learner._pt.put<string>("workspace",string("./")+NAME);
    learner._cb.reset(new Callback<scalarD,Kernel<scalarD> >());
    learner.learn(env);
    env.Environment::write(NAME+"Learned.dat");
  }

  TrainingData dat;
  dat.randomSample(1,1,200,200,env,NULL,NAME+"Learned");
  return 0;
}
