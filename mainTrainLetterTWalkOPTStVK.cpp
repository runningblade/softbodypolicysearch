#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("letterTWalkOPTStVK")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  createLetterTWalk(tree,10,false);
  tree.put<bool>("useReducedStVKModel",true);
  parseProps(argc,argv,tree);

  FEMEnvironment env;
  if(!exists(NAME+".dat")) {
    env.reset(tree);
    env.Environment::write(NAME+".dat");
  } else {
    env.Environment::read(NAME+".dat");
  }
  env.sol().getBody()._tree.put<bool>("MTCubature",false);
  env.sol()._tree.put<bool>("useCallback",false);

  FEMDDPSolver learner(env);
  learner._tree.put<sizeType>("nrFrm",2E2);
  learner._tree.put<sizeType>("nrIterInner",6E3);
  learner._tree.put<scalar>("regPhysicsCoef",5E4);
  learner._tree.put<scalar>("muConicShuffleAvoidance",1);
  learner._tree.put<scalar>("regConicShuffleAvoidance",1E4);
  learner._tree.put<scalar>("regContactActivationCoef",1E4);
  learner._tree.put<scalar>("regCoef",1E-3f);
  learner._tree.put<scalar>("k1",1);
  learner._tree.put<bool>("frequentContactUpdate",true);
  learner._tree.put<bool>("handleSelfCollision",true);
  learner._tree.put<bool>("periodicPosition",true);
  learner._tree.put<bool>("periodicRotation",true);
  parseProps(argc,argv,learner._tree);
  learner.resetParam(learner._tree.get<sizeType>("nrIterInner"),1,learner._tree.get<sizeType>("nrFrm"));
  learner.resetEps(1E-5f,1E-5f,0);

  //learner.debugCIOSQPEnergy(true,true,true);
  //learner.debugCIOLMEnergy(true,true);
  //learner.debugCIOForce(true,true);

  learner.optimizeCIOPolicy(true,true);
  learner.writeStatesVTK(NAME+"Learned");
  env.Environment::write(NAME+"Learned.dat");
  writePtreeAscii(learner._tree,"optTrajs.xml");
  return 0;
}
