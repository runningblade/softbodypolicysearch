#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("articulatedSwimOPT")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  createArticulatedSwim(tree,4,true,false);
  parseProps(argc,argv,tree);

  FEMEnvironment env;
  if(!exists(NAME+".dat")) {
    env.reset(tree);
    env.Environment::write(NAME+".dat");
  } else {
    env.Environment::read(NAME+".dat");
  }
  env.sol().getBody()._tree.put<bool>("MTCubature",false);
  env.sol()._tree.put<bool>("useCallback",false);

  FEMDDPSolver learner(env);
  learner._tree.put<sizeType>("nrFrm",2E2);
  learner._tree.put<sizeType>("nrIterInner",6E3);
  learner._tree.put<scalar>("regPhysicsCoef",1E5);
  learner._tree.put<scalar>("regContactActivationCoef",0);
  learner._tree.put<scalar>("regCoef",1E-9f);
  learner._tree.put<bool>("periodicDeform",false);
  //learner._tree.put<scalar>("randomizedScale",0.1f);
  learner.resetParam(learner._tree.get<sizeType>("nrIterInner"),1,learner._tree.get<sizeType>("nrFrm"));
  learner.resetEps(0,1E-5f,0);
  learner.debugCIOLMEnergy(true,true);
  learner.optimizeCIOPolicy(true,true);
  //learner.optimizeCIOLM(true);
  learner.writeStatesVTK(NAME+"Learned");
  env.Environment::write(NAME+"Learned.dat");
  return 0;
}
