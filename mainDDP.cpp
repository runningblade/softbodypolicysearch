#include <SoftBodyController/DDPSolver.h>
#include <Dynamics/FEMUtils.h>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  DDPSolverDubin ddp;
  ddp.createExample();
//#define DEBUG_DDP
#ifdef DEBUG_DDP
  ddp.debugFx(false);
  //ddp.debugObj();
  ddp.debugGradient();
  ddp.debugILQGOptQP();
  ddp.debugILQGOpt();
  ddp.debugILQG();
  exit(-1);
#endif

  ddp._tree.put<sizeType>("maxIterILQG",10);
  ddp._tree.put<bool>("optBasedILQG",true);
  for(sizeType i=0;i<500;i++) {
    INFOV("Frame %ld",i)
    //ddp.solveTrajOpt();
    ddp.solveILQG();
  }
  ddp.writeStatesVTK("./states");
  ddp.writeEnvVTK("./states/environment.vtk");
  writePtreeAscii(ddp._tree,"./states/tree.xml");
  return 0;
}
