#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("acrobotDQL")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  tree.put<bool>("useNeural",false);
  tree.put<bool>("useNeuralQ",true);
  tree.put<bool>("useNeuralProbQ",false);
  tree.put<bool>("useSimpleQ",false);
  tree.put<bool>("useStablizedSimpleQ",false);
  tree.put<sizeType>("downsample",1);
  parseProps(argc,argv,tree);

  AcrobotEnvironment env;
  MPIInterface::createMPI(argc,argv);
  TrainingData::mpiSlaveRun(env);
  env.reset(tree);
  env.Environment::write(NAME+".dat");

  QLearning learner(env);
  learner._tree.put<scalar>("learningRate",0.01f);
  learner._tree.put<sizeType>("trajLen",10000);
  learner._tree.put<bool>("useWholeMemoryNewton",false);
  learner._tree.put<bool>("useWholeMemoryRMSProp",true);
  learner._tree.put<bool>("useWholeMemoryLBFGS",false);
  learner._tree.put<sizeType>("maxIter",10000);
  parseProps(argc,argv,learner._tree);
  learner.learn();

  TrainingData dat;
  dat.randomSample(5,5,1000,1000,env,NULL,NAME+"Learned");
  TrainingData::mpiExit();
  return 0;
}
