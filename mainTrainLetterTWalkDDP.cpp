#include "mainUtils.h"
#include <SoftBodyController/MLUtil.h>

USE_PRJ_NAMESPACE

#define G -9.81f
#define NAME string("letterTWalkDDP")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  createLetterTWalk2D(tree,5,false);
  parseProps(argc,argv,tree);
  tree.put<scalar>("dt",0.01f);
  tree.put<scalar>("actionScale",1E3f);

  FEMEnvironment env;
  if(!exists(NAME+".dat")) {
    env.reset(tree);
    env.Environment::write(NAME+".dat");
  } else {
    env.Environment::read(NAME+".dat");
  }

  FEMDDPSolver ddp(env);
  ddp._tree.put<sizeType>("frm",0);
  ddp._tree.put<scalar>("regCoef",1E-3f);
  ddp.resetEps(1E-6f,1E-6f,1E-6f);
  ddp.resetParam(1E10,10,10);
  env.sol()._tree.put<scalar>("dt",0.01f);
  env.sol()._tree.put<bool>("positionAdjust",true);
  env.sol()._tree.put<bool>("advanceImplicit",false);
  env.sol()._tree.put<bool>("updateColl",true);
  env.sol()._tree.put<scalar>("eps",1E-6f);
  env.sol()._tree.put<scalar>("deltaDFDX",1E-2f);
  env.sol()._tree.put<scalar>("deltaDFDU",1E-2f);
  //assignVec(Cold::Random(env.sol().getMCalc().size()*2),"x",0,ddp._tree);
  //assignVec(Cold::Random(env.sol().getMCalc().size()-6),"u",0,ddp._tree);
  //ddp.debugGradient();
  //ddp.debugFx(true);

  //solve
  ddp._tree.put<bool>("optBasedILQG",false);
  for(sizeType i=0;i<200;i++) {
    INFOV("Frame %ld",i)
    ddp.solveILQG();
  }
  ddp.writeStatesVTK("./states");
  ddp.writeEnvVTK("./states/environment.vtk");
  return 0;
}
