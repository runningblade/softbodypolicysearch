#include "mainUtils.h"
#include <SoftBodyController/MLUtil.h>
#include <SoftBodyController/FEMAugmentedBody.h>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  ASSERT_MSG(argc >= 2,"Usage: [exe] [path] [options]")
  //initialize
  FEMEnvironment env;
  string path(argv[1]);
  FEMDDPSolver ddp(env);
  boost::filesystem::ifstream is(path+"/CIOResult.dat",ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  ddp.read(is,dat.get());
  //parameter treaking
  boost::property_tree::ptree tree;
  parseProps(argc,argv,env.sol()._tree);
  readPtreeAscii(tree,path+"/tree.xml");

  Vec3 ctr;
  ctr[0]=env.sol()._tree.get<scalar>("recenterX",0);
  ctr[1]=env.sol()._tree.get<scalar>("recenterY",0);
  ctr[2]=env.sol()._tree.get<scalar>("recenterZ",0);
  cout << env.sol().getCons().size() << endl;
  //env.resetContacts();

  //test augmented mesh
  {
    string pathEmbed="./Models/deformedDino.obj";
    FEMAugmentedBody body(env.sol(),"./Models/deformedDino.abq",&pathEmbed,ctr,0.05f);
    //body.debugGradient();
    recreate("./meshAug");
    body._keyframe=env.sol()._tree.get<bool>("useKeyframe",true);
    sizeType frm=tree.get<sizeType>("horizon");
    for(sizeType i=0; i<frm; i++) {
      INFOV("%ld of %ld!",i,frm)
      FEMBody& b=env.sol().getBody();
      Cold x=parseVec<Cold>("x",i,tree);
      ASSERT(x.size() == b._system->size()*2)
      x=x.segment(x.size()/2,x.size()/2).eval();
      b._system->setPos(x);
      //solve
      body.solveAug();
      //assign/write
      ObjMesh mesh;
      body.getMeshAug().getB(0).writeObjEmbedded(mesh);
      body.getMeshAug().writeVTK("./meshAug/mesh"+boost::lexical_cast<string>(i)+".vtk");
      mesh.writePov("./meshAug/mesh"+boost::lexical_cast<string>(i)+".pov",false);
      mesh.writePov("./meshAug/mesh"+boost::lexical_cast<string>(i)+".povn",true);
    }
  }

  //generate keyframe
  {
    string pathEmbed="./Models/deformedDino.obj";
    FEMAugmentedBody body(env.sol(),"./Models/deformedDino.abq",&pathEmbed,ctr,0.05f);
    //body.debugGradient();
    recreate("./meshKey");
    body._keyframe=env.sol()._tree.get<bool>("useKeyframe",true);
    sizeType frm=tree.get<sizeType>("horizon");
    for(sizeType i=0; i<frm; i++) {
      INFOV("%ld of %ld!",i,frm)
      FEMBody& b=env.sol().getBody();
      Cold x=parseVec<Cold>("x",i,tree);
      ASSERT(x.size() == b._system->size()*2)
      x=x.segment(x.size()/2,x.size()/2).eval();
      x.segment<6>(x.size()-6).setZero();
      b._system->setPos(x);
      //solve
      body.solveAug();
      //assign/write
      ObjMesh mesh;
      body.getMeshAug().getB(0).writeObjEmbedded(mesh);
      body.getMeshAug().writeVTK("./meshKey/mesh"+boost::lexical_cast<string>(i)+".vtk");
      mesh.writePov("./meshKey/mesh"+boost::lexical_cast<string>(i)+".pov",false);
      mesh.writePov("./meshKey/mesh"+boost::lexical_cast<string>(i)+".povn",true);
    }
  }
  return 0;
}
