#include "mainUtils.h"
#include <SoftBodyController/DMPLayer.h>
#include <SoftBodyController/FEMDDPPlotter.h>

USE_PRJ_NAMESPACE

#define NAME string("articulatedSwimCMAES")
int main(int argc,char** argv)
{
  FEMEnvironment env;
  if(exists(NAME+"Learned.dat")) {
    env.Environment::read(NAME+"Learned.dat");
    env.sol()._tree.put<bool>("useCallback",false);
    env.removeGPUSupport();
  } else {
    boost::property_tree::ptree tree;
    createArticulatedSwim(tree,4,true,false);
    tree.put<bool>("GPUEval",true);
    tree.put<bool>("cosOnly",true);
    tree.put<sizeType>("DMPBasis",1);
    parseProps(argc,argv,tree);

    if(!exists(NAME+".dat")) {
      env.reset(tree);
      env.Environment::write(NAME+".dat");
    } else {
      env.Environment::read(NAME+".dat");
    }
    env.getPolicy()._net->getLayer<DMPLayer>(0)->wBiasOptimizable()=false;
    env.getPolicy()._net->_tree.put<scalar>("maxWeight",1000);
    env.sol().getBody()._tree.put<bool>("MTCubature",true);
    env.sol()._tree.put<bool>("useCallback",false);

    Evolution learner;
    parseProps(argc,argv,learner._pt);
    learner._pt.put<sizeType>("maxIter",2E2);
    learner._pt.put<sizeType>("trajLen",2E2);
    learner._pt.put<sizeType>("maxTraj",1E2);
    learner._pt.put<string>("workspace",string("./")+NAME);
    learner._cb.reset(new Callback<scalarD,Kernel<scalarD> >());
    learner.learn(env);
    env.Environment::write(NAME+"Learned.dat");
  }

  Matd states,actions;
  TrainingData dat;
#ifdef VISUALIZE
  dat.clear();
  env.sol()._tree.put<bool>("advanceImplicit",true);
  dat.randomSample(1,1,500,500,env,NULL,NAME+"LearnedLM");

  dat.clear();
  env.sol()._tree.put<bool>("advanceImplicit",false);
  dat.randomSample(1,1,500,500,env,NULL,NAME+"LearnedLMLCP");
#else
  boost::filesystem::ofstream os("./data.txt",ios::binary);

  dat.clear();
  env.sol()._tree.put<bool>("advanceImplicit",true);
  dat.randomSample(1,1,500,500,env,NULL);
  dat.assemble(states,actions);
  writeMaxima(os,"swimLM",NULL,states.row(states.rows()-5));

  dat.clear();
  env.sol()._tree.put<bool>("advanceImplicit",false);
  dat.randomSample(1,1,500,500,env,NULL);
  dat.assemble(states,actions);
  writeMaxima(os,"swimLMLCP",NULL,states.row(states.rows()-5));
#endif
  return 0;
}
