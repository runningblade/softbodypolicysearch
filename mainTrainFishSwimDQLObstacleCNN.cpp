#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("fishSwimMTOPT")
int main(int argc,char** argv)
{
  OmpSettings::getOmpSettingsNonConst().setNrThreads((int)omp_get_num_procs());
  boost::property_tree::ptree pt;
  string addParam=parseProps(argc,argv,pt);

  //read learned policy
  FEMEnvironmentDQNObstacleDynamic envDQL;
  ASSERT(exists(NAME+addParam+"LearnedDQNObstacleTwo.dat"))
  envDQL.Environment::read(NAME+addParam+"LearnedDQNObstacleTwo.dat");

  //train CNN
  if(!exists(NAME+addParam+"LearnedCNN.dat")) {
    CNNObstacleFeature cnn(envDQL,Vec3d(1,0,0));
    cnn.getNet()._tree.put<sizeType>("maxIterations",1000);
    cnn.trainCNNFeature(envDQL,10000);
    cnn.write(NAME+addParam+"LearnedCNN.dat");
  } else {
    //CNNObstacleFeature cnn;
    //cnn.read(NAME+addParam+"LearnedCNN.dat");
    //cnn.trainCNNFeature(envDQL,100,false,true);
    RandEngine::seedTime();
    envDQL.setUseFit(false);
    envDQL.setUseFixTarget(false);
    envDQL.addEmbeddedMesh("./Models/GoldenFishSmall.obj");
    envDQL.setupCNNFeature(NAME+addParam+"LearnedCNN.dat");
    envDQL.sol()._tree.put<bool>("stopOnCollision",true);
    envDQL.sol()._tree.put<bool>("writePov",true);
    envDQL.setUsePeriodicTarget(10,1);
    envDQL.getPolicy().setEps(0);
    envDQL.debugTrajectory("./trajs",1,100);
  }
  return 0;
}
