#include "mainUtils.h"
#include <CommonFile/geom/StaticGeom.h>
#include <SoftBodyController/MLUtil.h>

USE_PRJ_NAMESPACE

#define G -9.81f
#define NAME string("articulatedWalkDDP")
int main(int argc,char** argv)
{
  scalar dt=0.05f;
  boost::property_tree::ptree tree;
  createArticulatedWalker2D(tree);
  parseProps(argc,argv,tree);
  tree.put<scalar>("dt",dt);
  tree.put<scalar>("collK",50);
  tree.put<scalar>("collKCIO",500);
  tree.put<scalar>("actionScale",1E9f);

  FEMEnvironment env;
  if(!exists(NAME+".dat")) {
    env.reset(tree);
    env.Environment::write(NAME+".dat");
  } else {
    env.Environment::read(NAME+".dat");
  }

  FEMDDPSolver ddp(env);
  ddp._tree.put<sizeType>("frm",0);
  ddp._tree.put<scalar>("regCoef",1E-8f);
  ddp.resetEps(1E-6f,1E-6f,1E-6f);
  ddp.resetParam(1E10,10,20);
  env.sol()._tree.put<scalar>("dt",dt);
  env.sol()._tree.put<bool>("positionAdjust",false);
  env.sol()._tree.put<bool>("advanceImplicit",true);
  env.sol()._tree.put<bool>("updateColl",true);
  env.sol()._tree.put<scalar>("eps",1E-9f);
  env.sol()._tree.put<scalar>("deltaDFDX",1E-5f);
  env.sol()._tree.put<scalar>("deltaDFDU",1E-5f);
  //assignVec(Cold::Random(env.sol().getMCalc().size()*2),"x",0,ddp._tree);
  //assignVec(Cold::Random(env.sol().getMCalc().size()-6),"u",0,ddp._tree);
  //ddp.debugGradient();
  //ddp.debugFx(true);

//#define SIMULATE
#ifdef SIMULATE
  recreate("./simulate");
  //Mat4d T=Mat4d::Identity();
  //T.block<3,3>(0,0)=makeRotation<scalarD>(Vec3d(0,0,-M_PI/2));
  //T.block<3,1>(0,3)=Vec3d(0,1,0);
  //env.sol().initializeTrans(T);
  //env.sol().initializeVel(Vec3(1,0,0),0.05f);
  env.sol()._geom->writeVTK("./geom.vtk");
  for(sizeType i=0; i<200; i++) {
    INFOV("Frm: %ld",i)
    cout << env.sol()._x.x() << endl;
    env.sol()._tree.put<bool>("useCallback",true);
    env.sol().advanceImplicit(0.05f);
    env.sol().getMesh().writeVTK("./simulate/frm"+boost::lexical_cast<string>(i)+".vtk");
  }
  exit(-1);
#endif

  //solve
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("./DDPInit.dat",ios::binary);
    ddp.write(os,dat.get());
  }
  //ddp._tree.put<bool>("qpBasedILQG",true);
  ddp._tree.put<bool>("optBasedILQG",true);
  for(sizeType i=0; i*dt < 10; i++) {
    INFOV("Frame %ld",i)
    ddp.solveILQG();
  }
  ddp.writeStatesVTK("./states");
  ddp.writeEnvVTK("./states/environment.vtk");
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("./DDPResult.dat",ios::binary);
    ddp.write(os,dat.get());
  }
  return 0;
}
