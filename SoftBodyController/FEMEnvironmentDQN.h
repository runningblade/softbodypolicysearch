#ifndef FEM_ENVIRONMENT_DQN_H
#define FEM_ENVIRONMENT_DQN_H

#include "FEMEnvironment.h"
#include "DMPNeuralNet.h"

PRJ_BEGIN

class FEMEnvironmentDQN : public FEMEnvironment
{
public:
  FEMEnvironmentDQN();
  FEMEnvironmentDQN(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt=NULL);
  virtual void reset(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt=NULL);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void debugFitTrajectory(const string& path,sizeType horizon=100);
  virtual void debugDMPTrajectory(const string& path,sizeType horizon=100);
  virtual void debugTrajectory(const string& path,sizeType nrTraj,sizeType horizon=100);
  virtual void writeEnvVTK(const string& path,const Mat4Xd* obstacle=NULL,bool dynamic=false) const;
  virtual void writeEnvPov(const string& path,const Mat4Xd* obstacle=NULL,bool dynamic=false) const;
  virtual void writeObstacleVTKOrPov(const string& path,const Mat4Xd& obstacle,bool pov) const;
  virtual void writeLocusVTK(const string& path) const;
  virtual void writeLocusPov(const string& path) const;
  virtual void writeStateVTK(const string& path);
  virtual void writeStatePov(const string& path);
  virtual sizeType nrTraj() const;
  virtual void fitTrajectory(sizeType horizon=100,bool profile=false);
  virtual void fitTrajectoryRandom(sizeType horizon=100);
  virtual void setUsePeriodicTarget(sizeType s0,sizeType s1);
  virtual void setUseFixTarget(bool fix);
  virtual void setUseFit(bool useFit);
  virtual Vec3d getIgnoreDir() const;
  virtual NeuralNetPolicy& getPolicy();
  virtual Vec transfer(sizeType i,const Vec& A);
  virtual Vec transferFit(sizeType i,const Vec& A);
  virtual scalarD getCost(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const override;
  virtual Vec sampleS0() override;
  //for playing recorded trajectory
  scalarD distMetric(const vector<GradientInfo>& traj,sizeType x,sizeType y) const;
  void addContactInfo(const vector<GradientInfo>& traj,vector<CIOContactInfo>& infos,vector<Vec,Eigen::aligned_allocator<Vec> >& dss);
  vector<GradientInfo> addTaskDataInner(const boost::property_tree::ptree& pt);
  scalarD defoMetric(const GradientInfo& x,const GradientInfo& y) const;
  sizeType findFrm(const vector<GradientInfo>& traj,const Vec& y) const;
  Mat4d AToBR(const Vec& a,const Vec& b) const;
  Mat4d AToBR(const GradientInfo& a,const GradientInfo& b) const;
  void applyT(const Mat4d& T,Vec& x) const;
  Vec applyTR(const Mat4d& T,const Vec& x) const;
  Mat4d computeT(const GradientInfo& x) const;
  Mat3d computeS(const Vec& A,const Vec& B) const;
  void debugAToB();
protected:
  FEMEnvironmentDQN(const string& name);
  virtual void cycleTarget();
  virtual void pickTarget();
  virtual void syncTarget();
  virtual sizeType getInterval() const;
  virtual Vec extractStateWithFeature() const;
  virtual void updateFeature(Vec& ret) const;
  virtual sizeType nrFeature() const;
  //data
  vector<Mat4d,Eigen::aligned_allocator<Mat4d> > _fit;
  DMPNeuralNet _DMPMT;
  string _perFramePath;
  sizeType _perFrameId;
  bool _useFit,_fixTarget;
  //recorded trajectory
  Mat3Xd _LCtr;
  Matd _defoMetricSqrt,_AToBRTpl[9];
  vector<vector<GradientInfo> > _trajs;
  //goal position
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > _targets;
  Vec3d _target;
  scalarD _time;
};

PRJ_END

#endif
