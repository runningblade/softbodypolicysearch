#include "TRPO.h"
#include "NeuralNetQPolicy.h"
#include <Dynamics/FEMUtils.h>
#include <CommonFile/solvers/PMinresQLP.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//Polynomial Baseline function
Polynomial::Polynomial(sizeType order,sizeType nrS):_order(order),_nrS(nrS) {}
scalarD Polynomial::operator()(const Vec& x)
{
  if(_coef.size() != inputs())
    return 0;
  else return computeFeature(x).dot(_coef);
}
void Polynomial::fit(const vector<Transition>& memoryBank)
{
  sizeType nr=(sizeType)memoryBank.size();
  ParallelMatVec<Matd> LHS(inputs(),inputs());
  ParallelMatVec<Vec> RHS(inputs());
  Vec feature;
  OMP_PARALLEL_FOR_I(OMP_PRI(feature))
  for(sizeType i=0; i<nr; i++) {
    feature=computeFeature(memoryBank[i]._state);
    LHS+=feature*feature.transpose();
    RHS+=memoryBank[i]._return*feature;
  }
  _coef=LHS.get().fullPivLu().solve(RHS.get());
}
Polynomial::Vec Polynomial::computeFeature(const Vec& x) const
{
  Vec ret=Vec::Zero(inputs()),tmp=Vec::Ones(x.size());
  for(sizeType i=0; i<=_order; i++) {
    ret.segment(i*_nrS,_nrS)=tmp;
    tmp.array()*=x.array();
  }
  return ret;
}
int Polynomial::inputs() const
{
  return (_order+1)*_nrS;
}
//this implement the TrustRegionPolicyOptimization algorithm
TRPO::TRPO(Environment &env):_env(env)
{
  seed(0);
}
TRPO::~TRPO() {}
void TRPO::learn()
{
  typedef Callback<scalarD,Kernel<scalarD> > CB;
  typedef NoCallback<scalarD,Kernel<scalarD> > NCB;
  typedef PMINRESSolverQLP<scalarD,Kernel<scalarD>,NoPreconSolver<scalarD> > SOLVER;
  //initialize
  sizeType maxIter=_tree.get<sizeType>("maxIter",1E2);
  sizeType maxTraj=_tree.get<sizeType>("maxTraj",1E2);
  sizeType trajLen=_tree.get<sizeType>("trajLen",1E2);
  sizeType repInterval=_tree.get<sizeType>("reportSampleInterval",0);
  bool debugRSampler=_tree.get<bool>("debugUseRandomSampler",false);
  bool debugTRPOIter=_tree.get<bool>("debugTRPOIter",false);
  RandEngine::useDeterministic(); //COMMON RANDOM NUMBER
  RandEngine::clearAllRecord();  //COMMON RANDOM NUMBER
  RandEngine::seed(_seed);  //COMMON RANDOM NUMBER

  SOLVER sol;
  if(_tree.get<bool>("useCallback",false))
    sol.setCallback(boost::shared_ptr<CB>(new CB()));
  else sol.setCallback(boost::shared_ptr<CB>(new NCB()));
  sol.setSolverParameters(1E-6f,1E4);
  Vec G,invAG,param;

  //random sampler for debug
  Vec S0=_env.sampleS0();
  Vec A0=_env.getPolicy().getAction(S0,NULL,NULL);
  RandomTrajectorySampler rsampler(S0.size(),A0.size());

  //baseline
  _baseline.reset(new Polynomial(_tree.get<sizeType>("orderBaselinePolynomial",2),_env.sampleS0().size()));

  //main loop
  if(debugTRPOIter)
    recreate("./TRPOIterProfile/");
  for(sizeType it=0; it<maxIter; it++) {
    //add trajectory
    RandEngine::seed(_seed);  //COMMON RANDOM NUMBER
    _trajs._trajs.clear();
    _trajs.randomSample(maxTraj,maxTraj,trajLen,trajLen,
                        debugRSampler ? (TrajectorySampler&)rsampler : (TrajectorySampler&)_env,
                        NULL,"",repInterval);
    INFOV("Running %ld of %ld iteration!",it,maxIter)
    //debug trajectory
    if(debugTRPOIter) {
      _env.getPolicy().setDeterministic(true);
      TrainingData dataTest;
      dataTest.randomSample(1,1,trajLen,trajLen,
                            debugRSampler ? (TrajectorySampler&)rsampler : (TrajectorySampler&)_env,
                            NULL,"./TRPOIterProfile/Iter"+boost::lexical_cast<string>(it),repInterval);
      _env.getPolicy().setDeterministic(false);
    }
    //collect
    _memoryBank.clear();
    _offMemoryTraj.clear();
    for(sizeType t=0; t<(sizeType)_trajs._trajs.size(); t++)
      collectTransition(_trajs._trajs[t].get());
    profileMemory();
    //fit baseline
    _baseline->fit(_memoryBank);
    //optimize: compute surrogate gradient
    param=_env.getPolicy().toVec();
    surrogateGradient(param,param,&G);
    //optimize: search direction
    _env.getPolicy().fromVec(param);
    _hessKL=_env.getPolicy().getHessKLDistance(getStates());
    sol.setKrylovMatrix(_hessKL);
    sol.resize(_hessKL->n());
    if(sol.solve(G,invAG) == SOLVER::SUCCESSFUL) {
      //optimize: line search
      param=lineSearch(param,invAG,it,maxIter);
      _env.getPolicy().fromVec(param);
    } else {
      WARNING("Solver failed, restarting!")
    }
  }
}
void TRPO::profileMemory() const
{
  Vec rets=Vec::Zero(_offMemoryTraj.size());
  for(sizeType i=0; i<rets.size(); i++)
    rets[i]=_memoryBank[_offMemoryTraj[i]]._return;
  INFOV("MinReturn: %f MaxReturn: %f AvgReturn: %f",
        rets.minCoeff(),rets.maxCoeff(),rets.sum()/(scalarD)rets.size())
}
//constraint (mean kl-distance)
void TRPO::debugKLDistance()
{
#define NR_TEST 100
#define DELTA 1E-9f
  scalarD loss1,loss2;
  Vec paramRef=_env.getPolicy().toVec();
  for(sizeType t=0; t<NR_TEST; t++) {
    randomMemory();
    Vec paramNew=Vec::Random(_env.getPolicy().nrParam());
    Vec paramOld=Vec::Random(_env.getPolicy().nrParam());
    Vec delta=Vec::Random(_env.getPolicy().nrParam());
    Vec grad1,grad2;
    paramNew.array()*=paramRef.array();
    paramOld.array()*=paramRef.array();

    _env.getPolicy().fromVec(paramNew);
    loss1=_env.getPolicy().getDiffKLDistance(getStates(),paramOld,&grad1);
    _env.getPolicy().fromVec(paramNew+delta*DELTA);
    loss2=_env.getPolicy().getDiffKLDistance(getStates(),paramOld,&grad2);
    INFOV("Gradient: %f Err: %f",grad1.dot(delta),grad1.dot(delta)-(loss2-loss1)/DELTA)
  }
  for(sizeType t=0; t<NR_TEST; t++) {
    randomMemory();
    Vec param=Vec::Random(_env.getPolicy().nrParam());
    Vec delta=Vec::Random(_env.getPolicy().nrParam());
    Vec delta2=Vec::Random(_env.getPolicy().nrParam());
    Vec grad1,grad2,HDelta,HDelta2;
    param.array()*=paramRef.array();

    _env.getPolicy().fromVec(param);
    _env.getPolicy().getDiffKLDistance(getStates(),param,&grad1);
    _env.getPolicy().getHessKLDistance(getStates())->multiply(delta,HDelta);
    _env.getPolicy().getHessKLDistance(getStates())->multiply(delta2,HDelta2);
    INFOV("HessianSym: %f Err: %f",HDelta.dot(delta2),HDelta.dot(delta2)-HDelta2.dot(delta))

    _env.getPolicy().fromVec(param+delta*DELTA);
    _env.getPolicy().getDiffKLDistance(getStates(),param,&grad2);
    INFOV("Hessian: %f Err: %f",HDelta.norm(),(HDelta-(grad2-grad1)/DELTA).norm())
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
}
//surrogate loss
void TRPO::seed(sizeType seed)
{
  INFOV("Using seed: %ld!",seed)
  _seed=seed;
}
scalarD TRPO::surrogateGradient(const Vec& param,const Vec& paramOld,Vec* grad)
{
  sizeType nrM=(sizeType)_memoryBank.size();
  //compute
  Vec possLogOld=Vec::Zero(nrM);
  Vec possLogNew=Vec::Zero(nrM),coef;
  Matd states=getStates(),actions=getActions();
  //old
  _env.getPolicy().fromVec(paramOld);
  _env.getPolicy().getAction(states,&possLogOld,NULL,NULL,&actions);
  //new
  _env.getPolicy().fromVec(param);
  _env.getPolicy().getAction(states,&possLogNew,NULL,NULL,&actions);
  coef=((possLogNew-possLogOld).array().exp()*getAdvantages().array()).matrix()/(scalarD)nrM;
  if(grad)
    _env.getPolicy().getAction(states,NULL,grad,&coef,&actions);
  return coef.sum();
}
TRPO::Vec TRPO::lineSearch(const Vec& paramOld,const Vec& D,sizeType tid,sizeType maxIter)
{
  scalar KLThres=_tree.get<scalar>("maxKLDivergence",0.01f);
  scalar backtrace=_tree.get<scalar>("backtrace",0.8f);
  if(_tree.get_optional<scalar>("minKLDivergence")) {
    KLThres=interp1D(KLThres,_tree.get<scalar>("minKLDivergence"),(scalar)tid/(scalar)(maxIter-1));
  }

  //initialize
  Vec HD,param;
  _hessKL->multiply(D,HD);
  scalarD stepSz=sqrt(2*KLThres/(HD.dot(D)+1E-8f)),KL,loss,loss0;

  //satisfy constraint
  Matd states=getStates();
  while(true) {
    param=paramOld+stepSz*D;
    _env.getPolicy().fromVec(param);
    KL=_env.getPolicy().getDiffKLDistance(states,paramOld,NULL);
    if(KL < KLThres)
      break;
    else stepSz*=backtrace;
  }

  //increase loss
  loss0=surrogateGradient(param,param,NULL);
  while(true) {
    param=paramOld+stepSz*D;
    loss=surrogateGradient(param,paramOld,NULL);
    if(loss > loss0)
      break;
    else stepSz*=backtrace;
  }

  INFOV("KL: %f, KLThres: %f LossOld: %f LossNew: %f",KL,KLThres,loss0,loss)
  return param;
}
void TRPO::debugSurrogateGradient(scalar perturb)
{
#define NR_TEST 100
#define DELTA 1E-9f
  scalarD loss,loss2;
  Vec paramRef=_env.getPolicy().toVec();
  for(sizeType t=0; t<NR_TEST; t++) {
    randomMemory();
    Vec paramOld=Vec::Random(_env.getPolicy().nrParam());
    paramOld.array()*=paramRef.array();

    _env.getPolicy().fromVec(paramOld);
    Vec param=paramOld+Vec::Random(paramOld.size())*perturb;
    Vec delta=Vec::Random(param.size()),grad;
    loss=surrogateGradient(param,paramOld,&grad);
    loss2=surrogateGradient(param+delta*DELTA,paramOld,NULL);
    INFOV("Gradient: %f Err: %f",grad.dot(delta),grad.dot(delta)-(loss2-loss)/DELTA)
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
}
//helper
Matd TRPO::getStates() const
{
  sizeType nrS=_memoryBank[0]._state.size();
  sizeType nrM=(sizeType)_memoryBank.size();
  Matd ret=Matd::Zero(nrS,nrM);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrM; i++)
    ret.col(i)=_memoryBank[i]._state;
  return ret;
}
Matd TRPO::getActions() const
{
  sizeType nrA=_memoryBank[0]._action.size();
  sizeType nrM=(sizeType)_memoryBank.size();
  Matd ret=Matd::Zero(nrA,nrM);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrM; i++)
    ret.col(i)=_memoryBank[i]._action;
  return ret;
}
TRPO::Vec TRPO::getAdvantages() const
{
  sizeType nrM=(sizeType)_memoryBank.size();
  Vec ret=Vec::Zero(nrM);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrM; i++)
    ret[i]=_memoryBank[i]._advantage;
  return ret;
}
void TRPO::collectTransition(const TrajectoryState* ss)
{
  //initialize
  Transition trans;
  trans._baseline=_baseline->operator()(ss->_data);
  scalarD discountCoef=_tree.get<scalar>("discountCoef",0.99f);
  scalarD gaeLambdaCoef=_tree.get<scalar>("gaeLambdaCoef",1.0f);

  //fill state,action,reward,baseline
  sizeType beg=(sizeType)_memoryBank.size();
  _offMemoryTraj.push_back(beg);
  while(!ss->_actions.empty()) {
    trans._state=ss->_data;
    trans._action=ss->_actions[0]._data;
    trans._nextState=ss->_actions[0]._next._data;
    trans._reward=trans._return=ss->_actions[0]._reward;
    trans._nextBaseline=_baseline->operator()(trans._nextState);
    trans._delta=trans._advantage=trans._reward+discountCoef*trans._nextBaseline-trans._baseline;

    _memoryBank.push_back(trans);
    trans._baseline=trans._nextBaseline;
    ss=&(ss->_actions[0]._next);
  }
  sizeType end=(sizeType)_memoryBank.size();
  //fill return,advantage
  for(sizeType i=end-2; i>=beg; i--) {
    _memoryBank[i]._return=_memoryBank[i+1]._return*discountCoef+_memoryBank[i]._return;
    _memoryBank[i]._advantage=_memoryBank[i+1]._advantage*discountCoef*gaeLambdaCoef+_memoryBank[i]._advantage;
  }
}
void TRPO::randomMemory()
{
  sizeType nrM=200;
  sizeType nrS=_env.sampleS0().size();
  sizeType nrA=_env.sampleA(0,_env.sampleS0()).size();
  _memoryBank.resize(nrM);
  for(sizeType i=0; i<nrM; i++) {
    _memoryBank[i]._state.setRandom(nrS);
    try {
      NeuralNetQPolicy& p=dynamic_cast<NeuralNetQPolicy&>(_env.getPolicy());
      _memoryBank[i]._action.setConstant(1,p.getAction()[RandEngine::randI(0,p.getAction().size()-1)]);
    } catch(...) {
      _memoryBank[i]._action.setRandom(nrA);
    }
    _memoryBank[i]._advantage=RandEngine::randR01();
  }
}
