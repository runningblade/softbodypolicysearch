#include "DMPNeuralNet.h"
#include "DMPLayer.h"
#include "DMPLayerMT.h"
#include "OneLineCallback.h"
#include "NNBLEICInterface.h"
#include <Dynamics/FEMUtils.h>
#include <CommonFile/solvers/Minimizer.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

void subdivide(Matd& input)
{
  Matd ret=Matd::Zero(input.rows(),input.cols()*2-1);
  for(sizeType i=0; i<ret.cols(); i++)
    if(i%2 == 0)
      ret.col(i)=input.col(i/2);
    else ret.col(i)=(input.col(i/2)+input.col(i/2+1))/2;
  input=ret;
}
scalarD maxInterval(const DMPNeuralNet::Vec& input)
{
  DMPNeuralNet::Vec diff=input.segment(0,input.size()-1)-input.segment(1,input.size()-1);
  return diff.cwiseAbs().maxCoeff();
}
template <typename T,typename ALLOC>
void deleteElem(vector<T,ALLOC>& v,sizeType id)
{
  v[id]=v.back();
  v.pop_back();
}
//DMPNeuralNet
DMPNeuralNet::DMPNeuralNet()
  :NeuralNet(typeid(DMPNeuralNet).name()),_log(NULL) {}
DMPNeuralNet::DMPNeuralNet(sizeType nrAction,sizeType nrBasis,const boost::property_tree::ptree* pt)
  :NeuralNet(typeid(DMPNeuralNet).name()),_log(NULL)
{
  if(pt)
    _tree=*pt;
  sizeType lastIn=1;
  _foreBlocks.resize(1);
  _backBlocks.resize(1);
  _foreBlocks[0].push_back(Matd(lastIn,1));
  _backBlocks[0].push_back(Matd());
  sizeType K=_tree.get<sizeType>("multitask",1);
  bool rhythmic=_tree.get<bool>("rhythmic",true);
  bool cosOnly=_tree.get<bool>("cosOnly",false);
  if(rhythmic && K == 1 && cosOnly)
    lastIn=addLayer("CosDMP",boost::shared_ptr<NeuralLayer>(new CosDMPLayer(nrAction,nrBasis)),lastIn);
  else if(rhythmic && K > 1 && cosOnly)
    lastIn=addLayer("CosDMPMT",boost::shared_ptr<NeuralLayer>(new CosDMPLayerMT(nrAction,nrBasis,K)),lastIn);
  else if(rhythmic && K == 1)
    lastIn=addLayer("DMP",boost::shared_ptr<NeuralLayer>(new DMPLayer(nrAction,nrBasis)),lastIn);
  else if(rhythmic && K > 1)
    lastIn=addLayer("DMPMT",boost::shared_ptr<NeuralLayer>(new DMPLayerMT(nrAction,nrBasis,K)),lastIn);
  else if(!rhythmic && K == 1)
    lastIn=addLayer("NRDMP",boost::shared_ptr<NeuralLayer>(new NRDMPLayer(nrAction,nrBasis)),lastIn);
  else if(!rhythmic && K > 1)
    lastIn=addLayer("NRDMPMT",boost::shared_ptr<NeuralLayer>(new NRDMPLayerMT(nrAction,nrBasis,K)),lastIn);
  else {
    ASSERT_MSG(false,"Strange error!")
  }

  foreprop(NULL);
  backprop(NULL);
  copyMultithread();
}
bool DMPNeuralNet::read(istream& is,IOData* dat)
{
  NeuralNet::read(is,dat);
  return is.good();
}
bool DMPNeuralNet::write(ostream& os,IOData* dat) const
{
  NeuralNet::write(os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> DMPNeuralNet::copy() const
{
  return boost::shared_ptr<Serializable>(new DMPNeuralNet);
}
//main functionality with exhaustive search
void DMPNeuralNet::train(const UserData* dat)
{
  //{
  //  boost::shared_ptr<IOData> dat=getIOData();
  //  boost::filesystem::ofstream os("DMPNeuralNet.dat",ios::binary);
  //  write(os,dat.get());
  //}
  //exit(-1);
  bool rhythmic=_tree.get<bool>("rhythmic",true);
  if(rhythmic)
    trainRhythmic();
  else trainNonRhythmic();
}
void DMPNeuralNet::augment(sizeType nrDMPBasis)
{
  DMPLayer* layer=getLayer<DMPLayer>(0);
  if(layer) {
    layer->augment(nrDMPBasis,_tree);
    INFOV("We have %ld elements after augmentation!",layer->w().cols())
  }
}
void DMPNeuralNet::setupCandidates()
{
  Vec weights=toVec(false);
  DMPLayer* layer=getLayer<DMPLayer>(0);
  _tree.put<sizeType>("exhaustiveSearch.iter",0);
  scalarD periodFrom=_tree.get<scalar>("exhaustiveSearch.periodFrom",0.5f);
  scalarD periodTo=_tree.get<scalar>("exhaustiveSearch.periodTo",2.0f);
  scalarD periodInterval=_tree.get<scalar>("exhaustiveSearch.periodInterval",0.1f);
  INFOV("DMPNeuralNet: Period from %f to %f!",periodFrom,periodTo)
  for(scalarD period=periodFrom; period<=periodTo; period+=periodInterval) {
    fromVec(weights);
    layer->tau()=M_PI*2/period;
    layer->configOffline(_tree);
    _weights.push_back(toVec(false));
    _fxs.push_back(0);
    _taus.push_back(layer->tau());
    _last.push_back(0);
  }
  recreate("./exhaustiveSearch");
}
void DMPNeuralNet::trainNonRhythmic()
{
  augmentSolution();
  NeuralNet::train();
}
void DMPNeuralNet::trainRhythmic()
{
  //train from different initial conditions
  if(_weights.empty()) {
    WARNING("No candidate found, using default setting!")
    setupCandidates();
  }
  //augment solution
  augmentSolution();
  //clear task tag
  _tree.put<scalarD>("augment0",-1);
  resetOnline();
  //log
  sizeType it=_tree.get<sizeType>("exhaustiveSearch.iter",0);
  boost::property_tree::ptree& log=_tree.add_child("exhaustiveSearch.iter"+boost::lexical_cast<string>(it),boost::property_tree::ptree());
  _log=&log;
  //setup solver
  NNBLEICInterface sol(*this);
  sol.reset(nrParam(),false);
  setSolverParam(sol);
  setRhythmicBound(sol);
  _tree.put<scalar>("tangentMatching",0);
  //main loop
  saveDummyLayer();
  Callback<scalarD,Kernel<scalarD> > cbConsole;
  for(sizeType i=0; i<(sizeType)_weights.size(); i++) {
    //optimize
    sol.solve(_weights[i]);
    //cout << sol.getIterationsCount() << endl;
    fromVec(_weights[i]);
    _fxs[i]=sol.getFunctionValue();
    //fetch tau
    DMPLayer* layer=getLayer<DMPLayer>(0);
    _taus[i]=layer->tau();
    //console callback
    cbConsole(_weights[i],_taus[i],i);
    if(_log)
      _log->put<string>("candidate"+boost::lexical_cast<string>(i),
                        "tau="+boost::lexical_cast<string>(_taus[i])+
                        " fx="+boost::lexical_cast<string>(_fxs[i])+
                        " last="+boost::lexical_cast<string>(_last[i]));
  }
  loadDummyLayer();
  //return the best solution
  findBestSolution();
  //finish log
  if(_log) {
    create("./exhaustiveSearch");
    writePtreeAscii(*_log,"./exhaustiveSearch/iter"+boost::lexical_cast<string>(it)+".xml");
    _tree.put<sizeType>("exhaustiveSearch.iter",it+1);
    _log=NULL;
  }
}
sizeType DMPNeuralNet::K() const
{
  const DMPLayerMT* R=getLayer<DMPLayerMT>(0);
  const NRDMPLayerMT* NR=getLayer<NRDMPLayerMT>(0);
  if(R)
    return R->K();
  else if(NR)
    return NR->K();
  else return 1;
}
//debug
void DMPNeuralNet::debugOutput(const string& path,sizeType nrFrm,scalar dt)
{
  recreate(path);
  for(sizeType i=0; i<K(); i++) {
    //reset online
    _tree.put<sizeType>("augment0",i);
    resetOnline();
    //debug
    boost::filesystem::ofstream os(path+"/DMPTask"+boost::lexical_cast<string>(i)+".txt");
    debugOutput(os,nrFrm,dt);
  }
}
void DMPNeuralNet::debugOutput(ostream& os,sizeType nrFrm,scalar dt)
{
  for(sizeType d=0; d<nrOutput(); d++) {
    os << "output" << d << ": " << "[discrete,[" << endl;
    for(sizeType i=0; i<nrFrm; i++) {
      scalar t=(scalar)i*dt;
      Vec DMP=foreprop(Vec::Constant(1,t));
      os << "[" << t << "," << DMP[d] << "]";
      if(i < nrFrm-1)
        os << ",";
      if(i%5 == 0)
        os << endl;
    }
    os << "]];" << endl << endl;
  }
}
//helper
void DMPNeuralNet::augmentSolution()
{
  EuclideanLossLayer* loss=getLastLayer<EuclideanLossLayer>();
  ASSERT_MSG(loss,"You are not in training mode, loss layer not found!")
#define ACTIONS loss->actions()
#define STATES _foreBlocks[omp_get_thread_num()][0]
  Matd actionsOut=Matd::Zero(ACTIONS.rows(),0);
  Matd statesOut=Matd::Zero(STATES.rows(),0);
  ASSERT_MSG(STATES.rows() == 1,"DMP inputs should have dimension=1, which is the time!")
  //check if we are multi-tasking
  sizeType nrTask=1;
  sizeType BLK=STATES.cols();
  DMPLayerMT* layer=getLayer<DMPLayerMT>(0);
  if(layer) {
    nrTask=layer->K();
    BLK/=nrTask;
  }
  //for each task
  for(sizeType t=0; t<nrTask; t++) {
    //extract block for task[t]
    Matd actions=ACTIONS.block(0,t*BLK,ACTIONS.rows(),BLK);
    Matd states=STATES.block(0,t*BLK,STATES.rows(),BLK);
    //longer samples
    bool rhythmic=_tree.get<bool>("rhythmic",true);
    scalarD horizonThres=_tree.get<scalar>("exhaustiveSearch.horizonThres",5.0f);
    while(rhythmic && states(0,states.cols()-1) < horizonThres) {
      Matd statesExt=states;
      actions=concatCol(actions,actions);
      scalarD dT=states(0,1)-states(0,0);
      scalarD offT=states(0,states.cols()-1)+dT;
      statesExt.array()+=offT-statesExt(0,0);
      states=concatCol(states,statesExt);
    }
    //denser samples
    {
      //if((sizeType)_weights.size() > 1) {
      scalarD subdivideThres=_tree.get<scalar>("exhaustiveSearch.subdivideThres",0.025f);
      while(maxInterval(states.row(0)) > subdivideThres) {
        subdivide(actions);
        subdivide(states);
      }
    }
    //append
    actionsOut=concatCol(actionsOut,actions);
    statesOut=concatCol(statesOut,states);
  }
  //output
  ACTIONS=actionsOut;
  STATES=statesOut;
#undef STATES
#undef ACTIONS
}
void DMPNeuralNet::findBestSolution()
{
  ASSERT(!_fxs.empty())
  sizeType it=_tree.get<sizeType>("exhaustiveSearch.iter",0);
  sizeType unusedInterval=_tree.get<sizeType>("exhaustiveSearch.unusedInterval",10);
  //delete unused solutions
  for(sizeType i=0; i<(sizeType)_fxs.size(); i++)
    if(_last[i] < it-unusedInterval) {
      deleteElem(_weights,i);
      deleteElem(_fxs,i);
      deleteElem(_taus,i);
      deleteElem(_last,i);
      i=0;
    }
  //merge similar solutions
  scalarD mergeThres=_tree.get<scalar>("exhaustiveSearch.mergeThres",0.01f);
  for(sizeType i=0; i<(sizeType)_fxs.size(); i++)
    for(sizeType j=i+1; j<(sizeType)_fxs.size(); j++)
      if(abs(M_PI*2/_taus[i]-M_PI*2/_taus[j]) < mergeThres) {
        sizeType delId=_fxs[i] < _fxs[j] ? j : i;
        deleteElem(_weights,delId);
        deleteElem(_fxs,delId);
        deleteElem(_taus,delId);
        deleteElem(_last,delId);
        i=0,j=1;
      }
  //find best solution
  sizeType bestId=-1;
  scalarD bestVal=ScalarUtil<scalarD>::scalar_max;
  for(sizeType i=0; i<(sizeType)_fxs.size(); i++)
    if(_fxs[i] < bestVal) {
      bestVal=_fxs[i];
      bestId=i;
    }
  _last[bestId]=it;
  //log
  if(_log)
    _log->put<string>("bestId"+boost::lexical_cast<string>(bestId),"");
  //assign this solution
  fromVec(_weights[bestId]);
}
void DMPNeuralNet::setRhythmicBound(NNBLEICInterface& sol)
{
  ASSERT(!_taus.empty())
  Vec L=Vec::Constant(nrParam(),-numeric_limits<scalarD>::infinity());
  Vec U=Vec::Constant(nrParam(), numeric_limits<scalarD>::infinity());
  L[L.size()-1]=std::min_element(_taus.begin(),_taus.end())[0];
  U[U.size()-1]=std::max_element(_taus.begin(),_taus.end())[0];
  sol.setCB(L,U);
}
