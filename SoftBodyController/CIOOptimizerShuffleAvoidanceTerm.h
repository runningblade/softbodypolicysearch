#ifndef FEM_CIO_OPTIMIZER_SHUFFLE_AVOIDANCE_TERM_H
#define FEM_CIO_OPTIMIZER_SHUFFLE_AVOIDANCE_TERM_H

#include "CIOOptimizer.h"

PRJ_BEGIN

//Shuffle Avoidance Term
class CIOOptimizerShuffleAvoidanceTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerShuffleAvoidanceTerm(CIOOptimizer& opt);
  virtual scalarD runSQP(FEMCIOEngine& eng,sizeType i,Vec& DFDXI,Matd* DDFDDXI) const;
  virtual bool valid() const;
};
//Conic Shuffle Avoidance Term
class CIOOptimizerConicShuffleAvoidanceTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerConicShuffleAvoidanceTerm(CIOOptimizer& opt);
  virtual scalarD runSQP(FEMCIOEngine& eng,sizeType i,Vec& DFDXI,Matd* DDFDDXI) const;
  virtual bool valid() const;
};

PRJ_END

#endif
