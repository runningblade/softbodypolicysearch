#ifndef FEM_DDP_PLOTTER_H
#define FEM_DDP_PLOTTER_H

#include "FEMDDPSolver.h"

PRJ_BEGIN

struct NeuralNet;
class FEMDDPPlotter
{
public:
  typedef DDPSolver::Vec Vec;
  FEMDDPPlotter(FEMDDPSolver& ddp);
  //maxima
  void writeDMPParamMaxima(const Matd& data,const string& name,const string& path) const;
  void writeDMPStateMaxima(NeuralNet& net,const Matd& data,const string& name,const string& path,bool norm) const;
  void writeDMPPolicyMaxima(const string& name,const string& path,bool norm,sizeType res) const;
  void writeDMPPolicyMaximaAnalytic(const string& name,const string& path) const;
  //ascii
  void writeDMPParamAscii(const Matd& data,const string& name,const string& path) const;
  void writeDMPStateAscii(NeuralNet& net,const Matd& data,const string& name,const string& path,bool norm) const;
  void writeDMPPolicyAscii(const string& name,const string& path,bool norm,sizeType res) const;
  //misc.
  void writeControlMaxima(const string name,const string& path) const;
  void writeControlAscii(const string& name,const string& path) const;
  void writeMaximaScript(const string& stateName,const string& actionName,const string& path,bool force=false) const;
private:
  Matd collectControlData() const;
  Vec collectTimeData() const;
  FEMDDPSolver& _ddp;
};
//static
void checkMaximaPath(const string& path);
void writeMaxima(ostream& os,const string& name,const FEMDDPPlotter::Vec* tss,const FEMDDPPlotter::Vec& uss);
void beginMaxima(ostream& os);
void endMaxima(ostream& os);
void beginMaximaCmd(ostream& os);
void endMaximaCmd(ostream& os);

PRJ_END

#endif
