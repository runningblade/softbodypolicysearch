#include "DeepQLearning.h"
#include "NeuralNetQPolicy.h"
#include <Dynamics/FEMUtils.h>
#include <CommonFile/solvers/PMinresQLP.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//this implement the Simple QLearning algorithm
QLearning::QLearning(Environment& env):_env(env) {}
QLearning::~QLearning() {}
void QLearning::learn()
{
  //initialize
  scalarD expo=0;
  scalarD expoFinal=_tree.get<scalar>("finalExploration",0.1f);
  sizeType nrTraj=_tree.get<sizeType>("nrTraj",10);
  sizeType trajLen=_tree.get<sizeType>("trajLen",100);
  sizeType maxIter=_tree.get<sizeType>("maxIter",1E6);
  sizeType maxIterOpt=_tree.get<sizeType>("maxIterOpt",1);
  sizeType memSizeMultiplier=_tree.get<sizeType>("memoryBankSizeMultiplier",1);
  sizeType memSize=_tree.get<sizeType>("memoryBankSize",max<sizeType>(nrTraj*trajLen,1000))*memSizeMultiplier;
  bool stopOnFinalExpo=_tree.get<bool>("stopOnFinalExploration",true);

  //main loop
  _memoryBank.clear();
  _memoryBank.reserve(memSize);
  INFOV("Using memory bank size: %ld!",memSize)
  for(sizeType it=0,tt=0,tidAll=0; it<maxIter || (stopOnFinalExpo && expo > expoFinal); it++) {
    //add trajectory
    for(sizeType tid=0; tid<nrTraj; tid++,tidAll++) {
      Transition T;
      scalarD totalReward=0;
      T._state=_env.sampleS0();
      for(sizeType i=0; i<trajLen; i++) {
        //new transition
        T._action=_env.getPolicy().getAction(T._state,NULL,NULL);
        T._nextState=_env.transfer(i,T._action,T._reward);
        T._last=_env.isTerminal(i,T._state);
        totalReward+=T._reward;
        //add to memory bank
        if((sizeType)_memoryBank.size() < memSize) {
          ASSERT((sizeType)_memoryBank.size() == tt)
          _memoryBank.push_back(T);
        } else _memoryBank[tt%memSize]=T;
        //update target policy
        updateTargetParameter(tt);
        //update exploration
        expo=updateExploration(tt);
        //update transition
        tt++;
        if(T._last)
          break;
        T._state=T._nextState;
      }
      //profile
      INFOV("Total Reward at Episode %ld (TrajSlot: %ld): %f, exploration: %f",it,tt%memSize,totalReward,expo)
      _tree.put<scalarD>("reward"+boost::lexical_cast<string>(tidAll),totalReward);
    }
    //update neural network
    for(sizeType itOpt=0; itOpt < maxIterOpt; itOpt++)
      if(isPolicy<SimpleQPolicy>(_env) && _tree.get<bool>("useWholeMemoryNewton",true))
        updateParameterWholeMemoryNewton();
      else if(_tree.get<bool>("useWholeMemoryRMSProp",true))
        updateParameterWholeMemoryRMSProp();
      else if(isPolicy<NeuralNetQPolicy>(_env) && _tree.get<bool>("useWholeMemoryLBFGS",true))
        updateParameterWholeMemoryLBFGS();
      else {
        ASSERT_MSG(false,"Incompatible Setting!")
      }
  }

  //checkpoint path
  string path=_tree.get<string>("checkpointPath","./Checkpoints");
  recreate(path);
  writePtreeAscii(_tree,path+"/tree.xml");
  _env.Environment::write(path+"/env.env");
  boost::filesystem::ofstream os(path+"/profile.txt");
  os << "Iter Reward" << endl;
  for(sizeType it=0; it<maxIter; it++) {
    boost::optional<scalarD> op=_tree.get_optional<scalarD>("reward"+boost::lexical_cast<string>(it));
    if(op)
      os << it << " " << *op << endl;
    else break;
  }
}
scalarD QLearning::updateExploration(sizeType tt)
{
  sizeType nrTraj=_tree.get<sizeType>("nrTraj",10);
  sizeType trajLen=_tree.get<sizeType>("trajLen",100);
  sizeType maxIter=_tree.get<sizeType>("maxIter",1E6);

  scalarD expoInit=_tree.get<scalar>("initExploration",1);
  scalarD expoFinal=_tree.get<scalar>("finalExploration",0.1f);
  sizeType expoFrm=_tree.get<sizeType>("explorationFrame",maxIter*nrTraj*trajLen);
  scalarD coef=max<scalar>(min<scalar>((scalar)tt/(scalar)expoFrm,1),0);
  scalarD expo=expoInit*(1-coef)+expoFinal*coef;
  _env.getPolicy().setEps(expo);
  return expo;
}
void QLearning::updateTargetParameter(sizeType tt)
{
  sizeType nrTraj=_tree.get<sizeType>("nrTraj",10);
  sizeType trajLen=_tree.get<sizeType>("trajLen",100);
  sizeType updateFreq=_tree.get<sizeType>("updateFreq",nrTraj*trajLen);
  if(tt%updateFreq != 0)
    return;
  if(isPolicy<NeuralNetQPolicy>(_env)) {
    _env.getPolicy().updateQTarget();
    INFO("Updating parameter!")
  }
  if(isPolicy<StablizedSimpleQPolicy>(_env)) {
    _env.getPolicy().updateQTarget();
    INFO("Updating parameter!")
  }
}
void QLearning::updateParameterWholeMemoryNewton()
{
  ASSERT_MSG(isPolicy<SimpleQPolicy>(_env),"We cannot do whole memory Newton update, if you don't have a SimpleQPolicy!")
  sizeType N=_tree.get<sizeType>("batchSize",32);
  if((sizeType)_memoryBank.size() < N)
    return;

  //compute gradient
  Vec grad;
  SimpleQPolicy::SMat hess;
  map<sizeType,sizeType> MMap;
  scalarD discountCoef=_tree.get<scalar>("discountCoef",0.99f);

  Policy& policy=_env.getPolicy();
  policy.QLossMMapped(_memoryBank,discountCoef,0,(sizeType)_memoryBank.size(),grad,hess,MMap);

  Vec delta=grad;
  PMINRESSolverQLP<scalarD> sol;
  boost::shared_ptr<KrylovMatrix<scalarD> > kry(new DefaultEigenKrylovMatrix<scalarD>(hess));
  sol.setKrylovMatrix(kry);
  sol.solve(grad,delta);
  grad=policy.toVec(&MMap);
  scalarD rate=_tree.get<scalar>("learningRate",0.00025f);
  policy.fromVec(grad-delta*rate,&MMap);
}
void QLearning::updateParameterWholeMemoryRMSProp()
{
  sizeType N=_tree.get<sizeType>("batchSize",32);
  if((sizeType)_memoryBank.size() < N)
    return;

  //RMSProp with whole memory bank
  scalar rate=_tree.get<scalar>("learningRate",0.00025f);
  scalar gamma=_tree.get<scalar>("RMSPropGamma",0.95f);
  scalar eps=_tree.get<scalar>("RMSPropEps",0.01f);
  Policy& policy=_env.getPolicy();

  Vec grad;
  vector<Transition> batch=_memoryBank;
  std::random_shuffle(batch.begin(),batch.end());
  scalarD discountCoef=_tree.get<scalar>("discountCoef",0.99f);
  for(sizeType i=0; i+N<=(sizeType)batch.size(); i+=N) {
    policy.QLoss(batch,discountCoef,i,i+N,&grad,NULL);
    if(_squaredMoment.size() != grad.size())
      _squaredMoment.setZero(grad.size());
    _squaredMoment.array()=gamma*_squaredMoment.array()+(1-gamma)*grad.array().square();
    grad.array()*=rate/(_squaredMoment.array()+eps).sqrt();
    policy.fromVec(policy.toVec()-grad);
  }
}
void QLearning::updateParameterWholeMemoryLBFGS()
{
  sizeType N=_tree.get<sizeType>("batchSize",32);
  if((sizeType)_memoryBank.size() < N)
    return;

  scalar rate=_tree.get<scalar>("learningRate",0.00025f);
  scalar gamma=_tree.get<scalar>("RMSPropGamma",0.95f);

  NeuralNetPolicy& policy=dynamic_cast<NeuralNetPolicy&>(_env.getPolicy());
  //policy._net->_tree.put<sizeType>("maxIterations",1E2);
  policy._net->_tree.put<bool>("useCommonLBFGS",true);

  vector<Transition> batch=_memoryBank;
  Vec weight=policy.toVec();
  policy.optimizeQLoss(batch,gamma,0,(sizeType)batch.size());
  policy.fromVec(weight*(1-rate)+policy.toVec()*rate);
}
