#include "FEMEnvironment.h"
#include "FEMFeature.h"
#include "DMPNeuralNet.h"
#include "DDPEnergy.h"
#include <Dynamics/FEMRotationUtil.h>
#include <Dynamics/FEMMeshFormat.h>
#include <Dynamics/ArticulatedConfig.h>
#include <Dynamics/ArticulatedCIOEngine.h>
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/FEMUtils.h>

#include <CommonFile/geom/StaticGeom.h>
#include <CommonFile/geom/StaticGeomCell.h>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

USE_PRJ_NAMESPACE

Vec3d tryToFindCons(FEMCIOEngine& eng,const Vec3d& dir,bool empty)
{
  FEMCIOEngine::Contacts cons;
  GradientInfo::Vec X=eng._x.x();

  Vec3d dirE=dir.normalized()*eng.avgElementLength();
  sizeType nrStep=dir.norm()/dirE.norm();
  for(sizeType i=0; i<nrStep; i++) {
    X=eng._x.x();
    X.segment<3>(X.size()-6)+=dirE;
    eng._x.reset(eng.getMCalc(),X);
    eng.resetContacts(eng._x,&cons);
    if(cons.empty() != empty)
      return eng._x.x().segment<3>(X.size()-6);
  }
  scalar ext=eng.getBody().getBB(true).getExtent().norm();
  return dir.normalized()*ext*1000;
}
Vec3d findAboveFloorConfig(FEMCIOEngine& eng,const Vec3d& dir)
{
  //binary search
  FEMCIOEngine::Contacts cons;
  GradientInfo::Vec X=eng._x.x();

  Vec3d A=X.segment<3>(X.size()-6);
  X.segment<3>(X.size()-6)=A;
  eng._x.reset(eng.getMCalc(),X);
  eng.resetContacts(eng._x,&cons);
  bool ACons=cons.empty();

  Vec3d B=A+dir;
  X.segment<3>(X.size()-6)=B;
  eng._x.reset(eng.getMCalc(),X);
  eng.resetContacts(eng._x,&cons);
  bool BCons=cons.empty();

  Vec3d mid=(A+B)/2;
  ASSERT(ACons != BCons)
  scalarD thres=eng.avgElementLength()*1E-5f;
  while((A-B).norm() > thres) {
    mid=(A+B)/2;
    X.segment<3>(X.size()-6)=mid;
    eng._x.reset(eng.getMCalc(),X);
    eng.resetContacts(eng._x,&cons);
    if(cons.empty() == ACons)
      A=mid;
    else B=mid;
  }

  //test again
  //X.segment<3>(X.size()-6)=A;
  //eng._x.reset(eng.getMCalc(),X);
  //eng.resetContacts(eng._x,&cons);
  //ACons=cons.empty();
  //X.segment<3>(X.size()-6)=B;
  //eng._x.reset(eng.getMCalc(),X);
  //eng.resetContacts(eng._x,&cons);
  //BCons=cons.empty();
  return !ACons ? A : B;
}
void writeVTK(const FEMMesh& mesh,const string& path,scalar dist)
{
  ObjMesh smesh;
  const FEMBody& b=mesh.getB(0);
  if(b.dim() == 2)
    b.writeObjExtrude(smesh,dist);
  else if(b._emiss.empty() && b._emvss.empty())
    b.writeObj(smesh);
  else b.writeObjEmbedded(smesh);
  smesh.writeVTK(path,true);
}
void writePov(const FEMMesh& mesh,const string& path,scalar dist)
{
  ObjMesh smesh;
  const FEMBody& b=mesh.getB(0);
  if(b.dim() == 2)
    b.writeObjExtrude(smesh,dist);
  else if(b._emiss.empty() && b._emvss.empty())
    b.writeObj(smesh);
  else b.writeObjEmbedded(smesh);
  smesh.writePov(path+"n",true);
  smesh.writePov(path,false);
}
//FEMEnvironment
class ImplicitFuncBB : public ImplicitFunc<scalar>
{
public:
  ImplicitFuncBB(const BBox<scalar>& bb,sizeType dim):_bb(bb),_dim(dim) {}
  virtual scalar operator()(const Vec3& pos) const {
    if(!_bb.contain(pos,_dim))
      return -1;
    else return 1;
  }
private:
  const BBox<scalar> _bb;
  const sizeType _dim;
};
class ImplicitFuncBBC : public ImplicitFunc<scalar>
{
public:
  ImplicitFuncBBC(const BBox<scalar>& bb,scalar thresBBC,sizeType dim)
    :_bb(bb),_thresBBC(thresBBC*bb.getExtent().maxCoeff()),_dim(dim) {}
  virtual scalar operator()(const Vec3& pos) const {
    for(sizeType i=0; i<3; i++)
      if(min<scalar>(abs(pos[i]-_bb._minC[i]),abs(pos[i]-_bb._maxC[i])) > _thresBBC)
        return 1;
    return -1;
  }
private:
  const BBox<scalar> _bb;
  const scalar _thresBBC;
  const sizeType _dim;
};
Vec3 recenterBody(FEMBody& body)
{
  //sum
  scalar denom=0;
  Vec3 num=Vec3::Zero();
  for(sizeType v=0; v<body.nrV(); v++) {
    const FEMVertex& vert=body.getV(v);
    num+=vert._mass*vert._pos0;
    denom+=vert._mass;
  }
  //move
  Mat4 T=Mat4::Identity();
  T.block<3,1>(0,3)=-num/denom;
  body.applyTrans(T,true,true);
  //check
  num=Vec3::Zero();
  for(sizeType v=0; v<body.nrV(); v++) {
    const FEMVertex& vert=body.getV(v);
    num+=vert._mass*vert._pos0;
  }
  INFOV("recentered center: (%f,%f,%f)!",num[0],num[1],num[2])
  return T.block<3,1>(0,3);
}
void createPhysicsRigidReduced(const string& name,FEMBody& b,const Vec3& gravity,scalar young,scalar poisson,sizeType DOF,bool useCubAD,bool useGPU,bool useStVK)
{
  //system
  MaterialEnergy::TYPE energyType=MaterialEnergy::LINEAR;
  FEMReducedSystem::KINETIC_MODEL_TYPE modelType=FEMReducedSystem::CUBATURE_COUPLED_REDUCED_RS;
  if(useStVK) {
    energyType=MaterialEnergy::STVK;
    modelType=FEMReducedSystem::LMA;
  }
  FEMSystem::GroupInfo info;
  info._young=young;
  info._poisson=poisson;
  b._tree.put<string>("meshName",name);
  b._tree.put<bool>("GPUEval",useGPU);
  b._tree.put<bool>("rigidDOF",true);
  b._tree.put<bool>("debugRSB",false);
  b._tree.put<bool>("debugRSDLDS",true);
  b._tree.put<bool>("debugMCalc",false);
  b._tree.put<bool>("debugKCalc",false);
  b._tree.put<bool>("debugKineticCubature",true);
  b._tree.put<bool>("allAeroDynamicsCub",!useCubAD);
  b._tree.put<sizeType>("PadeP",3);
  b._tree.put<sizeType>("PadeQ",2);
  b._system.reset(new FEMReducedSystem(b));
  b._system->addEnergyMaterial(info,energyType,true);
  b._system->addEnergyMass(gravity,NULL);
  if(useStVK)
    dynamic_cast<FEMReducedSystem&>(*(b._system)).setReducedModel(modelType,0,0,0);
  else dynamic_cast<FEMReducedSystem&>(*(b._system)).setReducedModel(modelType,100,1000,0.01f);
  //dynamic_cast<FEMReducedSystem&>(*(b._system)).setReducedModel(modelType,100,1000,0.01f);
  dynamic_cast<FEMReducedSystem&>(*(b._system)).buildU(DOF);
  //boost::shared_ptr<FEMReducedSystem> sys=boost::dynamic_pointer_cast<FEMReducedSystem>(b._system);
  //sys->writeBasisVTK("./basis",3,true);
  b._system->onDirty();
}
void createPhysicsArticulated(const string& name,FEMBody& b,const Vec3& gravity,bool useCubAD,bool useGPU)
{
  //system
  FEMReducedSystem::KINETIC_MODEL_TYPE modelType=FEMReducedSystem::ARTICULATED;
  b._tree.put<string>("meshName",name);
  b._tree.put<bool>("GPUEval",useGPU);
  b._tree.put<bool>("debugMCalc",false);
  b._tree.put<bool>("debugKCalc",false);
  b._tree.put<bool>("debugKineticCubature",true);
  b._tree.put<bool>("allAeroDynamicsCub",!useCubAD);
  b._system.reset(new FEMReducedSystem(b));
  b._system->addEnergyMass(gravity,NULL);
  dynamic_cast<FEMReducedSystem&>(*(b._system)).setReducedModel(modelType,0,0,0);
  dynamic_cast<FEMReducedSystem&>(*(b._system)).buildU(0);
  b._system->onDirty();
}
FEMEnvironment::FEMEnvironment():Environment(typeid(FEMEnvironment).name()) {}
bool FEMEnvironment::read(istream& is,IOData* dat)
{
  //solver
  sizeType dim;
  readBinaryData(dim,is);
  registerType(dat,boost::shared_ptr<FEMCIOEngineImplicit>(new FEMCIOEngine(dim)));
  registerType(dat,boost::shared_ptr<FEMCIOEngineImplicit>(new FEMCIOEngineImplicit(dim)));
  registerType(dat,boost::shared_ptr<FEMCIOEngineImplicit>(new ArticulatedCIOEngineImplicit(dim)));
  readBinaryData(_sol,is,dat);
  //policy
  _policy=NULL;
  getPolicy().read(is,dat);
  //ess
  DDPEnergy::registerType(dat);
  readVector(_ess,is,dat);
  return is.good();
}
bool FEMEnvironment::write(ostream& os,IOData* dat) const
{
  //solver
  sizeType dim=_sol ? _sol->_tree.get<sizeType>("dim") : 0;
  writeBinaryData(dim,os);
  registerType(dat,boost::shared_ptr<FEMCIOEngineImplicit>(new FEMCIOEngine(dim)));
  registerType(dat,boost::shared_ptr<FEMCIOEngineImplicit>(new FEMCIOEngineImplicit(dim)));
  registerType(dat,boost::shared_ptr<FEMCIOEngineImplicit>(new ArticulatedCIOEngineImplicit(dim)));
  writeBinaryData(_sol,os,dat);
  //policy
  const_cast<FEMEnvironment&>(*this).getPolicy().write(os,dat);
  //ess
  DDPEnergy::registerType(dat);
  writeVector(_ess,os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> FEMEnvironment::copy() const
{
  return boost::shared_ptr<Serializable>(new FEMEnvironment);
}
void FEMEnvironment::setState(const Vec& state)
{
  _state=state;
  sizeType nrS=_state.size()/2;
  _sol->_xN.reset(_sol->getMCalc(),_state.segment(0,nrS));
  _sol->_x .reset(_sol->getMCalc(),_state.segment(nrS,nrS));
  _sol->_xP=_sol->_x;
}
void FEMEnvironment::writeStateVTK(const string& path)
{
  //write control
  sizeType nrS=_sol->getMCalc().size();
  Vec controlInput=_sol->getControlInput();
  scalar dist=_sol->_tree.get<scalar>("extrudeDist",1E-3f);
  if(_sol->_tree.get<bool>("writeControlShape",false) && controlInput.size() == nrS-6) {
    //from control matrix to control shape
    const Matd& controlMatrix=_sol->getControlMatrix();
    Matd controlShape=getStiffness().fullPivLu().solve(controlMatrix);
    //set to control shape
    Vec state=_state.segment(nrS,nrS);
    state.segment(0,_sol->getControlInput().size())=controlShape*_sol->getControlInput();
    _sol->getBody()._system->setPos(state);
    _sol->getMesh().writeVTK(boost::filesystem::path(path).replace_extension("").string()+"ControlShape.vtk");
  }
  //write state
  {
    Vec state=_state.segment(nrS,nrS);
    _sol->getBody()._system->setPos(state);
    writeVTK(_sol->getMesh(),path,dist);
  }
  //write contact
  scalar len=_sol->_tree.get<scalar>("drawContactLength",0);
  if(len != 0) {
    string pathC=path;
    boost::replace_all(pathC,".vtk","Contact.vtk");
    _sol->writeContactVTK(pathC,len);
  }
  //write feature
  FEMFeature* feature=getPolicy()._net->getLayer<FEMFeature>(0);
  if(feature) {
    string pathC=path;
    boost::replace_all(pathC,".vtk","Feature.vtk");
    feature->writeVTK(_state,pathC);
  }
}
void FEMEnvironment::writeStatePov(const string& path)
{
  //write control
  sizeType nrS=_sol->getMCalc().size();
  Vec controlInput=_sol->getControlInput();
  scalar dist=_sol->_tree.get<scalar>("extrudeDist",1E-3f);
  if(controlInput.size() == nrS-6) {
    //from control matrix to control shape
    const Matd& controlMatrix=_sol->getControlMatrix();
    Matd controlShape=getStiffness().fullPivLu().solve(controlMatrix);
    //set to control shape
    Vec state=_state.segment(nrS,nrS);
    state.segment(0,_sol->getControlInput().size())=controlShape*_sol->getControlInput();
    _sol->getBody()._system->setPos(state);
    //write povray
    string pathPov=boost::filesystem::path(path).replace_extension("").string()+"ControlShape.pov";
    writePov(_sol->getMesh(),pathPov,dist);
  }
  //write state
  {
    string pathPov=boost::filesystem::path(path).replace_extension("").string()+".pov";
    Vec state=_state.segment(nrS,nrS);
    _sol->getBody()._system->setPos(state);
    writePov(_sol->getMesh(),pathPov,dist);
  }
}
void FEMEnvironment::writeEnvVTK(const string& path) const
{
  _sol->_geom->writeVTK(path);
}
void FEMEnvironment::addEmbeddedMesh(const string& path)
{
  ObjMesh smesh;
  boost::filesystem::ifstream is(path);
  smesh.read(is,false,false);
  smesh.getPos()[0]=_sol->_tree.get<scalar>("recenterX",0);
  smesh.getPos()[1]=_sol->_tree.get<scalar>("recenterY",0);
  smesh.getPos()[2]=_sol->_tree.get<scalar>("recenterZ",0);
  smesh.applyTrans(Vec3::Zero());

  FEMMesh& mesh=sol().getMesh();
  FEMSystem& sys=*(mesh.getB(0)._system);
  sys.setPos(Vec::Zero(sys.size()));
  //cout << smesh.getBB()._minC.transpose() << endl;
  //cout << smesh.getBB()._maxC.transpose() << endl;
  //cout << mesh.getB(0).getBB(false)._minC.transpose() << endl;
  //cout << mesh.getB(0).getBB(false)._maxC.transpose() << endl;
  mesh.addEmbeddedMesh(smesh);
}
void FEMEnvironment::reset(const boost::property_tree::ptree& pt)
{
  //clear policy
  _policy=NULL;
  //create solver
  boost::filesystem::path path=pt.get<string>("model","");
  boost::optional<const boost::property_tree::ptree&> pathArt=pt.get_child_optional("articulatedModel");
  sizeType dim=pt.get<sizeType>("dim");
  if(!path.empty())
    _sol.reset(new FEMCIOEngineImplicit(dim));
  else if(pathArt)
    _sol.reset(new ArticulatedCIOEngineImplicit(dim));
  else {
    ASSERT_MSG(false,"We cannot find model!")
  }
  _sol->_tree=pt;
  _sol->resetParam(pt.get<scalar>("damping",0.01f),pt.get<scalar>("eps",1E-3f),pt.get<sizeType>("maxIter",100));
  _sol->setCollCoef(pt.get<scalar>("collK",0.1f),pt.get<scalar>("collMu",0.0f),6);
  _sol->_geom.reset(new StaticGeom(dim));
  _sol->_geom->assemble();
  //create geometry
  FEMMesh& mesh=_sol->getMesh();
  if(pathArt) {
    mesh.reset(*pathArt);
  } else if(path.extension().string() == ".svg" || path.extension().string() == ".SVG") {
    boost::filesystem::path pathObj=path;
    pathObj.replace_extension(".obj");
    ObjMesh vol;
    if(!exists(pathObj)) {
      FEMMeshFormat::SVGToMesh(path.string(),vol,true,true,0.001f);
      vol.write(pathObj);
    } else {
      boost::filesystem::ifstream is(pathObj);
      vol.read(is,false,false);
    }
    mesh.reset(vol);
  } else if(path.extension().string() == ".abq" || path.extension().string() == ".ABQ") {
    mesh.reset(path.string(),0);
  } else if(path.extension().string() == ".xml" || path.extension().string() == ".XML") {
    boost::property_tree::ptree ptArticulated;
    readPtreeAscii(ptArticulated,path);
    mesh.reset(ptArticulated);
  } else {
    ASSERT_MSG(false,"Unsupported mesh extension!")
  }
  ASSERT_MSG(mesh.nrB() == 1,"Only 1 body is supported!")
  if(_sol->_tree.get<bool>("recenter",false) && !pathArt) {
    //articulated body does not support recentering
    Vec3 recenter=recenterBody(mesh.getB(0));
    _sol->_tree.put<scalar>("recenterX",recenter[0]);
    _sol->_tree.put<scalar>("recenterY",recenter[1]);
    _sol->_tree.put<scalar>("recenterZ",recenter[2]);
  }
  //create physics
  path.replace_extension("");
  sizeType DOF=_sol->_tree.get<sizeType>("DOF",5);
  scalar young=_sol->_tree.get<scalar>("young",1E6f);
  scalar poisson=_sol->_tree.get<scalar>("poisson",0.4f);
  bool useCubAD=_sol->_tree.get<bool>("useCubAD",false);
  bool useGPU=_sol->_tree.get<bool>("GPUEval",pt.get<bool>("GPUEval",false));
  bool useSTVK=_sol->_tree.get<bool>("useReducedStVKModel",false);
  _sol->getBody()._tree.put<bool>("MTCubature",true);
  _sol->getBody()._tree.put<bool>("fixZ",_sol->_tree.get<bool>("fixZ",false));
  if(pathArt)
    createPhysicsArticulated(path.filename().string(),mesh.getB(0),getGravity(),useCubAD,useGPU);
  else if(!path.empty())
    createPhysicsRigidReduced(path.filename().string(),mesh.getB(0),getGravity(),young,poisson,DOF,useCubAD,useGPU,useSTVK);
  //set timestep size
  _sol->_tree.put<scalar>("dt",_sol->_tree.get<scalar>("dt",0.01f));
  //setup AeroDynamic model
  _sol->addFluid();
  //setup geometry
  if(createFloor()) {}
  else if(createDoubleFloor()) {}
  sampleS0();
  //setup energy
  addEnergyBalance(&pt);
  addEnergyJump(&pt);
  addEnergyWalk(&pt);
  addEnergyWalkTo(&pt);
  addEnergyWalkSpeedTo(&pt);
  addEnergy2DRoll(&pt);
  addEnergy3DRoll(&pt);
  addEnergy3DOrientation(&pt);
  addEnergy3DOrientationTo(&pt);
  addEnergyCurvedWalk(&pt);
  _sol->getBody()._tree.put<bool>("MTCubature",false);
}
void FEMEnvironment::resetContacts()
{
  //reset contact points
  Vec x=sampleS0();
  x=x.segment(x.size()/2,x.size()/2).eval();
  scalar thresBB=_sol->_tree.get<scalar>("contactBBThres",0);
  scalar thresBBL=_sol->_tree.get<scalar>("contactBBLThres",0);
  scalar thresBBC=_sol->_tree.get<scalar>("contactBBCThres",0);
  GradientInfo info(_sol->getMCalc(),x);
  sizeType dim=_sol->getBody().dim();
  if(thresBB > 0) {
    BBox<scalar> bb=_sol->getBody().getBB(true);
    bb.enlarged(-thresBB,dim);
    ImplicitFuncBB f(bb,dim);
    _sol->resetContactsSimplified(info,NULL,f);
  } else if(thresBBL > 0) {
    BBox<scalar> bb=_sol->getBody().getBB(true);
    bb._minC[1]+=thresBBL;
    ImplicitFuncBB f(bb,dim);
    _sol->resetContactsSimplified(info,NULL,f);
  } else if(thresBBC > 0) {
    BBox<scalar> bb=_sol->getBody().getBB(true);
    ImplicitFuncBBC f(bb,thresBBC,dim);
    _sol->resetContactsSimplified(info,NULL,f);
  } else _sol->resetContactsSurface(info,NULL);
}
Matd FEMEnvironment::getStiffness() const
{
  sizeType nrS=sol().getMCalc().size()-6;
  const FEMBody& body=sol().getBody();
  Matd ret=Matd::Zero(nrS,nrS);

  if(body._articulated)
    ret.diagonal()=body._articulated->limits().row(2).segment(0,nrS);
  else ret=body._basis->_UTKU;
  return ret;
}
boost::property_tree::ptree& FEMEnvironment::pt()
{
  return _sol->_tree;
}
FEMCIOEngineImplicit* FEMEnvironment::solPtr() const
{
  return _sol.get();
}
const FEMCIOEngineImplicit& FEMEnvironment::sol() const
{
  return *_sol;
}
FEMCIOEngineImplicit& FEMEnvironment::sol()
{
  return *_sol;
}
void FEMEnvironment::writeActionScaleVTK()
{
  getPolicy();
  boost::shared_ptr<NeuralNetPolicy> policy=
    boost::dynamic_pointer_cast<NeuralNetPolicy>(_policy);
  if(!policy)
    return;

  sizeType nrS=_sol->getMCalc().size();
  Vec scale=policy->getActionScale();
  const string path="./actionScale";
  recreate(path);
  for(sizeType i=0; i<scale.size(); i++) {
    _state=Vec::Zero(_sol->getMCalc().size()*2);
    _state[i]=_state[i+nrS]=scale[i];
    writeStateVTK(path+"/action"+boost::lexical_cast<string>(i)+".vtk");
  }
}
//geometry
Vec3 FEMEnvironment::getGravity() const
{
  return Vec3(0,-_sol->_tree.get<scalar>("gravityCoef",9.81f),0);
}
Mat4 FEMEnvironment::moveAboveFloor() const
{
  Mat4d T=Mat4d::Identity();
  _sol->getMesh().updateMesh();
  T.block<3,3>(0,0)=_sol->_x.R();
  T.block<3,1>(0,3)=_sol->_x.x().segment<3>(_sol->_x.x().size()-6);

  //simply move above floor
  BBox<scalar> bb=_sol->getMesh().getB(0).getBB(false);
  FEMCIOEngine::Contacts cons;
  GradientInfo x=_sol->_x;
  T.block<3,1>(0,3)=Vec3d(0,-bb._minC[1],0);
  //cout << T.block<3,1>(0,3) << endl;

  //search in a more detailed way
  Vec3d dirG=getGravity().cast<scalarD>();
  if(dirG.norm() > 1E-10f) {
    //save
    boost::property_tree::ptree ptSave=_sol->_tree;
    FEMLCPSolver::Contacts consSave=_sol->getCons();
    //update
    _sol->_tree.put<scalar>("collisionExpand",0);
    dirG=-dirG.normalized();
    _sol->resetContacts(x,&cons);
    Vec3d upper=tryToFindCons(*_sol, dirG*bb.getExtent().norm()*10,cons.empty());
    _sol->_x=x;
    Vec3d lower=tryToFindCons(*_sol,-dirG*bb.getExtent().norm()*10,cons.empty());
    _sol->_x=x;
    if(upper.norm() < lower.norm())
      T.block<3,1>(0,3)=   findAboveFloorConfig(*_sol,upper);
    else T.block<3,1>(0,3)=findAboveFloorConfig(*_sol,lower);
    _sol->resetContacts(x,&cons);
    //load
    _sol->setCons(consSave);
    _sol->_tree=ptSave;
  }
  return T.cast<scalar>();
}
bool FEMEnvironment::createDoubleFloor()
{
  scalar sz=_sol->_tree.get<scalar>("doubleFloorSize",0);
  scalar th=_sol->_tree.get<scalar>("doubleFloorThickness",1);
  scalar ang=_sol->_tree.get<scalar>("doubleFloorAngle",45)*M_PI/180.0f;
  if(sz > 0) {
    sizeType dim=_sol->getMesh().dim();
    _sol->_geom.reset(new StaticGeom(dim));
    Mat4 T=Mat4::Identity();
    scalar rz=_sol->_tree.get<scalar>("doubleFloorRotZ",0);
    BBox<scalar> bb(Vec3(-sz,-th,dim==2?0:-sz),Vec3(sz,0,dim==2?0:sz));
    T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(0,0,rz))*makeRotation<scalar>(Vec3( ang,0,0));
    _sol->_geom->addGeomBox(T,bb);
    T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(0,0,rz))*makeRotation<scalar>(Vec3(-ang,0,0));
    _sol->_geom->addGeomBox(T,bb);
    _sol->_geom->assemble();
    return true;
  }
  return false;
}
bool FEMEnvironment::createFloor()
{
  scalar sz=_sol->_tree.get<scalar>("floorSize",1000);
  scalar th=_sol->_tree.get<scalar>("floorThickness",1);
  if(sz > 0) {
    sizeType dim=_sol->getMesh().dim();
    _sol->_geom.reset(new StaticGeom(dim));
    Mat4 T=Mat4::Identity();
    scalar rx=_sol->_tree.get<scalar>("floorRotX",0);
    scalar rz=_sol->_tree.get<scalar>("floorRotZ",0);
    BBox<scalar> bb(Vec3(-sz,-th,dim==2?0:-sz),Vec3(sz,0,dim==2?0:sz));
    T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(rx,0,rz));
    _sol->_geom->addGeomBox(T,bb);
    _sol->_geom->assemble();
    return true;
  }
  return false;
}
void FEMEnvironment::fixPos()
{
  sizeType nrS=_sol->getMCalc().size();
  Matd CI=Matd::Zero(3,nrS);
  for(sizeType i=0; i<3; i++)
    CI(i,nrS-6+i)=1;
  _sol->setCI(CI,Vec::Zero(3));
}
void FEMEnvironment::fixRot()
{
  sizeType nrS=_sol->getMCalc().size();
  Matd CI=Matd::Zero(3,nrS);
  CI(0,nrS-3+0)=1;
  CI(1,nrS-3+1)=1;
  CI(2,nrS-3+2)=1;
  _sol->setCI(CI,Vec::Zero(3));
}
void FEMEnvironment::fixRotXY()
{
  sizeType nrS=_sol->getMCalc().size();
  Matd CI=Matd::Zero(2,nrS);
  CI(0,nrS-3+0)=1;
  CI(1,nrS-3+1)=1;
  _sol->setCI(CI,Vec::Zero(2));
}
void FEMEnvironment::fixRotYZ()
{
  sizeType nrS=_sol->getMCalc().size();
  Matd CI=Matd::Zero(2,nrS);
  CI(0,nrS-3+1)=1;
  CI(1,nrS-3+2)=1;
  _sol->setCI(CI,Vec::Zero(2));
}
void FEMEnvironment::fixRotXZ()
{
  sizeType nrS=_sol->getMCalc().size();
  Matd CI=Matd::Zero(2,nrS);
  CI(0,nrS-3+0)=1;
  CI(1,nrS-3+2)=1;
  _sol->setCI(CI,Vec::Zero(2));
}
//energy from ptree
void FEMEnvironment::addEnergyBalance(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefBalance",0);
  if(coef <= 0)
    return;

  //add balance energy
  Vec x=sampleS0();
  Vec3 dirG(0,0,0),dirL;
  sizeType off=x.size()-3;
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off,3),NULL,NULL,NULL,NULL);
  boost::optional<scalar> dx=pt->get_optional<scalar>("targetBalanceDirX");
  boost::optional<scalar> dy=pt->get_optional<scalar>("targetBalanceDirY");
  boost::optional<scalar> dz=pt->get_optional<scalar>("targetBalanceDirZ");
  if(dx || dy || dz) {
    if(dx)
      dirG[0]=*dx;
    if(dy)
      dirG[1]=*dy;
    if(dz)
      dirG[2]=*dz;
    INFOV("Detected custom dirG: (%f,%f,%f)",dirG[0],dirG[1],dirG[2])
  } else {
    dirG=-getGravity();
    if(dirG.norm() < 1E-10f) {
      WARNING("No gravity, using (0,1,0)!")
      dirG=Vec3(0,1,0);
    }
  }

  boost::optional<scalar> dlx=pt->get_optional<scalar>("targetBalanceDirLX");
  boost::optional<scalar> dly=pt->get_optional<scalar>("targetBalanceDirLY");
  boost::optional<scalar> dlz=pt->get_optional<scalar>("targetBalanceDirLZ");
  if(dlx || dly || dlz) {
    if(dlx)
      dirL[0]=*dlx;
    if(dly)
      dirL[1]=*dly;
    if(dlz)
      dirL[2]=*dlz;
  } else {
    dirL=R.transpose().cast<scalar>()*dirG;
  }
  addEnergyBalance(dirL,dirG,coef);
  INFOV("Adding energy balance: (%f,%f,%f)->(%f,%f,%f)",
        dirL[0],dirL[1],dirL[2],dirG[0],dirG[1],dirG[2])

  //add additional balance energy
  for(sizeType i=0;; i++) {
    const string str=boost::lexical_cast<string>(i);
    scalar posX=pt->get<scalar>("targetBalanceDirX"+str,0);
    scalar posY=pt->get<scalar>("targetBalanceDirY"+str,0);
    scalar posZ=pt->get<scalar>("targetBalanceDirZ"+str,0);
    dirG=Vec3(posX,posY,posZ);
    if(dirG.isZero())
      break;
    dirL=R.transpose().cast<scalar>()*dirG;
    addEnergyBalance(dirL,dirG,coef);
    INFOV("Adding energy balance: (%f,%f,%f)->(%f,%f,%f)",
          dirL[0],dirL[1],dirL[2],dirG[0],dirG[1],dirG[2])
  }
}
void FEMEnvironment::addEnergyJump(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefJump",0);
  if(coef <= 0)
    return;

  for(sizeType i=0;; i++) {
    const string str=boost::lexical_cast<string>(i);
    boost::optional<scalar> frame=pt->get_optional<scalar>("peakJumpingTime"+str);
    if(!frame)
      break;
    scalar height=pt->get<scalar>("targetJumpingHeight"+str,1);
    addEnergyJump(height,*frame,coef);
  }
}
void FEMEnvironment::addEnergyWalk(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefWalk",0);
  if(coef <= 0)
    return;

  scalar spdX=pt->get<scalar>("targetWalkingSpeedX",0);
  scalar spdY=pt->get<scalar>("targetWalkingSpeedY",0);
  scalar spdZ=pt->get<scalar>("targetWalkingSpeedZ",0);
  Vec3 spd(spdX,spdY,spdZ);
  addEnergyWalk(spd,coef);
}
void FEMEnvironment::addEnergyWalkTo(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefWalkTo",0);
  if(coef <= 0)
    return;

  for(sizeType i=0;; i++) {
    const string str=boost::lexical_cast<string>(i);
    boost::optional<scalar> frame=pt->get_optional<scalar>("peakWalkingTime"+str);
    if(!frame)
      break;
    //INFO("Add target walking to energy!")
    scalar posX=pt->get<scalar>("targetWalkingPositionX"+str,0);
    scalar posY=pt->get<scalar>("targetWalkingPositionY"+str,0);
    scalar posZ=pt->get<scalar>("targetWalkingPositionZ"+str,0);
    Vec3 pos(posX,posY,posZ);
    Vec3 g=getGravity();
    if(g.norm() > 1E-10f) {
      //set same gravity as S0
      //first remove current gravity
      g/=g.norm();
      pos-=pos.dot(g)*g;
      //second add S0 gravity
      Vec x=_sol->_x.x();
      Vec3 x0=x.segment<3>(x.size()-6).cast<scalar>();
      pos+=x0.dot(g)*g;
    }
    addEnergyWalkTo(pos,
                    pt->get<bool>("targetWalkingPositionGT"+str,true),
                    pt->get<bool>("targetWalkingPositionGN"+str,false),
                    *frame,coef);
  }
}
void FEMEnvironment::addEnergyWalkSpeedTo(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefSpeedTo",0);
  if(coef <= 0)
    return;

  for(sizeType i=0;; i++) {
    const string str=boost::lexical_cast<string>(i);
    boost::optional<scalar> frame=pt->get_optional<scalar>("peakSpeedToTime"+str);
    if(!frame)
      break;
    //INFO("Add target walking to energy!")
    scalar posX=pt->get<scalar>("targetWalkingSpeedToX"+str,0);
    scalar posY=pt->get<scalar>("targetWalkingSpeedToY"+str,0);
    scalar posZ=pt->get<scalar>("targetWalkingSpeedToZ"+str,0);
    Vec3 pos(posX,posY,posZ);
    /*Vec3 g=getGravity();
    if(g.norm() > 1E-10f) {
      //set same gravity as S0
      //first remove current gravity
      g/=g.norm();
      pos-=pos.dot(g)*g;
      //second add S0 gravity
      Vec x=_sol->_x.x();
      Vec3 x0=x.segment<3>(x.size()-6).cast<scalar>();
      pos+=x0.dot(g)*g;
    }*/
    addEnergyWalkSpeedTo(pos,
                         pt->get<bool>("targetWalkingSpeedGT"+str,true),
                         pt->get<bool>("targetWalkingSpeedGN"+str,false),
                         *frame,coef);
  }
}
void FEMEnvironment::addEnergy2DRoll(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefRoll",0);
  if(coef <= 0)
    return;

  scalar spd=pt->get<scalar>("targetRollingSpeed",1);
  addEnergy2DRoll(spd,coef);
}
void FEMEnvironment::addEnergy3DRoll(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefRoll3D",0);
  if(coef <= 0)
    return;

  scalar spdX=pt->get<scalar>("targetRollingSpeedX",0);
  scalar spdY=pt->get<scalar>("targetRollingSpeedY",0);
  scalar spdZ=pt->get<scalar>("targetRollingSpeedZ",0);
  addEnergy3DRoll(Vec3(spdX,spdY,spdZ),coef);
}
void FEMEnvironment::addEnergy3DOrientation(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefOrientation",0);
  if(coef <= 0)
    return;

  Vec x=sampleS0();
  sizeType off=x.size()-3;
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off,3),NULL,NULL,NULL,NULL);
  scalarD dirX=pt->get<scalar>("3DOrientationDirX",1);
  scalarD dirY=pt->get<scalar>("3DOrientationDirY",0);
  scalarD dirZ=pt->get<scalar>("3DOrientationDirZ",0);
  Vec3 dirG=Vec3(dirX,dirY,dirZ).normalized(),dirL=R.transpose().cast<scalar>()*dirG;
  scalarD rotX=pt->get<scalar>("3DOrientationSpdX",0);
  scalarD rotY=pt->get<scalar>("3DOrientationSpdY",0);
  scalarD rotZ=pt->get<scalar>("3DOrientationSpdZ",0);
  addEnergy3DOrientation(dirL,Vec3(rotX,rotY,rotZ),coef);
}
void FEMEnvironment::addEnergy3DOrientationTo(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefOrientationTo",0);
  if(coef <= 0)
    return;

  for(sizeType i=0;; i++) {
    const string str=boost::lexical_cast<string>(i);
    boost::optional<scalar> frame=pt->get_optional<scalar>("peakOrientationTime"+str);
    if(!frame)
      break;
    //INFO("Add target walking to energy!")
    scalar rotX=pt->get<scalar>("3DOrientationToX"+str,0);
    scalar rotY=pt->get<scalar>("3DOrientationToY"+str,0);
    scalar rotZ=pt->get<scalar>("3DOrientationToZ"+str,0);
    Vec3 rot(rotX,rotY,rotZ);
    addEnergy3DOrientationTo(rot,*frame,coef);
  }
}
void FEMEnvironment::addEnergyCurvedWalk(const boost::property_tree::ptree* pt)
{
  if(!pt)
    pt=&(_sol->_tree);
  scalar coef=pt->get<scalar>("coefCurvedWalk",0);
  if(coef <= 0)
    return;

  scalar velX=pt->get<scalar>("curveWalkVelX",0);
  scalar velY=pt->get<scalar>("curveWalkVelY",0);
  scalar velZ=pt->get<scalar>("curveWalkVelZ",0);
  scalar rotX=pt->get<scalar>("curveWalkRotX",0);
  scalar rotY=pt->get<scalar>("curveWalkRotY",0);
  scalar rotZ=pt->get<scalar>("curveWalkRotZ",0);
  sizeType mtId=pt->get<sizeType>("curveWalkMTId",-1);
  addEnergyCurvedWalk(Vec3(velX,velY,velZ),Vec3(rotX,rotY,rotZ),coef,mtId);
}
//energy from parameter
void FEMEnvironment::addEnergyBalance(const Vec3& dirL,const Vec3& dirG,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  _ess.push_back(boost::shared_ptr<DDPEnergy>(new DDPBalanceEnergy(dirL.cast<scalarD>(),dirG.cast<scalarD>(),coef,dim)));
}
void FEMEnvironment::addEnergyJump(scalar height,scalar frame,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  _ess.push_back(boost::shared_ptr<DDPEnergy>(new DDPJumpEnergy(getGravity(),height,frame,coef,dim)));
}
void FEMEnvironment::addEnergyWalk(const Vec3& spd,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  _ess.push_back(boost::shared_ptr<DDPEnergy>(new DDPWalkEnergy(spd.cast<scalarD>(),coef,dim)));
}
void FEMEnvironment::addEnergyWalkTo(const Vec3& pos,bool gT,bool gN,scalar frame,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  Vec3d gd=getGravity().cast<scalarD>();
  _ess.push_back(boost::shared_ptr<DDPEnergy>
                 (new DDPWalkToEnergy(pos.cast<scalarD>(),
                                      gT ? &gd : NULL,
                                      gN ? &gd : NULL,
                                      frame,coef,dim)));
}
void FEMEnvironment::addEnergyWalkSpeedTo(const Vec3& pos,bool gT,bool gN,scalar frame,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  Vec3d gd=getGravity().cast<scalarD>();
  _ess.push_back(boost::shared_ptr<DDPEnergy>
                 (new DDPWalkSpeedToEnergy(pos.cast<scalarD>(),
                     gT ? &gd : NULL,
                     gN ? &gd : NULL,
                     frame,coef,dim)));
}
void FEMEnvironment::addEnergy2DRoll(scalar spd,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  _ess.push_back(boost::shared_ptr<DDPEnergy>(new DDP2DRollEnergy(spd,coef,dim)));
}
void FEMEnvironment::addEnergy3DRoll(const Vec3& spd,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  _ess.push_back(boost::shared_ptr<DDPEnergy>(new DDP3DRollEnergy(spd.cast<scalarD>(),coef,dim)));
}
void FEMEnvironment::addEnergy3DOrientation(const Vec3& dirL,const Vec3& rot,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  _ess.push_back(boost::shared_ptr<DDPEnergy>(new DDP3DOrientationEnergy(dirL.cast<scalarD>(),rot.cast<scalarD>(),coef,dim)));
}
void FEMEnvironment::addEnergy3DOrientationTo(const Vec3& dirL,scalar frame,scalar coef)
{
  sizeType dim=_sol->getBody().dim();
  _ess.push_back(boost::shared_ptr<DDPEnergy>(new DDP3DOrientationToEnergy(dirL.cast<scalarD>(),frame,coef,dim)));
}
void FEMEnvironment::addEnergyCurvedWalk(const Vec3& dirL,const Vec3& rot,scalar coef,sizeType mtId)
{
  sizeType dim=_sol->getBody().dim();
  boost::shared_ptr<DDPCurvedWalkEnergy> e;
  e.reset(new DDPCurvedWalkEnergy(dirL.cast<scalarD>(),rot.cast<scalarD>(),coef,dim));
  e->setMTId(mtId);
  _ess.push_back(e);
}
const vector<boost::shared_ptr<DDPEnergy> >& FEMEnvironment::getEnergy() const
{
  return _ess;
}
void FEMEnvironment::clearEnergy()
{
  _ess.clear();
}
void FEMEnvironment::debugPeriodic()
{
  Vec S0=sampleS0(),E,E2;
  S0.setRandom();

  sizeType dim=_sol->getBody().dim();
  sizeType nrS=_sol->getMCalc().size();
  scalar dt=_sol->_tree.get<scalar>("dt");
  _sol->_xN.reset(_sol->getMCalc(),Vec::Random(nrS));
  _sol->_x .reset(_sol->getMCalc(),Vec::Random(nrS));
  _sol->_xP.reset(_sol->getMCalc(),Vec::Random(nrS));
  _sol->computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);

#define NR_TEST 10
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec xP=_sol->_xP.x();
    xP.segment(nrS-6,dim).setRandom();
    _sol->_xP.reset(_sol->getMCalc(),xP);
    _sol->computePhysicsDerivativeCIO(dt,E2,NULL,NULL,NULL,false);
    INFOV("E: %f, x->u: %f, x->x: %f, x->r: %f",E.norm(),
          (E-E2).segment(0,nrS-6).norm(),
          (E-E2).segment(nrS-6,3).norm(),
          (E-E2).segment(nrS-3,3).norm())
  }
#undef NR_TEST
}
sizeType FEMEnvironment::K() const
{
  return 1;
}
NeuralNetPolicy& FEMEnvironment::getPolicy()
{
  if(!_policy) {
    //create neural network
    sizeType nrS=_sol->getMCalc().size(),nrU=nrS-6;
    sizeType nrDMP=_sol->_tree.get<sizeType>("DMPBasis",0);
    if(nrDMP > 0) {
      if(_sol->_tree.get<bool>("inNeuralNetDebug",false))
        _policy.reset(new NeuralNetPolicyPositiveCov(0,0,0,1,nrU,NULL,&(_sol->_tree)));
      else _policy.reset(new NeuralNetPolicyPositiveCov(nrU,nrDMP,&(_sol->_tree)));
      //disable exploration
      getPolicy().setActionCov(Vec::Zero(nrU));
    } else {
      scalar dt=_sol->_tree.get<scalar>("dt");
      sizeType nrH1=_sol->_tree.get<sizeType>("nrH1",40);
      sizeType nrH2=_sol->_tree.get<sizeType>("nrH2",40);
      sizeType nrH3=_sol->_tree.get<sizeType>("nrH3",0);
      bool defOnly=_sol->_tree.get<bool>("defoFeatureOnly",true);
      boost::shared_ptr<FeatureTransformLayer> feature;
      if(_sol->_tree.get<bool>("useRSFeature",true))
        feature.reset(new FEMRSFeatureTransformLayer(nrS*2,dt,_sol->getBody().dim(),false,defOnly));
      boost::shared_ptr<Vec> scale;
      if(_sol->_tree.get_optional<scalar>("scaleAction"))
        scale.reset(new Vec(Vec::Constant(nrU,_sol->_tree.get<scalar>("scaleAction"))));
      _policy.reset(new NeuralNetPolicyPositiveCov(nrH1,nrH2,nrH3,nrS*2,nrU,scale.get(),&(_sol->_tree),feature));
      //encourage exploration
      scalar confidence=_sol->_tree.get<scalar>("confidence",0.9f);
      getPolicy().setActionCov(0.5f,confidence);
    }
  }
  return dynamic_cast<NeuralNetPolicy&>(*_policy);
}
bool FEMEnvironment::isTerminal(sizeType i,const Vec& state) const
{
  sizeType nrS=_sol->getMCalc().size();
  scalar thresBal=_sol->_tree.get<scalar>("avoidFallingThres",0.0f);
  const DDPBalanceEnergy* bal=getEnergyType<DDPBalanceEnergy>();
  if(thresBal > 0 && bal) {
    scalar coef=bal->objX(0,0,state.segment(0,nrS*2),NULL,NULL)/bal->_coef;
    thresBal=Vec2(1-cos(thresBal),sin(thresBal)).squaredNorm();
    if(coef > thresBal)
      return true;
  }
  if(_sol->_tree.get<bool>("avoidSelfCollision",false)) {
    const RigidReducedMCalculator& M=_sol->getMCalc();
    _sol->_xN.reset(M,state.segment(0,nrS));
    _sol->_x.reset(M,state.segment(nrS,nrS));
    if(_sol->hasSelfColl())
      return true;
  }
  return false;
}
FEMEnvironment::Vec FEMEnvironment::sampleA(sizeType i,const Vec& S)
{
  Vec ret;
  NeuralNetPolicy& policy=getPolicy();
  try {
    DMPNeuralNet& netDMP=dynamic_cast<DMPNeuralNet&>(*(policy._net));
    Vec t=Vec::Zero(1);
    t[0]=_sol->_tree.get<scalar>("dt")*(scalarD)i;
    ret=netDMP.foreprop(t);
  } catch(...) {
    ret=Environment::sampleA(i,S);
  }
  //control bounds
  boost::optional<scalar> weight=_sol->_tree.get_optional<scalar>("maxWeight");
  if(weight) {
    return ret**weight;
  } else return ret;
}
FEMEnvironment::Vec FEMEnvironment::transfer(sizeType i,const Vec& A)
{
  const RigidReducedMCalculator& M=_sol->getMCalc();
  scalar dt=_sol->_tree.get<scalar>("dt");
  sizeType nrS=M.size();

  _sol->setControlInput(A);
  _sol->_xN.reset(M,_state.segment(0,nrS));
  _sol->_x.reset(M,_state.segment(nrS,nrS));
  if(_sol->_tree.get<bool>("advanceImplicit",true))
    _sol->advanceImplicit(dt);
  else _sol->FEMLCPSolver::advance(dt);
  return _state=extractState();
}
scalarD FEMEnvironment::getTrajCost(const Matd& states) const
{
  scalarD cost=0;
  scalar dt=_sol->_tree.get<scalar>("dt");
  for(sizeType i=0; i<(sizeType)_ess.size(); i++)
    cost+=_ess[i]->trajCost(dt,states);
  return cost;
}
scalarD FEMEnvironment::getCost(sizeType i,const Vec& state,const Vec&,Vec* DRDA,Matd* DDRDDA) const
{
  sizeType nrS=_sol->getMCalc().size();
  scalar dt=_sol->_tree.get<scalar>("dt");
  scalarD ret=0;

  Vec stateSeg=state.segment(0,nrS*2);
  if(DRDA)DRDA->setZero(stateSeg.size());
  if(DDRDDA)DDRDDA->setZero(stateSeg.size(),stateSeg.size());
  for(sizeType e=0; e<(sizeType)_ess.size(); e++)
    ret+=_ess[e]->objX(i,dt,stateSeg,DRDA,DDRDDA);
  return ret;
}
FEMEnvironment::Vec FEMEnvironment::getCostF(sizeType i,const Vec& state,const Vec&,Matd* FJac) const
{
  sizeType nrS=_sol->getMCalc().size();
  scalarD nrF=nrCostF();

  Vec F=Vec::Zero(nrF);
  Vec stateSeg=state.segment(0,nrS*2);
  if(FJac)FJac->setZero(nrF,stateSeg.size());
  //compute
  nrF=0;
  scalar dt=_sol->_tree.get<scalar>("dt");
  for(sizeType e=0; e<(sizeType)_ess.size(); e++) {
    _ess[e]->objXF(i,dt,stateSeg,F,FJac,nrF);
    nrF+=_ess[e]->nrF();
  }
  return F;
}
sizeType FEMEnvironment::nrCostF() const
{
  sizeType nrF=0;
  for(sizeType i=0; i<(sizeType)_ess.size(); i++)
    nrF+=_ess[i]->nrF();
  return nrF;
}
FEMEnvironment::Vec FEMEnvironment::sampleS0Rot(Vec3 rot)
{
  Vec3 rotVel=Vec3::Random()*_sol->_tree.get<scalar>("initVelRotScale",0.0f);
  Vec3 vel=Vec3::Random()*_sol->_tree.get<scalar>("initVelScale",0.0f);
  if(_sol->getBody().dim() == 2) {
    rotVel[0]=rotVel[1]=0;
    rot[0]=rot[1]=0;
    vel[2]=0;
  }

  Mat4 T=Mat4::Identity();
  T.block<3,3>(0,0)=makeRotation<scalar>(rot);
  _sol->initializeTrans(T);
  if(_sol->_tree.get<bool>("moveAboveFloor",true)) {
    T.block<3,1>(0,3)=moveAboveFloor().block<3,1>(0,3);
    _sol->initializeTrans(T);
  }
  _sol->initializeVel(vel,_sol->_tree.get<scalar>("dt"));
  _sol->initializeRotVel(rotVel,_sol->_tree.get<scalar>("dt"));
  return extractState();
}
FEMEnvironment::Vec FEMEnvironment::sampleS0()
{
  Vec3 rot=Vec3::Random()*_sol->_tree.get<scalar>("initRotScale",15.0f)*(M_PI/180.0f)+getInitRot();
  return _state=sampleS0Rot(rot);
}
FEMEnvironment::FEMEnvironment(const string& name):Environment(name) {}
Vec3 FEMEnvironment::getInitRot() const
{
  Vec3 ret(_sol->_tree.get<scalar>("initRotX",0),
           _sol->_tree.get<scalar>("initRotY",0),
           _sol->_tree.get<scalar>("initRotZ",0));
  return ret;
}
FEMEnvironment::Vec FEMEnvironment::extractState() const
{
  sizeType nrS=_sol->getMCalc().size();
  Vec ret=Vec::Zero(nrS*2);
  ret.segment(0,nrS)=_sol->_xN.x();
  ret.segment(nrS,nrS)=_sol->_x.x();
  return ret;
}
