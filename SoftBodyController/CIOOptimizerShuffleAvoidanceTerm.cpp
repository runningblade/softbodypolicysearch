#include "CIOOptimizerShuffleAvoidanceTerm.h"
#include <Dynamics/FEMCIOEngine.h>

USE_PRJ_NAMESPACE

//Shuffle Avoidance Term
CIOOptimizerShuffleAvoidanceTerm::CIOOptimizerShuffleAvoidanceTerm(CIOOptimizer& opt):CIOOptimizerTerm(opt) {}
scalarD CIOOptimizerShuffleAvoidanceTerm::runSQP(FEMCIOEngine& eng,sizeType i,Vec& DFDXI,Matd* DDFDDXI) const
{
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wSA=_opt._sol._tree.get<scalar>("regShuffleAvoidance",0);
  bool crossShuffle=_opt._sol._tree.get<bool>("shuffleAvoidanceCrossTerm",true);

  Vec DFDX;
  Matd DDFDDX;

  //compute shuffle avoidance
  scalarD ret=eng.computeShuffleAvoidanceCIO(dt,&DFDX,DDFDDXI ? &DDFDDX : NULL,crossShuffle);
  DFDXI+=DFDX*wSA;
  if(DDFDDXI)
    *DDFDDXI+=DDFDDX*wSA;
  return ret*wSA;
}
bool CIOOptimizerShuffleAvoidanceTerm::valid() const
{
  scalar wSA=_opt._sol._tree.get<scalar>("regShuffleAvoidance",0);
  return wSA > 0;
}
//Conic Shuffle Avoidance Term
CIOOptimizerConicShuffleAvoidanceTerm::CIOOptimizerConicShuffleAvoidanceTerm(CIOOptimizer& opt):CIOOptimizerTerm(opt) {}
scalarD CIOOptimizerConicShuffleAvoidanceTerm::runSQP(FEMCIOEngine& eng,sizeType i,Vec& DFDXI,Matd* DDFDDXI) const
{
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wSA=_opt._sol._tree.get<scalar>("regConicShuffleAvoidance",0);
  scalar muSA=_opt._sol._tree.get<scalar>("muConicShuffleAvoidance",1);
  bool crossShuffle=_opt._sol._tree.get<bool>("shuffleAvoidanceCrossTerm",true);

  Vec DFDX;
  Matd DDFDDX;

  //compute shuffle avoidance
  scalarD ret=eng.computeConicShuffleAvoidanceCIO(dt,muSA,&DFDX,DDFDDXI ? &DDFDDX : NULL,crossShuffle);
  DFDXI+=DFDX*wSA;
  if(DDFDDXI)
    *DDFDDXI+=DDFDDX*wSA;
  return ret*wSA;
}
bool CIOOptimizerConicShuffleAvoidanceTerm::valid() const
{
  scalar wCSA=_opt._sol._tree.get<scalar>("regConicShuffleAvoidance",0);
  return wCSA;
}
