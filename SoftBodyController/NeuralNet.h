#ifndef NEURAL_NET_H
#define NEURAL_NET_H

#include <CMAES/ParallelMatVec.h>
#include <CommonFile/MathBasic.h>
#include <CommonFile/solvers/Objective.h>
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

class TrainingData;
class BLEICInterface;
class CompositeLayerInPlace;
//Custom Machine Learning model wrapper
struct NeuralLayer : public Serializable {
  typedef vector<Matd,Eigen::aligned_allocator<Matd> > Mss;
  typedef Cold Vec;
public:
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual NeuralLayer& operator=(const NeuralLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const=0;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL);
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const;
  virtual void configOffline(const boost::property_tree::ptree& pt);
  virtual void configOnline(const boost::property_tree::ptree& pt);
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType nrOutput(sizeType lastIn) const;
  virtual Matd getBounds(const boost::property_tree::ptree& pt) const;
  void setLayerName(const string& name);
  const string& layerName() const;
  static void enforceSize(Matd& x,sizeType r,sizeType c);
  static void enforceSize(Matd& x,const Matd& fx);
protected:
  NeuralLayer(const string& name);
  string _layerName;
};
struct ScaleLayer : public NeuralLayer {
  ScaleLayer();
  ScaleLayer(const Vec& scale);
  const Vec& getScale() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual ScaleLayer& operator=(const ScaleLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
protected:
  Vec _scale;
};
struct FeatureTransformLayer : public NeuralLayer {
  virtual sizeType nrF() const=0;
  virtual sizeType nrInput() const=0;
  virtual sizeType nrOutput(sizeType lastIn) const;
protected:
  FeatureTransformLayer(const string& name);
};
struct InnerProductLayer : public FeatureTransformLayer {
  InnerProductLayer();
  InnerProductLayer(sizeType in,sizeType out);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual InnerProductLayer& operator=(const InnerProductLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType nrF() const;
  virtual sizeType nrInput() const;
protected:
  Matd _P;
  Vec _D;
  ParallelMatVec<Matd> _DEDP;
  ParallelMatVec<Vec> _DEDD;
};
struct ActivationLayer : public NeuralLayer {
  ActivationLayer(const string& name);
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
protected:
  virtual scalarD f(scalarD x) const=0;
  virtual scalarD df(scalarD x) const=0;
  virtual scalarD ddf(scalarD x) const=0;
};
struct BNLLLayer : public ActivationLayer {
  BNLLLayer();
  virtual boost::shared_ptr<Serializable> copy() const;
protected:
  virtual scalarD f(scalarD x) const;
  virtual scalarD df(scalarD x) const;
  virtual scalarD ddf(scalarD x) const;
};
struct ReLULayer : public ActivationLayer {
  ReLULayer();
  virtual boost::shared_ptr<Serializable> copy() const;
protected:
  virtual scalarD f(scalarD x) const;
  virtual scalarD df(scalarD x) const;
  virtual scalarD ddf(scalarD x) const;
};
struct TanHLayer : public ActivationLayer {
  TanHLayer();
  virtual boost::shared_ptr<Serializable> copy() const;
protected:
  virtual scalarD f(scalarD x) const;
  virtual scalarD df(scalarD x) const;
  virtual scalarD ddf(scalarD x) const;
};
struct LossLayer : public NeuralLayer {
  LossLayer(const string& name);
};
struct EuclideanLossLayer : public LossLayer {
  EuclideanLossLayer();
  EuclideanLossLayer(const string& name);
  Matd& actions();
  const Matd& actions() const;
  virtual EuclideanLossLayer& operator=(const EuclideanLossLayer& other);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
protected:
  Matd _actions;
};
struct ActionIdLossLayer : public LossLayer {
  ActionIdLossLayer();
  Coli& actionIds();
  const Coli& actionIds() const;
  Vec& actions();
  const Vec& actions() const;
  virtual ActionIdLossLayer& operator=(const ActionIdLossLayer& other);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
private:
  Coli _actionId;
  Vec _actions;
};
struct ScaledEuclideanLossLayer : public EuclideanLossLayer {
  typedef vector<Matd,Eigen::aligned_allocator<Matd> > Mss;
  ScaledEuclideanLossLayer();
  vector<Matd,Eigen::aligned_allocator<Matd> >& metrics();
  const vector<Matd,Eigen::aligned_allocator<Matd> >& metrics() const;
  virtual ScaledEuclideanLossLayer& operator=(const ScaledEuclideanLossLayer& other);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
protected:
  Matd computeDeriv(const Matd& x) const;
  Mss _metric;
};
struct UniformScaledEuclideanLossLayer : public EuclideanLossLayer {
  UniformScaledEuclideanLossLayer();
  Matd& metric();
  const Matd& metric() const;
  virtual UniformScaledEuclideanLossLayer& operator=(const UniformScaledEuclideanLossLayer& other);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
protected:
  Matd _metric;
};
struct NeuralNet : public Serializable, public Objective<scalarD> {
  typedef Objective<scalarD>::Vec Vec;
  typedef NeuralLayer::Mss Mss;
  typedef vector<void*> UserData;
  friend class CompositeLayerInPlace;
  friend class OpenMPNNObjective;
  friend class NNObjective;
  NeuralNet();
  NeuralNet(boost::shared_ptr<CompositeLayerInPlace> composite,sizeType nrAction,
            const boost::property_tree::ptree* pt=NULL);
  NeuralNet(const vector<Vec4i,Eigen::aligned_allocator<Vec4i> >& imgs,sizeType nrAction,
            const boost::property_tree::ptree* pt=NULL);
  NeuralNet(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,sizeType nrAction,
            const boost::property_tree::ptree* pt=NULL,const Vec* scale=NULL,
            boost::shared_ptr<FeatureTransformLayer> feature=NULL);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual NeuralNet& operator=(const NeuralNet& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual boost::shared_ptr<CompositeLayerInPlace> getCompositeLayer(sizeType l);
  virtual boost::shared_ptr<CompositeLayerInPlace> toCompositeLayer() const;
  static void registerNewLayerType(boost::shared_ptr<Serializable> type);
  static void registerLayerTypes(IOData* dat);
  //setup data
  void clearData();
  void setData(const TrainingData& data,const Mss* metrics=NULL,const Matd* metric=NULL);
  void setData(const Matd& states,const Matd& actions);
  void setData(const Matd& states,const Vec& actions,const Coli& actionIds);
  void setData(const Matd& states,const Matd& actions,const Mss& metrics);
  void setData(const Matd& states,const Matd& actions,const Matd& metric);
  //main functionality
  virtual void train(const UserData* dat=NULL);
  virtual void trainRMSProp(Matd states,Matd actions);
  //user data
  UserData createUserData() const;
  void setUserData(UserData& dat,sizeType l,void* data) const;
  void setUserDataByName(UserData& dat,const string& name,void* data) const;
  //F (possibly with DFDX)
  Matd& foreprop(const Matd* in,const UserData* dat=NULL);
  Matd& foreprop(const Vec& in,const UserData* dat=NULL);
  //DFDW
  Matd& backprop(const Matd* in,const UserData* dat=NULL);
  Matd& backprop(const Vec& in,const UserData* dat=NULL);
  //DFDWT
  Matd& weightprop(const Matd* in,const UserData* dat=NULL);
  Matd& weightprop(const Vec& in,const UserData* dat=NULL);
  //debug code
  void debugWeightprop(const TrainingData& datum,bool ELoss,bool QLoss);
  void debugForeBackpropDMP(const TrainingData& datum,const Mss* metrics=NULL,const Matd* metric=NULL);
  void debugForeBackprop(const TrainingData& datum,const Mss* metrics=NULL,const Matd* metric=NULL,bool DFDX=true);
  void debugPerformance();
  void debugDFDW();
  void debugDFDX();
  //misc
  template <typename T> const T* getLastLayer(sizeType* id=NULL) const {
    if(id)
      *id=(sizeType)_layers.size()-1;
    return getLayer<T>((sizeType)_layers.size()-1);
  }
  template <typename T> const T* getLayer(sizeType l) const {
    return boost::dynamic_pointer_cast<T>(_layers[l]).get();
  }
  template <typename T> const T* getLayerByName(const string& name,sizeType* id=NULL) const {
    for(sizeType i=0; i<(sizeType)_layers.size(); i++)
      if(_layers[i]->layerName() == name) {
        if(id)
          *id=i;
        return boost::dynamic_pointer_cast<T>(_layers[i]).get();
      }
    if(id)
      *id=-1;
    return NULL;
  }
  template <typename T> T* getLastLayer(sizeType* id=NULL) {
    return const_cast<T*>(const_cast<const NeuralNet*>(this)->getLastLayer<T>());
  }
  template <typename T> T* getLayer(sizeType l) {
    return const_cast<T*>(const_cast<const NeuralNet*>(this)->getLayer<T>(l));
  }
  template <typename T> T* getLayerByName(const string& name,sizeType* id=NULL) {
    return const_cast<T*>(const_cast<const NeuralNet*>(this)->getLayerByName<T>(name,id));
  }
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient);
  virtual int inputs() const;
  sizeType dataSize() const;
  sizeType nrParam() const;
  sizeType nrInput() const;
  sizeType nrOutput() const;
  Matd getBounds() const;
  void resetOffline();  //this resets all the learned parameters
  void resetOnline();  //this resets all the online parameters
  Vec toVec(bool diff) const;
  const Matd& toVecStateDiff(bool diff) const;
  void fromVec(const Vec& weights,bool diff=false);
  void fromVecStateDiff(const Matd& states,bool diff=false);
  //save,load foremost layers with no parameters
  void saveDummyLayer(const UserData* dat=NULL);
  void loadDummyLayer();
  //save,load setting
  void saveForeprop(NeuralNet& net) const;
  void loadForeprop(const NeuralNet& net);
  static boost::shared_ptr<ActivationLayer> getActivationLayer(const string& type);
  boost::property_tree::ptree _tree;
protected:
  //helper
  NeuralNet(const string& name);
  void copyMultithread();
  void setSolverParam(BLEICInterface& sol) const;
  template <typename T> void removeLayer();
  sizeType addActivationLayer(const string& name,const string& type,sizeType lastIn);
  sizeType addLayer(const string& name,boost::shared_ptr<NeuralLayer> layer,sizeType lastIn);
  vector<boost::shared_ptr<NeuralLayer> > _layers;
  //multithreaded cache
  vector<Mss> _foreBlocks,_backBlocks;
  sizeType _index,_iter;
};

PRJ_END

#endif
