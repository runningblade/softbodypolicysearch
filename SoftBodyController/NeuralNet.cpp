#include "NeuralNet.h"
#include "CNNLayer.h"
#include "DMPLayer.h"
#include "FEMFeature.h"
#include "DMPLayerMT.h"
#include "TrainingData.h"
#include "OneLineCallback.h"
#include "NNBLEICInterface.h"
#include <Dynamics/FEMUtils.h>
#include <CommonFile/solvers/Minimizer.h>
#include <CommonFile/Timing.h>
#include <CMAES/MLUtil.h>
#include <set>

USE_PRJ_NAMESPACE

//Layer Interface
bool NeuralLayer::read(istream& is,IOData* dat)
{
  readBinaryData(_layerName,is);
  return is.good();
}
bool NeuralLayer::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_layerName,os);
  return os.good();
}
NeuralLayer& NeuralLayer::operator=(const NeuralLayer& other)
{
  _layerName=other._layerName;
  return *this;
}
void NeuralLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  fx=x;
}
void NeuralLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  dEdx=dEdfx;
}
void NeuralLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  dEdfx=dEdx;
}
void NeuralLayer::configOffline(const boost::property_tree::ptree& pt) {}
void NeuralLayer::configOnline(const boost::property_tree::ptree& pt) {}
void NeuralLayer::setParam(const Vec& p,sizeType off,bool diff) {}
void NeuralLayer::param(Vec& p,sizeType off,bool diff) const {}
sizeType NeuralLayer::nrParam() const
{
  return 0;
}
sizeType NeuralLayer::nrOutput(sizeType lastIn) const
{
  Matd in=Matd::Zero(lastIn,1),out;
  foreprop(in,out);
  return out.rows();
}
Matd NeuralLayer::getBounds(const boost::property_tree::ptree& pt) const
{
  ASSERT_MSGV(false,"Layer %s does not support getBounds()!",typeid(*this).name())
  return Matd::Zero(nrParam(),2);
}
void NeuralLayer::setLayerName(const string& name)
{
  _layerName=name;
}
const string& NeuralLayer::layerName() const
{
  return _layerName;
}
//helper
void NeuralLayer::enforceSize(Matd& x,sizeType r,sizeType c)
{
  if(x.rows() != r || x.cols() != c)
    x.resize(r,c);
}
void NeuralLayer::enforceSize(Matd& x,const Matd& fx)
{
  enforceSize(x,fx.rows(),fx.cols());
}
NeuralLayer::NeuralLayer(const string& name):Serializable(name) {}
//ScaleLayer
ScaleLayer::ScaleLayer():NeuralLayer(typeid(ScaleLayer).name()) {}
ScaleLayer::ScaleLayer(const Vec& scale):NeuralLayer(typeid(ScaleLayer).name()),_scale(scale) {}
const ScaleLayer::Vec& ScaleLayer::getScale() const
{
  return _scale;
}
bool ScaleLayer::read(istream& is,IOData* dat)
{
  NeuralLayer::read(is,dat);
  readBinaryData(_scale,is);
  return is.good();
}
bool ScaleLayer::write(ostream& os,IOData* dat) const
{
  NeuralLayer::write(os,dat);
  writeBinaryData(_scale,os);
  return os.good();
}
ScaleLayer& ScaleLayer::operator=(const ScaleLayer& other)
{
  NeuralLayer::operator=(other);
  _scale=other._scale;
  return *this;
}
boost::shared_ptr<Serializable> ScaleLayer::copy() const
{
  boost::shared_ptr<ScaleLayer> ret(new ScaleLayer);
  *ret=*this;
  return ret;
}
void ScaleLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  Eigen::DiagonalMatrix<scalarD,-1,-1> diag;
  diag.diagonal()=_scale;
  fx=diag*x;
}
void ScaleLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  Eigen::DiagonalMatrix<scalarD,-1,-1> diag;
  diag.diagonal()=_scale;
  dEdx=diag*dEdfx;
}
void ScaleLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  Eigen::DiagonalMatrix<scalarD,-1,-1> diag;
  diag.diagonal()=_scale;
  dEdfx=diag*dEdx;
}
//FeatureTransformLayer
sizeType FeatureTransformLayer::nrOutput(sizeType lastIn) const
{
  ASSERT(lastIn == nrInput())
  return nrF();
}
FeatureTransformLayer::FeatureTransformLayer(const string& name):NeuralLayer(name) {}
//InnerProductLayer
InnerProductLayer::InnerProductLayer()
  :FeatureTransformLayer(typeid(InnerProductLayer).name()) {}
InnerProductLayer::InnerProductLayer(sizeType in,sizeType out)
  :FeatureTransformLayer(typeid(InnerProductLayer).name())
{
  _P.setZero(out,in);
  _D.setZero(out);

  _DEDP.get()=_P;
  _DEDD.get()=_D;
}
bool InnerProductLayer::read(istream& is,IOData* dat)
{
  NeuralLayer::read(is,dat);
  readBinaryData(_P,is);
  readBinaryData(_D,is);
  return is.good();
}
bool InnerProductLayer::write(ostream& os,IOData* dat) const
{
  NeuralLayer::write(os,dat);
  writeBinaryData(_P,os);
  writeBinaryData(_D,os);
  return os.good();
}
InnerProductLayer& InnerProductLayer::operator=(const InnerProductLayer& other)
{
  NeuralLayer::operator=(other);
  _P=other._P;
  _D=other._D;
  _DEDP=other._DEDP;
  _DEDD=other._DEDD;
  return *this;
}
boost::shared_ptr<Serializable> InnerProductLayer::copy() const
{
  boost::shared_ptr<InnerProductLayer> ret(new InnerProductLayer);
  *ret=*this;
  return ret;
}
void InnerProductLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,_P.rows(),x.cols());
  OMP_PARALLEL_FOR_
  for(sizeType c=0; c<x.cols(); c++)
    fx.col(c)=_P*x.col(c)+_D;
}
void InnerProductLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,x);
  _DEDP.clear(dEdfx.rows(),x.rows());
  _DEDD.clear(dEdfx.rows());
  OMP_PARALLEL_FOR_
  for(sizeType c=0; c<x.cols(); c++) {
    dEdx.col(c)=_P.transpose()*dEdfx.col(c);
    _DEDP+=dEdfx.col(c)*x.col(c).transpose();
    _DEDD+=dEdfx.col(c);
  }
}
void InnerProductLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  enforceSize(dEdfx,_P.rows(),x.cols());
  OMP_PARALLEL_FOR_
  for(sizeType c=0; c<x.cols(); c++)
    dEdfx.col(c)=_P*dEdx.col(c)+_DEDP.get()*x.col(c)+_DEDD.get();
}
void InnerProductLayer::configOffline(const boost::property_tree::ptree& pt)
{
  scalarD initW=pt.get<scalarD>("initWeightNorm",1E-3f);
  INFOV("InitWeightNorm=%f",initW)
  _P.setRandom();
  _P*=initW;

  scalarD initB=pt.get<scalarD>("initBiasNorm",1E-3f);
  _D.setRandom();
  _D*=initB;
}
void InnerProductLayer::setParam(const Vec& p,sizeType off,bool diff)
{
  Eigen::Map<Vec>((diff?_DEDP.get():_P).data(),_P.size())=p.segment(off,_P.size());
  (diff?_DEDD.get():_D)=p.segment(off+_P.size(),_D.size());
}
void InnerProductLayer::param(Vec& p,sizeType off,bool diff) const
{
  p.segment(off,_P.size())=Eigen::Map<const Vec>((diff?_DEDP.get():_P).data(),_P.size());
  p.segment(off+_P.size(),_D.size())=(diff?_DEDD.get():_D);
}
sizeType InnerProductLayer::nrParam() const
{
  return _P.size()+_D.size();
}
sizeType InnerProductLayer::nrF() const
{
  return _P.rows();
}
sizeType InnerProductLayer::nrInput() const
{
  return _P.cols();
}
//ActivationLayer
ActivationLayer::ActivationLayer(const string& name):NeuralLayer(name) {}
void ActivationLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,x);
  const Eigen::ArrayWrapper<const Matd> xArr=x.array();
  Eigen::ArrayWrapper<Matd> fxArr=fx.array();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<xArr.size(); i++)
    fxArr(i)=f(xArr(i));
}
void ActivationLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,x);
  const Eigen::ArrayWrapper<const Matd> xArr=x.array();
  const Eigen::ArrayWrapper<const Matd> dEdfxArr=dEdfx.array();
  Eigen::ArrayWrapper<Matd> dEdxArr=dEdx.array();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<xArr.size(); i++)
    dEdxArr(i)=df(xArr(i))*dEdfxArr(i);
}
void ActivationLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  enforceSize(dEdfx,x);
  const Eigen::ArrayWrapper<const Matd> xArr=x.array();
  const Eigen::ArrayWrapper<const Matd> dEdxArr=dEdx.array();
  Eigen::ArrayWrapper<Matd> dEdfxArr=dEdfx.array();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<xArr.size(); i++)
    dEdfxArr(i)=dEdxArr(i)*df(xArr(i));
}
//BNLLLayer
BNLLLayer::BNLLLayer():ActivationLayer(typeid(BNLLLayer).name()) {}
boost::shared_ptr<Serializable> BNLLLayer::copy() const
{
  return boost::shared_ptr<BNLLLayer>(new BNLLLayer);
}
scalarD BNLLLayer::f(scalarD x) const
{
  scalarD fx=0;
  if(x > 0)
    fx=x+log(1+exp(-x));
  else fx=log(1+exp(x));
  return fx;
}
scalarD BNLLLayer::df(scalarD x) const
{
  scalarD val=0;
  if(x > 0)
    val=1-exp(-x)/(1+exp(-x));
  else val=exp(x)/(1+exp(x));
  return val;
}
scalarD BNLLLayer::ddf(scalarD x) const
{
  scalarD ret=df(x);
  return ret-ret*ret;
}
//ReLULayer
ReLULayer::ReLULayer():ActivationLayer(typeid(ReLULayer).name()) {}
boost::shared_ptr<Serializable> ReLULayer::copy() const
{
  return boost::shared_ptr<ReLULayer>(new ReLULayer);
}
scalarD ReLULayer::f(scalarD x) const
{
  return max<scalarD>(x,0);
}
scalarD ReLULayer::df(scalarD x) const
{
  return x>0?1:0;
}
scalarD ReLULayer::ddf(scalarD x) const
{
  return 0;
}
//TanHLayer
TanHLayer::TanHLayer():ActivationLayer(typeid(TanHLayer).name()) {}
boost::shared_ptr<Serializable> TanHLayer::copy() const
{
  return boost::shared_ptr<TanHLayer>(new TanHLayer);
}
scalarD TanHLayer::f(scalarD x) const
{
  return tanh(x);
}
scalarD TanHLayer::df(scalarD x) const
{
  return 1-pow(tanh(x),2);
}
scalarD TanHLayer::ddf(scalarD x) const
{
  scalarD ret=tanh(x);
  return -2*ret*(1-ret*ret);
}
//LossLayer
LossLayer::LossLayer(const string& name):NeuralLayer(name) {}
//EuclideanLossLayer
EuclideanLossLayer::EuclideanLossLayer():LossLayer(typeid(EuclideanLossLayer).name()) {}
EuclideanLossLayer::EuclideanLossLayer(const string& name):LossLayer(name) {}
Matd& EuclideanLossLayer::actions()
{
  return _actions;
}
const Matd& EuclideanLossLayer::actions() const
{
  return _actions;
}
EuclideanLossLayer& EuclideanLossLayer::operator=(const EuclideanLossLayer& other)
{
  LossLayer::operator=(other);
  _actions=other._actions;
  return *this;
}
bool EuclideanLossLayer::read(istream& is,IOData* dat)
{
  LossLayer::read(is,dat);
  readBinaryData(_actions,is,dat);
  return is.good();
}
bool EuclideanLossLayer::write(ostream& os,IOData* dat) const
{
  LossLayer::write(os,dat);
  writeBinaryData(_actions,os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> EuclideanLossLayer::copy() const
{
  boost::shared_ptr<EuclideanLossLayer> ret(new EuclideanLossLayer);
  *ret=*this;
  return ret;
}
void EuclideanLossLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,1,1);
  if(_actions.rows() != x.rows() || _actions.cols() != x.cols())
    const_cast<Matd&>(_actions)=x;
  fx(0,0)=(x-_actions).squaredNorm()/(scalarD)x.cols();
}
void EuclideanLossLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,x);
  scalarD coef=2/(scalarD)x.cols();
  dEdx=(x-_actions)*(dEdfx(0,0)*coef);
}
void EuclideanLossLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  enforceSize(dEdfx,1,1);
  dEdfx(0,0)=(dEdx.transpose()*(x-_actions)).trace()*(2/(scalarD)x.cols());
}
//ActionIdLossLayer
ActionIdLossLayer::ActionIdLossLayer():LossLayer(typeid(ActionIdLossLayer).name()) {}
Coli& ActionIdLossLayer::actionIds()
{
  return _actionId;
}
const Coli& ActionIdLossLayer::actionIds() const
{
  return _actionId;
}
ActionIdLossLayer::Vec& ActionIdLossLayer::actions()
{
  return _actions;
}
const ActionIdLossLayer::Vec& ActionIdLossLayer::actions() const
{
  return _actions;
}
ActionIdLossLayer& ActionIdLossLayer::operator=(const ActionIdLossLayer& other)
{
  LossLayer::operator=(other);
  _actionId=other._actionId;
  _actions=other._actions;
  return *this;
}
bool ActionIdLossLayer::read(istream& is,IOData* dat)
{
  LossLayer::read(is,dat);
  readBinaryData(_actionId,is,dat);
  readBinaryData(_actions,is,dat);
  return is.good();
}
bool ActionIdLossLayer::write(ostream& os,IOData* dat) const
{
  LossLayer::write(os,dat);
  writeBinaryData(_actionId,os,dat);
  writeBinaryData(_actions,os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> ActionIdLossLayer::copy() const
{
  boost::shared_ptr<ActionIdLossLayer> ret(new ActionIdLossLayer);
  *ret=*this;
  return ret;
}
void ActionIdLossLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,1,1);
  if(_actionId.size() != x.cols())
    const_cast<Coli&>(_actionId).setZero(x.cols());
  if(_actions.size() != x.cols())
    const_cast<Vec&>(_actions).setZero(x.cols());
  Vec xMapped=_actions;
  for(sizeType i=0; i<_actions.size(); i++)
    xMapped[i]=x(_actionId[i],i)-_actions[i];
  fx(0,0)=xMapped.squaredNorm()/(scalarD)x.cols();
}
void ActionIdLossLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,x);
  dEdx.setZero();
  scalarD coef=dEdfx(0,0)*2/(scalarD)x.cols();
  for(sizeType i=0; i<_actions.size(); i++)
    dEdx(_actionId[i],i)=(x(_actionId[i],i)-_actions[i])*coef;
}
void ActionIdLossLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  enforceSize(dEdfx,1,1);
  dEdfx.setZero();
  scalarD coef=2/(scalarD)x.cols();
  for(sizeType i=0; i<_actions.size(); i++)
    dEdfx(0,0)+=dEdx(_actionId[i],i)*(x(_actionId[i],i)-_actions[i])*coef;
}
//ScaledEuclideanLossLayer
ScaledEuclideanLossLayer::ScaledEuclideanLossLayer():EuclideanLossLayer(typeid(ScaledEuclideanLossLayer).name()) {}
vector<Matd,Eigen::aligned_allocator<Matd> >& ScaledEuclideanLossLayer::metrics()
{
  return _metric;
}
const vector<Matd,Eigen::aligned_allocator<Matd> >& ScaledEuclideanLossLayer::metrics() const
{
  return _metric;
}
ScaledEuclideanLossLayer& ScaledEuclideanLossLayer::operator=(const ScaledEuclideanLossLayer& other)
{
  EuclideanLossLayer::operator=(other);
  _metric=other._metric;
  return *this;
}
bool ScaledEuclideanLossLayer::read(istream& is,IOData* dat)
{
  EuclideanLossLayer::read(is,dat);
  readVector(_metric,is,dat);
  return is.good();
}
bool ScaledEuclideanLossLayer::write(ostream& os,IOData* dat) const
{
  EuclideanLossLayer::write(os,dat);
  writeVector(_metric,os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> ScaledEuclideanLossLayer::copy() const
{
  boost::shared_ptr<ScaledEuclideanLossLayer> ret(new ScaledEuclideanLossLayer);
  *ret=*this;
  return ret;
}
void ScaledEuclideanLossLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,1,1);
  if(_actions.rows() != x.rows() || _actions.cols() != x.cols()) {
    const_cast<Mss&>(_metric).assign(1,Matd::Identity(x.rows(),x.rows()));
    const_cast<Matd&>(_actions)=x;
  }
  Matd deriv=computeDeriv(x);
  fx(0,0)=(deriv.transpose()*(x-_actions)).trace()/(scalarD)x.cols();
}
void ScaledEuclideanLossLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,x);
  Matd deriv=computeDeriv(x);
  dEdx=deriv*(dEdfx(0,0)*2/(scalarD)x.cols());
}
void ScaledEuclideanLossLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  enforceSize(dEdfx,1,1);
  Matd deriv=computeDeriv(x);
  dEdfx(0,0)=(dEdx.transpose()*deriv).trace()*(2/(scalarD)x.cols());
}
Matd ScaledEuclideanLossLayer::computeDeriv(const Matd& x) const
{
  Matd deriv;
  enforceSize(deriv,x.rows(),x.cols());

  sizeType nrA=_actions.cols();
  sizeType nrM=(sizeType)_metric.size();
  ASSERT_MSGV((nrA%nrM) == 0,
              "You must have: _actions.cols()%(sizeType)_metric.size() == 0,"
              " _actions.cols()=%ld, _metric.size()=%ld",(nrA%nrM))

  sizeType blk=nrA/nrM;
  for(sizeType i=0; i<nrM; i++)
    deriv.block(0,i*blk,x.rows(),blk)=
      _metric[i]*(x-_actions).block(0,i*blk,x.rows(),blk);
  return deriv;
}
//UniformScaledEuclideanLossLayer
UniformScaledEuclideanLossLayer::UniformScaledEuclideanLossLayer():EuclideanLossLayer(typeid(UniformScaledEuclideanLossLayer).name()) {}
Matd& UniformScaledEuclideanLossLayer::metric()
{
  return _metric;
}
const Matd& UniformScaledEuclideanLossLayer::metric() const
{
  return _metric;
}
UniformScaledEuclideanLossLayer& UniformScaledEuclideanLossLayer::operator=(const UniformScaledEuclideanLossLayer& other)
{
  EuclideanLossLayer::operator=(other);
  _metric=other._metric;
  return *this;
}
bool UniformScaledEuclideanLossLayer::read(istream& is,IOData* dat)
{
  EuclideanLossLayer::read(is,dat);
  readBinaryData(_metric,is,dat);
  return is.good();
}
bool UniformScaledEuclideanLossLayer::write(ostream& os,IOData* dat) const
{
  EuclideanLossLayer::write(os,dat);
  writeBinaryData(_metric,os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> UniformScaledEuclideanLossLayer::copy() const
{
  boost::shared_ptr<UniformScaledEuclideanLossLayer> ret(new UniformScaledEuclideanLossLayer);
  *ret=*this;
  return ret;
}
void UniformScaledEuclideanLossLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,1,1);
  if(_actions.rows() != x.rows() || _actions.cols() != x.cols()) {
    const_cast<Matd&>(_metric).setIdentity(x.rows(),x.rows());
    const_cast<Matd&>(_actions)=x;
  }
  Matd deriv=_metric*(x-_actions);
  fx(0,0)=(deriv.transpose()*(x-_actions)).trace()/(scalarD)x.cols();
}
void UniformScaledEuclideanLossLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,x);
  Matd deriv=_metric*(x-_actions);
  dEdx=deriv*(dEdfx(0,0)*2/(scalarD)x.cols());
}
void UniformScaledEuclideanLossLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  enforceSize(dEdfx,1,1);
  Matd deriv=_metric*(x-_actions);
  dEdfx(0,0)=(dEdx.transpose()*deriv).trace()*(2/(scalarD)x.cols());
}

//simple neural net implementation
struct CompareName {
  bool operator()(boost::shared_ptr<Serializable> A,boost::shared_ptr<Serializable> B) {
    return (*A) < (*B);
  }
};
set<boost::shared_ptr<Serializable>,CompareName> _layerTypes;
NeuralNet::NeuralNet():Serializable(typeid(NeuralNet).name()),_index(0) {}
NeuralNet::NeuralNet(boost::shared_ptr<CompositeLayerInPlace> composite,sizeType nrAction,
                     const boost::property_tree::ptree* pt)
  :Serializable(typeid(NeuralNet).name()),_index(0)
{
  if(pt)
    _tree=*pt;
  _foreBlocks.resize(1);
  _backBlocks.resize(1);
  sizeType lastIn=composite->nrInput();
  _foreBlocks[0].push_back(Matd(lastIn,1));
  _backBlocks[0].push_back(Matd());
  lastIn=addLayer("composite",composite,lastIn);
  lastIn=addLayer("HLast",boost::shared_ptr<NeuralLayer>(new InnerProductLayer(lastIn,nrAction)),lastIn);

  foreprop(NULL);
  backprop(NULL);
  copyMultithread();
}
NeuralNet::NeuralNet(const vector<Vec4i,Eigen::aligned_allocator<Vec4i> >& imgs,sizeType nrAction,
                     const boost::property_tree::ptree* pt)
  :Serializable(typeid(NeuralNet).name()),_index(0)
{
  if(pt)
    _tree=*pt;
  string activation=_tree.get<string>("activation","BNLL");
  string activationFinal=_tree.get<string>("activationFinal","BNLL");
  Vec3i imgIn=imgs[0].segment<3>(0);
  sizeType lastIn=imgIn.prod();
  _foreBlocks.resize(1);
  _backBlocks.resize(1);
  _foreBlocks[0].push_back(Matd(lastIn,1));
  _backBlocks[0].push_back(Matd());
  for(sizeType i=1; i<(sizeType)imgs.size(); i++) {
    boost::shared_ptr<ConvolutionLayer> l(new ConvolutionLayer(imgIn.segment<2>(0),imgIn[2],imgs[i].segment<2>(0),imgs[i][2],imgs[i][3]));
    lastIn=addLayer("C"+boost::lexical_cast<string>(i),l,lastIn);
    lastIn=addActivationLayer("AC"+boost::lexical_cast<string>(i),i == (sizeType)imgs.size()-1 ? activationFinal : activation,lastIn);
    imgIn.segment<2>(0)=l->imageSizeOut();
    imgIn[2]=imgs[i][2];
  }
  if(nrAction > 0)
    lastIn=addLayer("H0",boost::shared_ptr<NeuralLayer>(new InnerProductLayer(imgIn.segment<3>(0).prod(),nrAction)),lastIn);

  foreprop(NULL);
  backprop(NULL);
  copyMultithread();
}
NeuralNet::NeuralNet(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,sizeType nrAction,
                     const boost::property_tree::ptree* pt,const Vec* scale,
                     boost::shared_ptr<FeatureTransformLayer> feature)
  :Serializable(typeid(NeuralNet).name()),_index(0)
{
  if(pt)
    _tree=*pt;
  string activation=_tree.get<string>("activation","BNLL");
  string activationFinal=_tree.get<string>("activationFinal","TanH");
  sizeType lastIn=nrState;
  _foreBlocks.resize(1);
  _backBlocks.resize(1);
  _foreBlocks[0].push_back(Matd(lastIn,1));
  _backBlocks[0].push_back(Matd());
  if(feature) {
    lastIn=addLayer("Feature",feature,lastIn);
  }
  if(nrH1 > 0) {
    lastIn=addLayer("H1",boost::shared_ptr<NeuralLayer>(new InnerProductLayer(lastIn,nrH1)),lastIn);
    lastIn=addActivationLayer("AH1",activation,lastIn);
  }
  if(nrH2 > 0) {
    lastIn=addLayer("H2",boost::shared_ptr<NeuralLayer>(new InnerProductLayer(lastIn,nrH2)),lastIn);
    lastIn=addActivationLayer("AH2",activation,lastIn);
  }
  if(nrH3 > 0) {
    lastIn=addLayer("H3",boost::shared_ptr<NeuralLayer>(new InnerProductLayer(lastIn,nrH3)),lastIn);
    lastIn=addActivationLayer("AH3",activation,lastIn);
  }
  {
    lastIn=addLayer("HF",boost::shared_ptr<NeuralLayer>(new InnerProductLayer(lastIn,nrAction)),lastIn);
    lastIn=addActivationLayer("AHF",activationFinal,lastIn);
  }
  if(scale) {
    lastIn=addLayer("scale",boost::shared_ptr<NeuralLayer>(new ScaleLayer(*scale)),lastIn);
  }

  foreprop(NULL);
  backprop(NULL);
  copyMultithread();
  resetOffline();
}
bool NeuralNet::read(istream& is,IOData* dat)
{
  _foreBlocks.resize(1);
  _backBlocks.resize(1);
  registerLayerTypes(dat);
  readPtreeBinary(_tree,is);
  readVector(_layers,is,dat);
  readVector(_foreBlocks[0],is,dat);
  readVector(_backBlocks[0],is,dat);
  copyMultithread();
  return is.good();
}
bool NeuralNet::write(ostream& os,IOData* dat) const
{
  registerLayerTypes(dat);
  writePtreeBinary(_tree,os);
  writeVector(_layers,os,dat);
  writeVector(_foreBlocks[0],os,dat);
  writeVector(_backBlocks[0],os,dat);
  return os.good();
}
NeuralNet& NeuralNet::operator=(const NeuralNet& other)
{
  _tree=other._tree;
  _layers.resize(other._layers.size());
  for(sizeType i=0; i<(sizeType)other._layers.size(); i++)
    _layers[i]=boost::dynamic_pointer_cast<NeuralLayer>(other._layers[i]->copy());
  _foreBlocks=other._foreBlocks;
  _backBlocks=other._backBlocks;
  _index=other._index;
  return *this;
}
boost::shared_ptr<Serializable> NeuralNet::copy() const
{
  return boost::shared_ptr<Serializable>(new NeuralNet);
}
boost::shared_ptr<CompositeLayerInPlace> NeuralNet::getCompositeLayer(sizeType l)
{
  return boost::dynamic_pointer_cast<CompositeLayerInPlace>(_layers[l]);
}
boost::shared_ptr<CompositeLayerInPlace> NeuralNet::toCompositeLayer() const
{
  return boost::shared_ptr<CompositeLayerInPlace>(new CompositeLayerInPlace(*this));
}
void NeuralNet::registerNewLayerType(boost::shared_ptr<Serializable> type)
{
  ASSERT(type != NULL)
  _layerTypes.insert(type->copy());
}
void NeuralNet::registerLayerTypes(IOData* dat)
{
  //register all basic types
  registerType<InnerProductLayer>(dat);
  registerType<ScaleLayer>(dat);
  registerType<BNLLLayer>(dat);
  registerType<ReLULayer>(dat);
  registerType<TanHLayer>(dat);
  registerType<EuclideanLossLayer>(dat);
  registerType<ActionIdLossLayer>(dat);
  registerType<ScaledEuclideanLossLayer>(dat);
  registerType<UniformScaledEuclideanLossLayer>(dat);

  //register all advanced types
  registerType<NoFeatureTransformLayer>(dat);
  registerType<ConstFeatureTransformLayer>(dat);
  registerType<FEMRSFeatureTransformLayer>(dat);
  registerType<FEMRotRSFeatureTransformLayer>(dat);
  registerType<FeatureAugmentedInPlace>(dat);
  registerType<ConvolutionLayer>(dat);
  registerType<CompositeLayerInPlace>(dat);

  //if this neural net is actually a dynamic movement primitives
  registerType<DMPLayer>(dat);
  registerType<NRDMPLayer>(dat);
  registerType<CosDMPLayer>(dat);
  registerType<DMPLayerMT>(dat);
  registerType<NRDMPLayerMT>(dat);

  //register all existing types
  for(set<boost::shared_ptr<Serializable> >::const_iterator
      beg=_layerTypes.begin(),end=_layerTypes.end(); beg!=end; beg++)
    registerType(dat,(*beg)->copy());
}
//setup data
void NeuralNet::clearData()
{
  removeLayer<LossLayer>();
}
void NeuralNet::setData(const TrainingData& data,const Mss* metrics,const Matd* metric)
{
  clearData();
  Matd states,actions;
  data.assemble(states,actions);
  if(metrics)
    setData(states,actions,*metrics);
  else if(metric)
    setData(states,actions,*metric);
  else setData(states,actions);
  _backBlocks[0].back().setOnes(1,1);
  copyMultithread();
}
void NeuralNet::setData(const Matd& states,const Matd& actions)
{
  clearData();
  addLayer("Loss",boost::shared_ptr<EuclideanLossLayer>(new EuclideanLossLayer),nrOutput());
  getLastLayer<EuclideanLossLayer>()->actions()=actions;
  _foreBlocks[0][0]=states;
  _backBlocks[0].back().setOnes(1,1);
  copyMultithread();
}
void NeuralNet::setData(const Matd& states,const Vec& actions,const Coli& actionIds)
{
  clearData();
  addLayer("Loss",boost::shared_ptr<ActionIdLossLayer>(new ActionIdLossLayer),nrOutput());
  getLastLayer<ActionIdLossLayer>()->actionIds()=actionIds;
  getLastLayer<ActionIdLossLayer>()->actions()=actions;
  _foreBlocks[0][0]=states;
  _backBlocks[0].back().setOnes(1,1);
  copyMultithread();
}
void NeuralNet::setData(const Matd& states,const Matd& actions,const Mss& metrics)
{
  clearData();
  //ASSERT((sizeType)metrics.size() == actions.cols())
  addLayer("Loss",boost::shared_ptr<ScaledEuclideanLossLayer>(new ScaledEuclideanLossLayer),nrOutput());
  getLastLayer<ScaledEuclideanLossLayer>()->metrics()=metrics;
  getLastLayer<ScaledEuclideanLossLayer>()->actions()=actions;
  _foreBlocks[0][0]=states;
  _backBlocks[0].back().setOnes(1,1);
  copyMultithread();
}
void NeuralNet::setData(const Matd& states,const Matd& actions,const Matd& metric)
{
  clearData();
  ASSERT(metric.rows() == actions.rows() && metric.cols() == actions.rows())
  addLayer("Loss",boost::shared_ptr<UniformScaledEuclideanLossLayer>
           (new UniformScaledEuclideanLossLayer),nrOutput());
  getLastLayer<UniformScaledEuclideanLossLayer>()->metric()=metric;
  getLastLayer<UniformScaledEuclideanLossLayer>()->actions()=actions;
  _foreBlocks[0][0]=states;
  _backBlocks[0].back().setOnes(1,1);
  copyMultithread();
}
//main functionality
void NeuralNet::train(const UserData* dat)
{
  Vec weights=toVec(false);
  NNBLEICInterface sol(*this);
  sol.reset(nrParam(),false);
  setSolverParam(sol);

  _iter=0;
  saveDummyLayer(dat);
  if(_tree.get<bool>("useCommonLBFGS",false)) {
    Callback<scalarD,Kernel<scalarD> > cb;
    NoCallback<scalarD,Kernel<scalarD> > ncb;
    if(_tree.get<bool>("callbackLBFGS",false))
      sol.solveCommonFile(weights,cb);
    else sol.solveCommonFile(weights,ncb);
  } else sol.solve(weights);
  fromVec(weights);
  loadDummyLayer();
  INFOV("Training using LBFGS, Energy: %f!",foreprop(NULL)(0,0))
}
void NeuralNet::trainRMSProp(Matd states,Matd actions)
{
  sizeType N=_tree.get<sizeType>("batchSize",32);
  sizeType maxIter=_tree.get<sizeType>("maxIterations",10);
  scalar rate=_tree.get<scalar>("learningRate",0.00025f);
  scalar gamma=_tree.get<scalar>("RMSPropGamma",0.95f);
  scalar eps=_tree.get<scalar>("RMSPropEps",0.01f);

  Vec grad,squaredMoment;
  Eigen::PermutationMatrix<Eigen::Dynamic,Eigen::Dynamic> perm(states.cols());
  for(sizeType it=0; it<maxIter; it++) {
    INFOV("RMSProp iteration %ld/%ld!",it,maxIter)
    //random shuffle
    perm.setIdentity();
    std::random_shuffle(perm.indices().data(),perm.indices().data()+perm.indices().size());
    states=(states*perm).eval();
    actions=(actions*perm).eval();
    //update
    for(sizeType i=0; i+N<=states.cols(); i+=N) {
      //INFOV("RMSProp inner iteration %ld/%ld!",i,states.cols())
      //compute gradient
      setData(states.block(0,i,states.rows(),N),actions.block(0,i,actions.rows(),N));
      foreprop(NULL);
      backprop(NULL);
      grad=toVec(true);
      //update neural net
      if(squaredMoment.size() != grad.size())
        squaredMoment.setZero(grad.size());
      squaredMoment.array()=gamma*squaredMoment.array()+(1-gamma)*grad.array().square();
      grad.array()*=rate/(squaredMoment.array()+eps).sqrt();
      fromVec(toVec(false)-grad);
    }
  }
}
//user data
NeuralNet::UserData NeuralNet::createUserData() const
{
  return UserData(_layers.size(),NULL);
}
void NeuralNet::setUserData(UserData& dat,sizeType l,void* data) const
{
  dat.at(l)=data;
}
void NeuralNet::setUserDataByName(UserData& dat,const string& name,void* data) const
{
  sizeType id;
  getLayerByName<NeuralLayer>(name,&id);
  dat.at(id)=data;
}
//F (possibly with DFDX)
Matd& NeuralNet::foreprop(const Matd* in,const UserData* dat)
{
  Mss& foreBlocks=_foreBlocks[omp_get_thread_num()];
  if(_index == 0 && in) {
    ASSERT_MSG(in->rows() == nrInput(),"Incorrect input size!")
    foreBlocks[0]=*in;
  }
  for(sizeType i=_index; i<(sizeType)_layers.size(); i++) {
    void* lData=dat?dat->at(i):NULL;
    _layers[i]->foreprop(foreBlocks[i],foreBlocks[i+1],lData);
  }
  return foreBlocks.back();
}
Matd& NeuralNet::foreprop(const Vec& in,const UserData* dat)
{
  Matd tmp=in;
  return foreprop(&tmp,dat);
}
//DFDW
Matd& NeuralNet::backprop(const Matd* in,const UserData* dat)
{
  Mss& foreBlocks=_foreBlocks[omp_get_thread_num()];
  Mss& backBlocks=_backBlocks[omp_get_thread_num()];
  NeuralLayer::enforceSize(backBlocks.back(),foreBlocks.back());
  if(in)backBlocks.back()=*in;
  for(sizeType i=(sizeType)_layers.size()-1; i>=_index; i--) {
    void* lData=dat?dat->at(i):NULL;
    _layers[i]->backprop(foreBlocks[i],foreBlocks[i+1],backBlocks[i],backBlocks[i+1],lData);
  }
  return backBlocks.front();
}
Matd& NeuralNet::backprop(const Vec& in,const UserData* dat)
{
  Matd tmp=in;
  return backprop(&tmp,dat);
}
//DFDWT
Matd& NeuralNet::weightprop(const Matd* in,const UserData* dat)
{
  Mss& foreBlocks=_foreBlocks[omp_get_thread_num()];
  Mss& backBlocks=_backBlocks[omp_get_thread_num()];

  NeuralLayer::enforceSize(backBlocks.front(),foreBlocks.front());
  if(in)backBlocks.front()=*in;
  else backBlocks.front().setZero();
  for(sizeType i=_index; i<(sizeType)_layers.size(); i++) {
    void* lData=dat?dat->at(i):NULL;
    _layers[i]->weightprop(foreBlocks[i],foreBlocks[i+1],backBlocks[i],backBlocks[i+1],lData);
  }
  return backBlocks.back();
}
Matd& NeuralNet::weightprop(const Vec& in,const UserData* dat)
{
  Matd tmp=in;
  return weightprop(&tmp,dat);
}
//debug code
void NeuralNet::debugWeightprop(const TrainingData& datum,bool ELoss,bool QLoss)
{
  Mss& foreBlocks=_foreBlocks[omp_get_thread_num()];
  Mss& backBlocks=_backBlocks[omp_get_thread_num()];

  //set data
  if(ELoss)
    setData(datum);
  else if(QLoss) {
    Matd states,actions;
    datum.assemble(states,actions);
    Coli actionIds=Coli::Zero(states.cols());
    for(sizeType i=0; i<actionIds.size(); i++)
      actionIds[i]=RandEngine::randI(0,nrOutput()-1);
    setData(states,Vec::Random(states.cols()),actionIds);
  } else {
    clearData();
  }
  //initialize
  foreprop(NULL);
  backBlocks.back()=foreBlocks.back();
  sizeType rows=backBlocks.back().rows();
  sizeType cols=backBlocks.back().cols();
  //collect JT
  Matd JT=Matd::Zero(nrParam(),rows*cols);
  for(sizeType i=0; i<JT.cols(); i++) {
    backBlocks.back().setZero();
    backBlocks.back()(i%rows,i/rows)=1;
    backprop(NULL);
    JT.col(i)=toVec(true);
  }
  //collect J
  Matd J=Matd::Zero(rows*cols,nrParam());
  for(sizeType i=0; i<J.cols(); i++) {
    fromVec(Vec::Unit(nrParam(),i),true);
    weightprop(NULL);
    J.col(i)=Eigen::Map<const Vec>(backBlocks.back().data(),backBlocks.back().size());
  }
  INFOV("JT-J: %f, Err: %f",J.norm(),(J-JT.transpose()).norm())
  clearData();
}
void NeuralNet::debugForeBackpropDMP(const TrainingData& datum,const Mss* metrics,const Matd* metric)
{
  DMPLayer* layer=getLayer<DMPLayer>(0);
  if(!layer) {
    WARNING("No DMP Layer found!")
    return;
  }

  layer->wBiasOptimizable()=true;
  layer->wOptimizable()=false;
  layer->hOptimizable()=false;
  layer->muOptimizable()=false;
  layer->tauOptimizable()=false;
  INFOV("Testing WBias: %ld params",nrParam())
  debugForeBackprop(datum,NULL,NULL,false);

  layer->wBiasOptimizable()=false;
  layer->wOptimizable()=true;
  layer->hOptimizable()=false;
  layer->muOptimizable()=false;
  layer->tauOptimizable()=false;
  INFOV("Testing W: %ld params",nrParam())
  debugForeBackprop(datum,NULL,NULL,false);

  layer->wBiasOptimizable()=false;
  layer->wOptimizable()=false;
  layer->hOptimizable()=true;
  layer->muOptimizable()=false;
  layer->tauOptimizable()=false;
  INFOV("Testing H: %ld params",nrParam())
  debugForeBackprop(datum,NULL,NULL,false);

  layer->wBiasOptimizable()=false;
  layer->wOptimizable()=false;
  layer->hOptimizable()=false;
  layer->muOptimizable()=true;
  layer->tauOptimizable()=false;
  INFOV("Testing Mu: %ld params",nrParam())
  debugForeBackprop(datum,NULL,NULL,false);

  layer->wBiasOptimizable()=false;
  layer->wOptimizable()=false;
  layer->hOptimizable()=false;
  layer->muOptimizable()=false;
  layer->tauOptimizable()=true;
  INFOV("Testing Tau: %ld params",nrParam())
  debugForeBackprop(datum,NULL,NULL,false);

  //exit(-1);
}
void NeuralNet::debugForeBackprop(const TrainingData& datum,const Mss* metrics,const Matd* metric,bool DFDX)
{
  setData(datum,metrics,metric);
  //set to random value
  fromVec(toVec(false).setRandom());
  //test gradient
  debugDFDW();
  if(DFDX)
    debugDFDX();
  clearData();
}
void NeuralNet::debugPerformance()
{
  TBEG("Foreprop Performance");
  for(sizeType i=0; i<10000; i++)
    foreprop(NULL);
  TEND();

  TBEG("Backprop Performance");
  for(sizeType i=0; i<10000; i++)
    backprop(NULL);
  TEND();
}
void NeuralNet::debugDFDW()
{
#define NR_TEST 10
#define DELTA 1E-8f
  INFO("Testing DFDW")
  foreprop(NULL);
  backprop(NULL);
  Vec gradParam=toVec(true);
  scalarD loss=_foreBlocks[omp_get_thread_num()].back()(0,0);
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec tmp=toVec(false),delta=Vec::Random(tmp.size());
    fromVec(tmp+delta*DELTA);
    foreprop(NULL);
    scalarD loss2=_foreBlocks[omp_get_thread_num()].back()(0,0);
    DEBUG_COMPARE("NeuralNet param grad",gradParam.dot(delta),((loss2-loss)/DELTA-gradParam.dot(delta)),1E-5f);
    fromVec(tmp);
  }
#undef DELTA
#undef NR_TEST
}
void NeuralNet::debugDFDX()
{
#define NR_TEST 10
#define DELTA 1E-8f
  INFO("Testing DFDX")
  foreprop(NULL);
  backprop(NULL);
  Matd gradState=toVecStateDiff(true);
  scalarD loss=_foreBlocks[omp_get_thread_num()].back()(0,0);
  //OMP_PARALLEL_FOR_
  for(sizeType i=0; i<NR_TEST; i++) {
    Matd tmp=toVecStateDiff(false),delta=Matd::Random(tmp.rows(),tmp.cols());
    fromVecStateDiff(tmp+delta*DELTA);
    foreprop(NULL);
    scalarD loss2=_foreBlocks[omp_get_thread_num()].back()(0,0);
    DEBUG_COMPARE("NeuralNet state grad",(gradState.transpose()*delta).trace(),((loss2-loss)/DELTA-(gradState.transpose()*delta).trace()),1E-5f);
    fromVecStateDiff(tmp);
  }
#undef DELTA
#undef NR_TEST
}
//misc
int NeuralNet::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  fromVec(x);
  FX=foreprop(NULL)(0,0);
  backprop(NULL);
  DFDX=toVec(true);

  sizeType repInterval=_tree.get<sizeType>("reportInterval",-1);
  if(repInterval > 0 && (_iter++)%repInterval == 0) {
    INFOV("Iter%ld: FX=%f",_iter,FX)
  }
  return 0;
}
int NeuralNet::inputs() const
{
  return (int)nrParam();
}
sizeType NeuralNet::dataSize() const
{
  return _foreBlocks[omp_get_thread_num()][0].cols();
}
sizeType NeuralNet::nrParam() const
{
  sizeType nrP=0;
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    nrP+=_layers[i]->nrParam();
  return nrP;
}
sizeType NeuralNet::nrInput() const
{
  return _foreBlocks[omp_get_thread_num()].front().rows();
}
sizeType NeuralNet::nrOutput() const
{
  return _foreBlocks[omp_get_thread_num()].back().rows();
}
Matd NeuralNet::getBounds() const
{
  sizeType off=0;
  Matd ret=Matd::Zero(nrParam(),2);
  for(sizeType i=0; i<(sizeType)_layers.size(); i++) {
    ret.block(off,0,_layers[i]->nrParam(),2)=_layers[i]->getBounds(_tree);
    off+=_layers[i]->nrParam();
  }
  return ret;
}
void NeuralNet::resetOffline()
{
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    _layers[i]->configOffline(_tree);
}
void NeuralNet::resetOnline()
{
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    _layers[i]->configOnline(_tree);
}
NeuralNet::Vec NeuralNet::toVec(bool diff) const
{
  Vec ret=Vec::Zero(nrParam());
  for(sizeType i=0,off=0; i<(sizeType)_layers.size(); i++) {
    _layers[i]->param(ret,off,diff);
    off+=_layers[i]->nrParam();
  }
  return ret;
}
const Matd& NeuralNet::toVecStateDiff(bool diff) const
{
  if(diff)
    return _backBlocks[omp_get_thread_num()][0];
  else return _foreBlocks[omp_get_thread_num()][0];
}
void NeuralNet::fromVec(const Vec& weights,bool diff)
{
  ASSERT_MSG(weights.size() >= nrParam(),"Weights mismatch!")
  for(sizeType i=0,off=0; i<(sizeType)_layers.size(); i++) {
    _layers[i]->setParam(weights,off,diff);
    off+=_layers[i]->nrParam();
  }
}
void NeuralNet::fromVecStateDiff(const Matd& states,bool diff)
{
  if(diff) {
    ASSERT(states.size() == _backBlocks[omp_get_thread_num()][0].size())
    _backBlocks[omp_get_thread_num()][0]=states;
  } else {
    ASSERT(states.size() == _foreBlocks[omp_get_thread_num()][0].size())
    _foreBlocks[omp_get_thread_num()][0]=states;
  }
}
//save,load layers
void NeuralNet::saveDummyLayer(const UserData* dat)
{
  _index=0;
  foreprop(NULL,dat);
  backprop(NULL,dat);
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    if(_layers[i]->nrParam() == 0)
      _index++;
    else break;
}
void NeuralNet::loadDummyLayer()
{
  _index=0;
}
//save,load layers
void NeuralNet::saveForeprop(NeuralNet& net) const
{
  net._foreBlocks=_foreBlocks;
}
void NeuralNet::loadForeprop(const NeuralNet& net)
{
  _foreBlocks=net._foreBlocks;
}
boost::shared_ptr<ActivationLayer> NeuralNet::getActivationLayer(const string& type)
{
  if(type == "BNLL")
    return boost::shared_ptr<ActivationLayer>(new BNLLLayer);
  else if(type == "ReLU")
    return boost::shared_ptr<ActivationLayer>(new ReLULayer);
  else if(type == "TanH")
    return boost::shared_ptr<ActivationLayer>(new TanHLayer);
  return boost::shared_ptr<ActivationLayer>();
}
//helper
NeuralNet::NeuralNet(const string& name)
  :Serializable(name),_index(0) {}
void NeuralNet::copyMultithread()
{
  _foreBlocks.resize(omp_get_num_procs());
  _backBlocks.resize(omp_get_num_procs());
  for(sizeType i=1; i<(sizeType)_foreBlocks.size(); i++)
    _foreBlocks[i]=_foreBlocks[0];
  for(sizeType i=1; i<(sizeType)_backBlocks.size(); i++)
    _backBlocks[i]=_backBlocks[0];
}
void NeuralNet::setSolverParam(BLEICInterface& sol) const
{
  sol.setMaxIter(_tree.get<sizeType>("maxIterations",1E3));
  sol.setTolG(_tree.get<scalar>("epsilon",1E-10f));
  sol.setTolF(_tree.get<scalar>("delta",1E-10f));
}
template <typename T> void NeuralNet::removeLayer()
{
  while(!_layers.empty() && getLastLayer<T>()) {
    _layers.pop_back();
    for(sizeType i=0; i<(sizeType)_foreBlocks.size(); i++)
      _foreBlocks[i].pop_back();
    for(sizeType i=0; i<(sizeType)_backBlocks.size(); i++)
      _backBlocks[i].pop_back();
  }
}
sizeType NeuralNet::addActivationLayer(const string& name,const string& type,sizeType lastIn)
{
  boost::shared_ptr<ActivationLayer> l=getActivationLayer(type);
  if(l)
    return addLayer(name,l,lastIn);
  else return lastIn;  //activation layer does not change number of output neurons
}
sizeType NeuralNet::addLayer(const string& name,boost::shared_ptr<NeuralLayer> layer,sizeType lastIn)
{
  INFOV("Adding layer: %s!",typeid(*layer).name())
  _layers.push_back(layer);
  _layerTypes.insert(layer);
  layer->setLayerName(name);
  boost::optional<boost::property_tree::ptree&> pt=
    _tree.get_child_optional(name+"Param");
  if(pt)
    layer->configOffline(*pt);
  else layer->configOffline(boost::property_tree::ptree());
  for(sizeType i=0; i<(sizeType)_foreBlocks.size(); i++)
    _foreBlocks[i].push_back(Matd());
  for(sizeType i=0; i<(sizeType)_backBlocks.size(); i++)
    _backBlocks[i].push_back(Matd());
  return _layers.back()->nrOutput(lastIn);
}
