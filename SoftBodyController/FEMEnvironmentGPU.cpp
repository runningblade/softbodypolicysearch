#include "FEMEnvironmentMT.h"
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/ArticulatedCIOEngine.h>
#ifdef VIENNACL_SUPPORT
#include <MultiTraj/ArticulatedMultiTrajGPU.h>
#endif

USE_PRJ_NAMESPACE

//MultiTraj
void FEMEnvironment::addGPUSupport()
{
  _sol->getBody()._tree.put<bool>("GPUEval",true);
  _sol->getBody()._system->onDirty();
}
void FEMEnvironment::removeGPUSupport()
{
  _sol->getBody()._tree.put<bool>("GPUEval",false);
}
bool FEMEnvironment::supportMultiTraj() const
{
#ifdef VIENNACL_SUPPORT
  return _sol->getBody()._tree.get<bool>("GPUEval",false);
#else
  return false;
#endif
}
void FEMEnvironment::startMultiTraj(MultiTrajCallback& cb)
{
#ifdef VIENNACL_SUPPORT
  if(boost::dynamic_pointer_cast<ArticulatedCIOEngineImplicit>(_sol)) {
    ArticulatedMultiTrajGPU gpu(*_sol);
    gpu.multiTrajUpdate(cb);
  } else {
    FEMMultiTrajGPU gpu(*_sol);
    gpu.multiTrajUpdate(cb);
  }
#else
  TrajectorySampler::startMultiTraj(cb);
#endif
}
//MultiTraj/MultiTask
#ifdef VIENNACL_SUPPORT
struct MultiTrajCallbackWrapper : public MultiTrajCallback {
  MultiTrajCallbackWrapper(MultiTrajCallback& cb,FEMEnvironmentMT& mt):_cb(cb),_mt(mt) {}
  virtual Cold onNewState(const Cold& state,sizeType tid,sizeType fid) {
    if(_mt.K() <= 0) {
      Cold aug;
      _mt.setTrajId(tid);
      aug.setZero(_mt.nrMTAug());
      _mt.getMTAug(aug);
      return _cb.onNewState(concat(state,aug),tid,fid);
    } else return _cb.onNewState(state,tid,fid);
  }
  virtual Cold getInitState(sizeType tid) {
    return _cb.getInitState(tid);
  }
  virtual sizeType initialize() {
    return _cb.initialize ();
  }
  virtual sizeType trajLen() {
    return _cb.trajLen ();
  }
  MultiTrajCallback& _cb;
  FEMEnvironmentMT& _mt;
};
#endif
void FEMEnvironmentMT::startMultiTraj(MultiTrajCallback& cb)
{
#ifdef VIENNACL_SUPPORT
  if(boost::dynamic_pointer_cast<ArticulatedCIOEngineImplicit>(_sol)) {
    ArticulatedMultiTrajGPU gpu(*_sol);
    MultiTrajCallbackWrapper cbWrapper(cb,*this);
    gpu.multiTrajUpdate(cbWrapper);
  } else {
    FEMMultiTrajGPU gpu(*_sol);
    MultiTrajCallbackWrapper cbWrapper(cb,*this);
    gpu.multiTrajUpdate(cbWrapper);
  }
#else
  TrajectorySampler::startMultiTraj(cb);
#endif
}
