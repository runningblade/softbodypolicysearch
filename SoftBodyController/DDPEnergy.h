#ifndef DDP_ENERGY_H
#define DDP_ENERGY_H

#include <CommonFile/IOBasic.h>
#include <CommonFile/geom/StaticGeomCell.h>

PRJ_BEGIN

//gradient-based energy
class LineSegImplicitFuncInterface;
class DDPEnergy : public Serializable
{
public:
  typedef Cold Vec;
  DDPEnergy();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual scalarD trajCost(scalar dt,const Matd& x) const;
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x) const;
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec& fx) const;
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec& fx,Matd& fxx) const;
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const=0;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const=0;
  virtual sizeType nrF() const=0;
  static void registerType(IOData* dat);
  scalarD _coef;
  //multitask augmented feature
  virtual void setMTAug(const Vec& aug,sizeType off);
  virtual void getMTAug(Vec& aug,sizeType off) const;
  virtual sizeType nrMTAug() const;
  virtual void setMTId(sizeType id);
protected:
  DDPEnergy(const string& path);
  sizeType _mtFeatId;
};
class DDPBalanceEnergy : public DDPEnergy
{
public:
  DDPBalanceEnergy();
  DDPBalanceEnergy(const Vec3d& dirL,const Vec3d& dirG,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  Vec3d _dirL,_dirG;
  sizeType _dim;
};
class DDPJumpEnergy : public DDPEnergy
{
public:
  DDPJumpEnergy();
  DDPJumpEnergy(const Vec3& gravity,scalarD height,scalarD frame,scalarD coef,sizeType dim);
  virtual scalarD trajCost(scalar dt,const Matd& x) const;
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  //multitask augmented feature
  virtual void setMTAug(const Vec& aug,sizeType off) override;
  virtual void getMTAug(Vec& aug,sizeType off) const override;
  virtual sizeType nrMTAug() const override;
private:
  Vec3d _dir;
  scalarD _height,_frame;
  sizeType _dim;
};
class DDPWalkEnergy : public DDPEnergy
{
public:
  DDPWalkEnergy();
  DDPWalkEnergy(const Vec3d& spd,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  //multitask augmented feature
  virtual void setMTAug(const Vec& aug,sizeType off) override;
  virtual void getMTAug(Vec& aug,sizeType off) const override;
  virtual sizeType nrMTAug() const override;
private:
  Vec3d _spd,_spd0;
  sizeType _dim;
};
class DDPWalkToEnergy : public DDPEnergy
{
public:
  DDPWalkToEnergy();
  DDPWalkToEnergy(const Vec3d& pos,const Vec3d* gT,const Vec3d* gN,scalarD frm,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual void setPos(const Vec3d& pos);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  Vec3d _pos;
  Mat3d _PROJ;
  scalarD _frame;
  sizeType _dim;
};
class DDPWalkSpeedToEnergy : public DDPEnergy
{
public:
  DDPWalkSpeedToEnergy();
  DDPWalkSpeedToEnergy(const Vec3d& spd,const Vec3d* gT,const Vec3d* gN,scalarD frm,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  Vec3d _spd;
  Mat3d _PROJ;
  scalarD _frame;
  sizeType _dim;
};
class DDP2DRollEnergy : public DDPEnergy
{
public:
  DDP2DRollEnergy();
  DDP2DRollEnergy(scalarD spd,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  scalarD _spd;
  sizeType _dim;
};
class DDP3DRollEnergy : public DDPEnergy
{
public:
  DDP3DRollEnergy();
  DDP3DRollEnergy(const Vec3d& spd,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  Vec3d _spd;
  sizeType _dim;
};
class DDP3DOrientationEnergy : public DDPEnergy
{
public:
  DDP3DOrientationEnergy();
  DDP3DOrientationEnergy(const Vec3d& dirL,const Vec3d& rot,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  //multitask augmented feature
  virtual void setMTAug(const Vec& aug,sizeType off) override;
  virtual void getMTAug(Vec& aug,sizeType off) const override;
  virtual sizeType nrMTAug() const override;
private:
  Vec3d _dirL,_rot,_rot0;
  sizeType _dim;
};
class DDP3DOrientationToEnergy : public DDPEnergy
{
public:
  DDP3DOrientationToEnergy();
  DDP3DOrientationToEnergy(const Vec3d& dirL,scalarD frm,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  Vec3d _dirL;
  Mat3d _PROJ;
  scalarD _frame;
  sizeType _dim;
};
class DDPCurvedWalkEnergy : public DDPEnergy
{
public:
  DDPCurvedWalkEnergy();
  DDPCurvedWalkEnergy(const Vec3d& dirL,const Vec3d& rot,scalarD coef,sizeType dim);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  //multitask augmented feature
  virtual void setMTAug(const Vec& aug,sizeType off) override;
  virtual void getMTAug(Vec& aug,sizeType off) const override;
  virtual sizeType nrMTAug() const override;
private:
  Vec3d _dirL,_rot;
  Vec3d _dirL0,_rot0;
  sizeType _dim;
};
//reinforcement learning energy
class RLEnergy : public DDPEnergy
{
public:
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const;
  virtual sizeType nrF() const;
  virtual void setPos(const Vec3d& pos);
  virtual bool isTerminal(const Vec& x) const;
protected:
  RLEnergy(const string& path);
};
class GoalEnergy : public RLEnergy
{
public:
  GoalEnergy();
  GoalEnergy(const Vec3d& pos,const Vec3d* gT,const Vec3d* gN,scalarD coef);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void setPos(const Vec3d& pos) override;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  Vec3d _pos;
  Mat3d _PROJ;
};
class DirectedSpeedEnergy : public RLEnergy
{
public:
  DirectedSpeedEnergy();
  DirectedSpeedEnergy(const Vec3d& pos,const Vec3d* gT,const Vec3d* gN,scalarD coef);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void setPos(const Vec3d& pos) override;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
private:
  Vec3d _pos;
  Mat3d _PROJ;
};
class ObstacleEnergy : public RLEnergy
{
public:
  ObstacleEnergy();
  ObstacleEnergy(const Vec3d* gT,scalarD safeDist,scalarD fade,scalarD coef);
  virtual scalarD objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void setObstacle(const Mat4Xd& obstacle);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual bool isTerminal(const Vec& x) const override;
private:
  scalarD _safeDist,_fade,_coef;
  Mat4Xd _obstacle;
  Mat3d _PROJ;
};

PRJ_END

#endif
