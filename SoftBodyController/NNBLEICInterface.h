#ifndef NN_BLEIC_INTERFACE_H
#define NN_BLEIC_INTERFACE_H

#include <Dynamics/FEMUtils.h>
#include <Dynamics/OPTInterface.h>
#include "NeuralNet.h"

PRJ_BEGIN

class NNBLEICInterface : public BLEICInterface
{
public:
  NNBLEICInterface(NeuralNet& net):_net(net) {}
  virtual void operator()(Eigen::Map<const Cold>& x,scalarD& FX,Eigen::Map<Cold>& DFDX) {
    Cold DFDXNN=DFDX;
    _net(x,FX,DFDXNN,1,true);
    //INFOV("FX=%f DFDXNN=%f",FX,DFDXNN.norm())
    DFDX=DFDXNN;
  }
private:
  NeuralNet& _net;
};

PRJ_END

#endif
