#ifndef FEM_ENVIRONMENT_DQN_OBSTACLE_H
#define FEM_ENVIRONMENT_DQN_OBSTACLE_H

#include "FEMEnvironmentDQN.h"

PRJ_BEGIN

class FEMEnvironmentDQNObstacle;
class CNNObstacleFeature : public Serializable
{
public:
  typedef FEMEnvironmentDQN::Vec Vec;
  CNNObstacleFeature();
  CNNObstacleFeature(FEMEnvironmentDQN& env,Vec3d f,const boost::property_tree::ptree* pt=NULL);
  virtual bool read(const string& path);
  virtual bool write(const string& path) const;
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  const vector<Vec3d,Eigen::aligned_allocator<Vec3d> >& getDir() const;
  void writeSensingVTK(const string& path,FEMEnvironmentDQNObstacle& env,const Vec& state,const Vec& sensing) const;
  void writeVTK(const string& path,FEMEnvironmentDQNObstacle& env,const Vec& state,const Mat4Xd& obstacle,const Vec& sensing,const Vec& feature) const;
  void trainCNNFeature(FEMEnvironmentDQNObstacle& env,sizeType nrSample,bool debug=false,bool debugFeature=false);
  Vec getSensing(const FEMEnvironmentDQNObstacle& env,const Vec& state,const Mat4Xd& obstacle) const;
  Vec getFeature(const FEMEnvironmentDQNObstacle& env,const Vec& state,const Vec& sensing);
  sizeType nrFeature(const FEMEnvironmentDQN& env) const;
  sizeType nrSensing() const;
  NeuralNet& getNet();
protected:
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > _dir;
  boost::shared_ptr<NeuralNet> _net;
  Vec2i _res;
};
class FEMEnvironmentDQNObstacle : public FEMEnvironmentDQN
{
public:
  FEMEnvironmentDQNObstacle();
  FEMEnvironmentDQNObstacle(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt=NULL);
  virtual void reset(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt=NULL);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void writeStateVTK(const string& path);
  virtual void writeStatePov(const string& path);
  virtual void setUsePeriodicTarget(sizeType s0,sizeType s1);
  virtual bool isTerminal(sizeType i) const;
  sizeType nrEffectiveObstacles() const;
  void setupCNNFeature(const string& path);
protected:
  FEMEnvironmentDQNObstacle(const string& name);
  virtual void cycleTarget();
  virtual void pickTarget();
  virtual Mat4Xd pickObstacle(const Vec3d& target) const;
  virtual void syncTarget();
  virtual void updateFeature(Vec& ret) const;
  virtual sizeType nrFeature() const;
  //data
  boost::shared_ptr<CNNObstacleFeature> _feature;
  vector<Mat4Xd,Eigen::aligned_allocator<Mat4Xd> > _obstacles;
  Mat4Xd _obstacle;
};
class FEMEnvironmentDQNObstacleDynamic : public FEMEnvironmentDQNObstacle
{
public:
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void writeStateVTK(const string& path);
  virtual void writeStatePov(const string& path);
  virtual void setUsePeriodicTarget(sizeType s0,sizeType s1);
protected:
  virtual void cycleTarget();
  virtual void pickTarget();
  virtual Mat3Xd pickObstacleVel(const Mat4Xd& obstacle) const;
  virtual void updateFeature(Vec& ret) const;
  //data
  vector<Mat3Xd,Eigen::aligned_allocator<Mat3Xd> > _obstacleVels;
  Mat3Xd _obstacleVel;
  Mat4Xd _obstacle0;
};

PRJ_END

#endif
