#ifndef TRPO_H
#define TRPO_H

#include "Environment.h"

PRJ_BEGIN

//Polynomial Baseline function
class Polynomial : Objective<scalarD>
{
public:
  Polynomial(sizeType order,sizeType _nrS);
  virtual scalarD operator()(const Vec& x);
  virtual void fit(const vector<Transition>& memoryBank);
  virtual Vec computeFeature(const Vec& x) const;
  virtual int inputs() const;
private:
  sizeType _order,_nrS;
  Vec _coef;
};
//this implement the TrustRegionPolicyOptimization algorithm
class TRPO
{
public:
  typedef Environment::Vec Vec;
  TRPO(Environment& env);
  virtual ~TRPO();
  virtual void learn();
  void profileMemory() const;
  //constraint (mean kl-distance)
  void debugKLDistance();
  //surrogate loss
  void seed(sizeType seed);
  scalarD surrogateGradient(const Vec& param,const Vec& paramOld,Vec* grad);
  Vec lineSearch(const Vec& paramOld,const Vec& D,sizeType tid,sizeType maxIter);
  void debugSurrogateGradient(scalar perturb=1E-3f);
  boost::property_tree::ptree _tree;
protected:
  Matd getStates() const;
  Matd getActions() const;
  Vec getAdvantages() const;
  void collectTransition(const TrajectoryState* ss);
  void randomMemory();
  boost::shared_ptr<KrylovMatrix<scalarD> > _hessKL;
  boost::shared_ptr<Polynomial> _baseline;
  vector<Transition> _memoryBank;
  vector<sizeType> _offMemoryTraj;
  TrainingData _trajs;
  Environment& _env;
  sizeType _seed;
};

PRJ_END

#endif
