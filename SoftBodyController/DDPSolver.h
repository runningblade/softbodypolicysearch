#ifndef DDP_SOLVER_H
#define DDP_SOLVER_H

#include <boost/property_tree/ptree.hpp>
#include <boost/shared_ptr.hpp>
#include <CommonFile/MathBasic.h>
#include <Dynamics/OPTInterface.h>

PRJ_BEGIN

class Environment;
class DDPSolver
{
public:
  typedef Cold Vec;
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
  typedef vector<Matd,Eigen::aligned_allocator<Matd> > Mss;
  friend class TrajOptimizer;
  //interface
  virtual ~DDPSolver();
  virtual void writeEnvVTK(const string& path) const=0;
  virtual void writeStateVTK(const string& path,const Vec& x) const=0;
  virtual void writeStatesVTK(const string& path,const boost::property_tree::ptree* tree=NULL,bool useXs=false) const;
  virtual void resetParam(sizeType maxIter,sizeType maxIterILQG,sizeType horizon);
  virtual void resetEps(scalar epsF,scalar epsG,scalar epsX);
  void resetTrajectory(boost::property_tree::ptree& t); //you can call this, but usually this is called internally
  void setMaxIterILQG(sizeType maxIterILQG);
  void setMaxIter(sizeType maxIter);
  void setHorizon(sizeType horizon);
  void setAdopt(sizeType adopt);
  virtual Vec initialize(sizeType horizon) const;
  virtual void adopt(const Vec& u,sizeType horizon,Vss* xsP=NULL);
  virtual sizeType nrX() const;
  virtual sizeType nrU() const;
  //for trajectory opt
  void writePT(const Vec& us,const Vss& xsP,boost::property_tree::ptree& pt,sizeType horizon) const;
  virtual void evaluate(Vec& us,scalarD& FX,bool quadraticApprox,Vss& xsP,
                        const Vss* ks=NULL,const Mss* Ks=NULL,const scalarD* alpha=NULL,
                        boost::property_tree::ptree* pt=NULL);
  Vec solveTrajOpt();
  //for ilqg
  void increaseMu(scalarD& mu,scalarD& delta) const;
  void decreaseMu(scalarD& mu,scalarD& delta) const;
  virtual void collectDerivatives(Vss* objXq=NULL,Vss* objUq=NULL,Mss* objXQ=NULL,Mss* objUQ=NULL);
  virtual void ILQGIter(Vec& usOpt);
  virtual void ILQGIterQP(Vec& usOpt);
  virtual void ILQGIter(Vss& ks,Mss& Ks,scalarD& mu,scalarD& delta);
  virtual Vec solveILQG();
  //debug
  virtual void debugObj(const Vec& x,const Vec& u,bool addObjUX,sizeType frm=0);
  virtual void debugObj(bool addObjUX,sizeType frm=0);
  virtual void debugFxCoord(const Vec& x,const Vec& u);
  virtual void debugFx(const Vec& x,const Vec& u);
  virtual void debugFx(bool coord);
  virtual void debugGradient();
  virtual void debugILQGOptQP();
  virtual void debugILQGOpt();
  virtual void debugILQG();
  void debugIter(Mss& Ks,Vss& ks,
                 scalarD mu,scalarD alpha,scalarD delta,
                 scalarD newFX,scalarD relC);
  boost::property_tree::ptree _tree;
  //objective, dynamics
  //objective function: X
  virtual scalarD objX(sizeType i,const Vec& x) const;
  virtual scalarD objX(sizeType i,const Vec& x,Vec& fx) const;
  virtual scalarD objX(sizeType i,const Vec& x,Vec& fx,Matd& fxx) const;
  virtual scalarD objX(sizeType i,const Vec& x,Vec* fx,Matd* fxx) const=0;
  //objective function: U
  virtual scalarD objU(sizeType i,const Vec& u) const;
  virtual scalarD objU(sizeType i,const Vec& u,Vec& fu) const;
  virtual scalarD objU(sizeType i,const Vec& u,Vec& fu,Matd& fuu) const;
  virtual scalarD objU(sizeType i,const Vec& u,Vec* fu,Matd* fuu) const;
  //dynamic system
  virtual void dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f) const;
  virtual void dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd& fx,Matd& fu) const;
  virtual void dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu) const=0;
protected:
  boost::shared_ptr<Vec> _bound;
  Vss _xs;
  Mss _fxs,_fus;
  Vec _us0,_G;
  Matd _H;
};
class TrajOptimizer : public BLEICInterface
{
public:
  typedef DDPSolver::Vec Vec;
  typedef DDPSolver::Vss Vss;
  typedef DDPSolver::Mss Mss;
  TrajOptimizer(DDPSolver& sol,sizeType horizon);
  void operator()(Eigen::Map<const Vec>& us,scalarD& FX,Eigen::Map<Vec>& DFDX);
  void setBound(const Vec& bound);
  void reset();
private:
  sizeType _horizon;
  DDPSolver& _sol;
  Vss &_xs,_xsP;
  Mss &_fxs,&_fus;
  Vec &_us0;
};
class DDPSolverLinear : public DDPSolver
{
public:
  //interface
  virtual void createExample();
  virtual void writeEnvVTK(const string& path) const;
  virtual void writeStateVTK(const string& path,const Vec& x) const;
  virtual scalarD objX(sizeType i,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu) const;
};
class DDPSolverDubin : public DDPSolver
{
public:
  //interface
  virtual void createExample(bool bound=true);
  virtual void writeEnvVTK(const string& path) const;
  virtual void writeStateVTK(const string& path,const Vec& x) const;
  virtual scalarD objX(sizeType i,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual void dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu) const;
};

PRJ_END

#endif
