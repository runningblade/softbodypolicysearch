#ifndef POLICY_GRADIENT_H
#define POLICY_GRADIENT_H

#include "Environment.h"

PRJ_BEGIN

//Expectation with Baseline
class BaselineExpectation
{
public:
  typedef Environment::Vec Vec;
  void reset(sizeType nrParam);
  void feedNewData(const Vec& possLOG,scalarD reward);
  Vec calcExpectation(bool baseline);
  sizeType size() const;
private:
  //sufficient statistics
  Vec _sumPossLOG;
  Vec _sumPossLOGReward;
  Vec _sumPossLOGNum;
  Vec _sumPossLOGDenom;
  sizeType _size;
};
//this implement the REINFORCE algorithm
class PolicyGradientREINFORCE
{
public:
  typedef Environment::Vec Vec;
  PolicyGradientREINFORCE(Environment& env);
  virtual ~PolicyGradientREINFORCE();
  virtual void testGradientConvergence();
  virtual void learn();
  boost::property_tree::ptree _tree;
protected:
  virtual bool iter(sizeType it);
  virtual scalar stepSize(sizeType it) const;
  virtual scalarD reward(const TrajectoryState& s0) const;
  virtual Vec3d record(const string& name);
  virtual string createCheckoutPath();
  virtual Vec updatePolicyGradient();
  virtual void resizeSufficientStatistics();
  vector<Vec,Eigen::aligned_allocator<Vec> > _possLOGGrad;
  Environment& _env;
  TrainingData _dat;
private:
  BaselineExpectation _suffStat;
};
//this implement the GPOMDP algorithm
class PolicyGradientGPOMDP : public PolicyGradientREINFORCE
{
public:
  PolicyGradientGPOMDP(Environment& env);
protected:
  virtual Vec updatePolicyGradient();
  virtual void resizeSufficientStatistics();
private:
  vector<BaselineExpectation> _suffStatStaged;
};
//this implement the eNAC algorithm
class PolicyGradientNAC : public PolicyGradientREINFORCE
{
public:
  PolicyGradientNAC(Environment& env);
protected:
  virtual Vec updatePolicyGradient();
  virtual boost::shared_ptr<KrylovMatrix<scalarD> > getHessian();
};

PRJ_END

#endif
