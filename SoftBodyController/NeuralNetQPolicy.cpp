#include "NeuralNetQPolicy.h"
#include "TrainingData.h"
#include <CMAES/MLUtil.h>
#include <CommonFile/IO.h>

PRJ_BEGIN

//Simple QPolicy Wrapper
Tensor::Tensor():Serializable(typeid(Tensor).name()) {}
bool Tensor::read(istream& is,IOData* dat)
{
  registerType<Tensor>(dat);
  readVector(_subIndex,is,dat);
  readBinaryData(_scale,is);
  readBinaryData(_i,is);
  return is.good();
}
bool Tensor::write(ostream& os,IOData* dat) const
{
  registerType<Tensor>(dat);
  writeVector(_subIndex,os,dat);
  writeBinaryData(_scale,os);
  writeBinaryData(_i,os);
  return os.good();
}
boost::shared_ptr<Serializable> Tensor::copy() const
{
  return boost::shared_ptr<Serializable>(new Tensor);
}
sizeType Tensor::depth() const
{
  if(_subIndex.empty())
    return 0;
  else return 1+_subIndex[0]->depth();
}
void Tensor::randomize(Vec& state) const
{
  if(_subIndex.empty())
    return;
  state[_i]=RandEngine::randR(_scale[0],_scale[1]);
}
void Tensor::findQValue(const Vec& state,vector<pair<sizeType,scalarD> >& stencil,scalarD coef0) const
{
  sizeType nrSeg=(sizeType)(_subIndex.size()-1);
  if(nrSeg < 0) {
    stencil.push_back(make_pair(_i,coef0));
  } else {
    scalarD coef=(state[_i]-_scale[0])/(_scale[1]-_scale[0])*(scalar)nrSeg;
    sizeType id=max<sizeType>(min<sizeType>(floor(coef),nrSeg-1),0);
    scalarD frac=coef-(scalarD)id;
    _subIndex[id]->findQValue(state,stencil,coef0*(1-frac));
    _subIndex[id+1]->findQValue(state,stencil,coef0*frac);
  }
}
void Tensor::createQTensor(sizeType i,const Vec& L,const Vec& U,sizeType res,sizeType nrA,sizeType& off)
{
  ASSERT(L.size() == U.size())
  if(i == L.size()) {
    _scale=Vec2d::Constant(numeric_limits<scalarD>::quiet_NaN());
    _i=off;
    off+=nrA;
  } else {
    _subIndex.resize(res);
    for(sizeType t=0; t<res; t++) {
      _subIndex[t].reset(new Tensor);
      _subIndex[t]->createQTensor(i+1,L,U,res,nrA,off);
    }
    _scale=Vec2d(L[i],U[i]);
    _i=i;
  }
}
//SimpleQPolicy
SimpleQPolicy::SimpleQPolicy()
  :Policy(typeid(SimpleQPolicy).name()),_eps(0) {}
SimpleQPolicy::SimpleQPolicy(const Vec& L,const Vec& U,const Vec& action,sizeType res)
  :Policy(typeid(SimpleQPolicy).name()),_eps(0)
{
  sizeType off=0;
  _action=action;
  _tensor.createQTensor(0,L,U,res,action.size(),off);
  _Q.setZero(off);
}
bool SimpleQPolicy::read(istream& is,IOData* dat)
{
  readBinaryData(_deterministic,is);
  readBinaryData(_Q,is);
  readBinaryData(_action,is);
  _tensor.read(is,dat);
  readBinaryData(_eps,is);
  return is.good();
}
bool SimpleQPolicy::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_deterministic,os);
  writeBinaryData(_Q,os);
  writeBinaryData(_action,os);
  _tensor.write(os,dat);
  writeBinaryData(_eps,os);
  return os.good();
}
boost::shared_ptr<Serializable> SimpleQPolicy::copy() const
{
  return boost::shared_ptr<Serializable>(new SimpleQPolicy);
}
Matd SimpleQPolicy::getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples)
{
  Matd ret=Vec::Zero(1,output.cols());
  if(possLog)
    possLog->setZero(output.cols());
  //OMP_PARALLEL_FOR_
  for(sizeType j=0; j<output.cols(); j++)
    if(!_deterministic && RandEngine::randR01() < _eps) {
      //exploration
      ret(0,j)=_action[RandEngine::randI(0,_action.size()-1)];
      if(possLog)
        (*possLog)[j]=_eps/(scalar)_action.size();
    } else {
      //deterministic
      sizeType index;
      Vec QVal=findQValue(output.col(j));
      QVal.maxCoeff(&index);
      ret(0,j)=_action[index];
      if(possLog)
        (*possLog)[j]=1-_eps;
    }
  return ret;
}
SimpleQPolicy::Vec SimpleQPolicy::findQValue(const Vec& state) const
{
  vector<pair<sizeType,scalarD> > stencil;
  _tensor.findQValue(state,stencil,1);
  return eval(stencil,_Q);
}
scalarD SimpleQPolicy::QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* hess) const
{
  scalarD ret=0,loss=0;
  sizeType id=-1,idNext=-1;
  scalarD coef=1/(scalarD)(to-from),coefSqrt=sqrt(coef);
  if(grad)grad->setZero(nrParam());
  if(hess)hess->clear();

  vector<pair<sizeType,scalarD> > stencil,stencilNext;
  OMP_PARALLEL_FOR_I(OMP_FPRI(loss,id,idNext,stencil,stencilNext) OMP_ADD(ret))
  for(sizeType i=from; i<to; i++) {
    stencil.clear();
    stencilNext.clear();
    id=index(trans[i]._action[0]);
    _tensor.findQValue(trans[i]._state,stencil,1);
    _tensor.findQValue(trans[i]._nextState,stencilNext,gamma);
    {
      loss=eval(stencil,_Q)[id]-trans[i]._reward;
      if(!trans[i]._last)
        loss-=eval(stencilNext,_Q).maxCoeff(&idNext);
    }
    if(grad) {
      evalGrad(*grad,stencil,id,loss*coef);
      if(!trans[i]._last)
        evalGrad(*grad,stencilNext,idNext,-loss*coef);
    }
    if(hess) {
      evalHess(*hess,stencil,i-from,id,coefSqrt);
      if(!trans[i]._last)
        evalHess(*hess,stencilNext,i-from,idNext,-coefSqrt);
    }
    ret+=loss*loss*coef;
  }
  return ret/2;
}
void SimpleQPolicy::debugQLoss()
{
#define NR_TRANS 200
#define NR_TEST 100
#define DELTA 1E-7f
  Vec grad,grad2;
  SMat hess,hess2;
  sizeType nrState=_tensor.depth();
  vector<Transition> trans(NR_TRANS);
  for(sizeType i=0; i<(sizeType)trans.size(); i++) {
    trans[i]._state=Vec::Zero(nrState);
    _tensor.randomize(trans[i]._state);
    trans[i]._action=Policy::getAction(trans[i]._state,NULL,NULL);
    trans[i]._nextState=Vec::Zero(nrState);
    _tensor.randomize(trans[i]._nextState);
    trans[i]._reward=RandEngine::randR(-1,1);
    trans[i]._last=false;
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    scalarD loss=QLossM(trans,0.9f,NR_TRANS/3,NR_TRANS*2/3,grad,hess);
    Vec param=toVec(),delta=Vec::Random(param.size());
    fromVec(param+delta*DELTA);
    scalarD loss2=QLossM(trans,0.9f,NR_TRANS/3,NR_TRANS*2/3,grad2,hess2);
    DEBUG_COMPARE("GradientQLoss",delta.dot(grad),(delta.dot(grad)-(loss2-loss)/DELTA),1E-5f);
    DEBUG_COMPARE("HessianQLoss",(hess*delta).norm(),(hess*delta-(grad2-grad)/DELTA).norm(),1E-5f);
  }
  map<sizeType,sizeType> MMap;
  for(sizeType i=0; i<NR_TEST; i++) {
    scalarD loss=QLossMMapped(trans,0.9f,NR_TRANS/3,NR_TRANS*2/3,grad,hess,MMap);
    Vec param=toVec(&MMap),delta=Vec::Random(param.size());
    fromVec(param+delta*DELTA,&MMap);
    scalarD loss2=QLossMMapped(trans,0.9f,NR_TRANS/3,NR_TRANS*2/3,grad2,hess2,MMap);
    DEBUG_COMPARE("GradientQLossMMapped",delta.dot(grad),(delta.dot(grad)-(loss2-loss)/DELTA),1E-5f);
    DEBUG_COMPARE("HessianQLossMMapped",(hess*delta).norm(),(hess*delta-(grad2-grad)/DELTA).norm(),1E-5f);
  }
#undef DELTA
#undef NR_TEST
#undef NR_TRANS
}
sizeType SimpleQPolicy::index(scalarD val) const
{
  for(sizeType i=0; i<_action.size(); i++)
    if(_action[i] == val)
      return i;
  ASSERT(false);
  return -1;
}
void SimpleQPolicy::setEps(scalarD eps)
{
  _eps=eps;
}
sizeType SimpleQPolicy::nrParam() const
{
  return _Q.size();
}
SimpleQPolicy::Vec SimpleQPolicy::toVec() const
{
  return toVec(NULL);
}
void SimpleQPolicy::fromVec(const Vec& weights)
{
  fromVec(weights,NULL);
}
SimpleQPolicy::Vec SimpleQPolicy::toVec(const map<sizeType,sizeType>* MMap) const
{
  if(MMap) {
    Vec ret=Vec::Zero(MMap->size());
    for(map<sizeType,sizeType>::const_iterator beg=MMap->begin(),end=MMap->end(); beg!=end; beg++)
      ret[beg->second]=_Q[beg->first];
    return ret;
  } else {
    return _Q;
  }
}
void SimpleQPolicy::fromVec(const Vec& weights,const map<sizeType,sizeType>* MMap)
{
  if(MMap) {
    for(map<sizeType,sizeType>::const_iterator beg=MMap->begin(),end=MMap->end(); beg!=end; beg++)
      _Q[beg->first]=weights[beg->second];
  } else {
    _Q=weights;
  }
}
//helper
SimpleQPolicy::Vec SimpleQPolicy::eval(const vector<pair<sizeType,scalarD> >& stencil,const Vec& param) const
{
  sizeType nrA=_action.size();
  Vec ret=Vec::Zero(nrA);
  for(sizeType i=0; i<(sizeType)stencil.size(); i++)
    ret+=param.segment(stencil[i].first,nrA)*stencil[i].second;
  return ret;
}
void SimpleQPolicy::evalGrad(Vec& grad,const vector<pair<sizeType,scalarD> >& stencil,sizeType id,scalarD coef) const
{
  for(sizeType i=0; i<(sizeType)stencil.size(); i++) {
    scalarD& gradVal=grad[stencil[i].first+id];
    OMP_ATOMIC_
    gradVal+=coef*stencil[i].second;
  }
}
void SimpleQPolicy::evalHess(TRIPS& hess,const vector<pair<sizeType,scalarD> >& stencil,sizeType row,sizeType id,scalarD coef) const
{
  for(sizeType i=0; i<(sizeType)stencil.size(); i++)
    hess.push_back(Eigen::Triplet<scalarD,sizeType>(row,stencil[i].first+id,stencil[i].second*coef));
}
SimpleQPolicy::SimpleQPolicy(const string& name):Policy(name) {}
//StablizedSimpleQPolicy
StablizedSimpleQPolicy::StablizedSimpleQPolicy()
  :SimpleQPolicy(typeid(StablizedSimpleQPolicy).name()) {}
StablizedSimpleQPolicy::StablizedSimpleQPolicy(const Vec& L,const Vec& U,const Vec& action,sizeType res)
  :SimpleQPolicy(typeid(StablizedSimpleQPolicy).name())
{
  sizeType off=0;
  _action=action;
  _tensor.createQTensor(0,L,U,res,action.size(),off);
  _Q.setZero(off);
  _QTarget=_Q;
}
scalarD StablizedSimpleQPolicy::QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* hess) const
{
  scalarD ret=0,loss=0;
  sizeType id=-1,idNext=-1;
  scalarD coef=1/(scalarD)(to-from),coefSqrt=sqrt(coef);
  if(grad)grad->setZero(nrParam());
  if(hess)hess->clear();

  vector<pair<sizeType,scalarD> > stencil,stencilNext;
  OMP_PARALLEL_FOR_I(OMP_FPRI(loss,id,idNext,stencil,stencilNext) OMP_ADD(ret))
  for(sizeType i=from; i<to; i++) {
    stencil.clear();
    stencilNext.clear();
    id=index(trans[i]._action[0]);
    _tensor.findQValue(trans[i]._state,stencil,1);
    _tensor.findQValue(trans[i]._nextState,stencilNext,gamma);
    {
      loss=eval(stencil,_Q)[id]-trans[i]._reward;
      if(!trans[i]._last)
        loss-=eval(stencilNext,_QTarget).maxCoeff(&idNext);
    }
    if(grad)
      evalGrad(*grad,stencil,id,loss*coef);
    if(hess)
      evalHess(*hess,stencil,i-from,id,coefSqrt);
    ret+=loss*loss*coef;
  }
  return ret/2;
}
void StablizedSimpleQPolicy::updateQTarget()
{
  _QTarget=toVec();
}
//DQN Policy wrapper
NeuralNetQPolicy::NeuralNetQPolicy()
  :NeuralNetPolicy(typeid(NeuralNetQPolicy).name()),_eps(0) {}
NeuralNetQPolicy::NeuralNetQPolicy(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,const Vec& action,
                                   boost::property_tree::ptree* pt,boost::shared_ptr<FeatureTransformLayer> feature)
  :NeuralNetPolicy(typeid(NeuralNetQPolicy).name()),_eps(0)
{
  ASSERT_MSG(pt,"You must provide property tree for NeuralNetQPolicy!")
  pt->put<scalar>("HFParam.initWeightNorm",0.001f);
  pt->put<scalar>("HFParam.initBiasNorm",0.001f);
  pt->put<string>("activationFinal","");
  _net.reset(new NeuralNet(nrH1,nrH2,nrH3,nrState,action.size(),pt,NULL,feature));
  _action=action;
}
Matd NeuralNetQPolicy::getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples)
{
  Matd ret=Vec::Zero(1,output.cols());
  Matd QValue=_net->foreprop(&output);
  if(possLog)
    possLog->setZero(output.cols());
  //OMP_PARALLEL_FOR_
  for(sizeType j=0; j<output.cols(); j++)
    if(!_deterministic && RandEngine::randR01() < _eps) {
      //exploration
      ret(0,j)=_action[RandEngine::randI(0,_action.size()-1)];
      if(possLog)
        (*possLog)[j]=_eps/(scalar)_action.size();
    } else {
      //deterministic
      sizeType maxId;
      QValue.col(j).maxCoeff(&maxId);
      ret(0,j)=_action[maxId];
      if(possLog)
        (*possLog)[j]=1-_eps;
    }
  return ret;
}
scalarD NeuralNetQPolicy::QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* trips) const
{
  buildQLoss(trans,gamma,from,to);
  scalarD ret=_net->foreprop(NULL)(0,0);
  if(grad) {
    _net->backprop(Vec::Ones(1));
    *grad=_net->toVec(true);
  }
  _net->clearData();
  return ret;
}
void NeuralNetQPolicy::optimizeQLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to) const
{
  buildQLoss(trans,gamma,from,to);
  _net->train();
  _net->clearData();
}
void NeuralNetQPolicy::debugQLoss()
{
#define NR_TRANS 200
#define NR_TEST 100
#define DELTA 1E-7f
  Vec grad,delta;
  fromVec(Vec::Random(nrParam()));
  sizeType nrState=_net->nrInput();
  vector<Transition> trans(NR_TRANS);
  for(sizeType i=0; i<(sizeType)trans.size(); i++) {
    trans[i]._state=Vec::Random(nrState);
    trans[i]._action=Policy::getAction(trans[i]._state,NULL,NULL);
    trans[i]._nextState=Vec::Zero(nrState);
    trans[i]._nextState=Vec::Random(nrState);
    trans[i]._reward=RandEngine::randR(-1,1);
    trans[i]._last=false;
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    scalarD loss=QLoss(trans,0.9f,NR_TRANS/3,NR_TRANS*2/3,&grad,NULL);
    delta=Vec::Random(nrParam());
    fromVec(toVec()+delta*DELTA);
    scalarD loss2=QLoss(trans,0.9f,NR_TRANS/3,NR_TRANS*2/3,NULL,NULL);
    DEBUG_COMPARE("GradientQLoss",delta.dot(grad),(delta.dot(grad)-(loss2-loss)/DELTA),1E-5f);
  }
#undef DELTA
#undef NR_TEST
#undef NR_TRANS
}
sizeType NeuralNetQPolicy::nrParam() const
{
  return nrNetParam();
}
sizeType NeuralNetQPolicy::nrAction() const
{
  return 1;
}
NeuralNetQPolicy::Vec NeuralNetQPolicy::toVec() const
{
  return _net->toVec(false);
}
void NeuralNetQPolicy::fromVec(const Vec& weights)
{
  _net->fromVec(weights);
}
sizeType NeuralNetQPolicy::index(scalarD val) const
{
  for(sizeType i=0; i<_action.size(); i++)
    if(_action[i] == val)
      return i;
  ASSERT(false)
  return -1;
}
void NeuralNetQPolicy::setEps(scalarD eps)
{
  _eps=eps;
}
const NeuralNetQPolicy::Vec& NeuralNetQPolicy::getAction() const
{
  return _action;
}
void NeuralNetQPolicy::updateQTarget()
{
  _targetNet=_net->toVec(false);
}
NeuralNetQPolicy::NeuralNetQPolicy(const string& name):NeuralNetPolicy(name) {}
void NeuralNetQPolicy::buildQLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to) const
{
  //rewards
  Matd states=Matd::Zero(trans[0]._state.size(),to-from);
  Matd nextStates=Matd::Zero(trans[0]._nextState.size(),to-from);
  Coli actionIds=Coli::Zero(to-from);
  Vec rewards=Vec::Zero(to-from);

  //use temporary network
  Vec currNet=_net->toVec(false);
  if(_targetNet.size() != currNet.size())
    const_cast<NeuralNetQPolicy&>(*this).updateQTarget();

  //accumulate data
  _net->clearData();
  _net->fromVec(_targetNet);
  for(sizeType i=from; i<to; i++) {
    states.col(i-from)=trans[i]._state;
    nextStates.col(i-from)=trans[i]._nextState;
    actionIds[i-from]=index(trans[i]._action[0]);
  }
  const Matd& QVals=_net->foreprop(&nextStates);
  for(sizeType i=from; i<to; i++) {
    rewards[i-from]=trans[i]._reward;
    if(!trans[i]._last)
      rewards[i-from]+=gamma*QVals.col(i-from).maxCoeff();
  }
  _net->fromVec(currNet);
  _net->setData(states,rewards,actionIds);
}
//HessianKLMatrix
class HessianKLMatrixSoftMax : public KrylovMatrix<scalarD>
{
public:
  virtual void multiply(const Vec& b,Vec& out) const {
    _net->fromVec(b,true);
    Matd& diffBack=const_cast<Matd&>(_net->weightprop(NULL));
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<diffBack.cols(); i++)
      diffBack.col(i)=(_hess[i]*diffBack.col(i)).eval();
    _net->backprop(NULL);

    out.resize(b.size());
    out=_net->toVec(true);
  }
  virtual sizeType n() const {
    return _nrParam;
  }
  vector<Matd,Eigen::aligned_allocator<Matd> > _hess;
  boost::shared_ptr<NeuralNet> _net;
  sizeType _nrParam;
};
//NeuralNet ProbabilisticQPolicy
NeuralNetProbQPolicy::NeuralNetProbQPolicy()
  :NeuralNetQPolicy(typeid(NeuralNetProbQPolicy).name())
{
  _eps=0;
}
NeuralNetProbQPolicy::NeuralNetProbQPolicy(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,const Vec& action,
    boost::property_tree::ptree* pt,boost::shared_ptr<FeatureTransformLayer> feature)
  :NeuralNetQPolicy(typeid(NeuralNetProbQPolicy).name())
{
  _eps=0;
  ASSERT_MSG(pt,"You must provide property tree for NeuralNetProbQPolicy!")
  pt->put<scalar>("HFParam.initWeightNorm",0.001f);
  pt->put<scalar>("HFParam.initBiasNorm",0.001f);
  pt->put<string>("activationFinal","");
  _net.reset(new NeuralNet(nrH1,nrH2,nrH3,nrState,action.size(),pt,NULL,feature));
  _action=action;
}
Matd NeuralNetProbQPolicy::getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples)
{
  Matd ret=Matd::Zero(1,output.cols());
  Matd diffQValue,QValue=_net->foreprop(&output);
  if(possLog)
    possLog->setZero(output.cols());
  if(possLogGrad && weightPoss)
    diffQValue.setZero(output.rows(),output.cols());
  //OMP_PARALLEL_FOR_
  for(sizeType j=0; j<output.cols(); j++) {
    if(_deterministic) {
      sizeType mid;
      QValue.col(j).maxCoeff(&mid);
      ret(0,j)=_action[mid];
    } else {
      if(samples)
        ret(0,j)=(*samples)(0,j);
      else ret(0,j)=_action[proposeSoftMax(QValue.col(j))];
      if(possLog)
        (*possLog)[j]=logPossSoftMax(QValue.col(j),index(ret(0,j)));
      if(possLogGrad && weightPoss)
        diffQValue.col(j)=DLogPossDXSoftMax(QValue.col(j),index(ret(0,j)))*(*weightPoss)[j];
    }
  }
  if(possLogGrad && weightPoss) {
    _net->backprop(&diffQValue);
    *possLogGrad=_net->toVec(true);
  }
  return ret;
}
boost::shared_ptr<KrylovMatrix<scalarD> > NeuralNetProbQPolicy::getHessKLDistance(const Matd& states) const
{
  //compute hessian
  boost::shared_ptr<HessianKLMatrixSoftMax> ret(new HessianKLMatrixSoftMax);
  Matd& xs=_net->foreprop(&states);
  ret->_hess.resize(states.cols());
  ret->_nrParam=nrParam();
  ret->_net=_net;
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<xs.cols(); i++) {
    ret->_hess[i]=hessKLDistanceSoftMax(xs.col(i));
    ret->_hess[i]/=(scalarD)states.cols();
  }
  //return
  return boost::dynamic_pointer_cast<KrylovMatrix<scalarD> >(ret);
}
scalarD NeuralNetProbQPolicy::getDiffKLDistance(const Matd& states,const Matd& xsOld,Vec* grad) const
{
  //compute gradient
  scalarD ret=0;
  sizeType offNetParam=nrNetParam();
  if(grad)*grad=Vec::Zero(nrParam());
  Matd& xsNew=_net->foreprop(&states);
  Matd diffActionsNew=Matd::Zero(xsNew.rows(),xsNew.cols());
  OMP_PARALLEL_FOR_I(OMP_ADD(ret))
  for(sizeType i=0; i<states.cols(); i++) {
    ret+=KLDistanceSoftMax(xsOld.col(i),xsNew.col(i));
    diffActionsNew.col(i)=diffKLDistanceSoftMax(xsOld.col(i),xsNew.col(i));
  }
  //assemble
  if(grad) {
    _net->backprop(&diffActionsNew);
    grad->segment(0,offNetParam)=_net->toVec(true);
    *grad/=(scalarD)states.cols();
  }
  return ret/(scalarD)states.cols();
}
scalarD NeuralNetProbQPolicy::getDiffKLDistance(const Matd& states,const Vec& paramOld,Vec* grad)
{
  Vec param=toVec();
  fromVec(paramOld);
  const Matd xsOld=_net->foreprop(&states);

  fromVec(param);
  return const_cast<const NeuralNetProbQPolicy&>(*this).getDiffKLDistance(states,xsOld,grad);
}

PRJ_END
