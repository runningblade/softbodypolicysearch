#include "DMPLayer.h"

USE_PRJ_NAMESPACE

#define USE_HSQR

//rhythmic movement primitives
DMPLayer::DMPLayer()
  :NeuralLayer(typeid(DMPLayer).name())
{
  _wBiasOptimizable=false;
  _wOptimizable=true;
  _hOptimizable=true;
  _muOptimizable=true;
  _tauOptimizable=true;
}
DMPLayer::DMPLayer(sizeType nrOutput,sizeType nrBasis)
  :NeuralLayer(typeid(DMPLayer).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _h.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;

  _wBiasOptimizable=false;
  _wOptimizable=true;
  _hOptimizable=true;
  _muOptimizable=true;
  _tauOptimizable=true;
}
bool DMPLayer::read(istream& is,IOData* dat)
{
  readBinaryData(_wBias,is,dat);
  readBinaryData(_w,is,dat);
  readBinaryData(_h,is,dat);
  readBinaryData(_mu,is,dat);
  readBinaryData(_tau,is,dat);

  readBinaryData(_wBiasOptimizable,is);
  readBinaryData(_wOptimizable,is);
  readBinaryData(_hOptimizable,is);
  readBinaryData(_muOptimizable,is);
  readBinaryData(_tauOptimizable,is);
  return is.good();
}
bool DMPLayer::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_wBias,os,dat);
  writeBinaryData(_w,os,dat);
  writeBinaryData(_h,os,dat);
  writeBinaryData(_mu,os,dat);
  writeBinaryData(_tau,os,dat);

  writeBinaryData(_wBiasOptimizable,os);
  writeBinaryData(_wOptimizable,os);
  writeBinaryData(_hOptimizable,os);
  writeBinaryData(_muOptimizable,os);
  writeBinaryData(_tauOptimizable,os);
  return os.good();
}
DMPLayer& DMPLayer::operator=(const DMPLayer& other)
{
  NeuralLayer::operator=(other);
  _wBias=other._wBias;
  _w=other._w;
  _h=other._h;
  _mu=other._mu;
  _tau=other._tau;
  return *this;
}
boost::shared_ptr<Serializable> DMPLayer::copy() const
{
  boost::shared_ptr<DMPLayer> ret(new DMPLayer(_w.rows(),_w.cols()));
  *ret=*this;
  return ret;
}
void DMPLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,_w.rows(),x.cols());
  fx=_wBias*Vec::Ones(x.cols()).transpose();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<x.cols(); i++) {
    scalarD xI=x(0,i);
    for(sizeType r=0; r<_w.rows(); r++)
      for(sizeType c=0; c<_w.cols(); c++) {
        scalarD phase=cos(xI*_tau-_mu(r,c))-1;
        scalarD hRC=_h(r,c);
#ifdef USE_HSQR
        fx(r,i)+=exp(hRC*hRC*phase)*_w(r,c);
#else
        fx(r,i)+=exp(hRC*phase)*_w(r,c);
#endif
      }
  }
}
void DMPLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  _dwBias.setZero(_w.rows());
  _dw.setZero(_w.rows(),_w.cols());
  _dh.setZero(_h.rows(),_h.cols());
  _dmu.setZero(_mu.rows(),_mu.cols());
  _dtau=0;
  enforceSize(dEdx,x);
  backpropInner(x.block(0,0,x.rows(),x.cols()),
                fx.block(0,0,fx.rows(),fx.cols()),
                dEdx.block(0,0,dEdx.rows(),dEdx.cols()),
                dEdfx.block(0,0,dEdfx.rows(),dEdfx.cols()));
}
void DMPLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  ASSERT_MSG(false,"Weightprop of DMP not supported!")
}
void DMPLayer::augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt)
{
  DMPLayer layer(_w.rows(),nrDMPBasis);
  layer.configOffline(pt);
  _w=concatCol(_w,layer._w);
  _h=concatCol(_h,layer._h);
  _mu=concatCol(_mu,layer._mu);
}
void DMPLayer::configOffline(const boost::property_tree::ptree& pt)
{
  scalarD initW=pt.get<scalarD>("initDMPPerturb",1E-3f);
  scalarD initP=pt.get<scalarD>("initDMPPeriod",0.5f);

  _wBias.setRandom();
  _wBias*=initW;

  _w.setRandom();
  _w*=initW;

  _h.setRandom();
  _h.array()+=1.0f;
  _h*=initW;

  if(pt.get<bool>("reinitTau",false))
    _tau=M_PI*2/initP;

  _mu.setRandom();
  _mu*=initW;
  for(sizeType j=0; j<_mu.cols(); j++) {
    scalar coef=((scalar)j+0.5f)/(scalar)_mu.cols();
    _mu.col(j).array()+=coef*M_PI*2;
  }
}
void DMPLayer::setParam(const Vec& p,sizeType off,bool diff)
{
  if(_wBiasOptimizable) {
    Vec& wBias=diff?_dwBias:_wBias;
    Eigen::Map<Vec>(wBias.data(),wBias.size())=p.segment(off,wBias.size());
    off+=wBias.size();
  }
  if(_wOptimizable) {
    Matd& w=diff?_dw:_w;
    Eigen::Map<Vec>(w.data(),w.size())=p.segment(off,w.size());
    off+=w.size();
  }
  if(_hOptimizable) {
    Matd& h=diff?_dh:_h;
    Eigen::Map<Vec>(h.data(),h.size())=p.segment(off,h.size());
    off+=h.size();
  }
  if(_muOptimizable) {
    Matd& mu=diff?_dmu:_mu;
    Eigen::Map<Vec>(mu.data(),mu.size())=p.segment(off,mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      _dtau=p[off];
    else _tau=p[off];
    off++;
  }
}
void DMPLayer::param(Vec& p,sizeType off,bool diff) const
{
  if(_wBiasOptimizable) {
    const Vec& wBias=diff?_dwBias:_wBias;
    p.segment(off,wBias.size())+=Eigen::Map<const Vec>(wBias.data(),wBias.size());
    off+=wBias.size();
  }
  if(_wOptimizable) {
    const Matd& w=diff?_dw:_w;
    p.segment(off,w.size())+=Eigen::Map<const Vec>(w.data(),w.size());
    off+=w.size();
  }
  if(_hOptimizable) {
    const Matd& h=diff?_dh:_h;
    p.segment(off,h.size())+=Eigen::Map<const Vec>(h.data(),h.size());
    off+=h.size();
  }
  if(_muOptimizable) {
    const Matd& mu=diff?_dmu:_mu;
    p.segment(off,mu.size())+=Eigen::Map<const Vec>(mu.data(),mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      p[off]+=_dtau;
    else p[off]+=_tau;
    off++;
  }
}
sizeType DMPLayer::nrParam() const
{
  sizeType ret=0;
  if(_wBiasOptimizable)
    ret+=_wBias.size();
  if(_wOptimizable)
    ret+=_w.size();
  if(_hOptimizable)
    ret+=_h.size();
  if(_muOptimizable)
    ret+=_mu.size();
  if(_tauOptimizable)
    ret++;
  return ret;
}
sizeType DMPLayer::nrOutput(sizeType lastIn) const
{
  ASSERT(lastIn == 1)
  return _w.rows();
}
Matd DMPLayer::getBounds(const boost::property_tree::ptree& pt) const
{
  sizeType off=0;
  Matd ret=Matd::Zero(nrParam(),2);
  if(_wBiasOptimizable) {
    ret.block(off,0,_wBias.size(),1).setConstant(pt.get<scalar>("minBias",0.0f));
    ret.block(off,1,_wBias.size(),1).setConstant(pt.get<scalar>("maxBias",0.0f));
    off+=_wBias.size();
  }
  if(_wOptimizable) {
    ret.block(off,0,_w.size(),1).setConstant(-pt.get<scalar>("maxWeight",100.0f));
    ret.block(off,1,_w.size(),1).setConstant( pt.get<scalar>("maxWeight",100.0f));
    off+=_w.size();
  }
  if(_hOptimizable) {
    ret.block(off,0,_h.size(),1).setConstant(-pt.get<scalar>("maxActivation",5.0f));
    ret.block(off,1,_h.size(),1).setConstant( pt.get<scalar>("maxActivation",5.0f));
    off+=_h.size();
  }
  if(_muOptimizable) {
    ret.block(off,0,_mu.size(),1).setConstant(0);
    ret.block(off,1,_mu.size(),1).setConstant(M_PI*2);
    off+=_mu.size();
  }
  if(_tauOptimizable) {
    ret(off,0)=M_PI*2/pt.get<scalar>("maxPeriod",5.0f);
    ret(off,1)=M_PI*2/pt.get<scalar>("minPeriod",0.5f);
    off++;
  }
  return ret;
}
scalarD DMPLayer::period() const
{
  return M_PI*2/tau();
}
void DMPLayer::backpropInner(CMBlk x,CMBlk fx,MBlk dEdx,CMBlk dEdfx)
{
  initCache(x);
  dEdx.setZero();
  scalarD dtau=0;
  Vec xV=x.row(0).transpose();
  OMP_PARALLEL_FOR_I(OMP_ADD(dtau))
  for(sizeType r=0; r<_w.rows(); r++) {
    Vec& phase=_phase[omp_get_thread_num()];
    Vec& phaseCos=_phaseCos[omp_get_thread_num()];
    Vec& act=_act[omp_get_thread_num()];
    Vec& dEdfxRow=_dEdfxRow[omp_get_thread_num()];

    dEdfxRow=dEdfx.row(r);
    for(sizeType c=0; c<_w.cols(); c++) {
      scalarD& dw=_dw(r,c);
      scalarD& dh=_dh(r,c);
      scalarD& dmu=_dmu(r,c);
      scalarD hRC=_h(r,c),wRC=_w(r,c);
#ifdef USE_HSQR
      scalarD hSqr=hRC*hRC,hTwo=hRC*2;
#else
      scalarD hSqr=hRC,hTwo=1;
#endif
      scalarD hSqrWRC=hSqr*wRC;
      scalarD hTwoWRC=hTwo*wRC;
      phase=((xV*_tau).array()-_mu(r,c)).matrix();
      phaseCos=(phase.array().cos()-1).matrix();
      act=(phaseCos.array()*hSqr).exp().matrix();
      //dw
      act.array()*=dEdfxRow.array();
      dw+=act.array().sum();
      //dh
      dh+=(act.array()*phaseCos.array()).sum()*hTwoWRC;
      //dmu
      act.array()*=phase.array().sin();
      dmu+=act.array().sum()*hSqrWRC;
      //dtau
      act.array()*=-xV.array();
      dtau+=act.array().sum()*hSqrWRC;
    }
  }
  _dtau+=dtau;
  _dwBias+=dEdfx*Vec::Ones(dEdfx.cols());
}
void DMPLayer::initCache(const Matd& x)
{
  if((sizeType)_phase.size() != omp_get_num_procs() || _phase[0].size() != x.cols()) {
    _phase.assign(omp_get_num_procs(),Vec::Zero(x.cols()));
    _phaseCos.assign(omp_get_num_procs(),Vec::Zero(x.cols()));
    _act.assign(omp_get_num_procs(),Vec::Zero(x.cols()));
    _dEdfxRow.assign(omp_get_num_procs(),Vec::Zero(x.cols()));
  }
}
DMPLayer::DMPLayer(const string& name)
  :NeuralLayer(name)
{
  _wOptimizable=true;
  _hOptimizable=true;
  _muOptimizable=true;
  _tauOptimizable=true;
}
//non-rhythmic movement primitives
NRDMPLayer::NRDMPLayer():DMPLayer(typeid(NRDMPLayer).name()) {}
NRDMPLayer::NRDMPLayer(sizeType nrOutput,sizeType nrBasis):DMPLayer(typeid(NRDMPLayer).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _h.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;
}
boost::shared_ptr<Serializable> NRDMPLayer::copy() const
{
  boost::shared_ptr<NRDMPLayer> ret(new NRDMPLayer(_w.rows(),_w.cols()));
  *ret=*this;
  return ret;
}
void NRDMPLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,_w.rows(),x.cols());
  fx=_wBias*Vec::Ones(x.cols()).transpose();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<x.cols(); i++) {
    scalarD xI=x(0,i);
    for(sizeType r=0; r<_w.rows(); r++)
      for(sizeType c=0; c<_w.cols(); c++) {
        scalarD phase=_h(r,c)*xI-_mu(r,c);
        fx(r,i)+=exp(-phase*phase)*_w(r,c)*(xI-_tau);
      }
  }
}
void NRDMPLayer::configOffline(const boost::property_tree::ptree& pt)
{
  scalarD initW=pt.get<scalarD>("initDMPPerturb",1E-3f);
  scalarD span=pt.get<scalarD>("initTimeSpan",1);
  scalarD dt=span/(scalarD)_mu.cols();
  scalarD hRef=sqrt(-log(0.1f))/dt;
  INFOV("Reset DMPLayer with initTimeSpan: %f, dt: %f",span,dt)

  _wBias.setRandom();
  _wBias*=initW;

  _w.setRandom();
  _w*=initW;

  _h.setRandom();
  _h*=initW;
  _h.array()+=hRef;

  _mu.setRandom();
  _mu*=initW;
  for(sizeType j=0; j<_mu.cols(); j++) {
    scalar coef=((scalar)j+0.5f)/(scalar)_mu.cols();
    _mu.col(j).array()+=span*coef*hRef;
  }

  _tau=RandEngine::randR01()*initW;
}
void NRDMPLayer::backpropInner(CMBlk x,CMBlk fx,MBlk dEdx,CMBlk dEdfx)
{
  initCache(x);
  dEdx.setZero();
  scalarD dtau=0;
  Vec xV=x.row(0).transpose();
  OMP_PARALLEL_FOR_I(OMP_ADD(dtau))
  for(sizeType r=0; r<_w.rows(); r++) {
    Vec& phase=_phase[omp_get_thread_num()];
    Vec& act=_act[omp_get_thread_num()];
    Vec& dEdfxRow=_dEdfxRow[omp_get_thread_num()];

    dEdfxRow=dEdfx.row(r);
    for(sizeType c=0; c<_w.cols(); c++) {
      scalarD& dw=_dw(r,c);
      scalarD& dh=_dh(r,c);
      scalarD& dmu=_dmu(r,c);
      scalarD wRC=_w(r,c);
      phase=(_h(r,c)*xV.array()-_mu(r,c)).matrix();
      act=(-phase.array().square()).exp().matrix();
      //dtau
      act.array()*=dEdfxRow.array();
      dtau-=act.array().sum()*wRC;
      //dw
      act.array()*=(xV.array()-_tau);
      dw+=act.array().sum();
      //dmu
      act.array()*=2*phase.array();
      dmu+=act.array().sum()*wRC;
      //dh
      act.array()*=-xV.array();
      dh+=act.array().sum()*wRC;
    }
  }
  _dtau+=dtau;
  _dwBias+=dEdfx*Vec::Ones(dEdfx.cols());
}
NRDMPLayer::NRDMPLayer(const string& name):DMPLayer(name) {}
//cos-only movement primitives
CosDMPLayer::CosDMPLayer():DMPLayer(typeid(CosDMPLayer).name())
{
  _hOptimizable=false;
}
CosDMPLayer::CosDMPLayer(sizeType nrOutput,sizeType nrBasis):DMPLayer(typeid(CosDMPLayer).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _h.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;
  _hOptimizable=false;
}
boost::shared_ptr<Serializable> CosDMPLayer::copy() const
{
  boost::shared_ptr<CosDMPLayer> ret(new CosDMPLayer(_w.rows(),_w.cols()));
  *ret=*this;
  return ret;
}
void CosDMPLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  ASSERT(_hOptimizable == false)
  enforceSize(fx,_w.rows(),x.cols());
  fx=_wBias*Vec::Ones(x.cols()).transpose();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<x.cols(); i++) {
    scalarD xI=x(0,i);
    for(sizeType r=0; r<_w.rows(); r++)
      for(sizeType c=0; c<_w.cols(); c++)
        fx(r,i)+=cos(xI*_tau-_mu(r,c))*_w(r,c);
  }
}
void CosDMPLayer::backpropInner(CMBlk x,CMBlk fx,MBlk dEdx,CMBlk dEdfx)
{
  initCache(x);
  dEdx.setZero();
  scalarD dtau=0;
  Vec xV=x.row(0).transpose();
  OMP_PARALLEL_FOR_I(OMP_ADD(dtau))
  for(sizeType r=0; r<_w.rows(); r++) {
    Vec& phase=_phase[omp_get_thread_num()];
    Vec& phaseCos=_phaseCos[omp_get_thread_num()];
    Vec& act=_act[omp_get_thread_num()];
    Vec& dEdfxRow=_dEdfxRow[omp_get_thread_num()];

    dEdfxRow=dEdfx.row(r);
    for(sizeType c=0; c<_w.cols(); c++) {
      scalarD& dw=_dw(r,c);
      scalarD& dmu=_dmu(r,c);
      scalarD wRC=_w(r,c);
      phase=((xV*_tau).array()-_mu(r,c)).matrix();
      phaseCos=phase.array().cos().matrix();
      //dw
      act.array()=phase.array().cos()*dEdfxRow.array();
      dw+=act.array().sum();
      //dmu
      act.array()=phase.array().sin()*dEdfxRow.array();
      dmu+=act.array().sum()*wRC;
      //dtau
      act.array()*=-xV.array();
      dtau+=act.array().sum()*wRC;
    }
  }
  _dtau+=dtau;
  _dwBias+=dEdfx*Vec::Ones(dEdfx.cols());
}
CosDMPLayer::CosDMPLayer(const string& name):DMPLayer(name)
{
  _hOptimizable=false;
}
