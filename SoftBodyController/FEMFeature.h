#ifndef FEM_FEATURE_H
#define FEM_FEATURE_H

#include "NeuralNet.h"
#include <Dynamics/FEMCIOEngine.h>

PRJ_BEGIN

//Feature
struct FEMFeature {
  virtual ~FEMFeature() {}
  virtual void writeVTK(const FeatureTransformLayer::Vec& x,VTKWriter<scalar>& os) const=0;
  virtual void writeVTK(const FeatureTransformLayer::Vec& x,const string& path) const;
};
struct NoFeatureTransformLayer : public FeatureTransformLayer {
  NoFeatureTransformLayer();
  NoFeatureTransformLayer(sizeType nrState);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual NoFeatureTransformLayer& operator=(const NoFeatureTransformLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
  virtual sizeType nrF() const override;
  virtual sizeType nrInput() const override;
protected:
  sizeType _nrState;
};
struct ConstFeatureTransformLayer : public FeatureTransformLayer {
  ConstFeatureTransformLayer();
  ConstFeatureTransformLayer(sizeType nrState);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual ConstFeatureTransformLayer& operator=(const ConstFeatureTransformLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
  virtual sizeType nrF() const override;
  virtual sizeType nrInput() const override;
  virtual void configOnline(const boost::property_tree::ptree& pt);
  Vec _aug;
};
struct FEMRSFeatureTransformLayer : public FeatureTransformLayer, public FEMFeature {
public:
  FEMRSFeatureTransformLayer();
  FEMRSFeatureTransformLayer(sizeType nrState,scalarD dt,sizeType dim,bool posOnly,bool defOnly=false);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual void writeVTK(const Vec& x,VTKWriter<scalar>& os) const override;
  virtual FEMRSFeatureTransformLayer& operator=(const FEMRSFeatureTransformLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
  virtual sizeType nrF() const override;
  virtual sizeType nrInput() const override;
protected:
  FEMRSFeatureTransformLayer(const string& name);
  void writeInfo(sizeType nrState) const;
  string varName(sizeType i,sizeType nrState) const;
  void addFeature(sizeType nrState,sizeType dim,sizeType offF,sizeType offT,scalarD coef,bool isVel,bool defOnly);
  vector<Vec2i,Eigen::aligned_allocator<Vec2i> > _from,_to;
  vector<scalarD> _coef;
  Matd _deriv;
};
struct FEMRotRSFeatureTransformLayer : public FeatureTransformLayer, public FEMFeature {
public:
  FEMRotRSFeatureTransformLayer();
  FEMRotRSFeatureTransformLayer(sizeType nrState,scalarD dt,bool posOnly,bool hasDefo,bool hasCOM);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual void writeVTK(const Vec& x,VTKWriter<scalar>& os) const override;
  virtual FEMRotRSFeatureTransformLayer& operator=(const FEMRotRSFeatureTransformLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
  sizeType offC() const;
  sizeType offW() const;
  virtual sizeType nrF() const override;
  virtual sizeType nrInput() const override;
protected:
  FEMRotRSFeatureTransformLayer(const string& name);
  Vec compute(const Vec& x,Matd* deriv) const;
  scalarD _dt;
  sizeType _nrState;
  sizeType _nrPFeat;
  bool _posOnly;
};
struct FeatureAugmentedInPlace : public FeatureTransformLayer, public FEMFeature {
  FeatureAugmentedInPlace();
  FeatureAugmentedInPlace(boost::shared_ptr<FeatureTransformLayer> feat1,
                          boost::shared_ptr<FeatureTransformLayer> feat2);
  FeatureAugmentedInPlace(boost::shared_ptr<FeatureTransformLayer> feat1,
                          boost::shared_ptr<FeatureTransformLayer> feat2,
                          boost::shared_ptr<FeatureTransformLayer> feat3);
  FeatureAugmentedInPlace(boost::shared_ptr<FeatureTransformLayer> feat1,
                          boost::shared_ptr<FeatureTransformLayer> feat2,
                          boost::shared_ptr<FeatureTransformLayer> feat3,
                          boost::shared_ptr<FeatureTransformLayer> feat4);
  FeatureAugmentedInPlace(boost::shared_ptr<FeatureTransformLayer> feat1,
                          boost::shared_ptr<FeatureTransformLayer> feat2,
                          boost::shared_ptr<FeatureTransformLayer> feat3,
                          boost::shared_ptr<FeatureTransformLayer> feat4,
                          boost::shared_ptr<FeatureTransformLayer> feat5);
  FeatureAugmentedInPlace(boost::shared_ptr<FeatureTransformLayer> feat1,
                          boost::shared_ptr<FeatureTransformLayer> feat2,
                          boost::shared_ptr<FeatureTransformLayer> feat3,
                          boost::shared_ptr<FeatureTransformLayer> feat4,
                          boost::shared_ptr<FeatureTransformLayer> feat5,
                          boost::shared_ptr<FeatureTransformLayer> feat6);
  FeatureAugmentedInPlace(vector<boost::shared_ptr<FeatureTransformLayer> > feats);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual void writeVTK(const Vec& x,VTKWriter<scalar>& os) const override;
  virtual FeatureAugmentedInPlace& operator=(const FeatureAugmentedInPlace& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
  virtual sizeType nrF() const override;
  virtual sizeType nrInput() const override;
  virtual void configOffline(const boost::property_tree::ptree& pt);
  virtual void configOnline(const boost::property_tree::ptree& pt);
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  template <typename T>
  boost::shared_ptr<T> getFeaturePtr(sizeType i) {
    ASSERT(i >= 0 && i < (sizeType)_feats.size())
    return boost::dynamic_pointer_cast<T>(_feats[i]);
  }
  sizeType nrFeature() const {
    return (sizeType)_feats.size();
  }
protected:
  vector<boost::shared_ptr<FeatureTransformLayer> > _feats;
};

PRJ_END

#endif
