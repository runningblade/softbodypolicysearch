#include "CIOOptimizerSelfCollisionTerm.h"
#include <boost/lexical_cast.hpp>
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/FEMUtils.h>

USE_PRJ_NAMESPACE

void addSelfColl(vector<CIOSelfColl>& SC,CIOSelfColl& sc)
{
  for(sizeType i=0; i<(sizeType)SC.size(); i++)
    if(!(SC[i] < sc) && !(sc < SC[i])) {
      sc._coef+=SC[i]._coef;
      SC[i]=sc;
      return;
    }
  SC.push_back(sc);
}
//CIOOptimizerSelfCollisionTerm
CIOOptimizerSelfCollisionTerm::CIOOptimizerSelfCollisionTerm(CIOOptimizer& opt):CIOOptimizerTerm(opt)
{
  //clear self collisions
  if(_opt._sol._tree.get<bool>("initContactUpdate",true))
    for(sizeType i=0; i<_opt._horizon; i++)
      getSelfColl(i).clear();
  buildCollisionOffset();
}
void CIOOptimizerSelfCollisionTerm::writeSelfCollVTK(const std::string& path)
{
  //Vec x=_opt.collectSolution(false);
  //updateSelfColl(x,NULL);

  recreate(path);
  FEMCIOEngine& eng=_opt._sol.getEngine();
  scalar avgLen=(scalar)eng.avgElementLength();
  FEMBody& body=eng.getBody();
  for(sizeType i=0; i<_opt._horizon; i++) {
    const vector<CIOSelfColl>& selfColl=getSelfColl(i);
    if(!selfColl.empty()) {
      VTKWriter<scalar> os("body",path+"/body"+boost::lexical_cast<string>(i)+".vtk",true);
      body._system->setPos(getCollConfig(i));
      body.writeVTK(os);
    }
    if(!selfColl.empty()) {
      VTKWriter<scalar> os("selfColl",path+"/selfColl"+boost::lexical_cast<string>(i)+".vtk",true);
      vector<scalar> css;
      vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
      for(sizeType c=0; c<(sizeType)selfColl.size(); c++) {
        const CIOSelfColl& sc=selfColl[c];
        const Vec3& v0=body.getV(sc._vid)._pos;
        const Vec3& v1=body.getC(sc._I._id).getVert(sc._I);
        const Vec3 v0n=v0+sc._n.cast<scalar>()*avgLen,v1n=v1-sc._n.cast<scalar>()*avgLen;

        css.push_back(0);
        css.push_back(0);
        css.push_back(1);
        css.push_back(1);
        css.push_back(1);
        css.push_back(1);

        vss.push_back(v0);
        vss.push_back(v1);
        vss.push_back(v0);
        vss.push_back(v0n);
        vss.push_back(v1);
        vss.push_back(v1n);
      }
      os.appendPoints(vss.begin(),vss.end());
      os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                     VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),
                     VTKWriter<scalar>::LINE);
      os.appendCustomPointData("color",css.begin(),css.end());
    }
  }
}
void CIOOptimizerSelfCollisionTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  Mat3Xd vG;
  Vec DFDXBlk;
  off+=_cOff[i];
  const vector<CIOSelfColl>& SC=eng._c.getSelfCons();
  for(sizeType c=0; c<(sizeType)SC.size(); c++) {
    const CIOSelfColl& VI=SC[c];
    scalarD dir=eng.getMCalc().gradVert(eng._x,VI._vid,&vG).dot(VI._n);
    DFDXBlk=vG.transpose()*VI._n;
    dir-=eng.getMCalc().gradVert(eng._x,VI._I,&vG).dot(VI._n);
    DFDXBlk-=vG.transpose()*VI._n;
    dir-=VI._dist;

    if(dir < 0) {
      scalarD coefSqrt=sqrt(VI._coef);
      fvec[off+c]=dir*coefSqrt;
      if(fjac)
        _opt.addBlockX(*fjac,off+c,i,DFDXBlk.transpose()*coefSqrt);
    }
  }
}
void CIOOptimizerSelfCollisionTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  Mat3Xd vG;
  Vec DFDXBlk;
  const vector<CIOSelfColl>& SC=eng._c.getSelfCons();
  for(sizeType c=0; c<(sizeType)SC.size(); c++) {
    const CIOSelfColl& VI=SC[c];
    scalarD dir=eng.getMCalc().gradVert(eng._x,VI._vid,&vG).dot(VI._n);
    DFDXBlk=vG.transpose()*VI._n;
    dir-=eng.getMCalc().gradVert(eng._x,VI._I,&vG).dot(VI._n);
    DFDXBlk-=vG.transpose()*VI._n;
    dir-=VI._dist;

    if(dir < 0) {
      FX+=pow(dir,2)*VI._coef/2;
      _opt.addBlockX(DFDX,i,DFDXBlk*(dir*VI._coef));
    }
  }
}
void CIOOptimizerSelfCollisionTerm::updateSelfColl(vector<boost::shared_ptr<FEMCIOEngine> >* mp)
{
  //find colliding frames
  CIOSelfColl sc;
  vector<sizeType> selfCollFrms;
  sizeType nrSys=_opt._sol.getEngine().getMCalc().size();
  sizeType interval=_opt._sol._tree.get<sizeType>("modifyInterval",10);
  scalarD collK=_opt._sol.getEngine()._tree.get<scalar>("collK")*(scalar)interval;
  scalarD avgLen=_opt._sol.getEngine().avgElementLength();

  OMP_PARALLEL_FOR_XI(mp ? OmpSettings::getOmpSettings().nrThreads() : 1,OMP_PRI(sc))
  for(sizeType i=0; i<_opt._horizon; i++) {
    FEMCIOEngine& eng=mp ? *(*mp)[omp_get_thread_num()] : _opt._sol.getEngine();
    vector<CIOSelfColl>& selfColl=getSelfColl(i);
    FEMBody& body=eng.getBody();

    //add self colliding pair
    GradientInfo info=getCollConfig(i);
    if(eng.hasSelfColl(info)) {
      Vec2d bd=eng.findBoundarySelfCollConfiguration(info,0.25f);
      eng.addSelfCollEnergy(info,bd,sc);
      if(sc._vid == -1) {
        {
          WARNING("Detecting strange error in self collision handling, dumping configuration!")
          boost::filesystem::ofstream os("errorSelfColl.dat",ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          info.write(os,dat.get());
          writeBinaryData(bd,os,dat.get());
        }
        exit(-1);
      } else {
        sc._coef=collK;
        addSelfColl(selfColl,sc);
        OMP_CRITICAL_
        selfCollFrms.push_back(i);
      }
      //x.segment(nrSys*(i+1),nrSys-6)*=bd[0];
      //set to closest collision free configuration
      Vec xDeform=info.x().segment(0,nrSys-6)*bd[0];
      Vec xRigid=info.x().segment(nrSys-6,6);
      info.reset(eng.getMCalc(),concat(xDeform,xRigid));
    }

    //update collision
    body._system->setPos(info);
    for(sizeType c=0; c<(sizeType)selfColl.size(); c++) {
      CIOSelfColl& VI=selfColl[c];
      VI._n=(body.getV(VI._vid)._pos-body.getC(VI._I._id).getVert(VI._I)).cast<scalarD>();
      VI._dist=min(max<scalarD>(VI._n.norm(),1E-10f),avgLen);
      VI._n/=max<scalarD>(VI._n.norm(),1E-10f);
    }
    precompute(i,eng,info,selfColl);
  }

  //self colliding frames
  cout << "Self-colliding frames: ";
  for(sizeType i=0; i<(sizeType)selfCollFrms.size(); i++)
    cout << selfCollFrms[i] << " ";
  cout << endl;
  //add collision offset
  buildCollisionOffset();
}
void CIOOptimizerSelfCollisionTerm::precompute(sizeType i,const FEMCIOEngine& eng,const GradientInfo& info,vector<CIOSelfColl>& selfColl) {}
void CIOOptimizerSelfCollisionTerm::debugConsistency(sizeType i,const FEMCIOEngine& eng,const vector<CIOSelfColl>& selfColl) {}
void CIOOptimizerSelfCollisionTerm::addRandomSelfCollision()
{
  FEMCIOEngine& eng=_opt._sol.getEngine();
  FEMBody& body=eng.getBody();

  for(sizeType i=0; i<(sizeType)_opt._horizon; i++) {
    vector<CIOSelfColl>& SC=getSelfColl(i);
    SC.resize(RandEngine::randI(0,10));
    for(sizeType j=0; j<(sizeType)SC.size(); j++) {
      CIOSelfColl& SCJ=SC[j];
      SCJ._vid=RandEngine::randI(0,body.nrV()-1);
      SCJ._I._id=RandEngine::randI(0,body.nrC()-1);
      SCJ._I._coef=Vec4::Random();
      SCJ._dist=RandEngine::randI(0,1)*body.getBB(true).getExtent().maxCoeff();
      SCJ._coef=RandEngine::randI(0,1);
      SCJ._n.setRandom();
    }

    body._system->setPos(eng._x);
    precompute(i,eng,eng._x,SC);
    debugConsistency(i,eng,SC);
  }
  buildCollisionOffset();
}
const Coli& CIOOptimizerSelfCollisionTerm::getCollisionOff() const
{
  return _cOff;
}
int CIOOptimizerSelfCollisionTerm::values() const
{
  return _cOff[_cOff.size()-1];
}
bool CIOOptimizerSelfCollisionTerm::valid() const
{
  return _opt._sol._tree.get<bool>("handleSelfCollision",false);
}
void CIOOptimizerSelfCollisionTerm::buildCollisionOffset()
{
  _cOff.setZero(_opt._horizon+1);
  for(sizeType i=0; i<_opt._horizon; i++) {
    vector<CIOSelfColl>& sc=getSelfColl(i);
    _cOff[i+1]=_cOff[i]+(sizeType)sc.size();
  }
}
vector<CIOSelfColl>& CIOOptimizerSelfCollisionTerm::getSelfColl(sizeType i)
{
  return _opt._info._infos[i].getSelfCons();
}
GradientInfo& CIOOptimizerSelfCollisionTerm::getCollConfig(sizeType i)
{
  return _opt._info._xss[i+1];
}
//CIOOptimizerSelfCollisionTermPrecomputed
CIOOptimizerSelfCollisionTermPrecomputed::CIOOptimizerSelfCollisionTermPrecomputed(CIOOptimizer& opt):CIOOptimizerSelfCollisionTerm(opt)
{
  _Hss.assign(_opt._horizon,Matd::Zero(0,0));
  _Gss.assign(_opt._horizon,Vec::Zero(0));

  FEMCIOEngine& eng=_opt._sol.getEngine();
  for(sizeType i=0; i<_opt._horizon; i++)
    precompute(i,eng,getCollConfig(i),getSelfColl(i));
}
void CIOOptimizerSelfCollisionTermPrecomputed::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  off+=_cOff[i];
  const Matd& H=_Hss[i];
  const Vec& G=_Gss[i];
  const GradientInfo& info=eng._x;
  sizeType nrS=_opt._sol.nrX()/2,nrU=nrS-6;

  //E
  Vec E=H*info.L().segment(0,H.cols())+G;
  E=E.array().min(0).matrix();
  fvec.segment(off,_Gss[i].size())=E;
  if(fjac) {
    //DE
    Matd DE=H*info.DLDS().block(0,0,H.cols(),nrU);
    for(sizeType c=0; c<E.size(); c++)
      if(E[c] == 0)
        DE.row(c).setZero();
    //assemble
    _opt.addBlockX(*fjac,off,i,DE);
  }
}
void CIOOptimizerSelfCollisionTermPrecomputed::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  const Matd& H=_Hss[i];
  const Vec& G=_Gss[i];
  const GradientInfo& info=eng._x;
  sizeType nrS=_opt._sol.nrX()/2,nrU=nrS-6;

  //E
  Vec E=H*info.L().segment(0,H.cols())+G;
  E=E.array().min(0).matrix();
  //DE
  Matd DE=H*info.DLDS().block(0,0,H.cols(),nrU);
  for(sizeType c=0; c<E.size(); c++)
    if(E[c] == 0)
      DE.row(c).setZero();
  //assemble
  FX+=E.squaredNorm()/2;
  _opt.addBlockX(DFDX,i,DE.transpose()*E);
}
void CIOOptimizerSelfCollisionTermPrecomputed::precompute(sizeType i,const FEMCIOEngine& eng,const GradientInfo& info,vector<CIOSelfColl>& selfColl)
{
  CIOOptimizerSelfCollisionTerm::precompute(i,eng,info,selfColl);
  const FEMBody& body=eng.getBody();

  //precompute Hessian/Gradient
  Vec B0;
  body.getPos(B0,true);
  const Matd& L=eng.getMCalc()._innerMCalc->getL();

  sizeType SC=(sizeType)selfColl.size();
  Matd& H=_Hss[i]=Matd::Zero(SC,L.cols());
  Vec& G=_Gss[i]=Vec::Zero(SC);
  for(sizeType c=0; c<SC; c++) {
    const CIOSelfColl& VI=selfColl[c];
    const FEMCell& C=body.getC(VI._I._id);

    Vec3d nL=info.R().transpose()*VI._n;
    G[c]+=nL.dot(B0.segment<3>(VI._vid*3));
    H.row(c)+=nL.transpose()*L.block(VI._vid*3,0,3,L.cols());
    for(int v=0; v<4; v++)
      if(C._v[v]) {
        const FEMVertex& V=*(C._v[v]);
        G[c]-=nL.dot(B0.segment<3>(V._index*3))*VI._I._coef[v];
        H.row(c)-=nL.transpose()*L.block(V._index*3,0,3,L.cols())*VI._I._coef[v];
      }
    G[c]-=VI._dist;

    //multiply by coefficient
    G[c]*=sqrt(VI._coef);
    H.row(c)*=sqrt(VI._coef);
  }
}
void CIOOptimizerSelfCollisionTermPrecomputed::debugConsistency(sizeType i,const FEMCIOEngine& eng,const vector<CIOSelfColl>& selfColl)
{
  scalarD dirRefSum=0,dirSum=0,errSum=0;
  for(sizeType c=0; c<(sizeType)selfColl.size(); c++) {
    const CIOSelfColl& VI=selfColl[c];
    scalarD dirRef=_Hss[i].row(c)*eng._x.L().segment(0,_Hss[i].cols())+_Gss[i][c];
    scalarD dir=eng.getMCalc().gradVert(eng._x,VI._vid,NULL).dot(VI._n);
    dir-=eng.getMCalc().gradVert(eng._x,VI._I,NULL).dot(VI._n);
    dir-=VI._dist;
    dir*=sqrt(VI._coef);
    //sum
    dirRefSum+=abs(dirRef);
    dirSum+=abs(dir);
    errSum+=abs(dirRef-dir);
  }
  INFOV("Precomputed SelfColl: dirRef=%f dir=%f err=%f",dirRefSum,dirSum,errSum)
}
