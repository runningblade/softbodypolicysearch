#include "QCQP.h"

USE_PRJ_NAMESPACE

//CONE
template <int DIM>
struct CONETraits;
template <>
struct CONETraits<2> {
  typedef Mat2d FMat;
  typedef QCQP<2>::FVec FVec;
  static scalarD CONE(const FVec& F,FVec* df,scalarD mu) {
    if(df)
      *df=FVec(mu*F[0]*2,-F[1]*2);
    return F[0]*F[0]*mu-F[1]*F[1];
  }
  static FVec HESS(scalarD mu) {
    return FVec(2*mu,-2);
  }
  static FMat HESSDiag(scalarD mu) {
    FMat ret;
    ret.setZero();
    ret(0,0)=2*mu;
    ret(1,1)=-2;
    return ret;
  }
};
template <>
struct CONETraits<3> {
  typedef Mat3d FMat;
  typedef QCQP<3>::FVec FVec;
  static scalarD CONE(const FVec& F,FVec* df,scalarD mu) {
    if(df)
      *df=FVec(mu*F[0]*2,-F[1]*2,-F[2]*2);
    return F[0]*F[0]*mu-F[1]*F[1]-F[2]*F[2];
  }
  static FVec HESS(scalarD mu) {
    return FVec(2*mu,-2,-2);
  }
  static FMat HESSDiag(scalarD mu) {
    FMat ret;
    ret.setZero();
    ret(0,0)=2*mu;
    ret(1,1)=-2;
    ret(2,2)=-2;
    return ret;
  }
};
//QCQP: feasibility
template <int DIM>
typename QCQP<DIM>::FVec QCQP<DIM>::ensureFeasibleBlock(FVec F,scalarD mu,scalarD minVio)
{
  scalarD minVal=sqrt((minVio*2+F.template segment<DIM-1>(1).squaredNorm())/mu);
  F[0]=max<scalarD>(F[0],minVal);
  return F;
}
template <int DIM>
typename QCQP<DIM>::Vec QCQP<DIM>::ensureFeasible(Vec F,scalarD mu,scalarD minVio)
{
  sizeType nrC=F.size()/DIM;
  for(sizeType c=0; c<nrC; c++)
    F.segment<DIM>(c*DIM)=ensureFeasibleBlock(F.segment<DIM>(c*DIM),mu,minVio);
  return F;
}
//QCQP: main functionality
template <int DIM>
sizeType QCQP<DIM>::solve(const Matd& H,const Vec& G,Vec& F,scalarD mu,sizeType maxIter,scalar relTol,CB* cb)
{
  //writeProb(H,G,F,mu,"./testProb.dat");
  //initialize
  Matd HIt=H;
  Vec GIt=G,DF,TIKReg;
  Eigen::LDLT<Matd> ldlt;
  F=ensureFeasible(F,mu,_minVio);
  //main loop
  sizeType it=0;
  scalarD kappa=1;
  while(kappa<_kappaMax) {
    while(it<maxIter) {
      //compute direction
      scalarD E=computeGradHess(H,G,F,mu,kappa,&HIt,&GIt,&TIKReg);
      ldlt.compute(HIt);
      if(ldlt.info() != Eigen::Success) {
        HIt.diagonal()+=TIKReg;
        ldlt.compute(HIt);
      }
      DF=-ldlt.solve(GIt);
      //callback
      if(cb)
        (*cb)(F,GIt,E,0,0,kappa,it,0);
      //stopping condition
      if(GIt.cwiseAbs().maxCoeff() < relTol)
        break;
      //perform line search
      if(!lineSearchBruteForce(H,G,GIt,F,DF,mu,kappa))
        break;
      F+=DF;
      it++;
    }
    kappa*=_kappaInc;
  }
  return it;
}
template <int DIM>
scalarD QCQP<DIM>::computeGradHess(const Matd& H,const Matd& G,const Vec& F,scalarD mu,scalarD kappa,Matd* HIt,Vec* GIt,Vec* TIKReg)
{
  FVec df;
  scalarD f,E=F.dot(H*F/2+G)*kappa;
  if(GIt)
    *GIt=(H*F+G)*kappa;
  if(HIt)
    *HIt=H*kappa;
  if(TIKReg)
    TIKReg->setZero(F.size());
  sizeType nrC=G.size()/DIM;
  for(sizeType c=0; c<nrC; c++) {
    //compute gradient & hessian : CONE
    f=CONETraits<DIM>::CONE(F.segment<DIM>(c*DIM),GIt?&df:NULL,mu);
    E-=std::log(f);
    if(GIt)
      GIt->segment<DIM>(c*DIM)-=df/f;
    if(HIt) {
      HIt->block<DIM,DIM>(c*DIM,c*DIM).diagonal()-=CONETraits<DIM>::HESS(mu)/f;
      HIt->block<DIM,DIM>(c*DIM,c*DIM)+=df*df.transpose()/(f*f);
    }
    //add TIKReg
    if(TIKReg)
      TIKReg->segment<DIM>(c*DIM)[0]=CONETraits<DIM>::HESS(mu)[0]/f;
    //compute gradient & hessian : POS
    f=F[c*DIM];
    E-=std::log(f);
    if(GIt)
      GIt->coeffRef(c*DIM)-=1/f;
    if(HIt)
      HIt->coeffRef(c*DIM,c*DIM)+=1/(f*f);
  }
  return E;
}
//QCQP: line search
template <int DIM>
scalarD QCQP<DIM>::computeMaxVio(const Vec& F,scalarD mu)
{
  scalarD maxVio=0;
  for(sizeType c=0,nrC=F.size()/DIM; c<nrC; c++) {
    maxVio=min<scalarD>(maxVio,CONETraits<DIM>::CONE(F.segment<DIM>(c*DIM),NULL,mu));
    maxVio=min<scalarD>(maxVio,F[c*DIM]);
  }
  return maxVio;
}
template <int DIM>
scalarD QCQP<DIM>::computeAlphaThresI(const Vec3d& COEFC,const Vec2d& COEFP)
{
#define THRES 1E-20
  //alpha has solution COEFC[0]+alpha*COEFC[1]+alpha*alpha*COEFC[2]=0
  //alpha has solution COEFP[0]+alpha*COEFP[1]=0
  //ASSERT(COEFC[0] >= 0 && COEFP[0] >= 0)
  if(COEFC[0] <= 0 || COEFP[0] <= 0)
    return 1;
  //COEFC
  if(abs(COEFC[2]) < THRES) {
    if(abs(COEFC[1]) < THRES)
      return 1;
    else {
      scalarD alpha=-COEFC[0]/COEFC[1];
      //INFOV("Linear Solution: %f",COEFC.dot(Vec3d(1,alpha,alpha*alpha)))
      if(alpha < 0)
        return 1;
      else return min<scalarD>(alpha,1);
    }
  } else {
    scalarD delta=COEFC[1]*COEFC[1]-4*COEFC[2]*COEFC[0];
    if(delta <= 0)
      return 1;
    delta=sqrt(delta);
    scalarD alpha[2]= {
      (-COEFC[1]-delta)/(2*COEFC[2]),
      (-COEFC[1]+delta)/(2*COEFC[2])
    };
    //INFOV("Quadratic Solution: %f %f",
    //      COEFC.dot(Vec3d(1,alpha[0],alpha[0]*alpha[0])),
    //      COEFC.dot(Vec3d(1,alpha[1],alpha[1]*alpha[1])))
    if(alpha[0] < 0)
      alpha[0]=1;
    if(alpha[1] < 0)
      alpha[1]=1;
    return min<scalarD>(min<scalarD>(alpha[0],alpha[1]),1);
  }
  //COEFP
  if(abs(COEFP[1]) < THRES)
    return 1;
  else {
    scalarD alpha=-COEFP[0]/COEFP[1];
    //INFOV("Linear Solution: %f",COEFP.dot(Vec3d(1,alpha,alpha*alpha)))
    if(alpha < 0)
      return 1;
    else return min<scalarD>(alpha,1);
  }
#undef THRES
}
template <int DIM>
scalarD QCQP<DIM>::computeAlphaThres(const Vec& F,const Vec& DF,scalarD mu)
{
  FVec df,DFBlk;
  Vec3d COEFC;
  Vec2d COEFP;
  scalarD alpha=1;
  for(sizeType c=0,nrC=F.size()/DIM; c<nrC; c++) {
    DFBlk=DF.segment<DIM>(c*DIM);
    COEFC[0]=CONETraits<DIM>::CONE(F.segment<DIM>(c*DIM),&df,mu);
    COEFC[1]=DFBlk.dot(df);
    COEFC[2]=DFBlk.dot(CONETraits<DIM>::HESSDiag(mu)*DFBlk);
    COEFP[0]=F[c*DIM];
    COEFP[1]=DF[c*DIM];
    alpha=min<scalarD>(alpha,computeAlphaThresI(COEFC,COEFP));
  }
  return alpha;
}
template <int DIM>
bool QCQP<DIM>::lineSearchBruteForce(const Matd& H,const Vec& G,Vec& GIt,Vec& F,Vec& DF,scalarD mu,scalarD kappa)
{
//#define DELTA 1E-9f
  //line search: backtracing
  scalarD alpha=computeAlphaThres(F,DF,mu),DFDAlpha,F0,F1;
  F0=computeGradHess(H,G,F,mu,kappa,NULL,&GIt);
  DFDAlpha=GIt.dot(DF);
  ASSERT(!std::isnan(F0) && !std::isinf(F0))
  if(DFDAlpha >= 0) {
#ifdef DELTA
    scalarD E1=computeGradHess(H,G,F,mu,kappa);
    scalarD E2=computeGradHess(H,G,F+DF*DELTA,mu,kappa);
    INFOV("Energy: %f GradientN: %f GradientA: %f",E1,(E2-E1)/DELTA,DFDAlpha)
    INFOV("Function: %f Function Simplified: %f",E1,F0)
#endif
    return false;
  }
  while(true) {
    F1=computeGradHess(H,G,F+DF*alpha,mu,kappa,NULL,&GIt);
    DFDAlpha=GIt.dot(DF);
    if(std::isnan(F1) || std::isinf(F1) || F1 >= F0)
      alpha*=_alphaDec;
    else {
#ifdef DELTA
      scalarD E1=computeGradHess(H,G,F+DF*(alpha),mu,kappa);
      scalarD E2=computeGradHess(H,G,F+DF*(alpha+DELTA),mu,kappa);
      INFOV("Energy: %f GradientN: %f GradientA: %f",E1,(E2-E1)/DELTA,DFDAlpha)
      INFOV("Function: %f Function Simplified: %f",E1,F1)
#endif
      break;
    }
    if(alpha < _minAlpha) {
      DF.setZero();
      return false;
    }
  }
  DF*=alpha;
  return true;
#undef DELTA
}
//debug
template <int DIM>
void QCQP<DIM>::debugQCQP()
{
#define NR_TEST 10
#define NRP 50
  CB cb;
  scalarD mu=0.75f;
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec G=Vec::Random(NRP*DIM);
    Matd H=Matd::Random(NRP*DIM,NRP*DIM);
    H=(H.transpose()*H+Matd::Identity(NRP*DIM,NRP*DIM)*1E-6f).eval();
    //solve
    Vec F=Vec::Zero(G.size());
    sizeType it=solve(H,G,F,mu*mu,1E4f,1E-6f,&cb);
    INFOV("Converged in %ld iterations!",it)
  }
#undef NRP
#undef NR_TEST
}
template <int DIM>
void QCQP<DIM>::debugGradHess()
{
#define DELTA 1E-9f
#define NR_TEST 100
#define NRP 10
  scalarD mu=0.25f;
  Vec G=Vec::Random(NRP*DIM),GIt,GIt2;
  Matd H=Matd::Random(NRP*DIM,NRP*DIM),HIt,HIt2;
  H=(H.transpose()*H+Matd::Identity(NRP*DIM,NRP*DIM)*1E-4f).eval();
  for(sizeType i=0; i<NR_TEST; i++) {
    scalarD kappa=RandEngine::randR01();
    //create feasible solution
    Vec F=ensureFeasible(Vec::Random(NRP*DIM),mu,1E-3f);
    Vec delta=Vec::Random(NRP*DIM);
    //debug gradient
    scalarD E=computeGradHess(H,G,F,mu,kappa,&HIt,&GIt);
    scalarD E2=computeGradHess(H,G,F+delta*DELTA,mu,kappa);
    INFOV("Gradient: %f Err: %f",GIt.dot(delta),(GIt.dot(delta)-(E2-E)/DELTA))
    //debug hessian
    computeGradHess(H,G,F+delta*DELTA,mu,kappa,&HIt2,&GIt2);
    INFOV("Hessian: %f Err: %f",(HIt*delta).norm(),(HIt*delta-(GIt2-GIt)/DELTA).norm())
    //debug search feasible
    scalarD alpha=computeAlphaThres(F,delta,mu);
    scalarD maxVioProj=computeMaxVio(F+delta*alpha,mu);
    scalarD maxVio=computeMaxVio(F+delta,mu);
    INFOV("alpha: %f maxVio: %f maxVioProj: %f",alpha,maxVio,maxVioProj)
  }
#undef NRP
#undef NR_TEST
#undef DELTA
}
template <int DIM>
void QCQP<DIM>::debugReadProb(const string& path)
{
  Matd H;
  Vec G,F;
  scalarD mu;
  readProb(H,G,F,mu,path);

  CB cb;
  solve(H,G,F,mu,1E4,1E-10,&cb);
}
template <int DIM>
void QCQP<DIM>::writeProb(const Matd& H,const Vec& G,const Vec& F,scalarD mu,const string& path)
{
  boost::filesystem::ofstream os(path,ios::binary);
  writeBinaryData(H,os);
  writeBinaryData(G,os);
  writeBinaryData(F,os);
  writeBinaryData(mu,os);
}
template <int DIM>
void QCQP<DIM>::readProb(Matd& H,Vec& G,Vec& F,scalarD& mu,const string& path)
{
  boost::filesystem::ifstream is(path,ios::binary);
  readBinaryData(H,is);
  readBinaryData(G,is);
  readBinaryData(F,is);
  readBinaryData(mu,is);
}
//parameters
template <int DIM>
scalarD QCQP<DIM>::_alphaDec=0.5;
template <int DIM>
scalarD QCQP<DIM>::_minAlpha=1E-12;
template <int DIM>
scalarD QCQP<DIM>::_minVio=1E-8;
template <int DIM>
scalarD QCQP<DIM>::_kappaMax=1000;
template <int DIM>
scalarD QCQP<DIM>::_kappaInc=2;
//force instantiation
template class QCQP<2>;
template class QCQP<3>;
