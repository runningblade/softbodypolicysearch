#include "FEMEnvironmentDQN.h"
#include "NeuralNetQPolicy.h"
#include "FEMFeature.h"
#include "DDPEnergy.h"
#include "DMPLayer.h"
#include <CMAES/MLUtil.h>
#include <Dynamics/FEMUtils.h>
#include <Dynamics/FEMRotationUtil.h>
#include <Dynamics/FEMCIOEngine.h>

#include <CommonFile/geom/StaticGeom.h>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

USE_PRJ_NAMESPACE

Matd extractLDQN(const Matd& L,int r,int c)
{
  Matd ret=Matd::Zero(L.cols(),L.cols());
  for(sizeType i=0; i<L.rows(); i+=3)
    ret+=L.row(i+r).transpose()*L.row(i+c);
  return ret;
}
Mat3Xd extractLCtrDQN(Matd& L)
{
  //compute center
  Mat3Xd ctr=Mat3Xd::Zero(3,L.cols());
  for(sizeType i=0; i<L.rows(); i+=3)
    ctr+=L.block(i,0,3,L.cols());
  ctr/=(scalarD)(L.rows()/3);
  //remove center
  for(sizeType i=0; i<L.rows(); i+=3)
    L.block(i,0,3,L.cols())-=ctr;
  return ctr;
}
struct FitProfile {
  void print(const string& path) const {
    boost::filesystem::ofstream os(path);
    //w
    scalarD wStd=0,wMax=0;
    os << "wErr: [";
    for(sizeType i=0; i<(sizeType)_w.size(); i++) {
      wMax=max<scalarD>(wMax,(_w[i]-_w0).norm());
      wStd+=(_w[i]-_w0).squaredNorm();
      os << (_w[i]-_w0).norm();
      if(i < (sizeType)_w.size()-1)
        os << ",";
      else os << "]" << endl;
    }
    wStd=sqrt(wStd/(scalar)_w.size());
    os << _w0.norm() << " " << wStd << " " << wMax << endl;
    //t
    scalarD tStd=0,tMax=0;
    os << "tErr: [";
    for(sizeType i=0; i<(sizeType)_t.size(); i++) {
      tMax=max<scalarD>(wMax,(_t[i]-_t0).norm());
      tStd+=(_t[i]-_t0).squaredNorm();
      os << (_t[i]-_t0).norm();
      if(i < (sizeType)_t.size()-1)
        os << ",";
      else os << "]" << endl;
    }
    tStd=sqrt(tStd/(scalar)_t.size());
    os << _t0.norm() << " " << tStd << " " << tMax << endl;
  }
  Mat4d fit() {
    //compute R
    Mat3d R=Mat3d::Zero();
    for(sizeType i=0; i<(sizeType)_w.size(); i++)
      R+=expWGradV<Mat3d,Mat3d,Mat3d>(_w[i],NULL,NULL,NULL,NULL);
    R/=(scalarD)_w.size();
    //set orthogonal
    Eigen::JacobiSVD<Mat3d> svd(R,Eigen::ComputeFullU|Eigen::ComputeFullV);
    R=svd.matrixU()*svd.matrixV().transpose();
    _w0=invExpW(R);
    //compute t
    _t0.setZero();
    for(sizeType i=0; i<(sizeType)_t.size(); i++)
      _t0+=_t[i];
    _t0/=(scalarD)_t.size();
    //reconstruct fit
    Mat4d ret=Mat4d::Identity();
    ret.block<3,3>(0,0)=R;
    ret.block<3,1>(0,3)=_t0;
    return ret;
  }
  //data
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > _w;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > _t;
  Vec3d _w0,_t0;
};
FEMEnvironmentDQN::FEMEnvironmentDQN()
  :FEMEnvironment(typeid(FEMEnvironmentDQN).name()) {}
FEMEnvironmentDQN::FEMEnvironmentDQN(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt)
  :FEMEnvironment(typeid(FEMEnvironmentDQN).name())
{
  reset(sol,DMPMT,pt);
}
void FEMEnvironmentDQN::reset(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt)
{
  //setup solver
  _sol=sol;
  _sol->getBody()._tree.put<bool>("MTCubature",true);
  //setup energy
  boost::shared_ptr<DDPEnergy> energy;
  Vec3d g=getIgnoreDir();
#define DIRECT_SPEED_ENERGY
#ifdef DIRECT_SPEED_ENERGY
  if(g.norm() > 1E-10f)
    energy.reset(new DirectedSpeedEnergy(Vec3d::Zero(),&g,NULL,1));
  else energy.reset(new DirectedSpeedEnergy(Vec3d::Zero(),NULL,NULL,1));
#else
  if(g.norm() > 1E-10f)
    energy.reset(new GoalEnergy(Vec3d::Zero(),&g,NULL,1));
  else energy.reset(new GoalEnergy(Vec3d::Zero(),NULL,NULL,1));
#endif
  _ess.push_back(energy);
  //setup DMP-MT
  _DMPMT=DMPMT;
  INFOV("Selection interval=%ld!",getInterval())
  //load recorded trajectory
  if(pt) {
    //create defoMetric
    Vec B0;
    _sol->getBody().getPos(B0);
    Matd L=concatCol(_sol->getMCalc()._innerMCalc->getL(),B0);
    _LCtr=extractLCtrDQN(L);
    _defoMetricSqrt=matrixSqrt<Matd>(L.transpose()*L);
    //create AToBRTpl
    for(int c=0,k=0; c<3; c++)
      for(int r=0; r<3; r++)
        _AToBRTpl[k++]=extractLDQN(L,r,c);
    //debug AToB
    //debugAToB();
    //read trajectory
    _trajs.resize(_DMPMT.K());
    for(sizeType i=0; i<_DMPMT.K(); i++) {
      const string taskStr="task"+boost::lexical_cast<string>(i);
      boost::optional<const boost::property_tree::ptree&>
      child=pt->get_child_optional(taskStr);
      if(child) {
        _trajs[i]=addTaskDataInner(*child);
      } else {
        ASSERT_MSGV(false,"Cannot find: %s",taskStr.c_str())
      }
    }
    //getPolicy().setEps(1);
    //debugTrajectory("./trajs",1,10);
  }
  //fit trajectory
  //fitTrajectoryRandom();
  fitTrajectory();
  _useFit=true;
}
bool FEMEnvironmentDQN::read(istream& is,IOData* dat)
{
  _DMPMT.read(is,dat);
  readVector(_fit,is);
  //recorded trajectory
  readBinaryData(_LCtr,is);
  readBinaryData(_defoMetricSqrt,is);
  for(sizeType i=0; i<9; i++)
    readBinaryData(_AToBRTpl[i],is);
  sizeType n,m;
  readBinaryData(n,is);
  _trajs.resize(n);
  for(sizeType i=0; i<n; i++) {
    readBinaryData(m,is);
    _trajs[i].resize(m);
    for(sizeType j=0; j<m; j++)
      _trajs[i][j].read(is,dat);
  }
  //parent
  FEMEnvironment::read(is,dat);
  return is.good();
}
bool FEMEnvironmentDQN::write(ostream& os,IOData* dat) const
{
  _DMPMT.write(os,dat);
  writeVector(_fit,os);
  //recorded trajectory
  writeBinaryData(_LCtr,os);
  writeBinaryData(_defoMetricSqrt,os);
  for(sizeType i=0; i<9; i++)
    writeBinaryData(_AToBRTpl[i],os);
  sizeType n=(sizeType)_trajs.size(),m;
  writeBinaryData(n,os);
  for(sizeType i=0; i<n; i++) {
    m=(sizeType)_trajs[i].size();
    writeBinaryData(m,os);
    for(sizeType j=0; j<m; j++)
      _trajs[i][j].write(os,dat);
  }
  //parent
  FEMEnvironment::write(os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> FEMEnvironmentDQN::copy() const
{
  return boost::shared_ptr<Serializable>(new FEMEnvironmentDQN);
}
void FEMEnvironmentDQN::debugFitTrajectory(const string& path,sizeType horizon)
{
  _perFramePath="";
  _perFrameId=-1;
  _useFit=true;
  recreate(path);
  ASSERT_MSG(!_fit.empty(),"You must fit trajectory first!")
  for(sizeType i=0; i<_DMPMT.K(); i++) {
    string pathI=path+"/traj"+boost::lexical_cast<string>(i);
    create(pathI);
    //choose ith controller
    Vec act=Vec::Zero(1);
    act[0]=(scalarD)i;
    //create trajectory
    sampleS0();
    INFOV("Generating task: %ld!",i)
    for(sizeType j=0; j<horizon; j++) {
      transfer(j,act);
      string pathIFrm=pathI+"/frm"+boost::lexical_cast<string>(j)+".vtk";
      writeStateVTK(pathIFrm);
    }
  }
  exit(-1);
}
void FEMEnvironmentDQN::debugDMPTrajectory(const string& path,sizeType horizon)
{
  _useFit=false;
  recreate(path);
  if(_sol->_geom)
    _sol->_geom->writeVTK(path+"/env.vtk");
  INFOV("Selection interval=%ld!",getInterval())
  for(sizeType i=0; i<_DMPMT.K(); i++) {
    _perFramePath=path+"/traj"+boost::lexical_cast<string>(i);
    _perFrameId=0;
    create(_perFramePath);
    //choose ith controller
    Vec act=Vec::Zero(1);
    act[0]=(scalarD)i;
    //create trajectory
    sampleS0();
    INFOV("Generating task: %ld!",i)
    for(sizeType j=0; j<horizon; j++)
      transfer(j,act);
  }
  _perFramePath="";
  _perFrameId=-1;
  exit(-1);
}
void FEMEnvironmentDQN::debugTrajectory(const string& path,sizeType nrTraj,sizeType horizon)
{
  recreate(path);
  for(sizeType i=0; i<nrTraj; i++) {
    _perFramePath=path+"/traj"+boost::lexical_cast<string>(i);
    _perFrameId=0;
    create(_perFramePath);
    //create trajectory
    sampleS0();
    INFOV("Generating task: %ld, Interval: %ld!",i,getInterval())
    for(sizeType j=0; j<horizon; j++)
      transfer(j,sampleA(j,_state));
  }
  _perFramePath="";
  _perFrameId=-1;
}
void FEMEnvironmentDQN::writeStateVTK(const string& path)
{
  bool writePov=_sol->_tree.get<bool>("writePov",false);
  if(writePov) {
    string pathC=path;
    boost::replace_all(pathC,".vtk",".pov");
    writeStatePov(pathC);
  }
  FEMEnvironment::writeStateVTK(path);
  sizeType nrS=_sol->getMCalc().size();
  {
    string pathC=path;
    boost::replace_all(pathC,".vtk","ToTarget.vtk");
    VTKWriter<scalar> os("toTarget",pathC,true);
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    Vec3 center=_state.segment<3>(nrS*2-6).cast<scalar>(),dir;
    dir=_state.segment<3>(nrS*2).cast<scalar>();
    vss.push_back(center);
    vss.push_back(center+dir);
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                   VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),
                   VTKWriter<scalar>::LINE);
  }
  writeLocusVTK(path);
  writeEnvVTK(path);
}
void FEMEnvironmentDQN::writeStatePov(const string& path)
{
  FEMEnvironment::writeStatePov(path);
  writeLocusPov(path);
  writeEnvPov(path);
}
sizeType FEMEnvironmentDQN::nrTraj() const
{
  if(!_targets.empty())
    return (sizeType)_targets.size();
  else return 1;
}
void FEMEnvironmentDQN::fitTrajectory(sizeType horizon,bool profile)
{
  _fit.clear();
  Vec state0=sampleS0();
  sizeType nrS=_sol->getMCalc().size();
  Matd state=Matd::Zero(state0.size(),horizon);
  vector<Mat4d,Eigen::aligned_allocator<Mat4d> > fit;
  for(sizeType i=0; i<_DMPMT.K(); i++) {
    //sample
    FitProfile fitProfile;
    Vec action=Vec::Zero(1);
    action[0]=(scalarD)i;
    for(sizeType h=0; h<horizon; h++) {
      state.col(h)=transfer(i,action);
      INFOV("Fitting %ld task, %ld of %ld!",i,h,horizon)
    }
    //optimize: rotation
    for(sizeType h=1; h<horizon; h++) {
      Mat3d R1=expWGradV<Mat3d,Mat3d,Mat3d>(state.block<3,1>(nrS*2-3,h),NULL,NULL,NULL,NULL);
      Mat3d R0=expWGradV<Mat3d,Mat3d,Mat3d>(state.block<3,1>(nrS*2-3,h-1),NULL,NULL,NULL,NULL);
      fitProfile._w.push_back(invExpW(R0.transpose()*R1));
    }
    //optimize: translation
    for(sizeType h=1; h<horizon; h++) {
      Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(state.block<3,1>(nrS*2-3,h-1),NULL,NULL,NULL,NULL);
      Vec3d T=R.transpose()*(state.block<3,1>(nrS*2-6,h)-state.block<3,1>(nrS*2-6,h-1));
      fitProfile._t.push_back(T);
    }
    fit.push_back(fitProfile.fit());
    INFOV("Fit %ld tasks: off=(%f,%f,%f) rot=(%f,%f,%f)!",i,
          fitProfile._t0[0],fitProfile._t0[1],fitProfile._t0[2],
          fitProfile._w0[0],fitProfile._w0[1],fitProfile._w0[2])
    if(profile)
      fitProfile.print("./fitErrTask"+boost::lexical_cast<string>(i)+".dat");
  }
  _fit=fit;
}
void FEMEnvironmentDQN::fitTrajectoryRandom(sizeType horizon)
{
  //_perFrameId=0;
  //_perFramePath="./fitRandom";
  //recreate(_perFramePath);

  _fit.clear();
  sampleS0();

  Vec action=Vec::Zero(1);
  Vec stateLast=_state,state;
  sizeType nrS=_sol->getMCalc().size();
  vector<FitProfile> fitProfiles(_DMPMT.K());
  for(sizeType i=0;; i++) {
    //sample
    action[0]=(scalarD)RandEngine::randI(0,_DMPMT.K()-1);
    state=transfer(i,action);
    //optimize: rotation
    Mat3d R1=expWGradV<Mat3d,Mat3d,Mat3d>(state.segment<3>(nrS*2-3),NULL,NULL,NULL,NULL);
    Mat3d R0=expWGradV<Mat3d,Mat3d,Mat3d>(stateLast.segment<3>(nrS*2-3),NULL,NULL,NULL,NULL);
    fitProfiles[(sizeType)action[0]]._w.push_back(invExpW(R0.transpose()*R1));
    //optimize: translation
    Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(stateLast.segment<3>(nrS*2-3),NULL,NULL,NULL,NULL);
    Vec3d T=R.transpose()*(state.segment<3>(nrS*2-6)-stateLast.segment<3>(nrS*2-6));
    fitProfiles[(sizeType)action[0]]._t.push_back(T);
    //check fulfill
    bool fulfill=true;
    cout << "Sample " << i << ": ";
    for(sizeType i=0; i<(sizeType)fitProfiles.size(); i++) {
      cout << fitProfiles[i]._w.size() << " ";
      if((sizeType)fitProfiles[i]._w.size() < horizon)
        fulfill=false;
    }
    cout << endl;
    if(fulfill)
      break;
    //swap
    stateLast.swap(state);
  }
  //reconstruct
  for(sizeType i=0; i<(sizeType)fitProfiles.size(); i++) {
    _fit.push_back(fitProfiles[i].fit());
    fitProfiles[i].print("./fitErrTask"+boost::lexical_cast<string>(i)+".dat");
  }
}
void FEMEnvironmentDQN::setUsePeriodicTarget(sizeType s0,sizeType s1)
{
  _targets.clear();
  if(s0 <= 0 || s1 <= 0)
    return;

  Vec3d g=getIgnoreDir();
  bool is2D=g.norm() > 1E-10f;
  if(!is2D)
    g=Vec3d::Random();
  sizeType minCoeff;
  Vec3d gN=g.normalized();
  gN.minCoeff(&minCoeff);
  Vec3d gT1=Vec3d::Unit(minCoeff).cross(gN).normalized();
  Vec3d gT2=gT1.cross(gN);
  //gen targets
  if(is2D) {
    for(sizeType i=0; i<s0; i++) {
      scalarD theta=M_PI*2*(scalarD)i/(scalarD)s0;
      _targets.push_back(gT1*cos(theta)+gT2*sin(theta));
    }
  } else {
    for(sizeType i=0; i<s0; i++)
      for(sizeType j=0; j<s1; j++) {
        scalarD theta=M_PI*2*(0.5f+(scalarD)i)/(scalarD)s0;
        scalarD gamma=M_PI*(0.5f+(scalarD)j)/(scalarD)s1-M_PI/2;
        _targets.push_back((gT1*cos(theta)+gT2*sin(theta))*cos(gamma)+gN*sin(gamma));
      }
  }
  //scalarD length
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalar len=bb.getExtent().norm()*_sol->_tree.get<scalar>("targetDist",10);
  for(sizeType i=0; i<(sizeType)_targets.size(); i++)
    _targets[i]*=len;
  //randomize
  sizeType nrT=(sizeType)_targets.size();
  std::random_shuffle(_targets.begin(),_targets.end());

  //writeVTK
  INFOV("Totalize to %ld periods!",nrT)
  vector<scalar> css;
  for(sizeType i=0; i<nrT; i++)
    css.push_back((scalarD)i);
  VTKWriter<scalarD> os("targets","./targets.vtk",true);
  os.appendPoints(_targets.begin(),_targets.end());
  os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,0),
                 VTKWriter<scalarD>::IteratorIndex<Vec3i>((sizeType)_targets.size(),0,0),
                 VTKWriter<scalarD>::POINT);
  os.appendCustomData("color",css.begin(),css.end());
}
void FEMEnvironmentDQN::setUseFixTarget(bool fix)
{
  _fixTarget=fix;
  if(_fixTarget) {
    FEMEnvironment::sampleS0();
    pickTarget();
  }
}
void FEMEnvironmentDQN::setUseFit(bool useFit)
{
  _useFit=useFit;
}
Vec3d FEMEnvironmentDQN::getIgnoreDir() const
{
  Vec3d ret;
  ret[0]=_sol->_tree.get<scalar>("ignoreDirX",0);
  ret[1]=_sol->_tree.get<scalar>("ignoreDirY",0);
  ret[2]=_sol->_tree.get<scalar>("ignoreDirZ",0);
  return ret;
}
NeuralNetPolicy& FEMEnvironmentDQN::getPolicy()
{
  if(!_policy) {
    Vec tid=Vec::Zero(_DMPMT.K());
    for(sizeType i=0; i<tid.size(); i++)
      tid[i]=(scalarD)i;

    sizeType nrS=_sol->getMCalc().size()*2;
    scalar dt=_sol->_tree.get<scalar>("dt");
    sizeType nrH1=_sol->_tree.get<sizeType>("nrH1",40);
    sizeType nrH2=_sol->_tree.get<sizeType>("nrH2",40);
    sizeType nrH3=_sol->_tree.get<sizeType>("nrH3",0);
    bool hasDefo=_sol->_tree.get<bool>("hasDefo",false);
    bool hasCOM=_sol->_tree.get<bool>("hasCOM",false);
    boost::shared_ptr<FeatureTransformLayer> feature,noFeature(new NoFeatureTransformLayer(nrFeature()));
    if(_sol->_tree.get<bool>("useNeuralQ",false)) {
      if(_sol->_tree.get<bool>("useRSFeature",true))
        feature.reset(new FEMRotRSFeatureTransformLayer(nrS,dt,true,hasDefo || hasCOM,hasCOM));
      boost::shared_ptr<FeatureAugmentedInPlace> featureAug(new FeatureAugmentedInPlace(feature,noFeature));
      _policy.reset(new NeuralNetQPolicy(nrH1,nrH2,nrH3,featureAug->nrInput(),tid,&(_sol->_tree),featureAug));
      scalarD eps=_sol->_tree.get<scalar>("exploration",0.05f);
      getPolicy().setEps(eps);
    } else if(_sol->_tree.get<bool>("useNeuralProbQ",false)) {
      if(_sol->_tree.get<bool>("useRSFeature",true))
        feature.reset(new FEMRotRSFeatureTransformLayer(nrS,dt,true,hasDefo || hasCOM,hasCOM));
      boost::shared_ptr<FeatureAugmentedInPlace> featureAug(new FeatureAugmentedInPlace(feature,noFeature));
      _policy.reset(new NeuralNetProbQPolicy(nrH1,nrH2,nrH3,featureAug->nrInput(),tid,&(_sol->_tree),featureAug));
    } else {
      ASSERT_MSG(false,"Unknown Policy!")
    }
  }
  return dynamic_cast<NeuralNetPolicy&>(*_policy);
}
FEMEnvironmentDQN::Vec FEMEnvironmentDQN::transfer(sizeType i,const Vec& A)
{
  const RigidReducedMCalculator& M=_sol->getMCalc();
  scalarD dt=_sol->_tree.get<scalar>("dt");
  sizeType nrS=M.size();

  if(!_fit.empty() && _useFit) {
    //use fit trajectory
    return transferFit(i,A);
  } else if((sizeType)_trajs.size() == _DMPMT.K()) {
    //use recorded trajectory
    bool first=true;
    const vector<GradientInfo>& traj=_trajs[(sizeType)A[0]];
    sizeType frmId=findFrm(traj,_state);
    Mat4d T=AToBR(traj[frmId],GradientInfo(M,_state.segment(nrS,nrS)));
    for(sizeType i=frmId; i<(sizeType)traj.size();) {
      _state.segment(0,nrS)=applyTR(T,traj[i].x());
      if(i == (sizeType)traj.size()-1) {
        T=AToBR(traj[0],GradientInfo(M,_state.segment(0,nrS)));
        _state.segment(nrS,nrS)=applyTR(T,traj[0].x());
      } else _state.segment(nrS,nrS)=applyTR(T,traj[i+1].x());
      _time+=dt;
      if(!_perFramePath.empty()) {
        updateFeature(_state);
        INFOV("Generating frame: %ld!",_perFrameId)
        writeStateVTK(_perFramePath+"/frm"+boost::lexical_cast<string>(_perFrameId)+".vtk");
        _perFrameId++;
      }
      if(i == (sizeType)traj.size()-1 && first) {
        first=false;
        i=0;
      } else i++;
    }
    updateFeature(_state);
    return _state;
  } else {
    //use simulated trajectory
    //set multitask id
    sizeType K=(sizeType)A[0];
    _DMPMT._tree.put<scalarD>("augment0",K);
    _DMPMT.resetOnline();
    //check number of frames in one cycle
    sizeType nrFrame=getInterval();
    //run simulator for one cycle
    Vec t=Vec::Zero(1);
    _sol->_xN.reset(M,_state.segment(0,nrS));
    _sol->_x.reset(M,_state.segment(nrS,nrS));
    for(sizeType j=0; j<nrFrame; j++) {
      t[0]=(scalarD)j*dt;
      _sol->setControlInput(_DMPMT.foreprop(t));
      _sol->advanceImplicit(dt);
      _time+=dt;
      if(!_perFramePath.empty()) {
        _state=extractStateWithFeature();
        INFOV("Generating frame: %ld!",_perFrameId)
        writeStateVTK(_perFramePath+"/frm"+boost::lexical_cast<string>(_perFrameId)+".vtk");
        _perFrameId++;
      }
    }
    return _state=extractStateWithFeature();
  }
}
FEMEnvironmentDQN::Vec FEMEnvironmentDQN::transferFit(sizeType i,const Vec& A)
{
  const Mat4d& fit=_fit[(sizeType)A[0]];
  scalar dt=_sol->_tree.get<scalar>("dt");
  sizeType nrS=_sol->getMCalc().size();

  Vec ret=_state;
  ret.segment(0,nrS)=_state.segment(nrS,nrS);
  ret.segment(nrS,nrS)=_state.segment(nrS,nrS);
  //rotation
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(ret.segment<3>(nrS*2-3),NULL,NULL,NULL,NULL);
  ret.segment<3>(nrS*2-3)=invExpW(R*fit.block<3,3>(0,0));
  //position
  ret.segment<3>(nrS*2-6)+=R*fit.block<3,1>(0,3);
  //set state
  sizeType nrFrame=getInterval();
  _time+=nrFrame*dt;
  updateFeature(ret);
  _state=ret;
  //writeVTK and return
  if(!_perFramePath.empty()) {
    INFOV("Generating frame: %ld!",_perFrameId)
    string path=_perFramePath+"/frm"+boost::lexical_cast<string>(_perFrameId)+".vtk";
    writeStateVTK(path);
    _perFrameId++;
  }
  return _state;
}
scalarD FEMEnvironmentDQN::getCost(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const
{
  string name="actionCostCoef"+boost::lexical_cast<string>((sizeType)A[0]);
  scalar coefCost=_sol->_tree.get<scalar>(name,1);
  //INFOV("coefCost: %s",name.c_str())
  return FEMEnvironment::getCost(i,state,A,DRDA,DDRDDA)*coefCost;
}
FEMEnvironmentDQN::Vec FEMEnvironmentDQN::sampleS0()
{
  _time=0;
  //sample state
  _state=FEMEnvironment::sampleS0();
  //apply constraints
  sizeType nrS=_sol->getMCalc().size();
  Vec3d g=getIgnoreDir();
  if(g.norm() > 1E-10f) {
    Vec3d gN=g.normalized();
    Mat3d PRJT=gN*gN.transpose();
    Mat3d PRJN=Mat3d::Identity()-PRJT;
    _state.segment<3>(nrS-6)  =(PRJN*_state.segment<3>(nrS-6)  ).eval();
    _state.segment<3>(nrS*2-6)=(PRJN*_state.segment<3>(nrS*2-6)).eval();
    _state.segment<3>(nrS-3)  =(PRJT*_state.segment<3>(nrS-3)  ).eval();
    _state.segment<3>(nrS*2-3)=(PRJT*_state.segment<3>(nrS*2-3)).eval();
    _sol->_xN.reset(_sol->getMCalc(),_state.segment(0,nrS));
    _sol->_x.reset(_sol->getMCalc(),_state.segment(nrS,nrS));
  }
  //setup constraints
  if(g.norm() > 1E-10f) {
    sizeType minCoeff;
    Vec3d gN=g.normalized();
    gN.minCoeff(&minCoeff);
    Vec3d gT1=Vec3d::Unit(minCoeff).cross(gN).normalized();
    Vec3d gT2=gT1.cross(gN);

    Matd CI=Matd::Zero(3,nrS);
    CI.block<1,3>(0,nrS-6)=gN.transpose();
    CI.block<1,3>(1,nrS-3)=gT1.transpose();
    CI.block<1,3>(2,nrS-3)=gT2.transpose();
    _sol->setCI(CI,Vec::Zero(3));
  }
  //sample target
  if(!_targets.empty())
    cycleTarget();
  else if(!_fixTarget)
    pickTarget();
  syncTarget();
  //add feature
  _state=extractStateWithFeature();
  return _state;
}
//for playing recorded trajectory
scalarD FEMEnvironmentDQN::distMetric(const vector<GradientInfo>& traj,sizeType x,sizeType y) const
{
  ASSERT(x+1 < (sizeType)traj.size() && y+1 < (sizeType)traj.size())
  return defoMetric(traj[x],traj[y])+defoMetric(traj[x+1],traj[y+1]);
}
void FEMEnvironmentDQN::addContactInfo(const vector<GradientInfo>& traj,vector<CIOContactInfo>& infos,vector<Vec,Eigen::aligned_allocator<Vec> >& dss)
{
  infos.clear();
  scalar dt=_sol->_tree.get<scalar>("dt");
  if(!_sol->_geom || _sol->_geom->nrG() <= 0)
    return;
  scalarD len=_sol->avgElementLength();
  _sol->_tree.put<scalarD>("collisionExpand",len);
  sizeType nrF=(sizeType)traj.size();
  infos.resize(nrF);
  dss.resize(nrF);
  for(sizeType i=0; i<nrF; i++) {
    _sol->_c=CIOContactInfo();
    FEMCIOEngine::Contacts& cons=_sol->_c.getCons();
    _sol->_xN=_sol->_x=_sol->_xP=traj[i];
    _sol->updateActiveContact(dt);
    _sol->clusterPatch();
    infos[i]=_sol->_c;
    //dss
    const vector<vector<sizeType> >& pss=_sol->_c.getPatches();
    dss[i].setConstant((sizeType)pss.size(),ScalarUtil<scalarD>::scalar_max);
    for(sizeType pid=0; pid<(sizeType)pss.size(); pid++) {
      const vector<sizeType>& patch=pss[pid];
      for(sizeType p=0; p<(sizeType)patch.size(); p++) {
        const CollisionConstraint& cc=cons[patch[p]];
        scalarD dist=(cc.pos().cast<scalarD>()-cc.pos0()).dot(cc.n());
        dss[i][pid]=min<scalarD>(dss[i][pid],abs(dist));
      }
    }
    //dss sum
    scalarD maxD=dss[i].size() == 0 ? 0 : dss[i].maxCoeff();
    INFOV("Detected %ldcontacts/%ldpatches at frame %ld, len=%f!",cons.size(),pss.size(),i,maxD/len)
  }
}
vector<GradientInfo> FEMEnvironmentDQN::addTaskDataInner(const boost::property_tree::ptree& pt)
{
  //fill-in all frame data
  scalar dt=_sol->_tree.get<scalar>("dt");
  const RigidReducedMCalculator& MCalc=_sol->getMCalc();
  sizeType frm=pt.get<sizeType>("frm",-1),nrS=MCalc.size();
  vector<GradientInfo> traj(frm+1);
  for(sizeType i=0; i<frm; i++) {
    Vec x=parseVec<Vec>("x",i,pt);
    traj[i].reset(MCalc,x.segment(0,nrS));
    if(i == frm-1)
      traj[frm].reset(MCalc,x.segment(nrS,nrS));
  }
  //fill-in all contact data
  vector<Vec,Eigen::aligned_allocator<Vec> > dss;
  vector<CIOContactInfo> infos;
  addContactInfo(traj,infos,dss);
  //detecting cyclic data
  //find best periodic animation subsequence
  sizeType minI=-1,minJ=-1;
  scalarD minDist=ScalarUtil<scalarD>::scalar_max;
  scalarD thres=_sol->avgElementLength()*_sol->_tree.get<scalar>("periodicContactThres",1);
  sizeType range=_sol->_tree.get<sizeType>("periodicContactRange",1);
  scalarD periodUnit=_DMPMT.getLayer<DMPLayer>(0)->period(),period=periodUnit;
  while(true) {
    sizeType nrFrame=(sizeType)ceil(period/dt),maxPatch=0;
    if(nrFrame >= frm)
      break;
    if(!infos.empty())
      for(sizeType i=0; i<frm; i++)
        maxPatch=max(maxPatch,dss[i].size());
    for(sizeType i=0; i+nrFrame<frm; i++) {
      //contact aware metric
      scalarD dist=0;
      sizeType otherId=-1;
      if(!infos.empty()) {
        if(dss[i].size() != maxPatch || dss[i].maxCoeff() > thres)
          continue;
        otherId=i+nrFrame;
        scalarD otherDist=ScalarUtil<scalarD>::scalar_max;
        for(sizeType j=i+nrFrame-range; j<=min(frm-1,i+nrFrame+range); j++) {
          if(dss[j].size() == maxPatch && dss[j].maxCoeff() < otherDist) {
            otherDist=dss[j].maxCoeff();
            otherId=j;
          }
        }
        if(otherDist > thres)
          continue;
      } else {
        otherId=i+nrFrame;
      }
      dist=distMetric(traj,i,otherId);
      if(dist < minDist) {
        minDist=dist;
        minJ=otherId;
        minI=i;
      }
    }
    INFOV("minDist at period=%f is: %f!",period,minDist)
    period+=periodUnit;
  }
  ASSERT_MSG(minI >= 0,"Cannot find periodic subsequence!")
  //assign cyclic task
  vector<GradientInfo> cyclicTraj(traj.begin()+minI,traj.begin()+minJ);
  return cyclicTraj;
}
scalarD FEMEnvironmentDQN::defoMetric(const GradientInfo& x,const GradientInfo& y) const
{
  ASSERT(_defoMetricSqrt.cols() == x.L().size())
  return (_defoMetricSqrt*(x.L()-y.L())).norm();
}
sizeType FEMEnvironmentDQN::findFrm(const vector<GradientInfo>& traj,const Vec& y) const
{
  const RigidReducedMCalculator& MCalc=_sol->getMCalc();
  scalarD minDist=ScalarUtil<scalarD>::scalar_max;
  sizeType minI=-1,nrS=MCalc.size();

  const GradientInfo y0(MCalc,y.segment(0,nrS));
  const GradientInfo y1(MCalc,y.segment(nrS,nrS));
  for(sizeType i=0; i<(sizeType)traj.size()-1; i++) {
    scalarD dist=defoMetric(y0,traj[i])+defoMetric(y1,traj[i+1]);
    if(dist < minDist) {
      minDist=dist;
      minI=i;
    }
  }
  if(minI > (sizeType)traj.size()-2)
    minI=0;
  return minI;
}
Mat4d FEMEnvironmentDQN::AToBR(const Vec& a,const Vec& b) const
{
  const RigidReducedMCalculator& M=_sol->getMCalc();
  return AToBR(GradientInfo(M,a),GradientInfo(M,b));
}
Mat4d FEMEnvironmentDQN::AToBR(const GradientInfo& a,const GradientInfo& b) const
{
  Vec3d g=getIgnoreDir();
  Mat4d defoT=Mat4d::Identity();
  if(_sol->_tree.get<bool>("positionOnly",false)) {
    Eigen::Block<Mat4d,3,1> off=defoT.block<3,1>(0,3);
    off=(_LCtr*b.L()+b.x())-(_LCtr*a.L()+a.x());
    return defoT;
  } else if(g.norm() > 1E-10f) {
    g.normalize();
    Eigen::Block<Mat4d,3,1> off=defoT.block<3,1>(0,3);
    off=(_LCtr*b.L()+b.C())-(_LCtr*a.L()+a.C());
    off-=off.dot(g)*g;
    return defoT;
  } else {
    Eigen::JacobiSVD<Mat3d> svd(computeS(a.L(),b.L()),Eigen::ComputeFullU|Eigen::ComputeFullV);
    defoT.block<3,3>(0,0)=svd.matrixV()*svd.matrixU().transpose();
    defoT.block<3,1>(0,3)=_LCtr*b.L()-defoT.block<3,3>(0,0)*(_LCtr*a.L());
    //combine with reduced transformation
    return (computeT(b)*defoT)*computeT(a).inverse();
  }
}
void FEMEnvironmentDQN::applyT(const Mat4d& T,Vec& x) const
{
  for(sizeType i=0; i<x.size(); i+=3) {
    x.segment<3>(i)=(T.block<3,3>(0,0)*x.segment<3>(i)).eval();
    x.segment<3>(i)+=T.block<3,1>(0,3);
  }
}
FEMEnvironmentDQN::Vec FEMEnvironmentDQN::applyTR(const Mat4d& T,const Vec& x) const
{
  Mat4d TX=Mat4d::Identity();
  TX.block<3,3>(0,0)=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment<3>(x.size()-3),NULL,NULL,NULL,NULL);
  TX.block<3,1>(0,3)=x.segment<3>(x.size()-6);

  Vec ret=x;
  Mat4d TT=T*TX;
  ret.segment<3>(ret.size()-6)=TT.block<3,1>(0,3);
  ret.segment<3>(ret.size()-3)=invExpW(TT.block<3,3>(0,0));
  return ret;
}
Mat4d FEMEnvironmentDQN::computeT(const GradientInfo& x) const
{
  Mat4d ret=Mat4d::Identity();
  ret.block<3,3>(0,0)=x.R();
  ret.block<3,1>(0,3)=x.C();
  return ret;
}
Mat3d FEMEnvironmentDQN::computeS(const Vec& A,const Vec& B) const
{
  Mat3d S;
  for(int i=0; i<9; i++)
    S.data()[i]=A.dot(_AToBRTpl[i]*B);
  return S;
}
void FEMEnvironmentDQN::debugAToB()
{
#define DELTA 1E-7f
#define NR_TEST 100
  RigidReducedMCalculator& MCalc=_sol->getMCalcNonConst();
  Vec B0;
  _sol->getBody().getPos(B0);
  Matd L=concatCol(MCalc._innerMCalc->getL(),B0);
  for(sizeType i=0; i<NR_TEST; i++) {
    GradientInfo A(MCalc,Vec::Random(MCalc.size()));
    GradientInfo B(MCalc,Vec::Random(MCalc.size()));
    Vec a=L*A.L();
    Vec b=L*B.L();
    sizeType nrP=a.size()/3;
    Eigen::Map<Mat3Xd> aP(a.data(),3,nrP);
    Eigen::Map<Mat3Xd> bP(b.data(),3,nrP);
    Vec3d aCtr=aP*Vec::Constant(nrP,1/(scalarD)nrP);
    Vec3d bCtr=bP*Vec::Constant(nrP,1/(scalarD)nrP);
    INFOV("Ctr: %f %f, Err: %f %f",aCtr.norm(),bCtr.norm(),(aCtr-_LCtr*A.L()).norm(),(bCtr-_LCtr*B.L()).norm())
    aP-=aCtr*Vec::Ones(nrP).transpose();
    bP-=bCtr*Vec::Ones(nrP).transpose();
    Mat3d S=aP*bP.transpose();
    INFOV("S: %f, Err: %f",S.norm(),(computeS(A.L(),B.L())-S).norm())
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec x=Vec::Random(MCalc.size()),X,Y;
    //transform
    MCalc.setPos(x);
    _sol->getBody().getPos(X);
    //random transform
    Mat4d T=Mat4d::Identity();
    T.block<3,1>(0,3).setRandom();
    T.block<3,3>(0,0)=expWGradV<Mat3d,Mat3d,Mat3d>(Vec3d::Random(),NULL,NULL,NULL,NULL);
    applyT(T,Y=X);
    //compare
    Mat4d T2=AToB(X,Y);
    INFOV("TRigid: %f, Err: %f",T.norm(),(T2-T).norm())
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec A=Vec::Random(MCalc.size())*10,AX;
    Vec B=Vec::Random(MCalc.size())*10,BX;
    //A.segment<6>(A.size()-6).setZero();
    //B.segment<6>(B.size()-6).setZero();
    //transform
    MCalc.setPos(A);
    _sol->getBody().getPos(AX);
    MCalc.setPos(B);
    _sol->getBody().getPos(BX);
    //compare
    Mat4d T=AToB(AX,BX);
    Mat4d T2=AToBR(A,B);
    INFOV("TNonRigid: %f, Err: %f",T.norm(),(T2-T).norm())
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
}
//helper
FEMEnvironmentDQN::FEMEnvironmentDQN(const string& name):FEMEnvironment(name) {}
void FEMEnvironmentDQN::cycleTarget()
{
  sizeType nrS=_sol->getMCalc().size();
  _target=_targets.front();
  _targets.push_back(_target);
  _targets.erase(_targets.begin());
  _target+=_state.segment<3>(nrS-6);
}
void FEMEnvironmentDQN::pickTarget()
{
#define RAND_VEC3 Vec3d(RandEngine::randR(-1,1),RandEngine::randR(-1,1),RandEngine::randR(-1,1))
  INFO("Picking new target!")
  Vec3d ctr=_sol->_x.C(),dir,g=getIgnoreDir();
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalar len=bb.getExtent().norm()*_sol->_tree.get<scalar>("targetDist",10);
  do {
    dir=RAND_VEC3;
    if(g.norm() > 1E-10f)
      dir-=dir.dot(g.normalized())*g.normalized();
  } while(dir.norm() < 1E-5f);
  dir=dir.normalized()*len;
  _target=dir+ctr;
#undef RAND_VEC3
}
void FEMEnvironmentDQN::syncTarget()
{
  if((sizeType)_ess.size() > 0)
    boost::dynamic_pointer_cast<RLEnergy>(_ess[0])->setPos(_target);
}
sizeType FEMEnvironmentDQN::getInterval() const
{
  const DMPLayer* layer=_DMPMT.getLayer<DMPLayer>(0);
  scalarD period=M_PI*2/layer->tau();
  scalarD dt=_sol->_tree.get<scalar>("dt");
  sizeType interval=_sol->_tree.get<sizeType>("DMPSelectionInterval",1);
  return (sizeType)ceil(period/dt)*interval;
}
FEMEnvironmentDQN::Vec FEMEnvironmentDQN::extractStateWithFeature() const
{
  sizeType nrS=_sol->getMCalc().size();
  Vec ret=Vec::Zero(nrS*2+nrFeature());
  ret.segment(0,nrS)=_sol->_xN.x();
  ret.segment(nrS,nrS)=_sol->_x.x();
  updateFeature(ret);
  return ret;
}
void FEMEnvironmentDQN::updateFeature(Vec& ret) const
{
  sizeType nrS=_sol->getMCalc().size();
  ret.segment<3>(nrS*2)=(_target-ret.segment<3>(nrS*2-6)).normalized();
}
sizeType FEMEnvironmentDQN::nrFeature() const
{
  return 3;
}
