#include "DMPLayerMT.h"

USE_PRJ_NAMESPACE

//multitask rhythmic movement primitives Mu
DMPLayerMTMu::DMPLayerMTMu()
  :DMPLayer(typeid(DMPLayerMTMu).name())
{
  boost::property_tree::ptree pt;
  configOnline(pt);
}
DMPLayerMTMu::DMPLayerMTMu(sizeType nrOutput,sizeType nrBasis,sizeType K)
  :DMPLayer(typeid(DMPLayerMTMu).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _h.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;

  _muK.assign(K,_mu);
  _dmuK.assign(K,_mu);
  boost::property_tree::ptree pt;
  configOnline(pt);
}
bool DMPLayerMTMu::read(istream& is,IOData* dat)
{
  DMPLayer::read(is,dat);
  readVector(_muK,is,dat);
  return is.good();
}
bool DMPLayerMTMu::write(ostream& os,IOData* dat) const
{
  DMPLayer::write(os,dat);
  writeVector(_muK,os,dat);
  return os.good();
}
DMPLayerMTMu& DMPLayerMTMu::operator=(const DMPLayerMTMu& other)
{
  DMPLayer::operator=(other);
  _muK=other._muK;
  _dmuK=other._dmuK;
  return *this;
}
boost::shared_ptr<Serializable> DMPLayerMTMu::copy() const
{
  boost::shared_ptr<DMPLayerMTMu> ret(new DMPLayerMTMu());
  *ret=*this;
  return ret;
}
void DMPLayerMTMu::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  if(_setToTask >= 0)
    DMPLayer::foreprop(x,fx,dat);
  else {
    Matd tmp;
    sizeType K=(sizeType)_muK.size(),BLK=x.cols()/K;
    enforceSize(fx,_muK[0].rows(),x.cols());
    if(BLK*K != x.cols())
      return;
    for(sizeType i=0; i<K; i++) {
      const_cast<Matd&>(_mu)=_muK[i];
      DMPLayer::foreprop(x.block(0,i*BLK,x.rows(),BLK),tmp);
      fx.block(0,i*BLK,fx.rows(),BLK)=tmp;
    }
  }
}
void DMPLayerMTMu::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  sizeType K=(sizeType)_muK.size(),BLK=x.cols()/K;
  _dwBias.setZero(_wBias.size());
  _dw.setZero(_w.rows(),_w.cols());
  _dh.setZero(_h.rows(),_h.cols());
  _dtau=0;
  enforceSize(dEdx,x);
  if(BLK*K != x.cols())
    return;
  for(sizeType i=0; i<K; i++) {
    _mu=_muK[i];
    _dmu.setZero(_mu.rows(),_mu.cols());
    backpropInner(x.block(0,i*BLK,x.rows(),BLK),
                  fx.block(0,i*BLK,fx.rows(),BLK),
                  dEdx.block(0,i*BLK,dEdx.rows(),BLK),
                  dEdfx.block(0,i*BLK,dEdfx.rows(),BLK));
    _dmuK[i]=_dmu;
  }
}
void DMPLayerMTMu::augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt)
{
  DMPLayerMTMu layer(_w.rows(),nrDMPBasis,(sizeType)_muK.size());
  layer.configOffline(pt);
  _wBias=concat(_wBias,layer._wBias);
  _w=concatCol(_w,layer._w);
  _h=concatCol(_h,layer._h);
  _mu=concatCol(_mu,layer._mu);
  for(sizeType i=0; i<(sizeType)_muK.size(); i++) {
    _muK[i]=concatCol(_muK[i],layer._muK[i]);
    _dmuK[i]=concatCol(_dmuK[i],layer._dmuK[i]);
  }
}
void DMPLayerMTMu::configOffline(const boost::property_tree::ptree& pt)
{
  DMPLayer::configOffline(pt);
  sizeType K=(sizeType)_muK.size();
  for(sizeType i=0; i<K; i++) {
    _muK[i]=_mu;
    //scalarD initW=pt.get<scalarD>("initDMPPerturb",1E-6f);
    //_muK[i].setRandom();
    //_muK[i]*=initW;
    //for(sizeType j=0; j<_muK[i].cols(); j++) {
    //  scalar coef=((scalar)j+0.5f)/(scalar)_muK[i].cols();
    //  _muK[i].col(j).array()+=coef*M_PI*2;
    //}
  }
}
void DMPLayerMTMu::configOnline(const boost::property_tree::ptree& pt)
{
  sizeType id=(sizeType)pt.get<scalarD>("augment0",-1);
  _setToTask=id;
  if(id >= 0)
    mu()=_muK.at(id);
}
void DMPLayerMTMu::setParam(const Vec& p,sizeType off,bool diff)
{
  if(_wBiasOptimizable) {
    Vec& wBias=diff?_dwBias:_wBias;
    wBias=p.segment(off,wBias.size());
    off+=wBias.size();
  }
  if(_wOptimizable) {
    Matd& w=diff?_dw:_w;
    Eigen::Map<Vec>(w.data(),w.size())=p.segment(off,w.size());
    off+=w.size();
  }
  if(_hOptimizable) {
    Matd& h=diff?_dh:_h;
    Eigen::Map<Vec>(h.data(),h.size())=p.segment(off,h.size());
    off+=h.size();
  }
  if(_muOptimizable) {
    Mss& mu=diff?_dmuK:_muK;
    for(sizeType i=0; i<(sizeType)mu.size(); i++) {
      Eigen::Map<Vec>(mu[i].data(),mu[i].size())=p.segment(off,mu[i].size());
      off+=mu[i].size();
    }
  }
  if(_tauOptimizable) {
    if(diff)
      _dtau=p[off];
    else _tau=p[off];
    off++;
  }
}
void DMPLayerMTMu::param(Vec& p,sizeType off,bool diff) const
{
  if(_wBiasOptimizable) {
    const Vec& wBias=diff?_dwBias:_wBias;
    p.segment(off,wBias.size())+=wBias;
    off+=wBias.size();
  }
  if(_wOptimizable) {
    const Matd& w=diff?_dw:_w;
    p.segment(off,w.size())+=Eigen::Map<const Vec>(w.data(),w.size());
    off+=w.size();
  }
  if(_hOptimizable) {
    const Matd& h=diff?_dh:_h;
    p.segment(off,h.size())+=Eigen::Map<const Vec>(h.data(),h.size());
    off+=h.size();
  }
  if(_muOptimizable) {
    const Mss& mu=diff?_dmuK:_muK;
    for(sizeType i=0; i<(sizeType)mu.size(); i++) {
      p.segment(off,mu[i].size())+=Eigen::Map<const Vec>(mu[i].data(),mu[i].size());
      off+=mu[i].size();
    }
  }
  if(_tauOptimizable) {
    if(diff)
      p[off]+=_dtau;
    else p[off]+=_tau;
    off++;
  }
}
sizeType DMPLayerMTMu::nrParam() const
{
  sizeType ret=0;
  if(_wBiasOptimizable)
    ret+=_wBias.size();
  if(_wOptimizable)
    ret+=_w.size();
  if(_hOptimizable)
    ret+=_h.size();
  if(_muOptimizable)
    ret+=_muK[0].size()*(sizeType)_muK.size();
  if(_tauOptimizable)
    ret++;
  return ret;
}
sizeType DMPLayerMTMu::K() const
{
  return (sizeType)_muK.size();
}
Matd DMPLayerMTMu::getBounds(const boost::property_tree::ptree& pt) const
{
  sizeType off=0;
  Matd ret=Matd::Zero(nrParam(),2);
  if(_wBiasOptimizable) {
    ret.block(off,0,_wBias.size(),1).setConstant(pt.get<scalar>("minBias",0.0f));
    ret.block(off,1,_wBias.size(),1).setConstant(pt.get<scalar>("maxBias",0.0f));
    off+=_wBias.size();
  }
  if(_wOptimizable) {
    ret.block(off,0,_w.size(),1).setConstant(-pt.get<scalar>("maxWeight",100.0f));
    ret.block(off,1,_w.size(),1).setConstant( pt.get<scalar>("maxWeight",100.0f));
    off+=_w.size();
  }
  if(_hOptimizable) {
    ret.block(off,0,_h.size(),1).setConstant(-pt.get<scalar>("maxActivation",5.0f));
    ret.block(off,1,_h.size(),1).setConstant( pt.get<scalar>("maxActivation",5.0f));
    off+=_h.size();
  }
  if(_muOptimizable) {
    ret.block(off,0,_muK[0].size()*(sizeType)_muK.size(),1).setConstant(0);
    ret.block(off,1,_muK[0].size()*(sizeType)_muK.size(),1).setConstant(M_PI*2);
    off+=_muK[0].size()*(sizeType)_muK.size();
  }
  if(_tauOptimizable) {
    ret(off,0)=M_PI*2/pt.get<scalar>("maxPeriod",5.0f);
    ret(off,1)=M_PI*2/pt.get<scalar>("minPeriod",0.5f);
    off++;
  }
  return ret;
}
DMPLayerMTMu::DMPLayerMTMu(const string& name):DMPLayer(name) {}
//multitask non-rhythmic movement primitives Mu
NRDMPLayerMTMu::NRDMPLayerMTMu()
  :NRDMPLayer(typeid(NRDMPLayerMTMu).name())
{
  boost::property_tree::ptree pt;
  configOnline(pt);
}
NRDMPLayerMTMu::NRDMPLayerMTMu(sizeType nrOutput,sizeType nrBasis,sizeType K)
  :NRDMPLayer(typeid(NRDMPLayerMTMu).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _h.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;

  _muK.assign(K,_mu);
  _dmuK.assign(K,_mu);
  boost::property_tree::ptree pt;
  configOnline(pt);
}
bool NRDMPLayerMTMu::read(istream& is,IOData* dat)
{
  NRDMPLayer::read(is,dat);
  readVector(_muK,is,dat);
  return is.good();
}
bool NRDMPLayerMTMu::write(ostream& os,IOData* dat) const
{
  NRDMPLayer::write(os,dat);
  writeVector(_muK,os,dat);
  return os.good();
}
NRDMPLayerMTMu& NRDMPLayerMTMu::operator=(const NRDMPLayerMTMu& other)
{
  NRDMPLayer::operator=(other);
  _muK=other._muK;
  _dmuK=other._dmuK;
  return *this;
}
boost::shared_ptr<Serializable> NRDMPLayerMTMu::copy() const
{
  boost::shared_ptr<NRDMPLayerMTMu> ret(new NRDMPLayerMTMu());
  *ret=*this;
  return ret;
}
void NRDMPLayerMTMu::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  if(_setToTask >= 0)
    NRDMPLayer::foreprop(x,fx,dat);
  else {
    Matd tmp;
    sizeType K=(sizeType)_muK.size(),BLK=x.cols()/K;
    enforceSize(fx,_muK[0].rows(),x.cols());
    if(BLK*K != x.cols())
      return;
    for(sizeType i=0; i<K; i++) {
      const_cast<Matd&>(_mu)=_muK[i];
      NRDMPLayer::foreprop(x.block(0,i*BLK,x.rows(),BLK),tmp);
      fx.block(0,i*BLK,fx.rows(),BLK)=tmp;
    }
  }
}
void NRDMPLayerMTMu::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  sizeType K=(sizeType)_muK.size(),BLK=x.cols()/K;
  _dwBias.setZero(_wBias.size());
  _dw.setZero(_w.rows(),_w.cols());
  _dh.setZero(_h.rows(),_h.cols());
  _dtau=0;
  enforceSize(dEdx,x);
  if(BLK*K != x.cols())
    return;
  for(sizeType i=0; i<K; i++) {
    _mu=_muK[i];
    _dmu.setZero(_mu.rows(),_mu.cols());
    backpropInner(x.block(0,i*BLK,x.rows(),BLK),
                  fx.block(0,i*BLK,fx.rows(),BLK),
                  dEdx.block(0,i*BLK,dEdx.rows(),BLK),
                  dEdfx.block(0,i*BLK,dEdfx.rows(),BLK));
    _dmuK[i]=_dmu;
  }
}
void NRDMPLayerMTMu::augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt)
{
  NRDMPLayerMTMu layer(_w.rows(),nrDMPBasis,(sizeType)_muK.size());
  layer.configOffline(pt);
  _wBias=concat(_wBias,layer._wBias);
  _w=concatCol(_w,layer._w);
  _h=concatCol(_h,layer._h);
  _mu=concatCol(_mu,layer._mu);
  for(sizeType i=0; i<(sizeType)_muK.size(); i++) {
    _muK[i]=concatCol(_muK[i],layer._muK[i]);
    _dmuK[i]=concatCol(_dmuK[i],layer._dmuK[i]);
  }
}
void NRDMPLayerMTMu::configOffline(const boost::property_tree::ptree& pt)
{
  DMPLayer::configOffline(pt);
  sizeType K=(sizeType)_muK.size();
  for(sizeType i=0; i<K; i++) {
    _muK[i]=_mu;
    //scalarD span=pt.get<scalarD>("initTimeSpan",1);
    //scalarD dt=span/(scalarD)_mu.cols();
    //scalarD hRef=sqrt(-log(0.1f))/dt;
    //scalarD initW=pt.get<scalarD>("initDMPPerturb",1E-6f);
    //_muK[i].setRandom();
    //_muK[i]*=initW;
    //for(sizeType j=0; j<_muK[i].cols(); j++) {
    //  scalar coef=((scalar)j+0.5f)/(scalar)_muK[i].cols();
    //  _muK[i].col(j).array()+=span*coef*hRef;
    //}
  }
}
void NRDMPLayerMTMu::configOnline(const boost::property_tree::ptree& pt)
{
  sizeType id=(sizeType)pt.get<scalarD>("augment0",-1);
  _setToTask=id;
  if(id >= 0)
    mu()=_muK.at(id);
}
void NRDMPLayerMTMu::setParam(const Vec& p,sizeType off,bool diff)
{
  if(_wBiasOptimizable) {
    Vec& wBias=diff?_dwBias:_wBias;
    wBias=p.segment(off,wBias.size());
    off+=wBias.size();
  }
  if(_wOptimizable) {
    Matd& w=diff?_dw:_w;
    Eigen::Map<Vec>(w.data(),w.size())=p.segment(off,w.size());
    off+=w.size();
  }
  if(_hOptimizable) {
    Matd& h=diff?_dh:_h;
    Eigen::Map<Vec>(h.data(),h.size())=p.segment(off,h.size());
    off+=h.size();
  }
  if(_muOptimizable) {
    Mss& mu=diff?_dmuK:_muK;
    for(sizeType i=0; i<(sizeType)mu.size(); i++) {
      Eigen::Map<Vec>(mu[i].data(),mu[i].size())=p.segment(off,mu[i].size());
      off+=mu[i].size();
    }
  }
  if(_tauOptimizable) {
    if(diff)
      _dtau=p[off];
    else _tau=p[off];
    off++;
  }
}
void NRDMPLayerMTMu::param(Vec& p,sizeType off,bool diff) const
{
  if(_wBiasOptimizable) {
    const Vec& wBias=diff?_dwBias:_wBias;
    p.segment(off,wBias.size())+=wBias;
    off+=wBias.size();
  }
  if(_wOptimizable) {
    const Matd& w=diff?_dw:_w;
    p.segment(off,w.size())+=Eigen::Map<const Vec>(w.data(),w.size());
    off+=w.size();
  }
  if(_hOptimizable) {
    const Matd& h=diff?_dh:_h;
    p.segment(off,h.size())+=Eigen::Map<const Vec>(h.data(),h.size());
    off+=h.size();
  }
  if(_muOptimizable) {
    const Mss& mu=diff?_dmuK:_muK;
    for(sizeType i=0; i<(sizeType)mu.size(); i++) {
      p.segment(off,mu[i].size())+=Eigen::Map<const Vec>(mu[i].data(),mu[i].size());
      off+=mu[i].size();
    }
  }
  if(_tauOptimizable) {
    if(diff)
      p[off]+=_dtau;
    else p[off]+=_tau;
    off++;
  }
}
sizeType NRDMPLayerMTMu::nrParam() const
{
  sizeType ret=0;
  if(_wBiasOptimizable)
    ret+=_wBias.size();
  if(_wOptimizable)
    ret+=_w.size();
  if(_hOptimizable)
    ret+=_h.size();
  if(_muOptimizable)
    ret+=_muK[0].size()*(sizeType)_muK.size();
  if(_tauOptimizable)
    ret++;
  return ret;
}
sizeType NRDMPLayerMTMu::K() const
{
  return (sizeType)_muK.size();
}
Matd NRDMPLayerMTMu::getBounds(const boost::property_tree::ptree& pt) const
{
  sizeType off=0;
  Matd ret=Matd::Zero(nrParam(),2);
  if(_wBiasOptimizable) {
    ret.block(off,0,_wBias.size(),1).setConstant(pt.get<scalar>("minBias",0.0f));
    ret.block(off,1,_wBias.size(),1).setConstant(pt.get<scalar>("maxBias",0.0f));
    off+=_wBias.size();
  }
  if(_wOptimizable) {
    ret.block(off,0,_w.size(),1).setConstant(-pt.get<scalar>("maxWeight",100.0f));
    ret.block(off,1,_w.size(),1).setConstant( pt.get<scalar>("maxWeight",100.0f));
    off+=_w.size();
  }
  if(_hOptimizable) {
    ret.block(off,0,_h.size(),1).setConstant(-pt.get<scalar>("maxActivation",5.0f));
    ret.block(off,1,_h.size(),1).setConstant( pt.get<scalar>("maxActivation",5.0f));
    off+=_h.size();
  }
  if(_muOptimizable) {
    ret.block(off,0,_muK[0].size()*(sizeType)_muK.size(),1).setConstant(0);
    ret.block(off,1,_muK[0].size()*(sizeType)_muK.size(),1).setConstant(M_PI*2);
    off+=_muK[0].size()*(sizeType)_muK.size();
  }
  if(_tauOptimizable) {
    ret(off,0)=M_PI*2/pt.get<scalar>("maxPeriod",5.0f);
    ret(off,1)=M_PI*2/pt.get<scalar>("minPeriod",0.5f);
    off++;
  }
  return ret;
}
NRDMPLayerMTMu::NRDMPLayerMTMu(const string& name):NRDMPLayer(name) {}
//multitask rhythmic movement primitives wh
DMPLayerMTWH::DMPLayerMTWH()
  :DMPLayer(typeid(DMPLayerMTWH).name())
{
  boost::property_tree::ptree pt;
  configOnline(pt);
}
DMPLayerMTWH::DMPLayerMTWH(sizeType nrOutput,sizeType nrBasis,sizeType K)
  :DMPLayer(typeid(DMPLayerMTWH).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _h.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;

  _wBiasK.assign(K,_wBias);
  _wK.assign(K,_w);
  _hK.assign(K,_h);

  _dwBiasK.assign(K,_wBias);
  _dwK.assign(K,_w);
  _dhK.assign(K,_h);
  boost::property_tree::ptree pt;
  configOnline(pt);
}
bool DMPLayerMTWH::read(istream& is,IOData* dat)
{
  DMPLayer::read(is,dat);
  readVector(_wBiasK,is,dat);
  readVector(_wK,is,dat);
  readVector(_hK,is,dat);
  return is.good();
}
bool DMPLayerMTWH::write(ostream& os,IOData* dat) const
{
  DMPLayer::write(os,dat);
  writeVector(_wBiasK,os,dat);
  writeVector(_wK,os,dat);
  writeVector(_hK,os,dat);
  return os.good();
}
DMPLayerMTWH& DMPLayerMTWH::operator=(const DMPLayerMTWH& other)
{
  DMPLayer::operator=(other);
  _wBiasK=other._wBiasK;
  _wK=other._wK;
  _hK=other._hK;

  _dwBiasK=other._dwBiasK;
  _dwK=other._dwK;
  _dhK=other._dhK;
  return *this;
}
boost::shared_ptr<Serializable> DMPLayerMTWH::copy() const
{
  boost::shared_ptr<DMPLayerMTWH> ret(new DMPLayerMTWH);
  *ret=*this;
  return ret;
}
void DMPLayerMTWH::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  if(_setToTask >= 0)
    DMPLayer::foreprop(x,fx,dat);
  else {
    Matd tmp;
    sizeType K=(sizeType)_wK.size(),BLK=x.cols()/K;
    enforceSize(fx,_wK[0].rows(),x.cols());
    if(BLK*K != x.cols())
      return;
    for(sizeType i=0; i<K; i++) {
      const_cast<Vec&>(_wBias)=_wBiasK[i];
      const_cast<Matd&>(_w)=_wK[i];
      const_cast<Matd&>(_h)=_hK[i];
      DMPLayer::foreprop(x.block(0,i*BLK,x.rows(),BLK),tmp);
      fx.block(0,i*BLK,fx.rows(),BLK)=tmp;
    }
  }
}
void DMPLayerMTWH::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  sizeType K=(sizeType)_wK.size(),BLK=x.cols()/K;
  _dmu.setZero(_mu.rows(),_mu.cols());
  _dtau=0;
  _dwBiasK.resize(K);
  _dwK.resize(K);
  _dhK.resize(K);
  enforceSize(dEdx,x);
  if(BLK*K != x.cols())
    return;
  for(sizeType i=0; i<K; i++) {
    _wBias=_wBiasK[i];
    _dwBias.setZero(_wBias.size());
    _w=_wK[i];
    _dw.setZero(_w.rows(),_w.cols());
    _h=_hK[i];
    _dh.setZero(_h.rows(),_h.cols());
    backpropInner(x.block(0,i*BLK,x.rows(),BLK),
                  fx.block(0,i*BLK,fx.rows(),BLK),
                  dEdx.block(0,i*BLK,dEdx.rows(),BLK),
                  dEdfx.block(0,i*BLK,dEdfx.rows(),BLK));
    _dwBiasK[i]=_dwBias;
    _dwK[i]=_dw;
    _dhK[i]=_dh;
  }
}
void DMPLayerMTWH::augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt)
{
  DMPLayerMTWH layer(_w.rows(),nrDMPBasis,(sizeType)_wK.size());
  layer.configOffline(pt);
  _wBias=concatCol(_wBias,layer._wBias);
  _w=concatCol(_w,layer._w);
  _h=concatCol(_h,layer._h);
  _mu=concatCol(_mu,layer._mu);
  for(sizeType i=0; i<(sizeType)_wK.size(); i++) {
    _wBiasK[i]=concatCol(_wBiasK[i],layer._wBiasK[i]);
    _dwBiasK[i]=concatCol(_dwBiasK[i],layer._dwBiasK[i]);
    _wK[i]=concatCol(_wK[i],layer._wK[i]);
    _dwK[i]=concatCol(_dwK[i],layer._dwK[i]);
    _hK[i]=concatCol(_hK[i],layer._hK[i]);
    _dhK[i]=concatCol(_dhK[i],layer._dhK[i]);
  }
}
void DMPLayerMTWH::configOffline(const boost::property_tree::ptree& pt)
{
  DMPLayer::configOffline(pt);
  for(sizeType i=0; i<(sizeType)_wBiasK.size(); i++) {
    _wBiasK[i]=_wBias;
    _wK[i]=_w;
    _hK[i]=_h;
    //scalarD initW=pt.get<scalarD>("initDMPPerturb",1E-3f);
    //_wBiasK[i].setRandom();
    //_wBiasK[i]*=initW;
    //_wK[i].setRandom();
    //_wK[i]*=initW;
    //_hK[i].setRandom();
    //_hK[i].array()+=1.0f;
    //_hK[i]*=initW;
  }
}
void DMPLayerMTWH::configOnline(const boost::property_tree::ptree& pt)
{
  sizeType id=(sizeType)pt.get<scalarD>("augment0",-1);
  _setToTask=id;
  if(id >= 0) {
    wBias()=_wBiasK.at(id);
    w()=_wK.at(id);
    h()=_hK.at(id);
  }
}
void DMPLayerMTWH::setParam(const Vec& p,sizeType off,bool diff)
{
  if(_wBiasOptimizable) {
    Vss& wBiasK=diff?_dwBiasK:_wBiasK;
    for(sizeType i=0; i<(sizeType)wBiasK.size(); i++) {
      Eigen::Map<Vec>(wBiasK[i].data(),wBiasK[i].size())=p.segment(off,wBiasK[i].size());
      off+=wBiasK[i].size();
    }
  }
  if(_wOptimizable) {
    Mss& wK=diff?_dwK:_wK;
    for(sizeType i=0; i<(sizeType)wK.size(); i++) {
      Eigen::Map<Vec>(wK[i].data(),wK[i].size())=p.segment(off,wK[i].size());
      off+=wK[i].size();
    }
  }
  if(_hOptimizable) {
    Mss& hK=diff?_dhK:_hK;
    for(sizeType i=0; i<(sizeType)hK.size(); i++) {
      Eigen::Map<Vec>(hK[i].data(),hK[i].size())=p.segment(off,hK[i].size());
      off+=hK[i].size();
    }
  }
  if(_muOptimizable) {
    Matd& mu=diff?_dmu:_mu;
    Eigen::Map<Vec>(mu.data(),mu.size())=p.segment(off,mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      _dtau=p[off];
    else _tau=p[off];
    off++;
  }
}
void DMPLayerMTWH::param(Vec& p,sizeType off,bool diff) const
{
  if(_wBiasOptimizable) {
    const Vss& wBiasK=diff?_dwBiasK:_wBiasK;
    for(sizeType i=0; i<(sizeType)wBiasK.size(); i++) {
      p.segment(off,wBiasK[i].size())+=Eigen::Map<const Vec>(wBiasK[i].data(),wBiasK[i].size());
      off+=wBiasK[i].size();
    }
  }
  if(_wOptimizable) {
    const Mss& wK=diff?_dwK:_wK;
    for(sizeType i=0; i<(sizeType)wK.size(); i++) {
      p.segment(off,wK[i].size())+=Eigen::Map<const Vec>(wK[i].data(),wK[i].size());
      off+=wK[i].size();
    }
  }
  if(_hOptimizable) {
    const Mss& hK=diff?_dhK:_hK;
    for(sizeType i=0; i<(sizeType)hK.size(); i++) {
      p.segment(off,hK[i].size())+=Eigen::Map<const Vec>(hK[i].data(),hK[i].size());
      off+=hK[i].size();
    }
  }
  if(_muOptimizable) {
    const Matd& mu=diff?_dmu:_mu;
    p.segment(off,mu.size())+=Eigen::Map<const Vec>(mu.data(),mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      p[off]+=_dtau;
    else p[off]+=_tau;
    off++;
  }
}
sizeType DMPLayerMTWH::nrParam() const
{
  sizeType ret=0;
  if(_wBiasOptimizable)
    ret+=_wBiasK[0].size()*_wBiasK.size();
  if(_wOptimizable)
    ret+=_wK[0].size()*_wK.size();
  if(_hOptimizable)
    ret+=_hK[0].size()*_hK.size();
  if(_muOptimizable)
    ret+=_mu.size();
  if(_tauOptimizable)
    ret++;
  return ret;
}
sizeType DMPLayerMTWH::K() const
{
  return (sizeType)_wK.size();
}
Matd DMPLayerMTWH::getBounds(const boost::property_tree::ptree& pt) const
{
  sizeType off=0;
  Matd ret=Matd::Zero(nrParam(),2);
  if(_wBiasOptimizable) {
    ret.block(off,0,_wBiasK[0].size()*_wBiasK.size(),1).setConstant(pt.get<scalar>("minBias",0.0f));
    ret.block(off,1,_wBiasK[0].size()*_wBiasK.size(),1).setConstant(pt.get<scalar>("maxBias",0.0f));
    off+=_wBiasK[0].size()*_wBiasK.size();
  }
  if(_wOptimizable) {
    ret.block(off,0,_wK[0].size()*_wK.size(),1).setConstant(-pt.get<scalar>("maxWeight",100.0f));
    ret.block(off,1,_wK[0].size()*_wK.size(),1).setConstant( pt.get<scalar>("maxWeight",100.0f));
    off+=_wK[0].size()*_wK.size();
  }
  if(_hOptimizable) {
    ret.block(off,0,_hK[0].size()*_hK.size(),1).setConstant(-pt.get<scalar>("maxActivation",5.0f));
    ret.block(off,1,_hK[0].size()*_hK.size(),1).setConstant( pt.get<scalar>("maxActivation",5.0f));
    off+=_hK[0].size()*_hK.size();
  }
  if(_muOptimizable) {
    ret.block(off,0,_mu.size(),1).setConstant(0);
    ret.block(off,1,_mu.size(),1).setConstant(M_PI*2);
    off+=_mu.size();
  }
  if(_tauOptimizable) {
    ret(off,0)=M_PI*2/pt.get<scalar>("maxPeriod",5.0f);
    ret(off,1)=M_PI*2/pt.get<scalar>("minPeriod",0.5f);
    off++;
  }
  return ret;
}
DMPLayerMTWH::DMPLayerMTWH(const string& name):DMPLayer(name) {}
//multitask non-rhythmic movement primitives wh
NRDMPLayerMTWH::NRDMPLayerMTWH()
  :NRDMPLayer(typeid(NRDMPLayerMTWH).name())
{
  boost::property_tree::ptree pt;
  configOnline(pt);
}
NRDMPLayerMTWH::NRDMPLayerMTWH(sizeType nrOutput,sizeType nrBasis,sizeType K)
  :NRDMPLayer(typeid(NRDMPLayerMTWH).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _h.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;

  _wBiasK.assign(K,_wBias);
  _wK.assign(K,_w);
  _hK.assign(K,_h);

  _dwBiasK.assign(K,_wBias);
  _dwK.assign(K,_w);
  _dhK.assign(K,_h);
  boost::property_tree::ptree pt;
  configOnline(pt);
}
bool NRDMPLayerMTWH::read(istream& is,IOData* dat)
{
  NRDMPLayer::read(is,dat);
  readVector(_wBiasK,is,dat);
  readVector(_wK,is,dat);
  readVector(_hK,is,dat);
  return is.good();
}
bool NRDMPLayerMTWH::write(ostream& os,IOData* dat) const
{
  NRDMPLayer::write(os,dat);
  writeVector(_wBiasK,os,dat);
  writeVector(_wK,os,dat);
  writeVector(_hK,os,dat);
  return os.good();
}
NRDMPLayerMTWH& NRDMPLayerMTWH::operator=(const NRDMPLayerMTWH& other)
{
  NRDMPLayer::operator=(other);
  _wBiasK=other._wBiasK;
  _wK=other._wK;
  _hK=other._hK;

  _dwBiasK=other._dwBiasK;
  _dwK=other._dwK;
  _dhK=other._dhK;
  return *this;
}
boost::shared_ptr<Serializable> NRDMPLayerMTWH::copy() const
{
  boost::shared_ptr<NRDMPLayerMTWH> ret(new NRDMPLayerMTWH);
  *ret=*this;
  return ret;
}
void NRDMPLayerMTWH::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  if(_setToTask >= 0)
    NRDMPLayer::foreprop(x,fx,dat);
  else {
    Matd tmp;
    sizeType K=(sizeType)_wK.size(),BLK=x.cols()/K;
    enforceSize(fx,_wK[0].rows(),x.cols());
    if(BLK*K != x.cols())
      return;
    for(sizeType i=0; i<K; i++) {
      const_cast<Vec&>(_wBias)=_wBiasK[i];
      const_cast<Matd&>(_w)=_wK[i];
      const_cast<Matd&>(_h)=_hK[i];
      NRDMPLayer::foreprop(x.block(0,i*BLK,x.rows(),BLK),tmp);
      fx.block(0,i*BLK,fx.rows(),BLK)=tmp;
    }
  }
}
void NRDMPLayerMTWH::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  sizeType K=(sizeType)_wK.size(),BLK=x.cols()/K;
  _dmu.setZero(_mu.rows(),_mu.cols());
  _dtau=0;
  _dwBiasK.resize(K);
  _dwK.resize(K);
  _dhK.resize(K);
  enforceSize(dEdx,x);
  if(BLK*K != x.cols())
    return;
  for(sizeType i=0; i<K; i++) {
    _wBias=_wBiasK[i];
    _dwBias.setZero(_wBias.size());
    _w=_wK[i];
    _dw.setZero(_w.rows(),_w.cols());
    _h=_hK[i];
    _dh.setZero(_h.rows(),_h.cols());
    backpropInner(x.block(0,i*BLK,x.rows(),BLK),
                  fx.block(0,i*BLK,fx.rows(),BLK),
                  dEdx.block(0,i*BLK,dEdx.rows(),BLK),
                  dEdfx.block(0,i*BLK,dEdfx.rows(),BLK));
    _dwBiasK[i]=_dwBias;
    _dwK[i]=_dw;
    _dhK[i]=_dh;
  }
}
void NRDMPLayerMTWH::augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt)
{
  NRDMPLayerMTWH layer(_w.rows(),nrDMPBasis,(sizeType)_wK.size());
  layer.configOffline(pt);
  _wBias=concatCol(_wBias,layer._wBias);
  _w=concatCol(_w,layer._w);
  _h=concatCol(_h,layer._h);
  _mu=concatCol(_mu,layer._mu);
  for(sizeType i=0; i<(sizeType)_wK.size(); i++) {
    _wBiasK[i]=concatCol(_wBiasK[i],layer._wBiasK[i]);
    _dwBiasK[i]=concatCol(_dwBiasK[i],layer._dwBiasK[i]);
    _wK[i]=concatCol(_wK[i],layer._wK[i]);
    _dwK[i]=concatCol(_dwK[i],layer._dwK[i]);
    _hK[i]=concatCol(_hK[i],layer._hK[i]);
    _dhK[i]=concatCol(_dhK[i],layer._dhK[i]);
  }
}
void NRDMPLayerMTWH::configOffline(const boost::property_tree::ptree& pt)
{
  NRDMPLayer::configOffline(pt);
  for(sizeType i=0; i<(sizeType)_wBiasK.size(); i++) {
    _wBiasK[i]=_wBias;
    _wK[i]=_w;
    _hK[i]=_h;
    //scalarD initW=pt.get<scalarD>("initDMPPerturb",1E-3f);
    //_wBiasK[i].setRandom();
    //_wBiasK[i]*=initW;
    //_wK[i].setRandom();
    //_wK[i]*=initW;
    //_hK[i].setRandom();
    //_hK[i].array()+=1.0f;
    //_hK[i]*=initW;
  }
}
void NRDMPLayerMTWH::configOnline(const boost::property_tree::ptree& pt)
{
  sizeType id=(sizeType)pt.get<scalarD>("augment0",-1);
  _setToTask=id;
  if(id >= 0) {
    wBias()=_wBiasK.at(id);
    w()=_wK.at(id);
    h()=_hK.at(id);
  }
}
void NRDMPLayerMTWH::setParam(const Vec& p,sizeType off,bool diff)
{
  if(_wBiasOptimizable) {
    Vss& wBiasK=diff?_dwBiasK:_wBiasK;
    for(sizeType i=0; i<(sizeType)wBiasK.size(); i++) {
      Eigen::Map<Vec>(wBiasK[i].data(),wBiasK[i].size())=p.segment(off,wBiasK[i].size());
      off+=wBiasK[i].size();
    }
  }
  if(_wOptimizable) {
    Mss& wK=diff?_dwK:_wK;
    for(sizeType i=0; i<(sizeType)wK.size(); i++) {
      Eigen::Map<Vec>(wK[i].data(),wK[i].size())=p.segment(off,wK[i].size());
      off+=wK[i].size();
    }
  }
  if(_hOptimizable) {
    Mss& hK=diff?_dhK:_hK;
    for(sizeType i=0; i<(sizeType)hK.size(); i++) {
      Eigen::Map<Vec>(hK[i].data(),hK[i].size())=p.segment(off,hK[i].size());
      off+=hK[i].size();
    }
  }
  if(_muOptimizable) {
    Matd& mu=diff?_dmu:_mu;
    Eigen::Map<Vec>(mu.data(),mu.size())=p.segment(off,mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      _dtau=p[off];
    else _tau=p[off];
    off++;
  }
}
void NRDMPLayerMTWH::param(Vec& p,sizeType off,bool diff) const
{
  if(_wBiasOptimizable) {
    const Vss& wBiasK=diff?_dwBiasK:_wBiasK;
    for(sizeType i=0; i<(sizeType)wBiasK.size(); i++) {
      p.segment(off,wBiasK[i].size())+=Eigen::Map<const Vec>(wBiasK[i].data(),wBiasK[i].size());
      off+=wBiasK[i].size();
    }
  }
  if(_wOptimizable) {
    const Mss& wK=diff?_dwK:_wK;
    for(sizeType i=0; i<(sizeType)wK.size(); i++) {
      p.segment(off,wK[i].size())+=Eigen::Map<const Vec>(wK[i].data(),wK[i].size());
      off+=wK[i].size();
    }
  }
  if(_hOptimizable) {
    const Mss& hK=diff?_dhK:_hK;
    for(sizeType i=0; i<(sizeType)hK.size(); i++) {
      p.segment(off,hK[i].size())+=Eigen::Map<const Vec>(hK[i].data(),hK[i].size());
      off+=hK[i].size();
    }
  }
  if(_muOptimizable) {
    const Matd& mu=diff?_dmu:_mu;
    p.segment(off,mu.size())+=Eigen::Map<const Vec>(mu.data(),mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      p[off]+=_dtau;
    else p[off]+=_tau;
    off++;
  }
}
sizeType NRDMPLayerMTWH::nrParam() const
{
  sizeType ret=0;
  if(_wBiasOptimizable)
    ret+=_wBiasK[0].size()*_wBiasK.size();
  if(_wOptimizable)
    ret+=_wK[0].size()*_wK.size();
  if(_hOptimizable)
    ret+=_hK[0].size()*_hK.size();
  if(_muOptimizable)
    ret+=_mu.size();
  if(_tauOptimizable)
    ret++;
  return ret;
}
sizeType NRDMPLayerMTWH::K() const
{
  return (sizeType)_wK.size();
}
Matd NRDMPLayerMTWH::getBounds(const boost::property_tree::ptree& pt) const
{
  sizeType off=0;
  Matd ret=Matd::Zero(nrParam(),2);
  if(_wBiasOptimizable) {
    ret.block(off,0,_wBiasK[0].size()*_wBiasK.size(),1).setConstant(pt.get<scalar>("minBias",0.0f));
    ret.block(off,1,_wBiasK[0].size()*_wBiasK.size(),1).setConstant(pt.get<scalar>("maxBias",0.0f));
    off+=_wBiasK[0].size()*_wBiasK.size();
  }
  if(_wOptimizable) {
    ret.block(off,0,_wK[0].size()*_wK.size(),1).setConstant(-pt.get<scalar>("maxWeight",100.0f));
    ret.block(off,1,_wK[0].size()*_wK.size(),1).setConstant( pt.get<scalar>("maxWeight",100.0f));
    off+=_wK[0].size()*_wK.size();
  }
  if(_hOptimizable) {
    ret.block(off,0,_hK[0].size()*_hK.size(),1).setConstant(-pt.get<scalar>("maxActivation",5.0f));
    ret.block(off,1,_hK[0].size()*_hK.size(),1).setConstant( pt.get<scalar>("maxActivation",5.0f));
    off+=_hK[0].size()*_hK.size();
  }
  if(_muOptimizable) {
    ret.block(off,0,_mu.size(),1).setConstant(0);
    ret.block(off,1,_mu.size(),1).setConstant(M_PI*2);
    off+=_mu.size();
  }
  if(_tauOptimizable) {
    ret(off,0)=M_PI*2/pt.get<scalar>("maxPeriod",5.0f);
    ret(off,1)=M_PI*2/pt.get<scalar>("minPeriod",0.5f);
    off++;
  }
  return ret;
}
NRDMPLayerMTWH::NRDMPLayerMTWH(const string& name):NRDMPLayer(name) {}
//multitask rhythmic movement cos-only primitives:
CosDMPLayerMTWH::CosDMPLayerMTWH()
  :CosDMPLayer(typeid(CosDMPLayerMTWH).name())
{
  boost::property_tree::ptree pt;
  configOnline(pt);
}
CosDMPLayerMTWH::CosDMPLayerMTWH(sizeType nrOutput,sizeType nrBasis,sizeType K)
  :CosDMPLayer(typeid(CosDMPLayerMTWH).name())
{
  _wBias.setZero(nrOutput);
  _w.setZero(nrOutput,nrBasis);
  _mu.setZero(nrOutput,nrBasis);
  _tau=0;

  _wBiasK.assign(K,_wBias);
  _wK.assign(K,_w);

  _dwBiasK.assign(K,_wBias);
  _dwK.assign(K,_w);
  boost::property_tree::ptree pt;
  configOnline(pt);
}
bool CosDMPLayerMTWH::read(istream& is,IOData* dat)
{
  DMPLayer::read(is,dat);
  readVector(_wBiasK,is,dat);
  readVector(_wK,is,dat);
  return is.good();
}
bool CosDMPLayerMTWH::write(ostream& os,IOData* dat) const
{
  DMPLayer::write(os,dat);
  writeVector(_wBiasK,os,dat);
  writeVector(_wK,os,dat);
  return os.good();
}
CosDMPLayerMTWH& CosDMPLayerMTWH::operator=(const CosDMPLayerMTWH& other)
{
  CosDMPLayer::operator=(other);
  _wBiasK=other._wBiasK;
  _wK=other._wK;

  _dwBiasK=other._dwBiasK;
  _dwK=other._dwK;
  return *this;
}
boost::shared_ptr<Serializable> CosDMPLayerMTWH::copy() const
{
  boost::shared_ptr<CosDMPLayerMTWH> ret(new CosDMPLayerMTWH);
  *ret=*this;
  return ret;
}
void CosDMPLayerMTWH::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  if(_setToTask >= 0)
    CosDMPLayer::foreprop(x,fx,dat);
  else {
    Matd tmp;
    sizeType K=(sizeType)_wK.size(),BLK=x.cols()/K;
    enforceSize(fx,_wK[0].rows(),x.cols());
    if(BLK*K != x.cols())
      return;
    for(sizeType i=0; i<K; i++) {
      const_cast<Vec&>(_wBias)=_wBiasK[i];
      const_cast<Matd&>(_w)=_wK[i];
      CosDMPLayer::foreprop(x.block(0,i*BLK,x.rows(),BLK),tmp);
      fx.block(0,i*BLK,fx.rows(),BLK)=tmp;
    }
  }
}
void CosDMPLayerMTWH::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  sizeType K=(sizeType)_wK.size(),BLK=x.cols()/K;
  _dmu.setZero(_mu.rows(),_mu.cols());
  _dtau=0;
  _dwBiasK.resize(K);
  _dwK.resize(K);
  enforceSize(dEdx,x);
  if(BLK*K != x.cols())
    return;
  for(sizeType i=0; i<K; i++) {
    _wBias=_wBiasK[i];
    _dwBias.setZero(_wBias.size());
    _w=_wK[i];
    _dw.setZero(_w.rows(),_w.cols());
    backpropInner(x.block(0,i*BLK,x.rows(),BLK),
                  fx.block(0,i*BLK,fx.rows(),BLK),
                  dEdx.block(0,i*BLK,dEdx.rows(),BLK),
                  dEdfx.block(0,i*BLK,dEdfx.rows(),BLK));
    _dwBiasK[i]=_dwBias;
    _dwK[i]=_dw;
  }
}
void CosDMPLayerMTWH::augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt)
{
  CosDMPLayerMTWH layer(_w.rows(),nrDMPBasis,(sizeType)_wK.size());
  layer.configOffline(pt);
  _wBias=concatCol(_wBias,layer._wBias);
  _w=concatCol(_w,layer._w);
  _mu=concatCol(_mu,layer._mu);
  for(sizeType i=0; i<(sizeType)_wK.size(); i++) {
    _wBiasK[i]=concatCol(_wBiasK[i],layer._wBiasK[i]);
    _dwBiasK[i]=concatCol(_dwBiasK[i],layer._dwBiasK[i]);
    _wK[i]=concatCol(_wK[i],layer._wK[i]);
    _dwK[i]=concatCol(_dwK[i],layer._dwK[i]);
  }
}
void CosDMPLayerMTWH::configOffline(const boost::property_tree::ptree& pt)
{
  CosDMPLayer::configOffline(pt);
  for(sizeType i=0; i<(sizeType)_wBiasK.size(); i++) {
    _wBiasK[i]=_wBias;
    _wK[i]=_w;
    //scalarD initW=pt.get<scalarD>("initDMPPerturb",1E-3f);
    //_wBiasK[i].setRandom();
    //_wBiasK[i]*=initW;
    //_wK[i].setRandom();
    //_wK[i]*=initW;
  }
}
void CosDMPLayerMTWH::configOnline(const boost::property_tree::ptree& pt)
{
  sizeType id=(sizeType)pt.get<scalarD>("augment0",-1);
  _setToTask=id;
  if(id >= 0) {
    wBias()=_wBiasK.at(id);
    w()=_wK.at(id);
  }
}
void CosDMPLayerMTWH::setParam(const Vec& p,sizeType off,bool diff)
{
  if(_wBiasOptimizable) {
    Vss& wBiasK=diff?_dwBiasK:_wBiasK;
    for(sizeType i=0; i<(sizeType)wBiasK.size(); i++) {
      Eigen::Map<Vec>(wBiasK[i].data(),wBiasK[i].size())=p.segment(off,wBiasK[i].size());
      off+=wBiasK[i].size();
    }
  }
  if(_wOptimizable) {
    Mss& wK=diff?_dwK:_wK;
    for(sizeType i=0; i<(sizeType)wK.size(); i++) {
      Eigen::Map<Vec>(wK[i].data(),wK[i].size())=p.segment(off,wK[i].size());
      off+=wK[i].size();
    }
  }
  ASSERT(!_hOptimizable)
  if(_muOptimizable) {
    Matd& mu=diff?_dmu:_mu;
    Eigen::Map<Vec>(mu.data(),mu.size())=p.segment(off,mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      _dtau=p[off];
    else _tau=p[off];
    off++;
  }
}
void CosDMPLayerMTWH::param(Vec& p,sizeType off,bool diff) const
{
  if(_wBiasOptimizable) {
    const Vss& wBiasK=diff?_dwBiasK:_wBiasK;
    for(sizeType i=0; i<(sizeType)wBiasK.size(); i++) {
      p.segment(off,wBiasK[i].size())+=Eigen::Map<const Vec>(wBiasK[i].data(),wBiasK[i].size());
      off+=wBiasK[i].size();
    }
  }
  if(_wOptimizable) {
    const Mss& wK=diff?_dwK:_wK;
    for(sizeType i=0; i<(sizeType)wK.size(); i++) {
      p.segment(off,wK[i].size())+=Eigen::Map<const Vec>(wK[i].data(),wK[i].size());
      off+=wK[i].size();
    }
  }
  ASSERT(!_hOptimizable)
  if(_muOptimizable) {
    const Matd& mu=diff?_dmu:_mu;
    p.segment(off,mu.size())+=Eigen::Map<const Vec>(mu.data(),mu.size());
    off+=mu.size();
  }
  if(_tauOptimizable) {
    if(diff)
      p[off]+=_dtau;
    else p[off]+=_tau;
    off++;
  }
}
sizeType CosDMPLayerMTWH::nrParam() const
{
  sizeType ret=0;
  if(_wBiasOptimizable)
    ret+=_wBiasK[0].size()*_wBiasK.size();
  if(_wOptimizable)
    ret+=_wK[0].size()*_wK.size();
  ASSERT(!_hOptimizable)
  if(_muOptimizable)
    ret+=_mu.size();
  if(_tauOptimizable)
    ret++;
  return ret;
}
sizeType CosDMPLayerMTWH::K() const
{
  return (sizeType)_wK.size();
}
Matd CosDMPLayerMTWH::getBounds(const boost::property_tree::ptree& pt) const
{
  sizeType off=0;
  Matd ret=Matd::Zero(nrParam(),2);
  if(_wBiasOptimizable) {
    ret.block(off,0,_wBiasK[0].size()*_wBiasK.size(),1).setConstant(pt.get<scalar>("minBias",0.0f));
    ret.block(off,1,_wBiasK[0].size()*_wBiasK.size(),1).setConstant(pt.get<scalar>("maxBias",0.0f));
    off+=_wBiasK[0].size()*_wBiasK.size();
  }
  if(_wOptimizable) {
    ret.block(off,0,_wK[0].size()*_wK.size(),1).setConstant(-pt.get<scalar>("maxWeight",100.0f));
    ret.block(off,1,_wK[0].size()*_wK.size(),1).setConstant( pt.get<scalar>("maxWeight",100.0f));
    off+=_wK[0].size()*_wK.size();
  }
  ASSERT(!_hOptimizable)
  if(_muOptimizable) {
    ret.block(off,0,_mu.size(),1).setConstant(0);
    ret.block(off,1,_mu.size(),1).setConstant(M_PI*2);
    off+=_mu.size();
  }
  if(_tauOptimizable) {
    ret(off,0)=M_PI*2/pt.get<scalar>("maxPeriod",5.0f);
    ret(off,1)=M_PI*2/pt.get<scalar>("minPeriod",0.5f);
    off++;
  }
  return ret;
}
CosDMPLayerMTWH::CosDMPLayerMTWH(const string& name):CosDMPLayer(name) {}
