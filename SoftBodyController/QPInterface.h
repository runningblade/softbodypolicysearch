#ifndef QP_INTERFACE_H
#define QP_INTERFACE_H

#include <Dynamics/OPTInterface.h>
#include <qpOASES/Types.hpp>

BEGIN_NAMESPACE_QPOASES
class SQProblem;
END_NAMESPACE_QPOASES

PRJ_BEGIN

class QPInterface : public CommonInterface
{
public:
  typedef Eigen::Matrix<scalarD,-1,-1,Eigen::RowMajor> MatdT;
  typedef Eigen::Matrix<double,-1,-1,Eigen::RowMajor> MatdoubleT;
  QPInterface();
  virtual void reset(sizeType sz,bool hasCI);
  virtual void setDense(const Matd& H,const Cold& G);
  virtual void setCI(const Matd& CI,const Cold& CI0,const Coli& type);
  virtual void setCB(const Cold& L,const Cold& U);
  virtual bool solve(Cold& x);
  virtual bool solvePrimal(const Matd& H,const Cold& G,const Matd& CI,const Cold* CI0,Cold& x);
  virtual bool solveDual(const Matd& H,const Cold& G,const Matd& CI,const Cold* CI0,Cold& x);
  virtual bool solveAQPQ(const Eigen::Matrix<double,-1,-1>& H,
                         Eigen::Matrix<double,-1,1> G,
                         const MatdoubleT& CI,
                         const Eigen::Matrix<double,-1,1>* CI0,
                         Eigen::Matrix<double,-1,1>& x);
  virtual bool solveAQPD(const Matd& H,
                         const Cold& G,
                         const MatdT& CI,
                         const Cold* CI0,
                         Cold& x);
  virtual bool solveAQP(const Matd& H,const Cold& G,const MatdT& CI,const Cold* CI0,Cold& x);
  sizeType getTermination() const;
  static void debugPrimalDual(bool cons);
private:
  //data
  boost::shared_ptr<REFER_NAMESPACE_QPOASES SQProblem> _aqp;
  boost::shared_ptr<alglib::minqpstate> _state;
  boost::shared_ptr<alglib::minqpreport> _rep;
};

PRJ_END

#endif
