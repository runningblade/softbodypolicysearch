#ifndef CNN_LAYER_H
#define CNN_LAYER_H

#include "NeuralNet.h"

PRJ_BEGIN

class ConvolutionLayer : public FeatureTransformLayer
{
public:
  typedef vector<ParallelMatVec<Matd> > PMss;
  ConvolutionLayer();
  ConvolutionLayer(const Vec2i& imageSizeIn,sizeType nrFilterIn,const Vec2i& kernelSize,sizeType nrFilterOut,sizeType stride);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual ConvolutionLayer& operator=(const ConvolutionLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL);
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const;
  virtual void configOffline(const boost::property_tree::ptree& pt);
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  Vec2i imageSizeOut() const;
  virtual sizeType nrF() const;
  virtual sizeType nrInput() const;
protected:
  static Eigen::Map<Vec> getMap(Matd& A);
  static Eigen::Map<const Vec> getMap(const Matd& A);
  virtual void forepropInner(const scalarD* x,scalarD* fx) const;
  virtual void backpropInner(const scalarD* x,const scalarD* fx,scalarD* dEdx,const scalarD* dEdfx);
  virtual void weightpropInner(const scalarD* x,const scalarD* fx,const scalarD* dEdx,scalarD* dEdfx) const;
  void stencilForeprop(Eigen::Map<const Matd> mapX,scalarD* mapFx,const Matd& k) const;
  void stencilBackprop(Eigen::Map<const Matd> mapX,Eigen::Map<Matd> mapDEDx,const scalarD* mapDEDfx,Matd& kDiff,const Matd& k) const;
  void stencilWeightprop(Eigen::Map<const Matd> mapX,Eigen::Map<const Matd> mapDEDx,scalarD* mapDEDfx,const Matd& kDiff,const Matd& k) const;
  //data
  Vec _biases;
  vector<Mss> _filters;
  //data diff
  ParallelMatVec<Vec> _biasesDiff;
  vector<PMss> _filtersDiff;
  //param
  Vec2i _imageSizeIn;
  Vec2i _imageSizeOut;
  sizeType _stride;
};
class CompositeLayerInPlace : public FeatureTransformLayer
{
public:
  CompositeLayerInPlace();
  CompositeLayerInPlace(const NeuralNet& net);
  CompositeLayerInPlace(const vector<boost::shared_ptr<NeuralLayer> >& layers);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual CompositeLayerInPlace& operator=(const CompositeLayerInPlace& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL);
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const;
  virtual void configOffline(const boost::property_tree::ptree& pt);
  virtual void configOnline(const boost::property_tree::ptree& pt);
  virtual void setMaskParam(bool maskParam);
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType nrF() const;
  virtual sizeType nrInput() const;
protected:
  void copyMultithread();
  bool _maskParam;
  vector<boost::shared_ptr<NeuralLayer> > _layers;
  //multithreaded cache
  vector<Mss> _foreBlocks,_backBlocks;
};

PRJ_END

#endif
