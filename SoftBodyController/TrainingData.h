#ifndef TRAINING_DATA_H
#define TRAINING_DATA_H

#include <CommonFile/IOBasic.h>

PRJ_BEGIN

struct MultiTrajCallback;
//trajectory sampler
struct TrajectorySampler : public Serializable {
  typedef Cold Vec;
  TrajectorySampler(const string& name);
  virtual void setState(const Vec& state);
  virtual void writeStateVTK(const string& path);
  virtual bool isTerminal(sizeType i,const Vec& state) const;
  virtual Vec transfer(sizeType i,const Vec& A,scalarD& reward);
  virtual Vec transfer(sizeType i,const Vec& A)=0;
  virtual scalarD getReward(sizeType i,const Vec& state,const Vec& A) const=0;
  virtual Vec sampleA(sizeType i,const Vec& S)=0;
  virtual Vec sampleS0()=0;
  //MultiTraj inverse interface
  virtual bool supportMultiTraj() const;
  virtual void startMultiTraj(MultiTrajCallback& cb);
  //MultiTask interface
  virtual void setTrajId(sizeType trajId);
};
struct RandomTrajectorySampler : public TrajectorySampler {
  RandomTrajectorySampler();
  RandomTrajectorySampler(sizeType nrState,sizeType nrAction);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void setState(const Vec& state) override;
  virtual Vec transfer(sizeType i,const Vec& A) override;
  virtual scalarD getReward(sizeType i,const Vec& state,const Vec& A) const override;
  virtual Vec sampleA(sizeType i,const Vec& S) override;
  virtual Vec sampleS0() override;
private:
  sizeType _nrState;
  sizeType _nrAction;
};
//A state in a trajectory
struct Transition;
struct TrajectoryAction;
struct TrajectoryState : public Serializable {
  typedef Cold Vec;
  TrajectoryState();
  virtual ~TrajectoryState();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  void assemble(Matd& states,Matd& actions,sizeType& off) const;
  void assemble(vector<Transition>& trans) const;
  sizeType size() const;
  vector<TrajectoryAction> _actions;
  bool _last;
  Vec _data;
};
//An action in a trajectory
struct TrajectoryAction : public Serializable {
  typedef Cold Vec;
  TrajectoryAction();
  virtual ~TrajectoryAction();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  void assemble(Matd& states,Matd& actions,sizeType& off) const;
  void assemble(vector<Transition>& trans) const;
  sizeType size() const;
  TrajectoryState _next;
  scalarD _reward;
  Vec _data;
};
//A set of recorded state transitions
struct Transition {
  typedef TrajectoryState::Vec Vec;
  Vec _state,_action,_nextState;
  scalarD _reward,_return;
  scalarD _baseline,_nextBaseline;
  scalarD _delta,_advantage;
  bool _last;
};
//A collection of trajectories
struct TrainingData : public Serializable {
  typedef TrajectoryState::Vec Vec;
  TrainingData();
  virtual ~TrainingData();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  const TrajectoryState* traj(sizeType i) const;
  static bool exitByTerminal(const TrajectoryState& s0);
  static scalarD reward(const TrajectoryState& s0,scalarD discountCoef);
  void assemble(Matd& states,Matd& actions,sizeType& off) const;
  void assemble(Matd& states,Matd& actions) const;
  void assemble(vector<Transition>& trans) const;
  TrajectoryState* addTraj();
  void clear();
  sizeType nrTraj() const;
  sizeType nrState() const;
  sizeType nrAction() const;
  sizeType size() const;
  //mpi support
  void randomSampleInner(sizeType minNrTraj,sizeType maxNrTraj,sizeType minTrajSz,sizeType maxTrajSz,
                         TrajectorySampler& sampler,const Vec* action0=NULL,const string& path="",
                         const sizeType reportInterval=0,const sizeType offMPI=0);
  void randomSample(sizeType minNrTraj,sizeType maxNrTraj,sizeType minTrajSz,sizeType maxTrajSz,
                    TrajectorySampler& sampler,const Vec* action0=NULL,const string& path="",
                    const sizeType reportInterval=0,const sizeType offMPI=0);
  static void mpiSlaveRun(TrajectorySampler& sampler);
  static void mpiExit();
  vector<boost::shared_ptr<TrajectoryState> > _trajs;
};

PRJ_END

#endif
