#include "CNNLayer.h"
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//ConvolutionLayer
ConvolutionLayer::ConvolutionLayer()
  :FeatureTransformLayer(typeid(ConvolutionLayer).name()) {}
ConvolutionLayer::ConvolutionLayer(const Vec2i& imageSizeIn,sizeType nrFilterIn,const Vec2i& kernelSize,sizeType nrFilterOut,sizeType stride)
  :FeatureTransformLayer(typeid(ConvolutionLayer).name())
{
  _biases.resize(nrFilterOut);
  _biasesDiff.clear(nrFilterOut);

  _filters.assign(nrFilterOut,Mss(nrFilterIn,Matd(kernelSize[0],kernelSize[1])));
  _filtersDiff.assign(nrFilterOut,PMss(nrFilterIn,ParallelMatVec<Matd>(kernelSize[0],kernelSize[1])));

  _imageSizeIn=imageSizeIn;
  _stride=stride;
  for(sizeType i=0; i<2; i++) {
    _imageSizeOut[i]=(imageSizeIn[i]-kernelSize[i])/stride;
    ASSERT(_imageSizeOut[i] >= 0)
    ASSERT(_imageSizeOut[i]*stride+(kernelSize[i]-1) < imageSizeIn[i])
    ASSERT((_imageSizeOut[i]+1)*stride+(kernelSize[i]-1) >= imageSizeIn[i])
    _imageSizeOut[i]++;
  }
}
bool ConvolutionLayer::read(istream& is,IOData* dat)
{
  readBinaryData(_biases,is);
  _biasesDiff.clear(_biases.size());

  sizeType nr;
  readBinaryData(nr,is);
  _filters.resize(nr);
  for(sizeType i=0; i<nr; i++)
    readVector(_filters[i],is);
  _filtersDiff.assign(_filters.size(),PMss(_filters[0].size(),ParallelMatVec<Matd>(_filters[0][0].rows(),_filters[0][0].cols())));

  readBinaryData(_imageSizeIn,is);
  readBinaryData(_imageSizeOut,is);
  readBinaryData(_stride,is);
  return is.good();
}
bool ConvolutionLayer::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_biases,os);

  sizeType nr=(sizeType)_filters.size();
  writeBinaryData(nr,os);
  for(sizeType i=0; i<nr; i++)
    writeVector(_filters[i],os);

  writeBinaryData(_imageSizeIn,os);
  writeBinaryData(_imageSizeOut,os);
  writeBinaryData(_stride,os);
  return os.good();
}
ConvolutionLayer& ConvolutionLayer::operator=(const ConvolutionLayer& other)
{
  _biases=other._biases;
  _filters=other._filters;
  _imageSizeIn=other._imageSizeIn;
  _imageSizeOut=other._imageSizeOut;
  _stride=other._stride;
  return *this;
}
boost::shared_ptr<Serializable> ConvolutionLayer::copy() const
{
  boost::shared_ptr<ConvolutionLayer> ret(new ConvolutionLayer);
  *ret=*this;
  return ret;
}
void ConvolutionLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  ASSERT(x.rows() == _imageSizeIn.prod()*(sizeType)_filters[0].size())
  enforceSize(fx,_imageSizeOut.prod()*(sizeType)_filters.size(),x.cols());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<x.cols(); i++)
    forepropInner(x.data()+i*x.rows(),
                  fx.data()+i*fx.rows());
}
void ConvolutionLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,_imageSizeIn.prod()*(sizeType)_filters[0].size(),x.cols());
  dEdx.setZero();
  _biasesDiff.clear();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_filtersDiff.size(); i++)
    for(sizeType j=0; j<(sizeType)_filtersDiff[0].size(); j++)
      _filtersDiff[i][j].clear();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<x.cols(); i++)
    backpropInner(x.data()+i*x.rows(),
                  fx.data()+i*fx.rows(),
                  dEdx.data()+i*dEdx.rows(),
                  dEdfx.data()+i*dEdfx.rows());
}
void ConvolutionLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  enforceSize(dEdfx,_imageSizeOut.prod()*(sizeType)_filters.size(),x.cols());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<x.cols(); i++)
    weightpropInner(x.data()+i*x.rows(),
                    fx.data()+i*fx.rows(),
                    dEdx.data()+i*dEdx.rows(),
                    dEdfx.data()+i*dEdfx.rows());
}
void ConvolutionLayer::configOffline(const boost::property_tree::ptree& pt)
{
  scalarD initW=pt.get<scalarD>("initWeightNorm",1E-3f);
  for(sizeType i=0; i<(sizeType)_filters.size(); i++)
    for(sizeType j=0; j<(sizeType)_filters[i].size(); j++) {
      _filters[i][j].setRandom();
      _filters[i][j]*=initW;
    }

  scalarD initB=pt.get<scalarD>("initBiasNorm",1E-3f);
  _biases.setRandom();
  _biases*=initB;
}
void ConvolutionLayer::setParam(const Vec& p,sizeType off,bool diff)
{
  if(diff)
    _biasesDiff.get()=p.segment(off,_biases.size());
  else _biases=p.segment(off,_biases.size());
  off+=_biases.size();

  for(sizeType i=0; i<(sizeType)_filters.size(); i++)
    for(sizeType j=0; j<(sizeType)_filters[i].size(); j++) {
      if(diff)
        getMap(_filtersDiff[i][j].get())=p.segment(off,_filters[i][j].size());
      else getMap(_filters[i][j])=p.segment(off,_filters[i][j].size());
      off+=_filters[i][j].size();
    }
}
void ConvolutionLayer::param(Vec& p,sizeType off,bool diff) const
{
  p.segment(off,_biases.size())=diff ? _biasesDiff.get() : _biases;
  off+=_biases.size();

  for(sizeType i=0; i<(sizeType)_filters.size(); i++)
    for(sizeType j=0; j<(sizeType)_filters[0].size(); j++) {
      p.segment(off,_filters[i][j].size())=getMap(diff ? _filtersDiff[i][j].get() : _filters[i][j]);
      off+=_filters[i][j].size();
    }
}
sizeType ConvolutionLayer::nrParam() const
{
  return _biases.size()+_filters[0][0].size()*(sizeType)_filters[0].size()*(sizeType)_filters.size();
}
Vec2i ConvolutionLayer::imageSizeOut() const
{
  return _imageSizeOut;
}
sizeType ConvolutionLayer::nrF() const
{
  return _imageSizeOut.prod()*_filters.size();
}
sizeType ConvolutionLayer::nrInput() const
{
  return _imageSizeIn.prod()*_filters[0].size();
}
//helper
Eigen::Map<ConvolutionLayer::Vec> ConvolutionLayer::getMap(Matd& A)
{
  return Eigen::Map<Vec>(A.data(),A.size());
}
Eigen::Map<const ConvolutionLayer::Vec> ConvolutionLayer::getMap(const Matd& A)
{
  return Eigen::Map<const Vec>(A.data(),A.size());
}
void ConvolutionLayer::forepropInner(const scalarD* x,scalarD* fx) const
{
  for(sizeType fOut=0; fOut <(sizeType)_filters.size(); fOut++) {
    scalarD* fxDat=fx+_imageSizeOut.prod()*fOut;
    Eigen::Map<Vec>(fxDat,_imageSizeOut.prod()).setConstant(_biases[fOut]);
    for(sizeType fIn=0; fIn < (sizeType)_filters[0].size(); fIn++) {
      Eigen::Map<const Matd> mapX(x+_imageSizeIn.prod()*fIn,_imageSizeIn[0],_imageSizeIn[1]);
      stencilForeprop(mapX,fxDat,_filters[fOut][fIn]);
    }
  }
}
void ConvolutionLayer::backpropInner(const scalarD* x,const scalarD* fx,scalarD* dEdx,const scalarD* dEdfx)
{
  Vec& biasDiff=_biasesDiff.getCurr();
  for(sizeType fOut=0; fOut <(sizeType)_filters.size(); fOut++) {
    const scalarD* dEdfxDat=dEdfx+_imageSizeOut.prod()*fOut;
    biasDiff[fOut]+=Eigen::Map<const Vec>(dEdfxDat,_imageSizeOut.prod()).sum();
    for(sizeType fIn=0; fIn < (sizeType)_filters[0].size(); fIn++) {
      Eigen::Map<const Matd> mapX(x+_imageSizeIn.prod()*fIn,_imageSizeIn[0],_imageSizeIn[1]);
      Eigen::Map<Matd> mapDEDx(dEdx+_imageSizeIn.prod()*fIn,_imageSizeIn[0],_imageSizeIn[1]);
      stencilBackprop(mapX,mapDEDx,dEdfxDat,_filtersDiff[fOut][fIn].getCurr(),_filters[fOut][fIn]);
    }
  }
}
void ConvolutionLayer::weightpropInner(const scalarD* x,const scalarD* fx,const scalarD* dEdx,scalarD* dEdfx) const
{
  for(sizeType fOut=0; fOut <(sizeType)_filters.size(); fOut++) {
    scalarD* dEdfxDat=dEdfx+_imageSizeOut.prod()*fOut;
    Eigen::Map<Vec>(dEdfxDat,_imageSizeOut.prod()).setConstant(_biasesDiff.get()[fOut]);
    for(sizeType fIn=0; fIn < (sizeType)_filters[0].size(); fIn++) {
      Eigen::Map<const Matd> mapX(x+_imageSizeIn.prod()*fIn,_imageSizeIn[0],_imageSizeIn[1]);
      Eigen::Map<const Matd> mapDEDx(dEdx+_imageSizeIn.prod()*fIn,_imageSizeIn[0],_imageSizeIn[1]);
      stencilWeightprop(mapX,mapDEDx,dEdfxDat,_filtersDiff[fOut][fIn].get(),_filters[fOut][fIn]);
    }
  }
}
void ConvolutionLayer::stencilForeprop(Eigen::Map<const Matd> mapX,scalarD* mapFx,const Matd& k) const
{
  for(sizeType c=0,offFx=0; c<_imageSizeOut[1]; c++)
    for(sizeType r=0; r<_imageSizeOut[0]; r++,offFx++)
      mapFx[offFx]+=(k.transpose()*mapX.block(r*_stride,c*_stride,k.rows(),k.cols())).trace();
}
void ConvolutionLayer::stencilBackprop(Eigen::Map<const Matd> mapX,Eigen::Map<Matd> mapDEDx,const scalarD* mapDEDfx,Matd& kDiff,const Matd& k) const
{
  for(sizeType c=0,offFx=0; c<_imageSizeOut[1]; c++)
    for(sizeType r=0; r<_imageSizeOut[0]; r++,offFx++) {
      mapDEDx.block(r*_stride,c*_stride,k.rows(),k.cols())+=k*mapDEDfx[offFx];
      kDiff+=mapX.block(r*_stride,c*_stride,k.rows(),k.cols())*mapDEDfx[offFx];
    }
}
void ConvolutionLayer::stencilWeightprop(Eigen::Map<const Matd> mapX,Eigen::Map<const Matd> mapDEDx,scalarD* mapDEDfx,const Matd& kDiff,const Matd& k) const
{
  for(sizeType c=0,offFx=0; c<_imageSizeOut[1]; c++)
    for(sizeType r=0; r<_imageSizeOut[0]; r++,offFx++)
      mapDEDfx[offFx]+=
        (kDiff.transpose()*mapX.block(r*_stride,c*_stride,k.rows(),k.cols())).trace()+
        (k.transpose()*mapDEDx.block(r*_stride,c*_stride,k.rows(),k.cols())).trace();
}
//CompositeLayerInPlace
CompositeLayerInPlace::CompositeLayerInPlace()
  :FeatureTransformLayer(typeid(CompositeLayerInPlace).name()),_maskParam(false) {}
CompositeLayerInPlace::CompositeLayerInPlace(const NeuralNet& net)
  :FeatureTransformLayer(typeid(CompositeLayerInPlace).name()),_maskParam(false)
{
  //layers
  _layers=net._layers;
  //foreprop cache
  _foreBlocks=net._foreBlocks;
  _foreBlocks.erase(_foreBlocks.begin());
  _foreBlocks.pop_back();
  //backprop cache
  _backBlocks=net._backBlocks;
  _backBlocks.erase(_backBlocks.begin());
  _backBlocks.pop_back();
}
CompositeLayerInPlace::CompositeLayerInPlace(const vector<boost::shared_ptr<NeuralLayer> >& layers)
  :FeatureTransformLayer(typeid(CompositeLayerInPlace).name()),_maskParam(false),_layers(layers) {}
bool CompositeLayerInPlace::read(istream& is,IOData* dat)
{
  readVector(_layers,is,dat);
  copyMultithread();
  return is.good();
}
bool CompositeLayerInPlace::write(ostream& os,IOData* dat) const
{
  writeVector(_layers,os,dat);
  return os.good();
}
CompositeLayerInPlace& CompositeLayerInPlace::operator=(const CompositeLayerInPlace& other)
{
  _layers.resize(other._layers.size());
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    _layers[i]=boost::dynamic_pointer_cast<NeuralLayer>(other._layers[i]->copy());
  return *this;
}
boost::shared_ptr<Serializable> CompositeLayerInPlace::copy() const
{
  boost::shared_ptr<CompositeLayerInPlace> ret(new CompositeLayerInPlace);
  *ret=*this;
  return ret;
}
void CompositeLayerInPlace::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  Mss& ff=const_cast<Mss&>(_foreBlocks[omp_get_thread_num()]);
  sizeType nrL=(sizeType)_layers.size();
  for(sizeType i=0; i<nrL; i++)
    _layers[i]->foreprop(i == 0 ? x : ff[i-1],
                         i == nrL-1 ? fx : ff[i],dat);
}
void CompositeLayerInPlace::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  Mss& ff=const_cast<Mss&>(_foreBlocks[omp_get_thread_num()]);
  Mss& bb=const_cast<Mss&>(_backBlocks[omp_get_thread_num()]);
  sizeType nrL=(sizeType)_layers.size();
  for(sizeType i=nrL-1; i>=0; i--)
    _layers[i]->backprop(i == 0 ? x : ff[i-1],
                         i == nrL-1 ? fx : ff[i],
                         i == 0 ? dEdx : bb[i-1],
                         i == nrL-1 ? dEdfx : bb[i],dat);
}
void CompositeLayerInPlace::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  Mss& ff=const_cast<Mss&>(_foreBlocks[omp_get_thread_num()]);
  Mss& bb=const_cast<Mss&>(_backBlocks[omp_get_thread_num()]);
  sizeType nrL=(sizeType)_layers.size();
  for(sizeType i=0; i<nrL; i++)
    _layers[i]->weightprop(i == 0 ? x : ff[i-1],
                           i == nrL-1 ? fx : ff[i],
                           i == 0 ? dEdx : bb[i-1],
                           i == nrL-1 ? dEdfx : bb[i],dat);
}
void CompositeLayerInPlace::configOffline(const boost::property_tree::ptree& pt)
{
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    _layers[i]->configOffline(pt);
}
void CompositeLayerInPlace::configOnline(const boost::property_tree::ptree& pt)
{
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    _layers[i]->configOnline(pt);
}
void CompositeLayerInPlace::setMaskParam(bool maskParam)
{
  _maskParam=maskParam;
}
void CompositeLayerInPlace::setParam(const Vec& p,sizeType off,bool diff)
{
  if(_maskParam)
    return;
  for(sizeType i=0; i<(sizeType)_layers.size(); i++) {
    _layers[i]->setParam(p,off,diff);
    off+=_layers[i]->nrParam();
  }
}
void CompositeLayerInPlace::param(Vec& p,sizeType off,bool diff) const
{
  if(_maskParam)
    return;
  for(sizeType i=0; i<(sizeType)_layers.size(); i++) {
    _layers[i]->param(p,off,diff);
    off+=_layers[i]->nrParam();
  }
}
sizeType CompositeLayerInPlace::nrParam() const
{
  if(_maskParam)
    return 0;
  sizeType ret=0;
  for(sizeType i=0; i<(sizeType)_layers.size(); i++)
    ret+=_layers[i]->nrParam();
  return ret;
}
sizeType CompositeLayerInPlace::nrF() const
{
  boost::shared_ptr<FeatureTransformLayer> feat=
    boost::dynamic_pointer_cast<FeatureTransformLayer>(_layers.back());
  ASSERT_MSG(feat,"Last composite layer is not FeatureTransformLayer!")
  return feat->nrF();
}
sizeType CompositeLayerInPlace::nrInput() const
{
  boost::shared_ptr<FeatureTransformLayer> feat=
    boost::dynamic_pointer_cast<FeatureTransformLayer>(_layers.front());
  ASSERT_MSG(feat,"First composite layer is not FeatureTransformLayer!")
  return feat->nrInput();
}
void CompositeLayerInPlace::copyMultithread()
{
  _foreBlocks.assign(omp_get_num_procs(),Mss(_layers.size()-1));
  _backBlocks.assign(omp_get_num_procs(),Mss(_layers.size()-1));
}
