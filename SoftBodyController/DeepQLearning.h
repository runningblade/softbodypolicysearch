#ifndef DEEP_Q_LEARNING_H
#define DEEP_Q_LEARNING_H

#include "Environment.h"

PRJ_BEGIN

//this implement the DeepQLearning algorithm
class QLearning
{
public:
  typedef Environment::Vec Vec;
  QLearning(Environment& env);
  virtual ~QLearning();
  virtual void learn();
  boost::property_tree::ptree _tree;
protected:
  scalarD updateExploration(sizeType tt);
  void updateTargetParameter(sizeType tt);
  void updateParameterWholeMemoryNewton();
  void updateParameterWholeMemoryRMSProp();
  void updateParameterWholeMemoryLBFGS();
  vector<Transition> _memoryBank;
  Environment& _env;
  Vec _squaredMoment;
};

PRJ_END

#endif
