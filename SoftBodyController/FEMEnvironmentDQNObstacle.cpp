#include "FEMEnvironmentDQNObstacle.h"
#include "NeuralNetQPolicy.h"
#include "FEMFeature.h"
#include "DDPEnergy.h"
#include "CNNLayer.h"
#include <Dynamics/FEMRotationUtil.h>
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/FEMUtils.h>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

USE_PRJ_NAMESPACE

//CNNObstacleFeature
CNNObstacleFeature::CNNObstacleFeature()
  :Serializable(typeid(CNNObstacleFeature).name()) {}
CNNObstacleFeature::CNNObstacleFeature(FEMEnvironmentDQN& env,Vec3d f,const boost::property_tree::ptree* pt)
  :Serializable(typeid(CNNObstacleFeature).name())
{
  //radar distance
  FEMCIOEngineImplicit& eng=env.sol();
  BBox<scalar> bb=eng.getBody().getBB(true);
  scalar radarDist=bb.getExtent().norm()*eng._tree.get<scalar>("radarDistanceCoef",5);
  //radar direction
  _dir.clear();
  Vec3d g=env.getIgnoreDir(),gT1=g,gT2=g;
  f.normalize();
  ASSERT_MSG(abs(f.norm()-1) < 1E-6f,"FrontDir not defined!")
  //sample
  scalar rad=eng._tree.get<scalar>("radarAngleRes",5)*M_PI/180.0f;
  _res[0]=eng._tree.get<sizeType>("radarRes",32)/2*2+1;
  _res[1]=(g.norm() > 1E-10f ? 1 : _res[0])/2*2+1;
  if(g.norm() < 1E-10f) {
    sizeType minCoeff;
    f.minCoeff(&minCoeff);
    gT1=Vec3d::Unit(minCoeff).cross(f).normalized();
    gT2=gT1.cross(f);
  }
  for(sizeType x=-_res[0]/2; x<=_res[0]/2; x++)
    for(sizeType y=-_res[1]/2; y<=_res[1]/2; y++) {
      Mat3d R=Mat3d::Identity();
      if(g.norm() < 1E-10f) {
        R=makeRotation<scalarD>(gT1*(scalarD)x*rad);
        R=makeRotation<scalarD>(gT2*(scalarD)y*rad)*R;
      } else R=makeRotation<scalarD>(g.normalized()*(scalarD)x*rad);
      _dir.push_back(R*f);
    }
  //create net
  sizeType K1=eng._tree.get<sizeType>("szCNNKernel1",8);
  sizeType K2=eng._tree.get<sizeType>("szCNNKernel2",4);
  sizeType K3=eng._tree.get<sizeType>("szCNNKernel3",0);
  sizeType nrF1=eng._tree.get<sizeType>("nrCNNFilter1",16);
  sizeType nrF2=eng._tree.get<sizeType>("nrCNNFilter2",32);
  sizeType nrF3=eng._tree.get<sizeType>("nrCNNFilter3",0);
  sizeType nrCNNFeat=eng._tree.get<sizeType>("nrCNNFeature",32);
  vector<Vec4i,Eigen::aligned_allocator<Vec4i> > imgs;
  imgs.push_back(Vec4i(_res[0],_res[1],1,-1));
  if(nrF1 > 0)
    imgs.push_back(Vec4i(K1,_res[1] == 1 ? 1 : K1,nrF1,1));
  if(nrF2 > 0)
    imgs.push_back(Vec4i(K2,_res[1] == 1 ? 1 : K2,nrF2,1));
  if(nrF3 > 0)
    imgs.push_back(Vec4i(K3,_res[1] == 1 ? 1 : K3,nrF3,1));
  NeuralNet net(imgs,nrCNNFeat);
  boost::shared_ptr<CompositeLayerInPlace> composite=net.toCompositeLayer();
  _net.reset(new NeuralNet(composite,nrFeature(env),pt));
  _net->_tree.put<scalar>("maxRadarDistance",radarDist);
}
bool CNNObstacleFeature::read(const string& path)
{
  boost::shared_ptr<IOData> dat=getIOData();
  boost::filesystem::ifstream is(path,ios::binary);
  registerType<NeuralNet>(dat.get());
  return read(is,dat.get());
}
bool CNNObstacleFeature::write(const string& path) const
{
  boost::shared_ptr<IOData> dat=getIOData();
  boost::filesystem::ofstream os(path,ios::binary);
  return write(os,dat.get());
}
bool CNNObstacleFeature::read(istream& is,IOData* dat)
{
  readVector(_dir,is);
  readBinaryData(_net,is,dat);
  readBinaryData(_res,is);
  return is.good();
}
bool CNNObstacleFeature::write(ostream& os,IOData* dat) const
{
  writeVector(_dir,os);
  writeBinaryData(_net,os,dat);
  writeBinaryData(_res,os);
  return os.good();
}
boost::shared_ptr<Serializable> CNNObstacleFeature::copy() const
{
  return boost::shared_ptr<Serializable>(new CNNObstacleFeature);
}
const vector<Vec3d,Eigen::aligned_allocator<Vec3d> >& CNNObstacleFeature::getDir() const
{
  return _dir;
}
void CNNObstacleFeature::writeSensingVTK(const string& path,FEMEnvironmentDQNObstacle& env,const Vec& state,const Vec& sensing) const
{
  sizeType nrS=env.sol().getMCalc().size();
  VTKWriter<scalar> os("sensing",path,true);
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  Vec3 center=state.segment<3>(nrS*2-6).cast<scalar>();
  Vec3d rot=state.segment<3>(nrS*2-3);
  Mat3 R=expWGradV<Mat3d,Mat3d,Mat3d>(rot,NULL,NULL,NULL,NULL).cast<scalar>();
  for(sizeType i=0; i<sensing.size(); i++) {
    vss.push_back(center);
    vss.push_back(center+R*(_dir[i]*sensing[i]).cast<scalar>());
  }
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),
                 VTKWriter<scalar>::LINE);
}
void CNNObstacleFeature::writeVTK(const string& path,FEMEnvironmentDQNObstacle& env,const Vec& state,const Mat4Xd& obstacle,const Vec& sensing,const Vec& feature) const
{
  env.FEMEnvironment::writeStateVTK(path);
  {
    string pathC=path;
    boost::replace_all(pathC,".vtk","ObstacleGeom.vtk");
    env.writeObstacleVTKOrPov(pathC,obstacle,false);
  }
  {
    string pathC=path;
    boost::replace_all(pathC,".vtk","CNNFeature.vtk");
    writeSensingVTK(pathC,env,state,sensing);
  }
  {
    string pathC=path;
    boost::replace_all(pathC,".vtk","ToObstacle.vtk");
    VTKWriter<scalar> os("feature",pathC,true);
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    sizeType nrNN=feature.size()/4,nrS=env.sol().getMCalc().size();
    Vec3 center=state.segment<3>(nrS*2-6).cast<scalar>(),dir;
    for(sizeType i=0; i<nrNN; i++) {
      dir=feature.segment<3>(i*4).cast<scalar>();
      vss.push_back(center);
      vss.push_back(center+dir*feature[i*4+3]);
    }
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                   VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),
                   VTKWriter<scalar>::LINE);
  }
}
void CNNObstacleFeature::trainCNNFeature(FEMEnvironmentDQNObstacle& env,sizeType nrSample,bool debug,bool debugFeature)
{
  Vec state=env.sampleS0();
  const FEMCIOEngineImplicit& eng=env.sol();
  scalar maxDist=_net->_tree.get<scalar>("maxRadarDistance");
  sizeType nrS=eng.getMCalc().size();
  sizeType nrD=(sizeType)_dir.size();
  Vec dist=Vec::Constant(nrD,maxDist);
  //projection
  Mat3d PROJ=Mat3d::Identity();
  Vec3d g=env.getIgnoreDir();
  if(g.norm() > 1E-10f)
    PROJ=Mat3d::Identity()-g.normalized()*g.normalized().transpose();
  //create training data
  sizeType nrNN=eng._tree.get<sizeType>("nrNearestNeighbor",4);
  sizeType nrEff=env.nrEffectiveObstacles();
  Mat4Xd obstacle=Mat4Xd::Zero(4,nrEff);
  Matd states=Matd::Zero(nrD,nrSample);
  Matd actions=Matd::Zero(nrNN*4,nrSample);
  if(debug || debugFeature)
    recreate("./debugCNNFeature");
  for(sizeType i=0; i<nrSample;) {
    for(sizeType o=0; o<nrEff; o++) {
      obstacle.block<3,1>(0,o)=PROJ*Vec3d::Random()*maxDist;
      obstacle(3,o)=RandEngine::randR(0,obstacle.block<3,1>(0,o).norm());
      obstacle.block<3,1>(0,o)+=state.segment<3>(nrS*2-6);
      states.col(i)=getSensing(env,state,obstacle);
    }
    if(dist != states.col(i)) {
      //fillin data
      if(debugFeature) {
        actions.col(i)=getFeature(env,state,states.col(i));
      } else {
        for(sizeType o=0; o<nrEff; o++) {
          actions.block<3,1>(o*4,i)=obstacle.block<3,1>(0,o)-state.segment<3>(nrS*2-6);
          actions(o*4+3,i)=max<scalarD>(actions.block<3,1>(o*4,i).norm()-obstacle(3,o),0);
          actions.block<3,1>(o*4,i).normalize();
        }
      }
      if(debug || debugFeature)
        writeVTK("./debugCNNFeature/feat"+boost::lexical_cast<string>(i)+".vtk",
                 env,state,obstacle,states.col(i),actions.col(i));
      i++;
    }
  }
  if(!debug && !debugFeature)
    _net->trainRMSProp(states,actions);
  else exit(-1);
}
CNNObstacleFeature::Vec CNNObstacleFeature::getSensing(const FEMEnvironmentDQNObstacle& env,const Vec& state,const Mat4Xd& obstacle) const
{
  const FEMCIOEngineImplicit& eng=env.sol();
  sizeType nrS=eng.getMCalc().size();
  Vec3d rot=state.segment<3>(nrS*2-3);
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(rot,NULL,NULL,NULL,NULL);
  //generate feature
  Vec3d center=state.segment<3>(nrS*2-6);
  scalar maxDist=_net->_tree.get<scalar>("maxRadarDistance");
  Vec ret=Vec::Zero(nrSensing());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_dir.size(); i++) {
    scalarD& feat=ret[i]=maxDist;
    Vec3d dir=R*_dir[i],toObs;
    for(sizeType j=0; j<obstacle.cols(); j++) {
      toObs=obstacle.block<3,1>(0,j)-center;
      scalarD rad=obstacle(3,j);
      scalarD a=1,b=-2*dir.dot(toObs);
      scalarD c=toObs.squaredNorm()-rad*rad;
      scalarD delta=b*b-4*a*c;
      if(c <= 0) {
        feat=0;
        break;
      } else if(delta > 0) {
        scalarD t0=(-b-sqrt(delta))/(2*a);
        if(t0 > 0 && t0 < feat)
          feat=t0;
      }
    }
  }
  return ret;
}
CNNObstacleFeature::Vec CNNObstacleFeature::getFeature(const FEMEnvironmentDQNObstacle& env,const Vec& state,const Vec& sensing)
{
  _net->clearData();
  Vec ret=_net->foreprop(sensing);
  sizeType nrS=env.sol().getMCalc().size();
  Vec3d rot=state.segment<3>(nrS*2-3);
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(rot,NULL,NULL,NULL,NULL);
  for(sizeType j=0; j<ret.size(); j+=4)
    ret.segment<3>(j)=(R*ret.segment<3>(j)).eval();
  return ret;
}
sizeType CNNObstacleFeature::nrFeature(const FEMEnvironmentDQN& env) const
{
  sizeType nrNN=env.sol()._tree.get<sizeType>("nrNearestNeighbor",4);
  return nrNN*4;
}
sizeType CNNObstacleFeature::nrSensing() const
{
  return (sizeType)_dir.size();
}
NeuralNet& CNNObstacleFeature::getNet()
{
  return *_net;
}

//FEMEnvironmentDQNObstacle
struct LssObstacle {
  LssObstacle(const Vec3d& ctr,const Mat4Xd& obstacle):_ctr(ctr),_obstacle(obstacle) {}
  bool operator()(sizeType i,sizeType j) const {
    return (_obstacle.block<3,1>(0,i)-_ctr).squaredNorm() < (_obstacle.block<3,1>(0,j)-_ctr).squaredNorm();
  }
  const Vec3d& _ctr;
  const Mat4Xd& _obstacle;
};
FEMEnvironmentDQNObstacle::FEMEnvironmentDQNObstacle()
  :FEMEnvironmentDQN(typeid(FEMEnvironmentDQNObstacle).name()) {}
FEMEnvironmentDQNObstacle::FEMEnvironmentDQNObstacle(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt)
  :FEMEnvironmentDQN(typeid(FEMEnvironmentDQNObstacle).name())
{
  reset(sol,DMPMT,pt);
}
void FEMEnvironmentDQNObstacle::reset(boost::shared_ptr<FEMCIOEngineImplicit> sol,const DMPNeuralNet& DMPMT,const boost::property_tree::ptree* pt)
{
  FEMEnvironmentDQN::reset(sol,DMPMT,pt);
  boost::shared_ptr<DDPEnergy> energy;
  Vec3d g=getIgnoreDir();
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalarD fade=_sol->_tree.get<scalar>("obstacleFade",10.0f)/bb.getExtent().norm();
  scalarD coef=_sol->_tree.get<scalarD>("obstacleCoef",1E3f);
  if(g.norm() > 1E-10f)
    energy.reset(new ObstacleEnergy(&g,bb.getExtent().norm(),fade,coef));
  else energy.reset(new ObstacleEnergy(NULL,bb.getExtent().norm(),fade,coef));
  _ess.push_back(energy);
}
boost::shared_ptr<Serializable> FEMEnvironmentDQNObstacle::copy() const
{
  return boost::shared_ptr<Serializable>(new FEMEnvironmentDQNObstacle);
}
void FEMEnvironmentDQNObstacle::writeStateVTK(const string& path)
{
  bool writePov=_sol->_tree.get<bool>("writePov",false);
  if(writePov) {
    string pathC=path;
    boost::replace_all(pathC,".vtk",".pov");
    writeStatePov(pathC);
  }
  FEMEnvironment::writeStateVTK(path);
  if(_feature) {
    string pathC=path;
    boost::replace_all(pathC,".vtk","CNNFeature.vtk");
    Vec sensing=_feature->getSensing(*this,_state,_obstacle);
    _feature->writeSensingVTK(pathC,*this,_state,sensing);
  }
  {
    string pathC=path;
    boost::replace_all(pathC,".vtk","ToTargetAndObstacle.vtk");
    VTKWriter<scalar> os("toTargetAndObstacle",pathC,true);
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    vector<scalar> css;

    sizeType nrS=_sol->getMCalc().size();
    sizeType nrNN=_sol->_tree.get<sizeType>("nrNearestNeighbor",4);
    Vec3 center=_state.segment<3>(nrS*2-6).cast<scalar>(),dir;
    dir=_state.segment<3>(nrS*2).cast<scalar>();
    vss.push_back(center);
    vss.push_back(center+dir);
    css.push_back(0);
    for(sizeType i=0,off=nrS*2+3; i<nrNN; i++,off+=4) {
      dir=_state.segment<3>(off).cast<scalar>();
      vss.push_back(center);
      vss.push_back(center+dir*_state[off+3]);
      css.push_back(1);
    }
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                   VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),
                   VTKWriter<scalar>::LINE);
    os.appendCustomData("color",css.begin(),css.end());
  }
  writeLocusVTK(path);
  writeEnvVTK(path,&_obstacle);
}
void FEMEnvironmentDQNObstacle::writeStatePov(const string& path)
{
  FEMEnvironment::writeStatePov(path);
  writeLocusPov(path);
  writeEnvPov(path,&_obstacle);
}
void FEMEnvironmentDQNObstacle::setUsePeriodicTarget(sizeType s0,sizeType s1)
{
  FEMEnvironmentDQN::setUsePeriodicTarget(s0,s1);
  _obstacles.clear();
  for(sizeType i=0; i<(sizeType)_targets.size(); i++)
    _obstacles.push_back(pickObstacle(_targets[i]));
}
bool FEMEnvironmentDQNObstacle::isTerminal(sizeType i) const
{
  sizeType nrS=_sol->getMCalc().size();
  Vec state=_state.segment(0,nrS*2);
  bool stopOC=_sol->_tree.get<bool>("stopOnCollision",false);
  for(sizeType i=0; i<(sizeType)_ess.size(); i++)
    if(!stopOC && boost::dynamic_pointer_cast<ObstacleEnergy>(_ess[i]))
      continue;
    else if(boost::dynamic_pointer_cast<RLEnergy>(_ess[i])->isTerminal(state)) {
      INFO("DDP command termination!")
      return true;
    }
  return false;
}
sizeType FEMEnvironmentDQNObstacle::nrEffectiveObstacles() const
{
  Vec3d g=getIgnoreDir();
  string type=_sol->_tree.get<string>("obstacleType","");
  if(type == "noObstacle") {
    return 0;
  } else if(type == "middle") {
    return 1;
  } else if(type == "surroundTarget") {
    return (g.norm() > 1E-10f) ? 4 : 6;
  } else {
    ASSERT_MSG(false,"Unknown obstacle settings!")
    return 0;
  }
}
void FEMEnvironmentDQNObstacle::setupCNNFeature(const string& path)
{
  _feature.reset(new CNNObstacleFeature);
  _feature->read(path);
}
//helper
FEMEnvironmentDQNObstacle::FEMEnvironmentDQNObstacle(const string& name):FEMEnvironmentDQN(name) {}
void FEMEnvironmentDQNObstacle::cycleTarget()
{
  ASSERT(_obstacles.size() == _targets.size())
  sizeType nrS=_sol->getMCalc().size();
  //target
  _target=_targets.front();
  _targets.push_back(_target);
  _targets.erase(_targets.begin());
  _target+=_state.segment<3>(nrS-6);
  //obstacle
  _obstacle=_obstacles.front();
  _obstacles.push_back(_obstacle);
  _obstacles.erase(_obstacles.begin());
  _obstacle.block(0,0,3,_obstacle.cols())+=_state.segment<3>(nrS-6)*Vec::Ones(_obstacle.cols()).transpose();
}
void FEMEnvironmentDQNObstacle::pickTarget()
{
  sizeType nrS=_sol->getMCalc().size();
  FEMEnvironmentDQN::pickTarget();
  _obstacle=pickObstacle(_target);
  _obstacle.block(0,0,3,_obstacle.cols())+=_state.segment<3>(nrS-6)*Vec::Ones(_obstacle.cols()).transpose();
}
Mat4Xd FEMEnvironmentDQNObstacle::pickObstacle(const Vec3d& target) const
{
#define RAND_VEC3 Vec3d(RandEngine::randR(-1,1),RandEngine::randR(-1,1),RandEngine::randR(-1,1))
  //project
  Mat4Xd ret=Mat4Xd(4,0);
  Mat3d PROJ=Mat3d::Identity();
  Vec3d g=getIgnoreDir();
  if(g.norm() > 1E-10f)
    PROJ-=g.normalized()*g.normalized().transpose();
  //characteristic length
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalar len=bb.getExtent().norm();
  //parameters
  scalar minR=_sol->_tree.get<scalar>("minObstacleRad",1);
  scalar maxR=_sol->_tree.get<scalar>("maxObstacleRad",2);
  scalar perturb=_sol->_tree.get<scalar>("centerPerturbRad",1);
  scalar sepDist=_sol->_tree.get<scalar>("obstacleSepDist",0);
  sizeType nr=_sol->_tree.get<sizeType>("nrObstacle",10);
  //create obstacles by scene type
  string type=_sol->_tree.get<string>("obstacleType","");
  if(type == "noObstacle") {
  } if(type == "middle") {
    ret=Mat4Xd(4,nr);
    for(sizeType i=0; i<nr; i++)
      ret.block<3,1>(0,i)=PROJ*(target*(i+1)/(scalar)(nr+1));
  } else if(type == "surroundTarget") {
    if(g.norm() > 1E-10f) {
      Vec3d gT1=RAND_VEC3.cross(g.normalized()).normalized();
      Vec3d gT2=gT1.cross(g.normalized()).normalized();
      ret=Mat4Xd(4,4);
      ret.block<3,1>(0,0)= gT1*target.norm()/2;
      ret.block<3,1>(0,1)=-gT1*target.norm()/2;
      ret.block<3,1>(0,2)= gT2*target.norm()/2;
      ret.block<3,1>(0,3)=-gT2*target.norm()/2;
    } else {
      ret=Mat4Xd(4,6);
      Mat3d R=makeRotation<scalarD>(RAND_VEC3*M_PI);
      ret.block<3,1>(0,0)=R*Vec3d( 1,0,0)*target.norm()/2;
      ret.block<3,1>(0,1)=R*Vec3d(-1,0,0)*target.norm()/2;
      ret.block<3,1>(0,2)=R*Vec3d(0, 1,0)*target.norm()/2;
      ret.block<3,1>(0,3)=R*Vec3d(0,-1,0)*target.norm()/2;
      ret.block<3,1>(0,4)=R*Vec3d(0,0, 1)*target.norm()/2;
      ret.block<3,1>(0,5)=R*Vec3d(0,0,-1)*target.norm()/2;
    }
    ret.block(0,0,3,ret.cols())+=target*Vec::Ones(ret.cols()).transpose();
  } else {
    ASSERT_MSG(false,"Unknown obstacle settings!")
  }
  //set radius
  for(sizeType i=0; i<ret.cols(); i++)
    ret(3,i)=len*RandEngine::randR(minR,maxR);
  //perturb
  for(sizeType i=0; i<ret.cols(); i++)
    while(true) {
      Vec3d ctr=ret.block<3,1>(0,i)+PROJ*RAND_VEC3*len*perturb;
      bool valid=ctr.norm() > ret(3,i)+len*sepDist;
      valid=valid && (ctr-target).norm() > ret(3,i)+len*sepDist;
      for(sizeType j=0; valid && j<i; j++) {
        scalarD dist=(ret.block<3,1>(0,j)-ctr).norm();
        scalarD radSum=ret(3,j)+ret(3,i)+len*sepDist;
        if(radSum > dist)
          valid=false;
      }
      if(valid) {
        ret.block<3,1>(0,i)=ctr;
        break;
      }
    }
  return ret;
#undef RAND_VEC3
}
void FEMEnvironmentDQNObstacle::syncTarget()
{
  if((sizeType)_ess.size() > 0)
    boost::dynamic_pointer_cast<RLEnergy>(_ess[0])->setPos(_target);
  if((sizeType)_ess.size() > 1)
    boost::dynamic_pointer_cast<ObstacleEnergy>(_ess[1])->setObstacle(_obstacle);
}
void FEMEnvironmentDQNObstacle::updateFeature(Vec& ret) const
{
  FEMEnvironmentDQN::updateFeature(ret);
  sizeType nrS=_sol->getMCalc().size();
  sizeType off=nrS*2+FEMEnvironmentDQN::nrFeature();
  sizeType nrNN=_sol->_tree.get<sizeType>("nrNearestNeighbor",4);
  sizeType nrEff=nrEffectiveObstacles();
  if(_feature) {
    Vec sensing=_feature->getSensing(*this,_state,_obstacle);
    ret.segment(off,nrNN*4)=_feature->getFeature(*this,_state,sensing);
  } else {
    //sort obstacle
    vector<sizeType> oSort;
    Vec3d center=ret.segment<3>(nrS*2-6);
    for(sizeType i=0; i<_obstacle.cols(); i++)
      oSort.push_back(i);
    sort(oSort.begin(),oSort.end(),LssObstacle(center,_obstacle));
    //generate feature
    for(sizeType i=0; i<nrNN; i++,off+=4)
      if(i < _obstacle.cols() && i < nrEff) {
        //set to closest point
        ret.segment<3>(off)=_obstacle.block<3,1>(0,oSort[i])-center;
        ret[off+3]=max<scalarD>(ret.segment<3>(off).norm()-_obstacle(3,oSort[i]),0);
        ret.segment<3>(off).normalize();
      } else {
        ret.segment<4>(off).setZero();
      }
  }
}
sizeType FEMEnvironmentDQNObstacle::nrFeature() const
{
  sizeType nrNN=_sol->_tree.get<sizeType>("nrNearestNeighbor",4);
  return FEMEnvironmentDQN::nrFeature()+4*nrNN;
}

//FEMEnvironmentDQNObstacleDynamic
boost::shared_ptr<Serializable> FEMEnvironmentDQNObstacleDynamic::copy() const
{
  return boost::shared_ptr<Serializable>(new FEMEnvironmentDQNObstacleDynamic);
}
void FEMEnvironmentDQNObstacleDynamic::writeStateVTK(const string& path)
{
  bool writePov=_sol->_tree.get<bool>("writePov",false);
  if(writePov) {
    string pathC=path;
    boost::replace_all(pathC,".vtk",".pov");
    writeStatePov(pathC);
  }
  FEMEnvironment::writeStateVTK(path);
  if(_feature) {
    string pathC=path;
    boost::replace_all(pathC,".vtk","CNNFeature.vtk");
    Vec sensing=_feature->getSensing(*this,_state,_obstacle);
    _feature->writeSensingVTK(pathC,*this,_state,sensing);
  }
  {
    string pathC=path;
    boost::replace_all(pathC,".vtk","ToTargetAndObstacle.vtk");
    VTKWriter<scalar> os("toTargetAndObstacle",pathC,true);
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    vector<scalar> css;

    sizeType nrS=_sol->getMCalc().size();
    sizeType nrNN=_sol->_tree.get<sizeType>("nrNearestNeighbor",4);
    Vec3 center=_state.segment<3>(nrS*2-6).cast<scalar>(),dir;
    dir=_state.segment<3>(nrS*2).cast<scalar>();
    vss.push_back(center);
    vss.push_back(center+dir);
    css.push_back(0);
    for(sizeType i=0,off=nrS*2+3; i<nrNN; i++,off+=4) {
      dir=_state.segment<3>(off).cast<scalar>();
      vss.push_back(center);
      vss.push_back(center+dir*_state[off+3]);
      css.push_back(1);
    }
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                   VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),
                   VTKWriter<scalar>::LINE);
    os.appendCustomData("color",css.begin(),css.end());
  }
  writeLocusVTK(path);
  writeEnvVTK(path,&_obstacle,true);
}
void FEMEnvironmentDQNObstacleDynamic::writeStatePov(const string& path)
{
  FEMEnvironment::writeStatePov(path);
  writeLocusPov(path);
  writeEnvPov(path,&_obstacle,true);
}
void FEMEnvironmentDQNObstacleDynamic::setUsePeriodicTarget(sizeType s0,sizeType s1)
{
  FEMEnvironmentDQN::setUsePeriodicTarget(s0,s1);
  _obstacles.clear();
  _obstacleVels.clear();
  for(sizeType i=0; i<(sizeType)_targets.size(); i++) {
    _obstacles.push_back(pickObstacle(_targets[i]));
    _obstacleVels.push_back(pickObstacleVel(_obstacles.back()));
  }
}
//helper
void FEMEnvironmentDQNObstacleDynamic::cycleTarget()
{
  ASSERT(_obstacles.size() == _targets.size())
  sizeType nrS=_sol->getMCalc().size();
  //target
  _target=_targets.front();
  _targets.push_back(_target);
  _targets.erase(_targets.begin());
  _target+=_state.segment<3>(nrS-6);
  //obstacle
  _obstacle=_obstacles.front();
  _obstacles.push_back(_obstacle);
  _obstacles.erase(_obstacles.begin());
  _obstacle.block(0,0,3,_obstacle.cols())+=_state.segment<3>(nrS-6)*Vec::Ones(_obstacle.cols()).transpose();
  _obstacle0=_obstacle;
  //obstacle vel
  _obstacleVel=_obstacleVels.front();
  _obstacleVels.push_back(_obstacleVel);
  _obstacleVels.erase(_obstacleVels.begin());
}
void FEMEnvironmentDQNObstacleDynamic::pickTarget()
{
  sizeType nrS=_sol->getMCalc().size();
  FEMEnvironmentDQN::pickTarget();
  _obstacle=pickObstacle(_target);
  _obstacle.block(0,0,3,_obstacle.cols())+=_state.segment<3>(nrS-6)*Vec::Ones(_obstacle.cols()).transpose();
  _obstacleVel=pickObstacleVel(_obstacle);
  _obstacle0=_obstacle;
}
Mat3Xd FEMEnvironmentDQNObstacleDynamic::pickObstacleVel(const Mat4Xd& obstacle) const
{
#define RAND_VEC3 Vec3d(RandEngine::randR(-1,1),RandEngine::randR(-1,1),RandEngine::randR(-1,1))
  //PROJ
  Mat3d PROJ=Mat3d::Identity();
  Vec3d g=getIgnoreDir();
  if(g.norm() > 1E-10f)
    PROJ-=g.normalized()*g.normalized().transpose();
  //characteristic length
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalar len=bb.getExtent().norm();
  //ret
  Mat3Xd ret=Mat3Xd::Zero(3,obstacle.cols());
  for(sizeType i=0; i<ret.cols(); i++)
    ret.col(i)=(PROJ*RAND_VEC3).normalized();
  return ret*len;
#undef RAND_VEC3
}
void FEMEnvironmentDQNObstacleDynamic::updateFeature(Vec& ret) const
{
  //integrate
  scalarD phase=_sol->_tree.get<scalar>("phase",50);
  scalarD range=_sol->_tree.get<scalar>("obstacleRange",3);
  const_cast<Mat4Xd&>(_obstacle)=_obstacle0;
  const_cast<Mat4Xd&>(_obstacle).block(0,0,3,_obstacle.cols())+=_obstacleVel*sin(_time*M_PI*2/(scalarD)phase)*range;
  //extract feature
  if((sizeType)_ess.size() > 1)
    boost::dynamic_pointer_cast<ObstacleEnergy>(_ess[1])->setObstacle(_obstacle);
  FEMEnvironmentDQNObstacle::updateFeature(ret);
}
