#ifndef FEM_CIO_OPTIMIZER_TERM_H
#define FEM_CIO_OPTIMIZER_TERM_H

#include "FEMDDPSolver.h"

PRJ_BEGIN

class QPInterface;
//Terms in an objective function
class CIOOptimizerTerm
{
public:
  typedef FEMDDPSolver::Vec Vec;
  typedef FEMDDPSolver::Vss Vss;
  typedef FEMDDPSolver::Mss Mss;
  typedef Objective<scalarD>::STrip STrip;
  typedef Objective<scalarD>::STrips STrips;
  CIOOptimizerTerm(CIOOptimizer& opt);
  virtual scalarD runSQP(FEMCIOEngine& eng,sizeType i,Vec& DFDXI,Matd* DDFDDXI) const;
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual int values() const;
  virtual bool valid() const;
protected:
  CIOOptimizer& _opt;
};
//Objective Term
class CIOOptimizerObjectiveTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerObjectiveTerm(CIOOptimizer& opt);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual int values() const;
  virtual bool valid() const;
};
//CIOOptimizerObjectiveMTTerm
class CIOOptimizerObjectiveMTTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerObjectiveMTTerm(CIOOptimizer& opt,sizeType K);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual int values() const;
  virtual bool valid() const;
  virtual sizeType K() const;
private:
  sizeType _K;
};
//Laplacian Term
class CIOOptimizerLaplacianTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerLaplacianTerm(CIOOptimizer& opt);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual int values() const;
  virtual bool valid() const;
};
//Contact Invariant Optimization
class CIOOptimizer : public BLEICInterface
{
  friend class CIOOptimizerTerm;
  friend class CIOOptimizerPhysicsTerm;
  friend class CIOOptimizerPolicyPhysicsTerm;
  friend class CIOOptimizerContactTerm;
  friend class CIOOptimizerShuffleAvoidanceTerm;
  friend class CIOOptimizerConicShuffleAvoidanceTerm;
  friend class CIOOptimizerObjectiveTerm;
  friend class CIOOptimizerObjectiveMTTerm;
  friend class CIOOptimizerLaplacianTerm;
  friend class CIOOptimizerSelfCollisionTerm;
  friend class CIOOptimizerSelfCollisionTermPrecomputed;
public:
  typedef FEMDDPSolver::Vec Vec;
  typedef FEMDDPSolver::Vss Vss;
  typedef FEMDDPSolver::Mss Mss;
  CIOOptimizer(FEMDDPSolver& sol,sizeType horizon,FEMDDPSolverInfo& info,sizeType K=-1);
  template <typename T> void beginSingleOutTerm()
  {
    _termBackups=_terms;
    boost::shared_ptr<T> t=getTerm<T>();
    _terms.clear();
    if(t)
      _terms.push_back(t);
  }
  template <typename T1,typename T2> void beginSingleOutTwoTerms()
  {
    _termBackups=_terms;
    boost::shared_ptr<T1> t1=getTerm<T1>();
    boost::shared_ptr<T2> t2=getTerm<T2>();
    _terms.clear();
    if(t1)
      _terms.push_back(t1);
    if(t2)
      _terms.push_back(t2);
  }
  void endSingleOutTerm()
  {
    _terms=_termBackups;
    _termBackups.clear();
  }
  template <typename T> boost::shared_ptr<T> getTerm() {
    boost::shared_ptr<T> ret=NULL;
    for(sizeType i=0; i<(sizeType)_terms.size(); i++)
      if(ret=boost::dynamic_pointer_cast<T>(_terms[i]))
        return ret;
    return ret;
  }
  template <typename T> const T* getTermConst() const {
    boost::shared_ptr<T> ret=NULL;
    for(sizeType i=0; i<(sizeType)_terms.size(); i++)
      if(ret=boost::dynamic_pointer_cast<T>(_terms[i]))
        return ret.get();
    return ret.get();
  }
  virtual void clearInvalidTerms();
  virtual void reset();
  virtual int inputs() const;
  virtual int values() const;
  virtual int valuesNoCollision() const;
  virtual const FEMDDPSolverInfo& info() const;
  virtual FEMDDPSolverInfo& info();
  virtual const Matd& DUDE() const;
  //operators: mpi related
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable);
  virtual scalarD operator()(const Vec& x,Vec* fgrad,STrips* fhess);
  virtual scalarD operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable,bool LMOrSQP);
  virtual scalarD operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable,bool LMOrSQP,const Vec2i& fromTo);
  virtual void operator()(Eigen::Map<const Vec>& xs,scalarD& FX,Eigen::Map<Vec>& DFDX);
  virtual void updateConfig(const Vec& x,bool cf,bool hess=false,const Vec2i* fromTo=NULL,vector<boost::shared_ptr<FEMCIOEngine> >* mp=NULL);
  virtual Vec removeCF(const Vec& X) const;
  virtual Vec collectSolution(bool cf) const;
  virtual void writeResidueInfo(const Vec& xs);
  virtual void assignControl();
  //working code
  virtual scalarD runSQP(FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac) const;
  virtual void runLM(FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  //update neural network
  virtual void updateWeight();
  virtual void applyControl(scalar dt,FEMCIOEngine& eng,sizeType i) const;
  virtual void updateNeuralNet(const Vec& fvec);
  virtual void updateNeuralNet(const Vec* x,bool modifiable);
  //update force
  virtual void updateF(sizeType i,FEMCIOEngine& eng,QPInterface& qp,const Matd& metricSqr,scalar dt,scalar wCont,Vec vios,const Vec& E) const;
  virtual void updateForce();
  //metric
  virtual void resetMetric();
  static void debugComputeMetric();
  static scalarD rescaleMetric(const Matd& m);
  static void computeMetricU(const Matd& C1,const Matd& C2,Matd& metricSqr,Matd& DUDE);
  static void computeMetricX(const Matd& C1,const Matd& C2,Matd& metricSqr,Matd& TE);
  //for debug
  virtual void compareDFDX(const Vec& DFDX0,const Vec& DFDX1) const;
  virtual void randomizeParameter(bool phys=true,bool coll=true);
  virtual void randomizeContact();
  virtual Vec get2DMask() const;
  virtual bool hasContact() const;
protected:
  //add jacobian
  void addBlockXN(STrips& fjac,sizeType row,sizeType i,const Matd& blk) const;
  void addBlockX(STrips& fjac,sizeType row,sizeType i,const Matd& blk) const;
  void addBlockXP(STrips& fjac,sizeType row,sizeType i,const Matd& blk) const;
  //add gradient
  void addBlockXN(Eigen::Map<Vec>& DFDX,sizeType i,const Vec& blk) const;
  void addBlockX(Eigen::Map<Vec>& DFDX,sizeType i,const Vec& blk) const;
  void addBlockXP(Eigen::Map<Vec>& DFDX,sizeType i,const Vec& blk) const;
  //add gradient/hessian
  void addBlockSQP(sizeType i,const Vec& GI,const Matd& HI,Vec& G,STrips* H) const;
  Coli blockIndex(sizeType i) const;
  Coli blockIndexX(sizeType i) const;
  //data
  bool _periodicRotation;
  bool _periodicPosition;
  bool _periodicDeform;
  bool _fixRotation;
  sizeType _horizon;
  FEMDDPSolver& _sol;
  FEMDDPSolverInfo& _info;
  vector<boost::shared_ptr<CIOOptimizerTerm> > _terms;
  vector<boost::shared_ptr<CIOOptimizerTerm> > _termBackups;
  Matd _metricSqr,_metricSqrSqrt,_DUDE;
  boost::shared_ptr<QPInterface> _qpSol;
  sizeType _iter;
};
class CIOOptimizerMP : public CIOOptimizer
{
public:
  CIOOptimizerMP(FEMDDPSolver& sol,sizeType horizon,FEMDDPSolverInfo& info,sizeType K=-1);
  //parallel operators
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable);
  virtual scalarD operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable,bool LMOrSQP,const Vec2i& fromTo);
  virtual void operator()(Eigen::Map<const Vec>& xs,scalarD& FX,Eigen::Map<Vec>& DFDX);
  const vector<boost::shared_ptr<FEMCIOEngine> >* mp() const;
  vector<boost::shared_ptr<FEMCIOEngine> >* mp();
protected:
  vector<boost::shared_ptr<FEMCIOEngine> > _mp;
  vector<boost::shared_ptr<QPInterface> > _qp;
};

PRJ_END

#endif
