#ifndef QCQP_H
#define QCQP_H

#include <Dynamics/OPTInterface.h>

PRJ_BEGIN

template <int DIM>
class QCQP
{
public:
  typedef BLEICInterface::Vec Vec;
  typedef Eigen::Matrix<scalarD,DIM,1> FVec;
  typedef Callback<scalarD,Kernel<scalarD> > CB;
  //QCQP: feasibility
  static FVec ensureFeasibleBlock(FVec F,scalarD mu,scalarD minVio);
  static Vec ensureFeasible(Vec F,scalarD mu,scalarD minVio);
  //QCQP: main functionality
  static sizeType solve(const Matd& H,const Vec& G,Vec& F,scalarD mu,sizeType maxIter,scalar relTol,CB* cb=NULL);
  static scalarD computeGradHess(const Matd& H,const Matd& G,const Vec& F,scalarD mu,scalarD kappa,Matd* HIt=NULL,Vec* GIt=NULL,Vec* TIKReg=NULL);
  //QCQP: line search
  static scalarD computeMaxVio(const Vec& F,scalarD mu);
  static scalarD computeAlphaThresI(const Vec3d& COEFC,const Vec2d& COEFP);
  static scalarD computeAlphaThres(const Vec& F,const Vec& DF,scalarD mu);
  static bool lineSearchBruteForce(const Matd& H,const Vec& G,Vec& GIt,Vec& F,Vec& DF,scalarD mu,scalarD kappa);
  //debug
  static void debugQCQP();
  static void debugGradHess();
  static void debugReadProb(const string& path);
  static void writeProb(const Matd& H,const Vec& G,const Vec& F,scalarD mu,const string& path);
  static void readProb(Matd& H,Vec& G,Vec& F,scalarD& mu,const string& path);
private:
  //parameters
  static scalarD _alphaDec;
  static scalarD _minAlpha;
  static scalarD _minVio;
  static scalarD _kappaMax;
  static scalarD _kappaInc;
};

PRJ_END

#endif
