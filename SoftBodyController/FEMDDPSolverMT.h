#ifndef FEM_DDP_SOLVER_MT_H
#define FEM_DDP_SOLVER_MT_H

#include "FEMDDPSolver.h"
#include "FEMEnvironmentMT.h"

PRJ_BEGIN

class FEMDDPSolverMT;
class DMPNeuralNet;
//CIOOptimizerMT
class CIOOptimizerMT : public BLEICInterface
{
public:
  typedef FEMDDPSolver::Vec Vec;
  typedef FEMDDPSolver::Vss Vss;
  typedef FEMDDPSolver::Mss Mss;
  CIOOptimizerMT(FEMDDPSolverMT& sol,sizeType horizon);
  virtual void reset();
  virtual int inputs() const;
  virtual int values() const;
  virtual int valuesNoCollision() const;
  //operators: mpi related
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable);
  virtual scalarD operator()(const Vec& x,Vec* fgrad,STrips* fhess);
  virtual void operator()(Eigen::Map<const Vec>& xs,scalarD& FX,Eigen::Map<Vec>& DFDX);
  virtual void updateConfig(const Vec& x,bool cf,bool hess=false,const Vec2i* fromTo=NULL);
  virtual Vec removeCF(const Vec& X) const;
  virtual Vec collectSolution(bool cf) const;
  virtual void writeResidueInfo(const Vec& xs);
  virtual void assignControl();
  //update neural network
  virtual void updateWeight();
  virtual void updateNeuralNet();
  virtual void updateNeuralNet(const Vec* x,bool modifiable);
  virtual scalarD collectNeuralNetData(const CIOOptimizer& opt,const Vec& fvec);
  //update force
  virtual void updateForce();
  //for debug
  virtual void compareDFDX(const Vec& DFDX0,const Vec& DFDX1) const;
  virtual void randomizeParameter(bool phys=true,bool coll=true);
  virtual void randomizeContact();
  virtual Vec get2DMask() const;
  virtual bool hasContact() const;
  vector<boost::shared_ptr<CIOOptimizer> > _tasks;
protected:
  FEMDDPSolverMT& _sol;
  sizeType _iter,_horizon;
  //temporary data
  Matd _states,_actions;
  Mss _metrics;
  STrips _fjacBlk;
  Vec _fvecBlk;
};
//CIOPolicyOptimizerMT
class CIOPolicyOptimizerMT : public CIOOptimizerMT
{
public:
  CIOPolicyOptimizerMT(FEMDDPSolverMT& sol,sizeType horizon);
  //update neural network
  virtual void updateNeuralNet();
  virtual scalarD collectNeuralNetData(const CIOOptimizer& opt,const Vec& fvec);
protected:
  DMPNeuralNet& _net;
};
//update neural network
class FEMDDPSolverMT : public FEMDDPSolver
{
public:
  FEMDDPSolverMT(FEMEnvironmentMT& env);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual void readFromTree();
  virtual void writeToTree();
  virtual void writeSelfCollVTK(const string& path);
  virtual void writeStatesMTVTK(const string& path,bool useXs=false,bool pov=false) const;
  //debug and visualization
  virtual void debugObj(const Vec& x,const Vec& u,bool addObjUX);
  virtual void updateNeuralNet(bool debugMode,sizeType maxIt,scalar epsilon=-1,scalar delta=-1);
  virtual void writeResidualInfo(bool SP,bool MP);
  virtual void assignControl(bool SP);
  virtual void recenter(bool useXs=false);
  //for cio
  virtual void initializeCIO();
  virtual void optimizeCIO(bool MP);
  virtual void debugCIOGradient(bool SP,bool MP,bool load=false);
  //for cio using ADLM
  virtual void optimizeCIOLM(bool MP,bool load=false);
  virtual void debugCIOLMEnergy(bool SP,bool MP,bool load=false);
  virtual void debugCIOForce(bool SP,bool MP,bool load=false);
  virtual void debugCIOSQPEnergy(bool SP,bool MP,bool load=false);
  //for policy search
  virtual void optimizeCIOPolicy(bool SP,bool MP,bool load=false);
  virtual void debugNeuralNet(bool load=false);
protected:
  virtual boost::shared_ptr<CIOOptimizerMT> createOptMT(bool SP,bool MP);
  vector<FEMDDPSolverInfo> _infos;
};

PRJ_END

#endif
