#ifndef CIO_OPTIMIZER_PHYSICS_TERM_H
#define CIO_OPTIMIZER_PHYSICS_TERM_H

#include "CIOOptimizer.h"
#include "CIOOptimizerContactTerm.h"

PRJ_BEGIN

//Physics Term
class CIOOptimizerPhysicsTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerPhysicsTerm(CIOOptimizer& opt);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual const Matd& metricF() const;
  virtual void randomizeParam();
  virtual void balanceResidual();
  virtual int values() const;
  virtual bool valid() const;
  boost::shared_ptr<CIOOptimizerContactTerm> _cTerm;
protected:
  Matd _metricF;
  Matd _metricFSqrt;
};
//Policy Physics Term
class CIOOptimizerPolicyPhysicsTerm : public CIOOptimizerPhysicsTerm
{
public:
  struct FrameInfo {
    Vec _DUDEE;
    Vec _CNT;
  };
  CIOOptimizerPolicyPhysicsTerm(CIOOptimizer& opt,sizeType K);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual void randomizeParam() override;
  virtual void balanceResidual() override;
  void setCoefPhys(scalar coefPhys);
  void setCoefPhysADMM(scalar coefPhysADMM);
  void setForNNUpdate(bool NNUpdate);
  Eigen::DiagonalMatrix<scalarD,-1,-1> getCoefPhysADMM() const;
protected:
  void setTask() const;
  void recomputeMetric();
  //Matd _C;
  bool _forNNUpdate;
  scalarD _coefPhys;
  scalarD _coefPhysADMM;
  Matd _lambdaCoef;
private:
  sizeType _K;
  string _prefix;
  vector<FrameInfo> _infos;
};

PRJ_END

#endif
