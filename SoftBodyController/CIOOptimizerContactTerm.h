#ifndef FEM_CIO_OPTIMIZER_CONTACT_TERM_H
#define FEM_CIO_OPTIMIZER_CONTACT_TERM_H

#include "CIOOptimizer.h"

PRJ_BEGIN

class CIOOptimizerContactTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerContactTerm(CIOOptimizer& opt);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual void updateContactVelReg(CIOContactInfo& info) const;
  virtual void updateContacts(vector<boost::shared_ptr<FEMCIOEngine> >* mp);
  virtual const Coli& getCollisionOff() const;
  virtual int values() const;
  virtual bool valid() const;
protected:
  virtual void buildCollisionOffset();
  Coli _cOff;
};

PRJ_END

#endif
