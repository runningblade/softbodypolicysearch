#ifndef FEM_GRADIENT_OPT_H
#define FEM_GRADIENT_OPT_H

#include <Dynamics/GradientInfo.h>
#include <CommonFile/solvers/Objective.h>

PRJ_BEGIN

struct FEMBody;
class FEMGradientOpt : public Objective<scalarD>
{
public:
  FEMGradientOpt(RigidReducedMCalculator& MCalc,FEMBody& body,const Vec& D);
  virtual void solve(Vec& x,sizeType maxIt,scalarD eps,scalarD delta,bool useCB);
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient);
  virtual int inputs() const;
  void debugGradient();
protected:
  RigidReducedMCalculator& _MCalc;
  FEMBody& _body;
  const Vec& _D;
};

PRJ_END

#endif
