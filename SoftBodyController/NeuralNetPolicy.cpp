#include "NeuralNetPolicy.h"
#include "DMPNeuralNet.h"
#include <CMAES/MLUtil.h>
#include <CommonFile/IO.h>

PRJ_BEGIN

//Policy Interface
Policy::Policy(const string& name):Serializable(name),_deterministic(false) {}
scalarD Policy::QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* hess) const
{
  WARNING("This is not a QPolicy!")
  return 0;
}
scalarD Policy::QLossM(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec& grad,SMat& hessM) const
{
  TRIPS hess;
  scalarD ret=QLoss(trans,gamma,from,to,&grad,&hess);
  hessM.resize(to-from,nrParam());
  hessM.setFromTriplets(hess.begin(),hess.end());
  hessM=(hessM.transpose()*hessM);
  return ret;
}
scalarD Policy::QLossMMapped(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec& grad,SMat& hessM,map<sizeType,sizeType>& MMap) const
{
  Vec grad0;
  TRIPS hess;
  scalarD ret=QLoss(trans,gamma,from,to,&grad0,&hess);
  //mapping
  sizeType off=0;
  for(TRIPS::const_iterator beg=hess.begin(),end=hess.end(); beg!=end; beg++) {
    if(MMap.find(beg->col()) == MMap.end())
      MMap[beg->col()]=off++;
    const_cast<sizeType&>(beg->col())=MMap[beg->col()];
  }
  grad.resize((sizeType)MMap.size());
  for(map<sizeType,sizeType>::const_iterator beg=MMap.begin(),end=MMap.end(); beg!=end; beg++)
    grad[beg->second]=grad0[beg->first];
  hessM.resize(to-from,(sizeType)MMap.size());
  hessM.setFromTriplets(hess.begin(),hess.end());
  hessM=(hessM.transpose()*hessM);
  return ret;
}
void Policy::optimizeQLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to) const
{
  WARNING("This is not a QPolicy!")
}
Policy::Vec Policy::getAction(const Vec& output,scalarD* possLog,Vec* possLogGrad,const Vec* samples)
{
  Vec possLogVec;
  Matd samplesMat;

  //assign
  if(samples)
    samplesMat=*samples;
  //compute
  Vec weight=Vec::Ones(1);
  Matd ret=getAction(output,
                     possLog?&possLogVec:NULL,
                     possLogGrad,&weight,
                     samples?&samplesMat:NULL);
  //collect
  if(possLog)
    *possLog=possLogVec[0];
  return ret;
}
boost::shared_ptr<KrylovMatrix<scalarD> > Policy::getHessKLDistance(const Matd& states) const
{
  WARNING("KLDistance not supported!")
  return boost::shared_ptr<KrylovMatrix<scalarD> >();
}
scalarD Policy::getDiffKLDistance(const Matd& states,const Vec& paramOld,Vec* grad)
{
  WARNING("KLDistance not supported!")
  return 0;
}
void Policy::setActionCov(scalar coef,scalar confidence)
{
  WARNING("Action covariance not supported!")
}
void Policy::setActionCov(const Vec& cov)
{
  WARNING("Action covariance not supported!")
}
void Policy::setEps(scalarD eps)
{
  WARNING("Exploration coefficient not supported!")
}
void Policy::updateQTarget()
{
  WARNING("This is not a QPolicy!")
}
void Policy::setAug(const Vec& aug)
{
  ASSERT_MSG(false,"This is not a multitask policy!")
}
Policy::Vec Policy::toVec(const map<sizeType,sizeType>* MMap) const
{
  WARNING("Sparse assign not allowed!")
  return Vec();
}
void Policy::fromVec(const Vec& weights,const map<sizeType,sizeType>* MMap)
{
  WARNING("Sparse assign not allowed!")
}
void Policy::setDeterministic(bool deterministic)
{
  _deterministic=deterministic;
}

//HessianKLMatrix
class HessianKLMatrix : public KrylovMatrix<scalarD>
{
public:
  virtual void multiply(const Vec& b,Vec& out) const {
    sizeType nrCov=_hessCov.size();
    sizeType offNet=_nrParam-nrCov;

    _net->fromVec(b,true);
    Matd& diffBack=const_cast<Matd&>(_net->weightprop(NULL));
    //OMP_PARALLEL_FOR_
    for(sizeType c=0; c<_hessMean.cols(); c++)
      diffBack.col(c).array()*=_hessMean.col(c).array();
    _net->backprop(NULL);

    out.resize(b.size());
    out.segment(0,offNet)=_net->toVec(true);
    out.segment(offNet,nrCov)=(b.segment(offNet,nrCov).array()*_hessCov.array()).matrix();
  }
  virtual sizeType n() const {
    return _nrParam;
  }
  boost::shared_ptr<NeuralNet> _net;
  sizeType _nrParam;
  Matd _hessMean; //action
  Vec _hessCov; //cov
};
//NeuralNet Policy Wrapper
NeuralNetPolicy::NeuralNetPolicy()
  :Policy(typeid(NeuralNetPolicy).name()) {}
NeuralNetPolicy::NeuralNetPolicy
(sizeType nrAction,sizeType nrBasis,
 boost::property_tree::ptree* pt)
  :Policy(typeid(NeuralNetPolicy).name())
{
  _net.reset(new DMPNeuralNet(nrAction,nrBasis,pt));
  setActionCov(Vec::Ones(nrAction));
}
NeuralNetPolicy::NeuralNetPolicy
(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,sizeType nrAction,const Vec* scale,
 boost::property_tree::ptree* pt,boost::shared_ptr<FeatureTransformLayer> feature)
  :Policy(typeid(NeuralNetPolicy).name())
{
  ASSERT_MSG(pt,"You must provide property tree for NeuralNetPolicy!")
  pt->put<scalar>("HFParam.initWeightNorm",0.001f);
  pt->put<scalar>("HFParam.initBiasNorm",0.001f);
  ASSERT(!scale || scale->size() == nrAction)
  pt->put<string>("activationFinal",scale?"TanH":"");
  _net.reset(new NeuralNet(nrH1,nrH2,nrH3,nrState,nrAction,pt,scale,feature));
  setActionCov(Vec::Ones(nrAction));
}
bool NeuralNetPolicy::read(istream& is,IOData* dat)
{
  readBinaryData(_deterministic,is);
  registerType<NeuralNet>(dat);
  registerType<DMPNeuralNet>(dat);
  readBinaryData(_net,is,dat);
  readBinaryData(_actionCov,is);
  return is.good();
}
bool NeuralNetPolicy::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_deterministic,os);
  registerType<NeuralNet>(dat);
  registerType<DMPNeuralNet>(dat);
  writeBinaryData(_net,os,dat);
  writeBinaryData(_actionCov,os);
  return os.good();
}
boost::shared_ptr<Serializable> NeuralNetPolicy::copy() const
{
  return boost::shared_ptr<Serializable>(new NeuralNetPolicy);
}
NeuralNetPolicy::Vec NeuralNetPolicy::getActionScale() const
{
  ScaleLayer* layer=_net->getLayerByName<ScaleLayer>("scale");
  if(layer)
    return layer->getScale();
  else return Vec::Constant(_net->nrOutput(),1E10);
}
Matd NeuralNetPolicy::getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples)
{
  //initialize
  _net->_tree.put<bool>("useMT",true);
  sizeType offWeight=nrNetParam();
  Matd diffMean,ret=_net->foreprop(&output);
  if(_deterministic)
    return ret;
  //action possibility
  if(possLog)
    possLog->setZero(output.cols());
  //gradient of action possibility
  if(possLogGrad && weightPoss) {
    diffMean=Matd::Zero(nrParam()-nrNetParam(),output.cols());
    possLogGrad->setZero(nrParam());
  }
  Vec COV=cov(),DCOV=dcov();
  //OMP_PARALLEL_FOR_
  for(sizeType j=0; j<output.cols(); j++)
    for(sizeType i=0; i<ret.rows(); i++) {
      if(COV[i] == 0)
        continue;
      //sample
      scalarD mean=ret(i,j);
      if(samples)
        ret(i,j)=(*samples)(i,j); //use the same sample set
      else ret(i,j)=proposeNormal(mean,COV[i]);
      //action possibility
      if(possLog)
        (*possLog)[j]+=logPossNormal(mean,COV[i],ret(i,j));
      //gradient of action possibility
      if(possLogGrad && weightPoss) {
        diffMean(i,j)+=DLogPossDMeanNormal(mean,COV[i],ret(i,j))*(*weightPoss)[j];
        OMP_ATOMIC_
        (*possLogGrad)[offWeight+i]+=DLogPossDCovNormal(mean,COV[i],ret(i,j))*DCOV[i]*(*weightPoss)[j];
      }
    }
  if(possLogGrad && weightPoss) {
    _net->backprop(&diffMean);
    possLogGrad->segment(0,nrNetParam())=_net->toVec(true);
  }
  _net->_tree.put<bool>("useMT",false);
  return ret;
}
boost::shared_ptr<KrylovMatrix<scalarD> > NeuralNetPolicy::getHessKLDistance(const Matd& states) const
{
  //compute new convariant
  sizeType nrA=nrAction();
  Vec covNew=cov(),dcovNew=dcov();
  //compute hessian
  Mat4d hessKL;
  const Matd& actionsNew=_net->foreprop(&states);
  boost::shared_ptr<HessianKLMatrix> ret(new HessianKLMatrix);
  Matd& hessActionsNew=ret->_hessMean=Matd::Zero(nrA,states.cols());
  Vec& hessCovNew=ret->_hessCov=Vec::Zero(nrA);
  ret->_nrParam=nrParam();
  ret->_net=_net;
  OMP_PARALLEL_FOR_I(OMP_PRI(hessKL))
  for(sizeType i=0; i<states.cols(); i++)
    for(sizeType a=0; a<nrA; a++) {
      hessKL=hessKLDistanceNormal(actionsNew(a,i),covNew[a],actionsNew(a,i),covNew[a]);
      hessActionsNew(a,i)=hessKL(2,2);
      OMP_ATOMIC_
      hessCovNew[a]+=hessKL(3,3)*pow(dcovNew[a],2);
    }
  hessActionsNew/=(scalarD)states.cols();
  hessCovNew/=(scalarD)states.cols();
  //return
  return boost::dynamic_pointer_cast<KrylovMatrix<scalarD> >(ret);
}
scalarD NeuralNetPolicy::getDiffKLDistance(const Matd& states,const Matd& actionsOld,const Vec& covOld,Vec* grad) const
{
  //compute new convariant
  scalarD ret=0;
  sizeType nrA=nrAction();
  Vec covNew=cov(),dcovNew=dcov();
  _net->_tree.put<bool>("useMT",true);
  //compute gradient
  Vec4d diffKL;
  sizeType offNetParam=nrNetParam();
  if(grad)*grad=Vec::Zero(nrParam());
  Matd& actionsNew=_net->foreprop(&states);
  Matd diffActionsNew=actionsNew;
  OMP_PARALLEL_FOR_I(OMP_PRI(diffKL) OMP_ADD(ret))
  for(sizeType i=0; i<states.cols(); i++)
    for(sizeType a=0; a<nrA; a++) {
      ret+=KLDistanceNormal(actionsOld(a,i),covOld[a],actionsNew(a,i),covNew[a]);
      diffKL=diffKLDistanceNormal(actionsOld(a,i),covOld[a],actionsNew(a,i),covNew[a]);
      if(grad) {
        diffActionsNew(a,i)=diffKL[2];
        OMP_ATOMIC_
        (*grad)[a+offNetParam]+=diffKL[3]*dcovNew[a];
      }
    }
  //assemble
  if(grad) {
    _net->backprop(&diffActionsNew);
    grad->segment(0,offNetParam)=_net->toVec(true);
    *grad/=(scalarD)states.cols();
  }
  _net->_tree.put<bool>("useMT",false);
  return ret/(scalarD)states.cols();
}
scalarD NeuralNetPolicy::getDiffKLDistance(const Matd& states,const Vec& paramOld,Vec* grad)
{
  _net->_tree.put<bool>("useMT",true);
  Vec param=toVec();
  fromVec(paramOld);
  const Matd actionsOld=_net->foreprop(&states);
  const Vec covOld=cov();
  _net->_tree.put<bool>("useMT",false);

  fromVec(param);
  return getDiffKLDistance(states,actionsOld,covOld,grad);
}
void NeuralNetPolicy::setActionCov(scalar coef,scalar confidence)
{
  ScaleLayer* layer=_net->getLayerByName<ScaleLayer>("scale");
  Vec COV=cov();
  if(layer)
    COV=layer->getScale()*coef;
  else COV.setConstant(nrAction(),coef);
  for(sizeType i=0; i<COV.size(); i++)
    COV[i]=confidenceCovNormal(COV[i],confidence);
  setActionCov(COV);
  cout << "Covariance Wanted: " << COV.transpose() << endl;
  cout << "Covariance Set: " << cov().transpose() << endl;
}
void NeuralNetPolicy::setActionCov(const Vec& cov)
{
  _actionCov=cov;
}
void NeuralNetPolicy::setAug(const Vec& aug)
{
  for(sizeType i=0; i<(sizeType)aug.size(); i++)
    _net->_tree.put<scalarD>("augment"+boost::lexical_cast<string>(i),aug[i]);
  _net->resetOnline();
  _aug=aug;
}
NeuralNetPolicy::Vec NeuralNetPolicy::getAug() const
{
  return _aug;
}
sizeType NeuralNetPolicy::nrNetParam() const
{
  return _net->nrParam();
}
sizeType NeuralNetPolicy::nrParam() const
{
  return nrNetParam()+_actionCov.size();
}
sizeType NeuralNetPolicy::nrAction() const
{
  return _net->nrOutput();
}
NeuralNetPolicy::Vec NeuralNetPolicy::toVec() const
{
  Vec ret=Vec::Zero(nrParam());
  ret.segment(0,nrNetParam())=_net->toVec(false);
  ret.segment(nrNetParam(),_actionCov.size())=_actionCov;
  return ret;
}
void NeuralNetPolicy::fromVec(const Vec& weights)
{
  _net->fromVec(weights.segment(0,nrNetParam()));
  _actionCov=weights.segment(nrNetParam(),_actionCov.size());
  if(_actionCov.minCoeff() < 1E-2f) {
    WARNING("Negative covariance!")
    _actionCov.array()=_actionCov.array().exp();
    const_cast<Vec&>(weights).segment(nrNetParam(),_actionCov.size()).array()=_actionCov.array();
  }
}
//feature transformation
sizeType NeuralNetPolicy::nrState() const
{
  return _net->nrInput();
}
NeuralNetPolicy::Vec NeuralNetPolicy::cov() const
{
  Vec ret=Vec::Zero(nrAction());
  for(sizeType i=0; i<ret.size(); i++)
    ret[i]=cov(i);
  return ret;
}
NeuralNetPolicy::Vec NeuralNetPolicy::dcov() const
{
  Vec ret=Vec::Zero(nrAction());
  for(sizeType i=0; i<ret.size(); i++)
    ret[i]=dcov(i);
  return ret;
}
scalarD NeuralNetPolicy::cov(sizeType i) const
{
  return _actionCov[i];
}
scalarD NeuralNetPolicy::dcov(sizeType i) const
{
  return 1;
}
NeuralNetPolicy::NeuralNetPolicy(const string& name):Policy(name) {}

//ensure positive covariance
NeuralNetPolicyPositiveCov::NeuralNetPolicyPositiveCov()
  :NeuralNetPolicy(typeid(NeuralNetPolicyPositiveCov).name()) {}
NeuralNetPolicyPositiveCov::NeuralNetPolicyPositiveCov
(sizeType nrAction,sizeType nrBasis,
 boost::property_tree::ptree* pt)
  :NeuralNetPolicy(typeid(NeuralNetPolicyPositiveCov).name())
{
  _net.reset(new DMPNeuralNet(nrAction,nrBasis,pt));
  setActionCov(Vec::Ones(nrAction));
}
NeuralNetPolicyPositiveCov::NeuralNetPolicyPositiveCov
(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,sizeType nrAction,const Vec* scale,
 boost::property_tree::ptree* pt,boost::shared_ptr<FeatureTransformLayer> feature)
  :NeuralNetPolicy(typeid(NeuralNetPolicyPositiveCov).name())
{
  ASSERT_MSG(pt,"You must provide property tree for NeuralNetPolicy!")
  pt->put<scalar>("HFParam.initWeightNorm",0.001f);
  pt->put<scalar>("HFParam.initBiasNorm",0.001f);
  ASSERT(!scale || scale->size() == nrAction)
  pt->put<string>("activationFinal",scale?"TanH":"");
  _net.reset(new NeuralNet(nrH1,nrH2,nrH3,nrState,nrAction,pt,scale,feature));
  setActionCov(Vec::Ones(nrAction));
}
boost::shared_ptr<Serializable> NeuralNetPolicyPositiveCov::copy() const
{
  return boost::shared_ptr<Serializable>(new NeuralNetPolicyPositiveCov);
}
void NeuralNetPolicyPositiveCov::setActionCov(const Vec& cov)
{
  //actionCov0
  _actionCov0=cov;
  _actionCov=cov;
  _actionCov.setConstant(-2);
}
void NeuralNetPolicyPositiveCov::fromVec(const Vec& weights)
{
  _net->fromVec(weights.segment(0,nrNetParam()));
  _actionCov=weights.segment(nrNetParam(),_actionCov.size());
  //_actionCov.cwiseMax(1E-2f); //we don't need to ensure it is zero now
}
scalarD NeuralNetPolicyPositiveCov::cov(sizeType i) const
{
  return _actionCov0[i]/(1+exp(_actionCov[i]));
}
scalarD NeuralNetPolicyPositiveCov::dcov(sizeType i) const
{
  scalarD expCov=exp(_actionCov[i]);
  return -_actionCov0[i]*expCov/pow(1+expCov,2);
}
NeuralNetPolicyPositiveCov::NeuralNetPolicyPositiveCov(const string& name):NeuralNetPolicy(name) {}

PRJ_END
