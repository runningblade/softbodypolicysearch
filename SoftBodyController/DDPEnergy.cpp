#include "DDPEnergy.h"
#include <Dynamics/FEMRotationUtil.h>
#include <Dynamics/FEMMeshFormat.h>
#include <CommonFile/GridOp.h>
#include <CommonFile/MarchingCube3D.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

void clearDim(Mat3d J[3],Mat3d JJ[3][3])
{
  if(J)
    J[0]=J[1]=Mat3d::Zero();
  if(JJ)
    JJ[0][0]=JJ[0][1]=JJ[0][2]=JJ[1][0]=JJ[1][1]=JJ[1][2]=JJ[2][0]=JJ[2][1]=Mat3d::Zero();
}
//DDPEnergy
DDPEnergy::DDPEnergy():Serializable(typeid(DDPEnergy).name()),_mtFeatId(-1)
{
  _coef=0;
}
bool DDPEnergy::read(istream& is,IOData* dat)
{
  readBinaryData(_coef,is);
  readBinaryData(_mtFeatId,is);
  return is.good();
}
bool DDPEnergy::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_coef,os);
  writeBinaryData(_mtFeatId,os);
  return os.good();
}
scalarD DDPEnergy::trajCost(scalar dt,const Matd& x) const
{
  scalarD cost=0;
  for(sizeType i=0; i<x.cols(); i++)
    cost+=objX(i,dt,x.col(i));
  return cost;
}
scalarD DDPEnergy::objX(sizeType i,scalar dt,const Vec& x) const
{
  return objX(i,dt,x,NULL,NULL);
}
scalarD DDPEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec& fx) const
{
  return objX(i,dt,x,&fx,NULL);
}
scalarD DDPEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec& fx,Matd& fxx) const
{
  return objX(i,dt,x,&fx,&fxx);
}
void DDPEnergy::registerType(IOData* dat)
{
  COMMON::registerType<DDPBalanceEnergy>(dat);
  COMMON::registerType<DDPJumpEnergy>(dat);
  COMMON::registerType<DDPWalkEnergy>(dat);
  COMMON::registerType<DDPWalkToEnergy>(dat);
  COMMON::registerType<DDPWalkSpeedToEnergy>(dat);
  COMMON::registerType<DDP2DRollEnergy>(dat);
  COMMON::registerType<DDP3DRollEnergy>(dat);
  COMMON::registerType<DDP3DOrientationEnergy>(dat);
  COMMON::registerType<DDP3DOrientationToEnergy>(dat);
  COMMON::registerType<DDPCurvedWalkEnergy>(dat);
  //reinforcement learning energy
  COMMON::registerType<GoalEnergy>(dat);
  COMMON::registerType<DirectedSpeedEnergy>(dat);
  COMMON::registerType<ObstacleEnergy>(dat);
}
//multitask augmented feature
void DDPEnergy::setMTAug(const Vec& aug,sizeType off) {}
void DDPEnergy::getMTAug(Vec& aug,sizeType off) const {}
sizeType DDPEnergy::nrMTAug() const
{
  return 0;
}
void DDPEnergy::setMTId(sizeType id)
{
  _mtFeatId=id;
}
DDPEnergy::DDPEnergy(const string& path):Serializable(path) {}
//DDPBalanceEnergy
DDPBalanceEnergy::DDPBalanceEnergy():DDPEnergy(typeid(DDPBalanceEnergy).name()) {}
DDPBalanceEnergy::DDPBalanceEnergy(const Vec3d& dirL,const Vec3d& dirG,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDPBalanceEnergy).name()),_dirL(dirL),_dirG(dirG),_dim(dim)
{
  _dirL.normalize();
  _dirG.normalize();
  _coef=coef;
}
scalarD DDPBalanceEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  sizeType off=x.size()-3;
  Mat3d DRDW[3],DDRDDW[3][3];
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off,3),DRDW,DDRDDW,NULL,NULL);
  Vec3d diff=R*_dirL-_dirG;
  if(_dim == 2)
    clearDim(DRDW,DDRDDW);
  if(fx)
    for(sizeType r=0; r<3; r++)
      (*fx)[off+r]+=(DRDW[r]*_dirL).dot(diff)*_coef;
  if(fxx)
    for(sizeType r=0; r<3; r++)
      for(sizeType c=0; c<3; c++) {
        (*fxx)(off+r,off+c)+=(DRDW[r]*_dirL).dot(DRDW[c]*_dirL)*_coef;
        (*fxx)(off+r,off+c)+=diff.dot(DDRDDW[r][c]*_dirL)*_coef;
      }
  return diff.squaredNorm()*_coef/2;
}
void DDPBalanceEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  sizeType off=x.size()-3;
  Mat3d DRDW[3];
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off,3),DRDW,NULL,NULL,NULL);
  Vec3d diff=R*_dirL-_dirG;
  if(_dim == 2)
    clearDim(DRDW,NULL);
  scalarD scoef=sqrt(_coef);
  f.segment(offf,3)=diff*scoef;
  if(fjac)
    for(sizeType r=0; r<3; r++)
      fjac->block(offf,off+r,3,1)=(DRDW[r]*_dirL)*scoef;
}
sizeType DDPBalanceEnergy::nrF() const
{
  return 3;
}
bool DDPBalanceEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_dirL,is);
  readBinaryData(_dirG,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDPBalanceEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_dirL,os);
  writeBinaryData(_dirG,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDPBalanceEnergy::copy() const
{
  boost::shared_ptr<DDPBalanceEnergy> ret(new DDPBalanceEnergy);
  *ret=*this;
  return ret;
}
//DDPJumpEnergy
DDPJumpEnergy::DDPJumpEnergy():DDPEnergy(typeid(DDPJumpEnergy).name()) {}
DDPJumpEnergy::DDPJumpEnergy(const Vec3& gravity,scalarD height,scalarD frame,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDPJumpEnergy).name()),_height(height),_frame(frame),_dim(dim)
{
  _dir=-gravity.normalized().cast<scalarD>();
  _coef=coef;
}
scalarD DDPJumpEnergy::trajCost(scalar dt,const Matd& x) const
{
  sizeType off2=x.rows()-6;
  scalarD minVal=ScalarUtil<scalarD>::scalar_max;
  for(sizeType i=0; i<x.cols(); i++) {
    scalarD deltaH=x.col(i).segment(off2,3).dot(_dir)-_height;
    minVal=min<scalarD>(minVal,deltaH*deltaH);
  }
  return minVal*_coef/2;
}
scalarD DDPJumpEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  scalarD currTime=(scalarD)i*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return 0;
  sizeType off2=x.size()-6;
  scalarD height=x.segment(off2,3).dot(_dir);
  if(fx)
    fx->segment(off2,3)+=_dir*(height-_height)*_coef;
  if(fxx)
    fxx->block(off2,off2,3,3)+=_dir*_dir.transpose()*_coef;
  return pow(height-_height,2)*_coef/2;
}
void DDPJumpEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  scalarD currTime=(scalarD)i*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return;
  sizeType off2=x.size()-6;
  scalarD height=x.segment(off2,3).dot(_dir);
  scalarD scoef=sqrt(_coef);
  f[offf]=(height-_height)*scoef;
  if(fjac)
    fjac->block<1,3>(offf,off2)=_dir.transpose()*scoef;
}
sizeType DDPJumpEnergy::nrF() const
{
  return 1;
}
bool DDPJumpEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_dir,is);
  readBinaryData(_height,is);
  readBinaryData(_frame,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDPJumpEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_dir,os);
  writeBinaryData(_height,os);
  writeBinaryData(_frame,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDPJumpEnergy::copy() const
{
  boost::shared_ptr<DDPJumpEnergy> ret(new DDPJumpEnergy);
  *ret=*this;
  return ret;
}
//multitask augmented feature
void DDPJumpEnergy::setMTAug(const Vec& aug,sizeType off)
{
  if(_mtFeatId == 0)
    _height=aug[off];
  else DDPEnergy::setMTAug(aug,off);
}
void DDPJumpEnergy::getMTAug(Vec& aug,sizeType off) const
{
  if(_mtFeatId == 0)
    aug[off]=_height;
  else return DDPEnergy::getMTAug(aug,off);
}
sizeType DDPJumpEnergy::nrMTAug() const
{
  if(_mtFeatId == 0)
    return 1;
  else return DDPEnergy::nrMTAug();
}
//DDPWalkEnergy
DDPWalkEnergy::DDPWalkEnergy():DDPEnergy(typeid(DDPWalkEnergy).name()) {}
DDPWalkEnergy::DDPWalkEnergy(const Vec3d& spd,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDPWalkEnergy).name()),_spd(spd),_dim(dim)
{
  _coef=coef;
  _spd0=_spd.normalized();
}
scalarD DDPWalkEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  sizeType off1=x.size()/2-6,off2=x.size()-6;
  Vec3d diff=(x.segment(off2,3)-x.segment(off1,3))/dt-_spd;
  if(fx) {
    fx->segment(off1,_dim)-=diff.segment(0,_dim)*_coef/dt;
    fx->segment(off2,_dim)+=diff.segment(0,_dim)*_coef/dt;
  }
  if(fxx) {
    fxx->block(off1,off1,_dim,_dim)+=Matd::Identity(_dim,_dim)*_coef/(dt*dt);
    fxx->block(off2,off2,_dim,_dim)+=Matd::Identity(_dim,_dim)*_coef/(dt*dt);
    fxx->block(off1,off2,_dim,_dim)-=Matd::Identity(_dim,_dim)*_coef/(dt*dt);
    fxx->block(off2,off1,_dim,_dim)-=Matd::Identity(_dim,_dim)*_coef/(dt*dt);
  }
  return diff.squaredNorm()*_coef/2;
}
void DDPWalkEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  sizeType off1=x.size()/2-6,off2=x.size()-6;
  Vec3d diff=(x.segment(off2,3)-x.segment(off1,3))/dt-_spd;
  scalarD scoef=sqrt(_coef);

  f.segment(offf,_dim)=diff.segment(0,_dim)*scoef;
  if(fjac) {
    fjac->block(offf,off1,_dim,_dim).diagonal().setConstant(-scoef/dt);
    fjac->block(offf,off2,_dim,_dim).diagonal().setConstant( scoef/dt);
  }
}
sizeType DDPWalkEnergy::nrF() const
{
  return _dim;
}
bool DDPWalkEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_spd,is);
  readBinaryData(_spd0,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDPWalkEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_spd,os);
  writeBinaryData(_spd0,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDPWalkEnergy::copy() const
{
  boost::shared_ptr<DDPWalkEnergy> ret(new DDPWalkEnergy);
  *ret=*this;
  return ret;
}
//multitask augmented feature
void DDPWalkEnergy::setMTAug(const Vec& aug,sizeType off)
{
  if(_mtFeatId == 0)
    _spd=aug[off]*_spd0;
  else if(_mtFeatId == 1)
    _spd=aug.segment<3>(off);
  else DDPEnergy::setMTAug(aug,off);
}
void DDPWalkEnergy::getMTAug(Vec& aug,sizeType off) const
{
  if(_mtFeatId == 0)
    aug[off]=_spd.dot(_spd0);
  else if(_mtFeatId == 1)
    aug.segment<3>(off)=_spd;
  else DDPEnergy::getMTAug(aug,off);
}
sizeType DDPWalkEnergy::nrMTAug() const
{
  if(_mtFeatId == 0)
    return 1;
  else if(_mtFeatId == 1)
    return 3;
  else return DDPEnergy::nrMTAug();
}
//DDPWalkToEnergy
DDPWalkToEnergy::DDPWalkToEnergy():DDPEnergy(typeid(DDPWalkToEnergy).name()) {}
DDPWalkToEnergy::DDPWalkToEnergy(const Vec3d& pos,const Vec3d* gT,const Vec3d* gN,scalarD frm,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDPWalkToEnergy).name()),_pos(pos),_frame(frm),_dim(dim)
{
  _coef=coef;
  if(gT && gT->norm() > 1E-10f) {
    Vec3d N=gT->normalized();
    _PROJ=Mat3d::Identity()-N*N.transpose();
  } else if(gN && gN->norm() > 1E-10f) {
    Vec3d N=gN->normalized();
    _PROJ=N*N.transpose();
  } else {
    _PROJ.setIdentity();
  }
  //cout << _PROJ << endl;
}
scalarD DDPWalkToEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  scalarD currTime=(scalarD)(i+1)*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return 0;
  sizeType off2=x.size()-6;
  Vec3d diff=_PROJ*(x.segment(off2,3)-_pos);
  if(fx)
    fx->segment(off2,3)+=diff*_coef;
  if(fxx)
    fxx->block(off2,off2,3,3)+=_PROJ*_coef;
  return diff.squaredNorm()*_coef/2;
}
void DDPWalkToEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  scalarD currTime=(scalarD)(i+1)*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return;
  sizeType off2=x.size()-6;
  Vec3d diff=_PROJ*(x.segment(off2,3)-_pos);
  scalarD scoef=sqrt(_coef);
  f.segment(offf,_dim)=diff.segment(0,_dim)*scoef;
  if(fjac)
    fjac->block(offf,off2,_dim,_dim)=_PROJ.block(0,0,_dim,_dim)*scoef;
}
sizeType DDPWalkToEnergy::nrF() const
{
  return 3;
}
void DDPWalkToEnergy::setPos(const Vec3d& pos)
{
  _pos=pos;
}
bool DDPWalkToEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_pos,is);
  readBinaryData(_PROJ,is);
  readBinaryData(_frame,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDPWalkToEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_pos,os);
  writeBinaryData(_PROJ,os);
  writeBinaryData(_frame,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDPWalkToEnergy::copy() const
{
  boost::shared_ptr<DDPWalkToEnergy> ret(new DDPWalkToEnergy);
  *ret=*this;
  return ret;
}
//DDPWalkSpeedToEnergy
DDPWalkSpeedToEnergy::DDPWalkSpeedToEnergy():DDPEnergy(typeid(DDPWalkSpeedToEnergy).name()) {}
DDPWalkSpeedToEnergy::DDPWalkSpeedToEnergy(const Vec3d& spd,const Vec3d* gT,const Vec3d* gN,scalarD frm,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDPWalkSpeedToEnergy).name()),_spd(spd),_frame(frm),_dim(dim)
{
  _coef=coef;
  if(gT && gT->norm() > 1E-10f) {
    Vec3d N=gT->normalized();
    _PROJ=Mat3d::Identity()-N*N.transpose();
  } else if(gN && gN->norm() > 1E-10f) {
    Vec3d N=gN->normalized();
    _PROJ=N*N.transpose();
  } else {
    _PROJ.setIdentity();
  }
  //cout << _PROJ << endl;
}
scalarD DDPWalkSpeedToEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  scalarD currTime=(scalarD)(i+1)*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return 0;
  sizeType off1=x.size()/2-6,off2=x.size()-6;
  Vec3d diff=_PROJ*((x.segment(off2,3)-x.segment(off1,3))/dt-_spd);
  if(fx) {
    fx->segment(off1,_dim)-=diff.segment(0,_dim)*_coef/dt;
    fx->segment(off2,_dim)+=diff.segment(0,_dim)*_coef/dt;
  }
  if(fxx) {
    fxx->block(off1,off1,_dim,_dim)+=_PROJ.block(0,0,_dim,_dim)*_coef/(dt*dt);
    fxx->block(off2,off2,_dim,_dim)+=_PROJ.block(0,0,_dim,_dim)*_coef/(dt*dt);
    fxx->block(off1,off2,_dim,_dim)-=_PROJ.block(0,0,_dim,_dim)*_coef/(dt*dt);
    fxx->block(off2,off1,_dim,_dim)-=_PROJ.block(0,0,_dim,_dim)*_coef/(dt*dt);
  }
  return diff.squaredNorm()*_coef/2;
}
void DDPWalkSpeedToEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  scalarD currTime=(scalarD)(i+1)*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return;
  sizeType off1=x.size()/2-6,off2=x.size()-6;
  Vec3d diff=_PROJ*((x.segment(off2,3)-x.segment(off1,3))/dt-_spd);
  scalarD scoef=sqrt(_coef);

  f.segment(offf,_dim)=diff.segment(0,_dim)*scoef;
  if(fjac) {
    fjac->block(offf,off1,_dim,_dim)=_PROJ.block(0,0,_dim,_dim)*(-scoef/dt);
    fjac->block(offf,off2,_dim,_dim)=_PROJ.block(0,0,_dim,_dim)*( scoef/dt);
  }
}
sizeType DDPWalkSpeedToEnergy::nrF() const
{
  return _dim;
}
bool DDPWalkSpeedToEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_spd,is);
  readBinaryData(_PROJ,is);
  readBinaryData(_frame,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDPWalkSpeedToEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_spd,os);
  writeBinaryData(_PROJ,os);
  writeBinaryData(_frame,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDPWalkSpeedToEnergy::copy() const
{
  boost::shared_ptr<DDPWalkSpeedToEnergy> ret(new DDPWalkSpeedToEnergy);
  *ret=*this;
  return ret;
}
//DDP2DRollEnergy
DDP2DRollEnergy::DDP2DRollEnergy():DDPEnergy(typeid(DDP2DRollEnergy).name()) {}
DDP2DRollEnergy::DDP2DRollEnergy(scalarD spd,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDP2DRollEnergy).name()),_spd(spd),_dim(dim)
{
  ASSERT(_dim == 2)
  _coef=coef;
}
scalarD DDP2DRollEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  sizeType off1=x.size()/2-1,off2=x.size()-1;
  if(fx) {
    (*fx)[off1]-=((x[off2]-x[off1])/dt-_spd)*_coef/dt;
    (*fx)[off2]+=((x[off2]-x[off1])/dt-_spd)*_coef/dt;
  }
  if(fxx) {
    (*fxx)(off1,off1)+=_coef/dt/dt;
    (*fxx)(off2,off2)+=_coef/dt/dt;
    (*fxx)(off1,off2)-=_coef/dt/dt;
    (*fxx)(off2,off1)-=_coef/dt/dt;
  }
  return pow((x[off2]-x[off1])/dt-_spd,2)*_coef/2;
}
void DDP2DRollEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  sizeType off1=x.size()/2-1,off2=x.size()-1;
  f[offf]=((x[off2]-x[off1])/dt-_spd)*sqrt(_coef);
  if(fjac) {
    (*fjac)(offf,off2)= sqrt(_coef)/dt;
    (*fjac)(offf,off1)=-sqrt(_coef)/dt;
  }
}
sizeType DDP2DRollEnergy::nrF() const
{
  return 1;
}
bool DDP2DRollEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_spd,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDP2DRollEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_spd,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDP2DRollEnergy::copy() const
{
  boost::shared_ptr<DDP2DRollEnergy> ret(new DDP2DRollEnergy);
  *ret=*this;
  return ret;
}
//DDP3DRollEnergy
DDP3DRollEnergy::DDP3DRollEnergy():DDPEnergy(typeid(DDP3DRollEnergy).name()) {}
DDP3DRollEnergy::DDP3DRollEnergy(const Vec3d& spd,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDP3DRollEnergy).name()),_spd(spd),_dim(dim)
{
  ASSERT(_dim == 3)
  _coef=coef;
}
scalarD DDP3DRollEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  sizeType off1=x.size()/2-3,off2=x.size()-3;
  if(fx) {
    fx->segment<3>(off1)-=((x.segment<3>(off2)-x.segment<3>(off1))/dt-_spd)*_coef/dt;
    fx->segment<3>(off2)+=((x.segment<3>(off2)-x.segment<3>(off1))/dt-_spd)*_coef/dt;
  }
  if(fxx) {
    fxx->block<3,3>(off1,off1)+=Mat3d::Identity()*_coef/dt/dt;
    fxx->block<3,3>(off2,off2)+=Mat3d::Identity()*_coef/dt/dt;
    fxx->block<3,3>(off1,off2)-=Mat3d::Identity()*_coef/dt/dt;
    fxx->block<3,3>(off2,off1)-=Mat3d::Identity()*_coef/dt/dt;
  }
  return ((x.segment<3>(off2)-x.segment<3>(off1))/dt-_spd).squaredNorm()*_coef/2;
}
void DDP3DRollEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  sizeType off1=x.size()/2-3,off2=x.size()-3;
  f.segment<3>(offf)=((x.segment<3>(off2)-x.segment<3>(off1))/dt-_spd)*sqrt(_coef);
  if(fjac) {
    fjac->block<3,3>(offf,off2)=Mat3d::Identity()* sqrt(_coef)/dt;
    fjac->block<3,3>(offf,off1)=Mat3d::Identity()*-sqrt(_coef)/dt;
  }
}
sizeType DDP3DRollEnergy::nrF() const
{
  return 3;
}
bool DDP3DRollEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_spd,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDP3DRollEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_spd,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDP3DRollEnergy::copy() const
{
  boost::shared_ptr<DDP3DRollEnergy> ret(new DDP3DRollEnergy);
  *ret=*this;
  return ret;
}
//DDP3DOrientationEnergy
DDP3DOrientationEnergy::DDP3DOrientationEnergy():DDPEnergy(typeid(DDP3DOrientationEnergy).name()) {}
DDP3DOrientationEnergy::DDP3DOrientationEnergy(const Vec3d& dirL,const Vec3d& rot,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDP3DOrientationEnergy).name()),_dirL(dirL),_rot(rot),_dim(dim)
{
  _coef=coef;
  _rot0=_rot.normalized();
}
scalarD DDP3DOrientationEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  sizeType off1=x.size()/2-3,off2=x.size()-3;
  Mat3d DRDWL[3],DDRDDWL[3][3],DRDW[3],DDRDDW[3][3];
  Mat3d rot=makeRotation<scalarD>(_rot*dt);
  Mat3d RL=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off1,3),DRDWL,DDRDDWL,NULL,NULL);
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off2,3),DRDW,DDRDDW,NULL,NULL);
  Vec3d diff=R*_dirL-rot*(RL*_dirL);
  if(_dim == 2) {
    clearDim(DRDW,DDRDDW);
    clearDim(DRDWL,DDRDDWL);
  }
  if(fx)
    for(sizeType r=0; r<3; r++) {
      (*fx)[off2+r]+=(DRDW[r]*_dirL).dot(diff)*_coef;
      (*fx)[off1+r]-=(rot*(DRDWL[r]*_dirL)).dot(diff)*_coef;
    }
  if(fxx)
    for(sizeType r=0; r<3; r++)
      for(sizeType c=0; c<3; c++) {
        (*fxx)(off2+r,off2+c)+=(DRDW[r]*_dirL).dot(DRDW[c]*_dirL)*_coef;
        (*fxx)(off2+r,off2+c)+=diff.dot(DDRDDW[r][c]*_dirL)*_coef;

        (*fxx)(off2+r,off1+c)-=(DRDW[r]*_dirL).dot(rot*(DRDWL[c]*_dirL))*_coef;
        (*fxx)(off1+r,off2+c)-=(rot*(DRDWL[r]*_dirL)).dot(DRDW[c]*_dirL)*_coef;

        (*fxx)(off1+r,off1+c)+=(rot*(DRDWL[r]*_dirL)).dot(rot*(DRDWL[c]*_dirL))*_coef;
        (*fxx)(off1+r,off1+c)-=diff.dot(rot*(DDRDDWL[r][c]*_dirL))*_coef;
      }
  return diff.squaredNorm()*_coef/2;
}
void DDP3DOrientationEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  sizeType off1=x.size()/2-3,off2=x.size()-3;
  Mat3d DRDW[3],DRDWL[3];
  Mat3d rot=makeRotation<scalarD>(_rot*dt);
  Mat3d RL=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off1,3),DRDWL,NULL,NULL,NULL);
  Mat3d R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off2,3),DRDW,NULL,NULL,NULL);
  Vec3d diff=R*_dirL-rot*(RL*_dirL);
  if(_dim == 2) {
    clearDim(DRDW,NULL);
    clearDim(DRDWL,NULL);
  }
  scalarD scoef=sqrt(_coef);
  f.segment(offf,3)=diff*scoef;
  if(fjac)
    for(sizeType r=0; r<3; r++) {
      fjac->block(offf,off2+r,3,1)=(DRDW[r]*_dirL)*scoef;
      fjac->block(offf,off1+r,3,1)=-rot*(DRDWL[r]*_dirL)*scoef;
    }
}
sizeType DDP3DOrientationEnergy::nrF() const
{
  return 3;
}
bool DDP3DOrientationEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_dirL,is);
  readBinaryData(_rot,is);
  readBinaryData(_rot0,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDP3DOrientationEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_dirL,os);
  writeBinaryData(_rot,os);
  writeBinaryData(_rot0,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDP3DOrientationEnergy::copy() const
{
  boost::shared_ptr<DDP3DOrientationEnergy> ret(new DDP3DOrientationEnergy);
  *ret=*this;
  return ret;
}
//multitask augmented feature
void DDP3DOrientationEnergy::setMTAug(const Vec& aug,sizeType off)
{
  if(_mtFeatId == 0)
    _rot=aug[off+0]*_rot0;
  else if(_mtFeatId == 1)
    _rot=aug.segment<3>(off);
  else DDPEnergy::setMTAug(aug,off);
}
void DDP3DOrientationEnergy::getMTAug(Vec& aug,sizeType off) const
{
  if(_mtFeatId == 0)
    aug[off+0]=_rot.dot(_rot0);
  else if(_mtFeatId == 1)
    aug.segment<3>(off)=_rot;
  else DDPEnergy::getMTAug(aug,off);
}
sizeType DDP3DOrientationEnergy::nrMTAug() const
{
  if(_mtFeatId == 0)
    return 1;
  else if(_mtFeatId == 1)
    return 3;
  else return DDPEnergy::nrMTAug();
}
//DDP3DOrientationToEnergy
DDP3DOrientationToEnergy::DDP3DOrientationToEnergy():DDPEnergy(typeid(DDP3DOrientationToEnergy).name()) {}
DDP3DOrientationToEnergy::DDP3DOrientationToEnergy(const Vec3d& dirL,scalarD frm,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDP3DOrientationToEnergy).name()),_dirL(dirL),_frame(frm),_dim(dim)
{
  _coef=coef;
  _PROJ.setIdentity();
  if(_dim == 2)
    _PROJ(0,0)=_PROJ(1,1)=0;
}
scalarD DDP3DOrientationToEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  scalarD currTime=(scalarD)(i+1)*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return 0;
  sizeType off2=x.size()-3;
  Vec3d diff=_PROJ*(x.segment(off2,3)-_dirL);
  if(fx)
    fx->segment(off2,3)+=diff*_coef;
  if(fxx)
    fxx->block(off2,off2,3,3)+=_PROJ*_coef;
  return diff.squaredNorm()*_coef/2;
}
void DDP3DOrientationToEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  scalarD currTime=(scalarD)(i+1)*dt;
  scalarD lastTime=currTime-dt;
  if(_frame >= 0 && (currTime-_frame)*(lastTime-_frame) > 0)
    return;
  sizeType off2=x.size()-3;
  Vec3d diff=_PROJ*(x.segment(off2,3)-_dirL);
  scalarD scoef=sqrt(_coef);
  f.segment(offf,3)=diff*scoef;
  if(fjac)
    fjac->block(offf,off2,3,3)=_PROJ*scoef;
}
sizeType DDP3DOrientationToEnergy::nrF() const
{
  return 3;
}
bool DDP3DOrientationToEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_dirL,is);
  readBinaryData(_PROJ,is);
  readBinaryData(_frame,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDP3DOrientationToEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_dirL,os);
  writeBinaryData(_PROJ,os);
  writeBinaryData(_frame,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDP3DOrientationToEnergy::copy() const
{
  return boost::shared_ptr<Serializable>(new DDP3DOrientationToEnergy);
}
//DDPCurvedWalkEnergy
DDPCurvedWalkEnergy::DDPCurvedWalkEnergy():DDPEnergy(typeid(DDPCurvedWalkEnergy).name()) {}
DDPCurvedWalkEnergy::DDPCurvedWalkEnergy(const Vec3d& dirL,const Vec3d& rot,scalarD coef,sizeType dim)
  :DDPEnergy(typeid(DDPCurvedWalkEnergy).name()),_dirL(dirL),_rot(rot),_dim(dim)
{
  _coef=coef;
  //if(_dim == 2) {
  //  _dirL[2]=0;
  //  _rot[0]=_rot[1]=0;
  //}
  _dirL0=_dirL.normalized();
  _rot0=_rot.normalized();
}
scalarD DDPCurvedWalkEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  sizeType off1=x.size()/2-6,off2=x.size()-6;
  Mat3d DRDWL[3],DDRDDWL[3][3];
  Vec3d pos=x.segment(off2,3);
  Vec3d posL=x.segment(off1,3);
  Mat3d rot=makeRotation<scalarD>(_rot*dt);
  Mat3d RL=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off1+3,3),DRDWL,DDRDDWL,NULL,NULL);
  Vec3d diff=(pos-posL)/dt-rot*(RL*_dirL);
  if(_dim == 2)
    clearDim(DRDWL,DDRDDWL);
  if(fx) {
    fx->segment(off2,3)+=diff*_coef/dt;
    fx->segment(off1,3)-=diff*_coef/dt;
    for(sizeType r=0; r<3; r++)
      (*fx)[off1+3+r]-=(rot*(DRDWL[r]*_dirL)).dot(diff)*_coef;
  }
  if(fxx) {
    fxx->block(off1,off1,3,3)+=Mat3d::Identity()*_coef/(dt*dt);
    fxx->block(off2,off2,3,3)+=Mat3d::Identity()*_coef/(dt*dt);
    fxx->block(off1,off2,3,3)-=Mat3d::Identity()*_coef/(dt*dt);
    fxx->block(off2,off1,3,3)-=Mat3d::Identity()*_coef/(dt*dt);
    for(sizeType r=0; r<3; r++) {
      Vec3d tmp=(rot*(DRDWL[r]*_dirL))*_coef;
      fxx->block<1,3>(off1+3+r,off1)+=tmp.transpose()/dt;
      fxx->block<1,3>(off1+3+r,off2)-=tmp.transpose()/dt;
      fxx->block<3,1>(off1,off1+3+r)+=tmp/dt;
      fxx->block<3,1>(off2,off1+3+r)-=tmp/dt;
      for(sizeType c=0; c<3; c++) {
        (*fxx)(off1+3+r,off1+3+c)+=tmp.dot(rot*(DRDWL[c]*_dirL));
        (*fxx)(off1+3+r,off1+3+c)-=(rot*(DDRDDWL[r][c]*_dirL)).dot(diff)*_coef;
      }
    }
  }
  return diff.squaredNorm()*_coef/2;
}
void DDPCurvedWalkEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  sizeType off1=x.size()/2-6,off2=x.size()-6;
  Mat3d DRDWL[3];
  Vec3d pos=x.segment(off2,3);
  Vec3d posL=x.segment(off1,3);
  Mat3d rot=makeRotation<scalarD>(_rot*dt);
  Mat3d RL=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment(off1+3,3),DRDWL,NULL,NULL,NULL);
  Vec3d diff=(pos-posL)/dt-rot*(RL*_dirL);
  scalarD scoef=sqrt(_coef);

  if(_dim == 2)
    clearDim(DRDWL,NULL);
  f.segment(offf,3)=diff.segment(0,3)*scoef;
  if(fjac) {
    fjac->block(offf,off1,_dim,_dim).diagonal().setConstant(-scoef/dt);
    fjac->block(offf,off2,_dim,_dim).diagonal().setConstant( scoef/dt);
    for(sizeType r=0; r<3; r++)
      fjac->block(offf,off1+3+r,3,1)=-rot*(DRDWL[r]*_dirL)*scoef;
  }
}
sizeType DDPCurvedWalkEnergy::nrF() const
{
  return 3;
}
bool DDPCurvedWalkEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_dirL,is);
  readBinaryData(_rot,is);
  readBinaryData(_dirL0,is);
  readBinaryData(_rot0,is);
  readBinaryData(_dim,is);
  return is.good();
}
bool DDPCurvedWalkEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_dirL,os);
  writeBinaryData(_rot,os);
  writeBinaryData(_dirL0,os);
  writeBinaryData(_rot0,os);
  writeBinaryData(_dim,os);
  return os.good();
}
boost::shared_ptr<Serializable> DDPCurvedWalkEnergy::copy() const
{
  boost::shared_ptr<DDPCurvedWalkEnergy> ret(new DDPCurvedWalkEnergy);
  *ret=*this;
  return ret;
}
//multitask augmented feature
void DDPCurvedWalkEnergy::setMTAug(const Vec& aug,sizeType off)
{
  if(_mtFeatId == 0) {
    _rot=aug[off+0]*_rot0;
    _dirL=aug[off+1]*_dirL0;
  } else if(_mtFeatId == 1) {
    _rot=aug.segment<3>(off);
    _dirL=aug.segment<3>(off+3);
  } else if(_mtFeatId == 2) {
    _rot=aug[off]*_rot0;
  } else DDPEnergy::setMTAug(aug,off);
}
void DDPCurvedWalkEnergy::getMTAug(Vec& aug,sizeType off) const
{
  if(_mtFeatId == 0) {
    aug[off+0]=_rot.dot(_rot0);
    aug[off+1]=_dirL.dot(_dirL0);
  } else if(_mtFeatId == 1) {
    aug.segment<3>(off)=_rot;
    aug.segment<3>(off+3)=_dirL;
  } else if(_mtFeatId == 2) {
    aug[off]=_rot.dot(_rot0);
  } else DDPEnergy::getMTAug(aug,off);
}
sizeType DDPCurvedWalkEnergy::nrMTAug() const
{
  if(_mtFeatId == 0)
    return 2;
  else if(_mtFeatId == 1)
    return 6;
  else if(_mtFeatId == 2)
    return 1;
  else return DDPEnergy::nrMTAug();
}
//reinforcement learning energy
scalarD RLEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  ASSERT_MSG(!fx && !fxx,"RLEnergy does not support fx!=NULL || fxx!=NULL!")
  return 0;
}
void RLEnergy::objXF(sizeType i,scalar dt,const Vec& x,Vec& f,Matd* fjac,sizeType offf) const
{
  ASSERT_MSG(false,"RLEnergy does not support objXF()!")
}
sizeType RLEnergy::nrF() const
{
  ASSERT_MSG(false,"RLEnergy does not support nrF()!")
  return -1;
}
void RLEnergy::setPos(const Vec3d& pos) {}
bool RLEnergy::isTerminal(const Vec& x) const
{
  return false;
}
RLEnergy::RLEnergy(const string& path):DDPEnergy(path) {}
//GoalEnergy
GoalEnergy::GoalEnergy():RLEnergy(typeid(GoalEnergy).name()) {}
GoalEnergy::GoalEnergy(const Vec3d& pos,const Vec3d* gT,const Vec3d* gN,scalarD coef)
  :RLEnergy(typeid(GoalEnergy).name()),_pos(pos)
{
  _coef=coef;
  if(gT && gT->norm() > 1E-10f) {
    Vec3d N=gT->normalized();
    _PROJ=Mat3d::Identity()-N*N.transpose();
  } else if(gN && gN->norm() > 1E-10f) {
    Vec3d N=gN->normalized();
    _PROJ=N*N.transpose();
  } else {
    _PROJ.setIdentity();
  }
  //cout << _PROJ << endl;
}
scalarD GoalEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  RLEnergy::objX(i,dt,x,fx,fxx);
  Vec3d xP=x.segment<3>(x.size()-6);
  return (xP-_pos).norm()*_coef;
}
void GoalEnergy::setPos(const Vec3d& pos)
{
  _pos=pos;
}
bool GoalEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_pos,is);
  readBinaryData(_PROJ,is);
  return is.good();
}
bool GoalEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_pos,os);
  writeBinaryData(_PROJ,os);
  return os.good();
}
boost::shared_ptr<Serializable> GoalEnergy::copy() const
{
  boost::shared_ptr<GoalEnergy> ret(new GoalEnergy);
  *ret=*this;
  return ret;
}
//DirectedSpeedEnergy
DirectedSpeedEnergy::DirectedSpeedEnergy():RLEnergy(typeid(DirectedSpeedEnergy).name()) {}
DirectedSpeedEnergy::DirectedSpeedEnergy(const Vec3d& pos,const Vec3d* gT,const Vec3d* gN,scalarD coef)
  :RLEnergy(typeid(DirectedSpeedEnergy).name()),_pos(pos)
{
  _coef=coef;
  if(gT && gT->norm() > 1E-10f) {
    Vec3d N=gT->normalized();
    _PROJ=Mat3d::Identity()-N*N.transpose();
  } else if(gN && gN->norm() > 1E-10f) {
    Vec3d N=gN->normalized();
    _PROJ=N*N.transpose();
  } else {
    _PROJ.setIdentity();
  }
  //cout << _PROJ << endl;
}
scalarD DirectedSpeedEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  RLEnergy::objX(i,dt,x,fx,fxx);
  Vec3d xN=x.segment<3>(x.size()/2-6);
  Vec3d xP=x.segment<3>(x.size()-6);
  return ((_pos-xP).norm()-(_pos-xN).norm())/dt;
}
void DirectedSpeedEnergy::setPos(const Vec3d& pos)
{
  _pos=pos;
}
bool DirectedSpeedEnergy::read(istream& is,IOData* dat)
{
  DDPEnergy::read(is,dat);
  readBinaryData(_pos,is);
  readBinaryData(_PROJ,is);
  return is.good();
}
bool DirectedSpeedEnergy::write(ostream& os,IOData* dat) const
{
  DDPEnergy::write(os,dat);
  writeBinaryData(_pos,os);
  writeBinaryData(_PROJ,os);
  return os.good();
}
boost::shared_ptr<Serializable> DirectedSpeedEnergy::copy() const
{
  boost::shared_ptr<DirectedSpeedEnergy> ret(new DirectedSpeedEnergy);
  *ret=*this;
  return ret;
}
//ObstacleEnergy
ObstacleEnergy::ObstacleEnergy():RLEnergy(typeid(ObstacleEnergy).name()) {}
ObstacleEnergy::ObstacleEnergy(const Vec3d* gT,scalarD safeDist,scalarD fade,scalarD coef)
  :RLEnergy(typeid(ObstacleEnergy).name()),_safeDist(safeDist),_fade(fade),_coef(coef)
{
  if(gT && gT->norm() > 1E-10f) {
    Vec3d N=gT->normalized();
    _PROJ=Mat3d::Identity()-N*N.transpose();
  } else {
    _PROJ.setIdentity();
  }
}
scalarD ObstacleEnergy::objX(sizeType i,scalar dt,const Vec& x,Vec* fx,Matd* fxx) const
{
  scalarD ret=0;
  Vec3d pos=x.segment<3>(x.size()-6);
  for(sizeType i=0; i<_obstacle.cols(); i++) {
    scalarD rad=_safeDist+_obstacle(3,i);
    scalarD dist=(_obstacle.block<3,1>(0,i)-pos).norm();
    ret+=std::exp(_fade*std::min<scalarD>(rad-dist,0));
  }
  return ret*_coef;
}
void ObstacleEnergy::setObstacle(const Mat4Xd& obstacle)
{
  _obstacle=obstacle;
}
bool ObstacleEnergy::read(istream& is,IOData* dat)
{
  readBinaryData(_safeDist,is);
  readBinaryData(_fade,is);
  readBinaryData(_coef,is);
  readBinaryData(_obstacle,is);
  readBinaryData(_PROJ,is);
  return is.good();
}
bool ObstacleEnergy::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_safeDist,os);
  writeBinaryData(_fade,os);
  writeBinaryData(_coef,os);
  writeBinaryData(_obstacle,os);
  writeBinaryData(_PROJ,os);
  return os.good();
}
boost::shared_ptr<Serializable> ObstacleEnergy::copy() const
{
  return boost::shared_ptr<Serializable>(new ObstacleEnergy);
}
bool ObstacleEnergy::isTerminal(const Vec& x) const
{
  Vec3d pos=x.segment<3>(x.size()-6);
  for(sizeType i=0; i<_obstacle.cols(); i++) {
    scalarD rad=_obstacle(3,i);//_safeDist+_obstacle(3,i);
    scalarD dist=(_obstacle.block<3,1>(0,i)-pos).norm();
    if(std::min<scalarD>(rad-dist,0) == 0)
      return true;
  }
  return false;
}
