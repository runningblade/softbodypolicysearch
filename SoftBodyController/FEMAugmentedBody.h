#ifndef FEM_AUGMENTED_BODY_H
#define FEM_AUGMENTED_BODY_H

#include <Dynamics/FEMMesh.h>
#include <Dynamics/FEMSystem.h>
#include <Dynamics/FEMCIOEngine.h>
#include <CommonFile/solvers/Objective.h>
#include <boost/unordered_set.hpp>

PRJ_BEGIN

class FEMAugmentedBody : public Objective<scalarD>
{
public:
  typedef FEMMesh::Vec Vec;
  FEMAugmentedBody(const FEMCIOEngine& mesh,const string& path,const string* pathEmbed,const Vec3& ctr,scalar dt);
  virtual ~FEMAugmentedBody();
  virtual void solveAug();
  virtual FEMMesh& getMeshAug();
  virtual const FEMMesh& getMeshAug() const;
  //objective
  virtual void debugGradient();
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient);
  virtual void removeConstraint(const Vec& x,scalarD& FX,Vec* fgrad,STrips* fhess) const;
  virtual int inputs() const;
  bool _keyframe;
private:
  //data
  Vec _xLast,_xCurr,_M,_x0;
  boost::unordered_set<sizeType> _fixSet;
  vector<sizeType> _corr;
  const FEMCIOEngine& _eng;
  FEMMesh _meshAug;
  bool _dynamics;
  scalar _dt;
  //constraints
  void buildConstraint(const Vec3& ctr,const Vec3& a0,const Vec3& b0);
  void addDeriv(Vec& DFDX,const FEMInterp& I,const Vec3& df) const;
  scalar _len,_phase,_angleThres,_t;
  FEMSystem::GroupInfo _info;
  FEMInterp _a,_b;
};

PRJ_END

#endif
