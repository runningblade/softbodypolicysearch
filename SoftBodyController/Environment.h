#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "TrainingData.h"
#include "NeuralNetPolicy.h"
#include <boost/filesystem/path.hpp>

PRJ_BEGIN

class Environment : public TrajectorySampler
{
public:
  Environment();
  virtual bool read(const string& path);
  virtual bool write(const string& path) const;
  virtual bool read(istream& is,IOData* dat)=0;
  virtual bool write(ostream& os,IOData* dat) const=0;
  virtual void writeEnvVTK(const string& path) const;
  virtual void reset(const boost::property_tree::ptree& pt)=0;
  virtual Policy& getPolicy()=0;
  virtual const Policy& getPolicy() const;
  virtual Vec sampleA(sizeType i,const Vec& S) override;
  virtual scalarD getReward(sizeType i,const Vec& state,const Vec& A) const;
  virtual scalarD getReward(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const;
  virtual scalarD getCost(sizeType i,const Vec& state,const Vec& A) const;
  virtual scalarD getCost(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const;
  void setState(const Vec& s) override;
  const Vec& getState() const;
  void resetPolicy();
protected:
  Environment(const string& name);
  boost::shared_ptr<Policy> _policy;
  Vec _state;
};
class EnvironmentMT
{
public:
  virtual ~EnvironmentMT() {}
  virtual sizeType K() const=0;
  virtual void setK(sizeType k)=0;
};
template <typename T>
bool isPolicy(const Environment& env)
{
  return dynamic_cast<const T*>(&(env.getPolicy()));
}
class PendulumEnvironment : public Environment
{
public:
  PendulumEnvironment();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual void writeStateVTK(const string& path);
  virtual void reset(const boost::property_tree::ptree& pt);
  virtual Policy& getPolicy();
  virtual Vec transfer(sizeType i,const Vec& A);
  virtual scalarD getReward(sizeType i,const Vec& state,const Vec& A) const;
  virtual Vec sampleS0();
  boost::property_tree::ptree _tree;
};
class AcrobotEnvironment : public Environment
{
public:
  AcrobotEnvironment();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual void writeStateVTK(const string& path);
  virtual void reset(const boost::property_tree::ptree& pt);
  virtual Policy& getPolicy();
  virtual bool isTerminal(sizeType i,const Vec& state) const override;
  virtual Vec transfer(sizeType i,const Vec& A);
  virtual scalarD getReward(sizeType i,const Vec& state,const Vec& A) const;
  virtual Vec sampleS0();
  boost::property_tree::ptree _tree;
protected:
  Vec DSDT(const Vec& S,const Vec& A) const;
};
class LinkEnvironment : public Environment
{
public:
  typedef vector<Vec2d,Eigen::aligned_allocator<Vec2d> > Vss;
  LinkEnvironment();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual void writeStateVTK(const string& path);
  virtual void reset(const boost::property_tree::ptree& pt);
  virtual Policy& getPolicy();
  virtual bool isTerminal(sizeType i,const Vec& state) const override;
  virtual Vec transfer(sizeType i,const Vec& A);
  virtual scalarD getReward(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const;
  virtual Vec sampleS0();
  boost::property_tree::ptree _tree;
protected:
  Vec2d jacobian(const Vec& A,Matd& DADC,Matd DDADDC[2],Vss& vss) const;
};

PRJ_END

#endif
