#include "CIOOptimizer.h"
#include "CIOPolicyOptimizer.h"
#include "CIOOptimizerPhysicsTerm.h"
#include "CIOOptimizerContactTerm.h"
#include "CIOOptimizerSelfCollisionTerm.h"
#include "CIOOptimizerShuffleAvoidanceTerm.h"
#include "QPInterface.h"
#include "QCQP.h"
#include <CMAES/MLUtil.h>
#include <Dynamics/FEMUtils.h>
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/ArticulatedCIOEngine.h>

USE_PRJ_NAMESPACE

//Terms in an objective function
CIOOptimizerTerm::CIOOptimizerTerm(CIOOptimizer& opt):_opt(opt) {}
scalarD CIOOptimizerTerm::runSQP(FEMCIOEngine& eng,sizeType i,Vec& DFDXI,Matd* DDFDDXI) const
{
  return 0;
}
void CIOOptimizerTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const {}
void CIOOptimizerTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const {}
int CIOOptimizerTerm::values() const
{
  return 0;
}
bool CIOOptimizerTerm::valid() const
{
  return false;
}
//Objective Term
CIOOptimizerObjectiveTerm::CIOOptimizerObjectiveTerm(CIOOptimizer& opt):CIOOptimizerTerm(opt) {}
void CIOOptimizerObjectiveTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  Vec xS;
  Matd fxS;
  sizeType nrS=_opt._sol.nrX()/2;
  sizeType nrO=_opt._sol.nrObjXF();

  //objective
  xS=concat(eng._x.x(),eng._xP.x());
  if(fjac)
    fvec.segment(off+i*nrO,nrO)=_opt._sol.objXF(i,xS,&fxS);
  else
    fvec.segment(off+i*nrO,nrO)=_opt._sol.objXF(i,xS,NULL);
  //assign result
  if(fjac) {
    //X
    _opt.addBlockX (*fjac,off+i*nrO,i,fxS.block(0,0,nrO,nrS));
    //XP
    _opt.addBlockXP(*fjac,off+i*nrO,i,fxS.block(0,nrS,nrO,nrS));
  }
}
void CIOOptimizerObjectiveTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  Vec xS,fxS;
  sizeType nrS=_opt._sol.nrX()/2;
  //objective
  xS=concat(eng._x.x(),eng._xP.x());
  FX+=_opt._sol.objX(i,xS,&fxS,NULL);
  //assign gradient
  //X
  _opt.addBlockX (DFDX,i,fxS.segment(0,nrS));
  //XP
  _opt.addBlockXP(DFDX,i,fxS.segment(nrS,nrS));
}
int CIOOptimizerObjectiveTerm::values() const
{
  return _opt._horizon*_opt._sol.nrObjXF();
}
bool CIOOptimizerObjectiveTerm::valid() const
{
  return true;
}
//ObjectiveMT Term
CIOOptimizerObjectiveMTTerm::CIOOptimizerObjectiveMTTerm(CIOOptimizer& opt,sizeType K):CIOOptimizerTerm(opt),_K(K) {}
void CIOOptimizerObjectiveMTTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  Vec xS;
  Matd fxS;
  sizeType nrS=_opt._sol.nrX()/2;
  sizeType nrO=_opt._sol.nrObjXFMT(_K);

  //objective
  xS=concat(eng._x.x(),eng._xP.x());
  if(fjac)
    fvec.segment(off+i*nrO,nrO)=_opt._sol.objXFMT(i,xS,&fxS,_K);
  else
    fvec.segment(off+i*nrO,nrO)=_opt._sol.objXFMT(i,xS,NULL,_K);
  //assign result
  if(fjac) {
    //X
    _opt.addBlockX (*fjac,off+i*nrO,i,fxS.block(0,0,nrO,nrS));
    //XP
    _opt.addBlockXP(*fjac,off+i*nrO,i,fxS.block(0,nrS,nrO,nrS));
  }
}
void CIOOptimizerObjectiveMTTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  Vec xS,fxS;
  sizeType nrS=_opt._sol.nrX()/2;
  //objective
  xS=concat(eng._x.x(),eng._xP.x());
  FX+=_opt._sol.objXMT(i,xS,&fxS,NULL,_K);
  //assign gradient
  //X
  _opt.addBlockX (DFDX,i,fxS.segment(0,nrS));
  //XP
  _opt.addBlockXP(DFDX,i,fxS.segment(nrS,nrS));
}
int CIOOptimizerObjectiveMTTerm::values() const
{
  return _opt._horizon*_opt._sol.nrObjXFMT(_K);
}
bool CIOOptimizerObjectiveMTTerm::valid() const
{
  return true;
}
sizeType CIOOptimizerObjectiveMTTerm::K() const
{
  return _K;
}
//Laplacian Term
CIOOptimizerLaplacianTerm::CIOOptimizerLaplacianTerm(CIOOptimizer& opt):CIOOptimizerTerm(opt) {}
void CIOOptimizerLaplacianTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  scalar wLap=_opt._sol._tree.get<scalar>("regStateLaplacianCoef",0);
  scalar wLapSqrt=sqrt(wLap);

  sizeType nrS=_opt._sol.nrX()/2;

  //state laplacian
  Vec lap=(eng._xN.x()-eng._x.x()*2+eng._xP.x());
  fvec.segment(off+i*(nrS-6),nrS-6)=lap.segment(0,nrS-6)*wLapSqrt;
  //assign gradient
  if(fjac) {
    Matd ID=Matd::Identity(nrS-6,nrS-6);
    extend(ID,0,6);
    //XN
    _opt.addBlockXN(*fjac,off+i*(nrS-6),i,ID*wLapSqrt);
    //X
    _opt.addBlockX (*fjac,off+i*(nrS-6),i,-2*ID*wLapSqrt);
    //XP
    _opt.addBlockXP(*fjac,off+i*(nrS-6),i,ID*wLapSqrt);
  }
}
void CIOOptimizerLaplacianTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  scalar wLap=_opt._sol._tree.get<scalar>("regStateLaplacianCoef",0);

  sizeType nrS=_opt._sol.nrX()/2;

  //state laplacian
  Vec lap=(eng._xN.x()-eng._x.x()*2+eng._xP.x());
  lap.segment(nrS-6,6).setZero();
  FX+=lap.squaredNorm()*wLap/2;
  //assign gradient
  //XN
  _opt.addBlockXN(DFDX,i,wLap*lap);
  //X
  _opt.addBlockX (DFDX,i,-2*wLap*lap);
  //XP
  _opt.addBlockXP(DFDX,i,wLap*lap);
}
int CIOOptimizerLaplacianTerm::values() const
{
  return _opt._horizon*(_opt._sol.nrX()/2-6);
}
bool CIOOptimizerLaplacianTerm::valid() const
{
  scalar wLap=_opt._sol._tree.get<scalar>("regStateLaplacianCoef",0);
  return wLap > 0;
}
//Contact Invariant Optimization
CIOOptimizer::CIOOptimizer(FEMDDPSolver& sol,sizeType horizon,FEMDDPSolverInfo& info,sizeType K)
  :_horizon(horizon),_sol(sol),_info(info),_iter(0)
{
  _qpSol.reset(new QPInterface);
  _sol.getEngine().getBody()._tree.put<bool>("MTCubature",true);
  _periodicPosition=_sol._tree.get<bool>("periodicPosition",false);
  _periodicRotation=_sol._tree.get<bool>("periodicRotation",false);
  _periodicDeform=(_sol._tree.get<bool>("periodicDeform",false) || _periodicPosition || _periodicRotation);
  _fixRotation=_sol._tree.get<bool>("fixRotation",false);
  if(_fixRotation)
    _periodicRotation=false;
  INFOV("periodicPosition=%s periodicRotation=%s periodicDeform=%s fixRotation=%s",
        _periodicPosition?"true":"false",
        _periodicRotation?"true":"false",
        _periodicDeform?"true":"false",
        _fixRotation?"true":"false")
  //force updating solver object to remove nodes
  resetMetric();
  //build terms
  _terms.clear();
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerPhysicsTerm(*this)));
  if(K < 0)
    _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerObjectiveTerm(*this)));
  else _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerObjectiveMTTerm(*this,K)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerLaplacianTerm(*this)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerContactTerm(*this)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerShuffleAvoidanceTerm(*this)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerConicShuffleAvoidanceTerm(*this)));
  if(dynamic_cast<ArticulatedCIOEngineImplicit*>(_sol.getEnv().solPtr()))
    _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerSelfCollisionTerm(*this)));
  else _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerSelfCollisionTermPrecomputed(*this)));
  clearInvalidTerms();

  boost::shared_ptr<CIOOptimizerContactTerm> cTerm=getTerm<CIOOptimizerContactTerm>();
  if(cTerm)
    cTerm->updateContacts(NULL);
}
void CIOOptimizer::clearInvalidTerms()
{
  for(sizeType i=0; i<(sizeType)_terms.size();)
    if(!_terms[i]->valid())
      _terms.erase(_terms.begin()+i);
    else i++;

  //linked PhysicsTerm with ContactTerm
  boost::shared_ptr<CIOOptimizerPhysicsTerm> pTerm=getTerm<CIOOptimizerPhysicsTerm>();
  if(pTerm)
    pTerm->_cTerm=getTerm<CIOOptimizerContactTerm>();
}
void CIOOptimizer::reset()
{
  const CIOOptimizerContactTerm* cTerm=getTermConst<CIOOptimizerContactTerm>();
  sizeType dim=_sol.getEngine().getBody().dim();
  sizeType nrInput=inputs();
  if(cTerm) {
    const Coli& cOff=cTerm->getCollisionOff();
    nrInput+=cOff[cOff.size()-1]*dim;
  }
  BLEICInterface::reset(nrInput,false);
}
int CIOOptimizer::inputs() const
{
  sizeType nrS=_sol.nrX()/2;
  if(_fixRotation)
    nrS-=3;
  sizeType ret=_horizon*nrS;
  if(_periodicPosition)
    ret+=6;
  if(_periodicRotation)
    ret+=6;
  return ret;
}
int CIOOptimizer::values() const
{
  int ret=0;
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    ret+=_terms[i]->values();
  return ret;
}
int CIOOptimizer::valuesNoCollision() const
{
  int ret=0;
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    if(boost::dynamic_pointer_cast<CIOOptimizerContactTerm>(_terms[i]) == NULL)
      ret+=_terms[i]->values();
  return ret;
}
const FEMDDPSolverInfo& CIOOptimizer::info() const
{
  return _info;
}
FEMDDPSolverInfo& CIOOptimizer::info()
{
  return _info;
}
const Matd& CIOOptimizer::DUDE() const
{
  return _DUDE;
}
//operators
void CIOOptimizer::writeResidueInfo(const Vec& xs)
{
  updateConfig(xs,true);
  FEMCIOEngine& eng=_sol.getEngine();
  const CIOOptimizerObjectiveMTTerm* OTerm=getTermConst<CIOOptimizerObjectiveMTTerm>();

  scalar dt=eng._tree.get<scalar>("dt");
  scalar wContV=_sol._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar muSA=_sol._tree.get<scalar>("muConicShuffleAvoidance",1);
  bool crossShuffle=_sol._tree.get<bool>("shuffleAvoidanceCrossTerm",true);
  scalar k1=_sol._tree.get<scalar>("k1",10);
  scalar k2=_sol._tree.get<scalar>("k2",k1);

  Vec E;
  INFO("Residue Information")
  INFOV("%10s %10s %10s %10s %10s %10s %10s","Phys.","uNN.","Cont.","SA.","CSA.","Obj.","nrC.")
  for(sizeType i=0; i<_horizon; i++) {
    //state
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    //physics violation
    eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
    eng.setControlInput(_DUDE*E);
    eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,true);
    scalarD P=E.norm();
    //neural-net violation
    scalarD uNN=eng.getControlInput().norm();
    //contact activation
    scalarD C=eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
    //shuffle avoidance
    scalarD SA=eng.computeShuffleAvoidanceCIO(dt,NULL,NULL,crossShuffle);
    scalarD CSA=eng.computeConicShuffleAvoidanceCIO(dt,muSA,NULL,NULL,crossShuffle);
    //high level objective
    scalarD O=0;
    if(OTerm)
      O=_sol.objXMT(i,concat(_info._xss[i+1].x(),_info._xss[i+2].x()),NULL,NULL,OTerm->K());
    else O=_sol.objX(i,concat(_info._xss[i+1].x(),_info._xss[i+2].x()),NULL,NULL);
    //write
    INFOV("%10.5f %10.5f %10.5f %10.5f %10.5f %10.5f %10.5ld",P,uNN,C,SA,CSA,O,eng._c.nrC())
  }

  Vec fvec;
  INFO("Function Information")
  //total function
  operator()(xs,fvec,NULL,false);
  INFOV("Function value: %f",fvec.squaredNorm()/2)

  //total physics function
  beginSingleOutTerm<CIOOptimizerPhysicsTerm>();
  boost::shared_ptr<CIOOptimizerPolicyPhysicsTerm> ppTerm=getTerm<CIOOptimizerPolicyPhysicsTerm>();
  //test physics violation
  operator()(xs,fvec,NULL,false);
  INFOV("Physics violation: %f",fvec.squaredNorm()/2)
  endSingleOutTerm();

  //total contact function
  beginSingleOutTerm<CIOOptimizerContactTerm>();
  operator()(xs,fvec,NULL,false);
  INFOV("Contact violation: %f",fvec.squaredNorm()/2)
  endSingleOutTerm();
}
void CIOOptimizer::assignControl()
{
  FEMCIOEngine& eng=_sol.getEngine();
  scalar dt=eng._tree.get<scalar>("dt");

  Vec E;
  for(sizeType i=0; i<_horizon; i++) {
    //state
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    //physics violation
    eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
    eng.setControlInput(_DUDE*E);
    _info._infos[i]=eng._c;
  }
}
//working code
scalarD CIOOptimizer::runSQP(FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac) const
{
  sizeType nrSys=_sol.getEngine().getMCalc().size();

  scalarD ret=0;
  Vec GI=Vec::Zero(nrSys*2);
  Matd HI=Matd::Zero(nrSys*2,nrSys*2);
  for(sizeType t=0; t<(sizeType)_terms.size(); t++)
    ret+=_terms[t]->runSQP(eng,i,GI,fjac ? &HI : NULL);
  addBlockSQP(i,GI,HI,fvec,fjac);
  return ret;
}
void CIOOptimizer::runLM(FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  sizeType off=0;
  for(sizeType t=0; t<(sizeType)_terms.size(); t++) {
    _terms[t]->runLM(off,eng,qp,i,fvec,fjac,modifiable);
    off+=_terms[t]->values();
  }
}
void CIOOptimizer::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  for(sizeType t=0; t<(sizeType)_terms.size(); t++)
    _terms[t]->runLBFGS(eng,i,FX,DFDX);
}
//update neural network
void CIOOptimizer::updateWeight() {}
void CIOOptimizer::applyControl(scalar dt,FEMCIOEngine& eng,sizeType i) const
{
  Vec time=Vec::Zero(1);
  time[0]=dt*(scalarD)i;
  eng.setControlInput(_sol.getNeuralNet().foreprop(time));
  eng._DUNNDXN.setZero(0,0);
  eng._DUNNDX .setZero(0,0);
}
void CIOOptimizer::updateNeuralNet(const Vec& fvec)
{
  //assemble solution
  sizeType nrS=_sol.nrX()/2,nrU=nrS-6;
  Matd states=Matd::Zero(nrU,_horizon);
  Matd actions=Matd::Zero(nrU,_horizon);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<_horizon; i++) {
    states.col(i)=_info._xss[i+2].x().segment(0,nrU);
    actions.col(i)=_DUDE*fvec.segment(i*nrS,nrS);
  }
}
void CIOOptimizer::updateNeuralNet(const Vec* x,bool modifiable)
{
  //save
  beginSingleOutTwoTerms<CIOOptimizerPhysicsTerm,CIOOptimizerContactTerm>();
  boost::shared_ptr<CIOOptimizerPolicyPhysicsTerm> ppTerm=getTerm<CIOOptimizerPolicyPhysicsTerm>();
  if(ppTerm)
    ppTerm->setForNNUpdate(true);
  //operator
  Vec X=x ? *x : collectSolution(false),fvec;
  operator()(X,fvec,NULL,modifiable,true);
  updateNeuralNet(fvec);
  //load
  if(ppTerm)
    ppTerm->setForNNUpdate(false);
  endSingleOutTerm();
}
//metric
void CIOOptimizer::resetMetric()
{
  //parameter
  FEMCIOEngine& eng=_sol.getEngine();
  scalar dt=eng._tree.get<scalar>("dt");
  scalar regU=_sol._tree.get<scalar>("regCoef",0.01f);
  {
    const Matd& C=eng.getControlMatrix();
    Matd invK=_sol.getEnv().getStiffness().inverse();
    Matd C1=-concatRow(C,Matd::Zero(6,C.cols()))*dt*dt;
    Matd C2=C.transpose()*(invK*C);
    scalarD M1=rescaleMetric(C1.transpose()*C1);
    scalarD M2=rescaleMetric(C2);
    regU*=M2/M1;
    computeMetricU(C1,C2*regU,_metricSqr,_DUDE);
    //if(_sol._tree.get<bool>("printEffectiveRegCoef",false)) {
    //  INFOV("Effective regCoef: %f",regU)
    //}
  }
  _metricSqrSqrt=matrixSqrt(_metricSqr);
}
void CIOOptimizer::debugComputeMetric()
{
#define N 5
  Matd M,D,TE;
  Matd C1=Matd::Random(N+6,N);
  Matd C2=Matd::Random(N  ,N);
  C2=(C2.transpose()*C2).eval();
  computeMetricU(C1,C2,M,D);
  Vec E=Vec::Random(N+6);
  scalarD F1=E.dot(M*E)/2;
  scalarD F2=(E+C1*D*E).squaredNorm()/2+(D*E).dot(C2*D*E)/2;
  INFOV("F1: %f F2: %f Err: %f",F1,F2,F1-F2)

  computeMetricX(C1,C2,M,TE);
  Vec u=Vec::Random(N);
  scalarD delta=(E.squaredNorm()-(TE*E).dot(M*(TE*E)))/2;
  F1=(E+C1*u).squaredNorm()/2+u.dot(C2*u)/2;
  F2=(u-TE*E).dot(M*(u-TE*E))/2+delta;
  INFOV("F1: %f F2: %f Err: %f",F1,F2,F1-F2)
  exit(-1);
#undef N
}
scalarD CIOOptimizer::rescaleMetric(const Matd& m)
{
  Eigen::SelfAdjointEigenSolver<Matd> eig(m);
  //INFO("Rescaling metric, eigenvalues: ")
  //cout << eig.eigenvalues().transpose() << endl;
  //INFOV("Scale by: %f",eig.eigenvalues().maxCoeff())
  return 1/eig.eigenvalues().maxCoeff();
}
void CIOOptimizer::computeMetricU(const Matd& C1,const Matd& C2,Matd& metricSqr,Matd& DUDE)
{
  //the energy term is:     F = [ ||E0+C1*u||^2 + ||u||_{C2}^2 ] / 2
  //we compute u = DUDE*E0, F = ||E0||_{metricSqr}^2 / 2
  ASSERT(C2.rows() == C2.cols() && C1.cols() == C2.cols())
  Matd H=C1.transpose()*C1+C2;
  DUDE=-H.fullPivLu().solve(C1.transpose());

  metricSqr=Matd::Zero(C1.rows(),C1.rows());
  Matd C1Eq=Matd::Identity(C1.rows(),C1.rows())+C1*DUDE;
  metricSqr+=C1Eq.transpose()*C1Eq;
  metricSqr+=DUDE.transpose()*(C2*DUDE);
}
void CIOOptimizer::computeMetricX(const Matd& C1,const Matd& C2,Matd& metricSqr,Matd& TE)
{
  //the energy term is: F = [ ||E0+C1*u||^2 + ||u||_{C2}^2 ] / 2
  //we compute F = const + ||u-TE*E0||)_{metricSqr}^2 / 2
  ASSERT(C2.rows() == C2.cols() && C1.cols() == C2.cols())
  metricSqr=C1.transpose()*C1+C2;
  TE=-metricSqr.fullPivLu().solve(C1.transpose());
}
//for debug
void CIOOptimizer::compareDFDX(const Vec& DFDX0,const Vec& DFDX1) const
{
  sizeType off=inputs(),dim=_sol.getEngine().getBody().dim();
  const Coli& cOff=getTermConst<CIOOptimizerContactTerm>()->getCollisionOff();
  for(sizeType i=0; i<_horizon; i++) {
    scalarD B=DFDX0.segment(off+cOff[i]*dim,(cOff[i+1]-cOff[i])*dim).norm();
    scalarD A=DFDX1.segment(off+cOff[i]*dim,(cOff[i+1]-cOff[i])*dim).norm();
    INFOV("DFDC Before: %f After: %f",B,A)
  }
}
void CIOOptimizer::randomizeParameter(bool phys,bool coll)
{
  //set random physics penalty
  if(phys) {
    boost::shared_ptr<CIOOptimizerPhysicsTerm> termPhys=
      getTerm<CIOOptimizerPhysicsTerm>();
    if(termPhys)
      termPhys->randomizeParam();
  }
  //add random self collision
  boost::shared_ptr<CIOOptimizerSelfCollisionTerm> term=
    getTerm<CIOOptimizerSelfCollisionTerm>();
  if(term && coll)
    term->addRandomSelfCollision();
  reset();
}
void CIOOptimizer::randomizeContact()
{
  FEMCIOEngine& eng=_sol.getEngine();
  scalar dt=eng._tree.get<scalar>("dt");
  for(sizeType i=0; i<_horizon; i++) {
    CIOContactInfo& xInfo=_info._infos[i];
    Vec cf=Vec::Random(xInfo.cf().size());
    xInfo.setCF(dt,eng.getMCalc(),&cf);
  }
}
CIOOptimizer::Vec CIOOptimizer::get2DMask() const
{
  sizeType nrS=_sol.nrX()/2,nrU=nrS-6;
  Vec ret=Vec::Ones(BLEICInterface::inputs());
  for(sizeType i=0; i<_horizon; i++)
    ret.segment(i*nrS+nrU+2,3).setZero();
  return ret;
}
bool CIOOptimizer::hasContact() const
{
  return getTermConst<CIOOptimizerContactTerm>();
}
//update force
void CIOOptimizer::updateF(sizeType i,FEMCIOEngine& eng,QPInterface& qp,const Matd& metricSqr,scalar dt,scalar wCont,Vec vios,const Vec& E) const
{
  const CIOOptimizerContactTerm* cTerm=getTermConst<CIOOptimizerContactTerm>();
  if(!cTerm)
    return;
  const Coli& cOff=cTerm->getCollisionOff();

  //construct normal/frictional constraints
  scalar mu=eng._tree.get<scalar>("collMu");
  sizeType dim=eng.getBody().dim();
  sizeType nrC=cOff[i+1]-cOff[i];
  if(nrC == 0)
    return;

  ASSERT_MSG(mu > 0 && mu <= 1,"You must have mu in range (0,1]")
  const RigidReducedMCalculator& MCalc=eng.getMCalc();
  Matd DEDC=eng.computePhysicsDerivativeEFCIO(),H;
  Vec G,F;

  //build problem
  vios=vios.array().max(1E-6f).matrix();
  H=(DEDC*metricSqr)*DEDC.transpose();
  G=(DEDC*metricSqr)*E-H*eng._c.cf();
  H.diagonal()+=vios*wCont*2;

  //solve problem
  Matd CI;
  if(_sol._tree.get<bool>("forceInCone",true)) {
    try {
      if(_sol._tree.get<bool>("forceGPSolve",false)) {
        //gradient projection solver
        sizeType maxGPIter=eng._tree.get<sizeType>("maxGPIter",1000);
        scalarD relGPErr=eng._tree.get<scalar>("relGPErr",1E-3f);
        if(dim == 2)
          QCQP<2>::solve(H,G,F=eng._c.cf(),mu*mu,maxGPIter,relGPErr);
        else QCQP<3>::solve(H,G,F=eng._c.cf(),mu*mu,maxGPIter,relGPErr);
      } else {
        //dual solver
        Matd D=concatCol(Vec::Constant(eng.getD().cols(),mu),eng.getD().transpose());
        CI.setZero(nrC*D.rows(),nrC*dim);
        for(sizeType i=0; i<nrC; i++)
          CI.block(i*D.rows(),i*dim,D.rows(),dim)=D;
        //qp.solveDual(H,G,CI,NULL,F=Vec::Zero(G.size()));
        qp.solveAQP(H,G,CI,NULL,F=eng._c.cf());
      }
    } catch(...) {
      try {
        //fallback to primal solver
        Matd D=concatCol(Vec::Constant(eng.getD().cols(),mu),eng.getD().transpose());
        CI.setZero(nrC*D.rows(),nrC*dim);
        for(sizeType i=0; i<nrC; i++)
          CI.block(i*D.rows(),i*dim,D.rows(),dim)=D;
        qp.solvePrimal(H,G,CI,NULL,F=Vec::Zero(G.size()));
      } catch(...) {
        OMP_CRITICAL_ {
          //write problem
          boost::filesystem::ofstream osQP("./errorQP.dat");
          osQP << "LHS:" << endl << H << endl;
          osQP << "RHS:" << endl << G << endl;
          osQP << "CI:"  << endl << CI << endl;

          //write error configuration
          boost::filesystem::ofstream osSol("./errorConfig.dat",ios::binary);
          boost::shared_ptr<IOData> dat=getIOData();
          _sol.write(osSol,dat.get());

          Eigen::SelfAdjointEigenSolver<Matd> eig(H);
          INFOV("Eigenvalues in range: [%f,%f]",eig.eigenvalues().minCoeff(),eig.eigenvalues().maxCoeff())
          ASSERT_MSG(false,"QPPrimalSolver failed!")
        }
      }
    }
  } else {
    F=-H.ldlt().solve(G);
  }

  //assign result
  eng._c.setCF(dt,MCalc,&F);
}
void CIOOptimizer::updateForce()
{
  FEMCIOEngine& eng=_sol.getEngine();
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wCont=_sol._tree.get<scalar>("regContactActivationCoef",1000);
  scalar wContV=_sol._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_sol._tree.get<scalar>("k1",10);
  scalar k2=_sol._tree.get<scalar>("k2",k1);

  Vec vio,E;
  const Matd& metricF=getTermConst<CIOOptimizerPhysicsTerm>()->metricF();
  for(sizeType i=0; i<_horizon; i++) {
    //state
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    //compute
    eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
    eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,&vio,NULL,NULL,NULL);
    updateF(i,eng,*_qpSol,metricF,dt,wCont,vio,E);
    _info._infos[i]=eng._c;
  }
}
