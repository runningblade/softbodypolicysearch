#include "FEMDDPSolver.h"
#include "FEMEnvironmentMT.h"
#include "CIOPolicyOptimizer.h"
#include "CIOOptimizerPhysicsTerm.h"
#include "CIOOptimizerContactTerm.h"
#include "CIOOptimizerSelfCollisionTerm.h"
#include "CIOOptimizerShuffleAvoidanceTerm.h"
#include "OneLineCallback.h"
#include <CommonFile/Timing.h>
#include <CMAES/DenseInterface.h>
#include <CMAES/MLUtil.h>
#include <Dynamics/FEMUtils.h>
#include <Dynamics/LMInterface.h>
#include <Dynamics/FEMCIOEngine.h>
#include <boost/lexical_cast.hpp>
#include <iomanip>

USE_PRJ_NAMESPACE

//helper
static void dfdxExplicit(FEMEnvironment& env,scalarD dt,sizeType i,
                         const FEMEnvironment::Vec& x,const FEMEnvironment::Vec& u,
                         FEMEnvironment::Vec& f,Matd* fx=NULL,Matd* fu=NULL,FEMLCPSolver::Contacts* cons=NULL)
{
  typedef FEMEnvironment::Vec Vec;
  Matd DXPDXN,DXPDX,DXPDU;
  FEMCIOEngine& eng=env.sol();
  sizeType nrS=eng.getMCalc().size(),nrU=nrS-6;
  Vec diag=Vec::Ones(nrS);
  if(eng.getBody().dim() == 2)
    diag.segment(nrS-4,3).setZero();
  ASSERT(x.size() == nrS*2)

  eng.setControlInput(u);
  eng._xN.reset(eng.getMCalc(),x.segment(0,nrS));
  eng._x.reset(eng.getMCalc(),x.segment(nrS,nrS));
  if(fx && fu) {
    eng.FEMCIOEngine::advanceDerivative(dt,DXPDXN,DXPDX,DXPDU);
    ASSERT(DXPDXN.rows() == nrS && DXPDXN.cols() == nrS)
    ASSERT(DXPDX.rows() == nrS && DXPDX.cols() == nrS)
    ASSERT(DXPDU.rows() == nrS && DXPDU.cols() == nrU)
    //fx part
    fx->setZero(nrS*2,nrS*2);
    fx->block(nrS,0,nrS,nrS)=DXPDXN;
    fx->block(nrS,nrS,nrS,nrS)=DXPDX;
    fx->block(0,nrS,nrS,nrS).diagonal()=diag;
    //fu part
    fu->setZero(nrS*2,nrU);
    fu->block(nrS,0,nrS,nrU)=DXPDU;
  } else {
    ASSERT(!fx && !fu)
    eng.FEMCIOEngine::advance(dt,true);
  }
  f.resize(x.size());
  f.segment(0,nrS)=eng._xN.x();
  f.segment(nrS,nrS)=eng._x.x();
  if(cons)
    *cons=eng.getCons();
}
static void dfdxImplicit(FEMEnvironment& env,scalarD dt,sizeType i,
                         const FEMEnvironment::Vec& x,const FEMEnvironment::Vec& u,
                         FEMEnvironment::Vec& f,Matd* fx=NULL,Matd* fu=NULL,FEMLCPSolver::Contacts* cons=NULL)
{
  typedef FEMEnvironment::Vec Vec;
  FEMCIOEngineImplicit& eng=env.sol();
  sizeType nrS=eng.getMCalc().size(),nrU=nrS-6;
  scalar deltaS=eng._tree.get<scalar>("deltaDFDX",0.01f);
  scalar deltaU=eng._tree.get<scalar>("deltaDFDU",0.01f);
  Vec diag=Vec::Ones(nrS);
  if(eng.getBody().dim() == 2)
    diag.segment(nrS-4,3).setZero();
  ASSERT(x.size() == nrS*2)

  eng.setControlInput(u);
  eng._xN.reset(eng.getMCalc(),x.segment(0,nrS));
  eng._x.reset(eng.getMCalc(),x.segment(nrS,nrS));
  //advance
  eng.advanceImplicit(dt,false);
  f.resize(x.size());
  f.segment(0,nrS)=eng._x.x();
  f.segment(nrS,nrS)=eng._xP.x();
  //fx part
  if(fx) {
    fx->setZero(nrS*2,nrS*2);
    fx->block(0,nrS,nrS,nrS).diagonal()=diag;
    GradientInfo tmpXN=eng._xN,tmpX=eng._x;
    for(sizeType i=0; i<nrS; i++) {
      if(i >= nrS-4 && i < nrS-1)
        continue;
      //xN
      eng._xN.reset(eng.getMCalc(),eng._xN.x()+Vec::Unit(nrS,i)*deltaS);
      eng.advanceImplicit(dt,false);
      fx->col(i).segment(nrS,nrS)=(eng._xP.x()-f.segment(nrS,nrS))/deltaS;
      eng._xN=tmpXN;
      //x
      eng._x.reset(eng.getMCalc(),eng._x.x()+Vec::Unit(nrS,i)*deltaS);
      eng.advanceImplicit(dt,false);
      fx->col(nrS+i).segment(nrS,nrS)=(eng._xP.x()-f.segment(nrS,nrS))/deltaS;
      eng._x=tmpX;
    }
  }
  //fu part
  if(fu) {
    fu->setZero(nrS*2,nrU);
    for(sizeType i=0; i<u.size(); i++) {
      eng.setControlInput(u+Vec::Unit(nrU,i)*deltaU);
      eng.advanceImplicit(dt,false);
      fu->col(i).segment(0,nrS)=(eng._x.x()-f.segment(0,nrS))/deltaU;
      fu->col(i).segment(nrS,nrS)=(eng._xP.x()-f.segment(nrS,nrS))/deltaU;
    }
  }
}
//FEMDDPSolver
FEMDDPSolver::FEMDDPSolver(FEMEnvironment& env)
  :Serializable(typeid(FEMDDPSolver).name()),_env(env)
{
  if(env.solPtr() == NULL)
    return;
  if(isPolicy<NeuralNetPolicy>(env)) {
    NeuralNetPolicy& policy=static_cast<NeuralNetPolicy&>(env.getPolicy());
    _bound.reset(new Vec(policy.getActionScale()));
  }

  _env.resetContacts();
  boost::property_tree::ptree& pt=_env.sol()._tree;
  if(!pt.get_optional<scalar>("collisionExpand")) {
    scalarD avgDim=_env.sol().avgElementLength();
    WARNINGV("collisionExpand property not defined, setting to default expand=%f!",avgDim)
    pt.put<scalar>("collisionExpand",avgDim);
  }

  if(_tree.get<string>("workspace","").empty()) {
    string name=env.sol().getBody()._tree.get<string>("meshName","");
    _tree.put<string>("workspace",name);
    recreate(name);
  }
}
bool FEMDDPSolver::read(istream& is,IOData* dat)
{
  _env.read(is,dat);
  readPtreeBinary(_tree,is);
  readVector(_info._xss,is,dat);
  readVector(_info._infos,is,dat);
  return is.good();
}
bool FEMDDPSolver::write(ostream& os,IOData* dat) const
{
  _env.write(os,dat);
  writePtreeBinary(_tree,os);
  writeVector(_info._xss,os,dat);
  writeVector(_info._infos,os,dat);
  return os.good();
}
bool FEMDDPSolver::read(const string& name)
{
  string path=_tree.get<string>("workspace")+"/"+name+".dat";
  boost::filesystem::ifstream is(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return read(is,dat.get());
}
bool FEMDDPSolver::write(const string& name)
{
  writeToTree();
  create(_tree.get<string>("workspace"));
  string path=_tree.get<string>("workspace")+"/"+name+".dat";
  boost::filesystem::ofstream os(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  return write(os,dat.get());
}
void FEMDDPSolver::readFromTree()
{
  sizeType horizon=_tree.get<sizeType>("horizon");
  readFromTree(_env.sol().getMCalc(),_info,_tree,horizon);
}
void FEMDDPSolver::writeToTree()
{
  sizeType horizon=_tree.get<sizeType>("horizon");
  writeToTree(_env.sol().getMCalc(),_info,_tree,horizon);
}
void FEMDDPSolver::writeSelfCollVTK(const string& path)
{
  _tree.put<bool>("initContactUpdate",false);
  boost::shared_ptr<CIOOptimizer> opt=createOpt(false,false);
  boost::shared_ptr<CIOOptimizerSelfCollisionTerm> scTerm=opt->getTerm<CIOOptimizerSelfCollisionTerm>();
  if(scTerm)
    scTerm->writeSelfCollVTK(path);
}
void FEMDDPSolver::writeEnvVTK(const string& path) const
{
  _env.writeEnvVTK(path);
}
void FEMDDPSolver::writeStateVTK(const string& path,const Vec& x) const
{
  _env.sol().getBody()._tree.put<bool>("MTCubature",true);
  _env.setState(x);
  _env.writeStateVTK(path);
  _env.sol().getBody()._tree.put<bool>("MTCubature",false);
}
void FEMDDPSolver::writeStatePov(const string& path,const Vec& x) const
{
  _env.sol().getBody()._tree.put<bool>("MTCubature",true);
  _env.setState(x);
  _env.writeStatePov(path);
  _env.sol().getBody()._tree.put<bool>("MTCubature",false);
}
void FEMDDPSolver::writeStatesVTK(const string& path,const boost::property_tree::ptree* tree,bool useXs,bool pov) const
{
  sizeType frm=0;
  const boost::property_tree::ptree& pt=tree ? *tree : _tree;
  if(useXs)
    frm=(sizeType)_xs.size()-1;
  else frm=pt.get<sizeType>("frm");

  Vec x,u;
  recreate(path);
  for(sizeType i=0; i<frm; i++) {
    _env.sol().setCons(FEMLCPSolver::Contacts());
    if(useXs) {
      x=_xs[i];
      u.setZero(0);
      _env.sol().setControlInput(Vec::Zero(_env.sol().getMCalc()._innerMCalc->size()));
    } else {
      x=parseVec<Vec>("x",i+1,pt);
      u=parseVec<Vec>("u",i,pt);
      _env.sol().setControlInput(u);
      if(i < (sizeType)_info._infos.size())
        _env.sol().setCons(_info._infos[i].getCons());
    }
    ASSERT(x.size() == nrX())
    string pathFrm=path+"/frm"+boost::lexical_cast<string>(i)+".vtk";
    if(pov)
      writeStatePov(pathFrm,x);
    else writeStateVTK(pathFrm,x);
  }
}
void FEMDDPSolver::resetParam(sizeType maxIter,sizeType maxIterILQG,sizeType horizon)
{
  Vec S=_env.sampleS0();
  Vec A=_env.sampleA(0,S);
  _tree.put<sizeType>("frm",0);
  assignVec(S,"x",0,_tree);
  assignVec(A,"u",0,_tree);
  DDPSolver::resetParam(maxIter,maxIterILQG,horizon);
  setBound();
}
void FEMDDPSolver::setBound()
{
  //set action scale
  FEMLCPSolver& sol=static_cast<FEMEnvironment&>(_env).sol();
  scalar actionScale=_env.pt().get<scalar>("actionScale",1);
  if(sol.getBody()._basis) {
    Matd stiff=_env.getStiffness();
    Vec scale=(stiff.diagonal().array().inverse()*stiff.diagonal().minCoeff()).sqrt().matrix()*actionScale;
    _bound.reset(new Vec(scale));
  }
}
//debug and visualization
void FEMDDPSolver::debugObj(const Vec& x,const Vec& u,bool addObjUX)
{
#define PEAK_FRAME 20
#define NR_TEST 10
#define DELTA 1E-8f
  scalarD dt=_env.sol()._tree.get<scalar>("dt");
  sizeType dim=_env.sol().getBody().dim();
  _env.addEnergyBalance(randVec(dim),randVec(dim),RandEngine::randR01());
  _env.addEnergyJump(RandEngine::randR01(),PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalk(randVec(dim),RandEngine::randR01());
  _env.addEnergyWalkTo(randVec(dim),true,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalkTo(randVec(dim),false,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalkTo(randVec(dim),true,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalkTo(randVec(dim),false,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalkSpeedTo(randVec(dim),true,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalkSpeedTo(randVec(dim),false,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalkSpeedTo(randVec(dim),true,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyWalkSpeedTo(randVec(dim),false,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  if(dim == 2)
    _env.addEnergy2DRoll(RandEngine::randR01(),RandEngine::randR01());
  else _env.addEnergy3DRoll(Vec3::Random(),RandEngine::randR01());
  _env.addEnergy3DOrientation(randVec(dim),randVec(dim),RandEngine::randR01());
  _env.addEnergy3DOrientationTo(randVecR(dim),PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
  _env.addEnergyCurvedWalk(randVec(dim),randVec(dim),RandEngine::randR01());

  Matd fxx,fjac;
  Vec fx,fx2,fvec,fvec2;
  scalarD df,dfN,ddf,ddfN;
  scalarD f=DDPSolver::objX(PEAK_FRAME,x,fx,fxx);

  sizeType nrF=nrObjXF();
  fvec=objXF(PEAK_FRAME,x,&fjac);
  ASSERT(fvec.size() == nrF)
  INFOV("LM: %f Err: %f",fvec.squaredNorm()/2,fvec.squaredNorm()/2-f)

  for(sizeType i=0; i<NR_TEST; i++) {
    Vec deltaX=Vec::Random(x.size());
    if(dim == 2) {
      deltaX.segment<3>(deltaX.size()/2-4).setZero();
      deltaX.segment<3>(deltaX.size()-4).setZero();
    }
    df=fx.dot(deltaX);
    dfN=DDPSolver::objX(PEAK_FRAME,x+deltaX*DELTA,fx2);
    dfN=(dfN-f)/DELTA;
    INFOV("ObjGradient: %f, Err: %f!",df,df-dfN)
    ddf=(fxx*deltaX).norm();
    ddfN=((fx2-fx)/DELTA).norm();
    INFOV("ObjHessian: %f, Err: %f!",ddf,ddf-ddfN)
    fvec2=objXF(PEAK_FRAME,x+deltaX*DELTA,NULL);
    INFOV("ObjLMGradient: %f,Err: %f",(fjac*deltaX).norm(),(fjac*deltaX-(fvec2-fvec)/DELTA).norm())
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
#undef PEAK_FRAME
}
void FEMDDPSolver::updateNeuralNet(bool debugMode,sizeType maxIt,scalar epsilon,scalar delta)
{
  readFromTree();
  _tree.put<bool>("initContactUpdate",false);
  _env.sol()._tree.put<bool>("inNeuralNetDebug",debugMode);
  getNeuralNet()._tree.put<sizeType>("reportInterval",100);
  getNeuralNet()._tree.put<sizeType>("maxIterations",maxIt);
  if(epsilon >= 0)
    getNeuralNet()._tree.put<scalar>("epsilon",epsilon);
  if(delta >= 0)
    getNeuralNet()._tree.put<scalar>("delta",delta);
  boost::shared_ptr<CIOOptimizer> opt=createOpt(true,true);
  opt->updateNeuralNet(NULL,false);
  opt->assignControl();
  writeToTree();
}
void FEMDDPSolver::writeResidualInfo(bool SP,bool MP)
{
  readFromTree();
  _tree.put<bool>("initContactUpdate",false);
  boost::shared_ptr<CIOOptimizer> opt=createOpt(SP,MP);
  opt->writeResidueInfo(opt->collectSolution(true));
}
void FEMDDPSolver::assignControl(bool SP)
{
  readFromTree();
  _tree.put<bool>("initContactUpdate",false);
  boost::shared_ptr<CIOOptimizer> opt=createOpt(SP,true);
  opt->assignControl();
  writeToTree();
}
void FEMDDPSolver::recenter(boost::property_tree::ptree* tree,bool useXs)
{
  boost::property_tree::ptree& pt=tree ? *tree : _tree;
  sizeType frm;
  if(useXs)
    frm=(sizeType)_xs.size()-1;
  else frm=pt.get<sizeType>("frm");

  Vec x0=parseVec<Vec>("x",0,pt);
  Vec3d off=x0.segment<3>(x0.size()/2-6);
  INFOV("Initial location: (%f,%f,%f)",off[0],off[1],off[2])
  //exclude gravity
  Vec3d g=_env.getGravity().cast<scalarD>();
  if(g.norm() > 1E-10f) {
    g/=g.norm();
    off-=off.dot(g)*g;
  }
  //move whole sequence
  for(sizeType i=0; i<=frm; i++) {
    Vec x=parseVec<Vec>("x",i,pt);
    x.segment<3>(x.size()/2-6)-=off;
    x.segment<3>(x.size()-6)-=off;
    assignVec(x,"x",i,pt);
  }
}
//for cio
struct LSSKey {
  bool operator()(const pair<int,Vec3>& a,const pair<int,Vec3>& b) const {
    return a.first < b.first;
  }
};
FEMDDPSolver::Vec FEMDDPSolver::sampleInit(const boost::property_tree::ptree& pt,sizeType f) const
{
  vector<pair<int,Vec3> > rots;
  for(sizeType i=0;; i++)
    if(pt.get_optional<sizeType>("key"+boost::lexical_cast<string>(i))) {
      pair<int,Vec3> key;
      key.first=pt.get<sizeType>("key"+boost::lexical_cast<string>(i));
      key.second.x()=pt.get<scalar>("rotX"+boost::lexical_cast<string>(i),0);
      key.second.y()=pt.get<scalar>("rotY"+boost::lexical_cast<string>(i),0);
      key.second.z()=pt.get<scalar>("rotZ"+boost::lexical_cast<string>(i),0);
      rots.push_back(key);
    } else break;
  ASSERT_MSG((sizeType)rots.size() > 0,"Not keypoint found!")
  sort(rots.begin(),rots.end(),LSSKey());
  if(f <= rots.front().first) {
    return rots.front().second;
  } else if(f >= rots.back().first) {
    return rots.back().second;
  } else {
    for(sizeType i=0; i<(sizeType)rots.size()-1; i++) {
      sizeType a=rots[i].first;
      sizeType b=rots[i+1].first;
      if(a <= f && b >= f)
        return interp1D(rots[i].second,rots[i+1].second,(scalar)(f-a)/(scalar)(b-a));
      //return rots[i].second;
    }
    ASSERT(false)
    return Vec3::Zero();
  }
}
void FEMDDPSolver::initializeCIO(const string& name)
{
  FEMCIOEngine& eng=_env.sol();
  bool onGround=eng._geom != NULL;
  bool randomizeDeformationOnly=_tree.get<bool>("randomizeDeformationOnly",true);
  Vec x0=parseVec<Vec>("x",_tree.get<sizeType>("frm"),_tree);
  scalar randomScale=_tree.get<scalar>("randomizedScale",0);
  sizeType horizon=_tree.get<sizeType>("horizon"),nrS=x0.size()/2;
  if(randomScale > 0) {
    INFOV("Initialize to random scale=%f %s (%s)!",
          randomScale,onGround ? "on ground" : "in water",
          (onGround || randomizeDeformationOnly) ? "deform" : "deform+rigid")
  }
  //initialize state
  _info._infos.resize(horizon);
  _info._xss.resize(horizon+2);
  for(sizeType i=0; i<horizon+2; i++) {
    Vec x;
    Vec3 rot;
    Vec3 xOff;
    xOff[0]=_tree.get<scalar>("smartInitialization.offX",0);
    xOff[1]=_tree.get<scalar>("smartInitialization.offY",0);
    xOff[2]=_tree.get<scalar>("smartInitialization.offZ",0);
    if(i == 0) {
      x=x0.segment(0  ,nrS);
      x.segment<3>(x.size()-6)+=xOff;
      _info._xss[i].reset(eng.getMCalc(),x);
    } else if(i == 1) {
      x=x0.segment(nrS,nrS);
      x.segment<3>(x.size()-6)+=xOff;
      _info._xss[i].reset(eng.getMCalc(),x);
    } else if(_tree.get_child_optional("smartInitialization")) {
      INFOV("Smart sampling frame: %ld/%ld!",i,(horizon+2))
      x=_info._xss[i-1].x();
      rot=sampleInit(_tree.get_child("smartInitialization"),i);
      if(rot == x.segment<3>(x.size()-3)) {
        _info._xss[i]=_info._xss[i-1];
      } else {
        x.segment<3>(x.size()-3)=rot;
        //x=_env.sampleS0Rot(rot).segment(0,nrS);
        _info._xss[i].reset(eng.getMCalc(),x);
      }
    } else {
      _info._xss[i]=_info._xss[i-1];
    }
    //perturb
    if(randomScale > 0) {
      Vec delta=Vec::Random(_info._xss[i].x().size());
      if(onGround || randomizeDeformationOnly)
        delta.segment<6>(delta.size()-6).setZero();
      _info._xss[i].reset(eng.getMCalc(),_info._xss[i].x()+delta*randomScale);
    }
  }
  write(name);
}
void FEMDDPSolver::optimizeCIO(bool MP)
{
  initializeCIO();
  //optimize
  boost::shared_ptr<CIOOptimizer> opt=createOpt(false,MP);
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  cb.writeToFile(_tree.get<string>("writeToFile",""));
  opt->reset();
  opt->setTolF(_tree.get<scalar>("epsF"));
  opt->setTolG(_tree.get<scalar>("epsG"));
  opt->setTolX(_tree.get<scalar>("epsX"));
  opt->setMaxIter(_tree.get<sizeType>("maxIter"));
  Vec x=opt->collectSolution(true);
  INFOV("Using epsGNorm: %f",_tree.get<scalar>("epsG"))
  //opt.solve(x);
  opt->writeResidueInfo(x);
  opt->solveCommonFile(x,cb);
  opt->writeResidueInfo(x);
  opt->updateConfig(x,true);
  cb.reset();
  INFOV("Termination type: %ld, using %ld iterations",
        opt->getTermination(),opt->getIterationsCount())
  //assign solution
  scalarD FX;
  Vec DFDX=x;
  opt->gradient(x,FX,DFDX);
  write();
}
void FEMDDPSolver::debugCIOGradient(bool SP,bool MP,bool load)
{
  if(load)
    read();
  else initializeCIO();
  //check gradient
  FEMCIOEngine& eng=_env.sol();
  boost::shared_ptr<CIOOptimizer> opt=createOpt(SP,MP);
  //opt->beginSingleOutTerm<CIOOptimizerPhysicsTerm>();
  opt->randomizeParameter();
  //compute mask
  Vec x=opt->collectSolution(true);
  Vec mask=Vec::Ones(x.size());
  if(eng.getBody().dim() == 2)
    mask=opt->get2DMask();
  //test
  INFO("CIO Gradient")
  opt->debugGradient(x,mask);
  //opt->endSingleOutTerm();
}
//for cio using ADLM
void FEMDDPSolver::optimizeCIOLM(bool MP,bool load)
{
  if(load)
    read();
  else initializeCIO();

  //initialize
  _tree.put<bool>("adaptiveADMMPenalty",false);
  boost::shared_ptr<CIOOptimizer> prob=createOpt(false,MP);
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  cb.writeToFile(_tree.get<string>("writeToFile",""));
  LMInterface opt;
#ifdef SUITESPARSE_SUPPORT
  disableTiming();
  opt.useInterface(boost::shared_ptr<CholmodWrapper>(new CholmodWrapper));
#endif
  opt.setTolF(_tree.get<scalar>("epsF"));
  opt.setTolG(_tree.get<scalar>("epsG"));
  opt.setTolX(_tree.get<scalar>("epsX"));
  opt.setMaxIter(_tree.get<sizeType>("maxIter"));
  INFOV("Using epsGNorm: %f",_tree.get<scalar>("epsG"))
  //optimize
  Vec x=prob->collectSolution(false);
  opt.solveSparse(x,*prob,cb);
  prob->updateConfig(x,false);
  cb.reset();
  //assign solution
  Vec f=x;
  Matd fjac;
  prob->Objective<scalarD>::operator()(x,f,&fjac,false);
  //save result
  write();
}
void FEMDDPSolver::debugCIOLMEnergy(bool SP,bool MP,bool load)
{
  if(load)
    read();
  else initializeCIO();

  //initialize
  _tree.put<bool>("adaptiveADMMPenalty",false);
  _tree.put<bool>("handleSelfCollision",true);
  boost::shared_ptr<CIOOptimizer> opt=createOpt(SP,MP);
  opt->randomizeParameter();
  //check gradient
  scalarD FX,FX2;
  Objective<scalarD>::SMat fjac;
  Vec xLM=opt->collectSolution(false);
  Vec x=opt->collectSolution(true);
  Vec fvec,fvec2,DFDX=x;
  //check gradient LM
  //opt->beginSingleOutTerm<CIOOptimizerPhysicsTerm>();
  opt->Objective<scalarD>::operator()(xLM,fvec,&fjac,false);
  FX2=fvec.squaredNorm()/2;
  opt->gradient(x,FX,DFDX);
  INFOV("FX: %f FXLM: %f Err: %f",FX,FX2,FX-FX2)
  INFOV("Grad: %f GradLM: %f Err: %f",
        (fjac.transpose()*fvec).norm(),opt->removeCF(DFDX).norm(),
        (fjac.transpose()*fvec-opt->removeCF(DFDX)).norm())
  //check gradient LM with force updated
  opt->Objective<scalarD>::operator()(xLM,fvec,&fjac,false);
  FX2=fvec.squaredNorm()/2;
#define NR_TEST 10
#define DELTA 1E-9f
  //otherwise, just check for gradients of energy
  {
    xLM=opt->collectSolution(false);
    opt->Objective<scalarD>::operator()(xLM,fvec,&fjac,false);
    for(sizeType i=0; i<NR_TEST; i++) {
      Vec delta=Vec::Random(xLM.size());
      opt->Objective<scalarD>::operator()(xLM+delta*DELTA,fvec2,(Matd*)NULL,false);
      INFOV("LMGradient: %f Err: %f",(fjac*delta).norm(),(fjac*delta-(fvec2-fvec)/DELTA).norm())
    }
  }
#undef DELTA
#undef NR_TEST
  //opt->endSingleOutTerm();
}
void FEMDDPSolver::debugCIOForce(bool SP,bool MP,bool load)
{
  if(load)
    read();
  else initializeCIO();

  //initialize
  _tree.put<bool>("forceInCone",false);
  boost::shared_ptr<CIOOptimizer> opt=createOpt(SP,MP);
  if(!opt->hasContact())
    return;

  //try twice, one using updateFFunc, the other using operator(modifiable)
  for(sizeType it=0; it<2; it++) {
    opt->randomizeContact();
    opt->randomizeParameter(false,false);
    //check gradient
    scalarD FX;
    Vec x=opt->collectSolution(true),DFDX0=x,DFDX1=x;
    //before
    opt->gradient(x,FX,DFDX0);
    //update force
    if(it == 0) {
      opt->updateConfig(x,true);
      opt->updateForce();
    } else {
      Vec fvec=Vec::Zero(opt->values());
      opt->operator()(x,fvec,NULL,true);
    }
    //after
    x=opt->collectSolution(true);
    opt->gradient(x,FX,DFDX1);
    //compare
    opt->compareDFDX(DFDX0,DFDX1);
  }
}
void FEMDDPSolver::debugCIOSQPEnergy(bool SP,bool MP,bool linearApprox,bool load)
{
  _tree.put<scalar>("regShuffleAvoidance",RandEngine::randR01());
  if(load)
    read();
  else initializeCIO();

  //initialize
  boost::shared_ptr<CIOOptimizer> opt=createOpt(SP,MP);
  //compute approximate quadratic energy
  CIOOptimizer::SMat H;
  Vec x=opt->collectSolution(false),f,G,G2;

#define NR_TEST 10
#define DELTA 1E-15f
  //debug SQPGradient
  for(sizeType i=0; i<NR_TEST; i++) {
    //note that you have to first call LM before SQP
    opt->Objective<scalarD>::operator()(x,f,(Matd*)NULL,false);
    scalarD E=opt->Objective<scalarD>::operator()(x,&G,&H);
    //debug gradient
    Vec delta=Vec::Random(opt->inputs());
    //note that you have to first call LM before SQP
    if(linearApprox) {
      FEMDDPSolverInfo infoLast=_info;
      opt->Objective<scalarD>::operator()(x+delta*DELTA,f,(Matd*)NULL,false);
      _info._infos=infoLast._infos;
      for(sizeType i=0; i<(sizeType)_info._infos.size(); i++) {
        CIOContactInfo& info=_info._infos[i];
        info.updateXVGLinearized(_info._xss[i+2].x()-infoLast._xss[i+2].x(),
                                 _info._xss[i+1].x()-infoLast._xss[i+1].x());
      }
    } else opt->Objective<scalarD>::operator()(x+delta*DELTA,f,(Matd*)NULL,false);
    scalarD E2=opt->Objective<scalarD>::operator()(x+delta*DELTA,&G2,(Matd*)NULL);
    INFOV("SQPGradient: %f Err: %f",G.dot(delta),G.dot(delta)-(E2-E)/DELTA)
    INFOV("SQPHessian: %f Err: %f",(H*delta).norm(),(H*delta-(G2-G)/DELTA).norm())
  }
#undef DELTA
#undef NR_TEST
}
//for policy search
void FEMDDPSolver::optimizeCIOPolicy(bool SP,bool MP,bool load)
{
  if(load)
    read();
  else initializeCIO();
  //initialize
  boost::shared_ptr<CIOOptimizer> prob=createOpt(SP,MP);
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  cb.writeToFile(_tree.get<string>("writeToFile",""));
  cb._rollback=false;
  //initialize time span
  NeuralNet& net=getNeuralNet();
  scalar dt=_env.sol()._tree.get<scalar>("dt");
  sizeType horizon=_tree.get<sizeType>("horizon");
  net._tree.put<scalar>("initTimeSpan",(scalar)horizon*dt);
  net.resetOffline();
  //cb._rollback=false;
  LMInterface opt;
#ifdef SUITESPARSE_SUPPORT
  disableTiming();
  opt.useInterface(boost::shared_ptr<CholmodWrapper>(new CholmodWrapper));
#endif
  opt.setTolF(_tree.get<scalar>("epsF"));
  opt.setTolG(_tree.get<scalar>("epsG"));
  opt.setTolX(_tree.get<scalar>("epsX"));
  opt.setMaxIter(_tree.get<sizeType>("maxIter"));
  //optimize
  scalarD minPolicyDelta=_tree.get<scalar>("minPolicyDelta",1E-3f)*prob->valuesNoCollision();
  sizeType maxPolicyIter=_tree.get<sizeType>("maxIterPolicy",_tree.get<sizeType>("maxIterILQG"));
  Vec x=prob->collectSolution(false);
  INFOV("Using epsGNorm: %f, minPolicyDelta: %f",_tree.get<scalar>("epsG"),minPolicyDelta)
  for(sizeType it=0; it < maxPolicyIter; it++) {
    cb.reset();
    opt.solveSparse(x,*prob,cb);
    cb.reset();
    prob->updateConfig(x,false);
    prob->updateNeuralNet(NULL,false);
    prob->updateWeight();
    WARNINGV("CIOPolicyIter%ld: f0=%f, f1=%f, minPolicyDelta=%f",it,cb._f0,cb._f1,minPolicyDelta)
    if(cb._f1 > cb._f0-minPolicyDelta)
      break;
  }
  //assign solution
  Vec f=x;
  Matd fjac;
  prob->Objective<scalarD>::operator()(x,f,&fjac,false);
  //save result
  write();
}
void FEMDDPSolver::debugNeuralNet(bool load)
{
#define DELTA 1E-8f
  if(load)
    read();
  else initializeCIO();
  _env.sol()._tree.put<bool>("inNeuralNetDebug",true);
  _env.sol()._tree.put<bool>("adaptiveADMMPenalty",false);
  _env.resetPolicy();
  _env.getPolicy();
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("./ddp.dat",ios::binary);
    write(os,dat.get());
  }
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is("./ddp.dat",ios::binary);
    read(is,dat.get());
  }
  boost::shared_ptr<CIOOptimizer> opt=createOpt(true,true);
  opt->getTerm<CIOOptimizerPhysicsTerm>()->randomizeParam();
  opt->randomizeParameter();
  NeuralNet& net=getNeuralNet();
  //randomize neural-net parameter
  Vec weights=net.toVec(false);
  weights.setRandom();
  net.fromVec(weights);
  INFOV("NeuralNet has %ld params!",weights.size())

  Vec fvec,fvec2;
  sizeType NR_TEST=_tree.get<sizeType>("debugNeuralNetIter",20);
  for(sizeType it=0; it<NR_TEST; it++) {
    //check gradient
    weights=net.toVec(false);
    opt->operator()(opt->collectSolution(false),fvec,NULL,false);
    Vec delta=Vec::Random(weights.size());
    net.fromVec(weights+delta*DELTA);
    opt->operator()(opt->collectSolution(false),fvec2,NULL,false);
    net.fromVec(weights);
    INFOV("NeuralNet Iter%ld: %f Gradient: %f",it,fvec.squaredNorm()/2,(fvec2.squaredNorm()-fvec.squaredNorm())/DELTA)
    //optimize neural-net
    getNeuralNet()._tree.put<scalar>("epsilon",0);
    getNeuralNet()._tree.put<scalar>("delta",0);
    opt->updateNeuralNet(NULL,false);
  }
  _env.sol()._tree.put<bool>("inNeuralNetDebug",false);
  _env.resetPolicy();
  _env.getPolicy();
#undef DELTA
}
NeuralNet& FEMDDPSolver::getNeuralNet() const
{
  return *(_env.getPolicy()._net);
}
//objective function and dynamics
scalarD FEMDDPSolver::objX(sizeType i,const Vec& x,Vec* fx,Matd* fxx) const
{
  return _env.getCost(i,x,Vec(),fx,fxx);
}
FEMDDPSolver::Vec FEMDDPSolver::objXF(sizeType i,const Vec& x,Matd* fjac) const
{
  return _env.getCostF(i,x,Vec(),fjac);
}
sizeType FEMDDPSolver::nrObjXF() const
{
  return _env.nrCostF();
}
//multi-task version
scalarD FEMDDPSolver::objXMT(sizeType i,const Vec& x,Vec* fx,Matd* fxx,sizeType K) const
{
  return dynamic_cast<FEMEnvironmentMT&>(_env).getCostMT(i,x,Vec(),fx,fxx,K);
}
FEMDDPSolver::Vec FEMDDPSolver::objXFMT(sizeType i,const Vec& x,Matd* fjac,sizeType K) const
{
  return dynamic_cast<FEMEnvironmentMT&>(_env).getCostFMT(i,x,Vec(),fjac,K);
}
sizeType FEMDDPSolver::nrObjXFMT(sizeType K) const
{
  return dynamic_cast<FEMEnvironmentMT&>(_env).nrCostFMT(K);
}
//dynamics
void FEMDDPSolver::dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu) const
{
  dfdxc(i,x,u,f,fx,fu,NULL);
}
void FEMDDPSolver::dfdxc(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu,FEMLCPSolver::Contacts* cons) const
{
  FEMCIOEngine& eng=_env.sol();
  FEMEnvironment& env=static_cast<FEMEnvironment&>(_env);
  scalarD dt=eng._tree.get<scalar>("dt");
  if(_env.sol()._tree.get<bool>("advanceImplicit",true))
    ::dfdxImplicit(env,dt,i,x,u,f,fx,fu,cons);
  else ::dfdxExplicit(env,dt,i,x,u,f,fx,fu,cons);
}
//helper
const FEMEnvironment& FEMDDPSolver::getEnv() const
{
  return _env;
}
FEMEnvironment& FEMDDPSolver::getEnv()
{
  return _env;
}
const FEMCIOEngine& FEMDDPSolver::getEngine() const
{
  return _env.sol();
}
FEMCIOEngine& FEMDDPSolver::getEngine()
{
  return _env.sol();
}
//getter/setter
FEMDDPSolver::FEMDDPSolver(const string& name,FEMEnvironment& env)
  :Serializable(name),_env(env) {}
void FEMDDPSolver::readFromTree(const RigidReducedMCalculator& MCalc,FEMDDPSolverInfo& info,
                                const boost::property_tree::ptree& pt,sizeType horizon)
{
  sizeType nrS=MCalc.size();
  Vec x,u;
  //load initial x
  x=parseVec<Vec>("x",0,pt);
  info._xss[0].reset(MCalc,x.segment(0,nrS));
  info._xss[1].reset(MCalc,x.segment(nrS,nrS));
  for(sizeType i=0; i<horizon; i++) {
    //load u
    u=parseVec<Vec>("u",i,pt);
    info._infos[i].setU(u);
    //load x
    x=parseVec<Vec>("x",i+1,pt);
    info._xss[i+1].reset(MCalc,x.segment(0,nrS));
    info._xss[i+2].reset(MCalc,x.segment(nrS,nrS));
  }
}
void FEMDDPSolver::writeToTree(const RigidReducedMCalculator& MCalc,const FEMDDPSolverInfo& info,
                               boost::property_tree::ptree& pt,sizeType horizon)
{
  sizeType nrS=MCalc.size();
  Vec x=Vec::Zero(nrS*2);
  //save initial x
  x.segment(0,nrS)=info._xss[0].x();
  x.segment(nrS,nrS)=info._xss[1].x();
  assignVec<Vec>(x,"x",0,pt);
  pt.put<sizeType>("frm",horizon);
  for(sizeType i=0; i<horizon; i++) {
    //save u
    assignVec<Vec>(info._infos[i].u(),"u",i,pt);
    //save x
    x.segment(0,nrS)=info._xss[i+1].x();
    x.segment(nrS,nrS)=info._xss[i+2].x();
    assignVec<Vec>(x,"x",i+1,pt);
  }
}
boost::shared_ptr<CIOOptimizer> FEMDDPSolver::createOpt(bool SP,bool MP,FEMDDPSolverInfo& info,sizeType K)
{
  sizeType horizon=_tree.get<sizeType>("horizon");
  boost::shared_ptr<CIOOptimizer> ret;
  if(SP)
    ret=boost::shared_ptr<CIOOptimizer>(new CIOPolicyOptimizer(*this,horizon,info,K));
  else if(MP)
    ret=boost::shared_ptr<CIOOptimizer>(new CIOOptimizerMP(*this,horizon,info,K));
  else ret=boost::shared_ptr<CIOOptimizer>(new CIOOptimizer(*this,horizon,info,K));
  ret->reset();
  return ret;
}
boost::shared_ptr<CIOOptimizer> FEMDDPSolver::createOpt(bool SP,bool MP)
{
  return createOpt(SP,MP,_info);
}
