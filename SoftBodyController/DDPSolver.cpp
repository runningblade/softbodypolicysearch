#include "DDPSolver.h"
#include "Environment.h"
#include "QPInterface.h"
#include <CMAES/MLUtil.h>
#include <Dynamics/FEMUtils.h>
#include <boost/lexical_cast.hpp>
#include <CommonFile/MakeMesh.h>
#include <CommonFile/IO.h>

PRJ_BEGIN

//helper
void parseObjective(Vec2d& target,vector<Vec3d,Eigen::aligned_allocator<Vec3d> >& balls,const boost::property_tree::ptree& tree)
{
  target[0]=tree.get<scalar>("targetX");
  target[1]=tree.get<scalar>("targetY");
  for(sizeType i=0;; i++) {
    boost::optional<const boost::property_tree::ptree&> child=
      tree.get_child_optional("ball"+boost::lexical_cast<string>(i));
    if(!child)
      break;
    balls.push_back(Vec3d(child->get<scalar>("ctrX"),child->get<scalar>("ctrY"),child->get<scalar>("rad")));
  }
}
//solver implementation
DDPSolver::~DDPSolver() {}
void DDPSolver::writeStatesVTK(const string& path,const boost::property_tree::ptree* tree,bool useXs) const
{
  sizeType frm=0;
  const boost::property_tree::ptree& pt=tree ? *tree : _tree;
  if(useXs)
    frm=(sizeType)_xs.size()-1;
  else frm=pt.get<sizeType>("frm");

  Vec x;
  recreate(path);
  for(sizeType i=0; i<=frm; i++) {
    if(useXs)
      x=_xs[i];
    else x=parseVec<Vec>("x",i,pt);
    ASSERT(x.size() == nrX())
    writeStateVTK(path+"/frm"+boost::lexical_cast<string>(i)+".vtk",x);
  }
}
void DDPSolver::resetParam(sizeType maxIter,sizeType maxIterILQG,sizeType horizon)
{
  if(!_tree.get_optional<sizeType>("maxIter"))
    setMaxIter(maxIter);
  if(!_tree.get_optional<sizeType>("maxIterILQG"))
    setMaxIterILQG(maxIterILQG);
  if(!_tree.get_optional<sizeType>("horizon"))
    setHorizon(horizon);
  if(!_tree.get_optional<sizeType>("adopt"))
    setAdopt(1);
}
void DDPSolver::resetEps(scalar epsF,scalar epsG,scalar epsX)
{
  _tree.put<scalar>("epsF",epsF);
  _tree.put<scalar>("epsG",epsG);
  _tree.put<scalar>("epsX",epsX);
}
void DDPSolver::resetTrajectory(boost::property_tree::ptree& t)
{
  sizeType frm=_tree.get<sizeType>("frm");
  t.put<sizeType>("frm",frm);
  for(sizeType i=0;; i++) {
    string nameX="x"+boost::lexical_cast<string>(i);
    string nameU="u"+boost::lexical_cast<string>(i);
    boost::optional<boost::property_tree::ptree&> u=_tree.get_child_optional(nameU);
    boost::optional<boost::property_tree::ptree&> x=_tree.get_child_optional(nameX);
    if(!u && !x)
      break;
    //add to target tree
    if(u && i < frm)
      t.add_child(nameU,*u);
    if(x && i <= frm)
      t.add_child(nameX,*x);
    //remove from current tree
    if(x && i > 0)
      _tree.erase(nameX);
    if(u && i > 0)
      _tree.erase(nameU);
  }
  _tree.put("frm",0);
}
void DDPSolver::setMaxIterILQG(sizeType maxIterILQG)
{
  _tree.put<sizeType>("maxIterILQG",maxIterILQG);
}
void DDPSolver::setMaxIter(sizeType maxIter)
{
  _tree.put<sizeType>("maxIter",maxIter);
}
void DDPSolver::setHorizon(sizeType horizon)
{
  _tree.put<sizeType>("horizon",horizon);
}
void DDPSolver::setAdopt(sizeType adopt)
{
  _tree.put<sizeType>("adopt",adopt);
}
DDPSolver::Vec DDPSolver::initialize(sizeType horizon) const
{
  Vec u0=parseVec<Vec>("u",0,_tree),u=Vec::Zero(u0.size()*horizon);
  for(sizeType i=0,frm=_tree.get<sizeType>("frm"); i<horizon; i++,frm++) {
    Vec uFrm=parseVec<Vec>("u",frm,_tree);
    ASSERT(uFrm.size() == u0.size() || uFrm.size() == 0)
    if(uFrm.size() == 0)
      break;
    else u.segment(i*u0.size(),u0.size())=uFrm;
  }
  return u;
}
void DDPSolver::adopt(const Vec& u,sizeType horizon,Vss* xsP)
{
  sizeType frm=_tree.get<sizeType>("frm");
  sizeType adopt=_tree.get<sizeType>("adopt");
  sizeType adoptInterval=_tree.get<sizeType>("adoptInterval",1);
  sizeType nrU=u.size()/horizon;

  ASSERT(adopt*adoptInterval <= horizon)
  Vec x=parseVec<Vec>("x",frm,_tree),fx;
  if(xsP) {
    xsP->resize(frm+adopt+1);
    xsP->at(frm)=x;
  }
  for(sizeType i=0,k=0; i<adopt; i++) {
    for(sizeType j=0;j<adoptInterval;j++,k++) {
      dfdx(i,x,u.segment(k*nrU,nrU),fx);
      if(j<adoptInterval-1)
        x.swap(fx);
    }
    assignVec(fx,"x",frm+i+1,_tree);
    if(xsP)
      xsP->at(frm+i+1)=fx;
    x.swap(fx);
  }
  for(sizeType i=0,k=0; i<horizon; i+=adoptInterval) {
    Vec ui=Vec::Zero(nrU);
    for(sizeType j=0;j<adoptInterval;j++,k++)
      ui+=u.segment(k*nrU,nrU);
    assignVec(ui/(scalar)adoptInterval,"u",frm+i,_tree);
  }
  _tree.put<sizeType>("frm",frm+adopt);
}
sizeType DDPSolver::nrX() const
{
  return parseVec<Vec>("x",0,_tree).size();
}
sizeType DDPSolver::nrU() const
{
  return parseVec<Vec>("u",0,_tree).size();
}
//for trajectory opt
void DDPSolver::writePT(const Vec& us,const Vss& xsP,boost::property_tree::ptree& pt,sizeType horizon) const
{
  sizeType szU=nrU();
  pt.put<sizeType>("frm",horizon);
  for(sizeType i=0; i<horizon; i++)
    assignVec(us.segment(i*szU,szU),"u",i,pt);
  for(sizeType i=0; i<=horizon; i++)
    assignVec(xsP[i],"x",i,pt);
}
void DDPSolver::evaluate(Vec& us,scalarD& FX,bool quadraticApprox,Vss& xsP,
                         const Vss* ks,const Mss* Ks,const scalarD* alpha,
                         boost::property_tree::ptree* pt)
{
  sizeType szU=nrU(),horizon;
  if(ks && Ks)
    horizon=ks->size();
  else horizon=us.size()/szU;
  xsP.resize(horizon+1);
  xsP[0]=parseVec<Vec>("x",_tree.get<sizeType>("frm"),_tree);
  if(ks && Ks)
    us=_us0;

  //forward pass
  FX=objX(0,xsP[0]);
  for(sizeType i=0; i<horizon; i++) {
    if(ks && Ks && alpha)
      us.segment(i*szU,szU)=_us0.segment(i*szU,szU)+(*alpha*ks->at(i)+Ks->at(i)*(xsP[i]-_xs[i]));
    if(quadraticApprox)
      xsP[i+1]=_xs[i+1]+_fxs[i]*(xsP[i]-_xs[i])+_fus[i]*(us-_us0).segment(i*szU,szU);
    else dfdx(i,xsP[i],us.segment(i*szU,szU),xsP[i+1]);
    FX+=objX(i+1,xsP[i+1])+objU(i,us.segment(i*szU,szU));
  }

  //write result
  if(pt)
    writePT(us,xsP,*pt,horizon);
}
DDPSolver::Vec DDPSolver::solveTrajOpt()
{
  //solve
  sizeType horizon=_tree.get<sizeType>("horizon");
  Vec u=initialize(horizon);
  TrajOptimizer opt(*this,horizon);
  opt.reset();
  opt.setTolF(_tree.get<scalar>("epsF"));
  opt.setTolG(_tree.get<scalar>("epsG"));
  opt.setTolX(_tree.get<scalar>("epsX"));
  opt.setMaxIter(_tree.get<sizeType>("maxIter"));
  if(_bound)
    opt.setBound(*_bound);
  if(!_tree.get<bool>("recomputeDeriv",true))
    collectDerivatives();
  opt.solve(u);
  adopt(_us0=u,horizon,&_xs);
  return u;
}
//for ilqg
void DDPSolver::increaseMu(scalarD& mu,scalarD& delta) const
{
  scalarD delta0=2.0f,muMin=_tree.get<scalarD>("minMu",1E-4f);
  delta=max<scalarD>(delta0,delta*delta0);
  mu=max<scalarD>(muMin,mu*delta);
}
void DDPSolver::decreaseMu(scalarD& mu,scalarD& delta) const
{
  scalarD delta0=2.0f,muMin=_tree.get<scalarD>("minMu",1E-10f);
  delta=min<scalarD>(1/delta0,delta/delta0);
  mu=mu*delta > muMin ? mu*delta : 0;
}
void DDPSolver::collectDerivatives(Vss* objXq,Vss* objUq,Mss* objXQ,Mss* objUQ)
{
  sizeType szU=nrU(),horizon=_us0.size()/szU;
  ASSERT(_us0.size() == horizon*szU)
  //state derivatives
  _xs.resize(horizon+1);
  _fxs.resize(horizon);
  _fus.resize(horizon);
  _xs[0]=parseVec<Vec>("x",_tree.get<sizeType>("frm"),_tree);
  for(sizeType i=0; i<horizon; i++)
    dfdx(i,_xs[i],_us0.segment(i*szU,szU),_xs[i+1],_fxs[i],_fus[i]);
  //objective derivatives
  if(objXq && objUq && objXQ && objUQ) {
    objXq->resize(horizon+1);
    objXQ->resize(horizon+1);
    objUq->resize(horizon);
    objUQ->resize(horizon);
    for(sizeType i=0; i<horizon; i++) {
      objU(i,_us0.segment(i*szU,szU),&(*objUq)[i],&(*objUQ)[i]);
      objX(i,_xs[i],&(*objXq)[i],&(*objXQ)[i]);
    }
    objX(horizon,_xs[horizon],&(*objXq)[horizon],&(*objXQ)[horizon]);
  }
}
void DDPSolver::ILQGIter(Vec& usOpt)
{
  //debugGradient();
  collectDerivatives();
  sizeType horizon=_tree.get<sizeType>("horizon");
  sizeType nrU=_fus[0].cols();

  _tree.put<bool>("recomputeDeriv",false);
  TrajOptimizer opt(*this,horizon);
  opt.reset();
  opt.setTolF(_tree.get<scalar>("epsF"));
  opt.setTolG(_tree.get<scalar>("epsG"));
  opt.setTolX(_tree.get<scalar>("epsX"));
  opt.setMaxIter(_tree.get<sizeType>("maxIter"));
  if(_bound) {
    Vec CB=Vec::Zero(nrU*horizon);
    for(sizeType i=0; i<horizon; i++)
      CB.segment(i*nrU,nrU)=*_bound;
    opt.setCB(-CB,CB);
  }
  opt.solve(usOpt=_us0);
}
void DDPSolver::ILQGIterQP(Vec& usOpt)
{
  Vss objXq,objUq;
  Mss objXQ,objUQ;
  collectDerivatives(&objXq,&objUq,&objXQ,&objUQ);
  sizeType horizon=_tree.get<sizeType>("horizon");
  sizeType nrU=_fus[0].cols(),nrX=_fxs[0].cols();

  //build hessian/gradient
  Mss DXDU(horizon,Matd::Zero(nrX,nrU));
  _H=Matd::Zero(nrU*horizon,nrU*horizon);
  _G=Vec::Zero(nrU*horizon);
  for(sizeType i=0; i<horizon; i++) {
    for(sizeType j=0; j<i; j++)
      DXDU[j]=_fxs[i]*DXDU[j];
    DXDU[i]=_fus[i];
    //fill XUpper part
    for(sizeType r=0; r<=i; r++) {
      for(sizeType c=r; c<=i; c++)
        _H.block(r*nrU,c*nrU,nrU,nrU)+=DXDU[r].transpose()*(objXQ[i+1]*DXDU[c]);
      _G.segment(r*nrU,nrU)+=DXDU[r].transpose()*objXq[i+1];
    }
    //fill UUpper part
    _H.block(i*nrU,i*nrU,nrU,nrU)+=objUQ[i];
    _G.segment(i*nrU,nrU)+=objUq[i];
  }
  //fill lower part
  for(sizeType r=0; r<horizon; r++)
    for(sizeType c=0; c<r; c++)
      _H.block(r*nrU,c*nrU,nrU,nrU)=_H.block(c*nrU,r*nrU,nrU,nrU).transpose();
  //solve
  QPInterface opt;
  opt.reset(horizon*nrU,false);
  opt.setTolF(_tree.get<scalar>("epsF"));
  opt.setTolG(_tree.get<scalar>("epsG"));
  opt.setTolX(_tree.get<scalar>("epsX"));
  opt.setMaxIter(_tree.get<sizeType>("maxIter"));
  if(_bound) {
    Vec CB=Vec::Zero(nrU*horizon);
    for(sizeType i=0; i<horizon; i++)
      CB.segment(i*nrU,nrU)=*_bound;
    opt.setCB(-CB,CB);
  }
  opt.setDense(_H,_G-_H*_us0);
  opt.solve(usOpt=_us0);
}
void DDPSolver::ILQGIter(Vss& ks,Mss& Ks,scalarD& mu,scalarD& delta)
{
  Vss objXq,objUq;
  Mss objXQ,objUQ;
  collectDerivatives(&objXq,&objUq,&objXQ,&objUQ);
  sizeType horizon=_tree.get<sizeType>("horizon");
  ks.resize(horizon);
  Ks.resize(horizon);
  //backward pass: update value function
  Vec Vx,Qx,Qu;
  Matd Vxx,Qxx,Quu,Qux,invQuu;
  bool valid=false;
  while(!valid) {
    valid=true;
    Vx=objXq[horizon];
    Vxx=objXQ[horizon];
    for(sizeType i=horizon-1; i>=0; i--) {
      Qx=objXq[i]+_fxs[i].transpose()*Vx;
      Qu=objUq[i]+_fus[i].transpose()*Vx;
      Qxx=objXQ[i]+_fxs[i].transpose()*(Vxx*_fxs[i]);
      Quu=objUQ[i]+_fus[i].transpose()*(Vxx*_fus[i]);
      Qux=_fus[i].transpose()*(Vxx*_fxs[i]);
      //safe inversion
      ASSERT_MSG(isFinite(Quu),"Quu infinite!")
      while(true) {
        Eigen::SelfAdjointEigenSolver<Matd> eig(Quu);
        scalarD reg=max<scalarD>(0,mu-eig.eigenvalues().minCoeff());
        Eigen::DiagonalMatrix<scalarD,-1,-1> diag;
        diag.resize(Quu.rows());
        diag.diagonal().array()=1/(eig.eigenvalues().array()+reg);
        invQuu=eig.eigenvectors()*(diag*eig.eigenvectors().transpose());
        if(isFinite(invQuu))
          break;
        else increaseMu(mu,delta);
      }
      //compute K
      Ks[i]=-invQuu*Qux;
      ks[i]=-invQuu*Qu;
      if(tooLarge(Ks[i],1E8f) || tooLarge(Ks[i],1E8f)) {
        increaseMu(mu,delta);
        valid=false;
        break;
      }
      ASSERT_MSG(isFinite(Ks[i]) && isFinite(ks[i]),"K/k infinite")
      //compute V
      Vx=Qx+Ks[i].transpose()*(Quu*ks[i]+Qu)+Qux.transpose()*ks[i];
      Vxx=Qxx+Ks[i].transpose()*(Quu*Ks[i]+Qux)+Qux.transpose()*Ks[i];
    }
  }
}
DDPSolver::Vec DDPSolver::solveILQG()
{
  scalarD mu=0,delta=1.0f,muMax=_tree.get<scalarD>("maxMu",1E2f);
  scalarD relC=1,absC=1,tolRelC=_tree.get<scalarD>("tolRelC",1E-4f);
  scalarD FXDec=0,tolFXDec=_tree.get<scalarD>("tolFXDec",1E-4f);
  scalarD alpha=_tree.get<scalarD>("initAlpha",1),alphaInc=2.1f,alphaDec=0.5f;
  scalarD minAlpha=_tree.get<scalarD>("minAlpha",1E-3f);
  sizeType maxIterILQG=_tree.get<sizeType>("maxIterILQG");
  bool ILQGOpt=_tree.get<bool>("optBasedILQG",false);
  bool ILQGQP=_tree.get<bool>("qpBasedILQG",false);

  Mss Ks;
  Vss ks,xsP;
  Vec uOpt,uNew;
  scalarD FX,newFX;
  sizeType horizon=_tree.get<sizeType>("horizon");
  _us0=initialize(horizon);
  evaluate(_us0,FX,false,xsP);
  if(_tree.get<bool>("debug",true)) {
    INFOV("FX=%f!",FX)
  }
  for(sizeType it=0; it<maxIterILQG;) {
    //ILQG optimization
    if(ILQGOpt)
      ILQGIter(uOpt);
    else if(ILQGQP)
      ILQGIterQP(uOpt);
    else ILQGIter(ks,Ks,mu,delta);
    //line search
    while(true) {
      if(ILQGOpt || ILQGQP)
        evaluate(uNew=_us0*(1-alpha)+uOpt*alpha,newFX,false,xsP);
      else evaluate(uNew,newFX,false,xsP,&ks,&Ks,&alpha);
      if(newFX < FX) {
        //check relative control
        relC=(uNew-_us0).cwiseAbs().maxCoeff();
        absC=max(uNew.cwiseAbs().maxCoeff(),tolRelC);
        relC/=absC;
        //debug output
        debugIter(Ks,ks,mu,alpha,delta,newFX,relC);
        //increase alpha
        alpha=min<scalarD>(1.0f,alpha*alphaInc);
        //update solution
        FXDec=FX-newFX;
        FX=newFX;
        _us0=uNew;
        it++;
        break;
      } else {
        alpha*=alphaDec;
        if(alpha < minAlpha)
          break;
      }
    }
    //adjust mu
    if(alpha < minAlpha) {
      alpha=minAlpha;
      increaseMu(mu,delta);
      if(mu > muMax || ILQGOpt)
        break;
    } else {
      decreaseMu(mu,delta);
      if(relC < tolRelC && FXDec < tolFXDec)
        break;
    }
  }
  adopt(_us0,horizon);
  return _us0;
}
//debug
void DDPSolver::debugObj(const Vec& x,const Vec& u,bool addObjUX,sizeType frm)
{
#define NR_TEST 10
#define DELTA 1E-8f
  Matd fxx,fuu;
  Vec fx,fu,fx2,fu2;
  scalarD df,dfN,ddf,ddfN;
  scalarD f=objX(frm,x,fx,fxx)+objU(frm,u,fu,fuu);
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec deltaX=Vec::Random(x.size());
    Vec deltaU=Vec::Random(u.size());
    df=fx.dot(deltaX)+fu.dot(deltaU);
    dfN=objX(frm,x+deltaX*DELTA,fx2)+objU(frm,u+deltaU*DELTA,fu2);
    dfN=(dfN-f)/DELTA;
    INFOV("ObjGradient: %f, Err: %f!",df,df-dfN)
    ddf=sqrt((fxx*deltaX).squaredNorm()+(fuu*deltaU).squaredNorm());
    ddfN=sqrt((fx2-fx).squaredNorm()+(fu2-fu).squaredNorm())/DELTA;
    INFOV("ObjHessian: %f, Err: %f!",ddf,ddf-ddfN)
  }
#undef DELTA
#undef NR_TEST
  exit(-1);
}
void DDPSolver::debugObj(bool addObjUX,sizeType frm)
{
  Vec x=parseVec<Vec>("x",_tree.get<sizeType>("frm"),_tree);
  Vec u=parseVec<Vec>("u",_tree.get<sizeType>("frm"),_tree);
  x.setRandom();
  u.setRandom();
  debugObj(x,u,addObjUX,frm);
}
void DDPSolver::debugFxCoord(const Vec& x,const Vec& u)
{
#define DELTA 1E-9
  Matd fx,fu;
  Vec f,df,f2;
  _tree.put<bool>("updateColl",true);
  dfdx(0,x,u,f,fx,fu);
  _tree.put<bool>("updateColl",false);
  for(sizeType i=0; i<x.size(); i++) {
    Vec deltaX=Vec::Unit(x.size(),i);
    df=fx*deltaX;
    dfdx(0,x+deltaX*DELTA,u,f2);
    INFOV("FxGradientCoord: %lf, Err: %lf!",df.norm(),(df-(f2-f)/DELTA).norm())
  }
  for(sizeType i=0; i<u.size(); i++) {
    Vec deltaU=Vec::Unit(u.size(),i);
    df=fu*deltaU;
    dfdx(0,x,u+deltaU*DELTA,f2);
    INFOV("FuGradientCoord: %lf, Err: %lf!",df.norm(),(df-(f2-f)/DELTA).norm())
  }
  _tree.put<bool>("updateColl",true);
#undef DELTA
}
void DDPSolver::debugFx(const Vec& x,const Vec& u)
{
#define NR_TEST 10
#define DELTA 1E-7f
  Matd fx,fu;
  Vec f,df,f2;
  _tree.put<bool>("updateColl",true);
  dfdx(0,x,u,f,fx,fu);
  _tree.put<bool>("updateColl",false);
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec deltaX=Vec::Random(x.size());
    df=fx*deltaX;
    dfdx(0,x+deltaX*DELTA,u,f2);
    INFOV("FxGradient: %lf, Err: %lf!",df.norm(),(df-(f2-f)/DELTA).norm())
    Vec deltaU=Vec::Random(u.size());
    df=fu*deltaU;
    dfdx(0,x,u+deltaU*DELTA,f2);
    INFOV("FuGradient: %lf, Err: %lf!",df.norm(),(df-(f2-f)/DELTA).norm())
  }
  _tree.put<bool>("updateColl",true);
#undef DELTA
#undef NR_TEST
}
void DDPSolver::debugFx(bool coord)
{
  Vec x=parseVec<Vec>("x",_tree.get<sizeType>("frm"),_tree);
  Vec u=parseVec<Vec>("u",_tree.get<sizeType>("frm"),_tree);
  if(coord)
    debugFxCoord(x,u);
  else debugFx(x,u);
}
void DDPSolver::debugGradient()
{
  sizeType horizon=_tree.get<sizeType>("horizon");
  Vec u=parseVec<Vec>("u",_tree.get<sizeType>("frm"),_tree);

  TrajOptimizer opt(*this,horizon);
  _us0.setZero(u.size()*horizon);
  for(sizeType i=0; i<horizon; i++)
    _us0.segment(i*u.size(),u.size())=u;

  //non opt version
  _tree.put<bool>("recomputeDeriv",true);
  opt.debugGradient(_us0,Vec::Ones(_us0.size()));

  //opt version
  _tree.put<bool>("recomputeDeriv",false);
  collectDerivatives();
  opt.debugGradient(_us0,Vec::Ones(_us0.size()));
}
void DDPSolver::debugILQGOptQP()
{
  Vec usOpt,delta;
  sizeType horizon=_tree.get<sizeType>("horizon");
  _us0=initialize(horizon);
  _us0.setRandom();
  ILQGIterQP(usOpt);

  Vec G=_G;
  scalarD FX;
  TrajOptimizer opt(*this,horizon);
  _tree.put<bool>("recomputeDeriv",false);
  delta=Vec::Random(_us0.size());
  opt.gradient(_us0+delta,FX,G);
  INFOV("G Norm: %f Err: %f",G.norm(),(G-(_G+_H*delta)).norm())
}
void DDPSolver::debugILQGOpt()
{
#define NR_TEST 10
#define DELTA 1E-3f
  sizeType horizon=_tree.get<sizeType>("horizon");
  scalarD mu=0,delta=1.0f;
  scalarD alpha=1,FX,FX2;
  Vec usNew,usNew2;
  Mss Ks;
  Vss ks,xsP;
  _us0=initialize(horizon);
  //_us0.setRandom();
  //method A
  ILQGIter(ks,Ks,mu,delta);
  evaluate(usNew,FX,false,xsP,&ks,&Ks,&alpha);
  //_tree.put<bool>("recomputeDeriv",false);
  //gradient(usNew,FX2,usNew2=usNew);
  //method B
  ILQGIter(usNew2);
  evaluate(usNew2,FX,false,xsP);
  //test different
  INFOV("ControlForce: %f, Err: %f",usNew.norm(),(usNew-usNew2).norm())
  //test function value decrease
  usNew=usNew2;
  evaluate(usNew,FX,true,xsP);
  for(sizeType i=0; i<NR_TEST; i++) {
    usNew2=usNew+Vec::Random(usNew.size())*DELTA;
    evaluate(usNew2,FX2,true,xsP);
    INFOV("Delta FX: %f",(FX2-FX)/DELTA)
  }
#undef DELTA
#undef NR_TEST
}
void DDPSolver::debugILQG()
{
  sizeType adopt=_tree.get<sizeType>("adopt");
  _tree.put<sizeType>("maxIterILQG",1000);
  _tree.put<scalarD>("tolFXDec",1E-3f);
  _tree.put<sizeType>("adopt",0);
  Vss xsP;

  scalarD minVal;
  Vec us=solveILQG();
  evaluate(us,minVal,false,xsP);

  scalarD minValRef;
  Vec usRef=solveTrajOpt();
  evaluate(usRef,minValRef,false,xsP);

  INFOV("MinVal: %f Err: %f",minValRef,minValRef-minVal)
  INFOV("Solution: %f Err: %f",usRef.norm(),(usRef-us).norm())

  _tree.put<sizeType>("adopt",adopt);
}
void DDPSolver::debugIter(Mss& Ks,Vss& ks,
                          scalarD mu,scalarD alpha,scalarD delta,
                          scalarD newFX,scalarD relC)
{
  if(!_tree.get<bool>("debug",true))
    return;
  scalarD ksNorm=0;
  ILQGIter(ks,Ks,mu,delta);
  for(sizeType i=0; i<(sizeType)ks.size(); i++)
    ksNorm+=ks[i].norm();
  INFOV("ILQGGradient: %f!",ksNorm/(scalarD)ks.size())
  INFOV("Decreased to f=%f where alpha=%f, mu=%f, delta=%f, relControl=%f",newFX,alpha,mu,delta,relC)
}
//objective function: X
scalarD DDPSolver::objX(sizeType i,const Vec& x) const
{
  return objX(i,x,NULL,NULL);
}
scalarD DDPSolver::objX(sizeType i,const Vec& x,Vec& fx) const
{
  return objX(i,x,&fx,NULL);
}
scalarD DDPSolver::objX(sizeType i,const Vec& x,Vec& fx,Matd& fxx) const
{
  return objX(i,x,&fx,&fxx);
}
//objective function: U
scalarD DDPSolver::objU(sizeType i,const Vec& u) const
{
  return objU(i,u,NULL,NULL);
}
scalarD DDPSolver::objU(sizeType i,const Vec& u,Vec& fu) const
{
  return objU(i,u,&fu,NULL);
}
scalarD DDPSolver::objU(sizeType i,const Vec& u,Vec& fu,Matd& fuu) const
{
  return objU(i,u,&fu,&fuu);
}
scalarD DDPSolver::objU(sizeType i,const Vec& u,Vec* fu,Matd* fuu) const
{
  //fx,fu,fxx,fux,fuu
  if(fu)fu->setZero(u.size());
  if(fuu)fuu->setZero(u.size(),u.size());
  scalarD regCoef=_tree.get<scalar>("regCoef",0.01f);
  //regularization
  if(fu)*fu+=u*regCoef;
  if(fuu)*fuu+=Matd::Identity(u.size(),u.size())*regCoef;
  return u.squaredNorm()*regCoef/2;
}
//dynamic system
void DDPSolver::dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f) const
{
  dfdx(i,x,u,f,NULL,NULL);
}
void DDPSolver::dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd& fx,Matd& fu) const
{
  dfdx(i,x,u,f,&fx,&fu);
}
//TrajOptimizer
TrajOptimizer::TrajOptimizer(DDPSolver& sol,sizeType horizon)
  :_sol(sol),_xs(sol._xs),_fxs(sol._fxs),_fus(sol._fus),_us0(sol._us0)
{
  _horizon=horizon;
  _xsP.resize(horizon+1);
}
void TrajOptimizer::operator()(Eigen::Map<const Vec>& us,scalarD& FX,Eigen::Map<Vec>& DFDX)
{
  sizeType nrU=us.size()/_horizon;
  Vec fx,fu,dObjdx;
  //forward pass: collect states
  if(_sol._tree.get<bool>("recomputeDeriv")) {
    _fxs.resize(_horizon);
    _fus.resize(_horizon);
    _xsP[0]=parseVec<Vec>("x",_sol._tree.get<sizeType>("frm"),_sol._tree);
    for(sizeType i=0; i<_horizon; i++)
      _sol.dfdx(i,_xsP[i],us.segment(i*nrU,nrU),_xsP[i+1],_fxs[i],_fus[i]);
  } else {
    ASSERT(_xsP.size() == _xs.size())
    _xsP[0]=_xs[0];
    for(sizeType i=0; i<_horizon; i++)
      _xsP[i+1]=_xs[i+1]+_fxs[i]*(_xsP[i]-_xs[i])+_fus[i]*(us-_us0).segment(i*nrU,nrU);
  }
  //backward pass: collect derivatives
  FX=_sol.objX(_horizon,_xsP[_horizon],dObjdx);
  for(sizeType i=_horizon-1; i>=0; i--) {
    FX+=_sol.objX(i,_xsP[i],fx);
    FX+=_sol.objU(i,us.segment(i*nrU,nrU),fu);
    DFDX.segment(i*nrU,nrU)=fu+_fus[i].transpose()*dObjdx;
    dObjdx=fx+_fxs[i].transpose()*dObjdx;
  }
  //INFOV("Function Value: %f Gradient: %f",FX,DFDX.cwiseAbs().maxCoeff())
}
void TrajOptimizer::setBound(const Vec& bound)
{
  sizeType nrU=_sol.nrU();
  Vec CB=Vec::Zero(nrU*_horizon);
  for(sizeType i=0; i<_horizon; i++)
    CB.segment(i*nrU,nrU)=bound;
  setCB(-CB,CB);
}
void TrajOptimizer::reset()
{
  sizeType nrU=_sol.nrU();
  BLEICInterface::reset(_horizon*nrU,false);
}
//Linear example
void DDPSolverLinear::createExample()
{
  //current state
  _tree.put<sizeType>("frm",0);
  assignVec(Vec4d(0,0,100,0),"x",0,_tree);
  assignVec(Vec2d(0,0),"u",0,_tree);
  //target
  scalar Y=20.0f;
  _tree.put<scalar>("targetX",0);
  _tree.put<scalar>("targetY",Y);
  //we use exact solver for debug
  _tree.put<scalar>("dt",0.01f);
  resetParam(1E10,1000,50);
  resetEps(1E-6f,1E-6f,1E-6f);
}
void DDPSolverLinear::writeEnvVTK(const string& path) const
{
  Vec2d target;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > balls;
  parseObjective(target,balls,_tree);

  Vec4 c;
  ObjMesh mesh;
  VTKWriter<scalar> os("environment",path,true);
  MakeMesh::makeSphere2D(mesh,0.05f,32);
  mesh.getPos().segment(0,2)=target.cast<scalar>();
  mesh.applyTrans();
  c=Vec4(1,0,0,1);
  mesh.writeVTK(os,false,false,&c);
}
void DDPSolverLinear::writeStateVTK(const string& path,const Vec& x) const
{
  ObjMesh mesh;
  VTKWriter<scalar> os("environment",path,true);
  MakeMesh::makeSphere2D(mesh,0.1f,32);
  mesh.getPos().segment(0,2)=x.segment(0,2).cast<scalar>();
  mesh.applyTrans();
  mesh.writeVTK(os,false,false);
}
scalarD DDPSolverLinear::objX(sizeType i,const Vec& x,Vec* fx,Matd* fxx) const
{
  Vec2d target;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > balls;
  parseObjective(target,balls,_tree);
  //fx,fu,fxx,fux,fuu
  if(fx)fx->setZero(4);
  if(fxx)fxx->setZero(4,4);
  scalarD ret=0;
  scalarD velCoef=_tree.get<scalar>("velCoef",0.01f);
  //target
  ret+=(x.segment(0,2)-target).squaredNorm()/2;
  if(fx)fx->segment(0,2)+=x.segment(0,2)-target;
  if(fxx)fxx->block(0,0,2,2)+=Mat2d::Identity();
  //velocity
  ret+=x.segment(2,2).squaredNorm()*velCoef/2;
  if(fx)fx->segment(2,2)+=x.segment(2,2)*velCoef;
  if(fxx)fxx->block(2,2,2,2)+=Mat2d::Identity()*velCoef;
  return ret;
}
void DDPSolverLinear::dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu) const
{
  scalar dt=_tree.get<scalar>("dt");
  f.resize(4);
  //(x,y,vx,vy)
  f[0]=x[0]+(x[2]+u[0]*dt)*dt;
  f[1]=x[1]+(x[3]+u[1]*dt)*dt;
  f[2]=x[2]+u[0]*dt;
  f[3]=x[3]+u[1]*dt;
  //gradient
  if(fx) {
    fx->setZero(4,4);
    (*fx)(0,0)=1;
    (*fx)(0,2)=dt;
    (*fx)(1,1)=1;
    (*fx)(1,3)=dt;
    (*fx)(2,2)=1;
    (*fx)(3,3)=1;
  }
  if(fu) {
    fu->setZero(4,2);
    (*fu)(0,0)=dt*dt;
    (*fu)(1,1)=dt*dt;
    (*fu)(2,0)=dt;
    (*fu)(3,1)=dt;
  }
}
//Dubin's car example
void DDPSolverDubin::createExample(bool bound)
{
  //car property
  _tree.put<scalar>("carL",0.4f);
  _tree.put<scalar>("maxDelta",0.2f);
  //current state
  _tree.put<sizeType>("frm",0);
  assignVec(Vec4d(0,0,M_PI/2,1),"x",0,_tree);
  assignVec(Vec2d(0,0),"u",0,_tree);
  //target
  scalar Y=20.0f;
  _tree.put<scalar>("targetX",0);
  _tree.put<scalar>("targetY",Y);
  //obstacle 0
  _tree.put<scalar>("ball0.ctrX",2.5f);
  _tree.put<scalar>("ball0.ctrY",Y*1/3);
  _tree.put<scalar>("ball0.rad",3.5f);
  //obstacle 1
  _tree.put<scalar>("ball1.ctrX",-2.5f);
  _tree.put<scalar>("ball1.ctrY",Y*2/3);
  _tree.put<scalar>("ball1.rad",3.5f);
  //obstacle 2
  _tree.put<scalar>("ball2.ctrX",-3.5f);
  _tree.put<scalar>("ball2.ctrY",Y/2);
  _tree.put<scalar>("ball2.rad",3.0f);
  //for this use inexact solver for more general debug
  _tree.put<scalar>("dt",0.01f);
  resetParam(1E10,1,50);
  resetEps(1E-6f,1E-6f,1E-6f);
  if(bound)
    _bound.reset(new Vec(Vec2d(100,1)));
}
void DDPSolverDubin::writeEnvVTK(const string& path) const
{
  Vec2d target;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > balls;
  parseObjective(target,balls,_tree);

  Vec4 c;
  ObjMesh mesh;
  VTKWriter<scalar> os("environment",path,true);
  for(sizeType i=0; i<(sizeType)balls.size(); i++) {
    os.setRelativeIndex();
    MakeMesh::makeSphere2D(mesh,balls[i][2],32);
    mesh.getPos().segment(0,2)=balls[i].segment(0,2).cast<scalar>();
    mesh.applyTrans();
    c=Vec4(1,1,1,1);
    mesh.writeVTK(os,false,false,&c);
  }
  MakeMesh::makeSphere2D(mesh,0.05f,32);
  mesh.getPos().segment(0,2)=target.cast<scalar>();
  mesh.applyTrans();
  c=Vec4(1,0,0,1);
  mesh.writeVTK(os,false,false,&c);
}
void DDPSolverDubin::writeStateVTK(const string& path,const Vec& x) const
{
  scalar carL=_tree.get<scalar>("carL");
  Vec3 x0((scalar)x[0],(scalar)x[1],0);
  Vec3 dir0=Vec3((scalar)cos(x[2]),(scalar)sin(x[2]),0)*carL;
  Vec3 dir1=Vec3((scalar)sin(x[2]),-(scalar)cos(x[2]),0)*carL/2;
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  vss.push_back(x0+dir1/2);
  vss.push_back(x0+dir1/2+dir0);
  vss.push_back(x0-dir1/2+dir0);
  vss.push_back(x0-dir1/2);

  VTKWriter<scalar> os("state",path,true);
  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
  iss.push_back(Vec3i(0,1,-1));
  iss.push_back(Vec3i(1,2,-1));
  iss.push_back(Vec3i(2,3,-1));
  iss.push_back(Vec3i(3,0,-1));
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(iss.begin(),iss.end(),VTKWriter<scalar>::LINE);
}
scalarD DDPSolverDubin::objX(sizeType i,const Vec& x,Vec* fx,Matd* fxx) const
{
  Vec2d target;
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > balls;
  parseObjective(target,balls,_tree);
  //fx,fu,fxx,fux,fuu
  if(fx)fx->setZero(4);
  if(fxx)fxx->setZero(4,4);
  scalarD ret=0;
  scalarD velCoef=_tree.get<scalar>("velCoef",0.01f);
  //target
  ret+=(x.segment(0,2)-target).squaredNorm()/2;
  if(fx)fx->segment(0,2)+=x.segment(0,2)-target;
  if(fxx)fxx->block(0,0,2,2)+=Mat2d::Identity();
  //velocity
  ret+=x[3]*x[3]*velCoef/2;
  if(fx)(*fx)[3]+=x[3]*velCoef;
  if(fxx)(*fxx)(3,3)+=velCoef;
  //obstacle avoidance
  scalarD alpha=_tree.get<scalar>("alpha",25.0f);
  scalarD beta=_tree.get<scalar>("beta",100.0f);
  for(sizeType i=0; i<(sizeType)balls.size(); i++) {
    Vec2d delta=x.segment(0,2)-balls[i].segment(0,2);
    scalarD dist=delta.squaredNorm();
    scalarD expVal=exp(alpha*(balls[i][2]*balls[i][2]-dist))*beta;
    ret+=expVal;
    if(fx)fx->segment(0,2)+=delta*(-alpha*2*expVal);
    if(fxx)fxx->block(0,0,2,2)+=delta*delta.transpose()*(alpha*alpha*4*expVal);//+Mat2d::Identity()*(-alpha*2*expVal);
  }
  return ret;
}
void DDPSolverDubin::dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu) const
{
  ASSERT(x.size() == 4)
  scalar dt=_tree.get<scalar>("dt");
  scalar carL=_tree.get<scalar>("carL");
  scalar maxDelta=_tree.get<scalar>("maxDelta");
  //(x,y,theta,v)
  f.resize(4);
  f[0]=x[0]+x[3]*cos(x[2])*dt;
  f[1]=x[1]+x[3]*sin(x[2])*dt;
  f[2]=x[2]+x[3]*u[1]*(maxDelta/carL)*dt;
  f[3]=x[3]+u[0]*dt;
  //gradient
  if(fx) {
    fx->setZero(4,4);
    (*fx)(0,0)=1;
    (*fx)(0,2)=-x[3]*sin(x[2])*dt;
    (*fx)(0,3)=cos(x[2])*dt;
    (*fx)(1,1)=1;
    (*fx)(1,2)= x[3]*cos(x[2])*dt;
    (*fx)(1,3)=sin(x[2])*dt;
    (*fx)(2,2)=1;
    (*fx)(2,3)=u[1]*(maxDelta/carL)*dt;
    (*fx)(3,3)=1;
  }
  if(fu) {
    fu->setZero(4,2);
    (*fu)(2,1)=x[3]*(maxDelta/carL)*dt;
    (*fu)(3,0)=dt;
  }
}

PRJ_END
