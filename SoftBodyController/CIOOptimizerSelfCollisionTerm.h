#ifndef CIO_OPTIMIZER_SELF_COLLISION_TERM_H
#define CIO_OPTIMIZER_SELF_COLLISION_TERM_H

#include "CIOOptimizer.h"

PRJ_BEGIN

class CIOOptimizerSelfCollisionTerm : public CIOOptimizerTerm
{
public:
  CIOOptimizerSelfCollisionTerm(CIOOptimizer& opt);
  virtual void writeSelfCollVTK(const std::string& path);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual void updateSelfColl(vector<boost::shared_ptr<FEMCIOEngine> >* mp);
  virtual void precompute(sizeType i,const FEMCIOEngine& eng,const GradientInfo& info,vector<CIOSelfColl>& selfColl);
  virtual void debugConsistency(sizeType i,const FEMCIOEngine& eng,const vector<CIOSelfColl>& selfColl);
  virtual void addRandomSelfCollision();
  virtual const Coli& getCollisionOff() const;
  virtual int values() const;
  virtual bool valid() const;
protected:
  virtual void buildCollisionOffset();
  virtual vector<CIOSelfColl>& getSelfColl(sizeType i);
  virtual GradientInfo& getCollConfig(sizeType i);
  Coli _cOff;
};
class CIOOptimizerSelfCollisionTermPrecomputed : public CIOOptimizerSelfCollisionTerm
{
public:
  CIOOptimizerSelfCollisionTermPrecomputed(CIOOptimizer& opt);
  virtual void runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const;
  virtual void runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const;
  virtual void precompute(sizeType i,const FEMCIOEngine& eng,const GradientInfo& info,vector<CIOSelfColl>& selfColl) override;
  virtual void debugConsistency(sizeType i,const FEMCIOEngine& eng,const vector<CIOSelfColl>& selfColl) override;
private:
  Mss _Hss;
  Vss _Gss;
};

PRJ_END

#endif
