#include "CIOOptimizerContactTerm.h"
#include <Dynamics/FEMCIOEngine.h>

USE_PRJ_NAMESPACE

//Contact Term
CIOOptimizerContactTerm::CIOOptimizerContactTerm(CIOOptimizer& opt):CIOOptimizerTerm(opt)
{
  //build collisions
  if(_opt._sol._tree.get<bool>("initContactUpdate",true))
    updateContacts(NULL);
  buildCollisionOffset();
}
void CIOOptimizerContactTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wCont=_opt._sol._tree.get<scalar>("regContactActivationCoef",1000);
  scalar wContV=_opt._sol._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_opt._sol._tree.get<scalar>("k1",10);
  scalar k2=_opt._sol._tree.get<scalar>("k2",k1);
  scalar wContSqrt=sqrt(wCont);

  Vec C;
  Matd DCDXP,DCDX;

  //compute contact violation
  if(fjac)
    eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,NULL,&C,&DCDXP,&DCDX);
  else eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,NULL,&C,NULL,NULL);
  fvec.segment(off+_cOff[i]*FEMCIOEngine::NRC,(_cOff[i+1]-_cOff[i])*FEMCIOEngine::NRC)=C*wContSqrt;
  //assign gradient
  if(fjac) {
    //X
    _opt.addBlockX (*fjac,off+_cOff[i]*FEMCIOEngine::NRC,i,DCDX*wContSqrt);
    //XP
    _opt.addBlockXP(*fjac,off+_cOff[i]*FEMCIOEngine::NRC,i,DCDXP*wContSqrt);
  }
}
void CIOOptimizerContactTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wCont=_opt._sol._tree.get<scalar>("regContactActivationCoef",1000);
  scalar wContV=_opt._sol._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_opt._sol._tree.get<scalar>("k1",10);
  scalar k2=_opt._sol._tree.get<scalar>("k2",k1);

  Vec DCDX,DCDXP,DCDC;
  sizeType dim=_opt._sol.getEngine().getBody().dim();

  //contact activation
  FX+=wCont*eng.computeContactViolationCIO(wContV,k1,k2,&DCDX,&DCDXP,&DCDC,NULL,NULL,NULL,NULL);
  //assign gradient
  //X
  _opt.addBlockX(DFDX,i,wCont*DCDX);
  //XP
  _opt.addBlockXP(DFDX,i,wCont*DCDXP);
  DFDX.segment(_opt.inputs()+_cOff[i]*dim,(_cOff[i+1]-_cOff[i])*dim)+=wCont*DCDC;
}
void CIOOptimizerContactTerm::updateContactVelReg(CIOContactInfo& info) const
{
  bool VNReg=_opt._sol._tree.get<scalar>("VNReg",true);
  bool VTReg=_opt._sol._tree.get<scalar>("VTReg",true);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<info.nrC(); i++) {
    LCPConstraint& c=info.getCons(i);
    if(VNReg && VTReg)
      c._collisionType=LCPConstraint::STATIC;
    else if(VNReg)
      c._collisionType=LCPConstraint::NORMAL;
    else if(VTReg)
      c._collisionType=LCPConstraint::SLIDING;
    else c._collisionType=LCPConstraint::INACTIVE;
  }
}
void CIOOptimizerContactTerm::updateContacts(vector<boost::shared_ptr<FEMCIOEngine> >* mp)
{
  //update state
  //INFO("Updating contacts!")
  if((sizeType)_cOff.size() != _opt._horizon+1)
    _opt.updateConfig(_opt.collectSolution(false),false,false,NULL,mp);
  else _opt.updateConfig(_opt.collectSolution(true),true,false,NULL,mp);
  //update collision
  scalar dt=_opt._sol.getEngine()._tree.get<scalar>("dt");
  bool randomScale=_opt._sol._tree.get<scalar>("randomizedScale",0);
  OMP_PARALLEL_FOR_X(mp ? OmpSettings::getOmpSettings().nrThreads() : 1)
  for(sizeType i=0; i<_opt._horizon; i++) {
    FEMCIOEngine& eng=mp ? *(*mp)[omp_get_thread_num()] : _opt._sol.getEngine();
    const RigidReducedMCalculator& MCalc=eng.getMCalc();
    const FEMLCPSolver::Contacts& cs=eng.getCons();
    //state
    eng._xN=_opt._info._xss[i+0];
    eng._x =_opt._info._xss[i+1];
    eng._xP=_opt._info._xss[i+2];
    eng._c =_opt._info._infos[i];
    if(randomScale > 0) {
      eng._c.setXVGRandom(MCalc,cs,&(eng._xP),&(eng._x));
      Vec cf=Vec::Random(eng._c.cf().size())*randomScale;
      eng._c.setCF(dt,MCalc,&cf);
      eng.clusterPatch();
    } else eng.updateActiveContact(dt);
    updateContactVelReg(eng._c);
    //update force
    _opt._info._infos[i]=eng._c;
  }
  //add collision offset
  buildCollisionOffset();
}
const Coli& CIOOptimizerContactTerm::getCollisionOff() const
{
  return _cOff;
}
int CIOOptimizerContactTerm::values() const
{
  return _cOff[_cOff.size()-1]*FEMCIOEngine::NRC;
}
bool CIOOptimizerContactTerm::valid() const
{
  scalar wCont=_opt._sol._tree.get<scalar>("regContactActivationCoef",1000);
  return wCont > 0;
}
void CIOOptimizerContactTerm::buildCollisionOffset()
{
  sizeType dim=_opt._sol.getEngine().getBody().dim();
  _cOff.setZero(_opt._horizon+1);
  for(sizeType i=0; i<_opt._horizon; i++) {
    const CIOContactInfo& info=_opt._info._infos[i];
    _cOff[i+1]=_cOff[i]+info.nrC();
    ASSERT(info.cf().size() == (_cOff[i+1]-_cOff[i])*dim)
  }
}
