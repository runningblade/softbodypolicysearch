#include "FEMTracking.h"
#include "MLUtil.h"
#include <CommonFile/geom/StaticGeom.h>

USE_PRJ_NAMESPACE

EnergyTrack::EnergyTrack(FEMCIOEngine& eng):_eng(eng)
{
  boost::property_tree::ptree& pt=_eng._tree;
  pt.put<scalarD>("collisionExpand",_eng.avgElementLength());
  _tracking=true;
}
void EnergyTrack::setXCurr(const Vec& x,scalar regPhys,scalar regU)
{
  sizeType nrS=x.size()/2,nrU=nrS-6;
  _eng._xN.reset(_eng.getMCalc(),x.segment(0,nrS));
  _eng._x.reset(_eng.getMCalc(),x.segment(nrS,nrS));
  //metric
  _metricSqr.setIdentity(nrS,nrS);
  if(regU > 0) {
    _metricSqr.block<6,6>(nrU,nrU)*=regPhys;
    _metricSqr.block(0,0,nrU,nrU)*=regU*regPhys/(regU+regPhys);
  }
  _metricSqrt=matrixSqrt(_metricSqr);
}
void EnergyTrack::setXNext(const Vec& x)
{
  _eng._xP.reset(_eng.getMCalc(),x);
  _xTarget=_eng._xP;
  //contact update
  FEMLCPSolver::Contacts cons;
  _hasColl=_eng._geom && _eng._geom->nrG() > 0;
  if(_hasColl) {
    _eng._c.setXVG(_eng.getMCalc(),cons,&(_eng._xP),&(_eng._x));
    _eng.updateActiveContact(_eng._tree.get<scalar>("dt"));
    //INFOV("Number of contacts: %ld!",_eng._c.getCons().size())
  }
}
void EnergyTrack::clearContacts()
{
  scalar dt=_eng._tree.get<scalar>("dt");
  //_eng._c=CIOContactInfo();
  _eng._c.setZero(dt,_eng.getMCalc());
}
void EnergyTrack::randomContacts()
{
  scalar dt=_eng._tree.get<scalar>("dt");
  //_eng._c=CIOContactInfo();
  _eng._c.setRandom(dt,_eng.getMCalc());
}
EnergyTrack::Vec EnergyTrack::getU()
{
  scalar dt=_eng._tree.get<scalar>("dt");
  sizeType nrS=_xTarget.x().size(),nrU=nrS-6;
  //compute
  _eng.computePhysicsDerivativeCIO(dt,_E,NULL,NULL,NULL,false);
  Vec u=_eng.getControlMatrix().inverse()*_E.segment(0,nrU)/dt/dt;
  //check
  _eng.setControlInput(u);
  _eng.computePhysicsDerivativeCIO(dt,_E,NULL,NULL,NULL,true);
  return u;
}
int EnergyTrack::operator()(const Vec& x,scalarD& FX,Vec& dfdx,const scalarD& step,bool wantGradient)
{
  //initialize
  scalar dt=_eng._tree.get<scalar>("dt");
  scalar wCont=_eng._tree.get<scalar>("regContactActivationCoef",10)*_metricSqr(0,0);
  scalar wContV=_eng._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_eng._tree.get<scalar>("k1",10);
  scalar k2=_eng._tree.get<scalar>("k2",k1);
  dfdx.setZero(inputsLBFGS());
  Vec DFDXP,DFDC;
  //set data
  if(_eng._xP.x() != x.segment(0,inputs()))
    _eng._xP.reset(_eng.getMCalc(),x.segment(0,inputs()));
  if(_hasColl) {
    Vec cf=x.segment(inputs(),x.size()-inputs());
    _eng._c.setXVGKeepIndex(_eng.getMCalc(),_eng._c.getCons(),&(_eng._xP),&(_eng._x));
    _eng._c.setCF(dt,_eng.getMCalc(),&cf);
  }
  //physics
  FX=_eng.computePhysicsViolationCIO(dt,_metricSqr,NULL,NULL,&DFDXP,&DFDC);
  dfdx.segment(0,DFDXP.size())+=DFDXP;
  dfdx.segment(DFDXP.size(),DFDC.size())+=DFDC;
  //contact
  if(_hasColl) {
    FX+=wCont*_eng.computeContactViolationCIO(wContV,k1,k2,NULL,&DFDXP,&DFDC,NULL,NULL,NULL,NULL);
    dfdx.segment(0,DFDXP.size())+=DFDXP*wCont;
    dfdx.segment(DFDXP.size(),DFDC.size())+=DFDC*wCont;
  }
  return 0;
}
int EnergyTrack::operator()(const Vec& x,Vec& fvec,DMat* fjac,bool modifiable)
{
  //initialize
  scalar dt=_eng._tree.get<scalar>("dt");
  scalar wCont=_eng._tree.get<scalar>("regContactActivationCoef",10)*_metricSqr(0,0);
  scalar wContV=_eng._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_eng._tree.get<scalar>("k1",10);
  scalar k2=_eng._tree.get<scalar>("k2",k1);
  scalar wContSqrt=sqrt(wCont);
  Vec C,vio;
  Matd DCDXP;
  sizeType off=0;
  fvec.setZero(values());
  if(fjac)
    fjac->setZero(values(),inputs());
  //set data
  if(_eng._xP.x() != x)
    _eng._xP.reset(_eng.getMCalc(),x);
  if(_hasColl)
    _eng._c.setXVGKeepIndex(_eng.getMCalc(),_eng._c.getCons(),&(_eng._xP),&(_eng._x));
  //update force
  if(modifiable && wCont > 0 && _hasColl) {
    _eng.computePhysicsDerivativeCIO(dt,_E,NULL,NULL,NULL,false);
    _eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,&vio,NULL,NULL,NULL);
    updateF(dt,wCont,vio);
  }
  //physics
  _eng.computePhysicsDerivativeCIO(dt,_E,NULL,NULL,&_DEDXP,false);
  fvec.segment(0,_E.size())=_metricSqrt*_E;
  if(fjac)
    fjac->block(0,0,_E.size(),_DEDXP.cols())=_metricSqrt*_DEDXP;
  off+=_E.size();
  //contact
  if(_hasColl) {
    if(fjac)
      _eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,NULL,&C,&DCDXP,NULL);
    else _eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,NULL,&C,NULL,NULL);
    fvec.segment(off,_eng._c.getCons().size()*FEMCIOEngine::NRC)=C*wContSqrt;
    if(fjac)
      fjac->block(off,0,DCDXP.rows(),DCDXP.cols())=DCDXP*wContSqrt;
    off+=C.size();
  }
  return 0;
}
scalarD EnergyTrack::operator()(const Vec& x,Vec* fgrad,DMat* fhess)
{
  if(_eng._xP.x() != x)
    _eng._xP.reset(_eng.getMCalc(),x);
  if(fgrad)
    fgrad->setZero(inputs());
  if(fhess)
    fhess->setZero(inputs(),inputs());
  scalarD ret=0;
  //no tracking
  if(!_tracking)
    return ret;
  //tracking
  const RigidReducedMCalculator& MCalc=_eng.getMCalc();
  //target match
  ret+=MCalc.calcATHB(_eng._xP,_eng._xP,fgrad,fgrad,fhess,1.0f);
  ret+=MCalc.calcATHB(_eng._xP,_xTarget,fgrad,NULL,NULL,-2.0f);
  ret+=MCalc.calcATHB(_xTarget,_xTarget,NULL,NULL,NULL,1.0f);
  return ret;
}
void EnergyTrack::updateF()
{
  Vec vio;
  scalar dt=_eng._tree.get<scalar>("dt");
  scalar wCont=_eng._tree.get<scalar>("regContactActivationCoef",10)*_metricSqr(0,0);
  scalar wContV=_eng._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_eng._tree.get<scalar>("k1",10);
  scalar k2=_eng._tree.get<scalar>("k2",k1);
  //update force
  _eng.computePhysicsDerivativeCIO(dt,_E,NULL,NULL,NULL,false);
  _eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,&vio,NULL,NULL,NULL);
  updateF(dt,wCont,vio);
}
void EnergyTrack::updateF(scalar dt,scalar wCont,Vec& vios)
{
  if(vios.size() == 0)
    return;
  //param
  scalar mu=_eng._tree.get<scalar>("collMu");
  sizeType nrC=_eng._c.getCons().size();
  sizeType dim=_eng.getBody().dim();
  //initialize
  const RigidReducedMCalculator& MCalc=_eng.getMCalc();
  Matd DEDC=_eng.computePhysicsDerivativeEFCIO(),H;
  Vec G,F;
  //build problem
  vios=vios.array().max(1E-6f).matrix();
  H=(DEDC*_metricSqr)*DEDC.transpose();
  G=(DEDC*_metricSqr)*_E-H*_eng._c.cf();
  H.diagonal()+=vios*wCont*2;
  //dual solver
#define QP
#ifdef QP
  Matd CI;
  //Matd CI=Matd::Zero(0,nrC*dim);
  Matd D=concatCol(Vec::Constant(_eng.getD().cols(),mu),_eng.getD().transpose());
  CI.setZero(nrC*D.rows(),nrC*dim);
  for(sizeType i=0; i<nrC; i++)
    CI.block(i*D.rows(),i*dim,D.rows(),dim)=D;
  //qp.solveDual(H,G,CI,NULL,F=Vec::Zero(G.size()));
  _qp.solveAQP(H,G,CI,NULL,F=_eng._c.cf());
#else
  F=-H.ldlt().solve(G);
#endif
  //assign result
  _eng._c.setCF(dt,MCalc,&F);
}
EnergyTrack::Vec EnergyTrack::getXLBFGS() const
{
  return concat(_eng._xP.x(),_eng._c.cf());
}
void EnergyTrack::debugTrackingEnergy()
{
#define DELTA 1E-7f
#define NR_TEST 10
  sizeType nrS=_eng._x.x().size();
  INFO("Testing LBFGS")
  for(sizeType i=0; i<NR_TEST; i++) {
    setXCurr(Vec::Random(nrS*2),RandEngine::randR01(),RandEngine::randR01());
    setXNext(Vec::Random(nrS));
    randomContacts();

    scalarD FX,FX2;
    Vec DFDX,DFDX2;
    Vec x=getXLBFGS();
    operator()(x,FX,DFDX,1,true);
    Vec delta=Vec::Random(x.size());
    operator()(x+delta*DELTA,FX2,DFDX2,1,true);
    INFOV("GradientLBFGS: %f Err: %f",DFDX.dot(delta),(DFDX.dot(delta)-(FX2-FX)/DELTA))
  }
  INFO("Testing modifiable LM")
  for(sizeType i=0; i<NR_TEST; i++) {
    setXCurr(Vec::Random(nrS*2),RandEngine::randR01(),RandEngine::randR01());
    setXNext(Vec::Random(nrS));
    randomContacts();

    Matd fjac;
    Vec fvec,fvec2;
    Vec x=Vec::Random(nrS);
    operator()(x,fvec,&fjac,false);

    Vec delta=Vec::Random(nrS);
    operator()(x+delta*DELTA,fvec2,NULL,false);
    INFOV("GradientLM: %f, Err: %f",(fjac*delta).norm(),(fjac*delta-(fvec2-fvec)/DELTA).norm())

    Vec DFDX;
    scalarD FX;
    operator()(getXLBFGS(),FX,DFDX,1,true);
    INFOV("LBFGS Function: %f Err: %f",FX,(FX-fvec.squaredNorm()/2))
    INFOV("GradientF Before: %f",DFDX.segment(inputs(),DFDX.size()-inputs()).norm())
    updateF();
    operator()(getXLBFGS(),FX,DFDX,1,true);
    INFOV("GradientF After: %f",DFDX.segment(inputs(),DFDX.size()-inputs()).norm())
  }
  INFO("Testing SQP")
  for(sizeType i=0; i<NR_TEST; i++) {
    setXCurr(Vec::Random(nrS*2),RandEngine::randR01(),RandEngine::randR01());
    setXNext(Vec::Random(nrS));
    randomContacts();

    Matd fhess;
    Vec fgrad,fgrad2;
    Vec x=Vec::Random(nrS);
    scalarD E=operator()(x,&fgrad,&fhess);

    Vec delta=Vec::Random(nrS);
    scalarD E2=operator()(x+delta*DELTA,&fgrad2,NULL);
    INFOV("GradientSQP: %f, Err: %f",fgrad.dot(delta),(E2-E)/DELTA-fgrad.dot(delta))
    INFOV("HessianSQP: %f, Err: %f",((fgrad2-fgrad)/DELTA).norm(),((fgrad2-fgrad)/DELTA-fhess*delta).norm())
  }
  exit(-1);
#undef NR_TEST
#undef DELTA
}
//size of problem
int EnergyTrack::inputsLBFGS() const
{
  sizeType ret=_eng._xN.x().size();
  if(_hasColl)
    ret+=_eng._c.getCons().size()*_eng.getBody().dim();
  return (int)ret;
}
int EnergyTrack::inputs() const
{
  return _eng._xN.x().size();
}
int EnergyTrack::values() const
{
  sizeType ret=_eng._xN.x().size();
  if(_hasColl)
    ret+=_eng._c.getCons().size()*FEMCIOEngine::NRC;
  return (int)ret;
}
void EnergyTrack::setHasColl(bool hasColl)
{
  _hasColl=hasColl;
}
void EnergyTrack::setTracking(bool tracking)
{
  _tracking=tracking;
}
