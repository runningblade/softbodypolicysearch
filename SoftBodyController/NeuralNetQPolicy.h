#ifndef NEURAL_NET_Q_POLICY_H
#define NEURAL_NET_Q_POLICY_H

#include "NeuralNetPolicy.h"
#include <Eigen/Sparse>

PRJ_BEGIN

//Simple QPolicy Wrapper
struct Tensor : public Serializable {
  typedef Policy::Vec Vec;
  Tensor();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  sizeType depth() const;
  void randomize(Vec& state) const;
  void findQValue(const Vec& state,vector<pair<sizeType,scalarD> >& stencil,scalarD coef0) const;
  void createQTensor(sizeType i,const Vec& L,const Vec& U,sizeType res,sizeType nrA,sizeType& off);
  vector<boost::shared_ptr<Tensor> > _subIndex;
  Vec2d _scale;
  sizeType _i;
};
struct SimpleQPolicy : public Policy {
  SimpleQPolicy();
  SimpleQPolicy(const Vec& L,const Vec& U,const Vec& action,sizeType res);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual Matd getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples=NULL) override;
  virtual scalarD QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* hess) const;
  virtual void debugQLoss();
  Vec findQValue(const Vec& state) const;
  sizeType index(scalarD val) const;
  virtual void setEps(scalarD eps);
  sizeType nrParam() const;
  virtual Vec toVec() const;
  virtual void fromVec(const Vec& weights);
  virtual Vec toVec(const map<sizeType,sizeType>* MMap) const;
  virtual void fromVec(const Vec& weights,const map<sizeType,sizeType>* MMap);
protected:
  Vec eval(const vector<pair<sizeType,scalarD> >& stencil,const Vec& Q) const;
  void evalGrad(Vec& grad,const vector<pair<sizeType,scalarD> >& stencil,sizeType id,scalarD coef) const;
  void evalHess(TRIPS& hess,const vector<pair<sizeType,scalarD> >& stencil,sizeType row,sizeType id,scalarD coef) const;
  SimpleQPolicy(const string& name);
  Vec _Q,_action;
  Tensor _tensor;
  scalarD _eps;
};
//StablizedSimpleQPolicy
struct StablizedSimpleQPolicy : public SimpleQPolicy {
  StablizedSimpleQPolicy();
  StablizedSimpleQPolicy(const Vec& L,const Vec& U,const Vec& action,sizeType res);
  virtual scalarD QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* hess) const;
  void updateQTarget();
protected:
  Vec _QTarget;
};
//NeuralNet QPolicy Wrapper
struct NeuralNetQPolicy : public NeuralNetPolicy {
  NeuralNetQPolicy();
  NeuralNetQPolicy(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,const Vec& action,
                   boost::property_tree::ptree* pt=NULL,boost::shared_ptr<FeatureTransformLayer> feature=NULL);
  virtual Matd getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples=NULL) override;
  virtual scalarD QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* hess) const;
  virtual void optimizeQLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to) const;
  virtual void debugQLoss();
  virtual sizeType nrParam() const;
  virtual sizeType nrAction() const;
  virtual Vec toVec() const;
  virtual void fromVec(const Vec& weights);
  sizeType index(scalarD val) const;
  virtual void setEps(scalarD eps);
  const Vec& getAction() const;
  void updateQTarget();
protected:
  NeuralNetQPolicy(const string& name);
  void buildQLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to) const;
  Vec _targetNet,_action;
  scalarD _eps;
};
//NeuralNet ProbabilisticQPolicy
struct NeuralNetProbQPolicy : public NeuralNetQPolicy {
  NeuralNetProbQPolicy();
  NeuralNetProbQPolicy(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,const Vec& action,
                       boost::property_tree::ptree* pt=NULL,boost::shared_ptr<FeatureTransformLayer> feature=NULL);
  virtual Matd getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples=NULL) override;
  virtual boost::shared_ptr<KrylovMatrix<scalarD> > getHessKLDistance(const Matd& states) const override;
  virtual scalarD getDiffKLDistance(const Matd& states,const Matd& xsOld,Vec* grad) const;
  virtual scalarD getDiffKLDistance(const Matd& states,const Vec& paramOld,Vec* grad) override;
};

PRJ_END

#endif
