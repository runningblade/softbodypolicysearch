#ifndef FEM_DDP_SYNTHESIS_H
#define FEM_DDP_SYNTHESIS_H

#include "FEMDDPSolverMT.h"

PRJ_BEGIN

class FEMDDPSynthesis : public FEMDDPSolver
{
public:
  FEMDDPSynthesis(FEMEnvironment& env);
  void addTaskData(const boost::property_tree::ptree& pt);
  void addTaskDataInner(const boost::property_tree::ptree& pt);
  Vec applyTask(FEMDDPSolverInfo task,sizeType& frm,sizeType& currFrm,Vec x,const Vec3d* tar,scalarD* minCost,bool more,bool blend=false);
  Vec applyTask(const FEMDDPSolverInfo& task,sizeType& frm,sizeType& currFrm,bool more,bool blend=false);
  const FEMDDPSolverInfo& getTask(sizeType i) const;
  void calcAvgDist(FEMDDPSolverInfo& task);
  sizeType nrTask() const;
  void debugAToB();
  void solve();
  //helper
  void solveGeom();
  void solveInit(sizeType& frm,const boost::property_tree::ptree& pt);
  void solveRandomNavigation(sizeType& frm,const boost::property_tree::ptree& pt);
  void solveAStarNavigation(sizeType& frm,const boost::property_tree::ptree& pt);
  void solveTracking(sizeType& frm,const boost::property_tree::ptree& pt);
  scalarD defoMetric(const GradientInfo& x,const GradientInfo& y) const;
  scalarD distMetric(const FEMDDPSolverInfo& info,sizeType x,sizeType y) const;
  sizeType findFrm(const FEMDDPSolverInfo& info,const Vec& y) const;
  sizeType findTask(sizeType currTask,const vector<scalarD>& prob) const;
  FEMDDPSolverInfo blendFrame(const FEMDDPSolverInfo& task,sizeType currFrm,GradientInfo info);
  void debugTrackingEnergy();
protected:
  //transform
  Mat4d AToBR(Vec a,Vec b) const;
  Mat4d AToBR(const GradientInfo& a,const GradientInfo& b) const;
  Mat4d computeT(const GradientInfo& x) const;
  Mat3d computeS(const Vec& A,const Vec& B) const;
  scalarD dist(const Vec3d& A,const Vec3d& B) const;
  bool validFrm(const Vec& x) const;
  void applyT(const Mat4d& T,Vec& x) const;
  void applyInvT(const Mat4d& T,Vec& x) const;
  Vec applyTR(const Mat4d& T,const Vec& x) const;
  GradientInfo getInfo(const Vec& x) const;
  sizeType findTaskId(const FEMDDPSolverInfo& task) const;
  void addContactInfo(FEMDDPSolverInfo& info);
  bool hasColl() const;
  //data
  vector<FEMDDPSolverInfo> _tasks,_taskCyclics;
  Matd _defoMetricSqrt,_AToBRTpl[9];
  Mat3Xd _LCtr;
  //geometry/obstacle
  boost::shared_ptr<StaticGeom> _geom;
  scalar _radius;
  //tracking trajectory
  sizeType _multiple;
  Vss _trajectory;
};

PRJ_END

#endif
