#ifndef FEM_TRACKING_H
#define FEM_TRACKING_H

#include "QPInterface.h"
#include "FEMEnvironment.h"
#include <Dynamics/FEMCIOEngine.h>

PRJ_BEGIN

struct EnergyTrack : public Objective<scalarD> {
public:
  EnergyTrack(FEMCIOEngine& eng);
  virtual void setXCurr(const Vec& x,scalar regPhys,scalar regU);
  virtual void setXNext(const Vec& x);
  virtual void clearContacts();
  virtual void randomContacts();
  virtual Vec getU();
  virtual int operator()(const Vec& x,scalarD& FX,Vec& dfdx,const scalarD& step,bool wantGradient);
  virtual int operator()(const Vec& x,Vec& fvec,DMat* fjac,bool modifiable);
  virtual scalarD operator()(const Vec& x,Vec* fgrad,DMat* fhess);
  void updateF();
  void updateF(scalar dt,scalar wCont,Vec& vios);
  Vec getXLBFGS() const;
  void debugTrackingEnergy();
  //size of problem
  virtual int inputsLBFGS() const;
  virtual int inputs() const;
  virtual int values() const;
  void setHasColl(bool hasColl);
  void setTracking(bool tracking);
private:
  bool _hasColl,_tracking;
  FEMCIOEngine& _eng;
  Matd _DEDXP,_metricSqr,_metricSqrt;
  GradientInfo _xTarget;
  QPInterface _qp;
  Vec _E;
};

PRJ_END

#endif
