#include "FEMDDPSolver.h"
#include "CIOPolicyOptimizer.h"
#include "CIOOptimizerPhysicsTerm.h"
#include "CIOOptimizerContactTerm.h"
#include "CIOOptimizerSelfCollisionTerm.h"
#include "QPInterface.h"
#include <Dynamics/FEMUtils.h>
#include <Dynamics/FEMCIOEngine.h>

USE_PRJ_NAMESPACE

Coli indexCol(sizeType off,sizeType nr)
{
  Coli ret=Coli::Zero(nr);
  for(sizeType i=0; i<nr; i++)
    ret[i]=i+off;
  return ret;
}
Coli indexColInvalid(sizeType nr)
{
  return Coli::Constant(nr,-1);
}
//operators: mpi related
int CIOOptimizer::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable)
{
  sizeType interval=_sol._tree.get<sizeType>("modifyInterval",10);
  //we update neural network infrequently
  if(modifiable && fjac && interval > 0 && ((_iter++)%interval) == 0) {
    //update neural network
    updateNeuralNet(&x,true);
    //update contact
    if(_sol._tree.get<bool>("frequentContactUpdate",false)) {
      boost::shared_ptr<CIOOptimizerContactTerm> cTerm=getTerm<CIOOptimizerContactTerm>();
      if(cTerm)
        cTerm->updateContacts(NULL);
    }
    if(_sol._tree.get<bool>("handleSelfCollision",false)) {
      boost::shared_ptr<CIOOptimizerSelfCollisionTerm> scTerm=getTerm<CIOOptimizerSelfCollisionTerm>();
      if(scTerm)
        scTerm->updateSelfColl(NULL);
    }
  }
  //update penalty
  if(modifiable && _sol._tree.get<bool>("adaptiveADMMPenalty",true)) {
    boost::shared_ptr<CIOOptimizerPhysicsTerm> pTerm=getTerm<CIOOptimizerPhysicsTerm>();
    if(pTerm)
      pTerm->balanceResidual();
  }
  //compute gradient treating neural network as a function
  operator()(x,fvec,fjac,modifiable,true);
  return 0;
}
scalarD CIOOptimizer::operator()(const Vec& x,Vec* fgrad,STrips* fhess)
{
  Vec tmp=Vec::Zero(inputs());
  scalarD ret=operator()(x,tmp,fhess,false,false);
  if(fgrad)
    *fgrad=tmp;
  return ret;
}
scalarD CIOOptimizer::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable,bool LMOrSQP)
{
  return operator()(x,fvec,fjac,modifiable,LMOrSQP,Vec2i(0,_horizon));
}
scalarD CIOOptimizer::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable,bool LMOrSQP,const Vec2i& fromTo)
{
  if(fjac)
    fjac->clear();
  if(LMOrSQP) {
    fvec.setZero(values());
    updateConfig(x,false,&fromTo);
  } else {
    fvec.setZero(inputs());
  }

  scalarD ret=0;
  FEMCIOEngine& eng=_sol.getEngine();
  for(sizeType i=fromTo[0]; i<fromTo[1]; i++) {
    //state
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    if(LMOrSQP)
      runLM(eng,*_qpSol,i,fvec,fjac,modifiable);
    else ret+=runSQP(eng,*_qpSol,i,fvec,fjac);
    //update force
    _info._infos[i]=eng._c;
  }
  return ret;
}
void CIOOptimizer::operator()(Eigen::Map<const Vec>& xs,scalarD& FX,Eigen::Map<Vec>& DFDX)
{
  FX=0;
  DFDX.setZero();
  updateConfig(xs,true,true,NULL,NULL);
  FEMCIOEngine& eng=_sol.getEngine();
  for(sizeType i=0; i<_horizon; i++) {
    //state
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    runLBFGS(eng,i,FX,DFDX);
  }
}
void CIOOptimizer::updateConfig(const Vec& x,bool cf,bool hess,const Vec2i* fromTo,vector<boost::shared_ptr<FEMCIOEngine> >* mp)
{
  sizeType nrS=_sol.nrX()/2,offC=inputs();
  sizeType nrSWithoutRot=nrS;
  if(_fixRotation)
    nrSWithoutRot-=3;

  sizeType dim=_sol.getEngine().getBody().dim();
  scalar dt=_sol.getEngine()._tree.get<scalar>("dt");
  const CIOOptimizerContactTerm* cTerm=getTermConst<CIOOptimizerContactTerm>();
  Coli cOff=cTerm ? cTerm->getCollisionOff() : Coli::Zero(_horizon+1);
  _sol.getEngine().getBody()._tree.put<bool>("MTCubature",!mp);

  //multi-threading
  sizeType from=0;
  sizeType to=_horizon;
  if(fromTo) {
    from=max<sizeType>(fromTo->coeff(0)-2,0);
    to=fromTo->coeff(1);
  }
  if(cf && cTerm) {
    ASSERT_MSG(x.size() == offC+cOff[to]*dim,"If you want to update solution and contact force, provide it in x!")
  } else {
    ASSERT_MSG(x.size() >= offC,"If you want to update solution, provide it in x!")
  }

  //different modes
  if((_periodicPosition || _periodicRotation || _periodicDeform) && from < 2) {
    Vec xPeriodic=Vec::Zero(nrS);
    sizeType offPeriodicRot=inputs();
    sizeType offPeriodicPos=_periodicRotation ? inputs()-6 : inputs();
    const FEMCIOEngine& eng=_sol.getEngine();
    for(sizeType i=0; i<2; i++) {
      xPeriodic.segment(0,nrSWithoutRot)=
        x.segment((_horizon-(2-i))*nrSWithoutRot,nrSWithoutRot);
      if(_periodicPosition)
        xPeriodic.segment(nrS-6,3)=x.segment(offPeriodicPos-(2-i)*3,3);
      else xPeriodic.segment(nrS-6,3)=_info._xss[i].x().segment<3>(nrS-6);
      if(_periodicRotation)
        xPeriodic.segment(nrS-3,3)=x.segment(offPeriodicRot-(2-i)*3,3);
      else xPeriodic.segment(nrS-3,3)=_info._xss[i].x().segment<3>(nrS-3);
      _info._xss[i].reset(eng.getMCalc(),xPeriodic);
      if(hess)
        _info._xss[i].resetHessian(eng.getMCalc());
    }
  }

  //do work
  Vec xI;
  OMP_PARALLEL_FOR_XI(mp ? OmpSettings::getOmpSettings().nrThreads() : 1,OMP_PRI(xI))
  for(sizeType i=from; i<to; i++) {
    const FEMCIOEngine& eng=mp ? *(*mp)[omp_get_thread_num()] : _sol.getEngine();
    const RigidReducedMCalculator& MCalc=eng.getMCalc();
    xI=x.segment(i*nrSWithoutRot,nrSWithoutRot);
    if(_fixRotation)
      xI=concat(xI,Vec3d::Zero());
    _info._xss[i+2].reset(MCalc,xI);
    if(hess)
      _info._xss[i+2].resetHessian(MCalc);
  }
  OMP_PARALLEL_FOR_X(mp ? OmpSettings::getOmpSettings().nrThreads() : 1)
  for(sizeType i=from; i<to; i++) {
    const FEMCIOEngine& eng=mp ? *(*mp)[omp_get_thread_num()] : _sol.getEngine();
    const RigidReducedMCalculator& MCalc=eng.getMCalc();
    const FEMLCPSolver::Contacts& cs=eng.getCons();
    _info._infos[i].setXVGKeepIndex(MCalc,cs,&(_info._xss[i+2]),&(_info._xss[i+1]));
    if(cf && cTerm) {
      Vec cfI=x.segment(offC+cOff[i]*dim,_info._infos[i].cf().size());
      _info._infos[i].setCF(dt,MCalc,&cfI);
    }
  }
}
CIOOptimizer::Vec CIOOptimizer::removeCF(const Vec& X) const
{
  return X.segment(0,inputs());
}
CIOOptimizer::Vec CIOOptimizer::collectSolution(bool cf) const
{
  sizeType dim=_sol.getEngine().getBody().dim();
  const CIOOptimizerContactTerm* cTerm=getTermConst<CIOOptimizerContactTerm>();
  Coli cOff=cTerm ? cTerm->getCollisionOff() : Coli::Zero(_horizon+1);

  Vec ret;
  if(cf)
    ret=Vec::Zero(inputs()+cOff[cOff.size()-1]*dim);
  else ret=Vec::Zero(inputs());
  for(sizeType i=0; cf && i<_horizon; i++) {
    ASSERT(_info._infos[i].cf().size() == (cOff[i+1]-cOff[i])*dim)
    ret.segment(inputs()+cOff[i]*dim,_info._infos[i].cf().size())=_info._infos[i].cf();
  }

  for(sizeType i=-2; i<_horizon; i++) {
    Coli ids=blockIndexX(i);
    for(sizeType j=0; j<ids.size(); j++)
      if(ids[j] >= 0)
        ret[ids[j]]=_info._xss[i+2].x()[j];
  }
  return ret;
}
//helper
//add jacobian
void CIOOptimizer::addBlockXN(STrips& fjac,sizeType row,sizeType i,const Matd& blk) const
{
  addBlockXP(fjac,row,i-2,blk);
}
void CIOOptimizer::addBlockX(STrips& fjac,sizeType row,sizeType i,const Matd& blk) const
{
  addBlockXP(fjac,row,i-1,blk);
}
void CIOOptimizer::addBlockXP(STrips& fjac,sizeType row,sizeType i,const Matd& blk) const
{
  sizeType nrS=_sol.nrX()/2,nrSWithoutRot=nrS;
  if(_fixRotation)
    nrSWithoutRot-=3;
  if((_periodicPosition || _periodicRotation || _periodicDeform) && i < 0) {
    sizeType offPeriodicRot=inputs();
    sizeType offPeriodicPos=_periodicRotation ? inputs()-6 : inputs();
    //deformation part
    if(blk.cols() < nrS-6)
      return;
    else addBlock<Matd>(fjac,row,(_horizon+i)*nrSWithoutRot,blk.block(0,0,blk.rows(),nrS-6));
    //position part
    if(blk.cols() < nrS-3)
      return;
    else if(_periodicPosition)
      addBlock<Matd>(fjac,row,offPeriodicPos+i*3,blk.block(0,nrS-6,blk.rows(),3));
    //rotation part
    if(blk.cols() < nrS)
      return;
    else if(_periodicRotation)
      addBlock<Matd>(fjac,row,offPeriodicRot+i*3,blk.block(0,nrS-3,blk.rows(),3));
  } else if(i >= 0) {
    addBlock<Matd>(fjac,row,i*nrSWithoutRot,blk.block(0,0,blk.rows(),min<sizeType>(blk.cols(),nrSWithoutRot)));
  }
}
//add gradient
void CIOOptimizer::addBlockXN(Eigen::Map<Vec>& DFDX,sizeType i,const Vec& blk) const
{
  addBlockXP(DFDX,i-2,blk);
}
void CIOOptimizer::addBlockX(Eigen::Map<Vec>& DFDX,sizeType i,const Vec& blk) const
{
  addBlockXP(DFDX,i-1,blk);
}
void CIOOptimizer::addBlockXP(Eigen::Map<Vec>& DFDX,sizeType i,const Vec& blk) const
{
  sizeType nrS=_sol.nrX()/2,nrSWithoutRot=nrS;
  if(_fixRotation)
    nrSWithoutRot-=3;
  if((_periodicPosition || _periodicRotation || _periodicDeform) && i < 0) {
    sizeType offPeriodicRot=inputs();
    sizeType offPeriodicPos=_periodicRotation ? inputs()-6 : inputs();
    //deformation part
    if(blk.size() < nrS-6)
      return;
    else DFDX.segment((_horizon+i)*nrSWithoutRot,nrS-6)+=blk.segment(0,nrS-6);
    //position part
    if(blk.size() < nrS-3)
      return;
    else if(_periodicPosition)
      DFDX.segment(offPeriodicPos+i*3,3)+=blk.segment(nrS-6,3);
    //rotation part
    if(blk.size() < nrS)
      return;
    else if(_periodicRotation)
      DFDX.segment(offPeriodicRot+i*3,3)+=blk.segment(nrS-3,3);
  } else if(i >= 0) {
    DFDX.segment(i*nrSWithoutRot,min<sizeType>(blk.size(),nrSWithoutRot))+=
      blk.segment(0,min<sizeType>(blk.size(),nrSWithoutRot));
  }
}
//add gradient/hessian
void CIOOptimizer::addBlockSQP(sizeType i,const Vec& GI,const Matd& HI,Vec& G,STrips* H) const
{
  Coli ID=concat(blockIndexX(i-1),blockIndexX(i));
  //sizeType off=0;
  //while(off < ID.size() && ID[off] < 0)
  //  off+=ID.size()/2;
  for(sizeType r=0; r<ID.size(); r++)
    if(ID[r] >= 0)
      G[ID[r]]+=GI[r];
  if(H) {
    for(sizeType r=0; r<ID.size(); r++)
      for(sizeType c=0; c<ID.size(); c++)
        if(ID[r] >= 0 && ID[c] >= 0)
          H->push_back(STrip(ID[r],ID[c],HI(r,c)));
  }
}
Coli CIOOptimizer::blockIndex(sizeType i) const
{
  return concat(blockIndexX(i-2),blockIndexX(i-1));
}
Coli CIOOptimizer::blockIndexX(sizeType i) const
{
  sizeType nrS=_sol.nrX()/2,nrSWithoutRot=nrS;
  if(_fixRotation)
    nrSWithoutRot-=3;
  Coli ret=indexColInvalid(nrS);
  if(_periodicPosition || _periodicRotation || _periodicDeform) {
    sizeType offPeriodicRot=inputs();
    sizeType offPeriodicPos=_periodicRotation ? inputs()-6 : inputs();
    if(i >= 0) {
      ret.segment(0,nrSWithoutRot)=indexCol(i*nrSWithoutRot,nrSWithoutRot);
    } else {
      Coli def=indexCol((_horizon+i)*nrSWithoutRot,nrS-6);
      Coli lin=_periodicPosition ? indexCol(offPeriodicPos+i*3,3) : indexColInvalid(3);
      Coli rot=_periodicRotation ? indexCol(offPeriodicRot+i*3,3) : indexColInvalid(3);
      ret=concat(concat(def,lin),rot);
    }
  } else {
    ret.segment(0,nrSWithoutRot)=indexCol(i*nrSWithoutRot,nrSWithoutRot);
  }
  return ret;
}
//CIOOptimizerMP
CIOOptimizerMP::CIOOptimizerMP(FEMDDPSolver& sol,sizeType horizon,FEMDDPSolverInfo& info,sizeType K)
  :CIOOptimizer(sol,horizon,info,K)
{
  _sol.getEngine().getBody()._tree.put<bool>("MTCubature",false);
  FEMCIOEngine& eng=_sol.getEngine();
  _mp.resize(omp_get_num_procs());
  _qp.resize(_horizon);
  for(sizeType i=0; i<(sizeType)_mp.size(); i++)
    _mp[i]=boost::dynamic_pointer_cast<FEMCIOEngine>(eng.copy());
  for(sizeType i=0; i<(sizeType)_qp.size(); i++)
    _qp[i]=boost::shared_ptr<QPInterface>(new QPInterface);
}
//parallel operators
int CIOOptimizerMP::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable)
{
  //omp_set_nested(1);
  sizeType interval=_sol._tree.get<sizeType>("modifyInterval",10);
  //we update neural network infrequently
  if(modifiable && fjac && interval > 0 && ((_iter++)%interval) == 0) {
    //update neural network
    updateNeuralNet(&x,true);
    if(_sol._tree.get<bool>("frequentContactUpdate",false)) {
      boost::shared_ptr<CIOOptimizerContactTerm> cTerm=getTerm<CIOOptimizerContactTerm>();
      if(cTerm)
        cTerm->updateContacts(&_mp);
    }
    if(_sol._tree.get<bool>("handleSelfCollision",false)) {
      boost::shared_ptr<CIOOptimizerSelfCollisionTerm> scTerm=getTerm<CIOOptimizerSelfCollisionTerm>();
      if(scTerm)
        scTerm->updateSelfColl(&_mp);
    }
  }
  if(modifiable && _sol._tree.get<bool>("adaptiveADMMPenalty",true)) {
    boost::shared_ptr<CIOOptimizerPhysicsTerm> pTerm=getTerm<CIOOptimizerPhysicsTerm>();
    if(pTerm)
      pTerm->balanceResidual();
  }
  //compute gradient treating neural network as a function
  CIOOptimizer::operator()(x,fvec,fjac,modifiable,true);
  return 0;
}
scalarD CIOOptimizerMP::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable,bool LMOrSQP,const Vec2i& fromTo)
{
  if(fjac)
    fjac->clear();
  if(LMOrSQP) {
    fvec.setZero(values());
    updateConfig(x,false,fjac?true:false,&fromTo,&_mp);
  } else {
    fvec.setZero(inputs());
  }

  scalarD ret=0;
  OMP_PARALLEL_FOR_I(OMP_ADD(ret))
  for(sizeType i=fromTo[0]; i<fromTo[1]; i++) {
    ASSERT(i >= 0 && i < (sizeType)_qp.size())
    FEMCIOEngine& eng=*(_mp[omp_get_thread_num()]);
    QPInterface& qp=*(_qp[i]);
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    if(LMOrSQP)
      runLM(eng,qp,i,fvec,fjac,modifiable);
    else ret+=runSQP(eng,qp,i,fvec,fjac);
    //update force
    _info._infos[i]=eng._c;
  }
  return ret;
}
void CIOOptimizerMP::operator()(Eigen::Map<const Vec>& xs,scalarD& FX,Eigen::Map<Vec>& DFDX)
{
  scalarD FXLocal=0;
  Matd DFDXs=Matd::Zero(DFDX.size(),_mp.size());
  updateConfig(xs,true,true,NULL,&_mp);
  OMP_PARALLEL_FOR_I(OMP_ADD(FXLocal))
  for(sizeType i=0; i<_horizon; i++) {
    int tid=omp_get_thread_num();
    FEMCIOEngine& eng=*(_mp[tid]);
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    Eigen::Map<Vec> blk(DFDXs.data()+DFDX.size()*tid,DFDX.size());
    runLBFGS(eng,i,FXLocal,blk);
  }
  DFDX=DFDXs*Vec::Ones(DFDXs.cols());
  FX=FXLocal;
}
const vector<boost::shared_ptr<FEMCIOEngine> >* CIOOptimizerMP::mp() const
{
  return &_mp;
}
vector<boost::shared_ptr<FEMCIOEngine> >* CIOOptimizerMP::mp()
{
  return &_mp;
}
