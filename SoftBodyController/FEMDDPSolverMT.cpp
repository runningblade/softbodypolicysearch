#include "FEMDDPSolverMT.h"
#include "CIOPolicyOptimizer.h"
#include "CIOOptimizerPhysicsTerm.h"
#include "CIOOptimizerContactTerm.h"
#include "CIOOptimizerSelfCollisionTerm.h"
#include "OneLineCallback.h"
#include "DMPNeuralNet.h"
#include <CommonFile/Timing.h>
#include <CMAES/DenseInterface.h>
#include <CMAES/MLUtil.h>
#include <Dynamics/FEMUtils.h>
#include <Dynamics/LMInterface.h>
#include <Dynamics/FEMCIOEngine.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//helper
void addSTrips(CIOOptimizer::STrips& fjac,CIOOptimizer::STrips& fjacBlk,int offV,int offI)
{
  const CIOOptimizer::STrips::vector_type& vss=fjacBlk.getVector();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)vss.size(); i++) {
    const CIOOptimizer::STrip& T=vss[i];
    fjac.push_back(CIOOptimizer::STrip(T.row()+offV,T.col()+offI,T.value()));
  }
}
template <typename TA,typename TB>
void concatColFlexible(TA& A,const TB& B)
{
  if(A.rows() != B.rows())
    A=B;
  else A=concatCol(A,B);
}
//CIOOptimizerMT
CIOOptimizerMT::CIOOptimizerMT(FEMDDPSolverMT& sol,sizeType horizon)
  :_sol(sol),_iter(0),_horizon(horizon) {}
void CIOOptimizerMT::reset()
{
  sizeType nrInput=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    _tasks[i]->reset();
    nrInput+=_tasks[i]->BLEICInterface::inputs();
  }
  BLEICInterface::reset(nrInput,false);
}
int CIOOptimizerMT::inputs() const
{
  int ret=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    ret+=_tasks[i]->inputs();
  return ret;
}
int CIOOptimizerMT::values() const
{
  int ret=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    ret+=_tasks[i]->values();
  return ret;
}
int CIOOptimizerMT::valuesNoCollision() const
{
  int ret=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    ret+=_tasks[i]->valuesNoCollision();
  return ret;
}
//operators: mpi related
int CIOOptimizerMT::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable)
{
  sizeType interval=_sol._tree.get<sizeType>("modifyInterval",10);
  //we update neural network infrequently
  if(modifiable && fjac && interval > 0 && ((_iter++)%interval) == 0) {
    //update neural network
    updateNeuralNet(&x,true);
    //update contact
    if(_sol._tree.get<bool>("frequentContactUpdate",false))
      for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
        boost::shared_ptr<CIOOptimizerMP> taskMP=boost::dynamic_pointer_cast<CIOOptimizerMP>(_tasks[i]);
        boost::shared_ptr<CIOOptimizerContactTerm> cTerm=_tasks[i]->getTerm<CIOOptimizerContactTerm>();
        if(cTerm)
          cTerm->updateContacts(taskMP ? taskMP->mp() : NULL);
      }
    if(_sol._tree.get<bool>("handleSelfCollision",false))
      for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
        boost::shared_ptr<CIOOptimizerMP> taskMP=boost::dynamic_pointer_cast<CIOOptimizerMP>(_tasks[i]);
        boost::shared_ptr<CIOOptimizerSelfCollisionTerm> scTerm=_tasks[i]->getTerm<CIOOptimizerSelfCollisionTerm>();
        if(scTerm)
          scTerm->updateSelfColl(taskMP ? taskMP->mp() : NULL);
      }
  }
  //update penalty
  if(modifiable && _sol._tree.get<bool>("adaptiveADMMPenalty",true))
    for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
      boost::shared_ptr<CIOOptimizerPhysicsTerm> pTerm=_tasks[i]->getTerm<CIOOptimizerPhysicsTerm>();
      if(pTerm)
        pTerm->balanceResidual();
    }
  //compute gradient treating neural network as a function
  int offI=0;
  int offV=0;
  fvec.setZero(values());
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    int inputsI=_tasks[i]->inputs();
    int valuesI=_tasks[i]->values();
    _fvecBlk.setZero(valuesI);
    _tasks[i]->operator()(x.segment(offI,inputsI),_fvecBlk,fjac ? &_fjacBlk : NULL,modifiable,true);
    fvec.segment(offV,valuesI)=_fvecBlk;
    if(fjac)
      addSTrips(*fjac,_fjacBlk,offV,offI);
    offI+=inputsI;
    offV+=valuesI;
  }
  return 0;
}
scalarD CIOOptimizerMT::operator()(const Vec& x,Vec* fgrad,STrips* fhess)
{
  scalarD ret=0;
  if(fgrad)
    fgrad->setZero(inputs());

  Vec fgradBlk;
  STrips fhessBlk;
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    sizeType inputsI=_tasks[i]->inputs();
    if(fgrad)
      fgradBlk.setZero(inputsI);
    if(fhess)
      fhessBlk.clear();
    ret+=_tasks[i]->operator()(x.segment(off,inputsI),
                               fgrad ? &fgradBlk : (Vec*)NULL,
                               fhess ? &fhessBlk : (STrips*)NULL);
    if(fgrad)
      fgrad->segment(off,inputsI)=fgradBlk;
    if(fhess)
      addSTrips(*fhess,fhessBlk,off,off);
    off+=inputsI;
  }
  return ret;
}
void CIOOptimizerMT::operator()(Eigen::Map<const Vec>& xs,scalarD& FX,Eigen::Map<Vec>& DFDX)
{
  FX=0;
  int off=0;
  scalarD FXTask=0;
  Vec xsBlk,DFDXBlk;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    int inputsI=_tasks[i]->BLEICInterface::inputs();
    xsBlk=xs.segment(off,inputsI);
    Eigen::Map<const Vec> xsBLKMap(xsBlk.data(),inputsI);
    DFDXBlk.setZero(inputsI);
    Eigen::Map<Vec> DFDXBlkMap(DFDXBlk.data(),inputsI);
    _tasks[i]->operator()(xsBLKMap,FXTask,DFDXBlkMap);
    DFDX.segment(off,inputsI)=DFDXBlk;
    off+=inputsI;
    FX+=FXTask;
  }
}
void CIOOptimizerMT::updateConfig(const Vec& x,bool cf,bool hess,const Vec2i* fromTo)
{
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    int inputsI=cf ? _tasks[i]->BLEICInterface::inputs() : _tasks[i]->inputs();
    boost::shared_ptr<CIOOptimizerMP> taskMP=boost::dynamic_pointer_cast<CIOOptimizerMP>(_tasks[i]);
    _tasks[i]->updateConfig(x.segment(off,inputsI),cf,hess,fromTo,taskMP ? taskMP->mp() : NULL);
    off+=inputsI;
  }
}
CIOOptimizerMT::Vec CIOOptimizerMT::removeCF(const Vec& X) const
{
  Vec ret=Vec::Zero(0);
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    int inputsI=_tasks[i]->BLEICInterface::inputs();
    ret=concat(ret,_tasks[i]->removeCF(X.segment(off,inputsI)));
    off+=inputsI;
  }
  return ret;
}
CIOOptimizerMT::Vec CIOOptimizerMT::collectSolution(bool cf) const
{
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    off+=cf ? _tasks[i]->BLEICInterface::inputs() : _tasks[i]->inputs();

  Vec ret=Vec::Zero(off);
  off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    int inputsI=cf ? _tasks[i]->BLEICInterface::inputs() : _tasks[i]->inputs();
    ret.segment(off,inputsI)=_tasks[i]->collectSolution(cf);
    off+=inputsI;
  }
  return ret;
}
void CIOOptimizerMT::writeResidueInfo(const Vec& xs)
{
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    INFOV("------------------------------------------------Task%ld",i)
    int inputsI=_tasks[i]->BLEICInterface::inputs();
    _tasks[i]->writeResidueInfo(xs.segment(off,inputsI));
    off+=inputsI;
  }
}
void CIOOptimizerMT::assignControl()
{
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    _tasks[i]->assignControl();
}
//update neural network
void CIOOptimizerMT::updateWeight()
{
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    _tasks[i]->updateWeight();
}
void CIOOptimizerMT::updateNeuralNet() {}
void CIOOptimizerMT::updateNeuralNet(const Vec* x,bool modifiable)
{
  //data for training
  _states.setZero(0,0);
  _actions.setZero(0,0);
  _metrics.clear();
  //collect data
  int off=0;
  scalarD coef=1;
  Vss fvecs(_tasks.size());
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    int inputsI=_tasks[i]->inputs();
    //save
    _tasks[i]->beginSingleOutTwoTerms<CIOOptimizerPhysicsTerm,CIOOptimizerContactTerm>();
    boost::shared_ptr<CIOOptimizerPolicyPhysicsTerm> ppTerm=_tasks[i]->getTerm<CIOOptimizerPolicyPhysicsTerm>();
    if(ppTerm)
      ppTerm->setForNNUpdate(true);
    //operator
    Vec X=x ? Vec(x->segment(off,inputsI)) : _tasks[i]->collectSolution(false);
    _tasks[i]->operator()(X,fvecs[i],NULL,modifiable,true);
    coef=max<scalarD>(coef,collectNeuralNetData(*(_tasks[i]),fvecs[i]));
    //load
    if(ppTerm)
      ppTerm->setForNNUpdate(false);
    _tasks[i]->endSingleOutTerm();
    off+=inputsI;
  }
  //training
  for(sizeType i=0; i<(sizeType)_metrics.size(); i++)
    _metrics[i]*=coef;
  updateNeuralNet();
}
scalarD CIOOptimizerMT::collectNeuralNetData(const CIOOptimizer& opt,const Vec& fvec)
{
  sizeType nrS=_sol.nrX()/2,nrU=nrS-6,off=_states.cols();
  concatColFlexible(_states,Matd::Zero(nrU,_horizon));
  concatColFlexible(_actions,Matd::Zero(nrU,_horizon));
  _metrics.insert(_metrics.end(),Matd::Identity(nrU,nrU));
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<_horizon; i++) {
    _states.col(off+i)=opt.info()._xss[i+2].x().segment(0,nrU);
    _actions.col(off+i)=opt.DUDE()*fvec.segment(i*nrS,nrS);
  }
  return 1;
}
//update force
void CIOOptimizerMT::updateForce()
{
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    _tasks[i]->updateForce();
}
//for debug
void CIOOptimizerMT::compareDFDX(const Vec& DFDX0,const Vec& DFDX1) const
{
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    INFOV("------------------------------------------------Task%ld",i)
    int inputsI=_tasks[i]->BLEICInterface::inputs();
    _tasks[i]->compareDFDX(DFDX0.segment(off,inputsI),DFDX1.segment(off,inputsI));
    off+=inputsI;
  }
}
void CIOOptimizerMT::randomizeParameter(bool phys,bool coll)
{
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++) {
    //set random physics penalty
    if(phys)
      _tasks[i]->getTerm<CIOOptimizerPhysicsTerm>()->randomizeParam();
    //add random self collision
    boost::shared_ptr<CIOOptimizerSelfCollisionTerm> term=
      _tasks[i]->getTerm<CIOOptimizerSelfCollisionTerm>();
    if(term && coll)
      term->addRandomSelfCollision();
  }
  reset();
}
void CIOOptimizerMT::randomizeContact()
{
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    _tasks[i]->randomizeContact();
}
CIOOptimizerMT::Vec CIOOptimizerMT::get2DMask() const
{
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_tasks.size(); i++)
    off+=_tasks[i]->BLEICInterface::inputs();

  Vec ret=Vec::Ones(off);
  sizeType nrS=_sol.nrX()/2,nrU=nrS-6;
  off=0;
  for(sizeType t=0; t<(sizeType)_tasks.size(); t++) {
    int inputsI=_tasks[t]->BLEICInterface::inputs();
    for(sizeType i=0; i<_horizon; i++)
      ret.segment(off+i*nrS+nrU+2,3).setZero();
    off+=inputsI;
  }
  return ret;
}
bool CIOOptimizerMT::hasContact() const
{
  for(sizeType t=0; t<(sizeType)_tasks.size(); t++)
    if(!_tasks[t]->hasContact())
      return false;
  return true;
}
//CIOPolicyOptimizerMT
CIOPolicyOptimizerMT::CIOPolicyOptimizerMT(FEMDDPSolverMT& sol,sizeType horizon)
  :CIOOptimizerMT(sol,horizon),_net(static_cast<DMPNeuralNet&>(sol.getNeuralNet())) {}
//update neural network
void CIOPolicyOptimizerMT::updateNeuralNet()
{
  //fillin state, action
  _net.setData(_states,_actions,_metrics);
  //train neural network
  if(_sol.getEngine()._tree.get<bool>("inNeuralNetDebug",false))
    _net.NeuralNet::train();
  else _net.train();
  _net.clearData();
}
scalarD CIOPolicyOptimizerMT::collectNeuralNetData(const CIOOptimizer& opt,const Vec& fvec)
{
  FEMCIOEngine& eng=_sol.getEngine();
  scalar dt=eng._tree.get<scalar>("dt");
  //assemble solution
  sizeType nrS=_sol.nrX()/2,nrU=nrS-6,off=_states.cols();
  concatColFlexible(_states,Matd::Zero(1,_horizon));
  concatColFlexible(_actions,Matd::Zero(nrU,_horizon));
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<_horizon; i++) {
    _actions.col(off+i)=opt.DUDE()*fvec.segment(i*nrS,nrS);
    _states(0,off+i)=dt*(scalar)i;
  }
  //compute metric
  Matd metric=opt.getTermConst<CIOOptimizerPolicyPhysicsTerm>()->getCoefPhysADMM();
  _metrics.insert(_metrics.end(),metric);
  return CIOOptimizer::rescaleMetric(metric);
}
//FEMDDPSolverMT
FEMDDPSolverMT::FEMDDPSolverMT(FEMEnvironmentMT& env)
  :FEMDDPSolver(typeid(FEMDDPSolver).name(),env)
{
  if(env.solPtr() == NULL)
    return;
  if(isPolicy<NeuralNetPolicy>(env)) {
    NeuralNetPolicy& policy=static_cast<NeuralNetPolicy&>(env.getPolicy());
    _bound.reset(new Vec(policy.getActionScale()));
  }

  _env.resetContacts();
  _infos.resize(env.K());
  boost::property_tree::ptree& pt=_env.sol()._tree;
  if(!pt.get_optional<scalar>("collisionExpand")) {
    scalarD avgDim=_env.sol().avgElementLength();
    WARNINGV("collisionExpand property not defined, setting to default expand=%f!",avgDim)
    pt.put<scalar>("collisionExpand",avgDim);
  }

  if(_tree.get<string>("workspace","").empty()) {
    string name=env.sol().getBody()._tree.get<string>("meshName","");
    _tree.put<string>("workspace",name);
    recreate(name);
  }
}
bool FEMDDPSolverMT::read(istream& is,IOData* dat)
{
  _env.read(is,dat);
  readPtreeBinary(_tree,is);
  sizeType nr;
  readBinaryData(nr,is);
  _infos.resize(nr);
  for(sizeType i=0; i<nr; i++) {
    readVector(_infos[i]._xss,is,dat);
    readVector(_infos[i]._infos,is,dat);
  }
  return is.good();
}
bool FEMDDPSolverMT::write(ostream& os,IOData* dat) const
{
  _env.write(os,dat);
  writePtreeBinary(_tree,os);
  sizeType nr=(sizeType)_infos.size();
  writeBinaryData(nr,os);
  for(sizeType i=0; i<nr; i++) {
    writeVector(_infos[i]._xss,os,dat);
    writeVector(_infos[i]._infos,os,dat);
  }
  return os.good();
}
void FEMDDPSolverMT::readFromTree()
{
  writePtreeAscii(_tree,"./test.xml");
  sizeType horizon=_tree.get<sizeType>("horizon");
  for(sizeType i=0; i<(sizeType)_infos.size(); i++) {
    string taskStr="task"+boost::lexical_cast<string>(i);
    const boost::property_tree::ptree& child=_tree.get_child(taskStr);
    FEMDDPSolver::readFromTree(_env.sol().getMCalc(),_infos[i],child,horizon);
  }
}
void FEMDDPSolverMT::writeToTree()
{
  sizeType horizon=_tree.get<sizeType>("horizon");
  for(sizeType i=0; i<(sizeType)_infos.size(); i++) {
    string taskStr="task"+boost::lexical_cast<string>(i);
    if(!_tree.get_child_optional(taskStr))
      _tree.add_child(taskStr,boost::property_tree::ptree());
    boost::property_tree::ptree& child=_tree.get_child(taskStr);
    FEMDDPSolver::writeToTree(_env.sol().getMCalc(),_infos[i],child,horizon);
  }
}
void FEMDDPSolverMT::writeSelfCollVTK(const string& path)
{
  recreate(path);
  _tree.put<bool>("initContactUpdate",false);
  for(sizeType i=0; i<(sizeType)_infos.size(); i++) {
    boost::shared_ptr<CIOOptimizer> opt=createOpt(false,false,_infos[i]);
    boost::shared_ptr<CIOOptimizerSelfCollisionTerm> scTerm=opt->getTerm<CIOOptimizerSelfCollisionTerm>();
    if(scTerm)
      scTerm->writeSelfCollVTK(path+"/task"+boost::lexical_cast<string>(i));
  }
}
void FEMDDPSolverMT::writeStatesMTVTK(const string& path,bool useXs,bool pov) const
{
  recreate(path);
  for(sizeType i=0; i<(sizeType)_infos.size(); i++) {
    const_cast<FEMDDPSolverInfo&>(_info)=_infos[i];
    const string strTask="task"+boost::lexical_cast<string>(i);
    const boost::property_tree::ptree& child=_tree.get_child(strTask);
    writeStatesVTK(path+"/"+strTask,&child,useXs,pov);
  }
}
//debug and visualization
void FEMDDPSolverMT::debugObj(const Vec& x,const Vec& u,bool addObjUX)
{
#define PEAK_FRAME 20
#define NR_TEST 10
#define DELTA 1E-8f
  for(sizeType t=0; t<(sizeType)_infos.size(); t++) {
    scalarD dt=_env.sol()._tree.get<scalar>("dt");
    sizeType dim=_env.sol().getBody().dim();
    _env.clearEnergy();
    _env.addEnergyBalance(randVec(dim),randVec(dim),RandEngine::randR01());
    _env.addEnergyJump(RandEngine::randR01(),PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalk(randVec(dim),RandEngine::randR01());
    _env.addEnergyWalkTo(randVec(dim),true,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalkTo(randVec(dim),false,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalkTo(randVec(dim),true,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalkTo(randVec(dim),false,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalkSpeedTo(randVec(dim),true,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalkSpeedTo(randVec(dim),false,true,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalkSpeedTo(randVec(dim),true,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyWalkSpeedTo(randVec(dim),false,false,PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    if(dim == 2)
      _env.addEnergy2DRoll(RandEngine::randR01(),RandEngine::randR01());
    else _env.addEnergy3DRoll(Vec3::Random(),RandEngine::randR01());
    _env.addEnergy3DOrientation(randVec(dim),randVec(dim),RandEngine::randR01());
    _env.addEnergy3DOrientationTo(randVecR(dim),PEAK_FRAME*dt-dt*0.5f,RandEngine::randR01());
    _env.addEnergyCurvedWalk(randVec(dim),randVec(dim),RandEngine::randR01());
    dynamic_cast<FEMEnvironmentMT&>(_env).copyToTask(t);

    Matd fxx,fjac;
    Vec fx,fx2,fvec,fvec2;
    scalarD df,dfN,ddf,ddfN;
    scalarD f=DDPSolver::objX(PEAK_FRAME,x,fx,fxx);

    sizeType nrF=nrObjXF();
    fvec=objXF(PEAK_FRAME,x,&fjac);
    ASSERT(fvec.size() == nrF)
    INFOV("LM: %f Err: %f",fvec.squaredNorm()/2,fvec.squaredNorm()/2-f)

    for(sizeType i=0; i<NR_TEST; i++) {
      Vec deltaX=Vec::Random(x.size());
      df=fx.dot(deltaX);
      dfN=DDPSolver::objX(PEAK_FRAME,x+deltaX*DELTA,fx2);
      dfN=(dfN-f)/DELTA;
      INFOV("ObjGradient: %f, Err: %f!",df,df-dfN)
      ddf=(fxx*deltaX).norm();
      ddfN=((fx2-fx)/DELTA).norm();
      INFOV("ObjHessian: %f, Err: %f!",ddf,ddf-ddfN)
      fvec2=objXF(PEAK_FRAME,x+deltaX*DELTA,NULL);
      INFOV("ObjLMGradient: %f,Err: %f",(fjac*deltaX).norm(),(fjac*deltaX-(fvec2-fvec)/DELTA).norm())
    }
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
#undef PEAK_FRAME
}
void FEMDDPSolverMT::updateNeuralNet(bool debugMode,sizeType maxIt,scalar epsilon,scalar delta)
{
  readFromTree();
  _tree.put<bool>("initContactUpdate",false);
  _env.sol()._tree.put<bool>("inNeuralNetDebug",debugMode);
  getNeuralNet()._tree.put<sizeType>("reportInterval",100);
  getNeuralNet()._tree.put<sizeType>("maxIterations",maxIt);
  if(epsilon >= 0)
    getNeuralNet()._tree.put<scalar>("epsilon",epsilon);
  if(delta >= 0)
    getNeuralNet()._tree.put<scalar>("delta",delta);
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(true,true);
  opt->updateNeuralNet(NULL,false);
  opt->assignControl();
  writeToTree();
}
void FEMDDPSolverMT::writeResidualInfo(bool SP,bool MP)
{
  readFromTree();
  _tree.put<bool>("initContactUpdate",false);
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(SP,MP);
  opt->writeResidueInfo(opt->collectSolution(true));
}
void FEMDDPSolverMT::assignControl(bool SP)
{
  readFromTree();
  _tree.put<bool>("initContactUpdate",false);
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(SP,true);
  opt->assignControl();
  writeToTree();
}
void FEMDDPSolverMT::recenter(bool useXs)
{
  for(sizeType i=0; i<(sizeType)_infos.size(); i++) {
    const string strTask="task"+boost::lexical_cast<string>(i);
    boost::property_tree::ptree& child=_tree.get_child(strTask);
    FEMDDPSolver::recenter(&child,useXs);
  }
}
//for cio
void FEMDDPSolverMT::initializeCIO()
{
  for(sizeType i=0; i<(sizeType)_infos.size(); i++) {
    _info=_infos[i];
    FEMDDPSolver::initializeCIO("CIOInitialGuessTask"+boost::lexical_cast<string>(i));
    _infos[i]=_info;
  }
}
void FEMDDPSolverMT::optimizeCIO(bool MP)
{
  initializeCIO();
  //optimize
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(false,MP);
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  cb.writeToFile(_tree.get<string>("writeToFile",""));
  opt->reset();
  opt->setTolF(_tree.get<scalar>("epsF"));
  opt->setTolG(_tree.get<scalar>("epsG"));
  opt->setTolX(_tree.get<scalar>("epsX"));
  opt->setMaxIter(_tree.get<sizeType>("maxIter"));
  Vec x=opt->collectSolution(true);
  INFOV("Using epsGNorm: %f",_tree.get<scalar>("epsG"))
  //opt.solve(x);
  opt->writeResidueInfo(x);
  opt->solveCommonFile(x,cb);
  opt->writeResidueInfo(x);
  opt->updateConfig(x,true);
  cb.reset();
  INFOV("Termination type: %ld, using %ld iterations",
        opt->getTermination(),opt->getIterationsCount())
  //assign solution
  scalarD FX;
  Vec DFDX=x;
  opt->gradient(x,FX,DFDX);
  FEMDDPSolver::write();
}
void FEMDDPSolverMT::debugCIOGradient(bool SP,bool MP,bool load)
{
  if(load)
    FEMDDPSolver::read();
  else initializeCIO();
  //check gradient
  FEMCIOEngine& eng=_env.sol();
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(SP,MP);
  opt->randomizeParameter();
  Vec x=opt->collectSolution(true);
  Vec mask=Vec::Ones(x.size());
  if(eng.getBody().dim() == 2)
    mask=opt->get2DMask();
  //test
  INFO("CIO Gradient")
  opt->debugGradient(x,mask);
}
//for cio using ADLM
void FEMDDPSolverMT::optimizeCIOLM(bool MP,bool load)
{
  if(load)
    FEMDDPSolver::read();
  else initializeCIO();

  //initialize
  _tree.put<bool>("adaptiveADMMPenalty",false);
  boost::shared_ptr<CIOOptimizerMT> prob=createOptMT(false,MP);
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  cb.writeToFile(_tree.get<string>("writeToFile",""));
  LMInterface opt;
#ifdef SUITESPARSE_SUPPORT
  disableTiming();
  opt.useInterface(boost::shared_ptr<CholmodWrapper>(new CholmodWrapper));
#endif
  opt.setTolF(_tree.get<scalar>("epsF"));
  opt.setTolG(_tree.get<scalar>("epsG"));
  opt.setTolX(_tree.get<scalar>("epsX"));
  opt.setMaxIter(_tree.get<sizeType>("maxIter"));
  INFOV("Using epsGNorm: %f",_tree.get<scalar>("epsG"))
  //optimize
  Vec x=prob->collectSolution(false);
  opt.solveSparse(x,*prob,cb);
  prob->updateConfig(x,false);
  cb.reset();
  //assign solution
  Vec f=x;
  Matd fjac;
  prob->Objective<scalarD>::operator()(x,f,&fjac,false);
  //save result
  FEMDDPSolver::write();
}
void FEMDDPSolverMT::debugCIOLMEnergy(bool SP,bool MP,bool load)
{
  if(load)
    FEMDDPSolver::read();
  else initializeCIO();

  //initialize
  _tree.put<bool>("adaptiveADMMPenalty",false);
  _tree.put<bool>("handleSelfCollision",true);
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(SP,MP);
  opt->randomizeParameter();
  //check gradient
  scalarD FX,FX2;
  Objective<scalarD>::SMat fjac;
  Vec xLM=opt->collectSolution(false);
  Vec x=opt->collectSolution(true);
  Vec fvec,fvec2,DFDX=x;
  //check gradient LM
  opt->Objective<scalarD>::operator()(xLM,fvec,&fjac,false);
  FX2=fvec.squaredNorm()/2;
  opt->gradient(x,FX,DFDX);
  INFOV("FX: %f FXLM: %f Err: %f",FX,FX2,FX-FX2)
  INFOV("Grad: %f GradLM: %f Err: %f",
        (fjac.transpose()*fvec).norm(),opt->removeCF(DFDX).norm(),
        (fjac.transpose()*fvec-opt->removeCF(DFDX)).norm())
  //check gradient LM with force updated
  opt->Objective<scalarD>::operator()(xLM,fvec,&fjac,false);
  FX2=fvec.squaredNorm()/2;
#define NR_TEST 10
#define DELTA 1E-8f
  //otherwise, just check for gradients of energy
  {
    xLM=opt->collectSolution(false);
    opt->Objective<scalarD>::operator()(xLM,fvec,&fjac,false);
    for(sizeType i=0; i<NR_TEST; i++) {
      Vec delta=Vec::Random(xLM.size());
      opt->Objective<scalarD>::operator()(xLM+delta*DELTA,fvec2,(Matd*)NULL,false);
      INFOV("LMGradient: %f Err: %f",(fjac*delta).norm(),(fjac*delta-(fvec2-fvec)/DELTA).norm())
    }
  }
#undef DELTA
#undef NR_TEST
}
void FEMDDPSolverMT::debugCIOForce(bool SP,bool MP,bool load)
{
  if(load)
    FEMDDPSolver::read();
  else initializeCIO();

  //initialize
  _tree.put<bool>("forceInCone",false);
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(SP,MP);
  if(!opt->hasContact())
    return;

  //try twice, one using updateFFunc, the other using operator(modifiable)
  for(sizeType it=0; it<2; it++) {
    opt->randomizeContact();
    opt->randomizeParameter(false,false);
    //check gradient
    scalarD FX;
    Vec x=opt->collectSolution(true),DFDX0=x,DFDX1=x;
    //before
    opt->gradient(x,FX,DFDX0);
    //update force
    if(it == 0) {
      opt->updateConfig(x,true);
      opt->updateForce();
    } else {
      Vec fvec=Vec::Zero(opt->values());
      opt->operator()(x,fvec,NULL,true);
    }
    //after
    x=opt->collectSolution(true);
    opt->gradient(x,FX,DFDX1);
    //compare
    opt->compareDFDX(DFDX0,DFDX1);
  }
}
void FEMDDPSolverMT::debugCIOSQPEnergy(bool SP,bool MP,bool load)
{
  _tree.put<scalar>("regShuffleAvoidance",RandEngine::randR01());
  if(load)
    FEMDDPSolver::read();
  else initializeCIO();

  //initialize
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(SP,MP);
  //compute approximate quadratic energy
  CIOOptimizer::SMat H;
  Vec x=opt->collectSolution(false),f,G,G2;

#define NR_TEST 10
#define DELTA 1E-8f
  //debug SQPGradient
  for(sizeType i=0; i<NR_TEST; i++) {
    //note that you have to first call LM before SQP
    opt->Objective<scalarD>::operator()(x,f,(Matd*)NULL,false);
    scalarD E=opt->Objective<scalarD>::operator()(x,&G,&H);
    //debug gradient
    Vec delta=Vec::Random(opt->inputs());
    //note that you have to first call LM before SQP
    opt->Objective<scalarD>::operator()(x+delta*DELTA,f,(Matd*)NULL,false);
    scalarD E2=opt->Objective<scalarD>::operator()(x+delta*DELTA,&G2,(Matd*)NULL);
    INFOV("SQPGradient: %f Err: %f",G.dot(delta),G.dot(delta)-(E2-E)/DELTA)
    INFOV("SQPHessian: %f Err: %f",(H*delta).norm(),(H*delta-(G2-G)/DELTA).norm())
  }
#undef DELTA
#undef NR_TEST
}
//for policy search
void FEMDDPSolverMT::optimizeCIOPolicy(bool SP,bool MP,bool load)
{
  if(load)
    FEMDDPSolver::read();
  else initializeCIO();
  //initialize
  boost::shared_ptr<CIOOptimizerMT> prob=createOptMT(SP,MP);
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  cb.writeToFile(_tree.get<string>("writeToFile",""));
  cb._rollback=false;
  //initialize time span
  NeuralNet& net=getNeuralNet();
  scalar dt=_env.sol()._tree.get<scalar>("dt");
  sizeType horizon=_tree.get<sizeType>("horizon");
  net._tree.put<scalar>("initTimeSpan",(scalar)horizon*dt);
  net.resetOffline();
  //cb._rollback=false;
  LMInterface opt;
#ifdef SUITESPARSE_SUPPORT
  disableTiming();
  opt.useInterface(boost::shared_ptr<CholmodWrapper>(new CholmodWrapper));
#endif
  opt.setTolF(_tree.get<scalar>("epsF"));
  opt.setTolG(_tree.get<scalar>("epsG"));
  opt.setTolX(_tree.get<scalar>("epsX"));
  opt.setMaxIter(_tree.get<sizeType>("maxIter"));
  //optimize
  scalarD minPolicyDelta=_tree.get<scalar>("minPolicyDelta",1E-3f)*prob->valuesNoCollision();
  sizeType maxPolicyIter=_tree.get<sizeType>("maxIterPolicy",_tree.get<sizeType>("maxIterILQG"));
  Vec x=prob->collectSolution(false);
  INFOV("Using epsGNorm: %f, minPolicyDelta: %f",_tree.get<scalar>("epsG"),minPolicyDelta)
  for(sizeType it=0; it < maxPolicyIter; it++) {
    cb.reset();
    opt.solveSparse(x,*prob,cb);
    cb.reset();
    prob->updateConfig(x,false);
    prob->updateNeuralNet(NULL,false);
    prob->updateWeight();
    //profile
    WARNINGV("CIOPolicyIter%ld: f0=%f, f1=%f, minPolicyDelta=%f",it,cb._f0,cb._f1,minPolicyDelta)
    if(cb._f1 > cb._f0-minPolicyDelta)
      break;
  }
  //assign solution
  Vec f=x;
  Matd fjac;
  prob->Objective<scalarD>::operator()(x,f,&fjac,false);
  //save result
  FEMDDPSolver::write();
}
void FEMDDPSolverMT::debugNeuralNet(bool load)
{
#define DELTA 1E-8f
  if(load)
    FEMDDPSolver::read();
  else initializeCIO();
  _env.sol()._tree.put<bool>("inNeuralNetDebug",true);
  _env.sol()._tree.put<bool>("adaptiveADMMPenalty",false);
  _env.resetPolicy();
  _env.getPolicy();
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("./ddp.dat",ios::binary);
    write(os,dat.get());
  }
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is("./ddp.dat",ios::binary);
    read(is,dat.get());
  }
  boost::shared_ptr<CIOOptimizerMT> opt=createOptMT(true,true);
  opt->randomizeParameter();
  NeuralNet& net=getNeuralNet();
  //randomize neural-net parameter
  Vec weights=net.toVec(false);
  weights.setRandom();
  net.fromVec(weights);
  INFOV("NeuralNet has %ld params!",weights.size())

  Vec fvec,fvec2;
  sizeType NR_TEST=_tree.get<sizeType>("debugNeuralNetIter",20);
  for(sizeType it=0; it<NR_TEST; it++) {
    //check gradient
    weights=net.toVec(false);
    opt->operator()(opt->collectSolution(false),fvec,NULL,false);
    Vec delta=Vec::Random(weights.size());
    net.fromVec(weights+delta*DELTA);
    opt->operator()(opt->collectSolution(false),fvec2,NULL,false);
    net.fromVec(weights);
    INFOV("NeuralNet Iter%ld: %f Gradient: %f",it,fvec.squaredNorm()/2,(fvec2.squaredNorm()-fvec.squaredNorm())/DELTA)
    //optimize neural-net
    getNeuralNet()._tree.put<scalar>("epsilon",0);
    getNeuralNet()._tree.put<scalar>("delta",0);
    opt->updateNeuralNet(NULL,false);
  }
  _env.sol()._tree.put<bool>("inNeuralNetDebug",false);
  _env.resetPolicy();
  _env.getPolicy();
#undef DELTA
}
boost::shared_ptr<CIOOptimizerMT> FEMDDPSolverMT::createOptMT(bool SP,bool MP)
{
  sizeType horizon=_tree.get<sizeType>("horizon");
  boost::shared_ptr<CIOOptimizerMT> ret;
  if(SP)
    ret.reset(new CIOPolicyOptimizerMT(*this,horizon));
  else ret.reset(new CIOOptimizerMT(*this,horizon));
  //add tasks
  for(sizeType i=0; i<(sizeType)_infos.size(); i++) {
    boost::shared_ptr<CIOOptimizer> opt=createOpt(SP,MP,_infos[i],i);
    ret->_tasks.push_back(opt);
  }
  //reset
  ret->reset();
  return ret;
}
