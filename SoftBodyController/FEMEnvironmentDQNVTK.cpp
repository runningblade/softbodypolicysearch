#include "FEMEnvironmentDQN.h"
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/FEMUtils.h>

#include <CommonFile/MakeMesh.h>
#include <CommonFile/geom/StaticGeom.h>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

USE_PRJ_NAMESPACE

void writeFloorVTK(const string& path,const Vec3& ctr,scalar blockSz,sizeType blockNr)
{
  vector<scalar> css;
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  for(sizeType x=-blockNr; x<blockNr; x++)
    for(sizeType y=-blockNr; y<blockNr; y++) {
      //z
      vss.push_back(Vec3i(x  ,y  ,-blockNr-1).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i(x+1,y+1,-blockNr  ).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i(x  ,y  , blockNr  ).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i(x+1,y+1, blockNr+1).cast<scalar>()*blockSz+ctr);
      //y
      vss.push_back(Vec3i(x  ,-blockNr-1,y  ).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i(x+1,-blockNr  ,y+1).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i(x  , blockNr  ,y  ).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i(x+1, blockNr+1,y+1).cast<scalar>()*blockSz+ctr);
      //x
      vss.push_back(Vec3i(-blockNr-1,x  ,y  ).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i(-blockNr  ,x+1,y+1).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i( blockNr  ,x  ,y  ).cast<scalar>()*blockSz+ctr);
      vss.push_back(Vec3i( blockNr+1,x+1,y+1).cast<scalar>()*blockSz+ctr);
      //cell size
      for(sizeType c=0; c<6; c++)
        css.push_back((x+y+blockNr*2)%2);
    }
  VTKWriter<scalar> os("floor",path,true);
  os.appendVoxels(vss.begin(),vss.end(),true);
  os.appendCustomData("cell",css.begin(),css.end());
}
void writeTargetVTKOrPov(const string& path,const Vec3& target,scalar rad,bool pov)
{
  ObjMesh m;
  MakeMesh::makeSphere3D(m,rad,32);
  m.getPos()=target;
  m.applyTrans();
  if(pov)
    m.writePov(path);
  else m.writeVTK(path,true);
}
void writeObstacleCylinderVTKOrPov(const string& path,const Mat4Xd& obstacle,const Vec3& ctr,const Vec3& dir,scalar h,bool pov)
{
  ObjMesh m;
  VTKWriter<scalar> os("sphere",path,true);
  for(sizeType i=0; i<obstacle.cols(); i++) {
    MakeMesh::makeCylinder3D(m,(scalar)obstacle(3,i),h,128,128);
    //rotate
    Quatd q;
    q.setFromTwoVectors(Vec3d(0,1,0),dir.cast<scalarD>());
    m.getT()=q.toRotationMatrix().cast<scalar>();
    m.applyTrans();
    //translate
    m.getPos()=obstacle.block<3,1>(0,i).cast<scalar>()+ctr;
    m.applyTrans();
    if(pov) {
      string pathC=path;
      boost::replace_all(pathC,".pov",boost::lexical_cast<string>(i)+".pov");
      m.writePov(pathC);
    } else m.writeVTK(os);
  }
}
void writeObstacleSphereVTKOrPov(const string& path,const Mat4Xd& obstacle,bool pov)
{
  ObjMesh m;
  VTKWriter<scalar> os("sphere",path,true);
  for(sizeType i=0; i<obstacle.cols(); i++) {
    MakeMesh::makeSphere3D(m,(scalar)obstacle(3,i),128);
    m.getPos()=obstacle.block<3,1>(0,i).cast<scalar>();
    m.applyTrans();
    if(pov) {
      string pathC=path;
      boost::replace_all(pathC,".pov",boost::lexical_cast<string>(i)+".pov");
      m.writePov(pathC);
    } else m.writeVTK(os);
  }
}
void FEMEnvironmentDQN::writeEnvVTK(const string& path,const Mat4Xd* obstacle,bool dynamic) const
{
  sizeType nrS=_sol->getMCalc().size();
  //floor
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalar blockSz=_sol->_tree.get<scalar>("floorBlockSz",5);
  scalar blockNr=_sol->_tree.get<scalar>("floorBlockNr",10);
  scalar r=bb.getExtent().norm()*_sol->_tree.get<scalar>("targetBallSz",0.1f);
  scalar h=bb.getExtent().norm()*blockSz;
  Vec3 ctr=_state.segment<3>(nrS*2-6).cast<scalar>();
  Vec3 g=getIgnoreDir().cast<scalar>();
  if(g.norm() > 1E-10f && _sol->_geom && _sol->_geom->nrG() > 0) {
    BBox<scalar> bbGeom=_sol->_geom->getBVH()[0]._bb;
    ctr-=ctr.dot(g.normalized())*g.normalized();
    ctr+=g.normalized()*std::max(bbGeom._minC.dot(g.normalized()),bbGeom._maxC.dot(g.normalized()));
    ctr+=g.normalized()*h*blockNr;
  }
  //write
  if(dynamic) {
    string pathC=path;
    boost::replace_all(pathC,".vtk","Obstacle.vtk");
    if(obstacle)
      writeObstacleVTKOrPov(pathC,*obstacle,false);
  } else {
    string p=(boost::filesystem::path(path).parent_path()/"environment").string();
    if(!exists(p)) {
      create(p);
      writeFloorVTK(p+"/floor.vtk",ctr,h,(sizeType)blockNr);
      //obstacle/target
      writeTargetVTKOrPov(p+"/target.vtk",_target.cast<scalar>(),r,false);
      if(obstacle)
        writeObstacleVTKOrPov(p+"/obstacle.vtk",*obstacle,false);
    }
  }
}
void FEMEnvironmentDQN::writeEnvPov(const string& path,const Mat4Xd* obstacle,bool dynamic) const
{
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalar r=bb.getExtent().norm()*_sol->_tree.get<scalar>("targetBallSz",0.1f);
  //write
  if(dynamic) {
    string pathC=path;
    boost::replace_all(pathC,".pov","Obstacle.pov");
    if(obstacle)
      writeObstacleVTKOrPov(pathC,*obstacle,true);
  } else {
    string p=(boost::filesystem::path(path).parent_path()/"environmentPov").string();
    if(!exists(p)) {
      create(p);
      //obstacle/target
      writeTargetVTKOrPov(p+"/target.pov",_target.cast<scalar>(),r,true);
      if(obstacle)
        writeObstacleVTKOrPov(p+"/obstacle.pov",*obstacle,true);
    }
  }
}
void FEMEnvironmentDQN::writeObstacleVTKOrPov(const string& path,const Mat4Xd& obstacle,bool pov) const
{
  sizeType nrS=_sol->getMCalc().size();
  BBox<scalar> bb=_sol->getBody().getBB(true);
  scalar blockSz=_sol->_tree.get<scalar>("floorBlockSz",5);
  scalar blockNr=_sol->_tree.get<scalar>("floorBlockNr",10);
  scalar h=bb.getExtent().norm()*blockSz;
  Vec3 ctr=_state.segment<3>(nrS*2-6).cast<scalar>();
  Vec3 g=getIgnoreDir().cast<scalar>();
  if(g.norm() > 1E-10f) {
    ctr=ctr.dot(g.normalized())*g.normalized();
    writeObstacleCylinderVTKOrPov(path,obstacle,ctr,g.normalized(),h*blockNr,pov);
  } else writeObstacleSphereVTKOrPov(path,obstacle,pov);
}
void FEMEnvironmentDQN::writeLocusVTK(const string& path) const
{
  sizeType nrS=_sol->getMCalc().size();
  static string pathLocusLast="";
  static vector<Vec3d,Eigen::aligned_allocator<Vec3d> > ctrLocus;
  if(pathLocusLast != boost::filesystem::path(path).parent_path().string()) {
    pathLocusLast=boost::filesystem::path(path).parent_path().string();
    ctrLocus.clear();
  }
  ctrLocus.push_back(_state.segment<3>(nrS*2-6));
  //write locus
  string pathC=path;
  boost::replace_all(pathC,".vtk","Locus.vtk");
  VTKWriter<scalar> os("ctrLocus",pathC,true);
  os.appendPoints(ctrLocus.begin(),ctrLocus.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,1),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)ctrLocus.size()-1,0,1),
                 VTKWriter<scalar>::LINE);
}
void FEMEnvironmentDQN::writeLocusPov(const string& path) const
{
  sizeType nrS=_sol->getMCalc().size();
  static string pathLocusLast="";
  static vector<Vec3d,Eigen::aligned_allocator<Vec3d> > ctrLocus;
  if(pathLocusLast != boost::filesystem::path(path).parent_path().string()) {
    pathLocusLast=boost::filesystem::path(path).parent_path().string();
    ctrLocus.clear();
  }
  ctrLocus.push_back(_state.segment<3>(nrS*2-6));
  //write locus
  string pathC=path;
  boost::replace_all(pathC,".pov","Locus.pov");
  boost::filesystem::ofstream os(pathC);
  for(sizeType i=0; i<(sizeType)ctrLocus.size(); i++)
    os << ctrLocus[i][0] << "," << ctrLocus[i][1] << "," << ctrLocus[i][2] << "," << endl;
}
