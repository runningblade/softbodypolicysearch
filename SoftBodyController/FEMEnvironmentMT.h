#ifndef FEM_ENVIRONMENT_MT_H
#define FEM_ENVIRONMENT_MT_H

#include "FEMEnvironment.h"

PRJ_BEGIN

class FEMEnvironmentDQN;
class FEMEnvironmentDQNObstacle;
class FEMEnvironmentDQNObstacleCNN;
class FEMEnvironmentDQNTunnelSimplified;
class FEMEnvironmentMT : public FEMEnvironment, public EnvironmentMT
{
public:
  FEMEnvironmentMT();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void reset(const boost::property_tree::ptree& pt);
  virtual sizeType K() const override;
  virtual void setK(sizeType k) override;
  virtual NeuralNetPolicy& getPolicy() override;
  virtual void debugDMP(const string& DMP,sizeType nrFrm,scalar dt);
  //upgrade to DQN learnable FEMEnvironment
  virtual boost::shared_ptr<FEMEnvironmentDQN> upgradeDQN(const boost::property_tree::ptree* pt=NULL);
  virtual boost::shared_ptr<FEMEnvironmentDQNObstacle> upgradeDQNObstacle(const boost::property_tree::ptree* pt=NULL);
  //multitask objective function
  virtual scalarD getCostMT(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA,sizeType K) const;
  virtual Vec getCostFMT(sizeType i,const Vec& state,const Vec& A,Matd* FJac,sizeType K) const;
  virtual sizeType nrCostFMT(sizeType K) const;
  //invalidate singletask objective function
  virtual scalarD getCost(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const override;
  virtual Vec getCostF(sizeType i,const Vec& state,const Vec& A,Matd* FJac) const override;
  virtual sizeType nrCostF() const override;
  virtual void copyToTask(sizeType id);
  virtual void setState(const Vec& state) override;
  //multitask augmented feature
  void clearAugData();
  void setupAugDataInterp(const Vec& aug0,const Vec& aug1,sizeType nrTraj);
  void setupSampleAllTask(bool sampleAllTask);
  virtual void setTrajId(sizeType trajId) override;
  virtual void setMTAug(const Vec& aug);
  virtual void getMTAug(Vec& aug) const;
  virtual sizeType nrMTAug() const;
  //MultiTraj
  virtual void startMultiTraj(MultiTrajCallback& cb);
protected:
  FEMEnvironmentMT(const string& name);
  virtual Vec extractState() const override;
  vector<vector<boost::shared_ptr<DDPEnergy> > > _essMT;
  boost::shared_ptr<Matd> _augs;
  bool _sampleAllTask;
};

PRJ_END

#endif
