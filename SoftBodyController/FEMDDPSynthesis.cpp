#include "FEMDDPSynthesis.h"
#include "OneLineCallback.h"
#include "FEMGradientOpt.h"
#include "DMPNeuralNet.h"
#include "FEMTracking.h"
#include "DMPLayerMT.h"
#include "MLUtil.h"
#include <Dynamics/FEMUtils.h>
#include <Dynamics/LMInterface.h>
#include <Dynamics/FEMRotationUtil.h>
#include <CommonFile/Heap.h>
#include <CommonFile/MakeMesh.h>
#include <CommonFile/geom/StaticGeom.h>
#include <boost/unordered_map.hpp>

USE_PRJ_NAMESPACE

//helper
Matd extractLSYN(const Matd& L,int r,int c)
{
  Matd ret=Matd::Zero(L.cols(),L.cols());
  for(sizeType i=0; i<L.rows(); i+=3)
    ret+=L.row(i+r).transpose()*L.row(i+c);
  return ret;
}
Mat3Xd extractLCtrSYN(Matd& L)
{
  //compute center
  Mat3Xd ctr=Mat3Xd::Zero(3,L.cols());
  for(sizeType i=0; i<L.rows(); i+=3)
    ctr+=L.block(i,0,3,L.cols());
  ctr/=(scalarD)(L.rows()/3);
  //remove center
  for(sizeType i=0; i<L.rows(); i+=3)
    L.block(i,0,3,L.cols())-=ctr;
  return ctr;
}
struct AStarNode {
  typedef FEMDDPSynthesis::Vec Vec;
  bool operator>(const AStarNode& other) const {
    return _cost > other._cost;
  }
  Vec2i _seq;
  sizeType _parent;
  Vec _x;
  //cost related
  sizeType _nrFrame;
  scalarD _cost;
};
class AStar
{
public:
  typedef FEMDDPSynthesis::Vec Vec;
  typedef boost::unordered_map<Vec3i,sizeType,Hash> HASH;
  typedef vector<Vec2i,Eigen::aligned_allocator<Vec2i> > Path;
  AStar(FEMDDPSynthesis& syn,const Vec3d& tar,const scalarD rad):_syn(syn),_tar(tar),_rad(rad) {}
  Path solve(const Vec& x,const boost::property_tree::ptree& pt) {
    //initialize
    AStarNode node;
    node._seq=Vec2i(-1,-1);
    node._parent=-1;
    node._x=x;
    node._nrFrame=0;
    node._cost=0;
    initHeap();
    pushHeap(node);
    //search
    AStarNode bestNode=node;
    scalarD dist,bestCost=0,minDist=ScalarUtil<scalarD>::scalar_max;
    sizeType maxIter=pt.get<sizeType>("maxAStarIter",2000);
    sizeType checkIter=pt.get<sizeType>("checkAStarIter",10);
    for(sizeType i=0; !emptyHeap() && (i<maxIter || minDist >= _rad); i++) {
      if(i%checkIter == 0 && i > 0) {
        INFOV("AStar iter=%ld/%ld minDist=%f rad=%f",i,maxIter,minDist,_rad)
      }
      sizeType id=popHeap(),currFrm;
      const AStarNode n=_nodes[id];
      for(sizeType t=0; t<_syn.nrTask(); t++) {
        //run animation
        const FEMDDPSolverInfo& T=_syn.getTask(t);
        node._seq[0]=t;
        node._seq[1]=(n._seq[0] == t) ? 0 : _syn.findFrm(T,n._x);
        node._parent=id;
        node._x=_syn.applyTask(T,node._nrFrame=n._nrFrame,currFrm=node._seq[1],n._x,&_tar,&dist,true);
        if(node._x.size() == 0) //this means this is invalid, usually because we are in solid obstacle
          continue;
        getCost(node);
        //update node
        if(dist < minDist)
          minDist=dist;
        if(dist < _rad) {
          _history.push_back(getPos(node));
          if(node._cost > bestCost) {
            INFOV("Found AStar solution: cost=%f, iter=%ld!",node._cost,i)
            onNewNode(node);
            bestCost=node._cost;
            bestNode=node;
          }
        } else if(isNewNode(node)) {
          _history.push_back(getPos(node));
          pushHeap(node);
        }
      }
    }
    writeHistoryVTK("./history.vtk");
    return constructPath(bestNode);
  }
  void writeHistoryVTK(const string& path) const {
    vector<scalarD> css;
    for(sizeType i=0; i<(sizeType)_history.size(); i++)
      css.push_back(i);
    VTKWriter<scalarD> os("history",path,true);
    os.appendPoints(_history.begin(),_history.end());
    os.appendCustomPointData("order",css.begin(),css.end());
    os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,0,0),
                   VTKWriter<scalarD>::IteratorIndex<Vec3i>(_history.size(),0,0),
                   VTKWriter<scalarD>::POINT);
  }
  virtual ~AStar() {}
protected:
  //data
  virtual void onNewNode(const AStarNode& node) const {}
  virtual bool isNewNode(const AStarNode& node) {
    Vec3i id=floorV(getPos(node)/_rad*10.0f);
    if(_newNode.find(id) == _newNode.end()) {
      _newNode[id]=node._cost;
      return true;
    } else if(_newNode[id] < node._cost) {
      _newNode[id]=node._cost;
      return true;
    } else return false;
  }
  virtual Vec3d getPos(const AStarNode& node) const {
    return node._x.segment<3>(node._x.size()-6);
  }
  virtual void getCost(AStarNode& node) const {
    scalarD dist0=(getPos(_nodes[0])-_tar).norm();
    scalarD dist1=(getPos(node)-_tar).norm();
    node._cost=(dist0-dist1)/(scalarD)node._nrFrame;
  }
  Path constructPath(AStarNode bestNode) const {
    Path ret;
    while(bestNode._parent != -1) {
      ret.push_back(bestNode._seq);
      bestNode=_nodes[bestNode._parent];
    }
    std::reverse(ret.begin(),ret.end());
    return ret;
  }
  FEMDDPSynthesis& _syn;
  const Vec3d& _tar;
  const scalarD _rad;
  //heap
  void initHeap() {
    _heap.clear();
    _heapOffsets.clear();
    _nodes.clear();
  }
  void pushHeap(const AStarNode& node) {
    _nodes.push_back(node);
    _heapOffsets.push_back(-1);
    pushHeapMaxDef(_nodes,_heapOffsets,_heap,(sizeType)_nodes.size()-1);
  }
  sizeType popHeap() {
    sizeType err;
    ASSERT(!_heap.empty())
    return popHeapMaxDef(_nodes,_heapOffsets,_heap,err);
  }
  bool emptyHeap() const {
    return _heap.empty();
  }
  vector<sizeType> _heap;
  vector<sizeType> _heapOffsets;
  vector<AStarNode> _nodes;
  //new node
  boost::unordered_map<Vec3i,scalarD,Hash> _newNode;
  //history
  vector<Vec3d,Eigen::aligned_allocator<Vec3d> > _history;
};
//FEMDDPSynthesis
FEMDDPSynthesis::FEMDDPSynthesis(FEMEnvironment& env):FEMDDPSolver(env)
{
  _env.sol().getBody()._tree.put<bool>("MTCubature",true);
}
void FEMDDPSynthesis::addTaskData(const boost::property_tree::ptree& pt)
{
  //create defoMetric
  Vec B0;
  _env.sol().getBody().getPos(B0);
  Matd L=concatCol(_env.sol().getMCalc()._innerMCalc->getL(),B0);
  _LCtr=extractLCtrSYN(L);
  _defoMetricSqrt=matrixSqrt<Matd>(L.transpose()*L);
  //create AToBRTpl
  for(int c=0,k=0; c<3; c++)
    for(int r=0; r<3; r++)
      _AToBRTpl[k++]=extractLSYN(L,r,c);
  //add task data
  _tasks.clear();
  _taskCyclics.clear();
  if(pt.get_child_optional("task0")) {
    for(sizeType i=0;; i++) {
      const string taskStr="task"+boost::lexical_cast<string>(i);
      boost::optional<const boost::property_tree::ptree&>
      child=pt.get_child_optional(taskStr);
      if(child)
        addTaskDataInner(*child);
      else break;
    }
  } else {
    addTaskDataInner(pt);
  }
}
void FEMDDPSynthesis::addTaskDataInner(const boost::property_tree::ptree& pt)
{
  //fill task
  const FEMCIOEngine& eng=_env.sol();
  scalar dt=eng._tree.get<scalar>("dt");
  sizeType frm=pt.get<sizeType>("frm",-1);
  const RigidReducedMCalculator& MCalc=eng.getMCalc();
  FEMDDPSolverInfo task;
  task._xss.resize(frm+1);
  task._tss.resize(frm);
  INFOV("Adding task %ld!",_tasks.size())
  for(sizeType i=0; i<frm; i++) {
    Vec x=parseVec<Vec>("x",i,pt);
    sizeType nrS=x.size()/2;
    task._xss[i].reset(MCalc,x.segment(0,nrS));
    if(i == frm-1)
      task._xss[frm].reset(MCalc,x.segment(nrS,nrS));
    task._tss[i]=dt*i;
  }
  //fill contact info
  addContactInfo(task);
  calcAvgDist(task);
  _tasks.push_back(task);

  FEMDDPSolverInfo taskCyclic;
  NRDMPLayer* nl=getNeuralNet().getLayer<NRDMPLayer>(0);
  if(!nl) {
    //initialize
    INFO("Detecting cyclic!")
    //find best periodic animation subsequence
    sizeType minI=-1,minJ=-1;
    scalarD minDist=ScalarUtil<scalarD>::scalar_max;
    scalarD thres=eng.avgElementLength()*_tree.get<scalar>("periodicContactThres",1);
    sizeType range=_tree.get<sizeType>("periodicContactRange",1);
    scalarD periodUnit=getNeuralNet().getLayer<DMPLayer>(0)->period();
    sizeType maxPatch=0;
    while(true) {
      scalarD period=periodUnit;
      while(true) {
        sizeType nrFrame=(sizeType)ceil(period/dt);
        if(nrFrame >= frm)
          break;
        if(!task._infos.empty() && maxPatch == 0)
          for(sizeType i=0; i<frm; i++)
            maxPatch=max(maxPatch,task._dss[i].size());
        for(sizeType i=0; i+nrFrame<frm; i++) {
          //contact aware metric
          scalarD dist=0;
          sizeType otherId=-1;
          if(!task._infos.empty()) {
            if(task._dss[i].size() != maxPatch || task._dss[i].maxCoeff() > thres)
              continue;
            otherId=i+nrFrame;
            scalarD otherDist=ScalarUtil<scalarD>::scalar_max;
            for(sizeType j=i+nrFrame-range; j<=min(frm-1,i+nrFrame+range); j++) {
              if(task._dss[j].size() == maxPatch && task._dss[j].maxCoeff() < otherDist) {
                otherDist=task._dss[j].maxCoeff();
                otherId=j;
              }
            }
            if(otherDist > thres)
              continue;
          } else {
            otherId=i+nrFrame;
          }
          dist=distMetric(task,i,otherId);
          if(dist < minDist) {
            minDist=dist;
            minJ=otherId;
            minI=i;
          }
        }
        INFOV("minDist at period=%f is: %f!",period,minDist)
        period+=periodUnit;
      }
      if(minI < 0) {
        if(maxPatch > 2)
          maxPatch--;
        else break;
      } else break;
    }
    ASSERT_MSG(minI >= 0,"Cannot find periodic subsequence!")
    //assign cyclic task
    taskCyclic._xss.assign(task._xss.begin()+minI,task._xss.begin()+minJ);
    taskCyclic._tss.assign(task._tss.begin()+minI,task._tss.begin()+minJ);
    if(!task._infos.empty()) {
      taskCyclic._infos.assign(task._infos.begin()+minI,task._infos.begin()+minJ);
      taskCyclic._dss.assign(task._dss.begin()+minI,task._dss.begin()+minJ);
    }
    calcAvgDist(taskCyclic);
    _taskCyclics.push_back(taskCyclic);
  }
}
FEMDDPSynthesis::Vec FEMDDPSynthesis::applyTask(FEMDDPSolverInfo task,sizeType& frm,sizeType& currFrm,Vec x,const Vec3d* tar,scalarD* minCost,bool more,bool blend)
{
  //check neural-net type
  DMPNeuralNet& net=dynamic_cast<DMPNeuralNet&>(getNeuralNet());
  net._tree.put<sizeType>("augment0",findTaskId(task));
  net.resetOnline();
  //net.setK(findTaskId(task));

  //save state
  sizeType frmSave=frm;
  sizeType currFrmSave=currFrm;

  if(more)
    more=currFrm != 0;
  if(minCost)
    *minCost=ScalarUtil<scalarD>::scalar_max;
  if(_tree.get<bool>("physicalSynthesis",false)) {
    //timestep size
    FEMCIOEngine& eng=_env.sol();
    scalar dtLarge=eng._tree.get<scalar>("dt");
    scalar multiple=(scalar)_tree.get<sizeType>("timestepSubdivision",5);
    INFOV("Using timestep size %f/%f=%f!",dtLarge,multiple,dtLarge/multiple)
    eng._tree.put<bool>("implicitFluidForce",true);
    //simulate
    sizeType nrS=nrX()/2;
    eng._xN.reset(eng.getMCalc(),x.segment(0,nrS));
    eng._x.reset(eng.getMCalc(),x.segment(nrS,nrS));
    while(currFrm < (sizeType)task._xss.size()-1) {
      Vec u=net.foreprop(Vec::Constant(1,task._tss[currFrm])).col(0);
      eng.setControlInput(u);
      for(sizeType ss=0; ss<multiple; ss++)
        eng.advance(dtLarge/multiple);
      x=concat(eng._xN.x(),eng._x.x());
      if(!validFrm(eng._x.x())) {
        frm=frmSave;
        currFrm=currFrmSave;
        return Vec::Zero(0);
      }
      if(tar && minCost)
        *minCost=min<scalarD>(*minCost,dist(x.segment<3>(x.size()/2-6),*tar));
      else {
        assignVec<Vec>(x,"x",frm,_tree);
        assignVec<Vec>(u,"u",frm-1,_tree);
      }
      currFrm++;
      frm++;
      if(currFrm == (sizeType)task._xss.size()-1 && more) {
        more=false;
        currFrm=0;
      }
    }
  } else {
    FEMDDPSolverInfo taskNonBlend=task;
    if(blend && _tree.get<bool>("blendFrame",false))
      task=blendFrame(task,currFrm,getInfo(x));
    //non-physical synthesis, just copy frames
    Mat4d T=AToBR(task._xss[currFrm],getInfo(x));
    while(currFrm < (sizeType)task._xss.size()-1) {
      x=concat(applyTR(T,task._xss[currFrm].x()),applyTR(T,task._xss[currFrm+1].x()));
      if(!validFrm(applyTR(T,task._xss[currFrm+1].x()))) {
        frm=frmSave;
        currFrm=currFrmSave;
        return Vec::Zero(0);
      }
      if(tar && minCost)
        *minCost=min<scalarD>(*minCost,dist(x.segment<3>(x.size()/2-6),*tar));
      else {
        Vec u=net.foreprop(Vec::Constant(1,task._tss[currFrm])).col(0);
        assignVec(x,"x",frm,_tree);
        assignVec<Vec>(u,"u",frm-1,_tree);
      }
      currFrm++;
      frm++;
      if(currFrm == (sizeType)task._xss.size()-1 && more) {
        more=false;
        currFrm=0;
        task=taskNonBlend;
        T=AToBR(task._xss[currFrm],getInfo(x));
      }
    }
  }
  currFrm=0;
  return x;
}
FEMDDPSynthesis::Vec FEMDDPSynthesis::applyTask(const FEMDDPSolverInfo& task,sizeType& frm,sizeType& currFrm,bool more,bool blend)
{
  Vec x=parseVec<Vec>("x",frm-1,_tree);
  return applyTask(task,frm,currFrm,x,NULL,NULL,more,blend);
}
const FEMDDPSolverInfo& FEMDDPSynthesis::getTask(sizeType i) const
{
  return _taskCyclics.empty() ? _tasks[i] : _taskCyclics[i];
}
void FEMDDPSynthesis::calcAvgDist(FEMDDPSolverInfo& task)
{
  Vec lastX,currX;
  FEMBody& body=_env.sol().getBody();
  body._system->setPos(task._xss[0]);
  body.getPos(lastX);
  task._avgDist=0;
  for(sizeType i=1; i<(sizeType)task._xss.size(); i++) {
    body._system->setPos(task._xss[i]);
    body.getPos(currX);
    task._avgDist+=(lastX-currX).norm();
    swap(lastX,currX);
  }
  task._avgDist/=(scalar)(task._xss.size()-1);
}
sizeType FEMDDPSynthesis::nrTask() const
{
  return (sizeType)_tasks.size();
}
void FEMDDPSynthesis::debugAToB()
{
#define DELTA 1E-7f
#define NR_TEST 100
  RigidReducedMCalculator& MCalc=_env.sol().getMCalcNonConst();
  Vec B0;
  _env.sol().getBody().getPos(B0);
  Matd L=concatCol(MCalc._innerMCalc->getL(),B0);
  for(sizeType i=0; i<NR_TEST; i++) {
    GradientInfo A(MCalc,Vec::Random(MCalc.size()));
    GradientInfo B(MCalc,Vec::Random(MCalc.size()));
    Vec a=L*A.L();
    Vec b=L*B.L();
    sizeType nrP=a.size()/3;
    Eigen::Map<Mat3Xd> aP(a.data(),3,nrP);
    Eigen::Map<Mat3Xd> bP(b.data(),3,nrP);
    Vec3d aCtr=aP*Vec::Constant(nrP,1/(scalarD)nrP);
    Vec3d bCtr=bP*Vec::Constant(nrP,1/(scalarD)nrP);
    INFOV("Ctr: %f %f, Err: %f %f",aCtr.norm(),bCtr.norm(),(aCtr-_LCtr*A.L()).norm(),(bCtr-_LCtr*B.L()).norm())
    aP-=aCtr*Vec::Ones(nrP).transpose();
    bP-=bCtr*Vec::Ones(nrP).transpose();
    Mat3d S=aP*bP.transpose();
    INFOV("S: %f, Err: %f",S.norm(),(computeS(A.L(),B.L())-S).norm())
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec x=Vec::Random(MCalc.size()),X,Y;
    //transform
    MCalc.setPos(x);
    _env.sol().getBody().getPos(X);
    //random transform
    Mat4d T=Mat4d::Identity();
    T.block<3,1>(0,3).setRandom();
    T.block<3,3>(0,0)=expWGradV<Mat3d,Mat3d,Mat3d>(Vec3d::Random(),NULL,NULL,NULL,NULL);
    applyT(T,Y=X);
    //compare
    Mat4d T2=AToB(X,Y);
    INFOV("TRigid: %f, Err: %f",T.norm(),(T2-T).norm())
  }
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec A=Vec::Random(MCalc.size())*10,AX;
    Vec B=Vec::Random(MCalc.size())*10,BX;
    //A.segment<6>(A.size()-6).setZero();
    //B.segment<6>(B.size()-6).setZero();
    //transform
    MCalc.setPos(A);
    _env.sol().getBody().getPos(AX);
    MCalc.setPos(B);
    _env.sol().getBody().getPos(BX);
    //compare
    Mat4d T=AToB(AX,BX);
    Mat4d T2=AToBR(A,B);
    INFOV("TNonRigid: %f, Err: %f",T.norm(),(T2-T).norm())
  }
  exit(-1);
#undef DELTA
#undef NR_TEST
}
void FEMDDPSynthesis::solve()
{
  sizeType frm=0;
  string taskStr="taskInit";
  solveInit(frm,_tree.get_child(taskStr,boost::property_tree::ptree()));
  solveGeom();
  for(sizeType i=0;; i++) {
    taskStr="task"+boost::lexical_cast<string>(i);
    boost::optional<boost::property_tree::ptree&>
    child=_tree.get_child_optional(taskStr);
    if(child) {
      child->put<sizeType>("taskId",i);
      string type=child->get<string>("type","unknown");
      if(type == "randomNavigation")
        solveRandomNavigation(frm,*child);
      else if(type == "AStarNavigation")
        solveAStarNavigation(frm,*child);
      else if("tracking")
        solveTracking(frm,*child);
      else {
        ASSERT_MSG(false,"unknown task type!")
      }
    } else break;
  }
  _tree.put<sizeType>("frm",frm-1);
  INFOV("Finishing Synthesis with %ld frames!",frm)
}
//helper
void FEMDDPSynthesis::solveGeom()
{
  _geom=NULL;
  recreate("./AStarTarget");
  boost::optional<boost::property_tree::ptree&>
  child=_tree.get_child_optional("obstacle");
  if(child) {
    _geom.reset(new StaticGeom(_env.sol().getBody().dim()));
    for(sizeType i=0;; i++) {
      boost::optional<boost::property_tree::ptree&>
      object=child->get_child_optional("object"+boost::lexical_cast<string>(i));
      if(!object)
        break;
      else if(object->get<string>("type") == "sphere") {
        Vec3 ctr(object->get<scalar>("ctrX",0),
                 object->get<scalar>("ctrY",0),
                 object->get<scalar>("ctrZ",0));
        scalar rad=object->get<scalar>("rad",1);
        _geom->addGeomSphere(ctr,rad);
      } else if(object->get<string>("type") == "cylinder") {
        Mat4 T=Mat4::Identity();
        T(0,3)=object->get<scalar>("ctrX",0);
        T(1,3)=object->get<scalar>("ctrY",0);
        T(2,3)=object->get<scalar>("ctrZ",0);
        scalar rad=object->get<scalar>("rad",1);
        scalar y=object->get<scalar>("y",100);
        _geom->addGeomCylinder(T,rad,y);
      }
    }
    _geom->assemble();
    _geom->writeVTK("./AStarTarget/env.vtk");
  }

  //radius of bounding sphere
  BBox<scalar> bb=_env.sol().getBody().getBB(true);
  _radius=bb.getExtent().norm()/2;
}
void FEMDDPSynthesis::solveInit(sizeType& frm,const boost::property_tree::ptree& pt)
{
  sizeType taskId=pt.get<sizeType>("taskId",-1);
  if(taskId == -1)
    taskId=RandEngine::randI(0,(sizeType)_tasks.size()-1);
  Vec x=concat(_tasks[taskId]._xss[0].x(),_tasks[taskId]._xss[1].x());
  if(pt.get_optional<scalar>("ctrX") || pt.get_optional<scalar>("ctrY") || pt.get_optional<scalar>("ctrZ")) {
    Vec3d ctr0=x.segment<3>(x.size()/2-6);
    Vec3d ctr(pt.get<scalar>("ctrX",ctr0[0]),
              pt.get<scalar>("ctrY",ctr0[1]),
              pt.get<scalar>("ctrZ",ctr0[2]));
    x.segment<3>(x.size()/2-6)=ctr;
    x.segment<3>(x.size()-6)=ctr;
  }
  if(pt.get_optional<scalar>("rotX") || pt.get_optional<scalar>("rotY") || pt.get_optional<scalar>("rotZ")) {
    Vec3d rot0=x.segment<3>(x.size()/2-3);
    Vec3d rot(pt.get<scalar>("rotX",rot0[0]),
              pt.get<scalar>("rotY",rot0[1]),
              pt.get<scalar>("rotZ",rot0[2]));
    x.segment<3>(x.size()/2-3)=rot;
    x.segment<3>(x.size()-3)=rot;
  }
  assignVec(x,"x",frm,_tree);
  frm++;
}
void FEMDDPSynthesis::solveRandomNavigation(sizeType& frm,const boost::property_tree::ptree& pt)
{
  sizeType nrT=(sizeType)_tasks.size();
  //probability of using task
  scalarD sum=0,nrZero=0;
  vector<scalarD> prob(nrT);
  for(sizeType i=0; i<nrT; i++) {
    prob[i]=pt.get<scalarD>("probabilityTask"+boost::lexical_cast<string>(i),0);
    ASSERT_MSG(prob[i] >= 0,"Detected negative probability!")
    if(prob[i] == 0)
      nrZero+=1;
    sum+=prob[i];
  }
  //renormalize
  if(nrZero > 0) {
    ASSERT(sum <= 1)
    scalarD other=(1-sum)/nrZero;
    for(sizeType i=0; i<nrT; i++)
      if(prob[i] == 0)
        prob[i]=other;
  } else {
    for(sizeType i=0; i<nrT; i++)
      prob[i]/=sum;
  }
  //upper/lower bound of task execution times
  vector<Vec2i,Eigen::aligned_allocator<Vec2i> > length(nrT);
  for(sizeType i=0; i<nrT; i++) {
    length[i][0]=pt.get<sizeType>("lowerBoundTask"+boost::lexical_cast<string>(i),5);
    length[i][1]=pt.get<sizeType>("upperBoundTask"+boost::lexical_cast<string>(i),10);
  }
  //print
  scalarD dt=_env.sol()._tree.get<scalar>("dt");
  sizeType animLength=frm+(sizeType)(pt.get<scalarD>("animLength")/dt);
  INFOV("RandomNavigation animLength=%ld!",animLength)
  for(sizeType i=0; i<(sizeType)prob.size(); i++) {
    INFOV("task%ld: probability=%f, lowerBound=%ld, upperBound=%ld",i,prob[i],length[i][0],length[i][1])
  }
  //synthesis
  sizeType currTask=-1;
  sizeType currLen=-1;
  sizeType currFrm=-1;
  while(frm < animLength) {
    //transit: find task
    currTask=prob.size() > 1 ? findTask(currTask,prob) : 0;
    //transit: find length
    currLen=RandEngine::randI(length[currTask][0],length[currTask][1]);
    INFOV("Transit to task%ld: frame=%ld, length=%ld",currTask,frm,currLen)
    //transit: find frame
    const FEMDDPSolverInfo& task=getTask(currTask);
    currFrm=findFrm(task,parseVec<Vec>("x",frm-1,_tree));
    //gen animation
    for(sizeType l=0; l<currLen; l++) {
      Vec ret=applyTask(task,frm,currFrm,false,l == 0);
      if(ret.size() == 0) //this means this is invalid, usually because we are in solid obstacle
        break;
    }
  }
}
void FEMDDPSynthesis::solveAStarNavigation(sizeType& frm,const boost::property_tree::ptree& pt)
{
  Vec x=parseVec<Vec>("x",frm-1,_tree);
  Vec3d ctr=x.segment<3>(x.size()/2-6);
  BBox<scalar> bb=_env.sol().getBody().getBB(true);
  Vec3d targetPos(pt.get<scalar>("ctrX",ctr[0]),pt.get<scalar>("ctrY",ctr[1]),pt.get<scalar>("ctrZ",ctr[2]));
  scalarD targetRad=pt.get<scalar>("rad",1)*bb.getExtent().norm();
  INFOV("AStar: target=(%f,%f,%f) radius=%f",targetPos[0],targetPos[1],targetPos[2],targetRad)

  //write AStar target
  {
    ObjMesh mesh;
    MakeMesh::makeSphere3D(mesh,targetRad,32);
    mesh.getPos()=targetPos.cast<scalar>();
    mesh.applyTrans();

    create("./AStarTarget");
    sizeType tid=pt.get<sizeType>("taskId");
    boost::filesystem::ofstream os("./AStarTarget/target"+boost::lexical_cast<string>(tid)+".obj");
    boost::filesystem::ofstream osPov("./AStarTarget/target"+boost::lexical_cast<string>(tid)+".pov");
    boost::filesystem::ofstream osPovn("./AStarTarget/target"+boost::lexical_cast<string>(tid)+".povn");
    string osVTK("./AStarTarget/target"+boost::lexical_cast<string>(tid)+".vtk");
    mesh.writeVTK(osVTK,true);
    mesh.writePov(osPovn,true);
    mesh.writePov(osPov);
    mesh.write(os);
  }

  //solve AStar
  sizeType currFrm;
  AStar solver(*this,targetPos,targetRad);
  vector<Vec2i,Eigen::aligned_allocator<Vec2i> > seq=solver.solve(parseVec<Vec>("x",frm-1,_tree),pt);
  for(sizeType i=0; i<(sizeType)seq.size(); i++) {
    sizeType lastSeq=i > 0 ? seq[i-1][0] : -1;
    const FEMDDPSolverInfo& task=getTask(seq[i][0]);
    applyTask(task,frm,currFrm=seq[i][1],true,lastSeq != seq[i][0]);
  }
}
void FEMDDPSynthesis::solveTracking(sizeType& frm,const boost::property_tree::ptree& pt)
{
  //profile physics
  boost::filesystem::ofstream os("./profilePhysics.dat");
  //init solver
  LMInterface lm;
  lm.setTolF(1E-5f);
  lm.setTolG(0.05f);
  //debugTrackingEnergy();
  //read tracked trajectory
  EnergyTrack sol(_env.sol());
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  boost::property_tree::ptree trajectoryPt,groundtruthPt;
  //cb._rollback=false;
  readPtreeAscii(trajectoryPt,pt.get<string>("trajectory"));
  if(pt.get<bool>("profilePhysics",false)) {
    _tree.put<scalar>("regPhysTracking",100);
    _tree.put<scalar>("regU",1E-5f);
    readPtreeAscii(groundtruthPt,pt.get<string>("groundtruth"));
    os << _env.sol().avgElementLength() << endl;
    os << "[discrete,[" << endl;
  }
  sizeType trajFrm=trajectoryPt.get<sizeType>("frm");
  //ensure tracking is the first task
  ASSERT_MSG(frm == 1,"tracking task must be the first task!")
  Vec x=parseVec<Vec>("x",0,trajectoryPt),xCurr,xNext,fvec;
  scalar regPhys=_tree.get<scalar>("regPhysTracking",4);
  scalar regU=_tree.get<scalar>("regU",0.001f);
  assignVec(x,"x",0,_tree);
  sizeType nrS=x.size()/2;
  sol.clearContacts();
  INFOV("Tracking with regPhys=%f regU=%f!",regPhys,regU)
  for(sizeType i=1; i<trajFrm; i++) {
    xCurr=parseVec<Vec>("x",frm-1,_tree);
    xNext=parseVec<Vec>("x",frm,trajectoryPt).segment(nrS,nrS);
    sol.setXCurr(xCurr,regPhys,regU);
    sol.setXNext(xNext);
    //solve x
    if(pt.get<bool>("profilePhysics",false)) {
      Vec xGroundTruth=parseVec<Vec>("x",frm,groundtruthPt),XNEXT,XGT;
      FEMBody& body=_env.sol().getBody();
      //physics violation
      xNext=xGroundTruth.segment(nrS,nrS);
      sol.setXNext(xNext);
      lm.solveDense(xNext,sol,cb);
      body._system->setPos(xNext);
      body.getPos(XNEXT);
      //ground truth
      body._system->setPos(xGroundTruth.segment(nrS,nrS));
      body.getPos(XGT);
      scalarD l=sqrt((XNEXT-XGT).squaredNorm()/(scalarD)body.nrV());
      os << "[" << frm << "," << l/_env.sol().avgElementLength() << "]";
      if(i < trajFrm-1)
        os << ",";
      os << endl;
      //assign x
      assignVec(xGroundTruth,"x",frm,_tree);
      //assign u
      assignVec(sol.getU(),"u",frm-1,_tree);
    } else {
      if(pt.get<bool>("groundTruth",false)) {

      } else {
        lm.solveDense(xNext,sol,cb);
      }
      //assign x
      assignVec(concat(xCurr.segment(nrS,nrS),xNext),"x",frm,_tree);
      //assign u
      assignVec(sol.getU(),"u",frm-1,_tree);
      //debug info
      sol(xNext,fvec,NULL,false);
      INFOV("Tracking frame: %ld/%ld, energy=%f",i,trajFrm,fvec.squaredNorm()/2)
    }
    frm++;
  }
  if(pt.get<bool>("profilePhysics",false)) {
    os << "]]" << endl;
  }
}
scalarD FEMDDPSynthesis::defoMetric(const GradientInfo& x,const GradientInfo& y) const
{
  ASSERT(_defoMetricSqrt.cols() == x.L().size())
  return (_defoMetricSqrt*(x.L()-y.L())).norm();
}
scalarD FEMDDPSynthesis::distMetric(const FEMDDPSolverInfo& info,sizeType x,sizeType y) const
{
  ASSERT(x+1 < (sizeType)info._xss.size())
  ASSERT(y+1 < (sizeType)info._xss.size())
  return defoMetric(info._xss[x],info._xss[y])+defoMetric(info._xss[x+1],info._xss[y+1]);
}
sizeType FEMDDPSynthesis::findFrm(const FEMDDPSolverInfo& task,const Vec& y) const
{
  sizeType minI=-1;
  scalarD minDist=ScalarUtil<scalarD>::scalar_max,nrS=y.size()/2;
  const RigidReducedMCalculator& MCalc=_env.sol().getMCalc();
  const GradientInfo y0(MCalc,y.segment(0,nrS));
  const GradientInfo y1(MCalc,y.segment(nrS,nrS));
  for(sizeType i=0; i<(sizeType)task._xss.size()-1; i++) {
    scalarD dist=defoMetric(y0,task._xss[i])+defoMetric(y1,task._xss[i+1]);
    if(dist < minDist) {
      minDist=dist;
      minI=i;
    }
  }
  if(minI > (sizeType)task._xss.size()-2)
    minI=0;
  return minI;
}
sizeType FEMDDPSynthesis::findTask(sizeType currTask,const vector<scalarD>& prob) const
{
  scalarD curr=0;
  vector<sizeType> index;
  vector<scalarD> accum;
  for(sizeType i=0; i<(sizeType)prob.size(); i++)
    if(currTask != i) {
      index.push_back(i);
      curr+=prob[i];
      accum.push_back(curr);
    }

  ASSERT(!index.empty())
  scalarD R=RandEngine::randR(0,curr);
  for(sizeType i=0; i<(sizeType)index.size(); i++)
    if(R <= accum[i])
      return index[i];
  return index.back();
}
FEMDDPSolverInfo FEMDDPSynthesis::blendFrame(const FEMDDPSolverInfo& task,sizeType currFrm,GradientInfo info)
{
  Mat4d T=AToBR(info,task._xss[currFrm]);

  Vec lastX,currX;
  FEMBody& body=_env.sol().getBody();
  body._system->setPos(applyTR(T,info.x()));
  body.getPos(lastX);
  body._system->setPos(task._xss[currFrm]);
  body.getPos(currX);
  scalarD currDist=(lastX-currX).norm();
  INFOV("Blending frame, avgDist=%f, currDist=%f!",task._avgDist,currDist)

  info=task._xss[currFrm];
  FEMDDPSolverInfo ret=task;
  //determine how many frames we insert
  sizeType nrF=ceil(currDist/task._avgDist)-1;
  //generate frames from optimization
  vector<GradientInfo> xssI;
  vector<scalar> tssI;
  for(sizeType i=0; i<nrF; i++) {
    INFOV("Solving for: %ld/%ld frm!",i+1,nrF+1)
    Vec D=interp1D(lastX,currX,(scalar)(i+1)/(scalar)(nrF+1));
    {
      Vec x=info.x();
      FEMGradientOpt opt(_env.sol().getMCalcNonConst(),body,D);
      opt.solve(x,1E4,1E-10f,1E-10f,false);
      info.reset(_env.sol().getMCalc(),x);
    }
    xssI.push_back(info);
    tssI.push_back(ret._tss[currFrm]);
  }
  //insert frames
  ret._xss.insert(ret._xss.begin()+currFrm,xssI.begin(),xssI.end());
  ret._tss.insert(ret._tss.begin()+currFrm,tssI.begin(),tssI.end());
  return ret;
}
void FEMDDPSynthesis::debugTrackingEnergy()
{
  EnergyTrack sol(_env.sol());
  sol.debugTrackingEnergy();
}
//transform
Mat4d FEMDDPSynthesis::AToBR(Vec a,Vec b) const
{
  return AToBR(getInfo(a),getInfo(b));
}
Mat4d FEMDDPSynthesis::AToBR(const GradientInfo& a,const GradientInfo& b) const
{
  Mat4d defoT=Mat4d::Identity();
  if(_tree.get<bool>("positionOnly",false)) {
    Eigen::Block<Mat4d,3,1> off=defoT.block<3,1>(0,3);
    off=(_LCtr*b.L()+b.x())-(_LCtr*a.L()+a.x());
    return defoT;
  } else if(_tree.get<bool>("positionOnlyWithoutGravity",false)) {
    Eigen::Block<Mat4d,3,1> off=defoT.block<3,1>(0,3);
    off=(_LCtr*b.L()+b.C())-(_LCtr*a.L()+a.C());
    Vec3d g=_env.getGravity().normalized().cast<scalarD>();
    off-=off.dot(g)*g;
    return defoT;
  } else {
    Eigen::JacobiSVD<Mat3d> svd(computeS(a.L(),b.L()),Eigen::ComputeFullU|Eigen::ComputeFullV);
    defoT.block<3,3>(0,0)=svd.matrixV()*svd.matrixU().transpose();
    defoT.block<3,1>(0,3)=_LCtr*b.L()-defoT.block<3,3>(0,0)*(_LCtr*a.L());
    //combine with reduced transformation
    return (computeT(b)*defoT)*computeT(a).inverse();
  }
}
Mat4d FEMDDPSynthesis::computeT(const GradientInfo& x) const
{
  Mat4d ret=Mat4d::Identity();
  ret.block<3,3>(0,0)=x.R();
  ret.block<3,1>(0,3)=x.C();
  return ret;
}
Mat3d FEMDDPSynthesis::computeS(const Vec& A,const Vec& B) const
{
  Mat3d S;
  for(int i=0; i<9; i++)
    S.data()[i]=A.dot(_AToBRTpl[i]*B);
  return S;
}
scalarD FEMDDPSynthesis::dist(const Vec3d& A,const Vec3d& B) const
{
  Vec3d AB=A-B;
  if(_tree.get<bool>("positionOnlyWithoutGravity",false)) {
    Vec3d g=_env.getGravity().normalized().cast<scalarD>();
    AB-=AB.dot(g)*g;
  }
  return AB.norm();
}
bool FEMDDPSynthesis::validFrm(const Vec& x) const
{
  if(!_geom)
    return true;
  Vec3 ctr=x.segment<3>(x.size()-6).cast<scalar>(),n;
  for(sizeType i=0; i<_geom->nrG(); i++) {
    const StaticGeomCell& c=_geom->getG(i);
    bool inside=c.closest(ctr,n);
    if(inside || n.norm() < _radius) {
      //INFO("Invalid frame detected!")
      return false;
    }
  }
  return true;
}
void FEMDDPSynthesis::applyT(const Mat4d& T,Vec& x) const
{
  for(sizeType i=0; i<x.size(); i+=3) {
    x.segment<3>(i)=(T.block<3,3>(0,0)*x.segment<3>(i)).eval();
    x.segment<3>(i)+=T.block<3,1>(0,3);
  }
}
void FEMDDPSynthesis::applyInvT(const Mat4d& T,Vec& x) const
{
  applyT(T.inverse(),x);
}
FEMDDPSynthesis::Vec FEMDDPSynthesis::applyTR(const Mat4d& T,const Vec& x) const
{
  Mat4d TX=Mat4d::Identity();
  TX.block<3,3>(0,0)=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment<3>(x.size()-3),NULL,NULL,NULL,NULL);
  TX.block<3,1>(0,3)=x.segment<3>(x.size()-6);

  Vec ret=x;
  Mat4d TT=T*TX;
  ret.segment<3>(ret.size()-6)=TT.block<3,1>(0,3);
  ret.segment<3>(ret.size()-3)=invExpW(TT.block<3,3>(0,0));
  return ret;
}
GradientInfo FEMDDPSynthesis::getInfo(const Vec& x) const
{
  const RigidReducedMCalculator& MCalc=_env.sol().getMCalc();
  return GradientInfo(MCalc,x.segment(0,MCalc.size()));
}
sizeType FEMDDPSynthesis::findTaskId(const FEMDDPSolverInfo& task) const
{
  for(sizeType i=0; i<(sizeType)_taskCyclics.size(); i++)
    if(&(_taskCyclics[i]) == &task)
      return i;
  return -1;
}
void FEMDDPSynthesis::addContactInfo(FEMDDPSolverInfo& info)
{
  info._infos.clear();
  FEMCIOEngine& eng=_env.sol();
  scalar dt=eng._tree.get<scalar>("dt");
  if(!hasColl())
    return;

  scalarD len=eng.avgElementLength();
  eng._tree.put<scalarD>("collisionExpand",len);
  sizeType nrF=(sizeType)info._xss.size();
  info._infos.resize(nrF);
  info._dss.resize(nrF);
  for(sizeType i=0; i<nrF; i++) {
    eng._c=CIOContactInfo();
    FEMCIOEngine::Contacts& cons=eng._c.getCons();
    eng._xN=eng._x=eng._xP=info._xss[i];
    eng.updateActiveContact(dt);
    eng.clusterPatch();
    info._infos[i]=eng._c;
    //dss
    const vector<vector<sizeType> >& pss=eng._c.getPatches();
    info._dss[i].setConstant((sizeType)pss.size(),ScalarUtil<scalarD>::scalar_max);
    for(sizeType pid=0; pid<(sizeType)pss.size(); pid++) {
      const vector<sizeType>& patch=pss[pid];
      for(sizeType p=0; p<(sizeType)patch.size(); p++) {
        const CollisionConstraint& cc=cons[patch[p]];
        scalarD dist=(cc.pos().cast<scalarD>()-cc.pos0()).dot(cc.n());
        info._dss[i][pid]=min<scalarD>(info._dss[i][pid],abs(dist));
      }
    }
    //info._dss[i].setZero((sizeType)cons.size());
    //for(sizeType c=0; c<(sizeType)cons.size(); c++) {
    //  const CollisionConstraint& cc=cons[c];
    //  info._dss[i][c]=abs((cc.pos()-cc.pos0()).dot(cc.n()));
    //}
    //dss sum
    scalarD maxD=info._dss[i].size() == 0 ? 0 : info._dss[i].maxCoeff();
    INFOV("Detected %ldcontacts/%ldpatches at frame %ld, len=%f!",cons.size(),pss.size(),i,maxD/len)
  }
}
bool FEMDDPSynthesis::hasColl() const
{
  const FEMCIOEngine& eng=_env.sol();
  return eng._geom && eng._geom->nrG() > 0;
}
