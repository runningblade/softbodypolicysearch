#ifndef FEM_ENVIRONMENT_H
#define FEM_ENVIRONMENT_H

#include "Environment.h"
#include <Dynamics/CIOContactInfo.h>

PRJ_BEGIN

class DDPEnergy;
class FEMCIOEngineImplicit;
struct FEMBody;
struct FEMInterp;
struct GradientInfo;

//Environment
class FEMDDPSolverInfo
{
public:
  typedef Environment::Vec Vec;
  //for CIO
  vector<GradientInfo> _xss;
  vector<CIOContactInfo> _infos;
  //for synthesis, this is not included in IO operations
  vector<Vec,Eigen::aligned_allocator<Vec> > _dss;
  vector<scalar> _tss;
  scalar _avgDist;
};
class FEMEnvironment : public Environment
{
public:
  FEMEnvironment();
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void setState(const Vec& state) override;
  virtual void writeStateVTK(const string& path);
  virtual void writeStatePov(const string& path);
  virtual void writeEnvVTK(const string& path) const;
  virtual void addEmbeddedMesh(const string& path);
  virtual void reset(const boost::property_tree::ptree& pt);
  virtual void resetContacts();
  Matd getStiffness() const;
  boost::property_tree::ptree& pt();
  FEMCIOEngineImplicit* solPtr() const;
  const FEMCIOEngineImplicit& sol() const;
  FEMCIOEngineImplicit& sol();
  void writeActionScaleVTK();
  //geometry
  Vec3 getGravity() const;
  Mat4 moveAboveFloor() const;
  bool createDoubleFloor();
  bool createFloor();
  //constraint
  void fixPos();
  void fixRot();
  void fixRotXY();
  void fixRotYZ();
  void fixRotXZ();
  //energy from ptree
  void addEnergyBalance(const boost::property_tree::ptree* pt=NULL);
  void addEnergyJump(const boost::property_tree::ptree* pt=NULL);
  void addEnergyWalk(const boost::property_tree::ptree* pt=NULL);
  void addEnergyWalkTo(const boost::property_tree::ptree* pt=NULL);
  void addEnergyWalkSpeedTo(const boost::property_tree::ptree* pt=NULL);
  void addEnergy2DRoll(const boost::property_tree::ptree* pt=NULL);
  void addEnergy3DRoll(const boost::property_tree::ptree* pt=NULL);
  void addEnergy3DOrientation(const boost::property_tree::ptree* pt=NULL);
  void addEnergy3DOrientationTo(const boost::property_tree::ptree* pt=NULL);
  void addEnergyCurvedWalk(const boost::property_tree::ptree* pt=NULL);
  //energy from parameter
  void addEnergyBalance(const Vec3& dirL,const Vec3& dirG,scalar coef);
  void addEnergyJump(scalar height,scalar frame,scalar coef);
  void addEnergyWalk(const Vec3& spd,scalar coef);
  void addEnergyWalkTo(const Vec3& pos,bool gT,bool gN,scalar frame,scalar coef);
  void addEnergyWalkSpeedTo(const Vec3& pos,bool gT,bool gN,scalar frame,scalar coef);
  void addEnergy2DRoll(scalar spd,scalar coef);
  void addEnergy3DRoll(const Vec3& spd,scalar coef);
  void addEnergy3DOrientation(const Vec3& dirL,const Vec3& rot,scalar coef);
  void addEnergy3DOrientationTo(const Vec3& dirL,scalar frame,scalar coef);
  void addEnergyCurvedWalk(const Vec3& dirL,const Vec3& rot,scalar coef,sizeType mtId=-1);
  template <typename E>
  void deleteEnergy() {
    for(sizeType i=0; i<(sizeType)_ess.size();)
      if(boost::dynamic_pointer_cast<E>(_ess[i]))
        _ess.erase(_ess.begin()+i);
      else i++;
  }
  template <typename E>
  const E* getEnergyType() const {
    for(sizeType i=0; i<(sizeType)_ess.size();)
      if(boost::dynamic_pointer_cast<E>(_ess[i]))
        return boost::dynamic_pointer_cast<E>(_ess[i]).get();
      else i++;
    return NULL;
  }
  const vector<boost::shared_ptr<DDPEnergy> >& getEnergy() const;
  void clearEnergy();
  void debugPeriodic();
  virtual sizeType K() const;
  virtual NeuralNetPolicy& getPolicy();
  virtual bool isTerminal(sizeType i,const Vec& state) const override;
  virtual Vec sampleA(sizeType i,const Vec& S) override;
  virtual Vec transfer(sizeType i,const Vec& A) override;
  virtual scalarD getTrajCost(const Matd& states) const;
  virtual scalarD getCost(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const override;
  virtual Vec getCostF(sizeType i,const Vec& state,const Vec& A,Matd* FJac) const;
  virtual sizeType nrCostF() const;
  virtual Vec sampleS0Rot(Vec3 rot);
  virtual Vec sampleS0();
  //MultiTraj
  virtual void addGPUSupport();
  virtual void removeGPUSupport();
  virtual bool supportMultiTraj() const;
  virtual void startMultiTraj(MultiTrajCallback& cb);
protected:
  FEMEnvironment(const string& name);
  virtual Vec3 getInitRot() const;
  virtual Vec extractState() const;
  boost::shared_ptr<FEMCIOEngineImplicit> _sol;
  vector<boost::shared_ptr<DDPEnergy> > _ess;
};

PRJ_END

#endif
