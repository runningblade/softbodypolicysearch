#ifndef DMP_NEURAL_NET_H
#define DMP_NEURAL_NET_H

#include "NeuralNet.h"

PRJ_BEGIN

class NNBLEICInterface;
class DMPNeuralNet : public NeuralNet
{
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
public:
  DMPNeuralNet();
  DMPNeuralNet(sizeType nrAction,sizeType nrBasis,const boost::property_tree::ptree* pt=NULL);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  //main functionality with exhaustive search
  void train(const UserData* dat=NULL) override;
  void augment(sizeType nrBasis);
  void setupCandidates();
  void trainNonRhythmic();
  void trainRhythmic();
  virtual sizeType K() const;
  //debug
  void debugOutput(const string& path,sizeType nrFrm,scalar dt);
  void debugOutput(ostream& os,sizeType nrFrm,scalar dt);
private:
  void augmentSolution();
  void findBestSolution();
  void setRhythmicBound(NNBLEICInterface& sol);
  Vss _weights;
  vector<scalarD> _fxs,_taus;
  vector<sizeType> _last;
  boost::property_tree::ptree* _log;
};

PRJ_END

#endif
