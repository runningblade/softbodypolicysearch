#include "TrainingData.h"
#include <Dynamics/FEMUtils.h>
#include <MultiTraj/MultiTrajCallback.h>
#include <CMAES/MPIInterface.h>
#include <boost/lexical_cast.hpp>

PRJ_BEGIN

struct MultiTrajWrapper : public MultiTrajCallback {
  MultiTrajWrapper(TrainingData& dat,TrajectorySampler& sampler,const TrainingData::Vec* action0,sizeType maxNrTraj,sizeType maxTrajSz,const string& path)
    :_dat(dat),_sampler(sampler),_action0(action0),_maxNrTraj(maxNrTraj),_maxTrajSz(maxTrajSz),_path(path) {
    _dat._trajs.clear();
  }
  ~MultiTrajWrapper() {
    for(sizeType i=0; i<(sizeType)_dat._trajs.size(); i++) {
      TrajectoryState* s=_dat._trajs[i].get();
      while(s && !s->_actions.empty()) {
        if(s->_actions[0]._next._data.size() == 0) {
          s->_actions.clear();
          break;
        }
        s=&(s->_actions[0]._next);
      }
    }
  }
  Cold onNewState(const Cold& state,sizeType tid,sizeType fid) override {
    ASSERT_MSG(fid <= trajLen(),"MultiTrajError, out-of-bound call to onNewState!")
    _sampler.setTrajId(tid);
    TrajectoryState& s=*(_trajs[tid]);
    if(!s._last) {
      s._data=state;
      s._last=_sampler.isTerminal(fid,state);
      if(!_path.empty()) {
        string path=_path+"/traj"+boost::lexical_cast<string>(tid);
        path+="/frm"+boost::lexical_cast<string>(fid)+".vtk";
        _sampler.setState(state);
        _sampler.writeStateVTK(path);
      }
      if(!s._last) {
        RandEngine::beginRecordRandom(tid);
        s._actions.push_back(TrajectoryAction());
        s._actions[0]._data=_action0 ? *_action0 : _sampler.sampleA(fid,state);
        s._actions[0]._reward=_sampler.getReward(fid,state,s._actions[0]._data);
        s._actions[0]._next._last=false;
        s._actions[0]._next._data.resize(0);
        _trajs[tid]=&(s._actions[0]._next);
        RandEngine::endRecordRandom();
        return s._actions[0]._data;
      }
    }
    return Cold::Zero(0);
  }
  Cold getInitState(sizeType tid) override {
    if(!_path.empty())
      recreate(_path+"/traj"+boost::lexical_cast<string>(tid));
    //add trajectory state
    boost::shared_ptr<TrajectoryState> state(new TrajectoryState);
    _dat._trajs.push_back(state);
    _trajs.push_back(state.get());
    state->_last=false;
    _sampler.setTrajId(tid);
    return _sampler.sampleS0();
  }
  sizeType initialize() override {
    recreate(_path);
    return _maxNrTraj;
  }
  sizeType trajLen() override {
    return _maxTrajSz;
  }
protected:
  TrainingData& _dat;
  TrajectorySampler& _sampler;
  const TrainingData::Vec* _action0;
  vector<TrajectoryState*> _trajs;
  sizeType _maxNrTraj,_maxTrajSz;
  const string _path;
};
void TrainingData::randomSampleInner(sizeType minNrTraj,sizeType maxNrTraj,sizeType minTrajSz,sizeType maxTrajSz,
                                     TrajectorySampler& sampler,const Vec* action0,const string& path,
                                     const sizeType reportInterval,const sizeType offMPI)
{
  //these functionalities are only supported in serial version
  sizeType off=(sizeType)_trajs.size();
  _trajs.resize(off+RandEngine::randI(minNrTraj,maxNrTraj));
  for(sizeType i=off; i<(sizeType)_trajs.size(); i++) {
    _trajs[i].reset(new TrajectoryState);
    sampler.setTrajId(i-off+offMPI);
    _trajs[i]->_data=sampler.sampleS0();
  }
  for(sizeType i=off; i<(sizeType)_trajs.size(); i++) {
    //trajectory output path
    string pathTraj="";
    if(!path.empty())
      pathTraj=path+"/traj"+boost::lexical_cast<string>(i-off+offMPI);
    //sample trajectory
    sampler.setTrajId(i-off+offMPI);
    sizeType nrState=RandEngine::randI(minTrajSz,maxTrajSz);
    RandEngine::beginRecordRandom(i);
    TrajectoryState& s=*(_trajs[i]);
    s._last=sampler.isTerminal(0,s._data);
    s._actions.clear();
    //if initial state is the last one
    if(!s._last) {
      //sample path
      if(!pathTraj.empty())
        recreate(pathTraj);
      sampler.setState(s._data);
      TrajectoryState* last=&s;
      for(sizeType id=0; id<nrState; id++) {
        //frame output path
        if(!pathTraj.empty())
          sampler.writeStateVTK(pathTraj+"/frm"+boost::lexical_cast<string>(id)+".vtk");
        //sample transition
        last->_actions.resize(1);
        TrajectoryAction& action=last->_actions[0];
        action._data=action0 ? *action0 : sampler.sampleA(id,last->_data);
        action._next._data=sampler.transfer(id,action._data,action._reward);
        action._next._last=sampler.isTerminal(id,action._next._data);
        if(action._next._last)
          break;
        last=&(action._next);
      }
    }
    RandEngine::endRecordRandom();
    //report
    if(reportInterval > 0 && (i-off)%reportInterval == 0) {
      INFOV("Sampled %ld/%ld trajectories!",i-off,_trajs.size()-off)
    }
  }
}
void TrainingData::randomSample(sizeType minNrTraj,sizeType maxNrTraj,sizeType minTrajSz,sizeType maxTrajSz,
                                TrajectorySampler& sampler,const Vec* action0,const string& path,
                                const sizeType reportInterval,const sizeType offMPI)
{
  RandEngine::resetAllRecordRandom();
  if(!path.empty())
    recreate(path);
  if(sampler.supportMultiTraj()) {
    MultiTrajWrapper cb(*this,sampler,action0,maxNrTraj,maxTrajSz,path);
    sampler.startMultiTraj(cb);
    return;
  }
#ifdef MPI_SUPPORT
  if(!MPIInterface::getInterface() || MPIInterface::getInterface()->currProc() != 0 || reportInterval > 0) {
    randomSampleInner(minNrTraj,maxNrTraj,minTrajSz,maxTrajSz,
                      sampler,action0,path,reportInterval,offMPI);
  } else {
    //ASSERT_MSG(action0 == NULL && path == "" && reportInterval <= 0,
    //           "DefaultAction/Outputting/Reporting not supported in MPI version!")
    MPIInterface* mpi=MPIInterface::getInterface();
    sizeType nrTraj=RandEngine::randI(minNrTraj,maxNrTraj);
    sizeType nrTrajPerProc=(nrTraj+mpi->nrProc()-1)/mpi->nrProc();
    mpi->signalShutdown(0,false);
    //send remote sampler
    mpi->broadcastObject(sampler,0);
    //send remote sampling command
    string pathNC=path;
    mpi->broadcast(minTrajSz,0);
    mpi->broadcast(maxTrajSz,0);
    mpi->broadcast(nrTrajPerProc,0);
    if(action0) {
      Vec action0NC=*action0;
      mpi->broadcast(action0NC,0);
    } else {
      Vec action0=Vec::Zero(0);
      mpi->broadcast(action0,0);
    }
    mpi->broadcast(pathNC,0);
    //run in master node
    //INFOV("Proc %d: sampling %ld (%ld/%ld)-trajectories",mpi->currProc(),nrTrajPerProc,minTrajSz,maxTrajSz)
    sizeType off=(sizeType)_trajs.size();
    randomSampleInner(nrTrajPerProc,nrTrajPerProc,minTrajSz,maxTrajSz,sampler,action0,path,0,0);
    vector<boost::shared_ptr<TrajectoryState> > trajs=_trajs;
    _trajs.clear();
    //receive from slave nodes
    mpi->gatherDynamicVectorTpl(_trajs,trajs,0);
    //INFOV("Proc 0: Receving %ld trajectories",(sizeType)_trajs.size()-off)
    ASSERT((sizeType)_trajs.size() >= off+nrTraj)
    _trajs.resize(off+nrTraj);
  }
#else
  randomSampleInner(minNrTraj,maxNrTraj,minTrajSz,maxTrajSz,
                    sampler,action0,path,reportInterval,offMPI);
#endif
}
void TrainingData::mpiSlaveRun(TrajectorySampler& sampler)
{
  MPIInterface* mpi=MPIInterface::getInterface();
  if(!mpi)
    return;
  if(mpi->currProc() == 0)
    return;
  //evaluation loop
  //INFOV("Proc %d: waiting for sampling command!",mpi->currProc())
  while(!mpi->signalShutdown(0,false)) {
    //receive remote sampler
    //INFOV("Proc %d: reading sampler",mpi->currProc())
    mpi->broadcastObject(sampler,0);
    //receive remote sampling command
    //INFOV("Proc %d: reading sampling command",mpi->currProc())
    sizeType minTrajSz,maxTrajSz,nrTrajPerProc;
    Vec action0NC;
    string pathNC;
    mpi->broadcast(minTrajSz,0);
    mpi->broadcast(maxTrajSz,0);
    mpi->broadcast(nrTrajPerProc,0);
    mpi->broadcast(action0NC,0);
    mpi->broadcast(pathNC,0);
    //run in slave node
    //INFOV("Proc %d: sampling %ld (%ld/%ld)-trajectories",mpi->currProc(),nrTrajPerProc,minTrajSz,maxTrajSz)
    TrainingData dat;
    dat.randomSampleInner(nrTrajPerProc,nrTrajPerProc,minTrajSz,maxTrajSz,
                          sampler,action0NC.size() > 0 ? &action0NC : NULL,pathNC,
                          0,mpi->currProc()*nrTrajPerProc);
    //send to master node
    //INFOV("Proc %d: sending trajectories",mpi->currProc())
    mpi->gatherDynamicVectorTpl(dat._trajs,dat._trajs,0);
  }
  exit(EXIT_SUCCESS);
}
void TrainingData::mpiExit()
{
  MPIInterface* interface=MPIInterface::getInterface();
  if(interface)
    interface->signalShutdown(0,true);
}

PRJ_END
