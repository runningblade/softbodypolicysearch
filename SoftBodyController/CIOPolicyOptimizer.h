#ifndef CIO_POLICY_OPTIMIZER_H
#define CIO_POLICY_OPTIMIZER_H

#include "CIOOptimizer.h"

PRJ_BEGIN

//enforce exact neural-net
#define POLICY_PARENT CIOOptimizerMP
class CIOPolicyOptimizer : public POLICY_PARENT
{
public:
  typedef POLICY_PARENT ParentType;
  typedef GradientInfo* GradientInfoPtr;
  CIOPolicyOptimizer(FEMDDPSolver& sol,sizeType horizon,FEMDDPSolverInfo& info,sizeType K=-1);
  //update neural network
  virtual void updateWeight() override;
  virtual void updateNeuralNet(const Vec& fvec) override;
  //update force
  virtual void updateForce() override;
protected:
  NeuralNet& _net;
};

PRJ_END

#endif
