#include "CIOPolicyOptimizer.h"
#include "CIOOptimizerPhysicsTerm.h"
#include "CIOOptimizerContactTerm.h"
#include "CIOOptimizerSelfCollisionTerm.h"
#include "CIOOptimizerShuffleAvoidanceTerm.h"
#include "DMPLayer.h"
#include "DMPNeuralNet.h"
#include <Dynamics/FEMUtils.h>
#include <Dynamics/FEMCIOEngine.h>
#include <Dynamics/ArticulatedCIOEngine.h>

PRJ_BEGIN

//enforce exact neural-net
CIOPolicyOptimizer::CIOPolicyOptimizer(FEMDDPSolver& sol,sizeType horizon,FEMDDPSolverInfo& info,sizeType K)
  :ParentType(sol,horizon,info,K),_net(sol.getNeuralNet())
{
  //build terms
  _terms.clear();
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerPolicyPhysicsTerm(*this,K)));
  if(K < 0)
    _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerObjectiveTerm(*this)));
  else _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerObjectiveMTTerm(*this,K)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerLaplacianTerm(*this)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerContactTerm(*this)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerShuffleAvoidanceTerm(*this)));
  _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerConicShuffleAvoidanceTerm(*this)));
  if(dynamic_cast<ArticulatedCIOEngineImplicit*>(_sol.getEnv().solPtr()))
    _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerSelfCollisionTerm(*this)));
  else _terms.push_back(boost::shared_ptr<CIOOptimizerTerm>(new CIOOptimizerSelfCollisionTermPrecomputed(*this)));
  clearInvalidTerms();
}
//update neural network
void CIOPolicyOptimizer::updateWeight()
{
  scalar wPhys=_sol._tree.get<scalar>("regPhysicsCoef",100)*_sol._tree.get<scalar>("regPhysicsCoefUpdate",1);
  scalar wCont=_sol._tree.get<scalar>("regContactActivationCoef",10.0f)*_sol._tree.get<scalar>("regContactActivationCoefUpdate",1);
  _sol._tree.put<scalar>("regPhysicsCoef",wPhys);
  _sol._tree.put<scalar>("regContactActivationCoef",wCont);
  INFOV("Enlarged wPhys=%f wCont=%f",wPhys,wCont)
}
void CIOPolicyOptimizer::updateNeuralNet(const Vec& fvec)
{
  FEMCIOEngine& eng=_sol.getEngine();
  scalar dt=eng._tree.get<scalar>("dt");
  //assemble solution
  sizeType nrS=_sol.nrX()/2,nrU=nrS-6;
  Matd states=Matd::Zero(_net.nrInput(),_horizon);
  Matd actions=Matd::Zero(nrU,_horizon);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<_horizon; i++) {
    actions.col(i)=_DUDE*fvec.segment(i*nrS,nrS);
    states(0,i)=dt*(scalar)i;
  }
  //fillin state, action
  Matd metric=getTerm<CIOOptimizerPolicyPhysicsTerm>()->getCoefPhysADMM();
  metric*=rescaleMetric(metric);
  _net.setData(states,actions,metric);
  //train neural network
  if(eng._tree.get<bool>("inNeuralNetDebug",false))
    _net.NeuralNet::train();
  else _net.train();
  _net.clearData();
}
//update force
void CIOPolicyOptimizer::updateForce()
{
  FEMCIOEngine& eng=_sol.getEngine();
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wCont=_sol._tree.get<scalar>("regContactActivationCoef",1000);
  scalar wContV=_sol._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_sol._tree.get<scalar>("k1",10);
  scalar k2=_sol._tree.get<scalar>("k2",k1);

  Vec vio,E;
  const Matd& metricF=getTerm<CIOOptimizerPolicyPhysicsTerm>()->metricF();
  for(sizeType i=0; i<_horizon; i++) {
    //state
    eng._xN=_info._xss[i+0];
    eng._x =_info._xss[i+1];
    eng._xP=_info._xss[i+2];
    eng._c =_info._infos[i];
    //compute
    applyControl(dt,eng,i);
    eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
    eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,&vio,NULL,NULL,NULL);
    updateF(i,eng,*_qpSol,metricF,dt,wCont,vio,E);
    _info._infos[i]=eng._c;
  }
}

PRJ_END
