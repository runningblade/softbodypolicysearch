#include "FEMFeature.h"
#include <Dynamics/FEMRotationUtil.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//FEMFeature
void FEMFeature::writeVTK(const FeatureTransformLayer::Vec& x,const string& path) const
{
  VTKWriter<scalar> os("feature",path,true);
  writeVTK(x,os);
}

//No feature
NoFeatureTransformLayer::NoFeatureTransformLayer()
  :FeatureTransformLayer(typeid(NoFeatureTransformLayer).name()) {}
NoFeatureTransformLayer::NoFeatureTransformLayer(sizeType nrState)
  :FeatureTransformLayer(typeid(NoFeatureTransformLayer).name()),_nrState(nrState) {}
bool NoFeatureTransformLayer::read(istream& is,IOData* dat)
{
  readBinaryData(_nrState,is);
  return is.good();
}
bool NoFeatureTransformLayer::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_nrState,os);
  return os.good();
}
NoFeatureTransformLayer& NoFeatureTransformLayer::operator=(const NoFeatureTransformLayer& other)
{
  FeatureTransformLayer::operator=(other);
  _nrState=other._nrState;
  return *this;
}
boost::shared_ptr<Serializable> NoFeatureTransformLayer::copy() const
{
  boost::shared_ptr<NoFeatureTransformLayer> ret(new NoFeatureTransformLayer);
  *ret=*this;
  return ret;
}
void NoFeatureTransformLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  fx=x;
}
void NoFeatureTransformLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  dEdx=dEdfx;
}
void NoFeatureTransformLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  dEdfx=dEdx;
}
sizeType NoFeatureTransformLayer::nrF() const
{
  return _nrState;
}
sizeType NoFeatureTransformLayer::nrInput() const
{
  return _nrState;
}

//No feature
ConstFeatureTransformLayer::ConstFeatureTransformLayer()
  :FeatureTransformLayer(typeid(ConstFeatureTransformLayer).name()) {}
ConstFeatureTransformLayer::ConstFeatureTransformLayer(sizeType nrState)
  :FeatureTransformLayer(typeid(ConstFeatureTransformLayer).name()),_aug(Vec::Zero(nrState)) {}
bool ConstFeatureTransformLayer::read(istream& is,IOData* dat)
{
  readBinaryData(_aug,is);
  return is.good();
}
bool ConstFeatureTransformLayer::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_aug,os);
  return os.good();
}
ConstFeatureTransformLayer& ConstFeatureTransformLayer::operator=(const ConstFeatureTransformLayer& other)
{
  FeatureTransformLayer::operator=(other);
  _aug=other._aug;
  return *this;
}
boost::shared_ptr<Serializable> ConstFeatureTransformLayer::copy() const
{
  boost::shared_ptr<ConstFeatureTransformLayer> ret(new ConstFeatureTransformLayer);
  *ret=*this;
  return ret;
}
void ConstFeatureTransformLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  ASSERT(x.rows() == 0)
  fx=_aug*Vec::Ones(x.cols()).transpose();
}
void ConstFeatureTransformLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  ASSERT(x.rows() == 0)
  dEdx.setZero(0,x.cols());
}
void ConstFeatureTransformLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  dEdfx.setZero(_aug.size(),x.cols());
}
sizeType ConstFeatureTransformLayer::nrF() const
{
  return _aug.size();
}
sizeType ConstFeatureTransformLayer::nrInput() const
{
  return 0;
}
void ConstFeatureTransformLayer::configOnline(const boost::property_tree::ptree& pt)
{
  for(sizeType i=0; i<(sizeType)_aug.size(); i++)
    _aug[i]=pt.get<scalar>("augment"+boost::lexical_cast<string>(i));
}

//FEM feature transform
FEMRSFeatureTransformLayer::FEMRSFeatureTransformLayer()
  :FeatureTransformLayer(typeid(FEMRSFeatureTransformLayer).name()) {}
FEMRSFeatureTransformLayer::FEMRSFeatureTransformLayer(sizeType nrState,scalarD dt,sizeType dim,bool posOnly,bool defOnly)
  :FeatureTransformLayer(typeid(FEMRSFeatureTransformLayer).name())
{
  sizeType nrP=nrState/2;
  addFeature(nrState,dim,nrP,0,1,false,defOnly);
  sizeType to=_to.back()[1];
  if(!posOnly) {
    addFeature(nrState,dim,0  ,to,-1/dt,true,defOnly);
    addFeature(nrState,dim,nrP,to, 1/dt,true,defOnly);
  }
  writeInfo(nrState);

  sizeType nrFeature=_to.back()[1];
  _deriv=Matd::Zero(nrFeature,nrState);
  for(sizeType i=0; i<(sizeType)_from.size(); i++) {
    sizeType szR=_to[i][1]-_to[i][0];
    sizeType szC=_from[i][1]-_from[i][0];
    _deriv.block(_to[i][0],_from[i][0],szR,szC)=Matd::Identity(szR,szC)*_coef[i];
    ASSERT(szR == szC)
  }
}
bool FEMRSFeatureTransformLayer::read(istream& is,IOData* dat)
{
  FeatureTransformLayer::read(is,dat);
  readVector(_from,is);
  readVector(_to,is);
  readVector(_coef,is);
  readVector(_deriv,is);
  return is.good();
}
bool FEMRSFeatureTransformLayer::write(ostream& os,IOData* dat) const
{
  FeatureTransformLayer::write(os,dat);
  writeVector(_from,os);
  writeVector(_to,os);
  writeVector(_coef,os);
  writeVector(_deriv,os);
  return os.good();
}
void FEMRSFeatureTransformLayer::writeVTK(const Vec& x,VTKWriter<scalar>& os) const {}
FEMRSFeatureTransformLayer& FEMRSFeatureTransformLayer::operator=(const FEMRSFeatureTransformLayer& other)
{
  FeatureTransformLayer::operator=(other);
  _from=other._from;
  _to=other._to;
  _coef=other._coef;
  _deriv=other._deriv;
  return *this;
}
boost::shared_ptr<Serializable> FEMRSFeatureTransformLayer::copy() const
{
  boost::shared_ptr<FEMRSFeatureTransformLayer> ret(new FEMRSFeatureTransformLayer);
  *ret=*this;
  return ret;
}
void FEMRSFeatureTransformLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,_deriv.rows(),x.cols());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<x.cols(); i++)
    fx.col(i)=_deriv*x.col(i);
}
void FEMRSFeatureTransformLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,_deriv.cols(),dEdfx.cols());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<dEdfx.cols(); i++)
    dEdx.col(i)=_deriv.transpose()*dEdfx.col(i);
}
void FEMRSFeatureTransformLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  foreprop(dEdx,dEdfx);
}
sizeType FEMRSFeatureTransformLayer::nrF() const
{
  return _deriv.rows();
}
sizeType FEMRSFeatureTransformLayer::nrInput() const
{
  return _deriv.cols();
}
//helper
void FEMRSFeatureTransformLayer::addFeature(sizeType nrState,sizeType dim,sizeType offF,sizeType offT,scalarD coef,bool isVel,bool defOnly)
{
  sizeType nrP=nrState/2,nrU=nrP-6;
#define SCALARV(A) RANGEV(A,A+1)
#define RANGEV(A,B) { \
  _from.push_back(Vec2i(offF+(A),offF+(B)));  \
  _to.push_back(Vec2i(offT,offT+(B)-(A))); \
  _coef.push_back(coef);  \
  offT+=(B)-(A);  \
}
  RANGEV(0,nrU)
  if(defOnly)
    return;
  if(isVel) {
    SCALARV(nrU+0)
  }
  SCALARV(nrU+1)
  if(dim == 3) {
    if(isVel) {
      SCALARV(nrU+2)
    }
    SCALARV(nrU+3)
    SCALARV(nrU+4)
  }
  SCALARV(nrU+5)
#undef RANGEV
#undef SCALARV
}
FEMRSFeatureTransformLayer::FEMRSFeatureTransformLayer(const string& name):FeatureTransformLayer(name) {}
void FEMRSFeatureTransformLayer::writeInfo(sizeType nrState) const
{
  sizeType nrFeature=_to.back()[1];
  ASSERT(_from.size() == _to.size() && _from.size() == _coef.size())
  INFOV("FEMRSFeatureTransformLayer (%ld features, %ld states)",nrFeature,nrState)
  for(sizeType i=0; i<nrFeature; i++) {
    cout << "Feature" << i << "=";
    bool first=true;
    for(sizeType j=0; j<(sizeType)_from.size(); j++)
      if(_to[j][0] <= i && i < _to[j][1]) {
        if(!first)
          cout << "+";
        cout << varName(i-_to[j][0]+_from[j][0],nrState) << "*(" << _coef[j] << ")";
        first=false;
      }
    cout << endl;
  }
}
string FEMRSFeatureTransformLayer::varName(sizeType i,sizeType nrState) const
{
  ASSERT(0 <= i && i < nrState)
  sizeType nrSH=nrState/2,nrSHD=nrSH-6;
  string level=i<nrSH ? "Last" : "Curr";
  i%=nrSH;

  vector<string> AXIS(3);
  AXIS[0]="X";
  AXIS[1]="Y";
  AXIS[2]="Z";

  if(i < nrSHD)
    return "RSCoord"+boost::lexical_cast<string>(i)+level;
  else if(i < nrSHD+3)
    return "C"+AXIS.at(i-nrSHD)+level;
  else return "R"+AXIS.at(i-nrSHD-3)+level;
}

//FEM rot feature transform
FEMRotRSFeatureTransformLayer::FEMRotRSFeatureTransformLayer()
  :FeatureTransformLayer(typeid(FEMRotRSFeatureTransformLayer).name()) {}
FEMRotRSFeatureTransformLayer::FEMRotRSFeatureTransformLayer(sizeType nrState,scalarD dt,bool posOnly,bool hasDefo,bool hasCOM)
  :FeatureTransformLayer(typeid(FEMRotRSFeatureTransformLayer).name()),_dt(dt),_nrState(nrState),_posOnly(posOnly)
{
  _nrPFeat=0;
  if(hasDefo)
    _nrPFeat+=_nrState/2-6;
  if(hasDefo && hasCOM)
    _nrPFeat+=3;
}
bool FEMRotRSFeatureTransformLayer::read(istream& is,IOData* dat)
{
  FeatureTransformLayer::read(is,dat);
  readBinaryData(_dt,is);
  readBinaryData(_nrState,is);
  readBinaryData(_nrPFeat,is);
  readBinaryData(_posOnly,is);
  return is.good();
}
bool FEMRotRSFeatureTransformLayer::write(ostream& os,IOData* dat) const
{
  FeatureTransformLayer::write(os,dat);
  writeBinaryData(_dt,os);
  writeBinaryData(_nrState,os);
  writeBinaryData(_nrPFeat,os);
  writeBinaryData(_posOnly,os);
  return os.good();
}
void FEMRotRSFeatureTransformLayer::writeVTK(const Vec& x,VTKWriter<scalar>& os) const
{
  Vec feat=compute(x,NULL);
  Vec3 c=x.segment<3>(offC()).cast<scalar>();
  Mat3 R=Eigen::Map<Mat3d>(feat.data()+feat.size()-9).cast<scalar>();
  //vss
  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  vss.push_back(c);
  vss.push_back(c+R.col(0));
  vss.push_back(c+R.col(1));
  vss.push_back(c+R.col(2));
  os.appendPoints(vss.begin(),vss.end());
  //iss
  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
  iss.push_back(Vec3i(0,1,-1));
  iss.push_back(Vec3i(0,2,-1));
  iss.push_back(Vec3i(0,3,-1));
  os.appendCells(iss.begin(),iss.end(),VTKWriter<scalar>::LINE);
  //css
  vector<scalar> css;
  css.push_back(0);
  css.push_back(1);
  css.push_back(2);
  os.appendCustomData("direction",css.begin(),css.end());
}
FEMRotRSFeatureTransformLayer& FEMRotRSFeatureTransformLayer::operator=(const FEMRotRSFeatureTransformLayer& other)
{
  FeatureTransformLayer::operator=(other);
  _dt=other._dt;
  _nrState=other._nrState;
  return *this;
}
boost::shared_ptr<Serializable> FEMRotRSFeatureTransformLayer::copy() const
{
  boost::shared_ptr<Serializable> ret(new FEMRotRSFeatureTransformLayer);
  *ret=*this;
  return ret;
}
void FEMRotRSFeatureTransformLayer::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  enforceSize(fx,nrF(),x.cols());
  Matd deriv;
  OMP_PARALLEL_FOR_I(OMP_PRI(deriv))
  for(sizeType i=0; i<x.cols(); i++)
    fx.col(i)=compute(x.col(i),NULL);
}
void FEMRotRSFeatureTransformLayer::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  enforceSize(dEdx,x);
  Matd deriv;
  OMP_PARALLEL_FOR_I(OMP_PRI(deriv))
  for(sizeType i=0; i<x.cols(); i++) {
    compute(x.col(i),&deriv);
    dEdx.col(i)=deriv.transpose()*dEdfx.col(i);
  }
}
void FEMRotRSFeatureTransformLayer::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  Matd deriv;
  enforceSize(dEdfx,nrF(),x.cols());
  OMP_PARALLEL_FOR_I(OMP_PRI(deriv))
  for(sizeType i=0; i<x.cols(); i++) {
    compute(x.col(i),&deriv);
    dEdfx.col(i)=deriv*dEdx.col(i);
  }
}
sizeType FEMRotRSFeatureTransformLayer::offC() const
{
  return _nrState-6;
}
sizeType FEMRotRSFeatureTransformLayer::offW() const
{
  return _nrState-3;
}
sizeType FEMRotRSFeatureTransformLayer::nrF() const
{
  if(_posOnly)
    return _nrPFeat+9;
  else return _nrPFeat*2+9;
}
sizeType FEMRotRSFeatureTransformLayer::nrInput() const
{
  return _nrState;
}
//helper
FEMRotRSFeatureTransformLayer::FEMRotRSFeatureTransformLayer(const string& name):FeatureTransformLayer(name) {}
FEMRotRSFeatureTransformLayer::Vec FEMRotRSFeatureTransformLayer::compute(const Vec& x,Matd* deriv) const
{
  sizeType offX=_nrState/2;
  sizeType nrFeature=nrF();
  Vec ret=Vec::Zero(nrFeature);
  //position
  ret.segment(0,_nrPFeat)=x.segment(offX,_nrPFeat);
  if(!_posOnly)
    ret.segment(_nrPFeat,_nrPFeat)=(x.segment(offX,_nrPFeat)-x.segment(0,_nrPFeat))/_dt;
  //rotation
  Mat3d DRDW[3],R=expWGradV<Mat3d,Mat3d,Mat3d>(x.segment<3>(offW()),deriv ? DRDW : NULL,NULL,NULL,NULL);
  ret.segment<9>(nrFeature-9)=Eigen::Map<Vec9d>(R.data());
  //deriv
  if(deriv) {
    deriv->setZero(nrFeature,_nrState);
    deriv->block(0,offX,_nrPFeat,_nrPFeat).diagonal().setOnes();
    if(!_posOnly) {
      deriv->block(_nrPFeat,0,_nrPFeat,_nrPFeat).diagonal().setConstant(-1/_dt);
      deriv->block(_nrPFeat,offX,_nrPFeat,_nrPFeat).diagonal().setConstant(1/_dt);
    }
    for(sizeType i=0; i<3; i++)
      deriv->block<9,1>(nrFeature-9,offW()+i)=Eigen::Map<Vec9d>(DRDW[i].data());
  }
  return ret;
}

//FEM feature augment in-place
FeatureAugmentedInPlace::FeatureAugmentedInPlace()
  :FeatureTransformLayer(typeid(FeatureAugmentedInPlace).name()) {}
FeatureAugmentedInPlace::FeatureAugmentedInPlace
(boost::shared_ptr<FeatureTransformLayer> feat1,
 boost::shared_ptr<FeatureTransformLayer> feat2)
  :FeatureTransformLayer(typeid(FeatureAugmentedInPlace).name())
{
  _feats.push_back(feat1);
  _feats.push_back(feat2);
}
FeatureAugmentedInPlace::FeatureAugmentedInPlace
(boost::shared_ptr<FeatureTransformLayer> feat1,
 boost::shared_ptr<FeatureTransformLayer> feat2,
 boost::shared_ptr<FeatureTransformLayer> feat3)
  :FeatureTransformLayer(typeid(FeatureAugmentedInPlace).name())
{
  _feats.push_back(feat1);
  _feats.push_back(feat2);
  _feats.push_back(feat3);
}
FeatureAugmentedInPlace::FeatureAugmentedInPlace
(boost::shared_ptr<FeatureTransformLayer> feat1,
 boost::shared_ptr<FeatureTransformLayer> feat2,
 boost::shared_ptr<FeatureTransformLayer> feat3,
 boost::shared_ptr<FeatureTransformLayer> feat4)
  :FeatureTransformLayer(typeid(FeatureAugmentedInPlace).name())
{
  _feats.push_back(feat1);
  _feats.push_back(feat2);
  _feats.push_back(feat3);
  _feats.push_back(feat4);
}
FeatureAugmentedInPlace::FeatureAugmentedInPlace
(boost::shared_ptr<FeatureTransformLayer> feat1,
 boost::shared_ptr<FeatureTransformLayer> feat2,
 boost::shared_ptr<FeatureTransformLayer> feat3,
 boost::shared_ptr<FeatureTransformLayer> feat4,
 boost::shared_ptr<FeatureTransformLayer> feat5)
  :FeatureTransformLayer(typeid(FeatureAugmentedInPlace).name())
{
  _feats.push_back(feat1);
  _feats.push_back(feat2);
  _feats.push_back(feat3);
  _feats.push_back(feat4);
  _feats.push_back(feat5);
}
FeatureAugmentedInPlace::FeatureAugmentedInPlace
(boost::shared_ptr<FeatureTransformLayer> feat1,
 boost::shared_ptr<FeatureTransformLayer> feat2,
 boost::shared_ptr<FeatureTransformLayer> feat3,
 boost::shared_ptr<FeatureTransformLayer> feat4,
 boost::shared_ptr<FeatureTransformLayer> feat5,
 boost::shared_ptr<FeatureTransformLayer> feat6)
  :FeatureTransformLayer(typeid(FeatureAugmentedInPlace).name())
{
  _feats.push_back(feat1);
  _feats.push_back(feat2);
  _feats.push_back(feat3);
  _feats.push_back(feat4);
  _feats.push_back(feat5);
  _feats.push_back(feat6);
}
FeatureAugmentedInPlace::FeatureAugmentedInPlace
(vector<boost::shared_ptr<FeatureTransformLayer> > feats)
  :FeatureTransformLayer(typeid(FeatureAugmentedInPlace).name()),_feats(feats) {}
bool FeatureAugmentedInPlace::read(istream& is,IOData* dat)
{
  readVector(_feats,is,dat);
  return is.good();
}
bool FeatureAugmentedInPlace::write(ostream& os,IOData* dat) const
{
  writeVector(_feats,os,dat);
  return os.good();
}
void FeatureAugmentedInPlace::writeVTK(const Vec& x,VTKWriter<scalar>& os) const
{
  sizeType off=0;
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    boost::shared_ptr<FEMFeature> FEMFeat=boost::dynamic_pointer_cast<FEMFeature>(_feats[i]);
    if(FEMFeat)
      FEMFeat->writeVTK(x.segment(off,_feats[i]->nrInput()),os);
    off+=_feats[i]->nrInput();
  }
}
FeatureAugmentedInPlace& FeatureAugmentedInPlace::operator=(const FeatureAugmentedInPlace& other)
{
  FeatureTransformLayer::operator=(other);
  _feats.resize(other._feats.size());
  for(sizeType i=0; i<(sizeType)_feats.size(); i++)
    _feats[i]=boost::dynamic_pointer_cast<FeatureTransformLayer>(other._feats[i]->copy());
  return *this;
}
boost::shared_ptr<Serializable> FeatureAugmentedInPlace::copy() const
{
  boost::shared_ptr<Serializable> ret(new FeatureAugmentedInPlace);
  *ret=*this;
  return ret;
}
void FeatureAugmentedInPlace::foreprop(const Matd& x,Matd& fx,void* dat) const
{
  //foreprop
  sizeType nrInput=0,nrOutput=0;
  Mss tmp(_feats.size());
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    _feats[i]->foreprop(x.block(nrInput,0,_feats[i]->nrInput(),x.cols()),tmp[i],dat);
    nrInput+=_feats[i]->nrInput();
    nrOutput+=_feats[i]->nrF();
  }
  //assemble
  enforceSize(fx,nrOutput,x.cols());
  nrOutput=0;
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    fx.block(nrOutput,0,tmp[i].rows(),tmp[i].cols())=tmp[i];
    nrOutput+=tmp[i].rows();
  }
}
void FeatureAugmentedInPlace::backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat)
{
  //backprop
  sizeType nrInput=0,nrOutput=0;
  Mss tmp(_feats.size());
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    _feats[i]->backprop(x.block(nrInput,0,_feats[i]->nrInput(),x.cols()),
                        fx.block(nrOutput,0,_feats[i]->nrF(),x.cols()),
                        tmp[i],dEdfx.block(nrOutput,0,_feats[i]->nrF(),x.cols()),dat);
    nrInput+=_feats[i]->nrInput();
    nrOutput+=_feats[i]->nrF();
  }
  //assemble
  enforceSize(dEdx,nrInput,dEdfx.cols());
  nrInput=0;
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    dEdx.block(nrInput,0,tmp[i].rows(),tmp[i].cols())=tmp[i];
    nrInput+=tmp[i].rows();
  }
}
void FeatureAugmentedInPlace::weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat) const
{
  //foreprop
  sizeType nrInput=0,nrOutput=0;
  Mss tmp(_feats.size());
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    _feats[i]->weightprop(x.block(nrInput,0,_feats[i]->nrInput(),x.cols()),
                          fx.block(nrOutput,0,_feats[i]->nrF(),x.cols()),
                          dEdx.block(nrInput,0,_feats[i]->nrInput(),x.cols()),tmp[i],dat);
    nrInput+=_feats[i]->nrInput();
    nrOutput+=_feats[i]->nrF();
  }
  //assemble
  enforceSize(dEdfx,nrOutput,x.cols());
  nrOutput=0;
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    dEdfx.block(nrOutput,0,tmp[i].rows(),tmp[i].cols())=tmp[i];
    nrOutput+=tmp[i].rows();
  }
}
sizeType FeatureAugmentedInPlace::nrF() const
{
  sizeType ret=0;
  for(sizeType i=0; i<(sizeType)_feats.size(); i++)
    ret+=_feats[i]->nrF();
  return ret;
}
sizeType FeatureAugmentedInPlace::nrInput() const
{
  sizeType ret=0;
  for(sizeType i=0; i<(sizeType)_feats.size(); i++)
    ret+=_feats[i]->nrInput();
  return ret;
}
void FeatureAugmentedInPlace::configOffline(const boost::property_tree::ptree& pt)
{
  for(sizeType i=0; i<(sizeType)_feats.size(); i++)
    _feats[i]->configOffline(pt);
}
void FeatureAugmentedInPlace::configOnline(const boost::property_tree::ptree& pt)
{
  for(sizeType i=0; i<(sizeType)_feats.size(); i++)
    _feats[i]->configOnline(pt);
}
void FeatureAugmentedInPlace::setParam(const Vec& p,sizeType off,bool diff)
{
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    _feats[i]->setParam(p,off,diff);
    off+=_feats[i]->nrParam();
  }
}
void FeatureAugmentedInPlace::param(Vec& p,sizeType off,bool diff) const
{
  for(sizeType i=0; i<(sizeType)_feats.size(); i++) {
    _feats[i]->param(p,off,diff);
    off+=_feats[i]->nrParam();
  }
}
sizeType FeatureAugmentedInPlace::nrParam() const
{
  sizeType ret=0;
  for(sizeType i=0; i<(sizeType)_feats.size(); i++)
    ret+=_feats[i]->nrParam();
  return ret;
}
