#include "CIOOptimizerPhysicsTerm.h"
#include "DMPNeuralNet.h"
#include <CMAES/MLUtil.h>
#include <Dynamics/FEMUtils.h>
#include <Dynamics/FEMCIOEngine.h>

USE_PRJ_NAMESPACE

//Physics Term
CIOOptimizerPhysicsTerm::CIOOptimizerPhysicsTerm(CIOOptimizer& opt):CIOOptimizerTerm(opt)
{
  scalar wPhys=_opt._sol._tree.get<scalar>("regPhysicsCoef",100);

  _metricF=_opt._metricSqr*wPhys;
  _metricFSqrt=matrixSqrt(_metricF);
}
void CIOOptimizerPhysicsTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wCont=_opt._sol._tree.get<scalar>("regContactActivationCoef",1000);
  scalar wContV=_opt._sol._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_opt._sol._tree.get<scalar>("k1",10);
  scalar k2=_opt._sol._tree.get<scalar>("k2",k1);

  Vec E,vio;
  Matd DEDXN,DEDX,DEDXP;
  sizeType nrS=_opt._sol.nrX()/2;

  //update force
  if(modifiable && wCont > 0) {
    eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
    eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,&vio,NULL,NULL,NULL);
    _opt.updateF(i,eng,qp,_metricF,dt,wCont,vio,E);
  }
  //compute physics
  if(fjac)
    eng.computePhysicsDerivativeCIO(dt,E,&DEDXN,&DEDX,&DEDXP,false);
  else eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
  fvec.segment(off+i*nrS,nrS)=_metricFSqrt*E;
  //assign gradient
  if(fjac) {
    //XN
    _opt.addBlockXN(*fjac,off+i*nrS,i,_metricFSqrt*DEDXN);
    //X
    _opt.addBlockX(*fjac,off+i*nrS,i,_metricFSqrt*DEDX);
    //XP
    _opt.addBlockXP(*fjac,off+i*nrS,i,_metricFSqrt*DEDXP);
  }
}
void CIOOptimizerPhysicsTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  Vec DPDXN,DPDX,DPDXP,DPDC;
  sizeType dim=_opt._sol.getEngine().getBody().dim();
  scalar dt=eng._tree.get<scalar>("dt");

  //compute physics
  FX+=eng.computePhysicsViolationCIO(dt,_metricF,&DPDXN,&DPDX,&DPDXP,&DPDC);
  //assign gradient
  //XN
  _opt.addBlockXN(DFDX,i,DPDXN);
  //X
  _opt.addBlockX (DFDX,i,DPDX);
  //XP
  _opt.addBlockXP(DFDX,i,DPDXP);
  //C
  if(_cTerm) {
    const Coli& cOff=_cTerm->getCollisionOff();
    DFDX.segment(_opt.inputs()+cOff[i]*dim,(cOff[i+1]-cOff[i])*dim)+=DPDC;
  }
}
const Matd& CIOOptimizerPhysicsTerm::metricF() const
{
  return _metricF;
}
void CIOOptimizerPhysicsTerm::randomizeParam() {}
void CIOOptimizerPhysicsTerm::balanceResidual() {}
int CIOOptimizerPhysicsTerm::values() const
{
  return _opt._horizon*_opt._sol.nrX()/2;
}
bool CIOOptimizerPhysicsTerm::valid() const
{
  scalar wPhys=_opt._sol._tree.get<scalar>("regPhysicsCoef",100);
  return wPhys > 0;
}
//Policy Physics Term
CIOOptimizerPolicyPhysicsTerm::CIOOptimizerPolicyPhysicsTerm(CIOOptimizer& opt,sizeType K)
  :CIOOptimizerPhysicsTerm(opt),_K(K)
{
  scalar wPhys=_opt._sol._tree.get<scalar>("regPhysicsCoef",100);
  if(K < 0)
    _prefix="regPhysicsADMMCoef";
  else _prefix="task"+boost::lexical_cast<string>(K)+".regPhysicsADMMCoef";
  _coefPhys=_coefPhysADMM=1;
  _forNNUpdate=false;
  recomputeMetric();

  //initialize collected information
  _infos.resize(_opt._horizon);
  sizeType nrS=_opt._sol.nrX()/2,nrU=nrS-6;
  for(sizeType i=0; i<_opt._horizon; i++) {
    _infos[i]._DUDEE.setOnes(nrU);
    _infos[i]._CNT.setOnes(nrU);
  }

  //control matrix
  //const FEMCIOEngine& eng=_opt._sol.getEngine();
  //const Matd& C=eng.getControlMatrix();
  //scalar dt=eng._tree.get<scalar>("dt");
  //_C=-concatRow(C,Matd::Zero(6,C.cols()))*dt*dt;

  //debug metric
  Vec E=Vec::Random(_opt._sol.nrX()/2)*wPhys;
  Vec L=Vec::Random(E.size()-6);
  scalarD E1=E.dot(_opt._metricSqr*E)*wPhys/2;
  scalarD E2=(_opt._DUDE*E+L).dot(getCoefPhysADMM()*(_opt._DUDE*E+L))/2;
  scalarD E3=(_metricFSqrt*(E+_lambdaCoef*L)).squaredNorm()/2;
  INFOV("Debug Metric: E1=%f E2=%f E3=%f Err=%f",E1,E2,E3,E1+E2-E3)
}
void CIOOptimizerPolicyPhysicsTerm::runLM(sizeType off,FEMCIOEngine& eng,QPInterface& qp,sizeType i,Vec& fvec,STrips* fjac,bool modifiable) const
{
  scalar dt=eng._tree.get<scalar>("dt");
  scalar wCont=_opt._sol._tree.get<scalar>("regContactActivationCoef",1000);
  scalar wContV=_opt._sol._tree.get<scalar>("regContactVelocityCoef",dt);
  scalar k1=_opt._sol._tree.get<scalar>("k1",10);
  scalar k2=_opt._sol._tree.get<scalar>("k2",k1);

  Vec E,vio;
  Matd DEDXN,DEDX,DEDXP;
  sizeType nrS=_opt._sol.nrX()/2;

  //neural net
  setTask();
  _opt.applyControl(dt,eng,i);
  //update force
  if(modifiable && wCont > 0) {
    eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
    eng.computeContactViolationCIO(wContV,k1,k2,NULL,NULL,NULL,&vio,NULL,NULL,NULL);
    _opt.updateF(i,eng,qp,_metricF,dt,wCont,vio,E-_lambdaCoef*eng.getControlInput());
  }
  //compute physics
  if(fjac)
    eng.computePhysicsDerivativeCIO(dt,E,&DEDXN,&DEDX,&DEDXP,false);
  else eng.computePhysicsDerivativeCIO(dt,E,NULL,NULL,NULL,false);
  fvec.segment(off+i*nrS,nrS)=_forNNUpdate ? E : _metricFSqrt*(E-_lambdaCoef*eng.getControlInput());
  //collect information for residual balancing
  FrameInfo& info=const_cast<FrameInfo&>(_infos[i]);
  info._DUDEE=_opt._DUDE*E;
  info._CNT=eng.getControlInput();
  //assign gradient
  if(fjac) {
    //XN
    _opt.addBlockXN(*fjac,off+i*nrS,i,_metricFSqrt*DEDXN);
    //X
    _opt.addBlockX(*fjac,off+i*nrS,i,_metricFSqrt*DEDX);
    //XP
    _opt.addBlockXP(*fjac,off+i*nrS,i,_metricFSqrt*DEDXP);
  }
}
void CIOOptimizerPolicyPhysicsTerm::runLBFGS(FEMCIOEngine& eng,sizeType i,scalarD& FX,Eigen::Map<Vec>& DFDX) const
{
  Vec DPDXN,DPDX,DPDXP,DPDC;
  sizeType dim=_opt._sol.getEngine().getBody().dim();
  scalar dt=eng._tree.get<scalar>("dt");

  //neural net
  setTask();
  _opt.applyControl(dt,eng,i);
  //compute physics
  Vec LADMM=-_lambdaCoef*eng.getControlInput();
  FX+=eng.computePhysicsViolationCIO(dt,_metricF,&DPDXN,&DPDX,&DPDXP,&DPDC,&LADMM,false);
  //assign gradient
  //XN
  _opt.addBlockXN(DFDX,i,DPDXN);
  //X
  _opt.addBlockX (DFDX,i,DPDX);
  //XP
  _opt.addBlockXP(DFDX,i,DPDXP);
  //C
  if(_cTerm) {
    const Coli& cOff=_cTerm->getCollisionOff();
    DFDX.segment(_opt.inputs()+cOff[i]*dim,(cOff[i+1]-cOff[i])*dim)+=DPDC;
  }
}
void CIOOptimizerPolicyPhysicsTerm::randomizeParam()
{
  _coefPhys=RandEngine::randR01();
  _coefPhysADMM=RandEngine::randR01();
  sizeType nrS=_opt._sol.nrX()/2,nrU=nrS-6;
  for(sizeType i=0; i<nrU; i++) {
    scalar R=RandEngine::randR01();
    _opt._sol._tree.put<scalar>(_prefix+boost::lexical_cast<string>(i),R);
  }
  recomputeMetric();
}
void CIOOptimizerPolicyPhysicsTerm::balanceResidual()
{
  sizeType nrS=_opt._sol.nrX()/2,nrU=nrS-6;
  scalar maxCoef=1/_opt._DUDE.maxCoeff()/_opt._DUDE.maxCoeff();
  scalar wPhys=_opt._sol._tree.get<scalar>("regPhysicsCoef",100);
  scalar maxThres=_opt._sol._tree.get<scalar>("maxPhysViolation",0.5f);
  scalar minThres=_opt._sol._tree.get<scalar>("minPhysViolation",0.1f);
  maxCoef*=_opt._sol._tree.get<scalar>(_prefix,wPhys*10);

  //compute relative control error
  Vec num=Vec::Zero(nrU),denom=num,relErr;
  for(sizeType i=0; i<_opt._horizon; i++) {
    const FrameInfo& I=_infos[i];
    num.array()+=(I._DUDEE-I._CNT).array().square();
    denom.array()+=I._DUDEE.array().square();
  }
  num.array()=num.array().sqrt();
  denom.array()=denom.array().sqrt().max((scalar)_opt._horizon);
  relErr=(num.array()/denom.array()).matrix();

  //tune parameter
  Vec coef=Vec::Ones(nrU);
  for(sizeType i=0; i<nrU; i++) {
    coef[i]=_opt._sol._tree.get<scalar>(_prefix+boost::lexical_cast<string>(i),1);
    if(denom[i]/(scalar)_opt._horizon <= 1)
      //initialize, allow for exploration
      coef[i]=maxCoef*1E-2f;
    else if(relErr[i] > maxThres)
      //discourage exploration
      coef[i]=min<scalarD>(coef[i]*2,maxCoef);
    else if(relErr[i] < minThres)
      //encourage exploration
      coef[i]=max<scalarD>(coef[i]*0.5f,maxCoef*1E-2f);
    //update
    _opt._sol._tree.put<scalar>(_prefix+boost::lexical_cast<string>(i),coef[i]);
  }
  recomputeMetric();

  if(_opt._sol._tree.get<bool>("printResidualBalance",false)) {
    INFOV("Balancing Residual, maxCoef=%f",maxCoef)
    cout << "Num: " << num.transpose() << endl;
    cout << "Denom: " << denom.transpose() << endl;
    cout << "RelErr: " << relErr.transpose() << endl;
    cout << "Coef: " << coef.transpose() << endl;
  }
}
void CIOOptimizerPolicyPhysicsTerm::setCoefPhys(scalar coefPhys)
{
  _coefPhys=coefPhys;
  recomputeMetric();
}
void CIOOptimizerPolicyPhysicsTerm::setCoefPhysADMM(scalar coefPhysADMM)
{
  _coefPhysADMM=coefPhysADMM;
  recomputeMetric();
}
void CIOOptimizerPolicyPhysicsTerm::setForNNUpdate(bool NNUpdate)
{
  _forNNUpdate=NNUpdate;
}
Eigen::DiagonalMatrix<scalarD,-1,-1> CIOOptimizerPolicyPhysicsTerm::getCoefPhysADMM() const
{
  sizeType nrS=_opt._sol.nrX()/2,nrU=nrS-6;
  scalar wPhysADMM=_opt._sol._tree.get<scalar>(_prefix,1);

  Vec ret=Vec::Zero(nrU);
  for(sizeType i=0; i<nrU; i++)
    ret[i]=_opt._sol._tree.get<scalar>(_prefix+boost::lexical_cast<string>(i),wPhysADMM);

  Eigen::DiagonalMatrix<scalarD,-1,-1> retDiag(ret.size());
  retDiag.diagonal()=ret*_coefPhysADMM;
  return retDiag;
}
//helper
void CIOOptimizerPolicyPhysicsTerm::setTask() const
{
  NeuralNetPolicy& policy=_opt._sol.getEnv().getPolicy();
  if(policy.getAug().size() != 1 || (sizeType)policy.getAug()[0] != _K) {
    Vec aug=Vec::Zero(1);
    aug[0]=(scalarD)_K;
    OMP_CRITICAL_
    policy.setAug(aug);
  }
}
void CIOOptimizerPolicyPhysicsTerm::recomputeMetric()
{
  Matd metricFInv;
  scalarD wPhys=_opt._sol._tree.get<scalar>("regPhysicsCoef",100)*_coefPhys;
  Eigen::DiagonalMatrix<scalarD,-1,-1> wPhysADMMDiag=getCoefPhysADMM();
  _metricF=_opt._metricSqr*wPhys+_opt._DUDE.transpose()*(wPhysADMMDiag*_opt._DUDE);
  _metricFSqrt=matrixSqrt(_metricF,&metricFInv);
  _lambdaCoef=metricFInv*_opt._DUDE.transpose()*wPhysADMMDiag;
}
