#include "Environment.h"
#include "NeuralNetQPolicy.h"
#include <Dynamics/FEMUtils.h>
#include <CommonFile/ObjMesh.h>
#include <CommonFile/MakeMesh.h>

USE_PRJ_NAMESPACE

//Environment
Environment::Environment():TrajectorySampler(typeid(Environment).name()) {}
bool Environment::read(const string& path)
{
  boost::filesystem::ifstream is(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  read(is,dat.get());
  return is.good();
}
bool Environment::write(const string& path) const
{
  boost::filesystem::ofstream os(path,ios::binary);
  boost::shared_ptr<IOData> dat=getIOData();
  write(os,dat.get());
  return os.good();
}
void Environment::writeEnvVTK(const string& path) const {}
const Policy& Environment::getPolicy() const
{
  return const_cast<Environment&>(*this).getPolicy();
}
Environment::Vec Environment::sampleA(sizeType i,const Vec& S)
{
  return getPolicy().getAction(S,NULL,NULL);
}
scalarD Environment::getReward(sizeType i,const Vec& state,const Vec& A) const
{
  return getReward(i,state,A,NULL,NULL);
}
scalarD Environment::getReward(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const
{
  scalarD ret=-getCost(i,state,A,DRDA,DDRDDA);
  if(DRDA)*DRDA*=-1;
  if(DDRDDA)*DDRDDA*=-1;
  return ret;
}
scalarD Environment::getCost(sizeType i,const Vec& state,const Vec& A) const
{
  return getCost(i,state,A,NULL,NULL);
}
scalarD Environment::getCost(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const
{
  scalarD ret=-getReward(i,state,A,DRDA,DDRDDA);
  if(DRDA)*DRDA*=-1;
  if(DDRDDA)*DDRDDA*=-1;
  return ret;
}
const Environment::Vec& Environment::getState() const
{
  return _state;
}
void Environment::setState(const Vec& s)
{
  _state=s;
}
void Environment::resetPolicy()
{
  _policy=NULL;
}
Environment::Environment(const string& name):TrajectorySampler(name) {}
//PendulumEnvironment
scalarD normalizeAngle(scalarD angle)
{
  scalarD angleN=angle+M_PI;
  while(angleN < 0)
    angleN+=M_PI*2;
  while(angleN > M_PI*2)
    angleN-=M_PI*2;
  angleN-=M_PI;
  return angleN;
}
PendulumEnvironment::PendulumEnvironment():Environment(typeid(PendulumEnvironment).name()) {}
bool PendulumEnvironment::read(istream& is,IOData* dat)
{
  //property tree
  readPtreeBinary(_tree,is);
  reset(_tree);
  //policy
  getPolicy().read(is,dat);
  return is.good();
}
bool PendulumEnvironment::write(ostream& os,IOData* dat) const
{
  //property tree
  writePtreeBinary(_tree,os);
  //policy
  Environment::getPolicy().write(os,dat);
  return os.good();
}
void PendulumEnvironment::writeStateVTK(const string& path)
{
  ObjMesh m;
  scalar l=_tree.get<scalar>("l",1);
  MakeMesh::makeCapsule2D(m,l*0.05f,l/2,32);
  m.getPos()=Vec3(0,l/2,0);
  m.applyTrans(Vec3(0,0,0));
  m.getT()=makeRotation<scalar>(Vec3(0,0,_state[0]));
  m.applyTrans(Vec3(0,0,0));
  m.writeVTK(path,true);
}
void PendulumEnvironment::reset(const boost::property_tree::ptree& pt)
{
  _tree=pt;
}
Policy& PendulumEnvironment::getPolicy()
{
  if(_policy)
    return *_policy;
  Vec scale=Vec::Zero(1);
  scalarD s=_tree.get<scalar>("maxSpeed",8);
  scalarD t=_tree.get<scalar>("maxTorque",2);
  scalarD eps=_tree.get<scalar>("exploration",0.05f);
  if(_tree.get<bool>("useNeural",false)) {
    scale[0]=t;
    _policy.reset(new NeuralNetPolicyPositiveCov(40,40,0,2,scale.size(),&scale,&_tree));
    getPolicy().setActionCov(0.5f,0.95f);
  } else if(_tree.get<bool>("useNeuralQ",false)) {
    _policy.reset(new NeuralNetQPolicy(40,40,0,2,Vec2d(-t,t),&_tree));
    getPolicy().setEps(eps);
  } else if(_tree.get<bool>("useNeuralProbQ",false)) {
    _policy.reset(new NeuralNetProbQPolicy(40,40,0,2,Vec2d(-t,t),&_tree));
  } else if(_tree.get<bool>("useSimpleQ",false)) {
    _policy.reset(new SimpleQPolicy(Vec2d(-M_PI,-s),Vec2d(M_PI,s),Vec2d(-t,t),32));
    getPolicy().setEps(eps);
  } else if(_tree.get<bool>("useStablizedSimpleQ",false)) {
    _policy.reset(new StablizedSimpleQPolicy(Vec2d(-M_PI,-s),Vec2d(M_PI,s),Vec2d(-t,t),32));
    getPolicy().setEps(eps);
  } else {
    ASSERT_MSG(false,"Unknown Policy!")
  }
  return *_policy;
}
PendulumEnvironment::Vec PendulumEnvironment::transfer(sizeType i,const Vec& A)
{
  scalarD maxTorque=_tree.get<scalar>("maxTorque",2);
  scalarD maxSpeed=_tree.get<scalar>("maxSpeed",8);
  scalarD dt=_tree.get<scalar>("dt",0.05f);
  scalarD l=_tree.get<scalar>("l",1);
  scalarD m=_tree.get<scalar>("m",1);
  scalarD g=_tree.get<scalar>("g",10);

  scalarD theta=_state[0],DTheta=_state[1];
  scalarD u=max(min(A[0],maxTorque),-maxTorque);
  _state[1]=DTheta+(-3*g/(2*l)*sin(theta+M_PI)+3/(m*l*l)*u)*dt;
  _state[0]=max<scalarD>(min<scalarD>(normalizeAngle(theta+_state[1]*dt),M_PI),-M_PI);
  _state[1]=max(min(_state[1],maxSpeed),-maxSpeed);
  return _state;
}
scalarD PendulumEnvironment::getReward(sizeType i,const Vec& state,const Vec& A) const
{
  scalarD angleN=normalizeAngle(state[0]);
  return -(angleN*angleN+0.1f*state[1]*state[1]+0.001f*A[0]*A[0]);
}
PendulumEnvironment::Vec PendulumEnvironment::sampleS0()
{
  scalarD eps=0.1f,maxSpeed=_tree.get<scalar>("maxSpeed",8);
  if(_tree.get<bool>("balancing",true)) {
    _state=Vec2d(RandEngine::randR(-eps,eps),0);
  } else if(_tree.get<bool>("swingUp",true)) {
    _state=Vec2d(RandEngine::randR(-eps,eps)+M_PI,0);
  } else {
    _state=Vec2d(RandEngine::randR(-M_PI,M_PI),RandEngine::randR(-maxSpeed,maxSpeed));
  }
  _state[0]=normalizeAngle(_state[0]);
  return _state;
}
//AcrobotEnvironment
AcrobotEnvironment::AcrobotEnvironment():Environment(typeid(AcrobotEnvironment).name()) {}
bool AcrobotEnvironment::read(istream& is,IOData* dat)
{
  //property tree
  readPtreeBinary(_tree,is);
  reset(_tree);
  //policy
  getPolicy().read(is,dat);
  return is.good();
}
bool AcrobotEnvironment::write(ostream& os,IOData* dat) const
{
  //property tree
  writePtreeBinary(_tree,os);
  //policy
  Environment::getPolicy().write(os,dat);
  return os.good();
}
void AcrobotEnvironment::writeStateVTK(const string& path)
{
  //param
  scalar l1=_tree.get<scalar>("LINK_LENGTH_1",1);
  scalar l2=_tree.get<scalar>("LINK_LENGTH_2",1);

  //version 1
  Vec3 p0=Vec3(0,0,0);
  Vec3 p1=Vec3(sin(_state[0]),-cos(_state[0]),0)*l1;
  Vec3 p2=p1+Vec3(sin(_state[0]+_state[1]),-cos(_state[0]+_state[1]),0)*l2;

  vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
  vss.push_back(p0);
  vss.push_back(p1);
  vss.push_back(p2);

  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
  iss.push_back(Vec3i(0,1,0));
  iss.push_back(Vec3i(1,2,0));

  //version 2
  ObjMesh m1;
  MakeMesh::makeCapsule2D(m1,l1*0.05f,l1/2,32);
  m1.getPos()=Vec3(0,l1/2,0);
  m1.applyTrans(Vec3(0,0,0));
  m1.getT()=makeRotation<scalar>(Vec3(0,0,M_PI+_state[0]));
  m1.applyTrans(Vec3(0,0,0));

  ObjMesh m2;
  MakeMesh::makeCapsule2D(m2,l2*0.05f,l2/2,32);
  m2.getPos()=Vec3(0,l2/2,0);
  m2.applyTrans(Vec3(0,0,0));
  m2.getT()=makeRotation<scalar>(Vec3(0,0,M_PI+_state[0]+_state[1]));
  m2.applyTrans(Vec3(0,0,0));
  m2.getPos()=p1;
  m2.applyTrans(Vec3(0,0,0));

  //write
  VTKWriter<scalar> os("acrobot",path,true);
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(iss.begin(),iss.end(),VTKWriter<scalar>::LINE);
  m1.writeVTK(os);
  m2.writeVTK(os);
}
void AcrobotEnvironment::reset(const boost::property_tree::ptree& pt)
{
  _tree=pt;
}
Policy& AcrobotEnvironment::getPolicy()
{
  if(_policy)
    return *_policy;
  Vec scale=Vec::Zero(1);
  scalarD s1=_tree.get<scalar>("maxSpeed",4*M_PI);
  scalarD s2=_tree.get<scalar>("maxSpeed",9*M_PI);
  scalarD eps=_tree.get<scalar>("exploration",0.05f);
  if(_tree.get<bool>("useNeural",false)) {
    scale[0]=1;
    _policy.reset(new NeuralNetPolicyPositiveCov(40,40,40,4,scale.size(),&scale,&_tree));
    getPolicy().setActionCov(0.5f,0.95f);
  } else if(_tree.get<bool>("useNeuralQ",false)) {
    _policy.reset(new NeuralNetQPolicy(40,40,0,4,Vec3d(-1,0,1),&_tree));
    getPolicy().setEps(eps);
  } else if(_tree.get<bool>("useNeuralProbQ",false)) {
    _policy.reset(new NeuralNetProbQPolicy(40,40,0,4,Vec3d(-1,0,1),&_tree));
  } else if(_tree.get<bool>("useSimpleQ",false)) {
    _policy.reset(new SimpleQPolicy(Vec4d(-M_PI,-M_PI,-s1,-s2),Vec4d(M_PI,M_PI,s1,s2),Vec3d(-1,0,1),8));
    getPolicy().setEps(eps);
  } else if(_tree.get<bool>("useStablizedSimpleQ",false)) {
    _policy.reset(new StablizedSimpleQPolicy(Vec4d(-M_PI,-M_PI,-s1,-s2),Vec4d(M_PI,M_PI,s1,s2),Vec3d(-1,0,1),8));
    getPolicy().setEps(eps);
  } else {
    ASSERT_MSG(false,"Unknown Policy!")
  }
  return *_policy;
}
bool AcrobotEnvironment::isTerminal(sizeType i,const Vec& state) const
{
  return getReward(i,state,Vec::Zero(1)) == 0;
}
AcrobotEnvironment::Vec AcrobotEnvironment::transfer(sizeType i,const Vec& A)
{
  sizeType ds=_tree.get<sizeType>("downsample",1);
  scalarD s1=_tree.get<scalar>("maxSpeed",4*M_PI);
  scalarD s2=_tree.get<scalar>("maxSpeed",9*M_PI);
  scalarD dt=_tree.get<scalar>("dt",0.2f);

  for(sizeType i=0; i<ds; i++) {
    Vec K1=DSDT(_state,A);
    Vec K2=DSDT(_state+K1*dt/2,A);
    Vec K3=DSDT(_state+K2*dt/2,A);
    Vec K4=DSDT(_state+K3*dt,A);
    _state=_state+(K1+2*K2+2*K3+K4)*(dt/6);

    _state[0]=normalizeAngle(_state[0]);
    _state[1]=normalizeAngle(_state[1]);
    _state[2]=max(min(_state[2],s1),-s1);
    _state[3]=max(min(_state[3],s2),-s2);
    if(isTerminal(0,_state))
      break;
  }
  return _state;
}
scalarD AcrobotEnvironment::getReward(sizeType i,const Vec& state,const Vec&) const
{
  scalar l1=_tree.get<scalar>("LINK_LENGTH_1",1);
  scalar l2=_tree.get<scalar>("LINK_LENGTH_2",1);
  //Vec3 p0=Vec3(0,0,0);
  Vec3 p1=Vec3(sin(state[0]),-cos(state[0]),0)*l1;
  Vec3 p2=p1+Vec3(sin(state[0]+state[1]),-cos(state[0]+state[1]),0)*l2;
  return p2[1]>1 ? 0 : -1;
}
AcrobotEnvironment::Vec AcrobotEnvironment::sampleS0()
{
  return (_state=Vec::Random(4)*0.1f);
}
AcrobotEnvironment::Vec AcrobotEnvironment::DSDT(const Vec& S,const Vec& A) const
{
  scalarD m1=_tree.get<scalar>("LINK_MASS_1",1);
  scalarD m2=_tree.get<scalar>("LINK_MASS_2",1);
  scalarD l1=_tree.get<scalar>("LINK_LENGTH_1",1);
  scalarD lc1=_tree.get<scalar>("LINK_COM_POS_1",0.5f);
  scalarD lc2=_tree.get<scalar>("LINK_COM_POS_2",0.5f);
  scalarD I1=_tree.get<scalar>("LINK_MOI",1);
  scalarD I2=_tree.get<scalar>("LINK_MOI",1);
  scalarD g=9.8f;
  scalarD theta1 = S[0];
  scalarD theta2 = S[1];
  scalarD dtheta1 = S[2];
  scalarD dtheta2 = S[3];
  scalarD d1 = m1 * pow(lc1,2) + m2 *
               (pow(l1,2) + pow(lc2,2) + 2 * l1 * lc2 * cos(theta2)) + I1 + I2;
  scalarD d2 = m2 * (pow(lc2,2) + l1 * lc2 * cos(theta2)) + I2;
  scalarD phi2 = m2 * lc2 * g * cos(theta1 + theta2 - M_PI / 2.);
  scalarD phi1 = - m2 * l1 * lc2 * pow(dtheta2,2) * sin(theta2)
                 - 2 * m2 * l1 * lc2 * dtheta2 * dtheta1 * sin(theta2)
                 + (m1 * lc1 + m2 * l1) * g * cos(theta1 - M_PI / 2) + phi2;
  //scalarD ddtheta2 = (A[0] + d2 / d1 * phi1 - phi2) /
  //                   (m2 * pow(lc2,2) + I2 - pow(d2,2) / d1);
  scalarD ddtheta2 = (A[0] + d2 / d1 * phi1 - m2 * l1 * lc2 * pow(dtheta1,2) * sin(theta2) - phi2)
                     / (m2 * pow(lc2,2) + I2 - pow(d2,2) / d1);
  scalarD ddtheta1 = -(d2 * ddtheta2 + phi1) / d1;
  return Vec4d(dtheta1, dtheta2, ddtheta1, ddtheta2);
}
//LinkEnvironment
LinkEnvironment::LinkEnvironment():Environment(typeid(AcrobotEnvironment).name())
{
  reset(boost::property_tree::ptree());
}
bool LinkEnvironment::read(istream& is,IOData* dat)
{
  //property tree
  readPtreeBinary(_tree,is);
  reset(_tree);
  //policy
  getPolicy().read(is,dat);
  return is.good();
}
bool LinkEnvironment::write(ostream& os,IOData* dat) const
{
  //property tree
  writePtreeBinary(_tree,os);
  //policy
  Environment::getPolicy().write(os,dat);
  return os.good();
}
void LinkEnvironment::writeStateVTK(const string& path)
{
  sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);
  scalarD segLen=_tree.get<scalar>("segLen",1);

  ObjMesh m,m1;
  MakeMesh::makeCapsule2D(m,segLen*0.05f,segLen/2,32);
  m.getPos()=Vec3(0,segLen/2,0);
  m.applyTrans(Vec3(0,0,0));
  m.getT()=makeRotation<scalar>(Vec3(0,0,-M_PI/2));
  m.applyTrans(Vec3(0,0,0));

  //write
  Vec2 p0(0,0);
  scalar theta=0;
  VTKWriter<scalar> os("link",path,true);
  for(sizeType i=0; i<nrSeg; i++) {
    theta+=(scalar)_state[nrSeg+i];
    m1=m;
    m1.getT()=makeRotation<scalar>(Vec3(0,0,theta));
    m1.applyTrans(Vec3(0,0,0));
    m1.getPos()=Vec3(p0[0],p0[1],0);
    m1.applyTrans(Vec3(0,0,0));
    m1.writeVTK(os);
    p0+=Vec2(cos(theta),sin(theta))*segLen;
  }

  MakeMesh::makeSphere2D(m1,segLen*0.025f,32);
  m1.getPos()[0]=_tree.get<scalar>("targetX");
  m1.getPos()[1]=_tree.get<scalar>("targetY");
  m1.applyTrans();
  m1.writeVTK(os);
}
void LinkEnvironment::reset(const boost::property_tree::ptree& pt)
{
  sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);
  scalarD segLen=_tree.get<scalar>("segLen",1);
  scalarD L=RandEngine::randR(0,segLen*(scalarD)nrSeg);
  scalarD theta=RandEngine::randR(0,M_PI*2);

  _tree=pt;
  if(!_tree.get_optional<scalar>("targetX"))
    _tree.put<scalar>("targetX",cos(theta)*L);
  if(!_tree.get_optional<scalar>("targetY"))
    _tree.put<scalar>("targetY",sin(theta)*L);
}
Policy& LinkEnvironment::getPolicy()
{
  if(!_policy) {
    sizeType nrH1=_tree.get<sizeType>("nrH1",40);
    sizeType nrH2=_tree.get<sizeType>("nrH2",40);
    sizeType nrH3=_tree.get<sizeType>("nrH3",0);
    sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);
    _policy.reset(new NeuralNetPolicyPositiveCov(nrH1,nrH2,nrH3,nrSeg*2,nrSeg,NULL,&_tree));
  }
  return *_policy;
}
bool LinkEnvironment::isTerminal(sizeType i,const Vec& state) const
{
  sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);
  scalarD terminalVel=_tree.get<scalar>("terminalVel",0.5f);
  scalarD terminalDist=_tree.get<scalar>("terminalDist",1E-2f);
  scalarD maxVel=state.segment(0,nrSeg).cwiseAbs().maxCoeff();
  return (maxVel < terminalVel && Environment::getReward(i,state,Vec::Zero(nrSeg)) < terminalDist);
}
LinkEnvironment::Vec LinkEnvironment::transfer(sizeType i,const Vec& A)
{
  sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);
  scalar dt=_tree.get<scalarD>("dt",0.01f);
#ifdef NO_DYNAMICS
  _state.segment(nrSeg,nrSeg)+=A*dt;
#else
  _state.segment(0,nrSeg)+=A*dt;
  _state.segment(nrSeg,nrSeg)+=_state.segment(0,nrSeg)*dt;
#endif
  return _state;
}
scalarD LinkEnvironment::getReward(sizeType i,const Vec& state,const Vec&,Vec* DRDA,Matd* DDRDDA) const
{
  Vec2d target;
  target[0]=_tree.get<scalar>("targetX");
  target[1]=_tree.get<scalar>("targetY");
  sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);

  Vss vss;
  Matd DCDA,DDCDDA[2];
  Vec2d curr=jacobian(state.segment(nrSeg,nrSeg),DCDA,DDCDDA,vss);
  if(DRDA) {
    DRDA->setZero(nrSeg*2);
    DRDA->segment(nrSeg,nrSeg)=DCDA.transpose()*(curr-target);
  }
  if(DDRDDA) {
    DDRDDA->setZero(nrSeg*2,nrSeg*2);
    DDRDDA->block(nrSeg,nrSeg,nrSeg,nrSeg)+=DCDA.transpose()*DCDA;
    DDRDDA->block(nrSeg,nrSeg,nrSeg,nrSeg)+=(curr-target)[0]*DDCDDA[0];
    DDRDDA->block(nrSeg,nrSeg,nrSeg,nrSeg)+=(curr-target)[1]*DDCDDA[1];
  }
  return (curr-target).squaredNorm()/2;
}
LinkEnvironment::Vec LinkEnvironment::sampleS0()
{
  sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);
  _state.setZero(nrSeg*2);
  _state.segment(nrSeg,nrSeg)=Vec::Random(nrSeg)*M_PI;
  return _state;
}
Vec2d LinkEnvironment::jacobian(const Vec& state,Matd& DADC,Matd DDADDC[2],Vss& vss) const
{
  scalarD theta=0;
  sizeType nrSeg=_tree.get<sizeType>("nrSeg",3);
  scalarD segLen=_tree.get<scalar>("segLen",1);

  DADC.setZero(2,nrSeg);
  DDADDC[0].setZero(nrSeg,nrSeg);
  DDADDC[1].setZero(nrSeg,nrSeg);

  vss.clear();
  vss.push_back(Vec2d::Zero());
  for(sizeType i=0; i<state.size(); i++) {
    theta+=state[i];
    Vec2d p0=vss.back();
    vss.push_back(Vec2d(cos(theta),sin(theta))*segLen+p0);
    DADC.block(0,0,1,i+1)+=Vec::Constant(i+1,-sin(theta)*segLen).transpose();
    DADC.block(1,0,1,i+1)+=Vec::Constant(i+1, cos(theta)*segLen).transpose();
    DDADDC[0].block(0,0,i+1,i+1)+=Matd::Constant(i+1,i+1,-cos(theta)*segLen);
    DDADDC[1].block(0,0,i+1,i+1)+=Matd::Constant(i+1,i+1,-sin(theta)*segLen);
  }
  return vss.back();
}
