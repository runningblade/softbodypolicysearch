#ifndef FEM_DDP_SOLVER_H
#define FEM_DDP_SOLVER_H

#include "DDPSolver.h"
#include "FEMEnvironment.h"
#include <Dynamics/CIOContactInfo.h>

PRJ_BEGIN

//DDPSolverSoft
class CIOOptimizer;
class FEMCIOEngine;
class FEMDDPSolver : public DDPSolver, public Serializable
{
public:
  using DDPSolver::Vec;
  typedef vector<FEMLCPSolver::Contacts> Contacts;
  FEMDDPSolver(FEMEnvironment& env);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual bool read(const string& name="CIOResult");
  virtual bool write(const string& name="CIOResult");
  virtual void readFromTree();
  virtual void writeToTree();
  virtual void writeSelfCollVTK(const string& path);
  virtual void writeEnvVTK(const string& path) const;
  virtual void writeStateVTK(const string& path,const Vec& x) const;
  virtual void writeStatePov(const string& path,const Vec& x) const;
  virtual void writeStatesVTK(const string& path,const boost::property_tree::ptree* tree=NULL,bool useXs=false,bool pov=false) const;
  virtual void resetParam(sizeType maxIter,sizeType maxIterILQG,sizeType horizon);
  virtual void setBound();
  //debug and visualization
  virtual void debugObj(const Vec& x,const Vec& u,bool addObjUX);
  virtual void updateNeuralNet(bool debugMode,sizeType maxIt,scalar epsilon=-1,scalar delta=-1);
  virtual void writeResidualInfo(bool SP,bool MP);
  virtual void assignControl(bool SP);
  virtual void recenter(boost::property_tree::ptree* tree=NULL,bool useXs=false);
  //for cio
  virtual Vec sampleInit(const boost::property_tree::ptree& pt,sizeType f) const;
  virtual void initializeCIO(const string& name="CIOInitialGuess");
  virtual void optimizeCIO(bool MP);
  virtual void debugCIOGradient(bool SP,bool MP,bool load=false);
  //for cio using ADLM
  virtual void optimizeCIOLM(bool MP,bool load=false);
  virtual void debugCIOLMEnergy(bool SP,bool MP,bool load=false);
  virtual void debugCIOForce(bool SP,bool MP,bool load=false);
  virtual void debugCIOSQPEnergy(bool SP,bool MP,bool linearApprox,bool load=false);
  //for policy search
  virtual void optimizeCIOPolicy(bool SP,bool MP,bool load=false);
  virtual void debugNeuralNet(bool load=false);
  virtual NeuralNet& getNeuralNet() const;
  //objective function
  virtual scalarD objX(sizeType i,const Vec& x,Vec* fx,Matd* fxx) const;
  virtual Vec objXF(sizeType i,const Vec& x,Matd* fjac) const;
  virtual sizeType nrObjXF() const;
  //multi-task version
  virtual scalarD objXMT(sizeType i,const Vec& x,Vec* fx,Matd* fxx,sizeType K) const;
  virtual Vec objXFMT(sizeType i,const Vec& x,Matd* fjac,sizeType K) const;
  virtual sizeType nrObjXFMT(sizeType K) const;
  //dynamics
  virtual void dfdx(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu) const;
  virtual void dfdxc(sizeType i,const Vec& x,const Vec& u,Vec& f,Matd* fx,Matd* fu,FEMLCPSolver::Contacts* cons) const;
  //getter/setter
  const FEMEnvironment& getEnv() const;
  FEMEnvironment& getEnv();
  const FEMCIOEngine& getEngine() const;
  FEMCIOEngine& getEngine();
protected:
  FEMDDPSolver(const string& name,FEMEnvironment& env);
  static void readFromTree(const RigidReducedMCalculator& MCalc,FEMDDPSolverInfo& info,
                           const boost::property_tree::ptree& pt,sizeType horizon);
  static void writeToTree(const RigidReducedMCalculator& MCalc,const FEMDDPSolverInfo& info,
                          boost::property_tree::ptree& pt,sizeType horizon);
  virtual boost::shared_ptr<CIOOptimizer> createOpt(bool SP,bool MP,FEMDDPSolverInfo& info,sizeType K=-1);
  virtual boost::shared_ptr<CIOOptimizer> createOpt(bool SP,bool MP);
  FEMDDPSolverInfo _info;
  FEMEnvironment& _env;
};

PRJ_END

#endif
