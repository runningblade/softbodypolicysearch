#include "FEMGradientOpt.h"
#include <Dynamics/FEMMesh.h>
#include <Dynamics/FEMRigidReducedSystem.h>
#include <CommonFile/solvers/Minimizer.h>

USE_PRJ_NAMESPACE

FEMGradientOpt::FEMGradientOpt(RigidReducedMCalculator& MCalc,FEMBody& body,const Vec& D)
  :_MCalc(MCalc),_body(body),_D(D) {}
void FEMGradientOpt::solve(Vec& x,sizeType maxIt,scalarD eps,scalarD delta,bool useCB)
{
  _body._system->setPos(x);
  _body._system->getPos(x);

  scalarD fx=0;
  LBFGSMinimizer<scalarD> sol;
  boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > > cb;
  if(useCB)
    cb.reset(new Callback<scalarD,Kernel<scalarD> >);
  else cb.reset(new NoCallback<scalarD,Kernel<scalarD> >);
  sol.delta()=delta;
  sol.epsilon()=eps;
  sol.maxIterations()=maxIt;
  sol.minimize(x,fx,*this,*cb);
}
int FEMGradientOpt::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  Vec X;
  Matd M=Matd::Zero(inputs(),inputs());
  _body._system->setPos(x);
  _body.getPos(X);
  X-=_D;

  DFDX.setZero(x.size());
  RigidReducedMCalculator& MCalc=
    const_cast<RigidReducedMCalculator&>(_MCalc);
  MCalc.calculateM(x,M,X,DFDX,0,0,NULL);
  FX=X.dot(_body._basis->_M*X)/2;
  //INFOV("GNorm: %f",DFDX.norm())
  return 0;
}
int FEMGradientOpt::inputs() const
{
  return _MCalc.size();
}
void FEMGradientOpt::debugGradient()
{
#define NR_TEST 10
#define DELTA 1E-7f
  scalarD FX,FX2;
  for(sizeType i=0; i<NR_TEST; i++) {
    Vec x=Vec::Random(inputs()),DFDX,DFDX2;
    Vec delta=Vec::Random(inputs());
    operator()(x,FX,DFDX,1,true);
    operator()(x+delta*DELTA,FX2,DFDX2,1,true);
    INFOV("Gradient: %f Err: %f",DFDX.dot(delta),DFDX.dot(delta)-(FX2-FX)/DELTA)
  }
#undef DELTA
#undef NR_TEST
}
