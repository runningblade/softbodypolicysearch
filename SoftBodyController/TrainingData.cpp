#include "TrainingData.h"
#include <Dynamics/FEMUtils.h>
#include <boost/lexical_cast.hpp>
#include <CommonFile/IO.h>

USE_PRJ_NAMESPACE

TrajectorySampler::TrajectorySampler(const string& name):Serializable(name) {}
void TrajectorySampler::setState(const Vec& state)
{
  ASSERT_MSG(false,"This environment does not support setState()!")
}
void TrajectorySampler::writeStateVTK(const string& path)
{
  VTKWriter<scalarD> os("trajFrame",path,true);
}
bool TrajectorySampler::isTerminal(sizeType i,const Vec& state) const
{
  return false;
}
TrajectorySampler::Vec TrajectorySampler::transfer(sizeType i,const Vec& A,scalarD& reward)
{
  Vec state=transfer(i,A);
  reward=getReward(i,state,A);
  return state;
}
bool TrajectorySampler::supportMultiTraj() const
{
  return false;
}
void TrajectorySampler::startMultiTraj(MultiTrajCallback& cb)
{
  ASSERT_MSG(false,"MultiTraj interface is not supported!")
}
void TrajectorySampler::setTrajId(sizeType trajId) {}
//trajectory sampler
RandomTrajectorySampler::RandomTrajectorySampler()
  :TrajectorySampler(typeid(RandomTrajectorySampler).name()) {}
RandomTrajectorySampler::RandomTrajectorySampler(sizeType nrState,sizeType nrAction)
  :TrajectorySampler(typeid(RandomTrajectorySampler).name()),_nrState(nrState),_nrAction(nrAction) {}
bool RandomTrajectorySampler::read(istream& is,IOData* dat)
{
  readBinaryData(_nrState,is);
  readBinaryData(_nrAction,is);
  return is.good();
}
bool RandomTrajectorySampler::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_nrState,os);
  writeBinaryData(_nrAction,os);
  return os.good();
}
boost::shared_ptr<Serializable> RandomTrajectorySampler::copy() const
{
  return boost::shared_ptr<Serializable>(new RandomTrajectorySampler);
}
void RandomTrajectorySampler::setState(const Vec& state) {}
RandomTrajectorySampler::Vec RandomTrajectorySampler::transfer(sizeType i,const Vec& A)
{
  return Vec::Random(_nrState);
}
scalarD RandomTrajectorySampler::getReward(sizeType i,const Vec& state,const Vec& A) const
{
  return RandEngine::randR(-1,1);
}
RandomTrajectorySampler::Vec RandomTrajectorySampler::sampleA(sizeType i,const Vec& S)
{
  return Vec::Random(_nrAction);
}
RandomTrajectorySampler::Vec RandomTrajectorySampler::sampleS0()
{
  return Vec::Random(_nrState);
}
//A state in a trajectory
TrajectoryState::TrajectoryState():Serializable(typeid(TrajectoryAction).name()) {}
TrajectoryState::~TrajectoryState() {}
bool TrajectoryState::read(istream& is,IOData* dat)
{
  readVector(_actions,is,dat);
  readBinaryData(_last,is);
  readBinaryData(_data,is);
  return is.good();
}
bool TrajectoryState::write(ostream& os,IOData* dat) const
{
  writeVector(_actions,os,dat);
  writeBinaryData(_last,os);
  writeBinaryData(_data,os);
  return os.good();
}
boost::shared_ptr<Serializable> TrajectoryState::copy() const
{
  return boost::shared_ptr<Serializable>(new TrajectoryState);
}
void TrajectoryState::assemble(Matd& states,Matd& actions,sizeType& off) const
{
  for(sizeType i=0; i<(sizeType)_actions.size(); i++) {
    states.col(off)=_data;
    actions.col(off)=_actions[i]._data;
    off++;

    _actions[i].assemble(states,actions,off);
  }
}
void TrajectoryState::assemble(vector<Transition>& trans) const
{
  Transition tran;
  for(sizeType i=0; i<(sizeType)_actions.size(); i++) {
    tran._state=_data;
    tran._action=_actions[i]._data;
    tran._nextState=_actions[i]._next._data;
    tran._reward=_actions[i]._reward;
    tran._last=_actions[i]._next._last;
    trans.push_back(tran);
    _actions[i].assemble(trans);
  }
}
sizeType TrajectoryState::size() const
{
  sizeType cols=(sizeType)_actions.size();
  for(sizeType i=0; i<(sizeType)_actions.size(); i++)
    cols+=_actions[i].size();
  return cols;
}
//An action in a trajectory
TrajectoryAction::TrajectoryAction():Serializable(typeid(TrajectoryAction).name()) {}
TrajectoryAction::~TrajectoryAction() {}
bool TrajectoryAction::read(istream& is,IOData* dat)
{
  _next.read(is,dat);
  readBinaryData(_reward,is);
  readBinaryData(_data,is);
  return is.good();
}
bool TrajectoryAction::write(ostream& os,IOData* dat) const
{
  _next.write(os,dat);
  writeBinaryData(_reward,os);
  writeBinaryData(_data,os);
  return os.good();
}
boost::shared_ptr<Serializable> TrajectoryAction::copy() const
{
  return boost::shared_ptr<Serializable>(new TrajectoryAction);
}
void TrajectoryAction::assemble(Matd& states,Matd& actions,sizeType& off) const
{
  _next.assemble(states,actions,off);
}
void TrajectoryAction::assemble(vector<Transition>& trans) const
{
  _next.assemble(trans);
}
sizeType TrajectoryAction::size() const
{
  return (sizeType)_next.size();
}
//A collection of trajectories
TrainingData::TrainingData():Serializable(typeid(TrainingData).name()) {}
TrainingData::~TrainingData() {}
bool TrainingData::read(istream& is,IOData* dat)
{
  readVector(_trajs,is);
  return is.good();
}
bool TrainingData::write(ostream& os,IOData* dat) const
{
  writeVector(_trajs,os);
  return os.good();
}
boost::shared_ptr<Serializable> TrainingData::copy() const
{
  return boost::shared_ptr<Serializable>(new TrainingData);
}
const TrajectoryState* TrainingData::traj(sizeType i) const
{
  ASSERT(i >= 0 && i < nrTraj())
  return _trajs[i].get();
}
bool TrainingData::exitByTerminal(const TrajectoryState& s0)
{
  const TrajectoryState* last=&s0;
  while(last) {
    if(last->_last)
      return true;
    if(last->_actions.empty())
      break;
    last=&(last->_actions[0]._next);
  }
  return false;
}
scalarD TrainingData::reward(const TrajectoryState& s0,scalarD discountCoef)
{
  scalarD reward=0,discount=1;
  const TrajectoryState* last=&s0;
  while(last && !last->_actions.empty()) {
    reward+=last->_actions[0]._reward*discount;
    discount*=discountCoef;
    last=&(last->_actions[0]._next);
  }
  return reward;
}
void TrainingData::assemble(Matd& states,Matd& actions,sizeType& off) const
{
  states.resize(nrState(),size()+off);
  actions.resize(nrAction(),size()+off);
  sizeType j=off;
  for(sizeType i=0; i<(sizeType)_trajs.size(); i++)
    _trajs[i]->assemble(states,actions,j);
  ASSERT(j == states.cols());
}
void TrainingData::assemble(Matd& states,Matd& actions) const
{
  sizeType off=0;
  assemble(states,actions,off);
}
void TrainingData::assemble(vector<Transition>& trans) const
{
  sizeType off=(sizeType)trans.size();
  for(sizeType i=0; i<(sizeType)_trajs.size(); i++)
    _trajs[i]->assemble(trans);
  ASSERT((sizeType)trans.size() == off+size())
}
TrajectoryState* TrainingData::addTraj()
{
  _trajs.push_back(boost::shared_ptr<TrajectoryState>(new TrajectoryState));
  return _trajs.back().get();
}
void TrainingData::clear()
{
  _trajs.clear();
}
sizeType TrainingData::nrTraj() const
{
  return (sizeType)_trajs.size();
}
sizeType TrainingData::nrState() const
{
  ASSERT_MSG(!_trajs.empty(),"No trajectory exists to determine dimension of state space!")
  return _trajs[0]->_data.size();
}
sizeType TrainingData::nrAction() const
{
  ASSERT_MSG(!_trajs.empty() && !_trajs[0]->_actions.empty(),"No action exists to determine dimension of action space!")
  return _trajs[0]->_actions[0]._data.size();
}
sizeType TrainingData::size() const
{
  sizeType cols=0;
  for(sizeType i=0; i<(sizeType)_trajs.size(); i++)
    cols+=_trajs[i]->size();
  return cols;
}
