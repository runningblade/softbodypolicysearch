#ifndef ONE_LINE_CALLBACK_H
#define ONE_LINE_CALLBACK_H

#include <CommonFile/solvers/Callback.h>
#include <CMAES/MPIInterface.h>
#include <iomanip>

PRJ_BEGIN

template <typename T,typename KERNEL_TYPE>
struct OneLineCallback : public Callback<T,KERNEL_TYPE> {
public:
  using typename Callback<T,KERNEL_TYPE>::Vec;
  using typename Callback<T,KERNEL_TYPE>::Mat;
  OneLineCallback(int precision=10) {
    cout << setprecision(precision);
    _rollback=true;
    _new=true;
    reset();
  }
  ~OneLineCallback() {
    reset();
  }
  virtual void reset() {
    if(!_new) {
      rollback();
      cout << endl;
      _new=true;
    }
  }
  virtual void writeToFile(const string& path) {
    if(!path.empty())
      _os.reset(new boost::filesystem::ofstream(path));
  }
  virtual sizeType operator()(const Vec& x,const T& residueNorm,const sizeType& n) {
    cout << "ITER " << (int)n << ": r=" << convert<double>()(residueNorm);
    if(_os)
      *_os << "ITER " << (int)n << ": r=" << convert<double>()(residueNorm) << endl << flush;
    rollback();
    cout.flush();
    if(_new) {
      cout << endl;
      _new=false;
    }
    return 0;
  }
  virtual sizeType operator()(const Vec& x,const Vec& g,const T& fx,const T& xnorm,const T& gnorm,const T& step,const sizeType& k,const sizeType& ls) {
    cout << "ITER " << (int)k
         << ": f=" << convert<double>()(fx)
         << ", x=" << convert<double>()(xnorm)
         << ", g=" << convert<double>()(gnorm)
         << ", s=" << convert<double>()(step);
    if(_os)
      *_os << "ITER " << (int)k
           << ": f=" << convert<double>()(fx)
           << ", x=" << convert<double>()(xnorm)
           << ", g=" << convert<double>()(gnorm)
           << ", s=" << convert<double>()(step) << endl << flush;
    _f1=fx;
    rollback();
    cout.flush();
    if(_new) {
      _f0=fx;
      cout << endl;
      _new=false;
    }
    return 0;
  }
  virtual sizeType operator()(sizeType iter,const Vec& vals,const Mat& points) {
    cout << "ITER " << (int)iter
         << ": minF=" << convert<double>()(vals.minCoeff())
         << ", maxF=" << convert<double>()(vals.maxCoeff())
         << ", populationSize=" << (int)points.cols();
    if(_os)
      *_os << "ITER " << (int)iter
           << ": minF=" << convert<double>()(vals.minCoeff())
           << ", maxF=" << convert<double>()(vals.maxCoeff())
           << ", populationSize=" << (int)points.cols() << endl << flush;
    rollback();
    cout.flush();
    if(_new) {
      cout << endl;
      _new=false;
    }
    return 0;
  }
  bool _rollback;
  T _f0,_f1;
private:
  void rollback() {
    if(_rollback)
      cout << "\r";
    else cout << endl;
  }
  boost::shared_ptr<boost::filesystem::ofstream> _os;
  bool _new;
};

PRJ_END

#endif
