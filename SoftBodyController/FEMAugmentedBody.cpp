#include "FEMAugmentedBody.h"
#include "OneLineCallback.h"
#include "MLUtil.h"
#include <Dynamics/LMInterface.h>
#include <Dynamics/FEMCollision.h>
#include <CommonFile/solvers/Minimizer.h>
#include <CommonFile/DisjointSet.h>

USE_PRJ_NAMESPACE

FEMAugmentedBody::FEMAugmentedBody(const FEMCIOEngine& eng,const string& path,const string* pathEmbed,const Vec3& ctr,scalar dt)
  :_eng(eng),_meshAug(eng.getBody().dim(),boost::shared_ptr<FEMCollision>(new FEMCollision)),_dt(dt)
{
  //add mesh
  _meshAug.reset(path,0);
  FEMBody& b=_meshAug.getB(0);
  const FEMBody& b0=_eng.getBody();
  //add embedded mesh
  if(pathEmbed) {
    ObjMesh smesh;
    boost::filesystem::ifstream is(*pathEmbed);
    smesh.read(is,false,false);
    //smesh.getPos()=ctr;
    //smesh.applyTrans();
    _meshAug.addEmbeddedMesh(smesh);
  }
  //add energy
  _info._young=b0._tree.get<scalar>("cell0.young")*0.01f;
  _info._poisson=b0._tree.get<scalar>("cell0.poisson");
  MaterialEnergy::TYPE energyType=MaterialEnergy::COROTATIONAL;
  b._system.reset(new FEMSystem(b));
  b._system->addEnergyMaterial(_info,energyType,true);
  b.getMass(_M);
  //find correspondence
  vector<Vec3,Eigen::aligned_allocator<Vec3> > ps;
  vector<FEMInterp> evss(b0.nrV());
  for(sizeType i=0; i<b0.nrV(); i++)
    ps.push_back(b0.getV(i)._pos0-ctr);
  _meshAug.getBary(ps,evss,1E-2f);
  for(sizeType i=0; i<(sizeType)evss.size(); i++) {
    const FEMInterp& I=evss[i];
    ASSERT(I._cell)
    for(int c=0; c<4; c++)
      if(I._coef[c] > 0.9f) {
        sizeType id=I._cell->_v[c]->_index;
        _corr.push_back(id);
        _fixSet.insert(id*3+0);
        _fixSet.insert(id*3+1);
        _fixSet.insert(id*3+2);
      }
  }
  ASSERT((sizeType)_corr.size() == b0.nrV())
  _dynamics=false;
  //add constraint
  Vec3 aa0(1.2f,1.6f,1);
  Vec3 bb0(1.55f,1.4f,1.08f);
  buildConstraint(ctr,aa0,bb0);
}
FEMAugmentedBody::~FEMAugmentedBody() {}
void FEMAugmentedBody::solveAug()
{
  //apply transform
  FEMBody& b=_meshAug.getB(0);
  const FEMBody& b0=_eng.getBody();
  Vec xb=Vec::Zero(b0.nrV()*3),xb0=xb,x;
  for(sizeType i=0; i<(sizeType)_corr.size(); i++) {
    xb.segment<3>(i*3)=b.getV(_corr[i])._pos.cast<scalarD>();
    xb0.segment<3>(i*3)=b0.getV(i)._pos.cast<scalarD>();
  }
  Mat4d T=AToB(xb,xb0);
  for(sizeType i=0; i<b.nrV(); i++) {
    Vec3& pos=b.getV(i)._pos;
    pos=(T.block<3,3>(0,0)*pos.cast<scalarD>()+T.block<3,1>(0,3)).cast<scalar>();
  }

  //initialize
  b.getDPos(x);
  for(sizeType i=0; i<(sizeType)_corr.size(); i++)
    x.segment<3>(_corr[i]*3)=(b0.getV(i)._pos-b0.getV(i)._pos0).cast<scalarD>();
  OneLineCallback<scalarD,Kernel<scalarD> > cb;
  _x0=x;
  //solve LBFGS
  scalarD fx;
  LBFGSMinimizer<scalarD> sol;
  sol.delta()=1E-4f;
  sol.epsilon()=1E-4f;
  sol.nrCorrect()=100;
  sol.minimize(x,fx,*this,cb);
  _t+=_dt;
  //assign
  _meshAug.getB(0).setDPos(x);
  if(!_dynamics) {
    _xLast=_xCurr=x;
    _dynamics=true;
  } else {
    _xLast=_xCurr;
    _xCurr=x;
  }
}
FEMMesh& FEMAugmentedBody::getMeshAug()
{
  return _meshAug;
}
const FEMMesh& FEMAugmentedBody::getMeshAug() const
{
  return _meshAug;
}
//objective
void FEMAugmentedBody::debugGradient()
{
#define NR_TEST 10
#define DELTA 1E-7f
  scalarD fx,fx2;
  Vec x=Cold::Random(inputs());
  //removeConstraint(&x,NULL);
  Cold dfdx=Cold::Zero(inputs()),dfdx2=dfdx;
  operator()(x,fx,dfdx,1,true);
  for(sizeType i=0; i<NR_TEST; i++) {
    Cold delta=Cold::Random(inputs());
    //removeConstraint(&delta,NULL);
    operator()(x+delta*DELTA,fx2,dfdx2,1,true);
    INFOV("Gradient: %f, Err: %f",dfdx.dot(delta),(dfdx.dot(delta)-(fx2-fx)/DELTA))
  }
#undef DELTA
#undef NR_TEST
  exit(-1);
}
int FEMAugmentedBody::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  FEMBody& b=_meshAug.getB(0);
  DFDX.setZero();
  b.setDPos(x);
  //potential part
  FX=b._system->energys().materialEnergyEval(&DFDX,NULL,-1,1);
  //kinetic part
  if(_dynamics) {
    Vec accel=(x-2*_xCurr+_xLast)/_dt;
    FX+=(accel.array()*accel.array()*_M.array()).sum()/2;
    DFDX+=(accel.array()*_M.array()).matrix()/_dt;
  }
  //collision
  scalar collCoef=1E4f;
  for(sizeType v=0; v<b.nrV(); v++) {
    scalarD dist=b.getV(v)._pos[1]+0.01f;
    if(dist < 0) {
      FX+=dist*dist*collCoef/2;
      DFDX[v*3+1]+=dist*collCoef;
    }
  }
  //constraint
  {
    Vec3 df(0,0,0);
    Vec3 va=_a._cell->getVert(_a);
    Vec3 vb=_b._cell->getVert(_b);

    if(_keyframe) {
      scalar angle=sin(_t*M_PI*2/_phase)*_angleThres;
      Vec3 target=Vec3(cos(angle),0,sin(angle))*_len;
      FX+=(va-vb+target).dot(va-vb+target)*collCoef/2;
      df=(va-vb+target)*collCoef;
      addDeriv(DFDX,_a, df);
      addDeriv(DFDX,_b,-df);
    } else {
      FX+=(va[1]-vb[1])*(va[1]-vb[1])*collCoef/2;
      FX+=(va[2]-vb[2])*(va[2]-vb[2])*collCoef/2;
      df[1]=(va[1]-vb[1])*collCoef;
      df[2]=(va[2]-vb[2])*collCoef;
      addDeriv(DFDX,_a, df);
      addDeriv(DFDX,_b,-df);
    }
  }
  removeConstraint(x,FX,&DFDX,NULL);
  return 0;
}
void FEMAugmentedBody::removeConstraint(const Vec& x,scalarD& FX,Vec* fgrad,STrips* fhess) const
{
  if(fgrad)
    for(boost::unordered_set<sizeType>::const_iterator
        beg=_fixSet.begin(),end=_fixSet.end(); beg!=end; beg++)
      (*fgrad)[*beg]=0;
  if(fhess) {
    sizeType j=0;
    for(sizeType i=0; i<(sizeType)fhess->getVector().size(); i++) {
      STrip& s=fhess->getVector()[i];
      if(_fixSet.find(s.row()) != _fixSet.end() ||
         _fixSet.find(s.row()) != _fixSet.end())
        fhess->getVector()[j++]=fhess->getVector()[i];
    }
    fhess->getVector().resize(j);
  }

  //fix
  /*scalarD coef=_info._young*1E-2f;
  for(boost::unordered_set<sizeType>::const_iterator
      beg=_fixSet.begin(),end=_fixSet.end(); beg!=end; beg++) {
    scalarD D=x[*beg]-_x0[*beg];
    if(fgrad)
      (*fgrad)[*beg]+=coef*D;
    if(fhess)
      fhess->push_back(STrip(*beg,*beg,coef));
    FX+=coef*D*D/2;
  }*/
}
int FEMAugmentedBody::inputs() const
{
  return _meshAug.getB(0).nrV()*3;
}
//constraints
void FEMAugmentedBody::buildConstraint(const Vec3& ctr,const Vec3& a0,const Vec3& b0)
{
  vector<Vec3,Eigen::aligned_allocator<Vec3> > ps;
  vector<FEMInterp> evss(2);
  ps.push_back(a0);
  ps.push_back(b0);

  _t=0;
  _phase=2;
  _angleThres=M_PI/3;
  _len=(a0-b0).norm();

  _meshAug.getBary(ps,evss,1E-2f);
  _a=evss[0];
  _b=evss[1];
}
void FEMAugmentedBody::addDeriv(Vec& DFDX,const FEMInterp& I,const Vec3& df) const
{
  for(sizeType i=0; i<4; i++)
    if(I._cell->_v[i]) {
      sizeType id=I._cell->_v[i]->_index;
      DFDX.segment<3>(id*3)+=df*I._coef[i];
    }
}
