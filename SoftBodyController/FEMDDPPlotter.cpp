#include "FEMDDPPlotter.h"
#include "NeuralNet.h"
#include "DMPLayer.h"
#include "MLUtil.h"
#include <Dynamics/FEMUtils.h>
#include <Dynamics/FEMCIOEngine.h>
#include <boost/lexical_cast.hpp>

PRJ_BEGIN

//DDPPlotter
FEMDDPPlotter::FEMDDPPlotter(FEMDDPSolver& ddp):_ddp(ddp) {}
//maxima
void FEMDDPPlotter::writeDMPParamMaxima(const Matd& data,const string& name,const string& path) const
{
  checkMaximaPath(path);
  boost::filesystem::ofstream os(path);
  for(sizeType i=0; i<data.rows(); i++)
    writeMaxima(os,name+boost::lexical_cast<string>(i),NULL,data.row(i).transpose());
}
void FEMDDPPlotter::writeDMPStateMaxima(NeuralNet& net,const Matd& data,const string& name,const string& path,bool norm) const
{
  checkMaximaPath(path);
  Matd dataOut=Matd::Zero(norm ? 1 : net.nrOutput(),data.cols());
  for(sizeType i=0; i<data.cols(); i++)
    if(norm)
      dataOut(0,i)=net.foreprop(data.col(i)).norm();
    else dataOut.col(i)=net.foreprop(data.col(i));
  writeDMPParamMaxima(dataOut,name,path);
}
void FEMDDPPlotter::writeDMPPolicyMaxima(const string& name,const string& path,bool norm,sizeType res) const
{
  scalar dt=_ddp.getEngine()._tree.get<scalar>("dt")/(scalar)res;
  sizeType horizon=_ddp._tree.get<sizeType>("horizon");
  Matd data=Matd::Zero(1,horizon*res);
  for(sizeType i=0; i<data.cols(); i++)
    data(0,i)=(scalar)i*dt;
  writeDMPStateMaxima(*(_ddp.getEnv().getPolicy()._net),data,name,path,norm);
}
void FEMDDPPlotter::writeDMPPolicyMaximaAnalytic(const string& name,const string& path) const
{
  boost::filesystem::ofstream os(path);
  DMPLayer* lPtr=_ddp.getEnv().getPolicy()._net->getLayer<DMPLayer>(0);
  ASSERT_MSG(lPtr,"No DMPLayer found!")
  DMPLayer& l=*lPtr;
  for(sizeType i=0; i<l.nrOutput(1); i++) {
    os << name << i << "(x):= ";
    for(sizeType j=0; j<l.w().cols(); j++) {
      os << l.w()(i,j) << "*" << "exp(" << pow(l.h()(i,j),2) << "*(cos(x*" << l.tau() << "-" << l.mu()(i,j) << ")-1))";
      if(j < l.w().cols()-1)
        os << "+";
      else os << ";" << endl;
    }
  }
}
//ascii
void FEMDDPPlotter::writeDMPParamAscii(const Matd& data,const string& name,const string& path) const
{
  boost::filesystem::ofstream os(path);
  for(sizeType i=0; i<data.cols(); i++) {
    os << name << i << ":" << endl;
    os << data.col(i).transpose() << endl;
  }
}
void FEMDDPPlotter::writeDMPStateAscii(NeuralNet& net,const Matd& data,const string& name,const string& path,bool norm) const
{
  boost::filesystem::ofstream os(path);
  for(sizeType i=0; i<data.cols(); i++) {
    os << name << i << ":" << endl;
    if(norm)
      os << net.foreprop(data.col(i)).norm() << endl;
    else os << net.foreprop(data.col(i)).transpose() << endl;
  }
}
void FEMDDPPlotter::writeDMPPolicyAscii(const string& name,const string& path,bool norm,sizeType res) const
{
  scalar dt=_ddp.getEngine()._tree.get<scalar>("dt")/(scalar)res;
  sizeType horizon=_ddp._tree.get<sizeType>("horizon");
  Matd data=Matd::Zero(1,horizon*res);
  for(sizeType i=0; i<data.cols(); i++)
    data(0,i)=(scalar)i*dt;
  writeDMPStateAscii(*(_ddp.getEnv().getPolicy()._net),data,name,path,norm);
}
//misc.
void FEMDDPPlotter::writeControlMaxima(const string name,const string& path) const
{
  checkMaximaPath(path);
  Matd uss=collectControlData();
  Vec tss=collectTimeData();

  //write data
  boost::filesystem::ofstream os(path);
  for(sizeType r=0; r<uss.rows(); r++) {
    string nameU(name+boost::lexical_cast<string>(r));
    writeMaxima(os,nameU,&tss,uss.row(r).transpose());
  }
}
void FEMDDPPlotter::writeControlAscii(const string& name,const string& path) const
{
  Matd uss=collectControlData();
  Vec tss=collectTimeData();

  //write title
  boost::filesystem::ofstream os(path);
  os << "t ";
  for(sizeType r=0; r<uss.rows(); r++)
    os << name << r << " ";
  os << endl;

  //write data
  for(sizeType i=0; i<uss.cols(); i++) {
    os << tss[i] << " ";
    for(sizeType r=0; r<uss.rows(); r++)
      os << uss(r,i) << " ";
    os << endl;
  }
}
void FEMDDPPlotter::writeMaximaScript(const string& stateName,const string& actionName,const string& path,bool force) const
{
  if(!force && exists(path))
    return;
  sizeType nrU=_ddp.nrX()/2-6;
  boost::filesystem::ofstream os(path);
  beginMaxima(os);
  //cmd1
  beginMaximaCmd(os);
  os << "load(\"" << stateName << ".mac" << "\");" << endl;
  endMaximaCmd(os);
  //cmd2
  beginMaximaCmd(os);
  os << "load(\"" << actionName << ".mac" << "\");" << endl;
  endMaximaCmd(os);
  //cmd3
  beginMaximaCmd(os);
  os << "plot2d([";
  //write plot code
  for(sizeType i=0; i<nrU; i++) {
    os << "[discrete," << stateName << boost::lexical_cast<string>(i) << "]";
    os << "," << endl;
  }
  for(sizeType i=0; i<nrU; i++) {
    os << "[discrete," << actionName << boost::lexical_cast<string>(i) << "]";
    if(i < nrU-1)
      os << "," << endl;
  }
  //write legend
  os << "]," << endl << "[legend,";
  for(sizeType i=0; i<nrU; i++)
    os << '\"' << stateName << boost::lexical_cast<string>(i) << '\"' << ",";
  os << endl;
  for(sizeType i=0; i<nrU; i++) {
    os << '\"' << actionName << boost::lexical_cast<string>(i) << '\"';
    if(i < nrU-1)
      os << ",";
  }
  //write style
  os << "]," << endl << "[style,";
  for(sizeType i=0; i<nrU; i++)
    os << "points,";
  os << endl;
  for(sizeType i=0; i<nrU; i++) {
    os << "lines";
    if(i < nrU-1)
      os << ",";
  }
  os << "]);" << endl;
  endMaximaCmd(os);
  endMaxima(os);
}
Matd FEMDDPPlotter::collectControlData() const
{
  sizeType horizon=_ddp._tree.get<sizeType>("frm");
  Matd ret=Matd::Zero(_ddp.nrU(),horizon);
  for(sizeType i=0; i<horizon; i++)
    ret.col(i)=parseVec<Vec>("u",i,_ddp._tree);
  return ret;
}
FEMDDPPlotter::Vec FEMDDPPlotter::collectTimeData() const
{
  scalarD dt=_ddp._tree.get<scalar>("dt",1);
  sizeType horizon=_ddp._tree.get<sizeType>("frm");
  Vec ret=Vec::Zero(horizon);
  for(sizeType i=0; i<horizon; i++)
    ret[i]=dt*(scalarD)i;
  return ret;
}
//static
void checkMaximaPath(const string& path)
{
  endsWith(path,".mac");
}
void writeMaxima(ostream& os,const string& name,const FEMDDPPlotter::Vec* tss,const FEMDDPPlotter::Vec& uss)
{
  os << name << ": [";
  for(sizeType i=0; i<uss.size(); i++) {
    if(i > 0)
      os << ",";
    os << "[" << (tss ? (*tss)[i] : i) << "," << uss[i] << "]";
  }
  os << "];" << endl;
}
void beginMaxima(ostream& os)
{
  os << "/* [wxMaxima batch file version 1] [ DO NOT EDIT BY HAND! ]*/" << endl;
  os << "/* [ Created with wxMaxima version 13.04.2 ] */" << endl << endl;
}
void endMaxima(ostream& os)
{
  os << "/* Maxima can't load/batch files which end with a comment! */" << endl;
  os << "\"Created with wxMaxima\"$" << endl;
}
void beginMaximaCmd(ostream& os)
{
  os << "/* [wxMaxima: input   start ] */" << endl;
}
void endMaximaCmd(ostream& os)
{
  os << "/* [wxMaxima: input   end   ] */" << endl << endl;
}

PRJ_END
