#include "FEMEnvironmentMT.h"
#include "FEMEnvironmentDQNObstacle.h"
#include "FEMFeature.h"
#include "DDPEnergy.h"
#include <Dynamics/FEMCIOEngine.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

FEMEnvironmentMT::FEMEnvironmentMT()
  :FEMEnvironment(typeid(FEMEnvironmentMT).name()),_sampleAllTask(false) {}
bool FEMEnvironmentMT::read(istream& is,IOData* dat)
{
  DDPEnergy::registerType(dat);
  sizeType k;
  readBinaryData(k,is);
  if(k <= 0)
    _essMT.clear();
  else {
    _essMT.resize(k);
    for(sizeType i=0; i<k; i++)
      readVector(_essMT[i],is,dat);
  }
  FEMEnvironment::read(is,dat);
  return is.good();
}
bool FEMEnvironmentMT::write(ostream& os,IOData* dat) const
{
  sizeType k=K();
  writeBinaryData(k,os);
  for(sizeType i=0; i<k; i++)
    writeVector(_essMT[i],os,dat);
  FEMEnvironment::write(os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> FEMEnvironmentMT::copy() const
{
  return boost::shared_ptr<Serializable>(new FEMEnvironmentMT);
}
void FEMEnvironmentMT::reset(const boost::property_tree::ptree& pt)
{
  FEMEnvironment::reset(pt);
  //try read discrete multitask
  for(sizeType i=0;; i++) {
    boost::optional<const boost::property_tree::ptree&> taskEnergyI=
      pt.get_child_optional("taskEnergy"+boost::lexical_cast<string>(i));
    if(!taskEnergyI)
      break;
    //add energy
    _ess.clear();
    addEnergyBalance(taskEnergyI.get_ptr());
    addEnergyJump(taskEnergyI.get_ptr());
    addEnergyWalk(taskEnergyI.get_ptr());
    addEnergyWalkTo(taskEnergyI.get_ptr());
    addEnergy2DRoll(taskEnergyI.get_ptr());
    addEnergy3DOrientation(taskEnergyI.get_ptr());
    addEnergy3DOrientationTo(taskEnergyI.get_ptr());
    addEnergyCurvedWalk(taskEnergyI.get_ptr());
    _essMT.push_back(_ess);
  }
  _sol->_tree.put<sizeType>("multitask",(sizeType)_essMT.size());
  ASSERT_MSG(_essMT.size() != 1,"SingleTask energy specified, use FEMEnvironment instead!")
}
sizeType FEMEnvironmentMT::K() const
{
  sizeType mtK=_sol->_tree.get<sizeType>("multitask",-1);
  if(mtK == (sizeType)_essMT.size())
    return mtK;
  else return -1;
}
void FEMEnvironmentMT::setK(sizeType k)
{
  ASSERT(k >= 0 && k < K())
  Vec aug=Vec::Zero(1);
  aug[0]=(scalarD)k;
  getPolicy().setAug(aug);
  _ess=_essMT[k];
}
NeuralNetPolicy& FEMEnvironmentMT::getPolicy()
{
  if(!_policy) {
    //add multitask
    sizeType nrS=_sol->getMCalc().size(),nrU=nrS-6;
    sizeType nrDMP=_sol->_tree.get<sizeType>("DMPBasis",0);
    _sol->_tree.put<sizeType>("multitask",K());
    //create neural network
    if(nrDMP > 0) {
      ASSERT(K() > 1)
      if(_sol->_tree.get<bool>("inNeuralNetDebug",false))
        _policy.reset(new NeuralNetPolicyPositiveCov(0,0,0,1,nrU,NULL,&(_sol->_tree)));
      else _policy.reset(new NeuralNetPolicyPositiveCov(nrU,nrDMP,&(_sol->_tree)));
      //disable exploration
      getPolicy().setActionCov(Vec::Zero(nrU));
    } else {
      ASSERT(K() <= 0)
      scalar dt=_sol->_tree.get<scalar>("dt");
      sizeType nrH1=_sol->_tree.get<sizeType>("nrH1",40);
      sizeType nrH2=_sol->_tree.get<sizeType>("nrH2",40);
      sizeType nrH3=_sol->_tree.get<sizeType>("nrH3",0);
      boost::shared_ptr<FeatureTransformLayer> feature,featureConst(new NoFeatureTransformLayer(nrMTAug()));
      if(_sol->_tree.get<bool>("useRSFeature",true))
        feature.reset(new FEMRSFeatureTransformLayer(nrS*2,dt,_sol->getBody().dim(),false));
      boost::shared_ptr<FeatureAugmentedInPlace> featureAug(new FeatureAugmentedInPlace(feature,featureConst));
      boost::shared_ptr<Vec> scale;
      if(_sol->_tree.get_optional<scalar>("scaleAction"))
        scale.reset(new Vec(Vec::Constant(nrU,_sol->_tree.get<scalar>("scaleAction"))));
      _policy.reset(new NeuralNetPolicyPositiveCov(nrH1,nrH2,nrH3,nrS*2+nrMTAug(),nrU,scale.get(),&(_sol->_tree),featureAug));
      //encourage exploration
      scalar confidence=_sol->_tree.get<scalar>("confidence",0.9f);
      getPolicy().setActionCov(0.5f,confidence);
    }
  }
  NeuralNet::registerNewLayerType(boost::shared_ptr<Serializable>(new FEMRSFeatureTransformLayer));
  NeuralNet::registerNewLayerType(boost::shared_ptr<Serializable>(new ConstFeatureTransformLayer));
  return dynamic_cast<NeuralNetPolicy&>(*_policy);
}
void FEMEnvironmentMT::debugDMP(const string& path,sizeType nrFrm,scalar dt)
{
  boost::shared_ptr<DMPNeuralNet> net=boost::dynamic_pointer_cast<DMPNeuralNet>(getPolicy()._net);
  if(net)
    net->debugOutput(path,nrFrm,dt);
}
//upgrade to DQN learnable FEMEnvironment
boost::shared_ptr<FEMEnvironmentDQN> FEMEnvironmentMT::upgradeDQN(const boost::property_tree::ptree* pt)
{
  boost::shared_ptr<DMPNeuralNet> net=boost::dynamic_pointer_cast<DMPNeuralNet>(getPolicy()._net);
  return boost::shared_ptr<FEMEnvironmentDQN>(new FEMEnvironmentDQN(_sol,*net,pt));
}
boost::shared_ptr<FEMEnvironmentDQNObstacle> FEMEnvironmentMT::upgradeDQNObstacle(const boost::property_tree::ptree* pt)
{
  boost::shared_ptr<DMPNeuralNet> net=boost::dynamic_pointer_cast<DMPNeuralNet>(getPolicy()._net);
  return boost::shared_ptr<FEMEnvironmentDQNObstacle>(new FEMEnvironmentDQNObstacle(_sol,*net,pt));
}
//multitask objective function
scalarD FEMEnvironmentMT::getCostMT(sizeType i,const Vec& state,const Vec&,Vec* DRDA,Matd* DDRDDA,sizeType K) const
{
  const vector<boost::shared_ptr<DDPEnergy> >& ess=_essMT.at(K);
  scalar dt=_sol->_tree.get<scalar>("dt");
  scalarD ret=0;
  if(DRDA)DRDA->setZero(state.size());
  if(DDRDDA)DDRDDA->setZero(state.size(),state.size());
  for(sizeType e=0; e<(sizeType)ess.size(); e++)
    ret+=ess[e]->objX(i,dt,state,DRDA,DDRDDA);
  return ret;
}
FEMEnvironmentMT::Vec FEMEnvironmentMT::getCostFMT(sizeType i,const Vec& state,const Vec&,Matd* FJac,sizeType K) const
{
  const vector<boost::shared_ptr<DDPEnergy> >& ess=_essMT.at(K);
  scalar dt=_sol->_tree.get<scalar>("dt");
  scalarD nrF=nrCostFMT(K);
  Vec F=Vec::Zero(nrF);
  if(FJac)FJac->setZero(nrF,state.size());
  //compute
  nrF=0;
  for(sizeType e=0; e<(sizeType)ess.size(); e++) {
    ess[e]->objXF(i,dt,state,F,FJac,nrF);
    nrF+=ess[e]->nrF();
  }
  return F;
}
sizeType FEMEnvironmentMT::nrCostFMT(sizeType K) const
{
  const vector<boost::shared_ptr<DDPEnergy> >& ess=_essMT.at(K);
  sizeType nrF=0;
  for(sizeType i=0; i<(sizeType)ess.size(); i++)
    nrF+=ess[i]->nrF();
  return nrF;
}
//invalidate singletask objective function
scalarD FEMEnvironmentMT::getCost(sizeType i,const Vec& state,const Vec& A,Vec* DRDA,Matd* DDRDDA) const
{
  //ASSERT_MSG(false,"this is singletask version, use getCostMT!")
  return FEMEnvironment::getCost(i,state,A,DRDA,DDRDDA);
}
FEMEnvironmentMT::Vec FEMEnvironmentMT::getCostF(sizeType i,const Vec& state,const Vec& A,Matd* FJac) const
{
  //ASSERT_MSG(false,"this is singletask version, use getCostFMT!")
  return FEMEnvironment::getCostF(i,state,A,FJac);
}
sizeType FEMEnvironmentMT::nrCostF() const
{
  ASSERT_MSG(false,"this is singletask version, use nrCostFMT!")
  return FEMEnvironment::nrCostF();
}
void FEMEnvironmentMT::copyToTask(sizeType id)
{
  _essMT.at(id)=_ess;
  clearEnergy();
}
void FEMEnvironmentMT::setState(const Vec& state)
{
  FEMEnvironment::setState(state);
  if(K() <= 0) {
    sizeType nrS=_sol->getMCalc().size();
    setMTAug(state.segment(nrS*2,nrMTAug()));
  }
}
//multitask augmented feature
void FEMEnvironmentMT::clearAugData()
{
  ASSERT(K() <= 0)
  _augs=NULL;
}
void FEMEnvironmentMT::setupAugDataInterp(const Vec& aug0,const Vec& aug1,sizeType nrTraj)
{
  ASSERT(K() <= 0)
  _augs.reset(new Matd(aug0.size(),nrTraj));
  for(sizeType i=0; i<nrTraj; i++)
    _augs->col(i)=interp1D(aug0,aug1,(scalar)i/(scalar)(nrTraj-1));
}
void FEMEnvironmentMT::setupSampleAllTask(bool sampleAllTask)
{
  _sampleAllTask=sampleAllTask;
}
void FEMEnvironmentMT::setTrajId(sizeType trajId)
{
  if(K() <= 0 && _augs) {
    ASSERT_MSGV(trajId >= 0 && trajId < (sizeType)_augs->size(),
                "trajId=%ld out-of-bound, #AugDataArray=%ld!",
                trajId,(sizeType)_augs->cols())
    setMTAug(_augs->col(trajId));
  } else if(K() > 1 && _sampleAllTask) {
    setK(trajId%K());
  }
}
void FEMEnvironmentMT::setMTAug(const Vec& aug)
{
  sizeType off=0;
  ASSERT(K() <= 0)
  for(sizeType i=0; i<(sizeType)_ess.size(); i++) {
    _ess[i]->setMTAug(aug,off);
    off+=_ess[i]->nrMTAug();
  }
}
void FEMEnvironmentMT::getMTAug(Vec& aug) const
{
  sizeType off=0;
  ASSERT(K() <= 0)
  for(sizeType i=0; i<(sizeType)_ess.size(); i++) {
    _ess[i]->getMTAug(aug,off);
    off+=_ess[i]->nrMTAug();
  }
}
sizeType FEMEnvironmentMT::nrMTAug() const
{
  sizeType ret=0;
  ASSERT(K() <= 0)
  for(sizeType i=0; i<(sizeType)_ess.size(); i++)
    ret+=_ess[i]->nrMTAug();
  return ret;
}
//helper
FEMEnvironmentMT::FEMEnvironmentMT(const string& name):FEMEnvironment(name),_sampleAllTask(false) {}
FEMEnvironmentMT::Vec FEMEnvironmentMT::extractState() const
{
  if(K() <= 0) {
    Vec aug=Vec::Zero(nrMTAug());
    getMTAug(aug);
    return concat(FEMEnvironment::extractState(),aug);
  } else return FEMEnvironment::extractState();
}
