#include "PolicyGradient.h"
#include "MLUtil.h"
#include <Dynamics/FEMUtils.h>
#include <CommonFile/solvers/PMinresQLP.h>
#include <CommonFile/Timing.h>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//Expectation with Baseline
void BaselineExpectation::reset(sizeType nrParam)
{
  _sumPossLOG.setZero(nrParam);
  _sumPossLOGReward.setZero(nrParam);
  _sumPossLOGNum.setZero(nrParam);
  _sumPossLOGDenom.setZero(nrParam);
  _size=0;
}
void BaselineExpectation::feedNewData(const Vec& possLOG,scalarD reward)
{
  _sumPossLOG+=possLOG;
  _sumPossLOGReward+=possLOG*reward;
  _sumPossLOGNum+=possLOG.array().square().matrix()*reward;
  _sumPossLOGDenom+=possLOG.array().square().matrix();
  _size++;
}
BaselineExpectation::Vec BaselineExpectation::calcExpectation(bool baseline)
{
  if(baseline) {
    Vec denom=_sumPossLOGDenom.cwiseMax(1E-10f);
    Eigen::DiagonalMatrix<scalarD,-1,-1> baselineDiag;
    baselineDiag.diagonal()=(_sumPossLOGNum.array()/denom.array()).matrix();
    return (_sumPossLOGReward-baselineDiag*_sumPossLOG)/(scalarD)_size;
  } else {
    return _sumPossLOGReward/(scalarD)_size;
  }
}
sizeType BaselineExpectation::size() const
{
  return _size;
}
//this implement the REINFORCE algorithm
PolicyGradientREINFORCE::PolicyGradientREINFORCE(Environment& env):_env(env) {}
PolicyGradientREINFORCE::~PolicyGradientREINFORCE() {}
void PolicyGradientREINFORCE::testGradientConvergence()
{
  //sample
  sizeType nrTraj=_tree.get<sizeType>("nrTraj",100);
  sizeType trajLen=_tree.get<sizeType>("trajLen",100);
  sizeType maxIter=_tree.get<sizeType>("maxIter",1E6);
  scalarD thres=_tree.get<scalarD>("gradientAngleThres",1E-7f);
  //estimate gradient
  _dat.clear();
  resizeSufficientStatistics();
  vector<scalarD> angleErrs(1,0);
  vector<Vec,Eigen::aligned_allocator<Vec> > grads;
  while(_dat.nrTraj() < maxIter) {
    _dat.randomSample(nrTraj,nrTraj,trajLen,trajLen,_env);
    grads.push_back(updatePolicyGradient());
    if((sizeType)grads.size() > 1) {
      const Vec& a=grads[grads.size()-1];
      const Vec& b=grads[grads.size()-2];
      scalarD angleErr=1-a.dot(b)/a.norm()/b.norm();
      INFOV("NrTraj: %ld LogAngleErr: %f Norm: %f",_dat.nrTraj(),std::log10(angleErr),grads.back().norm())
      angleErrs.push_back(angleErr);
      if(abs(angleErr) < thres)
        break;
    }
  }
  //write angle
  string path=createCheckoutPath();
  boost::filesystem::ofstream os(path+"/policyGradientConvergence.txt");
  os << "Iter LogAngleErr Norm" << endl;
  for(sizeType it=0; it<(sizeType)angleErrs.size(); it++) {
    os << it << " ";
    os << std::log10(angleErrs[it]) << " ";
    os << grads[it].norm() << endl;
  }
}
void PolicyGradientREINFORCE::learn()
{
  //checkpoint path
  string path=createCheckoutPath();
  sizeType maxIter=_tree.get<sizeType>("maxIter",1E6);
  for(sizeType it=0; it<maxIter; it++) {
    if(!iter(it))
      break;
    //profile
    const Vec3d info=record("iter"+boost::lexical_cast<string>(it));
    INFOV("Training Iteration %ld, minReward: %f, maxReward: %f, meanReward: %f",it,info[0],info[1],info[2])
    //checkpoint
    if(it%_tree.get<sizeType>("checkpointInterval",1) == 0) {
      _tree.put<sizeType>("currIter",it);
      writePtreeAscii(_tree,path+"/iter"+boost::lexical_cast<string>(it)+".xml");
      _env.Environment::write(path+"/iter"+boost::lexical_cast<string>(it)+".env");
      boost::filesystem::ofstream os(path+"/profile.txt");
      os << "Iter MinReward MaxReward MeanReward" << endl;
      for(sizeType i=0; i<=it; i++) {
        os << i << " ";
        os << _tree.get<scalarD>("iter"+boost::lexical_cast<string>(i)+".min") << " ";
        os << _tree.get<scalarD>("iter"+boost::lexical_cast<string>(i)+".max") << " ";
        os << _tree.get<scalarD>("iter"+boost::lexical_cast<string>(i)+".mean") << endl;
      }
    }
  }
}
bool PolicyGradientREINFORCE::iter(sizeType it)
{
  sizeType interval=_tree.get<sizeType>("interval",5);
  sizeType trajLen=_tree.get<sizeType>("trajLen",100);
  sizeType nrTraj=_tree.get<sizeType>("nrTraj",100);
  //estimate gradient
  _dat.clear();
  resizeSufficientStatistics();
  _dat.randomSample(nrTraj,nrTraj,trajLen,trajLen,_env,NULL,"",interval);
  Vec grad=updatePolicyGradient();
  //update parameter
  scalar ss=stepSize(it);
  _tree.put<scalar>("stepSize"+boost::lexical_cast<string>(it),ss);
  if(ss == 0)
    return false;
  _env.getPolicy().fromVec(_env.getPolicy().toVec()+grad*ss);
  return true;
}
scalar PolicyGradientREINFORCE::stepSize(sizeType it) const
{
  sizeType stageSize=_tree.get<sizeType>("stageSize",1);
  scalar rate=_tree.get<scalar>("learningRate",0.00025f);
  scalar alphaThres=_tree.get<scalar>("alphaThres",1E-10f);
  scalar alphaCoef=_tree.get<scalar>("alphaCoef",0.99f);
  scalar ret=rate*pow(alphaCoef,(int)(it/stageSize));
  return ret < alphaThres ? 0 : ret;
}
scalarD PolicyGradientREINFORCE::reward(const TrajectoryState& s0) const
{
  scalarD discountCoef=_tree.get<scalar>("discountCoef",0.99f);
  return TrainingData::reward(s0,discountCoef);
}
Vec3d PolicyGradientREINFORCE::record(const string& name)
{
  //profiling
  Vec3d ret=Vec3d(ScalarUtil<scalarD>::scalar_max,-ScalarUtil<scalarD>::scalar_max,0);
  for(sizeType t=0; t<(sizeType)_dat.nrTraj(); t++) {
    scalarD R=reward(*(_dat.traj(t)));
    _tree.put<scalar>(name+".reward"+boost::lexical_cast<string>(t),R);
    ret[0]=min(ret[0],R);
    ret[1]=max(ret[1],R);
    ret[2]+=R;
  }
  ret[2]/=(scalarD)_dat.nrTraj();
  _tree.put<scalar>(name+".min",ret[0]);
  _tree.put<scalar>(name+".max",ret[1]);
  _tree.put<scalar>(name+".mean",ret[2]);
  return ret;
}
string PolicyGradientREINFORCE::createCheckoutPath()
{
  const string path=_tree.get<string>("checkpointPath","./Checkpoints");
  if(_tree.get<bool>("resetCheckpoints",true))
    recreate(path);
  else create(path);
  return path;
}
PolicyGradientREINFORCE::Vec PolicyGradientREINFORCE::updatePolicyGradient()
{
  sizeType nrTraj=_dat.nrTraj();
  for(sizeType i=_suffStat.size(); i<nrTraj; i++) {
    Vec gradStep,possLOG=Vec::Zero(_env.getPolicy().nrParam());
    const TrajectoryState* last=_dat.traj(i);
    sizeType t=0;
    for(; last && !last->_actions.empty(); t++) {
      _env.getPolicy().getAction(last->_data,NULL,&gradStep,&(last->_actions[0]._data));
      possLOG+=gradStep;
      last=&(last->_actions[0]._next);
    }
    _possLOGGrad.push_back(possLOG);
    _suffStat.feedNewData(possLOG,reward(*(_dat.traj(i))));
  }
  return _suffStat.calcExpectation(_tree.get<bool>("useBaseline",true));
}
void PolicyGradientREINFORCE::resizeSufficientStatistics()
{
  _possLOGGrad.clear();
  _suffStat.reset(_env.getPolicy().nrParam());
}
//this implement the GPOMDP algorithm
PolicyGradientGPOMDP::PolicyGradientGPOMDP(Environment& env):PolicyGradientREINFORCE(env) {}
PolicyGradientGPOMDP::Vec PolicyGradientGPOMDP::updatePolicyGradient()
{
  sizeType nrTraj=_dat.nrTraj();
  for(sizeType i=_suffStatStaged[0].size(); i<nrTraj; i++) {
    sizeType t=0;
    Vec gradStep,possLOG=Vec::Zero(_env.getPolicy().nrParam());
    scalarD discount=1,discountCoef=_tree.get<scalar>("discountCoef",0.99f);
    const TrajectoryState* last=_dat.traj(i);
    for(; last && !last->_actions.empty(); t++) {
      _env.getPolicy().getAction(last->_data,NULL,&gradStep,&(last->_actions[0]._data));
      possLOG+=gradStep;
      _suffStatStaged[t].feedNewData(possLOG,last->_actions[0]._reward*discount);
      discount*=discountCoef;
      last=&(last->_actions[0]._next);
    }
    ASSERT(t == (sizeType)_suffStatStaged.size())
    _possLOGGrad.push_back(possLOG);
  }
  Vec ret=Vec::Zero(_env.getPolicy().nrParam());
  for(sizeType t=0; t<(sizeType)_suffStatStaged.size(); t++)
    ret+=_suffStatStaged[t].calcExpectation(_tree.get<bool>("useBaseline",true));
  return ret;
}
void PolicyGradientGPOMDP::resizeSufficientStatistics()
{
  _possLOGGrad.clear();
  sizeType trajLen=_tree.get<sizeType>("trajLen",100);
  _suffStatStaged.resize(trajLen);
  for(sizeType i=0; i<trajLen; i++)
    _suffStatStaged[i].reset(_env.getPolicy().nrParam());
}
//this implement the eNAC algorithm
PolicyGradientNAC::PolicyGradientNAC(Environment& env):PolicyGradientREINFORCE(env) {}
PolicyGradientNAC::Vec PolicyGradientNAC::updatePolicyGradient()
{
  PMINRESSolverQLP<scalarD> sol;
  Vec grad=PolicyGradientREINFORCE::updatePolicyGradient(),delta=Vec::Zero(grad.size());
  //sol.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> >));
  sol.setKrylovMatrix(getHessian());
  sol.solve(grad,delta);
  //INFOV("Angle: %f, Grad Norm: %f, Delta Norm: %f",grad.dot(delta)/grad.norm()/delta.norm(),grad.norm(),delta.norm())
  return delta/sqrt(grad.dot(delta));
}
boost::shared_ptr<KrylovMatrix<scalarD> > PolicyGradientNAC::getHessian()
{
  if(_tree.get<bool>("useAnalyticalHessian",true)) {
    Matd states,actions;
    _dat.assemble(states,actions);
    return _env.getPolicy().getHessKLDistance(states);
  } else {
    boost::shared_ptr<DenseEigenKrylovMatrix<scalarD> > ret(new DenseEigenKrylovMatrix<scalarD>);
    sizeType nrParam=_env.getPolicy().nrParam();
    ret->_fixedMatrix.setZero(nrParam,nrParam);
    for(sizeType i=0; i<(sizeType)_possLOGGrad.size(); i++)
      ret->_fixedMatrix+=_possLOGGrad[i]*_possLOGGrad[i].transpose();
    ret->_fixedMatrix/=(scalarD)_possLOGGrad.size();
    return boost::dynamic_pointer_cast<KrylovMatrix<scalarD> >(ret);
  }
}
