#ifndef NEURAL_NET_POLICY_H
#define NEURAL_NET_POLICY_H

#include "NeuralNet.h"
#include <CommonFile/solvers/KrylovMatrix.h>
#include <boost/property_tree/ptree_fwd.hpp>

PRJ_BEGIN

//Policy Interface
struct Transition;
template <typename T> class ParallelVector;
struct Policy : public Serializable {
  typedef Cold Vec;
  typedef ParallelVector<Eigen::Triplet<scalarD,sizeType> > TRIPS;
  typedef Eigen::SparseMatrix<scalarD,0,sizeType> SMat;
  Policy(const string& name);
  virtual scalarD QLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec* grad,TRIPS* hess) const;
  virtual scalarD QLossM(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec& grad,SMat& hessM) const;
  virtual scalarD QLossMMapped(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to,Vec& grad,SMat& hessM,map<sizeType,sizeType>& MMap) const;
  virtual void optimizeQLoss(const vector<Transition>& trans,scalarD gamma,sizeType from,sizeType to) const;
  virtual Vec getAction(const Vec& output,scalarD* possLog,Vec* possLogGrad,const Vec* samples=NULL);
  virtual Matd getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples=NULL)=0;
  virtual boost::shared_ptr<KrylovMatrix<scalarD> > getHessKLDistance(const Matd& states) const;
  virtual scalarD getDiffKLDistance(const Matd& states,const Vec& paramOld,Vec* grad);
  virtual void setActionCov(scalar coef,scalar confidence);
  virtual void setActionCov(const Vec& cov);
  virtual void setEps(scalarD eps);
  virtual void updateQTarget();
  virtual void setAug(const Vec& aug);
  virtual sizeType nrParam() const=0;
  virtual Vec toVec() const=0;
  virtual void fromVec(const Vec& weights)=0;
  virtual Vec toVec(const map<sizeType,sizeType>* MMap) const;
  virtual void fromVec(const Vec& weights,const map<sizeType,sizeType>* MMap);
  virtual void setDeterministic(bool deterministic);
protected:
  bool _deterministic;
};
//NeuralNet Policy Wrapper
struct NeuralNetPolicy : public Policy {
  NeuralNetPolicy();
  NeuralNetPolicy(sizeType nrAction,sizeType nrBasis,boost::property_tree::ptree* pt=NULL);
  NeuralNetPolicy(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,sizeType nrAction,const Vec* scale,
                  boost::property_tree::ptree* pt=NULL,boost::shared_ptr<FeatureTransformLayer> feature=NULL);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual Vec getActionScale() const;
  virtual Matd getAction(const Matd& output,Vec* possLog,Vec* possLogGrad,Vec* weightPoss,const Matd* samples=NULL) override;
  virtual boost::shared_ptr<KrylovMatrix<scalarD> > getHessKLDistance(const Matd& states) const override;
  virtual scalarD getDiffKLDistance(const Matd& states,const Matd& actionsOld,const Vec& covOld,Vec* grad) const;
  virtual scalarD getDiffKLDistance(const Matd& states,const Vec& paramOld,Vec* grad) override;
  virtual void setActionCov(scalar coef,scalar confidence);
  virtual void setActionCov(const Vec& cov);
  virtual void setAug(const Vec& aug) override;
  virtual Vec getAug() const;
  virtual sizeType nrNetParam() const;
  virtual sizeType nrParam() const;
  virtual sizeType nrAction() const;
  virtual Vec toVec() const;
  virtual void fromVec(const Vec& weights);
  //feature transformation
  virtual sizeType nrState() const;
  boost::shared_ptr<NeuralNet> _net;
protected:
  virtual Vec cov() const;
  virtual Vec dcov() const;
  virtual scalarD cov(sizeType i) const;
  virtual scalarD dcov(sizeType i) const;
  NeuralNetPolicy(const string& name);
  Vec _actionCov,_aug;
};
struct NeuralNetPolicyPositiveCov : public NeuralNetPolicy {
  NeuralNetPolicyPositiveCov();
  NeuralNetPolicyPositiveCov(sizeType nrAction,sizeType nrBasis,boost::property_tree::ptree* pt=NULL);
  NeuralNetPolicyPositiveCov(sizeType nrH1,sizeType nrH2,sizeType nrH3,sizeType nrState,sizeType nrAction,const Vec* scale,
                             boost::property_tree::ptree* pt=NULL,boost::shared_ptr<FeatureTransformLayer> feature=NULL);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void setActionCov(const Vec& cov);
  virtual void fromVec(const Vec& weights);
protected:
  virtual scalarD cov(sizeType i) const;
  virtual scalarD dcov(sizeType i) const;
  NeuralNetPolicyPositiveCov(const string& name);
  Vec _actionCov0;
};

PRJ_END

#endif
