#include "QPInterface.h"
#include <Dynamics/FEMUtils.h>
#include <Dynamics/AlglibToEigen.h>
#include <CommonFile/solvers/alglib/optimization.h>

USE_PRJ_NAMESPACE

//minqp interface
QPInterface::QPInterface() {}
void QPInterface::reset(sizeType sz,bool hasCI)
{
  if(sz <= 0) {
    _sz=sz;
    _state=NULL;
    return;
  }
  if(_sz == sz && _hasCI == hasCI && _state)
    return;
  _sz=sz;
  _hasCI=hasCI;
  _state.reset(new alglib::minqpstate);
  alglib::minqpcreate(sz,*_state);
  if(_hasCI)
    alglib::minqpsetalgobleic(*_state,_tolg,_tolf,_tolx,_maxIter);
  else alglib::minqpsetalgoquickqp(*_state,_tolg,_tolf,_tolx,_maxIter,true);
}
void QPInterface::setDense(const Matd& H,const Cold& G)
{
  ASSERT(H.rows() == _sz && H.cols() == _sz && G.size() == _sz)
  alglib::minqpsetlinearterm(*_state,toAlglib(G));
  alglib::minqpsetquadraticterm(*_state,toAlglib(H));
}
void QPInterface::setCI(const Matd& CI,const Cold& CI0,const Coli& type)
{
  ASSERT(CI.rows() == CI0.size() && CI.cols() == _sz);
  ASSERT_MSG(_hasCI,"QuickQP does not support linear constraints!")
  Matd CII0=Matd::Zero(CI.rows(),CI.cols()+1);
  CII0.block(0,0,CI.rows(),CI.cols())=CI;
  CII0.col(CI.cols())=-CI0;

  alglib::integer_1d_array typeAlglib;
  typeAlglib.setcontent(type.size(),type.cast<alglib::ae_int_t>().eval().data());
  alglib::minqpsetlc(*_state,toAlglib(CII0),typeAlglib);
}
void QPInterface::setCB(const Cold& L,const Cold& U)
{
  ASSERT(L.size() == _sz && U.size() == _sz)
  alglib::minqpsetbc(*_state,toAlglib(L),toAlglib(U));
}
bool QPInterface::solve(Cold& x)
{
  ASSERT_MSG(x.size() == _sz && _state,"Setup solver first!")
  alglib::real_1d_array xAlglib;
  alglib::minqpsetstartingpoint(*_state,toAlglib(x));
  alglib::minqpoptimize(*_state);
  _rep.reset(new alglib::minqpreport);
  alglib::minqpresults(*_state,xAlglib,*_rep);
  x=fromAlglib(xAlglib);
  //INFOV("Termination type: %ld",_rep->terminationtype)
  return _rep->terminationtype <= 4 && _rep->terminationtype >= 1;
}
bool QPInterface::solvePrimal(const Matd& H,const Cold& G,const Matd& CI,const Cold* CI0,Cold& x)
{
  reset(x.size(),true);
  setDense(H,G);
  CommonInterface::setCI(CI,CI0?*CI0:Cold::Zero(CI.rows()),sizeType(1));
  return solve(x);
}
bool QPInterface::solveDual(const Matd& H,const Cold& G,const Matd& CI,const Cold* CI0,Cold& x)
{
  //solve dual problem:
  //minimize  l^T*CI*H^{-1}*CI^T*l/2 + (CI0-CI*H^{-1}G)^T*l  such that l >= 0
  //then set  x=H^{-1}*(CI^T*l-G) aka. H*x+G-CI^T*l=0
  bool ret=true;
  Eigen::LDLT<Matd> invH=H.ldlt();
  if(CI.rows() > 0) {
    if(_sz != CI.rows() || _hasCI != false) {
      //try to be lazy
      reset(CI.rows(),false);
      setCLB(Cold::Zero(CI.rows()));
    }
    Matd invHCIT=invH.solve(CI.transpose());
    Cold invHG=invH.solve(G);
    if(CI0)
      setDense(CI*invHCIT,*CI0-CI*invHG);
    else setDense(CI*invHCIT,-CI*invHG);
    Cold lambda=Cold::Zero(CI.rows());
    ret=solve(lambda);
    x=invHCIT*lambda-invHG;
  } else {
    x=invH.solve(-G);
  }
  return ret;
}
#ifdef EPS
#undef EPS
#endif
#include <qpOASES/SQProblem.hpp>
template <typename T>
struct CallAQP {
  typedef Eigen::Matrix<T,-1,-1> MAT;
  typedef Eigen::Matrix<T,-1,1> COL;
  typedef Eigen::Matrix<T,-1,-1,Eigen::RowMajor> MATT;
  bool operator()(QPInterface& qp,const MAT& H,const COL& G,const MATT& CI,const COL* CI0,COL& x) {
    return qp.solveAQPQ(H,G,CI,CI0,x);
  }
};
#if defined(QUADMATH_SUPPORT) && defined(__GNUC__)
template <>
struct CallAQP<__float128> {
  typedef __float128 T;
  typedef Eigen::Matrix<T,-1,-1> MAT;
  typedef Eigen::Matrix<T,-1,1> COL;
  typedef Eigen::Matrix<T,-1,-1,Eigen::RowMajor> MATT;
  bool operator()(QPInterface& qp,const MAT& H,const COL& G,const MATT& CI,const COL* CI0,COL& x) {
    return qp.solveAQPD(H,G,CI,CI0,x);
  }
};
#endif
bool QPInterface::solveAQPQ(const Eigen::Matrix<double,-1,-1>& H,
                            Eigen::Matrix<double,-1,1> G,
                            const MatdoubleT& CI,
                            const Eigen::Matrix<double,-1,1>* CI0,
                            Eigen::Matrix<double,-1,1>& x)
{
  //calculate taylor
  Eigen::Matrix<double,-1,1> CI0X=-CI*x,delta=x;
  G+=H*x;
  if(CI0)
    CI0X-=*CI0;

  //resolve or warm-started solve
  ASSERT(sizeof(REFER_NAMESPACE_QPOASES real_t) == sizeof(Matd::Scalar))
  REFER_NAMESPACE_QPOASES returnValue value=REFER_NAMESPACE_QPOASES SUCCESSFUL_RETURN;
  REFER_NAMESPACE_QPOASES int_t nrWSC=1E5;
  if(!_aqp || _aqp->getNV() != H.rows() || _aqp->getNC() != CI.rows()) {
    _aqp.reset(new REFER_NAMESPACE_QPOASES SQProblem(G.size(),CI.rows()));
    REFER_NAMESPACE_QPOASES Options option=_aqp->getOptions();
    option.printLevel=REFER_NAMESPACE_QPOASES PL_NONE;
    _aqp->setOptions(option);
    value=_aqp->init(H.data(),G.data(),CI.data(),NULL,NULL,CI0X.data(),NULL,nrWSC);
  } else value=_aqp->hotstart(H.data(),G.data(),CI.data(),NULL,NULL,CI0X.data(),NULL,nrWSC);
  if(value != REFER_NAMESPACE_QPOASES SUCCESSFUL_RETURN)
    return false;

  //get solution
  value=_aqp->getPrimalSolution(delta.data());
  x+=delta;
  return value == REFER_NAMESPACE_QPOASES SUCCESSFUL_RETURN;
}
bool QPInterface::solveAQPD(const Matd& H,
                            const Cold& G,
                            const MatdT& CI,
                            const Cold* CI0,
                            Cold& x)
{
  Eigen::Matrix<double,-1,1> CI0double;
  Eigen::Matrix<double,-1,1> xdouble=x.cast<double>();
  if(CI0)
    CI0double=CI0->cast<double>();
  bool ret=solveAQPQ(H.cast<double>(),G.cast<double>(),CI.cast<double>(),
                     CI0 ? &CI0double : NULL,xdouble);
  x=xdouble.cast<scalarD>();
  return ret;
}
bool QPInterface::solveAQP(const Matd& H,const Cold& G,const MatdT& CI,const Cold* CI0,Cold& x)
{
  return CallAQP<scalarD>()(*this,H,G,CI,CI0,x);
}
sizeType QPInterface::getTermination() const
{
  return _rep->terminationtype;
}
void QPInterface::debugPrimalDual(bool cons)
{
#define NNODE 100
  Matd H=Matd::Zero(NNODE,NNODE);
  Cold G=Cold::Zero(NNODE);
  //smoothness
  for(sizeType i=0; i<NNODE-2; i++)
    H.block<3,3>(i,i)+=Vec3d(-1,2,-1)*Vec3d(-1,2,-1).transpose();
  //fix point
  H(0,0)+=1;
  G[0]+=1;
  H(NNODE-1,NNODE-1)+=1;
  G[NNODE-1]+=-1;

  //constraint
  Matd CI=Matd::Zero(2,NNODE);
  Cold CI0=Cold::Zero(2);
  CI(0,NNODE*1/4)= 1;
  CI(0,NNODE*3/4)=-1;
  CI0[0]=0.2f;
  CI(1,NNODE*1/4)=-1;
  CI(1,NNODE*3/4)= 1;
  CI0[1]=0.2f;

  if(!cons) {
    CI.setZero(0,NNODE);
    CI0.setZero(0);
  }

  //solve
  QPInterface qp;
  qp.setTolF(1E-10f);
  qp.setTolX(1E-10f);
  qp.setTolG(1E-10f);
  Cold x0=Cold::Zero(NNODE),x1=x0,x2=x0,x3=x0;
  ASSERT(qp.solvePrimal(H,G,CI,&CI0,x1)); //primal solver
  ASSERT(qp.solveDual(H,G,CI,&CI0,x2)); //dual solver
  ASSERT(qp.solveAQP(H,G,CI,&CI0,x3));  //primal-dual solver

  //solve again to test lazy functionality
  ASSERT(qp.solveDual(H,G,CI,&CI0,x2));
  INFOV("Primal Dual: %f, Error: %f",x2.norm(),(x1-x2).norm())
  INFOV("AQP Dual: %f, Err: %f",x2.norm(),(x3-x2).norm())
  writeVTK(x1,"x1.vtk");
  writeVTK(x2,"x2.vtk");
  writeVTK(x3,"x3.vtk");
#undef NNODE
}
