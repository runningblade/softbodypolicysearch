#ifndef DMP_LAYER_H
#define DMP_LAYER_H

#include "NeuralNet.h"

PRJ_BEGIN

#define GET_SET_FUNCTION(TYPE,NAME) \
const TYPE& NAME() const{return _##NAME;}  \
TYPE& NAME(){return _##NAME;}
#define GET_SET_FUNCTIOND(TYPE,NAME) \
const TYPE& d##NAME() const{return _d##NAME;}  \
TYPE& d##NAME(){return _d##NAME;}
//this is rhythmic movement primitives:
//u_i = \sum_j*exp(h_{ij}^2(cos(tau*t-mu_{ij})-1))*w_{ij}
class DMPLayer : public NeuralLayer
{
public:
  typedef Eigen::Block<Matd> MBlk;
  typedef Eigen::Block<const Matd> CMBlk;
  DMPLayer();
  DMPLayer(sizeType nrOutput,sizeType nrBasis);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual DMPLayer& operator=(const DMPLayer& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void weightprop(const Matd& x,const Matd& fx,const Matd& dEdx,Matd& dEdfx,void* dat=NULL) const override;
  virtual void augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt);
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  sizeType nrOutput(sizeType lastIn) const;
  virtual Matd getBounds(const boost::property_tree::ptree& pt) const override;
  scalarD period() const;
  //get/set function
  GET_SET_FUNCTION(Vec,wBias)
  GET_SET_FUNCTION(Matd,w)
  GET_SET_FUNCTION(Matd,h)
  GET_SET_FUNCTION(Matd,mu)
  GET_SET_FUNCTION(scalarD,tau)

  GET_SET_FUNCTIOND(Vec,wBias)
  GET_SET_FUNCTIOND(Matd,w)
  GET_SET_FUNCTIOND(Matd,h)
  GET_SET_FUNCTIOND(Matd,mu)
  GET_SET_FUNCTIOND(scalarD,tau)

  GET_SET_FUNCTION(bool,wBiasOptimizable)
  GET_SET_FUNCTION(bool,wOptimizable)
  GET_SET_FUNCTION(bool,hOptimizable)
  GET_SET_FUNCTION(bool,muOptimizable)
  GET_SET_FUNCTION(bool,tauOptimizable)
protected:
  virtual void backpropInner(CMBlk x,CMBlk fx,MBlk dEdx,CMBlk dEdfx);
  virtual void initCache(const Matd& x);
  DMPLayer(const string& name);
  //data
  Vec _wBias;
  Matd _w,_h,_mu;
  scalarD _tau;
  //diff
  Vec _dwBias;
  Matd _dw,_dh,_dmu;
  scalarD _dtau;
  //optimization options
  bool _wBiasOptimizable;
  bool _wOptimizable;
  bool _hOptimizable;
  bool _muOptimizable;
  bool _tauOptimizable;
  vector<Vec,Eigen::aligned_allocator<Vec> > _phase,_phaseCos,_act,_dEdfxRow;
};
//this is non-rhythmic movement primitives:
//u_i = \sum_j*exp(-(h_{ij}*t-mu_{ij})^2)*w_{ij}*(t-tau)
class NRDMPLayer : public DMPLayer
{
public:
  NRDMPLayer();
  NRDMPLayer(sizeType nrOutput,sizeType nrBasis);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const;
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
protected:
  virtual void backpropInner(CMBlk x,CMBlk fx,MBlk dEdx,CMBlk dEdfx);
  NRDMPLayer(const string& name);
};
//this is cos-only movement primitives:
//u_i = cos(tau*t-mu_{ij})^(h_{ij}*h_{ij})*w_{ij}
class CosDMPLayer : public DMPLayer
{
public:
  CosDMPLayer();
  CosDMPLayer(sizeType nrOutput,sizeType nrBasis);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const;
protected:
  virtual void backpropInner(CMBlk x,CMBlk fx,MBlk dEdx,CMBlk dEdfx);
  CosDMPLayer(const string& name);
};
#undef GET_SET_FUNCTION

PRJ_END

#endif
