#ifndef DMP_LAYER_MT_H
#define DMP_LAYER_MT_H

#include "DMPLayer.h"

PRJ_BEGIN

//this is multitask rhythmic movement primitives:
//u_i^k = \sum_j*exp(h_{ij}^2(cos(tau*t-mu_{ij}^k)-1))*w_{ij}
class DMPLayerMTMu : public DMPLayer
{
public:
  DMPLayerMTMu();
  DMPLayerMTMu(sizeType nrOutput,sizeType nrBasis,sizeType K);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual DMPLayerMTMu& operator=(const DMPLayerMTMu& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt);
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
  virtual void configOnline(const boost::property_tree::ptree& pt) override;
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType K() const;
  virtual Matd getBounds(const boost::property_tree::ptree& pt) const override;
private:
  DMPLayerMTMu(const string& name);
  sizeType _setToTask;
  Mss _muK,_dmuK;
};
//this is non-rhythmic movement primitives:
//u_i^k = \sum_j*exp(-(h_{ij}*t-mu_{ij}^k)^2)*w_{ij}*(t-tau)
class NRDMPLayerMTMu : public NRDMPLayer
{
public:
  NRDMPLayerMTMu();
  NRDMPLayerMTMu(sizeType nrOutput,sizeType nrBasis,sizeType K);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual NRDMPLayerMTMu& operator=(const NRDMPLayerMTMu& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt);
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
  virtual void configOnline(const boost::property_tree::ptree& pt) override;
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType K() const;
  virtual Matd getBounds(const boost::property_tree::ptree& pt) const override;
private:
  NRDMPLayerMTMu(const string& name);
  sizeType _setToTask;
  Mss _muK,_dmuK;
};
//this is multitask rhythmic movement primitives:
//u_i^k = \sum_j*exp((h_{ij}^k)^2(cos(tau*t-mu_{ij})-1))*w_{ij}^k
class DMPLayerMTWH : public DMPLayer
{
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
public:
  DMPLayerMTWH();
  DMPLayerMTWH(sizeType nrOutput,sizeType nrBasis,sizeType K);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual DMPLayerMTWH& operator=(const DMPLayerMTWH& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt);
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
  virtual void configOnline(const boost::property_tree::ptree& pt) override;
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType K() const;
  virtual Matd getBounds(const boost::property_tree::ptree& pt) const override;
private:
  DMPLayerMTWH(const string& name);
  sizeType _setToTask;
  Vss _wBiasK,_dwBiasK;
  Mss _wK,_dwK;
  Mss _hK,_dhK;
};
//this is non-rhythmic movement primitives:
//u_i^k = \sum_j*exp(-(h_{ij}^k*t-mu_{ij})^2)*w_{ij}^k*(t-tau)
class NRDMPLayerMTWH : public NRDMPLayer
{
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
public:
  NRDMPLayerMTWH();
  NRDMPLayerMTWH(sizeType nrOutput,sizeType nrBasis,sizeType K);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual NRDMPLayerMTWH& operator=(const NRDMPLayerMTWH& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt);
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
  virtual void configOnline(const boost::property_tree::ptree& pt) override;
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType K() const;
  virtual Matd getBounds(const boost::property_tree::ptree& pt) const override;
private:
  NRDMPLayerMTWH(const string& name);
  sizeType _setToTask;
  Vss _wBiasK,_dwBiasK;
  Mss _wK,_dwK;
  Mss _hK,_dhK;
};
//this is multitask rhythmic movement cos-only primitives:
//u_i^k = \sum_j*cos(tau*t-mu_{ij})*w_{ij}^k
class CosDMPLayerMTWH : public CosDMPLayer
{
  typedef vector<Vec,Eigen::aligned_allocator<Vec> > Vss;
public:
  CosDMPLayerMTWH();
  CosDMPLayerMTWH(sizeType nrOutput,sizeType nrBasis,sizeType K);
  virtual bool read(istream& is,IOData* dat);
  virtual bool write(ostream& os,IOData* dat) const;
  virtual CosDMPLayerMTWH& operator=(const CosDMPLayerMTWH& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual void foreprop(const Matd& x,Matd& fx,void* dat=NULL) const override;
  virtual void backprop(const Matd& x,const Matd& fx,Matd& dEdx,const Matd& dEdfx,void* dat=NULL) override;
  virtual void augment(sizeType nrDMPBasis,const boost::property_tree::ptree& pt);
  virtual void configOffline(const boost::property_tree::ptree& pt) override;
  virtual void configOnline(const boost::property_tree::ptree& pt) override;
  virtual void setParam(const Vec& p,sizeType off,bool diff);
  virtual void param(Vec& p,sizeType off,bool diff) const;
  virtual sizeType nrParam() const;
  virtual sizeType K() const;
  virtual Matd getBounds(const boost::property_tree::ptree& pt) const override;
private:
  CosDMPLayerMTWH(const string& name);
  sizeType _setToTask;
  Vss _wBiasK,_dwBiasK;
  Mss _wK,_dwK;
};

//typedef DMPLayerMTMu DMPLayerMT;
//typedef NRDMPLayerMTMu NRDMPLayerMT;

typedef DMPLayerMTWH DMPLayerMT;
typedef NRDMPLayerMTWH NRDMPLayerMT;
typedef CosDMPLayerMTWH CosDMPLayerMT;

PRJ_END

#endif
