#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("fishSwimTRPO")
int main(int argc,char** argv)
{
  OmpSettings::getOmpSettingsNonConst().setNrThreads((int)omp_get_num_procs());
  boost::property_tree::ptree tree;
  createFishSwim(tree,5,false,false);
  tree.put<scalar>("scaleAction",100);
  tree.put<bool>("GPUEval",true);
  parseProps(argc,argv,tree);

  FEMEnvironment env;
  if(exists(NAME+"Learned.dat")) {
    env.Environment::read(NAME+"Learned.dat");
    env.fixRotXZ();
  } else {
    if(!exists(NAME+".dat")) {
      env.reset(tree);
      env.Environment::write(NAME+".dat");
    } else {
      env.Environment::read(NAME+".dat");
    }
    env.sol().getBody()._tree.put<bool>("MTCubature",true);
    env.sol()._tree.put<bool>("useCallback",false);
    env.fixRotXZ();

    TRPO learner(env);
    parseProps(argc,argv,learner._tree);
    learner._tree.put<sizeType>("maxIter",2E2);
    learner._tree.put<sizeType>("trajLen",2E2);
    learner._tree.put<sizeType>("maxTraj",1E2);
    learner._tree.put<scalar>("maxKLDivergence",0.1f);
    learner._tree.put<scalar>("minKLDivergence",0.01f);
    //learner._tree.put<bool>("debugTRPOIter",true);
    //learner._tree.put<bool>("useCallback",true);
    learner.seed(learner._tree.get<sizeType>("seed",0));
    //learner.debugSurrogateGradient();
    //learner.debugKLDistance();
    learner.learn();
    env.Environment::write(NAME+"Learned.dat");
  }

  TrainingData dat;
  env.getPolicy().setDeterministic(true);
  dat.randomSample(10,10,200,200,env,NULL,NAME+"Learned");
  return 0;
}
