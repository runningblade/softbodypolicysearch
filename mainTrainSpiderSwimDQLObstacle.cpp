#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("spiderSwimMTOPT")
int main(int argc,char** argv)
{
  OmpSettings::getOmpSettingsNonConst().setNrThreads((int)omp_get_num_procs());
  FEMEnvironmentMT env;
  ASSERT(exists(NAME+"Learned.dat"))
  env.Environment::read(NAME+"Learned.dat");
  string addParam=parseProps(argc,argv,env.sol()._tree);

  //configure QLearning
  boost::shared_ptr<FEMEnvironmentDQNObstacle> envDQL;
  if(!exists(NAME+addParam+"DQNObstacle.dat")) {
    env.sol()._tree.put<sizeType>("nrH3",40);
    env.sol()._tree.put<sizeType>("nrObstacle",2);
    env.sol()._tree.put<bool>("useNeuralQ",true);
    env.sol()._tree.put<scalar>("actionCostCoef0",0);
    env.sol()._tree.put<scalar>("actionCostCoef1",0);
    env.sol()._tree.put<scalar>("actionCostCoef2",0);
    env.sol()._tree.put<scalar>("actionCostCoef3",0);
    env.sol()._tree.put<string>("obstacleType","middle");
    envDQL=env.upgradeDQNObstacle();
    envDQL->Environment::write(NAME+addParam+"DQNObstacle.dat");
  } else {
    envDQL.reset(new FEMEnvironmentDQNObstacle);
    envDQL->Environment::read(NAME+addParam+"DQNObstacle.dat");
  }
  envDQL->setUseFit(envDQL->sol()._tree.get<bool>("useFitTrajectory",true));
  envDQL->setUseFixTarget(envDQL->sol()._tree.get<bool>("useFixTarget",false));
  envDQL->setUsePeriodicTarget(6,5);

  //learn
  if(!exists(NAME+addParam+"LearnedDQNObstacle.dat")) {
    QLearning learner(*envDQL);
    envDQL->getPolicy()._net->_tree.put<sizeType>("maxIterations",1E2);
    envDQL->getPolicy()._net->_tree.put<bool>("callbackLBFGS",false);
    learner._tree.put<scalar>("learningRate",0.1f);
    learner._tree.put<sizeType>("nrTraj",envDQL->nrTraj());
    learner._tree.put<sizeType>("trajLen",200);
    learner._tree.put<sizeType>("maxIter",10000);
    learner._tree.put<sizeType>("maxIterOpt",1);
    learner._tree.put<bool>("useWholeMemoryNewton",false);
    learner._tree.put<bool>("useWholeMemoryRMSProp",false);
    learner._tree.put<bool>("useWholeMemoryLBFGS",true);
    parseProps(argc,argv,learner._tree);
    learner.learn();
    envDQL->Environment::write(NAME+addParam+"LearnedDQNObstacle.dat");
    envDQL->debugTrajectory("./trajs",
                            learner._tree.get<sizeType>("nrTraj"),
                            learner._tree.get<sizeType>("trajLen"));
  } else {
    RandEngine::seedTime();
    envDQL->Environment::read(NAME+addParam+"LearnedDQNObstacle.dat");
    envDQL->setUseFit(false);
    envDQL->setUseFixTarget(false);
    envDQL->addEmbeddedMesh("./Models/4LegWalkerOpenS1.obj");
    envDQL->getPolicy().setEps(0);
    envDQL->debugTrajectory("./trajs",3,200);
  }
  return 0;
}
