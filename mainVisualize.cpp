#include "mainUtils.h"
#include <CommonFile/geom/StaticGeom.h>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  ASSERT(argc >= 2)
  FEMEnvironment env;
  FEMDDPSolver sol(env);
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is(argv[1],ios::binary);
    sol.read(is,dat.get());
  }
  parseProps(argc,argv,env.sol()._tree);
  sol.writeStatesVTK("./states");
  sol.writeEnvVTK("./states/environment.vtk");
  return 0;
}
