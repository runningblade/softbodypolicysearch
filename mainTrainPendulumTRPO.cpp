#include "mainUtils.h"

USE_PRJ_NAMESPACE

#define NAME string("pendulumTRPO")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  tree.put<bool>("useNeural",true);
  tree.put<bool>("useNeuralQ",false);
  tree.put<bool>("useNeuralProbQ",false);
  tree.put<bool>("useSimpleQ",false);
  tree.put<bool>("useStablizedSimpleQ",false);
  tree.put<bool>("balancing",false);
  parseProps(argc,argv,tree);

  PendulumEnvironment env;
  MPIInterface::createMPI(argc,argv);
  TrainingData::mpiSlaveRun(env);
  env.reset(tree);
  env.Environment::write(NAME+".dat");

  TRPO learner(env);
  learner._tree.put<sizeType>("maxIter",2E2);
  learner._tree.put<sizeType>("trajLen",5E2);
  learner._tree.put<scalar>("maxKLDivergence",0.001f);
  parseProps(argc,argv,learner._tree);
  //learner.debugSurrogateGradient();
  //learner.debugKLDistance();
  learner.seed(learner._tree.get<sizeType>("seed",0));
  learner.learn();

  TrainingData dat;
  dat.randomSample(5,5,1000,1000,env,NULL,NAME+"Learned");
  TrainingData::mpiExit();
  return 0;
}
