#include <SoftBodyController/DMPNeuralNet.h>
#include <SoftBodyController/FEMFeature.h>
#include <SoftBodyController/TrainingData.h>
#include <boost/lexical_cast.hpp>
#include "mainUtils.h"

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
#define K 3
#define NR_ACTION 10
  //DMP
  {
    TrainingData datum;
    RandomTrajectorySampler sampler(1,NR_ACTION);
    datum.randomSample(5,5,10,10,sampler);
    DMPNeuralNet net(NR_ACTION,10);
    testNetReadWrite(net);
    net.debugForeBackprop(datum,NULL,NULL,false);
  }
  //CosDMP
  {
    TrainingData datum;
    RandomTrajectorySampler sampler(1,NR_ACTION);
    datum.randomSample(5,5,10,10,sampler);
    boost::property_tree::ptree pt;
    pt.put<bool>("cosOnly",true);
    DMPNeuralNet net(NR_ACTION,10,&pt);
    testNetReadWrite(net);
    net.debugForeBackprop(datum,NULL,NULL,false);
  }
  //DMPMT
  {
    TrainingData datum;
    RandomTrajectorySampler sampler(1,NR_ACTION);
    datum.randomSample(5,5,15,15,sampler);
    boost::property_tree::ptree pt;
    pt.put<sizeType>("multitask",K);
    DMPNeuralNet net(NR_ACTION,10,&pt);
    testNetReadWrite(net);
    net.debugForeBackprop(datum,NULL,NULL,false);
  }
  //CosDMPMT
  {
    TrainingData datum;
    RandomTrajectorySampler sampler(1,NR_ACTION);
    datum.randomSample(5,5,15,15,sampler);
    boost::property_tree::ptree pt;
    pt.put<sizeType>("multitask",K);
    pt.put<bool>("cosOnly",true);
    DMPNeuralNet net(NR_ACTION,10,&pt);
    testNetReadWrite(net);
    net.debugForeBackprop(datum,NULL,NULL,false);
  }
#undef NR_ACTION
#undef K
  return 0;
}
