#include "mainUtils.h"
#include <SoftBodyController/FEMDDPSynthesis.h>

USE_PRJ_NAMESPACE

#define PATH string("CactusJump16/")
#define NAME string("cactusJumpOPTLearned")
int main(int argc,char** argv)
{
  boost::property_tree::ptree tree;
  createCrossWalk(tree,10);
  parseProps(argc,argv,tree);

  FEMEnvironment env;
  ASSERT(exists(PATH+NAME+".dat"))
  env.Environment::read(PATH+NAME+".dat");
  //env.addEmbeddedMesh("Models/");

  boost::property_tree::ptree pt;
  readPtreeAscii(pt,PATH+"optTrajs.xml");
  FEMDDPSynthesis syn(env);
  syn.addTaskData(pt);
  readPtreeAscii(syn._tree,PATH+"CIOSynthesis.xml");
  syn.solve();
  syn.writeStatesVTK(PATH+"cactusJumpSynthesisVTK",NULL,false,false);
  syn.writeStatesVTK(PATH+"cactusJumpSynthesis",NULL,false,true);
  return 0;
}
