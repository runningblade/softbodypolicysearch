#include <SoftBodyController/NeuralNet.h>
#include <SoftBodyController/FEMFeature.h>
#include <SoftBodyController/TrainingData.h>
#include <boost/lexical_cast.hpp>
#include "mainUtils.h"

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
#define NR_STATE 32
#define NR_ACTION 5
  boost::shared_ptr<FeatureTransformLayer> feat1(new FEMRSFeatureTransformLayer(NR_STATE,0.05f,3,true));
  boost::shared_ptr<FeatureTransformLayer> feat2(new FEMRotRSFeatureTransformLayer(NR_STATE,0.05f,false,false,false));
  boost::shared_ptr<FeatureTransformLayer> feat3(new ConstFeatureTransformLayer(3));
  boost::shared_ptr<FeatureTransformLayer> feat4(new FEMRotRSFeatureTransformLayer(NR_STATE,0.05f,false,true,false));
  boost::shared_ptr<FeatureTransformLayer> feat5(new NoFeatureTransformLayer(5));
  boost::shared_ptr<FeatureTransformLayer> feat6(new FEMRotRSFeatureTransformLayer(NR_STATE,0.05f,false,true,true));
  boost::shared_ptr<FeatureTransformLayer> featAugIP(new FeatureAugmentedInPlace(feat1,feat2,feat3,feat4,feat5,feat6));
  NeuralNet net(2,3,4,featAugIP->nrInput(),NR_ACTION,NULL,NULL,featAugIP);
  net._tree.put<scalar>("augment0",RandEngine::randR01());
  net._tree.put<scalar>("augment1",RandEngine::randR01());
  net._tree.put<scalar>("augment2",RandEngine::randR01());
  net.resetOnline();
  testNetReadWrite(net);

  TrainingData datum;
  RandomTrajectorySampler sampler(featAugIP->nrInput(),NR_ACTION);
  datum.randomSample(5,5,10,10,sampler);
  net.debugForeBackprop(datum);
  net.debugWeightprop(datum,true,true);
#undef NR_STATE
#undef NR_ACTION
  return 0;
}
